/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Willow Garage, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Auto-generated by genmsg_cpp from file /home/ines/catkin_ws/src/monarch_msgs/msg/PersonLocalizationTrackingData.msg
 *
 */


#ifndef MONARCH_MSGS_MESSAGE_PERSONLOCALIZATIONTRACKINGDATA_H
#define MONARCH_MSGS_MESSAGE_PERSONLOCALIZATIONTRACKINGDATA_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/Point.h>

namespace monarch_msgs
{
template <class ContainerAllocator>
struct PersonLocalizationTrackingData_
{
  typedef PersonLocalizationTrackingData_<ContainerAllocator> Type;

  PersonLocalizationTrackingData_()
    : header()
    , id(0)
    , pose()
    , groupFellows()
    , vel()
    , isActive()  {
      isActive.assign(false);
  }
  PersonLocalizationTrackingData_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , id(0)
    , pose(_alloc)
    , groupFellows(_alloc)
    , vel(_alloc)
    , isActive()  {
      isActive.assign(false);
  }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef uint32_t _id_type;
  _id_type id;

   typedef  ::geometry_msgs::PoseWithCovariance_<ContainerAllocator>  _pose_type;
  _pose_type pose;

   typedef std::vector<uint32_t, typename ContainerAllocator::template rebind<uint32_t>::other >  _groupFellows_type;
  _groupFellows_type groupFellows;

   typedef  ::geometry_msgs::Point_<ContainerAllocator>  _vel_type;
  _vel_type vel;

   typedef boost::array<uint8_t, 4>  _isActive_type;
  _isActive_type isActive;


    enum { ACT_WALK = 0 };
     enum { ACT_RUN = 1 };
     enum { ACT_CRAFT = 2 };
     enum { ACT_TV = 3 };
     enum { NUM_ACT = 4 };
 

  typedef boost::shared_ptr< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;

}; // struct PersonLocalizationTrackingData_

typedef ::monarch_msgs::PersonLocalizationTrackingData_<std::allocator<void> > PersonLocalizationTrackingData;

typedef boost::shared_ptr< ::monarch_msgs::PersonLocalizationTrackingData > PersonLocalizationTrackingDataPtr;
typedef boost::shared_ptr< ::monarch_msgs::PersonLocalizationTrackingData const> PersonLocalizationTrackingDataConstPtr;

// constants requiring out of line definition

   

   

   

   

   



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace monarch_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'sensor_msgs': ['/opt/ros/hydro/share/sensor_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/hydro/share/actionlib_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/hydro/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/hydro/share/geometry_msgs/cmake/../msg'], 'monarch_msgs': ['/home/ines/catkin_ws/src/monarch_msgs/msg'], 'move_base_msgs': ['/opt/ros/hydro/share/move_base_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "7f6cba6f5fae2718b20684bc938786c2";
  }

  static const char* value(const ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x7f6cba6f5fae2718ULL;
  static const uint64_t static_value2 = 0xb20684bc938786c2ULL;
};

template<class ContainerAllocator>
struct DataType< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "monarch_msgs/PersonLocalizationTrackingData";
  }

  static const char* value(const ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
{
  static const char* value()
  {
    return "#use standard header\n\
Header header\n\
\n\
uint32 id                              # tracker id\n\
geometry_msgs/PoseWithCovariance pose  # tracked person estimated pose\n\
uint32[] groupFellows                  # Who is this person in a group with\n\
geometry_msgs/Point vel                # Velocity of the person in x,y,z\n\
\n\
int32 ACT_WALK=0                       # if isActive[ACT_WALK]==true, the person is detected as walking\n\
int32 ACT_RUN=1                        # Running\n\
int32 ACT_CRAFT=2                      # The person is doing handwork at a table\n\
int32 ACT_TV=3                         # The person is watching TV\n\
int32 NUM_ACT=4                        # Length of the isActive array. The idea is that code can check whether any activity is\n\
                                       #    being detected without updating the code as new activities are added to the list.\n\
bool[4] isActive                       # SIZE MUST BE UPDATED AS NUM_ACT IS UPDATED\n\
                                       # What activities are discovered for this person \n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: geometry_msgs/PoseWithCovariance\n\
# This represents a pose in free space with uncertainty.\n\
\n\
Pose pose\n\
\n\
# Row-major representation of the 6x6 covariance matrix\n\
# The orientation parameters use a fixed-axis representation.\n\
# In order, the parameters are:\n\
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)\n\
float64[36] covariance\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Pose\n\
# A representation of pose in free space, composed of postion and orientation. \n\
Point position\n\
Quaternion orientation\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Quaternion\n\
# This represents an orientation in free space in quaternion form.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
float64 w\n\
";
  }

  static const char* value(const ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.id);
      stream.next(m.pose);
      stream.next(m.groupFellows);
      stream.next(m.vel);
      stream.next(m.isActive);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER;
  }; // struct PersonLocalizationTrackingData_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::monarch_msgs::PersonLocalizationTrackingData_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "id: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.id);
    s << indent << "pose: ";
    s << std::endl;
    Printer< ::geometry_msgs::PoseWithCovariance_<ContainerAllocator> >::stream(s, indent + "  ", v.pose);
    s << indent << "groupFellows[]" << std::endl;
    for (size_t i = 0; i < v.groupFellows.size(); ++i)
    {
      s << indent << "  groupFellows[" << i << "]: ";
      Printer<uint32_t>::stream(s, indent + "  ", v.groupFellows[i]);
    }
    s << indent << "vel: ";
    s << std::endl;
    Printer< ::geometry_msgs::Point_<ContainerAllocator> >::stream(s, indent + "  ", v.vel);
    s << indent << "isActive[]" << std::endl;
    for (size_t i = 0; i < v.isActive.size(); ++i)
    {
      s << indent << "  isActive[" << i << "]: ";
      Printer<uint8_t>::stream(s, indent + "  ", v.isActive[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // MONARCH_MSGS_MESSAGE_PERSONLOCALIZATIONTRACKINGDATA_H
