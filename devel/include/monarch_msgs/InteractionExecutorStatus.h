/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Willow Garage, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Auto-generated by genmsg_cpp from file /home/ines/catkin_ws/src/monarch_msgs/msg/InteractionExecutorStatus.msg
 *
 */


#ifndef MONARCH_MSGS_MESSAGE_INTERACTIONEXECUTORSTATUS_H
#define MONARCH_MSGS_MESSAGE_INTERACTIONEXECUTORSTATUS_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace monarch_msgs
{
template <class ContainerAllocator>
struct InteractionExecutorStatus_
{
  typedef InteractionExecutorStatus_<ContainerAllocator> Type;

  InteractionExecutorStatus_()
    : expression_name()
    , status()  {
    }
  InteractionExecutorStatus_(const ContainerAllocator& _alloc)
    : expression_name(_alloc)
    , status(_alloc)  {
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _expression_name_type;
  _expression_name_type expression_name;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _status_type;
  _status_type status;




  typedef boost::shared_ptr< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;

}; // struct InteractionExecutorStatus_

typedef ::monarch_msgs::InteractionExecutorStatus_<std::allocator<void> > InteractionExecutorStatus;

typedef boost::shared_ptr< ::monarch_msgs::InteractionExecutorStatus > InteractionExecutorStatusPtr;
typedef boost::shared_ptr< ::monarch_msgs::InteractionExecutorStatus const> InteractionExecutorStatusConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace monarch_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sensor_msgs': ['/opt/ros/hydro/share/sensor_msgs/cmake/../msg'], 'actionlib_msgs': ['/opt/ros/hydro/share/actionlib_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/hydro/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/hydro/share/geometry_msgs/cmake/../msg'], 'monarch_msgs': ['/home/ines/catkin_ws/src/monarch_msgs/msg'], 'move_base_msgs': ['/opt/ros/hydro/share/move_base_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
{
  static const char* value()
  {
    return "f5dff72b84a9a97e8f1cf9d1b4daefdf";
  }

  static const char* value(const ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xf5dff72b84a9a97eULL;
  static const uint64_t static_value2 = 0x8f1cf9d1b4daefdfULL;
};

template<class ContainerAllocator>
struct DataType< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
{
  static const char* value()
  {
    return "monarch_msgs/InteractionExecutorStatus";
  }

  static const char* value(const ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# Interaction Execution Status message\n\
\n\
string expression_name	# Eg. \"say_text\", \"express_emotion\", etc.\n\
string status       	# Status of interaction. Accepted values are \"OK\", \"ERROR\".\n\
";
  }

  static const char* value(const ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.expression_name);
      stream.next(m.status);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER;
  }; // struct InteractionExecutorStatus_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::monarch_msgs::InteractionExecutorStatus_<ContainerAllocator>& v)
  {
    s << indent << "expression_name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.expression_name);
    s << indent << "status: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.status);
  }
};

} // namespace message_operations
} // namespace ros

#endif // MONARCH_MSGS_MESSAGE_INTERACTIONEXECUTORSTATUS_H
