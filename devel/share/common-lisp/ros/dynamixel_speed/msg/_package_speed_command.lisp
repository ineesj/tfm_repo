(cl:in-package dynamixel_speed-msg)
(cl:export '(HEAD-VAL
          HEAD
          NECK-VAL
          NECK
          LEFTARM-VAL
          LEFTARM
          RIGHTARM-VAL
          RIGHTARM
          BASE-VAL
          BASE
))