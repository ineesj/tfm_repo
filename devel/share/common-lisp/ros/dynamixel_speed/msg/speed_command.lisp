; Auto-generated. Do not edit!


(cl:in-package dynamixel_speed-msg)


;//! \htmlinclude speed_command.msg.html

(cl:defclass <speed_command> (roslisp-msg-protocol:ros-message)
  ((head
    :reader head
    :initarg :head
    :type cl:float
    :initform 0.0)
   (neck
    :reader neck
    :initarg :neck
    :type cl:float
    :initform 0.0)
   (leftArm
    :reader leftArm
    :initarg :leftArm
    :type cl:float
    :initform 0.0)
   (rightArm
    :reader rightArm
    :initarg :rightArm
    :type cl:float
    :initform 0.0)
   (base
    :reader base
    :initarg :base
    :type cl:float
    :initform 0.0))
)

(cl:defclass speed_command (<speed_command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <speed_command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'speed_command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dynamixel_speed-msg:<speed_command> is deprecated: use dynamixel_speed-msg:speed_command instead.")))

(cl:ensure-generic-function 'head-val :lambda-list '(m))
(cl:defmethod head-val ((m <speed_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dynamixel_speed-msg:head-val is deprecated.  Use dynamixel_speed-msg:head instead.")
  (head m))

(cl:ensure-generic-function 'neck-val :lambda-list '(m))
(cl:defmethod neck-val ((m <speed_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dynamixel_speed-msg:neck-val is deprecated.  Use dynamixel_speed-msg:neck instead.")
  (neck m))

(cl:ensure-generic-function 'leftArm-val :lambda-list '(m))
(cl:defmethod leftArm-val ((m <speed_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dynamixel_speed-msg:leftArm-val is deprecated.  Use dynamixel_speed-msg:leftArm instead.")
  (leftArm m))

(cl:ensure-generic-function 'rightArm-val :lambda-list '(m))
(cl:defmethod rightArm-val ((m <speed_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dynamixel_speed-msg:rightArm-val is deprecated.  Use dynamixel_speed-msg:rightArm instead.")
  (rightArm m))

(cl:ensure-generic-function 'base-val :lambda-list '(m))
(cl:defmethod base-val ((m <speed_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dynamixel_speed-msg:base-val is deprecated.  Use dynamixel_speed-msg:base instead.")
  (base m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <speed_command>) ostream)
  "Serializes a message object of type '<speed_command>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'head))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'neck))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'leftArm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'rightArm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'base))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <speed_command>) istream)
  "Deserializes a message object of type '<speed_command>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'head) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'neck) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'leftArm) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'rightArm) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'base) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<speed_command>)))
  "Returns string type for a message object of type '<speed_command>"
  "dynamixel_speed/speed_command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'speed_command)))
  "Returns string type for a message object of type 'speed_command"
  "dynamixel_speed/speed_command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<speed_command>)))
  "Returns md5sum for a message object of type '<speed_command>"
  "8d12770d387428c19819f2908436113f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'speed_command)))
  "Returns md5sum for a message object of type 'speed_command"
  "8d12770d387428c19819f2908436113f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<speed_command>)))
  "Returns full string definition for message of type '<speed_command>"
  (cl:format cl:nil "float64 head~%float64 neck~%float64 leftArm~%float64 rightArm~%float64 base~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'speed_command)))
  "Returns full string definition for message of type 'speed_command"
  (cl:format cl:nil "float64 head~%float64 neck~%float64 leftArm~%float64 rightArm~%float64 base~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <speed_command>))
  (cl:+ 0
     8
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <speed_command>))
  "Converts a ROS message object to a list"
  (cl:list 'speed_command
    (cl:cons ':head (head msg))
    (cl:cons ':neck (neck msg))
    (cl:cons ':leftArm (leftArm msg))
    (cl:cons ':rightArm (rightArm msg))
    (cl:cons ':base (base msg))
))
