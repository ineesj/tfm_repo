
(cl:in-package :asdf)

(defsystem "dynamixel_speed-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "speed_command" :depends-on ("_package_speed_command"))
    (:file "_package_speed_command" :depends-on ("_package"))
  ))