(cl:in-package liveliness_init-msg)
(cl:export '(NECK-VAL
          NECK
          ARMS-VAL
          ARMS
          SCREEN-VAL
          SCREEN
          EYES-VAL
          EYES
          BASE-VAL
          BASE
          HEAD-VAL
          HEAD
))