; Auto-generated. Do not edit!


(cl:in-package liveliness_init-msg)


;//! \htmlinclude signalParameters.msg.html

(cl:defclass <signalParameters> (roslisp-msg-protocol:ros-message)
  ((amplitud
    :reader amplitud
    :initarg :amplitud
    :type cl:float
    :initform 0.0)
   (frecuencia
    :reader frecuencia
    :initarg :frecuencia
    :type cl:float
    :initform 0.0))
)

(cl:defclass signalParameters (<signalParameters>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <signalParameters>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'signalParameters)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name liveliness_init-msg:<signalParameters> is deprecated: use liveliness_init-msg:signalParameters instead.")))

(cl:ensure-generic-function 'amplitud-val :lambda-list '(m))
(cl:defmethod amplitud-val ((m <signalParameters>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:amplitud-val is deprecated.  Use liveliness_init-msg:amplitud instead.")
  (amplitud m))

(cl:ensure-generic-function 'frecuencia-val :lambda-list '(m))
(cl:defmethod frecuencia-val ((m <signalParameters>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:frecuencia-val is deprecated.  Use liveliness_init-msg:frecuencia instead.")
  (frecuencia m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <signalParameters>) ostream)
  "Serializes a message object of type '<signalParameters>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'amplitud))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'frecuencia))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <signalParameters>) istream)
  "Deserializes a message object of type '<signalParameters>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'amplitud) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'frecuencia) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<signalParameters>)))
  "Returns string type for a message object of type '<signalParameters>"
  "liveliness_init/signalParameters")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'signalParameters)))
  "Returns string type for a message object of type 'signalParameters"
  "liveliness_init/signalParameters")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<signalParameters>)))
  "Returns md5sum for a message object of type '<signalParameters>"
  "47a8c8dc82d1415d2093047b2f5a6346")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'signalParameters)))
  "Returns md5sum for a message object of type 'signalParameters"
  "47a8c8dc82d1415d2093047b2f5a6346")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<signalParameters>)))
  "Returns full string definition for message of type '<signalParameters>"
  (cl:format cl:nil "~%float32 amplitud~%float32 frecuencia~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'signalParameters)))
  "Returns full string definition for message of type 'signalParameters"
  (cl:format cl:nil "~%float32 amplitud~%float32 frecuencia~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <signalParameters>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <signalParameters>))
  "Converts a ROS message object to a list"
  (cl:list 'signalParameters
    (cl:cons ':amplitud (amplitud msg))
    (cl:cons ':frecuencia (frecuencia msg))
))
