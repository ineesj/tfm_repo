; Auto-generated. Do not edit!


(cl:in-package liveliness_init-msg)


;//! \htmlinclude colorsParameters.msg.html

(cl:defclass <colorsParameters> (roslisp-msg-protocol:ros-message)
  ((R
    :reader R
    :initarg :R
    :type cl:integer
    :initform 0)
   (G
    :reader G
    :initarg :G
    :type cl:integer
    :initform 0)
   (B
    :reader B
    :initarg :B
    :type cl:integer
    :initform 0))
)

(cl:defclass colorsParameters (<colorsParameters>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <colorsParameters>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'colorsParameters)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name liveliness_init-msg:<colorsParameters> is deprecated: use liveliness_init-msg:colorsParameters instead.")))

(cl:ensure-generic-function 'R-val :lambda-list '(m))
(cl:defmethod R-val ((m <colorsParameters>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:R-val is deprecated.  Use liveliness_init-msg:R instead.")
  (R m))

(cl:ensure-generic-function 'G-val :lambda-list '(m))
(cl:defmethod G-val ((m <colorsParameters>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:G-val is deprecated.  Use liveliness_init-msg:G instead.")
  (G m))

(cl:ensure-generic-function 'B-val :lambda-list '(m))
(cl:defmethod B-val ((m <colorsParameters>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:B-val is deprecated.  Use liveliness_init-msg:B instead.")
  (B m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <colorsParameters>) ostream)
  "Serializes a message object of type '<colorsParameters>"
  (cl:let* ((signed (cl:slot-value msg 'R)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'G)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'B)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <colorsParameters>) istream)
  "Deserializes a message object of type '<colorsParameters>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'R) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'G) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'B) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<colorsParameters>)))
  "Returns string type for a message object of type '<colorsParameters>"
  "liveliness_init/colorsParameters")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'colorsParameters)))
  "Returns string type for a message object of type 'colorsParameters"
  "liveliness_init/colorsParameters")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<colorsParameters>)))
  "Returns md5sum for a message object of type '<colorsParameters>"
  "6214ebbb91c32d0932ce07bca977914c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'colorsParameters)))
  "Returns md5sum for a message object of type 'colorsParameters"
  "6214ebbb91c32d0932ce07bca977914c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<colorsParameters>)))
  "Returns full string definition for message of type '<colorsParameters>"
  (cl:format cl:nil "~%int32 R~%int32 G~%int32 B~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'colorsParameters)))
  "Returns full string definition for message of type 'colorsParameters"
  (cl:format cl:nil "~%int32 R~%int32 G~%int32 B~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <colorsParameters>))
  (cl:+ 0
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <colorsParameters>))
  "Converts a ROS message object to a list"
  (cl:list 'colorsParameters
    (cl:cons ':R (R msg))
    (cl:cons ':G (G msg))
    (cl:cons ':B (B msg))
))
