; Auto-generated. Do not edit!


(cl:in-package liveliness_init-msg)


;//! \htmlinclude onOffInterfaces.msg.html

(cl:defclass <onOffInterfaces> (roslisp-msg-protocol:ros-message)
  ((neck
    :reader neck
    :initarg :neck
    :type cl:boolean
    :initform cl:nil)
   (arms
    :reader arms
    :initarg :arms
    :type cl:boolean
    :initform cl:nil)
   (screen
    :reader screen
    :initarg :screen
    :type cl:boolean
    :initform cl:nil)
   (eyes
    :reader eyes
    :initarg :eyes
    :type cl:boolean
    :initform cl:nil)
   (base
    :reader base
    :initarg :base
    :type cl:boolean
    :initform cl:nil)
   (head
    :reader head
    :initarg :head
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass onOffInterfaces (<onOffInterfaces>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <onOffInterfaces>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'onOffInterfaces)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name liveliness_init-msg:<onOffInterfaces> is deprecated: use liveliness_init-msg:onOffInterfaces instead.")))

(cl:ensure-generic-function 'neck-val :lambda-list '(m))
(cl:defmethod neck-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:neck-val is deprecated.  Use liveliness_init-msg:neck instead.")
  (neck m))

(cl:ensure-generic-function 'arms-val :lambda-list '(m))
(cl:defmethod arms-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:arms-val is deprecated.  Use liveliness_init-msg:arms instead.")
  (arms m))

(cl:ensure-generic-function 'screen-val :lambda-list '(m))
(cl:defmethod screen-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:screen-val is deprecated.  Use liveliness_init-msg:screen instead.")
  (screen m))

(cl:ensure-generic-function 'eyes-val :lambda-list '(m))
(cl:defmethod eyes-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:eyes-val is deprecated.  Use liveliness_init-msg:eyes instead.")
  (eyes m))

(cl:ensure-generic-function 'base-val :lambda-list '(m))
(cl:defmethod base-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:base-val is deprecated.  Use liveliness_init-msg:base instead.")
  (base m))

(cl:ensure-generic-function 'head-val :lambda-list '(m))
(cl:defmethod head-val ((m <onOffInterfaces>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader liveliness_init-msg:head-val is deprecated.  Use liveliness_init-msg:head instead.")
  (head m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <onOffInterfaces>) ostream)
  "Serializes a message object of type '<onOffInterfaces>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'neck) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'arms) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'screen) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'eyes) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'base) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'head) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <onOffInterfaces>) istream)
  "Deserializes a message object of type '<onOffInterfaces>"
    (cl:setf (cl:slot-value msg 'neck) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'arms) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'screen) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'eyes) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'base) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'head) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<onOffInterfaces>)))
  "Returns string type for a message object of type '<onOffInterfaces>"
  "liveliness_init/onOffInterfaces")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'onOffInterfaces)))
  "Returns string type for a message object of type 'onOffInterfaces"
  "liveliness_init/onOffInterfaces")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<onOffInterfaces>)))
  "Returns md5sum for a message object of type '<onOffInterfaces>"
  "7b844ed03f39c5220ffe026bfd740c90")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'onOffInterfaces)))
  "Returns md5sum for a message object of type 'onOffInterfaces"
  "7b844ed03f39c5220ffe026bfd740c90")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<onOffInterfaces>)))
  "Returns full string definition for message of type '<onOffInterfaces>"
  (cl:format cl:nil "~%bool neck~%bool arms~%bool screen~%bool eyes~%bool base~%bool head~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'onOffInterfaces)))
  "Returns full string definition for message of type 'onOffInterfaces"
  (cl:format cl:nil "~%bool neck~%bool arms~%bool screen~%bool eyes~%bool base~%bool head~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <onOffInterfaces>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <onOffInterfaces>))
  "Converts a ROS message object to a list"
  (cl:list 'onOffInterfaces
    (cl:cons ':neck (neck msg))
    (cl:cons ':arms (arms msg))
    (cl:cons ':screen (screen msg))
    (cl:cons ':eyes (eyes msg))
    (cl:cons ':base (base msg))
    (cl:cons ':head (head msg))
))
