
(cl:in-package :asdf)

(defsystem "liveliness_init-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "colorsParameters" :depends-on ("_package_colorsParameters"))
    (:file "_package_colorsParameters" :depends-on ("_package"))
    (:file "signalParameters" :depends-on ("_package_signalParameters"))
    (:file "_package_signalParameters" :depends-on ("_package"))
    (:file "onOffInterfaces" :depends-on ("_package_onOffInterfaces"))
    (:file "_package_onOffInterfaces" :depends-on ("_package"))
  ))