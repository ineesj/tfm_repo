; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude ScreenButton.msg.html

(cl:defclass <ScreenButton> (roslisp-msg-protocol:ros-message)
  ((button_name
    :reader button_name
    :initarg :button_name
    :type cl:string
    :initform "")
   (topic_name
    :reader topic_name
    :initarg :topic_name
    :type cl:string
    :initform "")
   (msg_value
    :reader msg_value
    :initarg :msg_value
    :type cl:string
    :initform ""))
)

(cl:defclass ScreenButton (<ScreenButton>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ScreenButton>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ScreenButton)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<ScreenButton> is deprecated: use monarch_msgs-msg:ScreenButton instead.")))

(cl:ensure-generic-function 'button_name-val :lambda-list '(m))
(cl:defmethod button_name-val ((m <ScreenButton>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:button_name-val is deprecated.  Use monarch_msgs-msg:button_name instead.")
  (button_name m))

(cl:ensure-generic-function 'topic_name-val :lambda-list '(m))
(cl:defmethod topic_name-val ((m <ScreenButton>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:topic_name-val is deprecated.  Use monarch_msgs-msg:topic_name instead.")
  (topic_name m))

(cl:ensure-generic-function 'msg_value-val :lambda-list '(m))
(cl:defmethod msg_value-val ((m <ScreenButton>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:msg_value-val is deprecated.  Use monarch_msgs-msg:msg_value instead.")
  (msg_value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ScreenButton>) ostream)
  "Serializes a message object of type '<ScreenButton>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'button_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'button_name))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'topic_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'topic_name))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'msg_value))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'msg_value))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ScreenButton>) istream)
  "Deserializes a message object of type '<ScreenButton>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'button_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'button_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'topic_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'topic_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'msg_value) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'msg_value) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ScreenButton>)))
  "Returns string type for a message object of type '<ScreenButton>"
  "monarch_msgs/ScreenButton")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ScreenButton)))
  "Returns string type for a message object of type 'ScreenButton"
  "monarch_msgs/ScreenButton")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ScreenButton>)))
  "Returns md5sum for a message object of type '<ScreenButton>"
  "1bf655cf2fb236edf668d5e413e6199b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ScreenButton)))
  "Returns md5sum for a message object of type 'ScreenButton"
  "1bf655cf2fb236edf668d5e413e6199b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ScreenButton>)))
  "Returns full string definition for message of type '<ScreenButton>"
  (cl:format cl:nil "~%string button_name	#Can be either text or a picture name. This picture~%					#should be located in the folder /data/icons~%					#of the screens project.~%					#Usage: \"text;ok\"    \"picture;yes_button.png\"~%					#(by default, it takes the text)~%					~%					~%string topic_name	#Topic in which the button should publish when pressed.~%					#The type of message should be std_msgs/String~%~%~%string msg_value	#value of the message that is sent when the button is~%					#pressed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ScreenButton)))
  "Returns full string definition for message of type 'ScreenButton"
  (cl:format cl:nil "~%string button_name	#Can be either text or a picture name. This picture~%					#should be located in the folder /data/icons~%					#of the screens project.~%					#Usage: \"text;ok\"    \"picture;yes_button.png\"~%					#(by default, it takes the text)~%					~%					~%string topic_name	#Topic in which the button should publish when pressed.~%					#The type of message should be std_msgs/String~%~%~%string msg_value	#value of the message that is sent when the button is~%					#pressed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ScreenButton>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'button_name))
     4 (cl:length (cl:slot-value msg 'topic_name))
     4 (cl:length (cl:slot-value msg 'msg_value))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ScreenButton>))
  "Converts a ROS message object to a list"
  (cl:list 'ScreenButton
    (cl:cons ':button_name (button_name msg))
    (cl:cons ':topic_name (topic_name msg))
    (cl:cons ':msg_value (msg_value msg))
))
