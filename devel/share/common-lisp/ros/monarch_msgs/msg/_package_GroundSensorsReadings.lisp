(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          LEFTFRONT-VAL
          LEFTFRONT
          RIGHTFRONT-VAL
          RIGHTFRONT
          LEFT-VAL
          LEFT
          RIGHT-VAL
          RIGHT
))