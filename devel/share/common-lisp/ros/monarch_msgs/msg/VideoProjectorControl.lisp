; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude VideoProjectorControl.msg.html

(cl:defclass <VideoProjectorControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (videoProjector
    :reader videoProjector
    :initarg :videoProjector
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass VideoProjectorControl (<VideoProjectorControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <VideoProjectorControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'VideoProjectorControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<VideoProjectorControl> is deprecated: use monarch_msgs-msg:VideoProjectorControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <VideoProjectorControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'videoProjector-val :lambda-list '(m))
(cl:defmethod videoProjector-val ((m <VideoProjectorControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:videoProjector-val is deprecated.  Use monarch_msgs-msg:videoProjector instead.")
  (videoProjector m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <VideoProjectorControl>) ostream)
  "Serializes a message object of type '<VideoProjectorControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'videoProjector) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <VideoProjectorControl>) istream)
  "Deserializes a message object of type '<VideoProjectorControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'videoProjector) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<VideoProjectorControl>)))
  "Returns string type for a message object of type '<VideoProjectorControl>"
  "monarch_msgs/VideoProjectorControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'VideoProjectorControl)))
  "Returns string type for a message object of type 'VideoProjectorControl"
  "monarch_msgs/VideoProjectorControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<VideoProjectorControl>)))
  "Returns md5sum for a message object of type '<VideoProjectorControl>"
  "34d315fcfceac1d66ec9ed16f0331da6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'VideoProjectorControl)))
  "Returns md5sum for a message object of type 'VideoProjectorControl"
  "34d315fcfceac1d66ec9ed16f0331da6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<VideoProjectorControl>)))
  "Returns full string definition for message of type '<VideoProjectorControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for video projector. True means projector enabled~%bool videoProjector~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'VideoProjectorControl)))
  "Returns full string definition for message of type 'VideoProjectorControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for video projector. True means projector enabled~%bool videoProjector~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <VideoProjectorControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <VideoProjectorControl>))
  "Converts a ROS message object to a list"
  (cl:list 'VideoProjectorControl
    (cl:cons ':header (header msg))
    (cl:cons ':videoProjector (videoProjector msg))
))
