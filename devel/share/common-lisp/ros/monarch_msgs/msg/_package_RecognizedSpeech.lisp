(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          RECOGNIZED-VAL
          RECOGNIZED
          CONFIDENCE-VAL
          CONFIDENCE
          SEMANTIC-VAL
          SEMANTIC
))