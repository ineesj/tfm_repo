(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          LR-VAL
          LR
          LG-VAL
          LG
          LB-VAL
          LB
          LCHANGE_TIME-VAL
          LCHANGE_TIME
          RR-VAL
          RR
          RG-VAL
          RG
          RB-VAL
          RB
          RCHANGE_TIME-VAL
          RCHANGE_TIME
))