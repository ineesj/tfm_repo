; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GaussianCostmapArray.msg.html

(cl:defclass <GaussianCostmapArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (costmaps
    :reader costmaps
    :initarg :costmaps
    :type (cl:vector monarch_msgs-msg:GaussianCostmap)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:GaussianCostmap :initial-element (cl:make-instance 'monarch_msgs-msg:GaussianCostmap))))
)

(cl:defclass GaussianCostmapArray (<GaussianCostmapArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GaussianCostmapArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GaussianCostmapArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GaussianCostmapArray> is deprecated: use monarch_msgs-msg:GaussianCostmapArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GaussianCostmapArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'costmaps-val :lambda-list '(m))
(cl:defmethod costmaps-val ((m <GaussianCostmapArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:costmaps-val is deprecated.  Use monarch_msgs-msg:costmaps instead.")
  (costmaps m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GaussianCostmapArray>) ostream)
  "Serializes a message object of type '<GaussianCostmapArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'costmaps))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'costmaps))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GaussianCostmapArray>) istream)
  "Deserializes a message object of type '<GaussianCostmapArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'costmaps) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'costmaps)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:GaussianCostmap))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GaussianCostmapArray>)))
  "Returns string type for a message object of type '<GaussianCostmapArray>"
  "monarch_msgs/GaussianCostmapArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GaussianCostmapArray)))
  "Returns string type for a message object of type 'GaussianCostmapArray"
  "monarch_msgs/GaussianCostmapArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GaussianCostmapArray>)))
  "Returns md5sum for a message object of type '<GaussianCostmapArray>"
  "47162dd502a9484ecb17ecfa16c4cd54")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GaussianCostmapArray)))
  "Returns md5sum for a message object of type 'GaussianCostmapArray"
  "47162dd502a9484ecb17ecfa16c4cd54")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GaussianCostmapArray>)))
  "Returns full string definition for message of type '<GaussianCostmapArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%GaussianCostmap[] costmaps~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/GaussianCostmap~%#use standard header~%~%Header header~%int32 id          # id of person associated to the costmap~%~%float32 mean_x    # x coordinate of the center of the costmap in map frame~%float32 mean_y   #  y coordinate of the center of the costmap in map frame~%float32 sigma_x    #  sigma of the costmap in x direction~%float32 sigma_y   #  sigma of the costmap in y direction~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GaussianCostmapArray)))
  "Returns full string definition for message of type 'GaussianCostmapArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%GaussianCostmap[] costmaps~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/GaussianCostmap~%#use standard header~%~%Header header~%int32 id          # id of person associated to the costmap~%~%float32 mean_x    # x coordinate of the center of the costmap in map frame~%float32 mean_y   #  y coordinate of the center of the costmap in map frame~%float32 sigma_x    #  sigma of the costmap in x direction~%float32 sigma_y   #  sigma of the costmap in y direction~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GaussianCostmapArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'costmaps) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GaussianCostmapArray>))
  "Converts a ROS message object to a list"
  (cl:list 'GaussianCostmapArray
    (cl:cons ':header (header msg))
    (cl:cons ':costmaps (costmaps msg))
))
