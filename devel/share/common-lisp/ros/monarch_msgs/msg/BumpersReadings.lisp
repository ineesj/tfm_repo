; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude BumpersReadings.msg.html

(cl:defclass <BumpersReadings> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (leftFrontBump
    :reader leftFrontBump
    :initarg :leftFrontBump
    :type cl:boolean
    :initform cl:nil)
   (rightFrontBump
    :reader rightFrontBump
    :initarg :rightFrontBump
    :type cl:boolean
    :initform cl:nil)
   (leftRearBump
    :reader leftRearBump
    :initarg :leftRearBump
    :type cl:boolean
    :initform cl:nil)
   (rightRearBump
    :reader rightRearBump
    :initarg :rightRearBump
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass BumpersReadings (<BumpersReadings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BumpersReadings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BumpersReadings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<BumpersReadings> is deprecated: use monarch_msgs-msg:BumpersReadings instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <BumpersReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'leftFrontBump-val :lambda-list '(m))
(cl:defmethod leftFrontBump-val ((m <BumpersReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFrontBump-val is deprecated.  Use monarch_msgs-msg:leftFrontBump instead.")
  (leftFrontBump m))

(cl:ensure-generic-function 'rightFrontBump-val :lambda-list '(m))
(cl:defmethod rightFrontBump-val ((m <BumpersReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFrontBump-val is deprecated.  Use monarch_msgs-msg:rightFrontBump instead.")
  (rightFrontBump m))

(cl:ensure-generic-function 'leftRearBump-val :lambda-list '(m))
(cl:defmethod leftRearBump-val ((m <BumpersReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftRearBump-val is deprecated.  Use monarch_msgs-msg:leftRearBump instead.")
  (leftRearBump m))

(cl:ensure-generic-function 'rightRearBump-val :lambda-list '(m))
(cl:defmethod rightRearBump-val ((m <BumpersReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightRearBump-val is deprecated.  Use monarch_msgs-msg:rightRearBump instead.")
  (rightRearBump m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BumpersReadings>) ostream)
  "Serializes a message object of type '<BumpersReadings>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftFrontBump) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightFrontBump) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftRearBump) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightRearBump) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BumpersReadings>) istream)
  "Deserializes a message object of type '<BumpersReadings>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'leftFrontBump) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightFrontBump) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'leftRearBump) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightRearBump) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BumpersReadings>)))
  "Returns string type for a message object of type '<BumpersReadings>"
  "monarch_msgs/BumpersReadings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BumpersReadings)))
  "Returns string type for a message object of type 'BumpersReadings"
  "monarch_msgs/BumpersReadings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BumpersReadings>)))
  "Returns md5sum for a message object of type '<BumpersReadings>"
  "85a5082e13e35d0585890bfa4041fdb4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BumpersReadings)))
  "Returns md5sum for a message object of type 'BumpersReadings"
  "85a5082e13e35d0585890bfa4041fdb4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BumpersReadings>)))
  "Returns full string definition for message of type '<BumpersReadings>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each bumper. True means bump detected~%bool leftFrontBump~%bool rightFrontBump~%bool leftRearBump~%bool rightRearBump~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BumpersReadings)))
  "Returns full string definition for message of type 'BumpersReadings"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each bumper. True means bump detected~%bool leftFrontBump~%bool rightFrontBump~%bool leftRearBump~%bool rightRearBump~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BumpersReadings>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BumpersReadings>))
  "Converts a ROS message object to a list"
  (cl:list 'BumpersReadings
    (cl:cons ':header (header msg))
    (cl:cons ':leftFrontBump (leftFrontBump msg))
    (cl:cons ':rightFrontBump (rightFrontBump msg))
    (cl:cons ':leftRearBump (leftRearBump msg))
    (cl:cons ':rightRearBump (rightRearBump msg))
))
