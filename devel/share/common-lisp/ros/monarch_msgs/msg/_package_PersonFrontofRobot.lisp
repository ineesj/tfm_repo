(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          ID-VAL
          ID
          PERSON_ORIENTATION-VAL
          PERSON_ORIENTATION
          DIRECTION_TO_PERSON-VAL
          DIRECTION_TO_PERSON
          DISTANCE-VAL
          DISTANCE
))