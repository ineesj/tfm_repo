; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude PersonLocalizationTrackingData.msg.html

(cl:defclass <PersonLocalizationTrackingData> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (id
    :reader id
    :initarg :id
    :type cl:integer
    :initform 0)
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:PoseWithCovariance
    :initform (cl:make-instance 'geometry_msgs-msg:PoseWithCovariance))
   (groupFellows
    :reader groupFellows
    :initarg :groupFellows
    :type (cl:vector cl:integer)
   :initform (cl:make-array 0 :element-type 'cl:integer :initial-element 0))
   (vel
    :reader vel
    :initarg :vel
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (isActive
    :reader isActive
    :initarg :isActive
    :type (cl:vector cl:boolean)
   :initform (cl:make-array 4 :element-type 'cl:boolean :initial-element cl:nil)))
)

(cl:defclass PersonLocalizationTrackingData (<PersonLocalizationTrackingData>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PersonLocalizationTrackingData>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PersonLocalizationTrackingData)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<PersonLocalizationTrackingData> is deprecated: use monarch_msgs-msg:PersonLocalizationTrackingData instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:id-val is deprecated.  Use monarch_msgs-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:pose-val is deprecated.  Use monarch_msgs-msg:pose instead.")
  (pose m))

(cl:ensure-generic-function 'groupFellows-val :lambda-list '(m))
(cl:defmethod groupFellows-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:groupFellows-val is deprecated.  Use monarch_msgs-msg:groupFellows instead.")
  (groupFellows m))

(cl:ensure-generic-function 'vel-val :lambda-list '(m))
(cl:defmethod vel-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:vel-val is deprecated.  Use monarch_msgs-msg:vel instead.")
  (vel m))

(cl:ensure-generic-function 'isActive-val :lambda-list '(m))
(cl:defmethod isActive-val ((m <PersonLocalizationTrackingData>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:isActive-val is deprecated.  Use monarch_msgs-msg:isActive instead.")
  (isActive m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<PersonLocalizationTrackingData>)))
    "Constants for message type '<PersonLocalizationTrackingData>"
  '((:ACT_WALK . 0)
    (:ACT_RUN . 1)
    (:ACT_CRAFT . 2)
    (:ACT_TV . 3)
    (:NUM_ACT . 4))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'PersonLocalizationTrackingData)))
    "Constants for message type 'PersonLocalizationTrackingData"
  '((:ACT_WALK . 0)
    (:ACT_RUN . 1)
    (:ACT_CRAFT . 2)
    (:ACT_TV . 3)
    (:NUM_ACT . 4))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PersonLocalizationTrackingData>) ostream)
  "Serializes a message object of type '<PersonLocalizationTrackingData>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'id)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'groupFellows))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) ele) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) ele) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) ele) ostream))
   (cl:slot-value msg 'groupFellows))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'vel) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if ele 1 0)) ostream))
   (cl:slot-value msg 'isActive))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PersonLocalizationTrackingData>) istream)
  "Deserializes a message object of type '<PersonLocalizationTrackingData>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'id)) (cl:read-byte istream))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'groupFellows) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'groupFellows)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:aref vals i)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:aref vals i)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:aref vals i)) (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'vel) istream)
  (cl:setf (cl:slot-value msg 'isActive) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'isActive)))
    (cl:dotimes (i 4)
    (cl:setf (cl:aref vals i) (cl:not (cl:zerop (cl:read-byte istream))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PersonLocalizationTrackingData>)))
  "Returns string type for a message object of type '<PersonLocalizationTrackingData>"
  "monarch_msgs/PersonLocalizationTrackingData")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PersonLocalizationTrackingData)))
  "Returns string type for a message object of type 'PersonLocalizationTrackingData"
  "monarch_msgs/PersonLocalizationTrackingData")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PersonLocalizationTrackingData>)))
  "Returns md5sum for a message object of type '<PersonLocalizationTrackingData>"
  "7f6cba6f5fae2718b20684bc938786c2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PersonLocalizationTrackingData)))
  "Returns md5sum for a message object of type 'PersonLocalizationTrackingData"
  "7f6cba6f5fae2718b20684bc938786c2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PersonLocalizationTrackingData>)))
  "Returns full string definition for message of type '<PersonLocalizationTrackingData>"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint32 id                              # tracker id~%geometry_msgs/PoseWithCovariance pose  # tracked person estimated pose~%uint32[] groupFellows                  # Who is this person in a group with~%geometry_msgs/Point vel                # Velocity of the person in x,y,z~%~%int32 ACT_WALK=0                       # if isActive[ACT_WALK]==true, the person is detected as walking~%int32 ACT_RUN=1                        # Running~%int32 ACT_CRAFT=2                      # The person is doing handwork at a table~%int32 ACT_TV=3                         # The person is watching TV~%int32 NUM_ACT=4                        # Length of the isActive array. The idea is that code can check whether any activity is~%                                       #    being detected without updating the code as new activities are added to the list.~%bool[4] isActive                       # SIZE MUST BE UPDATED AS NUM_ACT IS UPDATED~%                                       # What activities are discovered for this person ~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PersonLocalizationTrackingData)))
  "Returns full string definition for message of type 'PersonLocalizationTrackingData"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint32 id                              # tracker id~%geometry_msgs/PoseWithCovariance pose  # tracked person estimated pose~%uint32[] groupFellows                  # Who is this person in a group with~%geometry_msgs/Point vel                # Velocity of the person in x,y,z~%~%int32 ACT_WALK=0                       # if isActive[ACT_WALK]==true, the person is detected as walking~%int32 ACT_RUN=1                        # Running~%int32 ACT_CRAFT=2                      # The person is doing handwork at a table~%int32 ACT_TV=3                         # The person is watching TV~%int32 NUM_ACT=4                        # Length of the isActive array. The idea is that code can check whether any activity is~%                                       #    being detected without updating the code as new activities are added to the list.~%bool[4] isActive                       # SIZE MUST BE UPDATED AS NUM_ACT IS UPDATED~%                                       # What activities are discovered for this person ~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PersonLocalizationTrackingData>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'groupFellows) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'vel))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'isActive) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PersonLocalizationTrackingData>))
  "Converts a ROS message object to a list"
  (cl:list 'PersonLocalizationTrackingData
    (cl:cons ':header (header msg))
    (cl:cons ':id (id msg))
    (cl:cons ':pose (pose msg))
    (cl:cons ':groupFellows (groupFellows msg))
    (cl:cons ':vel (vel msg))
    (cl:cons ':isActive (isActive msg))
))
