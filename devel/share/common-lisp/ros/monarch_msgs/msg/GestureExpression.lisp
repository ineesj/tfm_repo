; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GestureExpression.msg.html

(cl:defclass <GestureExpression> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (gesture
    :reader gesture
    :initarg :gesture
    :type cl:string
    :initform "")
   (params
    :reader params
    :initarg :params
    :type monarch_msgs-msg:KeyValuePairArray
    :initform (cl:make-instance 'monarch_msgs-msg:KeyValuePairArray)))
)

(cl:defclass GestureExpression (<GestureExpression>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GestureExpression>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GestureExpression)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GestureExpression> is deprecated: use monarch_msgs-msg:GestureExpression instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GestureExpression>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'gesture-val :lambda-list '(m))
(cl:defmethod gesture-val ((m <GestureExpression>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:gesture-val is deprecated.  Use monarch_msgs-msg:gesture instead.")
  (gesture m))

(cl:ensure-generic-function 'params-val :lambda-list '(m))
(cl:defmethod params-val ((m <GestureExpression>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:params-val is deprecated.  Use monarch_msgs-msg:params instead.")
  (params m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GestureExpression>) ostream)
  "Serializes a message object of type '<GestureExpression>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'gesture))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'gesture))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'params) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GestureExpression>) istream)
  "Deserializes a message object of type '<GestureExpression>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'gesture) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'gesture) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'params) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GestureExpression>)))
  "Returns string type for a message object of type '<GestureExpression>"
  "monarch_msgs/GestureExpression")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GestureExpression)))
  "Returns string type for a message object of type 'GestureExpression"
  "monarch_msgs/GestureExpression")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GestureExpression>)))
  "Returns md5sum for a message object of type '<GestureExpression>"
  "df58bbfaccd06b18fe691b39ca92f3c9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GestureExpression)))
  "Returns md5sum for a message object of type 'GestureExpression"
  "df58bbfaccd06b18fe691b39ca92f3c9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GestureExpression>)))
  "Returns full string definition for message of type '<GestureExpression>"
  (cl:format cl:nil "#use standard header~%Header header~%~%string gesture      # Gesture name.~%~%# Params used in the gesture.~%# Examples of key:value pairs accepted are:~%# \"allowed_interfaces\":\"head|arms|voice\"~%# \"emotion\":\"happy\"  ~%# \"username\":\"Joao\"~%~%monarch_msgs/KeyValuePairArray params~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/KeyValuePairArray~%#use standard header~%Header header~%~%#Array of KeyValuePair data structure~%KeyValuePair[] array~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GestureExpression)))
  "Returns full string definition for message of type 'GestureExpression"
  (cl:format cl:nil "#use standard header~%Header header~%~%string gesture      # Gesture name.~%~%# Params used in the gesture.~%# Examples of key:value pairs accepted are:~%# \"allowed_interfaces\":\"head|arms|voice\"~%# \"emotion\":\"happy\"  ~%# \"username\":\"Joao\"~%~%monarch_msgs/KeyValuePairArray params~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/KeyValuePairArray~%#use standard header~%Header header~%~%#Array of KeyValuePair data structure~%KeyValuePair[] array~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GestureExpression>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'gesture))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'params))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GestureExpression>))
  "Converts a ROS message object to a list"
  (cl:list 'GestureExpression
    (cl:cons ':header (header msg))
    (cl:cons ':gesture (gesture msg))
    (cl:cons ':params (params msg))
))
