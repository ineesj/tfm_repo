(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          DTHETA1-VAL
          DTHETA1
          DTHETA2-VAL
          DTHETA2
          DTHETA3-VAL
          DTHETA3
          DTHETA4-VAL
          DTHETA4
))