; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude RfidReading.msg.html

(cl:defclass <RfidReading> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (tag_id
    :reader tag_id
    :initarg :tag_id
    :type cl:integer
    :initform 0)
   (tag_type
    :reader tag_type
    :initarg :tag_type
    :type cl:string
    :initform "")
   (tag_info
    :reader tag_info
    :initarg :tag_info
    :type cl:string
    :initform "")
   (tag_prob
    :reader tag_prob
    :initarg :tag_prob
    :type cl:float
    :initform 0.0)
   (tag_position
    :reader tag_position
    :initarg :tag_position
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (tag_angle
    :reader tag_angle
    :initarg :tag_angle
    :type cl:float
    :initform 0.0))
)

(cl:defclass RfidReading (<RfidReading>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RfidReading>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RfidReading)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<RfidReading> is deprecated: use monarch_msgs-msg:RfidReading instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'tag_id-val :lambda-list '(m))
(cl:defmethod tag_id-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_id-val is deprecated.  Use monarch_msgs-msg:tag_id instead.")
  (tag_id m))

(cl:ensure-generic-function 'tag_type-val :lambda-list '(m))
(cl:defmethod tag_type-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_type-val is deprecated.  Use monarch_msgs-msg:tag_type instead.")
  (tag_type m))

(cl:ensure-generic-function 'tag_info-val :lambda-list '(m))
(cl:defmethod tag_info-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_info-val is deprecated.  Use monarch_msgs-msg:tag_info instead.")
  (tag_info m))

(cl:ensure-generic-function 'tag_prob-val :lambda-list '(m))
(cl:defmethod tag_prob-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_prob-val is deprecated.  Use monarch_msgs-msg:tag_prob instead.")
  (tag_prob m))

(cl:ensure-generic-function 'tag_position-val :lambda-list '(m))
(cl:defmethod tag_position-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_position-val is deprecated.  Use monarch_msgs-msg:tag_position instead.")
  (tag_position m))

(cl:ensure-generic-function 'tag_angle-val :lambda-list '(m))
(cl:defmethod tag_angle-val ((m <RfidReading>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:tag_angle-val is deprecated.  Use monarch_msgs-msg:tag_angle instead.")
  (tag_angle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RfidReading>) ostream)
  "Serializes a message object of type '<RfidReading>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'tag_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'tag_id)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'tag_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'tag_type))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'tag_info))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'tag_info))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'tag_prob))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'tag_position) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'tag_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RfidReading>) istream)
  "Deserializes a message object of type '<RfidReading>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'tag_id)) (cl:read-byte istream))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'tag_type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'tag_type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'tag_info) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'tag_info) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'tag_prob) (roslisp-utils:decode-double-float-bits bits)))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'tag_position) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'tag_angle) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RfidReading>)))
  "Returns string type for a message object of type '<RfidReading>"
  "monarch_msgs/RfidReading")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RfidReading)))
  "Returns string type for a message object of type 'RfidReading"
  "monarch_msgs/RfidReading")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RfidReading>)))
  "Returns md5sum for a message object of type '<RfidReading>"
  "8c9232cf6b78ab759162df8722b6de5a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RfidReading)))
  "Returns md5sum for a message object of type 'RfidReading"
  "8c9232cf6b78ab759162df8722b6de5a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RfidReading>)))
  "Returns full string definition for message of type '<RfidReading>"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint64  tag_id	     			  #tag id value~%string  tag_type	 			  #type: 'robot'(localization),'person'(detection)~%string  tag_info     		      #tag description~%float64 tag_prob		     	  #probability value~%geometry_msgs/Point tag_position  #tag position estimation~%float64 tag_angle    			  #tag angle, for people detection~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RfidReading)))
  "Returns full string definition for message of type 'RfidReading"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint64  tag_id	     			  #tag id value~%string  tag_type	 			  #type: 'robot'(localization),'person'(detection)~%string  tag_info     		      #tag description~%float64 tag_prob		     	  #probability value~%geometry_msgs/Point tag_position  #tag position estimation~%float64 tag_angle    			  #tag angle, for people detection~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RfidReading>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
     4 (cl:length (cl:slot-value msg 'tag_type))
     4 (cl:length (cl:slot-value msg 'tag_info))
     8
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'tag_position))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RfidReading>))
  "Converts a ROS message object to a list"
  (cl:list 'RfidReading
    (cl:cons ':header (header msg))
    (cl:cons ':tag_id (tag_id msg))
    (cl:cons ':tag_type (tag_type msg))
    (cl:cons ':tag_info (tag_info msg))
    (cl:cons ':tag_prob (tag_prob msg))
    (cl:cons ':tag_position (tag_position msg))
    (cl:cons ':tag_angle (tag_angle msg))
))
