; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude ScreenInterfaceInputConfig.msg.html

(cl:defclass <ScreenInterfaceInputConfig> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (enable_input
    :reader enable_input
    :initarg :enable_input
    :type cl:boolean
    :initform cl:nil)
   (number_buttons
    :reader number_buttons
    :initarg :number_buttons
    :type cl:fixnum
    :initform 0)
   (buttons_config
    :reader buttons_config
    :initarg :buttons_config
    :type (cl:vector monarch_msgs-msg:ScreenButton)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:ScreenButton :initial-element (cl:make-instance 'monarch_msgs-msg:ScreenButton))))
)

(cl:defclass ScreenInterfaceInputConfig (<ScreenInterfaceInputConfig>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ScreenInterfaceInputConfig>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ScreenInterfaceInputConfig)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<ScreenInterfaceInputConfig> is deprecated: use monarch_msgs-msg:ScreenInterfaceInputConfig instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ScreenInterfaceInputConfig>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'enable_input-val :lambda-list '(m))
(cl:defmethod enable_input-val ((m <ScreenInterfaceInputConfig>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:enable_input-val is deprecated.  Use monarch_msgs-msg:enable_input instead.")
  (enable_input m))

(cl:ensure-generic-function 'number_buttons-val :lambda-list '(m))
(cl:defmethod number_buttons-val ((m <ScreenInterfaceInputConfig>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:number_buttons-val is deprecated.  Use monarch_msgs-msg:number_buttons instead.")
  (number_buttons m))

(cl:ensure-generic-function 'buttons_config-val :lambda-list '(m))
(cl:defmethod buttons_config-val ((m <ScreenInterfaceInputConfig>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:buttons_config-val is deprecated.  Use monarch_msgs-msg:buttons_config instead.")
  (buttons_config m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ScreenInterfaceInputConfig>) ostream)
  "Serializes a message object of type '<ScreenInterfaceInputConfig>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'enable_input) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'number_buttons)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'buttons_config))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'buttons_config))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ScreenInterfaceInputConfig>) istream)
  "Deserializes a message object of type '<ScreenInterfaceInputConfig>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'enable_input) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'number_buttons) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'buttons_config) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'buttons_config)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:ScreenButton))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ScreenInterfaceInputConfig>)))
  "Returns string type for a message object of type '<ScreenInterfaceInputConfig>"
  "monarch_msgs/ScreenInterfaceInputConfig")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ScreenInterfaceInputConfig)))
  "Returns string type for a message object of type 'ScreenInterfaceInputConfig"
  "monarch_msgs/ScreenInterfaceInputConfig")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ScreenInterfaceInputConfig>)))
  "Returns md5sum for a message object of type '<ScreenInterfaceInputConfig>"
  "12e10eba74faad7ec8439691bc39f22d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ScreenInterfaceInputConfig)))
  "Returns md5sum for a message object of type 'ScreenInterfaceInputConfig"
  "12e10eba74faad7ec8439691bc39f22d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ScreenInterfaceInputConfig>)))
  "Returns full string definition for message of type '<ScreenInterfaceInputConfig>"
  (cl:format cl:nil "#A message used to configure the screen interface: if the input is ~%#enabled or not, the number of buttons it should display and the~%#configuration of each button.~%~%#use standard header~%Header header~%~%bool enable_input				# false: hide menu; true: is required to show the menu~%~%int16 number_buttons			#currently, only from 1 to 4 supported~%~%ScreenButton[] buttons_config~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/ScreenButton~%~%string button_name	#Can be either text or a picture name. This picture~%					#should be located in the folder /data/icons~%					#of the screens project.~%					#Usage: \"text;ok\"    \"picture;yes_button.png\"~%					#(by default, it takes the text)~%					~%					~%string topic_name	#Topic in which the button should publish when pressed.~%					#The type of message should be std_msgs/String~%~%~%string msg_value	#value of the message that is sent when the button is~%					#pressed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ScreenInterfaceInputConfig)))
  "Returns full string definition for message of type 'ScreenInterfaceInputConfig"
  (cl:format cl:nil "#A message used to configure the screen interface: if the input is ~%#enabled or not, the number of buttons it should display and the~%#configuration of each button.~%~%#use standard header~%Header header~%~%bool enable_input				# false: hide menu; true: is required to show the menu~%~%int16 number_buttons			#currently, only from 1 to 4 supported~%~%ScreenButton[] buttons_config~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/ScreenButton~%~%string button_name	#Can be either text or a picture name. This picture~%					#should be located in the folder /data/icons~%					#of the screens project.~%					#Usage: \"text;ok\"    \"picture;yes_button.png\"~%					#(by default, it takes the text)~%					~%					~%string topic_name	#Topic in which the button should publish when pressed.~%					#The type of message should be std_msgs/String~%~%~%string msg_value	#value of the message that is sent when the button is~%					#pressed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ScreenInterfaceInputConfig>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     2
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'buttons_config) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ScreenInterfaceInputConfig>))
  "Converts a ROS message object to a list"
  (cl:list 'ScreenInterfaceInputConfig
    (cl:cons ':header (header msg))
    (cl:cons ':enable_input (enable_input msg))
    (cl:cons ':number_buttons (number_buttons msg))
    (cl:cons ':buttons_config (buttons_config msg))
))
