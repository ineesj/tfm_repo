; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude InteractionExecutorStatus.msg.html

(cl:defclass <InteractionExecutorStatus> (roslisp-msg-protocol:ros-message)
  ((expression_name
    :reader expression_name
    :initarg :expression_name
    :type cl:string
    :initform "")
   (status
    :reader status
    :initarg :status
    :type cl:string
    :initform ""))
)

(cl:defclass InteractionExecutorStatus (<InteractionExecutorStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <InteractionExecutorStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'InteractionExecutorStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<InteractionExecutorStatus> is deprecated: use monarch_msgs-msg:InteractionExecutorStatus instead.")))

(cl:ensure-generic-function 'expression_name-val :lambda-list '(m))
(cl:defmethod expression_name-val ((m <InteractionExecutorStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:expression_name-val is deprecated.  Use monarch_msgs-msg:expression_name instead.")
  (expression_name m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <InteractionExecutorStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:status-val is deprecated.  Use monarch_msgs-msg:status instead.")
  (status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <InteractionExecutorStatus>) ostream)
  "Serializes a message object of type '<InteractionExecutorStatus>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'expression_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'expression_name))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'status))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'status))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <InteractionExecutorStatus>) istream)
  "Deserializes a message object of type '<InteractionExecutorStatus>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'expression_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'expression_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'status) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<InteractionExecutorStatus>)))
  "Returns string type for a message object of type '<InteractionExecutorStatus>"
  "monarch_msgs/InteractionExecutorStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'InteractionExecutorStatus)))
  "Returns string type for a message object of type 'InteractionExecutorStatus"
  "monarch_msgs/InteractionExecutorStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<InteractionExecutorStatus>)))
  "Returns md5sum for a message object of type '<InteractionExecutorStatus>"
  "f5dff72b84a9a97e8f1cf9d1b4daefdf")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'InteractionExecutorStatus)))
  "Returns md5sum for a message object of type 'InteractionExecutorStatus"
  "f5dff72b84a9a97e8f1cf9d1b4daefdf")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<InteractionExecutorStatus>)))
  "Returns full string definition for message of type '<InteractionExecutorStatus>"
  (cl:format cl:nil "# Interaction Execution Status message~%~%string expression_name	# Eg. \"say_text\", \"express_emotion\", etc.~%string status       	# Status of interaction. Accepted values are \"OK\", \"ERROR\".~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'InteractionExecutorStatus)))
  "Returns full string definition for message of type 'InteractionExecutorStatus"
  (cl:format cl:nil "# Interaction Execution Status message~%~%string expression_name	# Eg. \"say_text\", \"express_emotion\", etc.~%string status       	# Status of interaction. Accepted values are \"OK\", \"ERROR\".~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <InteractionExecutorStatus>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'expression_name))
     4 (cl:length (cl:slot-value msg 'status))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <InteractionExecutorStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'InteractionExecutorStatus
    (cl:cons ':expression_name (expression_name msg))
    (cl:cons ':status (status msg))
))
