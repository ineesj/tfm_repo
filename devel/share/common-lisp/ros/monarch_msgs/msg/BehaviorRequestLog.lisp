; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude BehaviorRequestLog.msg.html

(cl:defclass <BehaviorRequestLog> (roslisp-msg-protocol:ros-message)
  ((behavior
    :reader behavior
    :initarg :behavior
    :type monarch_msgs-msg:BehaviorSelection
    :initform (cl:make-instance 'monarch_msgs-msg:BehaviorSelection))
   (success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass BehaviorRequestLog (<BehaviorRequestLog>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BehaviorRequestLog>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BehaviorRequestLog)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<BehaviorRequestLog> is deprecated: use monarch_msgs-msg:BehaviorRequestLog instead.")))

(cl:ensure-generic-function 'behavior-val :lambda-list '(m))
(cl:defmethod behavior-val ((m <BehaviorRequestLog>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:behavior-val is deprecated.  Use monarch_msgs-msg:behavior instead.")
  (behavior m))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <BehaviorRequestLog>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:success-val is deprecated.  Use monarch_msgs-msg:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BehaviorRequestLog>) ostream)
  "Serializes a message object of type '<BehaviorRequestLog>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'behavior) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BehaviorRequestLog>) istream)
  "Deserializes a message object of type '<BehaviorRequestLog>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'behavior) istream)
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BehaviorRequestLog>)))
  "Returns string type for a message object of type '<BehaviorRequestLog>"
  "monarch_msgs/BehaviorRequestLog")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BehaviorRequestLog)))
  "Returns string type for a message object of type 'BehaviorRequestLog"
  "monarch_msgs/BehaviorRequestLog")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BehaviorRequestLog>)))
  "Returns md5sum for a message object of type '<BehaviorRequestLog>"
  "41a4608217b33ca294e11fa002d82c4e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BehaviorRequestLog)))
  "Returns md5sum for a message object of type 'BehaviorRequestLog"
  "41a4608217b33ca294e11fa002d82c4e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BehaviorRequestLog>)))
  "Returns full string definition for message of type '<BehaviorRequestLog>"
  (cl:format cl:nil "# This message is used solely to log the behavior request made by the SAP to the GBM using the BehaviorRequest.srv service~%monarch_msgs/BehaviorSelection behavior~%bool success~%================================================================================~%MSG: monarch_msgs/BehaviorSelection~%string name                  # ID of behavior, as defined by the constants above~%uint32 instance_id		     # ID of specific instance of the behavior being requested~%int8[] robots                # the numbers here correspond to mbotX. At least one robot should be selected.~%KeyValuePair[] parameters    # parameters needed by the behavior, given as key/value pairs~%string[] resources           # robot resources needed for execution of behavior. Taken from list of Functionalities (see Joyful Warden Technical Script): RNAV,RNAVFC,RArmMotion,...~%bool active		     # indicates the execution/cancelation status of the request behavior (true = behavior to be execute, false = behavior to be canceled)~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BehaviorRequestLog)))
  "Returns full string definition for message of type 'BehaviorRequestLog"
  (cl:format cl:nil "# This message is used solely to log the behavior request made by the SAP to the GBM using the BehaviorRequest.srv service~%monarch_msgs/BehaviorSelection behavior~%bool success~%================================================================================~%MSG: monarch_msgs/BehaviorSelection~%string name                  # ID of behavior, as defined by the constants above~%uint32 instance_id		     # ID of specific instance of the behavior being requested~%int8[] robots                # the numbers here correspond to mbotX. At least one robot should be selected.~%KeyValuePair[] parameters    # parameters needed by the behavior, given as key/value pairs~%string[] resources           # robot resources needed for execution of behavior. Taken from list of Functionalities (see Joyful Warden Technical Script): RNAV,RNAVFC,RArmMotion,...~%bool active		     # indicates the execution/cancelation status of the request behavior (true = behavior to be execute, false = behavior to be canceled)~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BehaviorRequestLog>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'behavior))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BehaviorRequestLog>))
  "Converts a ROS message object to a list"
  (cl:list 'BehaviorRequestLog
    (cl:cons ':behavior (behavior msg))
    (cl:cons ':success (success msg))
))
