(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          SENTENCE-VAL
          SENTENCE
          EMOTION-VAL
          EMOTION
          VOLUME-VAL
          VOLUME
          AGENT-VAL
          AGENT
          PRIORITY-VAL
          PRIORITY
))