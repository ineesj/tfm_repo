; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude HeadControl.msg.html

(cl:defclass <HeadControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (head_pos
    :reader head_pos
    :initarg :head_pos
    :type cl:float
    :initform 0.0)
   (movement_speed
    :reader movement_speed
    :initarg :movement_speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass HeadControl (<HeadControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HeadControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HeadControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<HeadControl> is deprecated: use monarch_msgs-msg:HeadControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <HeadControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'head_pos-val :lambda-list '(m))
(cl:defmethod head_pos-val ((m <HeadControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:head_pos-val is deprecated.  Use monarch_msgs-msg:head_pos instead.")
  (head_pos m))

(cl:ensure-generic-function 'movement_speed-val :lambda-list '(m))
(cl:defmethod movement_speed-val ((m <HeadControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:movement_speed-val is deprecated.  Use monarch_msgs-msg:movement_speed instead.")
  (movement_speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HeadControl>) ostream)
  "Serializes a message object of type '<HeadControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'head_pos))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'movement_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HeadControl>) istream)
  "Deserializes a message object of type '<HeadControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'head_pos) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'movement_speed) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HeadControl>)))
  "Returns string type for a message object of type '<HeadControl>"
  "monarch_msgs/HeadControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HeadControl)))
  "Returns string type for a message object of type 'HeadControl"
  "monarch_msgs/HeadControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HeadControl>)))
  "Returns md5sum for a message object of type '<HeadControl>"
  "52bdb89f04808be47c7378c96b2ea81b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HeadControl)))
  "Returns md5sum for a message object of type 'HeadControl"
  "52bdb89f04808be47c7378c96b2ea81b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HeadControl>)))
  "Returns full string definition for message of type '<HeadControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#values for head position and movement speed~%float32 head_pos          # angle position in radians, where 0 looking front, pi/2 looking left and -pi/2 looking right~%float32 movement_speed    # percentage of the maximum velocity to rotate the head (0-100)~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HeadControl)))
  "Returns full string definition for message of type 'HeadControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%#values for head position and movement speed~%float32 head_pos          # angle position in radians, where 0 looking front, pi/2 looking left and -pi/2 looking right~%float32 movement_speed    # percentage of the maximum velocity to rotate the head (0-100)~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HeadControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HeadControl>))
  "Converts a ROS message object to a list"
  (cl:list 'HeadControl
    (cl:cons ':header (header msg))
    (cl:cons ':head_pos (head_pos msg))
    (cl:cons ':movement_speed (movement_speed msg))
))
