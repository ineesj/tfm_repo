; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude SetMotorBoardCoolingFans.msg.html

(cl:defclass <SetMotorBoardCoolingFans> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (automaticFansControl
    :reader automaticFansControl
    :initarg :automaticFansControl
    :type cl:boolean
    :initform cl:nil)
   (frontMotorsFan
    :reader frontMotorsFan
    :initarg :frontMotorsFan
    :type cl:boolean
    :initform cl:nil)
   (rearMotorsFan
    :reader rearMotorsFan
    :initarg :rearMotorsFan
    :type cl:boolean
    :initform cl:nil)
   (frontDriversFan
    :reader frontDriversFan
    :initarg :frontDriversFan
    :type cl:boolean
    :initform cl:nil)
   (rearDriversFan
    :reader rearDriversFan
    :initarg :rearDriversFan
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass SetMotorBoardCoolingFans (<SetMotorBoardCoolingFans>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetMotorBoardCoolingFans>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetMotorBoardCoolingFans)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<SetMotorBoardCoolingFans> is deprecated: use monarch_msgs-msg:SetMotorBoardCoolingFans instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'automaticFansControl-val :lambda-list '(m))
(cl:defmethod automaticFansControl-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:automaticFansControl-val is deprecated.  Use monarch_msgs-msg:automaticFansControl instead.")
  (automaticFansControl m))

(cl:ensure-generic-function 'frontMotorsFan-val :lambda-list '(m))
(cl:defmethod frontMotorsFan-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:frontMotorsFan-val is deprecated.  Use monarch_msgs-msg:frontMotorsFan instead.")
  (frontMotorsFan m))

(cl:ensure-generic-function 'rearMotorsFan-val :lambda-list '(m))
(cl:defmethod rearMotorsFan-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rearMotorsFan-val is deprecated.  Use monarch_msgs-msg:rearMotorsFan instead.")
  (rearMotorsFan m))

(cl:ensure-generic-function 'frontDriversFan-val :lambda-list '(m))
(cl:defmethod frontDriversFan-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:frontDriversFan-val is deprecated.  Use monarch_msgs-msg:frontDriversFan instead.")
  (frontDriversFan m))

(cl:ensure-generic-function 'rearDriversFan-val :lambda-list '(m))
(cl:defmethod rearDriversFan-val ((m <SetMotorBoardCoolingFans>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rearDriversFan-val is deprecated.  Use monarch_msgs-msg:rearDriversFan instead.")
  (rearDriversFan m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetMotorBoardCoolingFans>) ostream)
  "Serializes a message object of type '<SetMotorBoardCoolingFans>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'automaticFansControl) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'frontMotorsFan) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rearMotorsFan) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'frontDriversFan) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rearDriversFan) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetMotorBoardCoolingFans>) istream)
  "Deserializes a message object of type '<SetMotorBoardCoolingFans>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'automaticFansControl) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'frontMotorsFan) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rearMotorsFan) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'frontDriversFan) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rearDriversFan) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetMotorBoardCoolingFans>)))
  "Returns string type for a message object of type '<SetMotorBoardCoolingFans>"
  "monarch_msgs/SetMotorBoardCoolingFans")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetMotorBoardCoolingFans)))
  "Returns string type for a message object of type 'SetMotorBoardCoolingFans"
  "monarch_msgs/SetMotorBoardCoolingFans")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetMotorBoardCoolingFans>)))
  "Returns md5sum for a message object of type '<SetMotorBoardCoolingFans>"
  "15498ac12db735d7a52e9a85f2af5ee1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetMotorBoardCoolingFans)))
  "Returns md5sum for a message object of type 'SetMotorBoardCoolingFans"
  "15498ac12db735d7a52e9a85f2af5ee1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetMotorBoardCoolingFans>)))
  "Returns full string definition for message of type '<SetMotorBoardCoolingFans>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each fan and for automatic control. True means turn correspondent fan ON and turn automatic fans control ON.~%bool automaticFansControl~%bool frontMotorsFan~%bool rearMotorsFan~%bool frontDriversFan~%bool rearDriversFan~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetMotorBoardCoolingFans)))
  "Returns full string definition for message of type 'SetMotorBoardCoolingFans"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each fan and for automatic control. True means turn correspondent fan ON and turn automatic fans control ON.~%bool automaticFansControl~%bool frontMotorsFan~%bool rearMotorsFan~%bool frontDriversFan~%bool rearDriversFan~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetMotorBoardCoolingFans>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetMotorBoardCoolingFans>))
  "Converts a ROS message object to a list"
  (cl:list 'SetMotorBoardCoolingFans
    (cl:cons ':header (header msg))
    (cl:cons ':automaticFansControl (automaticFansControl msg))
    (cl:cons ':frontMotorsFan (frontMotorsFan msg))
    (cl:cons ':rearMotorsFan (rearMotorsFan msg))
    (cl:cons ':frontDriversFan (frontDriversFan msg))
    (cl:cons ':rearDriversFan (rearDriversFan msg))
))
