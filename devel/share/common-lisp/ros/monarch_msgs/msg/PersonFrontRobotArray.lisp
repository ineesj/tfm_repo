; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude PersonFrontRobotArray.msg.html

(cl:defclass <PersonFrontRobotArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (persons
    :reader persons
    :initarg :persons
    :type (cl:vector monarch_msgs-msg:PersonFrontofRobot)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:PersonFrontofRobot :initial-element (cl:make-instance 'monarch_msgs-msg:PersonFrontofRobot))))
)

(cl:defclass PersonFrontRobotArray (<PersonFrontRobotArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PersonFrontRobotArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PersonFrontRobotArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<PersonFrontRobotArray> is deprecated: use monarch_msgs-msg:PersonFrontRobotArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PersonFrontRobotArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'persons-val :lambda-list '(m))
(cl:defmethod persons-val ((m <PersonFrontRobotArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:persons-val is deprecated.  Use monarch_msgs-msg:persons instead.")
  (persons m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PersonFrontRobotArray>) ostream)
  "Serializes a message object of type '<PersonFrontRobotArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'persons))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'persons))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PersonFrontRobotArray>) istream)
  "Deserializes a message object of type '<PersonFrontRobotArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'persons) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'persons)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:PersonFrontofRobot))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PersonFrontRobotArray>)))
  "Returns string type for a message object of type '<PersonFrontRobotArray>"
  "monarch_msgs/PersonFrontRobotArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PersonFrontRobotArray)))
  "Returns string type for a message object of type 'PersonFrontRobotArray"
  "monarch_msgs/PersonFrontRobotArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PersonFrontRobotArray>)))
  "Returns md5sum for a message object of type '<PersonFrontRobotArray>"
  "a5868042ebb9eb3489b91f01ff2bef75")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PersonFrontRobotArray)))
  "Returns md5sum for a message object of type 'PersonFrontRobotArray"
  "a5868042ebb9eb3489b91f01ff2bef75")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PersonFrontRobotArray>)))
  "Returns full string definition for message of type '<PersonFrontRobotArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%PersonFrontofRobot[] persons~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonFrontofRobot~%#use standard header~%Header header~%~%int32 id                     # id of person in front of robot. For now, this is not linked to the ids assigned by the overhead cameras~%float32 person_orientation    # relative orientation of the person with respect to robot (0 degrees if person looking at robot, 180 degrees if facing away)~%float32 direction_to_person   # relative orientation of the person with respect to robot (0 degrees if straight in front of the head of the robot).~%float32 distance              # relative distance from the robot to the person~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PersonFrontRobotArray)))
  "Returns full string definition for message of type 'PersonFrontRobotArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%PersonFrontofRobot[] persons~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonFrontofRobot~%#use standard header~%Header header~%~%int32 id                     # id of person in front of robot. For now, this is not linked to the ids assigned by the overhead cameras~%float32 person_orientation    # relative orientation of the person with respect to robot (0 degrees if person looking at robot, 180 degrees if facing away)~%float32 direction_to_person   # relative orientation of the person with respect to robot (0 degrees if straight in front of the head of the robot).~%float32 distance              # relative distance from the robot to the person~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PersonFrontRobotArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'persons) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PersonFrontRobotArray>))
  "Converts a ROS message object to a list"
  (cl:list 'PersonFrontRobotArray
    (cl:cons ':header (header msg))
    (cl:cons ':persons (persons msg))
))
