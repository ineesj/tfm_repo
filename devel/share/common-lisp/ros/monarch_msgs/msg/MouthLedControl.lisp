; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude MouthLedControl.msg.html

(cl:defclass <MouthLedControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (data
    :reader data
    :initarg :data
    :type (cl:vector cl:boolean)
   :initform (cl:make-array 0 :element-type 'cl:boolean :initial-element cl:nil)))
)

(cl:defclass MouthLedControl (<MouthLedControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MouthLedControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MouthLedControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<MouthLedControl> is deprecated: use monarch_msgs-msg:MouthLedControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MouthLedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <MouthLedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:data-val is deprecated.  Use monarch_msgs-msg:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MouthLedControl>) ostream)
  "Serializes a message object of type '<MouthLedControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if ele 1 0)) ostream))
   (cl:slot-value msg 'data))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MouthLedControl>) istream)
  "Deserializes a message object of type '<MouthLedControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'data) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'data)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:not (cl:zerop (cl:read-byte istream)))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MouthLedControl>)))
  "Returns string type for a message object of type '<MouthLedControl>"
  "monarch_msgs/MouthLedControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MouthLedControl)))
  "Returns string type for a message object of type 'MouthLedControl"
  "monarch_msgs/MouthLedControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MouthLedControl>)))
  "Returns md5sum for a message object of type '<MouthLedControl>"
  "7475d0bdd7188f9acd60d6e96b16acfb")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MouthLedControl)))
  "Returns md5sum for a message object of type 'MouthLedControl"
  "7475d0bdd7188f9acd60d6e96b16acfb")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MouthLedControl>)))
  "Returns full string definition for message of type '<MouthLedControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool[] data #bool array for mouth led control, the first bool is on the top-Left corner.~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MouthLedControl)))
  "Returns full string definition for message of type 'MouthLedControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool[] data #bool array for mouth led control, the first bool is on the top-Left corner.~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MouthLedControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'data) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MouthLedControl>))
  "Converts a ROS message object to a list"
  (cl:list 'MouthLedControl
    (cl:cons ':header (header msg))
    (cl:cons ':data (data msg))
))
