; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GamePlayerStatus.msg.html

(cl:defclass <GamePlayerStatus> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (playerName
    :reader playerName
    :initarg :playerName
    :type cl:string
    :initform "")
   (turnStatus
    :reader turnStatus
    :initarg :turnStatus
    :type cl:string
    :initform ""))
)

(cl:defclass GamePlayerStatus (<GamePlayerStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GamePlayerStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GamePlayerStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GamePlayerStatus> is deprecated: use monarch_msgs-msg:GamePlayerStatus instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GamePlayerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'playerName-val :lambda-list '(m))
(cl:defmethod playerName-val ((m <GamePlayerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:playerName-val is deprecated.  Use monarch_msgs-msg:playerName instead.")
  (playerName m))

(cl:ensure-generic-function 'turnStatus-val :lambda-list '(m))
(cl:defmethod turnStatus-val ((m <GamePlayerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:turnStatus-val is deprecated.  Use monarch_msgs-msg:turnStatus instead.")
  (turnStatus m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GamePlayerStatus>) ostream)
  "Serializes a message object of type '<GamePlayerStatus>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'playerName))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'playerName))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'turnStatus))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'turnStatus))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GamePlayerStatus>) istream)
  "Deserializes a message object of type '<GamePlayerStatus>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'playerName) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'playerName) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'turnStatus) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'turnStatus) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GamePlayerStatus>)))
  "Returns string type for a message object of type '<GamePlayerStatus>"
  "monarch_msgs/GamePlayerStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GamePlayerStatus)))
  "Returns string type for a message object of type 'GamePlayerStatus"
  "monarch_msgs/GamePlayerStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GamePlayerStatus>)))
  "Returns md5sum for a message object of type '<GamePlayerStatus>"
  "15b471e2c7ba75c4c1b0ad5c818953a1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GamePlayerStatus)))
  "Returns md5sum for a message object of type 'GamePlayerStatus"
  "15b471e2c7ba75c4c1b0ad5c818953a1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GamePlayerStatus>)))
  "Returns full string definition for message of type '<GamePlayerStatus>"
  (cl:format cl:nil "#use standard header~%Header header~%~%string playerName #name of the player~%string turnStatus #name of the player~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GamePlayerStatus)))
  "Returns full string definition for message of type 'GamePlayerStatus"
  (cl:format cl:nil "#use standard header~%Header header~%~%string playerName #name of the player~%string turnStatus #name of the player~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GamePlayerStatus>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'playerName))
     4 (cl:length (cl:slot-value msg 'turnStatus))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GamePlayerStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'GamePlayerStatus
    (cl:cons ':header (header msg))
    (cl:cons ':playerName (playerName msg))
    (cl:cons ':turnStatus (turnStatus msg))
))
