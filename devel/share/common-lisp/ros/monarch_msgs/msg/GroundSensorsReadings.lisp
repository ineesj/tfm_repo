; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GroundSensorsReadings.msg.html

(cl:defclass <GroundSensorsReadings> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (leftFront
    :reader leftFront
    :initarg :leftFront
    :type cl:fixnum
    :initform 0)
   (rightFront
    :reader rightFront
    :initarg :rightFront
    :type cl:fixnum
    :initform 0)
   (left
    :reader left
    :initarg :left
    :type cl:fixnum
    :initform 0)
   (right
    :reader right
    :initarg :right
    :type cl:fixnum
    :initform 0))
)

(cl:defclass GroundSensorsReadings (<GroundSensorsReadings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GroundSensorsReadings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GroundSensorsReadings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GroundSensorsReadings> is deprecated: use monarch_msgs-msg:GroundSensorsReadings instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GroundSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'leftFront-val :lambda-list '(m))
(cl:defmethod leftFront-val ((m <GroundSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFront-val is deprecated.  Use monarch_msgs-msg:leftFront instead.")
  (leftFront m))

(cl:ensure-generic-function 'rightFront-val :lambda-list '(m))
(cl:defmethod rightFront-val ((m <GroundSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFront-val is deprecated.  Use monarch_msgs-msg:rightFront instead.")
  (rightFront m))

(cl:ensure-generic-function 'left-val :lambda-list '(m))
(cl:defmethod left-val ((m <GroundSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:left-val is deprecated.  Use monarch_msgs-msg:left instead.")
  (left m))

(cl:ensure-generic-function 'right-val :lambda-list '(m))
(cl:defmethod right-val ((m <GroundSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:right-val is deprecated.  Use monarch_msgs-msg:right instead.")
  (right m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GroundSensorsReadings>) ostream)
  "Serializes a message object of type '<GroundSensorsReadings>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFront)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFront)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'left)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'right)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GroundSensorsReadings>) istream)
  "Deserializes a message object of type '<GroundSensorsReadings>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFront)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFront)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'left)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'right)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GroundSensorsReadings>)))
  "Returns string type for a message object of type '<GroundSensorsReadings>"
  "monarch_msgs/GroundSensorsReadings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GroundSensorsReadings)))
  "Returns string type for a message object of type 'GroundSensorsReadings"
  "monarch_msgs/GroundSensorsReadings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GroundSensorsReadings>)))
  "Returns md5sum for a message object of type '<GroundSensorsReadings>"
  "8fdf83b0b701a232e812c1ed5ada8b31")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GroundSensorsReadings)))
  "Returns md5sum for a message object of type 'GroundSensorsReadings"
  "8fdf83b0b701a232e812c1ed5ada8b31")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GroundSensorsReadings>)))
  "Returns full string definition for message of type '<GroundSensorsReadings>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One byte for each sensor. 0 means no reflection, 255 means full reflection.~%uint8 leftFront~%uint8 rightFront~%uint8 left~%uint8 right~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GroundSensorsReadings)))
  "Returns full string definition for message of type 'GroundSensorsReadings"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One byte for each sensor. 0 means no reflection, 255 means full reflection.~%uint8 leftFront~%uint8 rightFront~%uint8 left~%uint8 right~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GroundSensorsReadings>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GroundSensorsReadings>))
  "Converts a ROS message object to a list"
  (cl:list 'GroundSensorsReadings
    (cl:cons ':header (header msg))
    (cl:cons ':leftFront (leftFront msg))
    (cl:cons ':rightFront (rightFront msg))
    (cl:cons ':left (left msg))
    (cl:cons ':right (right msg))
))
