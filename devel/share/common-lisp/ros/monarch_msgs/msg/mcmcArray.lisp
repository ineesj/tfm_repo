; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude mcmcArray.msg.html

(cl:defclass <mcmcArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (mcmcArray
    :reader mcmcArray
    :initarg :mcmcArray
    :type (cl:vector monarch_msgs-msg:mcmc)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:mcmc :initial-element (cl:make-instance 'monarch_msgs-msg:mcmc))))
)

(cl:defclass mcmcArray (<mcmcArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <mcmcArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'mcmcArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<mcmcArray> is deprecated: use monarch_msgs-msg:mcmcArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <mcmcArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'mcmcArray-val :lambda-list '(m))
(cl:defmethod mcmcArray-val ((m <mcmcArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mcmcArray-val is deprecated.  Use monarch_msgs-msg:mcmcArray instead.")
  (mcmcArray m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <mcmcArray>) ostream)
  "Serializes a message object of type '<mcmcArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'mcmcArray))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'mcmcArray))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <mcmcArray>) istream)
  "Deserializes a message object of type '<mcmcArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'mcmcArray) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'mcmcArray)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:mcmc))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<mcmcArray>)))
  "Returns string type for a message object of type '<mcmcArray>"
  "monarch_msgs/mcmcArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'mcmcArray)))
  "Returns string type for a message object of type 'mcmcArray"
  "monarch_msgs/mcmcArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<mcmcArray>)))
  "Returns md5sum for a message object of type '<mcmcArray>"
  "f9808555818e57671a5a5ff6ef993d00")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'mcmcArray)))
  "Returns md5sum for a message object of type 'mcmcArray"
  "f9808555818e57671a5a5ff6ef993d00")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<mcmcArray>)))
  "Returns full string definition for message of type '<mcmcArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of mcmc data structure~%mcmc[] mcmcArray~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/mcmc~%#use standard header~%Header header~%~%geometry_msgs/PoseStamped mcmc  # tracked person estimated pose~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'mcmcArray)))
  "Returns full string definition for message of type 'mcmcArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of mcmc data structure~%mcmc[] mcmcArray~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/mcmc~%#use standard header~%Header header~%~%geometry_msgs/PoseStamped mcmc  # tracked person estimated pose~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <mcmcArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'mcmcArray) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <mcmcArray>))
  "Converts a ROS message object to a list"
  (cl:list 'mcmcArray
    (cl:cons ':header (header msg))
    (cl:cons ':mcmcArray (mcmcArray msg))
))
