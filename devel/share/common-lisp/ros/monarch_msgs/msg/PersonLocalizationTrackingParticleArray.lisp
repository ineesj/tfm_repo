; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude PersonLocalizationTrackingParticleArray.msg.html

(cl:defclass <PersonLocalizationTrackingParticleArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (particles
    :reader particles
    :initarg :particles
    :type (cl:vector monarch_msgs-msg:PersonLocalizationTrackingParticle)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:PersonLocalizationTrackingParticle :initial-element (cl:make-instance 'monarch_msgs-msg:PersonLocalizationTrackingParticle))))
)

(cl:defclass PersonLocalizationTrackingParticleArray (<PersonLocalizationTrackingParticleArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PersonLocalizationTrackingParticleArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PersonLocalizationTrackingParticleArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<PersonLocalizationTrackingParticleArray> is deprecated: use monarch_msgs-msg:PersonLocalizationTrackingParticleArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PersonLocalizationTrackingParticleArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'particles-val :lambda-list '(m))
(cl:defmethod particles-val ((m <PersonLocalizationTrackingParticleArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:particles-val is deprecated.  Use monarch_msgs-msg:particles instead.")
  (particles m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PersonLocalizationTrackingParticleArray>) ostream)
  "Serializes a message object of type '<PersonLocalizationTrackingParticleArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'particles))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'particles))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PersonLocalizationTrackingParticleArray>) istream)
  "Deserializes a message object of type '<PersonLocalizationTrackingParticleArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'particles) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'particles)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:PersonLocalizationTrackingParticle))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PersonLocalizationTrackingParticleArray>)))
  "Returns string type for a message object of type '<PersonLocalizationTrackingParticleArray>"
  "monarch_msgs/PersonLocalizationTrackingParticleArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PersonLocalizationTrackingParticleArray)))
  "Returns string type for a message object of type 'PersonLocalizationTrackingParticleArray"
  "monarch_msgs/PersonLocalizationTrackingParticleArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PersonLocalizationTrackingParticleArray>)))
  "Returns md5sum for a message object of type '<PersonLocalizationTrackingParticleArray>"
  "4fcc89b901b1ffb973826d5ae1e428f7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PersonLocalizationTrackingParticleArray)))
  "Returns md5sum for a message object of type 'PersonLocalizationTrackingParticleArray"
  "4fcc89b901b1ffb973826d5ae1e428f7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PersonLocalizationTrackingParticleArray>)))
  "Returns full string definition for message of type '<PersonLocalizationTrackingParticleArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of PersonLocalizationTrackingParticle data structure~%PersonLocalizationTrackingParticle[] particles~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonLocalizationTrackingParticle~%#use standard header~%Header header~%~%geometry_msgs/PoseWithCovariance pose  # particle's estimated pose~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PersonLocalizationTrackingParticleArray)))
  "Returns full string definition for message of type 'PersonLocalizationTrackingParticleArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of PersonLocalizationTrackingParticle data structure~%PersonLocalizationTrackingParticle[] particles~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonLocalizationTrackingParticle~%#use standard header~%Header header~%~%geometry_msgs/PoseWithCovariance pose  # particle's estimated pose~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PersonLocalizationTrackingParticleArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'particles) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PersonLocalizationTrackingParticleArray>))
  "Converts a ROS message object to a list"
  (cl:list 'PersonLocalizationTrackingParticleArray
    (cl:cons ':header (header msg))
    (cl:cons ':particles (particles msg))
))
