(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          SOUNDNAME-VAL
          SOUNDNAME
          ACTION-VAL
          ACTION
          VOLUME-VAL
          VOLUME
          REPETITIONS-VAL
          REPETITIONS
          FADEIN-VAL
          FADEIN
          PITCH-VAL
          PITCH
          DURATION-VAL
          DURATION
))