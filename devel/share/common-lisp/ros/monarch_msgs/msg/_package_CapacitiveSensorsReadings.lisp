(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          HEAD-VAL
          HEAD
          LEFT_SHOULDER-VAL
          LEFT_SHOULDER
          RIGHT_SHOULDER-VAL
          RIGHT_SHOULDER
          LEFT_ARM-VAL
          LEFT_ARM
          RIGHT_ARM-VAL
          RIGHT_ARM
))