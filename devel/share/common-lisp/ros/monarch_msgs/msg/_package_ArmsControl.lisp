(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          TORQUE_ENABLE-VAL
          TORQUE_ENABLE
          LEFT_ARM-VAL
          LEFT_ARM
          RIGHT_ARM-VAL
          RIGHT_ARM
          MOVEMENT_TIME-VAL
          MOVEMENT_TIME
))