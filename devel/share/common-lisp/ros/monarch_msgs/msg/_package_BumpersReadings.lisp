(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          LEFTFRONTBUMP-VAL
          LEFTFRONTBUMP
          RIGHTFRONTBUMP-VAL
          RIGHTFRONTBUMP
          LEFTREARBUMP-VAL
          LEFTREARBUMP
          RIGHTREARBUMP-VAL
          RIGHTREARBUMP
))