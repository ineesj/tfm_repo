; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude AuxiliaryBatteriesVoltage.msg.html

(cl:defclass <AuxiliaryBatteriesVoltage> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (pc1
    :reader pc1
    :initarg :pc1
    :type cl:float
    :initform 0.0)
   (pc2
    :reader pc2
    :initarg :pc2
    :type cl:float
    :initform 0.0))
)

(cl:defclass AuxiliaryBatteriesVoltage (<AuxiliaryBatteriesVoltage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AuxiliaryBatteriesVoltage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AuxiliaryBatteriesVoltage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<AuxiliaryBatteriesVoltage> is deprecated: use monarch_msgs-msg:AuxiliaryBatteriesVoltage instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <AuxiliaryBatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'pc1-val :lambda-list '(m))
(cl:defmethod pc1-val ((m <AuxiliaryBatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:pc1-val is deprecated.  Use monarch_msgs-msg:pc1 instead.")
  (pc1 m))

(cl:ensure-generic-function 'pc2-val :lambda-list '(m))
(cl:defmethod pc2-val ((m <AuxiliaryBatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:pc2-val is deprecated.  Use monarch_msgs-msg:pc2 instead.")
  (pc2 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AuxiliaryBatteriesVoltage>) ostream)
  "Serializes a message object of type '<AuxiliaryBatteriesVoltage>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pc1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pc2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AuxiliaryBatteriesVoltage>) istream)
  "Deserializes a message object of type '<AuxiliaryBatteriesVoltage>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pc1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pc2) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AuxiliaryBatteriesVoltage>)))
  "Returns string type for a message object of type '<AuxiliaryBatteriesVoltage>"
  "monarch_msgs/AuxiliaryBatteriesVoltage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AuxiliaryBatteriesVoltage)))
  "Returns string type for a message object of type 'AuxiliaryBatteriesVoltage"
  "monarch_msgs/AuxiliaryBatteriesVoltage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AuxiliaryBatteriesVoltage>)))
  "Returns md5sum for a message object of type '<AuxiliaryBatteriesVoltage>"
  "8a7187885b1735f892e9a1ae538d4c3a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AuxiliaryBatteriesVoltage)))
  "Returns md5sum for a message object of type 'AuxiliaryBatteriesVoltage"
  "8a7187885b1735f892e9a1ae538d4c3a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AuxiliaryBatteriesVoltage>)))
  "Returns full string definition for message of type '<AuxiliaryBatteriesVoltage>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltage of PC1 and PC2. Measured in Volts.~%float32 pc1~%float32 pc2~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AuxiliaryBatteriesVoltage)))
  "Returns full string definition for message of type 'AuxiliaryBatteriesVoltage"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltage of PC1 and PC2. Measured in Volts.~%float32 pc1~%float32 pc2~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AuxiliaryBatteriesVoltage>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AuxiliaryBatteriesVoltage>))
  "Converts a ROS message object to a list"
  (cl:list 'AuxiliaryBatteriesVoltage
    (cl:cons ':header (header msg))
    (cl:cons ':pc1 (pc1 msg))
    (cl:cons ':pc2 (pc2 msg))
))
