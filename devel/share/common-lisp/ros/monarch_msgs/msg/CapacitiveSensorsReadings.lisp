; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude CapacitiveSensorsReadings.msg.html

(cl:defclass <CapacitiveSensorsReadings> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (head
    :reader head
    :initarg :head
    :type cl:boolean
    :initform cl:nil)
   (left_shoulder
    :reader left_shoulder
    :initarg :left_shoulder
    :type cl:boolean
    :initform cl:nil)
   (right_shoulder
    :reader right_shoulder
    :initarg :right_shoulder
    :type cl:boolean
    :initform cl:nil)
   (left_arm
    :reader left_arm
    :initarg :left_arm
    :type cl:boolean
    :initform cl:nil)
   (right_arm
    :reader right_arm
    :initarg :right_arm
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass CapacitiveSensorsReadings (<CapacitiveSensorsReadings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CapacitiveSensorsReadings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CapacitiveSensorsReadings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<CapacitiveSensorsReadings> is deprecated: use monarch_msgs-msg:CapacitiveSensorsReadings instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'head-val :lambda-list '(m))
(cl:defmethod head-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:head-val is deprecated.  Use monarch_msgs-msg:head instead.")
  (head m))

(cl:ensure-generic-function 'left_shoulder-val :lambda-list '(m))
(cl:defmethod left_shoulder-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:left_shoulder-val is deprecated.  Use monarch_msgs-msg:left_shoulder instead.")
  (left_shoulder m))

(cl:ensure-generic-function 'right_shoulder-val :lambda-list '(m))
(cl:defmethod right_shoulder-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:right_shoulder-val is deprecated.  Use monarch_msgs-msg:right_shoulder instead.")
  (right_shoulder m))

(cl:ensure-generic-function 'left_arm-val :lambda-list '(m))
(cl:defmethod left_arm-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:left_arm-val is deprecated.  Use monarch_msgs-msg:left_arm instead.")
  (left_arm m))

(cl:ensure-generic-function 'right_arm-val :lambda-list '(m))
(cl:defmethod right_arm-val ((m <CapacitiveSensorsReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:right_arm-val is deprecated.  Use monarch_msgs-msg:right_arm instead.")
  (right_arm m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CapacitiveSensorsReadings>) ostream)
  "Serializes a message object of type '<CapacitiveSensorsReadings>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'head) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'left_shoulder) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'right_shoulder) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'left_arm) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'right_arm) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CapacitiveSensorsReadings>) istream)
  "Deserializes a message object of type '<CapacitiveSensorsReadings>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'head) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'left_shoulder) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'right_shoulder) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'left_arm) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'right_arm) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CapacitiveSensorsReadings>)))
  "Returns string type for a message object of type '<CapacitiveSensorsReadings>"
  "monarch_msgs/CapacitiveSensorsReadings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CapacitiveSensorsReadings)))
  "Returns string type for a message object of type 'CapacitiveSensorsReadings"
  "monarch_msgs/CapacitiveSensorsReadings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CapacitiveSensorsReadings>)))
  "Returns md5sum for a message object of type '<CapacitiveSensorsReadings>"
  "07dd804bb9f9052f138ed4f3b2494486")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CapacitiveSensorsReadings)))
  "Returns md5sum for a message object of type 'CapacitiveSensorsReadings"
  "07dd804bb9f9052f138ed4f3b2494486")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CapacitiveSensorsReadings>)))
  "Returns full string definition for message of type '<CapacitiveSensorsReadings>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each capacitive sensor. True means corresponding sensor is active~%bool head~%bool left_shoulder~%bool right_shoulder~%bool left_arm~%bool right_arm~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CapacitiveSensorsReadings)))
  "Returns full string definition for message of type 'CapacitiveSensorsReadings"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each capacitive sensor. True means corresponding sensor is active~%bool head~%bool left_shoulder~%bool right_shoulder~%bool left_arm~%bool right_arm~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CapacitiveSensorsReadings>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CapacitiveSensorsReadings>))
  "Converts a ROS message object to a list"
  (cl:list 'CapacitiveSensorsReadings
    (cl:cons ':header (header msg))
    (cl:cons ':head (head msg))
    (cl:cons ':left_shoulder (left_shoulder msg))
    (cl:cons ':right_shoulder (right_shoulder msg))
    (cl:cons ':left_arm (left_arm msg))
    (cl:cons ':right_arm (right_arm msg))
))
