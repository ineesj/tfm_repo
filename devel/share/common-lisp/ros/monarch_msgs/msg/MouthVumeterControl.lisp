; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude MouthVumeterControl.msg.html

(cl:defclass <MouthVumeterControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (mouthVumeter
    :reader mouthVumeter
    :initarg :mouthVumeter
    :type cl:boolean
    :initform cl:nil)
   (mouthStyle
    :reader mouthStyle
    :initarg :mouthStyle
    :type cl:string
    :initform ""))
)

(cl:defclass MouthVumeterControl (<MouthVumeterControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MouthVumeterControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MouthVumeterControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<MouthVumeterControl> is deprecated: use monarch_msgs-msg:MouthVumeterControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MouthVumeterControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'mouthVumeter-val :lambda-list '(m))
(cl:defmethod mouthVumeter-val ((m <MouthVumeterControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mouthVumeter-val is deprecated.  Use monarch_msgs-msg:mouthVumeter instead.")
  (mouthVumeter m))

(cl:ensure-generic-function 'mouthStyle-val :lambda-list '(m))
(cl:defmethod mouthStyle-val ((m <MouthVumeterControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mouthStyle-val is deprecated.  Use monarch_msgs-msg:mouthStyle instead.")
  (mouthStyle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MouthVumeterControl>) ostream)
  "Serializes a message object of type '<MouthVumeterControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'mouthVumeter) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'mouthStyle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'mouthStyle))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MouthVumeterControl>) istream)
  "Deserializes a message object of type '<MouthVumeterControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'mouthVumeter) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mouthStyle) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'mouthStyle) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MouthVumeterControl>)))
  "Returns string type for a message object of type '<MouthVumeterControl>"
  "monarch_msgs/MouthVumeterControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MouthVumeterControl)))
  "Returns string type for a message object of type 'MouthVumeterControl"
  "monarch_msgs/MouthVumeterControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MouthVumeterControl>)))
  "Returns md5sum for a message object of type '<MouthVumeterControl>"
  "d0221057124ff84a867e99b356c55a1e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MouthVumeterControl)))
  "Returns md5sum for a message object of type 'MouthVumeterControl"
  "d0221057124ff84a867e99b356c55a1e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MouthVumeterControl>)))
  "Returns full string definition for message of type '<MouthVumeterControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool mouthVumeter #One bool for Mouth Vumeter. True means mouth vumeter is enabled~%string mouthStyle #Style of mouthVumeter. Accepted values are the names of catkin_ws/src/hri_interfaces/mouth/data mouth style folders.~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MouthVumeterControl)))
  "Returns full string definition for message of type 'MouthVumeterControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool mouthVumeter #One bool for Mouth Vumeter. True means mouth vumeter is enabled~%string mouthStyle #Style of mouthVumeter. Accepted values are the names of catkin_ws/src/hri_interfaces/mouth/data mouth style folders.~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MouthVumeterControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     4 (cl:length (cl:slot-value msg 'mouthStyle))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MouthVumeterControl>))
  "Converts a ROS message object to a list"
  (cl:list 'MouthVumeterControl
    (cl:cons ':header (header msg))
    (cl:cons ':mouthVumeter (mouthVumeter msg))
    (cl:cons ':mouthStyle (mouthStyle msg))
))
