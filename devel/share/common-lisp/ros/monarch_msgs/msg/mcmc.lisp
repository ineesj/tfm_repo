; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude mcmc.msg.html

(cl:defclass <mcmc> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (mcmc
    :reader mcmc
    :initarg :mcmc
    :type geometry_msgs-msg:PoseStamped
    :initform (cl:make-instance 'geometry_msgs-msg:PoseStamped)))
)

(cl:defclass mcmc (<mcmc>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <mcmc>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'mcmc)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<mcmc> is deprecated: use monarch_msgs-msg:mcmc instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <mcmc>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'mcmc-val :lambda-list '(m))
(cl:defmethod mcmc-val ((m <mcmc>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mcmc-val is deprecated.  Use monarch_msgs-msg:mcmc instead.")
  (mcmc m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <mcmc>) ostream)
  "Serializes a message object of type '<mcmc>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'mcmc) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <mcmc>) istream)
  "Deserializes a message object of type '<mcmc>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'mcmc) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<mcmc>)))
  "Returns string type for a message object of type '<mcmc>"
  "monarch_msgs/mcmc")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'mcmc)))
  "Returns string type for a message object of type 'mcmc"
  "monarch_msgs/mcmc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<mcmc>)))
  "Returns md5sum for a message object of type '<mcmc>"
  "433c3dd12b5ff8d3af83e662d0dbf665")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'mcmc)))
  "Returns md5sum for a message object of type 'mcmc"
  "433c3dd12b5ff8d3af83e662d0dbf665")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<mcmc>)))
  "Returns full string definition for message of type '<mcmc>"
  (cl:format cl:nil "#use standard header~%Header header~%~%geometry_msgs/PoseStamped mcmc  # tracked person estimated pose~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'mcmc)))
  "Returns full string definition for message of type 'mcmc"
  (cl:format cl:nil "#use standard header~%Header header~%~%geometry_msgs/PoseStamped mcmc  # tracked person estimated pose~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <mcmc>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'mcmc))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <mcmc>))
  "Converts a ROS message object to a list"
  (cl:list 'mcmc
    (cl:cons ':header (header msg))
    (cl:cons ':mcmc (mcmc msg))
))
