; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude Utterance.msg.html

(cl:defclass <Utterance> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (sentence
    :reader sentence
    :initarg :sentence
    :type cl:string
    :initform "")
   (emotion
    :reader emotion
    :initarg :emotion
    :type cl:string
    :initform "")
   (volume
    :reader volume
    :initarg :volume
    :type cl:integer
    :initform 0)
   (agent
    :reader agent
    :initarg :agent
    :type cl:string
    :initform "")
   (priority
    :reader priority
    :initarg :priority
    :type cl:fixnum
    :initform 0))
)

(cl:defclass Utterance (<Utterance>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Utterance>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Utterance)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<Utterance> is deprecated: use monarch_msgs-msg:Utterance instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'sentence-val :lambda-list '(m))
(cl:defmethod sentence-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:sentence-val is deprecated.  Use monarch_msgs-msg:sentence instead.")
  (sentence m))

(cl:ensure-generic-function 'emotion-val :lambda-list '(m))
(cl:defmethod emotion-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:emotion-val is deprecated.  Use monarch_msgs-msg:emotion instead.")
  (emotion m))

(cl:ensure-generic-function 'volume-val :lambda-list '(m))
(cl:defmethod volume-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:volume-val is deprecated.  Use monarch_msgs-msg:volume instead.")
  (volume m))

(cl:ensure-generic-function 'agent-val :lambda-list '(m))
(cl:defmethod agent-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:agent-val is deprecated.  Use monarch_msgs-msg:agent instead.")
  (agent m))

(cl:ensure-generic-function 'priority-val :lambda-list '(m))
(cl:defmethod priority-val ((m <Utterance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:priority-val is deprecated.  Use monarch_msgs-msg:priority instead.")
  (priority m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<Utterance>)))
    "Constants for message type '<Utterance>"
  '((:SHUT_UP_IMEDIATLY . 0)
    (:AFTER_CURRENT . 1)
    (:APPEND . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'Utterance)))
    "Constants for message type 'Utterance"
  '((:SHUT_UP_IMEDIATLY . 0)
    (:AFTER_CURRENT . 1)
    (:APPEND . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Utterance>) ostream)
  "Serializes a message object of type '<Utterance>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'sentence))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'sentence))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'emotion))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'emotion))
  (cl:let* ((signed (cl:slot-value msg 'volume)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'agent))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'agent))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'priority)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Utterance>) istream)
  "Deserializes a message object of type '<Utterance>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'sentence) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'sentence) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'emotion) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'emotion) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'volume) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'agent) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'agent) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'priority)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Utterance>)))
  "Returns string type for a message object of type '<Utterance>"
  "monarch_msgs/Utterance")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Utterance)))
  "Returns string type for a message object of type 'Utterance"
  "monarch_msgs/Utterance")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Utterance>)))
  "Returns md5sum for a message object of type '<Utterance>"
  "9608cc09f607362184e99610f60bed77")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Utterance)))
  "Returns md5sum for a message object of type 'Utterance"
  "9608cc09f607362184e99610f60bed77")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Utterance>)))
  "Returns full string definition for message of type '<Utterance>"
  (cl:format cl:nil "#use standard header~%Header header~%~%string sentence~%string emotion  ~%int32 volume~%string agent                    # The voice of the robot~%~%uint8 priority~%uint8 SHUT_UP_IMEDIATLY = 0     # Abort current loqution. Clear queue~%uint8 AFTER_CURRENT     = 1     # Wait current loqution to finish. Clear queue~%uint8 APPEND            = 2     # Append to loqutions Queue~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Utterance)))
  "Returns full string definition for message of type 'Utterance"
  (cl:format cl:nil "#use standard header~%Header header~%~%string sentence~%string emotion  ~%int32 volume~%string agent                    # The voice of the robot~%~%uint8 priority~%uint8 SHUT_UP_IMEDIATLY = 0     # Abort current loqution. Clear queue~%uint8 AFTER_CURRENT     = 1     # Wait current loqution to finish. Clear queue~%uint8 APPEND            = 2     # Append to loqutions Queue~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Utterance>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'sentence))
     4 (cl:length (cl:slot-value msg 'emotion))
     4
     4 (cl:length (cl:slot-value msg 'agent))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Utterance>))
  "Converts a ROS message object to a list"
  (cl:list 'Utterance
    (cl:cons ':header (header msg))
    (cl:cons ':sentence (sentence msg))
    (cl:cons ':emotion (emotion msg))
    (cl:cons ':volume (volume msg))
    (cl:cons ':agent (agent msg))
    (cl:cons ':priority (priority msg))
))
