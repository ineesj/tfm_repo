(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          DEVICE-VAL
          DEVICE
          R-VAL
          R
          G-VAL
          G
          B-VAL
          B
          CHANGE_TIME-VAL
          CHANGE_TIME
))