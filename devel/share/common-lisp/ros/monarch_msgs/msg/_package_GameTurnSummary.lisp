(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          PLAYERS-VAL
          PLAYERS
          PLAYERCELL-VAL
          PLAYERCELL
          FEASIBLE-VAL
          FEASIBLE
          COHERENT-VAL
          COHERENT
))