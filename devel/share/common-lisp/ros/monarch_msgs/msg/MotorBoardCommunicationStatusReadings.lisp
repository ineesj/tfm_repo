; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude MotorBoardCommunicationStatusReadings.msg.html

(cl:defclass <MotorBoardCommunicationStatusReadings> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (a
    :reader a
    :initarg :a
    :type cl:boolean
    :initform cl:nil)
   (b
    :reader b
    :initarg :b
    :type cl:boolean
    :initform cl:nil)
   (c
    :reader c
    :initarg :c
    :type cl:boolean
    :initform cl:nil)
   (d
    :reader d
    :initarg :d
    :type cl:boolean
    :initform cl:nil)
   (e
    :reader e
    :initarg :e
    :type cl:boolean
    :initform cl:nil)
   (f
    :reader f
    :initarg :f
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass MotorBoardCommunicationStatusReadings (<MotorBoardCommunicationStatusReadings>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotorBoardCommunicationStatusReadings>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotorBoardCommunicationStatusReadings)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<MotorBoardCommunicationStatusReadings> is deprecated: use monarch_msgs-msg:MotorBoardCommunicationStatusReadings instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'a-val :lambda-list '(m))
(cl:defmethod a-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:a-val is deprecated.  Use monarch_msgs-msg:a instead.")
  (a m))

(cl:ensure-generic-function 'b-val :lambda-list '(m))
(cl:defmethod b-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:b-val is deprecated.  Use monarch_msgs-msg:b instead.")
  (b m))

(cl:ensure-generic-function 'c-val :lambda-list '(m))
(cl:defmethod c-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:c-val is deprecated.  Use monarch_msgs-msg:c instead.")
  (c m))

(cl:ensure-generic-function 'd-val :lambda-list '(m))
(cl:defmethod d-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:d-val is deprecated.  Use monarch_msgs-msg:d instead.")
  (d m))

(cl:ensure-generic-function 'e-val :lambda-list '(m))
(cl:defmethod e-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:e-val is deprecated.  Use monarch_msgs-msg:e instead.")
  (e m))

(cl:ensure-generic-function 'f-val :lambda-list '(m))
(cl:defmethod f-val ((m <MotorBoardCommunicationStatusReadings>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:f-val is deprecated.  Use monarch_msgs-msg:f instead.")
  (f m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotorBoardCommunicationStatusReadings>) ostream)
  "Serializes a message object of type '<MotorBoardCommunicationStatusReadings>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'a) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'b) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'c) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'd) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'e) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'f) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotorBoardCommunicationStatusReadings>) istream)
  "Deserializes a message object of type '<MotorBoardCommunicationStatusReadings>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'a) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'b) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'c) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'd) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'e) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'f) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotorBoardCommunicationStatusReadings>)))
  "Returns string type for a message object of type '<MotorBoardCommunicationStatusReadings>"
  "monarch_msgs/MotorBoardCommunicationStatusReadings")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotorBoardCommunicationStatusReadings)))
  "Returns string type for a message object of type 'MotorBoardCommunicationStatusReadings"
  "monarch_msgs/MotorBoardCommunicationStatusReadings")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotorBoardCommunicationStatusReadings>)))
  "Returns md5sum for a message object of type '<MotorBoardCommunicationStatusReadings>"
  "72821e0ee489eed96fe5e69efe768cf1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotorBoardCommunicationStatusReadings)))
  "Returns md5sum for a message object of type 'MotorBoardCommunicationStatusReadings"
  "72821e0ee489eed96fe5e69efe768cf1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotorBoardCommunicationStatusReadings>)))
  "Returns full string definition for message of type '<MotorBoardCommunicationStatusReadings>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each parameter.~%bool a    # Board communication with the navigation computer~%bool b    # Board communication with the sensor&management board~%bool c    # Board communication with the left front PI motor controller slave~%bool d    # Board communication with the right front PI motor controller slave~%bool e    # Board communication with the left rear PI motor controller slave~%bool f    # Board communication with the right rear PI motor controller slave~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotorBoardCommunicationStatusReadings)))
  "Returns full string definition for message of type 'MotorBoardCommunicationStatusReadings"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each parameter.~%bool a    # Board communication with the navigation computer~%bool b    # Board communication with the sensor&management board~%bool c    # Board communication with the left front PI motor controller slave~%bool d    # Board communication with the right front PI motor controller slave~%bool e    # Board communication with the left rear PI motor controller slave~%bool f    # Board communication with the right rear PI motor controller slave~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotorBoardCommunicationStatusReadings>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotorBoardCommunicationStatusReadings>))
  "Converts a ROS message object to a list"
  (cl:list 'MotorBoardCommunicationStatusReadings
    (cl:cons ':header (header msg))
    (cl:cons ':a (a msg))
    (cl:cons ':b (b msg))
    (cl:cons ':c (c msg))
    (cl:cons ':d (d msg))
    (cl:cons ':e (e msg))
    (cl:cons ':f (f msg))
))
