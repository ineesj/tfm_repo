(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          TAG_ID-VAL
          TAG_ID
          TAG_TYPE-VAL
          TAG_TYPE
          TAG_INFO-VAL
          TAG_INFO
          TAG_PROB-VAL
          TAG_PROB
          TAG_POSITION-VAL
          TAG_POSITION
          TAG_ANGLE-VAL
          TAG_ANGLE
))