; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude SetStateSonars.msg.html

(cl:defclass <SetStateSonars> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (status
    :reader status
    :initarg :status
    :type cl:integer
    :initform 0))
)

(cl:defclass SetStateSonars (<SetStateSonars>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetStateSonars>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetStateSonars)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<SetStateSonars> is deprecated: use monarch_msgs-msg:SetStateSonars instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <SetStateSonars>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <SetStateSonars>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:status-val is deprecated.  Use monarch_msgs-msg:status instead.")
  (status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetStateSonars>) ostream)
  "Serializes a message object of type '<SetStateSonars>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetStateSonars>) istream)
  "Deserializes a message object of type '<SetStateSonars>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetStateSonars>)))
  "Returns string type for a message object of type '<SetStateSonars>"
  "monarch_msgs/SetStateSonars")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetStateSonars)))
  "Returns string type for a message object of type 'SetStateSonars"
  "monarch_msgs/SetStateSonars")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetStateSonars>)))
  "Returns md5sum for a message object of type '<SetStateSonars>"
  "0620b169da01d76752bc32343d70df89")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetStateSonars)))
  "Returns md5sum for a message object of type 'SetStateSonars"
  "0620b169da01d76752bc32343d70df89")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetStateSonars>)))
  "Returns full string definition for message of type '<SetStateSonars>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#one bool.True enables sonars, false otherwise.~%byte status~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetStateSonars)))
  "Returns full string definition for message of type 'SetStateSonars"
  (cl:format cl:nil "#use standard header~%Header header~%~%#one bool.True enables sonars, false otherwise.~%byte status~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetStateSonars>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetStateSonars>))
  "Converts a ROS message object to a list"
  (cl:list 'SetStateSonars
    (cl:cons ':header (header msg))
    (cl:cons ':status (status msg))
))
