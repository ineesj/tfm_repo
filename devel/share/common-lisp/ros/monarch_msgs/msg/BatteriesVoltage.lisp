; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude BatteriesVoltage.msg.html

(cl:defclass <BatteriesVoltage> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (charger
    :reader charger
    :initarg :charger
    :type cl:float
    :initform 0.0)
   (electronics
    :reader electronics
    :initarg :electronics
    :type cl:float
    :initform 0.0)
   (motors
    :reader motors
    :initarg :motors
    :type cl:float
    :initform 0.0))
)

(cl:defclass BatteriesVoltage (<BatteriesVoltage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BatteriesVoltage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BatteriesVoltage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<BatteriesVoltage> is deprecated: use monarch_msgs-msg:BatteriesVoltage instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <BatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'charger-val :lambda-list '(m))
(cl:defmethod charger-val ((m <BatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:charger-val is deprecated.  Use monarch_msgs-msg:charger instead.")
  (charger m))

(cl:ensure-generic-function 'electronics-val :lambda-list '(m))
(cl:defmethod electronics-val ((m <BatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:electronics-val is deprecated.  Use monarch_msgs-msg:electronics instead.")
  (electronics m))

(cl:ensure-generic-function 'motors-val :lambda-list '(m))
(cl:defmethod motors-val ((m <BatteriesVoltage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:motors-val is deprecated.  Use monarch_msgs-msg:motors instead.")
  (motors m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BatteriesVoltage>) ostream)
  "Serializes a message object of type '<BatteriesVoltage>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'charger))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'electronics))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'motors))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BatteriesVoltage>) istream)
  "Deserializes a message object of type '<BatteriesVoltage>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'charger) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'electronics) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'motors) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BatteriesVoltage>)))
  "Returns string type for a message object of type '<BatteriesVoltage>"
  "monarch_msgs/BatteriesVoltage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BatteriesVoltage)))
  "Returns string type for a message object of type 'BatteriesVoltage"
  "monarch_msgs/BatteriesVoltage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BatteriesVoltage>)))
  "Returns md5sum for a message object of type '<BatteriesVoltage>"
  "c7c595d1139e2e4efcddda33aa77d1f0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BatteriesVoltage)))
  "Returns md5sum for a message object of type 'BatteriesVoltage"
  "c7c595d1139e2e4efcddda33aa77d1f0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BatteriesVoltage>)))
  "Returns full string definition for message of type '<BatteriesVoltage>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltage of charger line, electronics and motors. Measured in Volts.~%float32 charger~%float32 electronics~%float32 motors~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BatteriesVoltage)))
  "Returns full string definition for message of type 'BatteriesVoltage"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltage of charger line, electronics and motors. Measured in Volts.~%float32 charger~%float32 electronics~%float32 motors~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BatteriesVoltage>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BatteriesVoltage>))
  "Converts a ROS message object to a list"
  (cl:list 'BatteriesVoltage
    (cl:cons ':header (header msg))
    (cl:cons ':charger (charger msg))
    (cl:cons ':electronics (electronics msg))
    (cl:cons ':motors (motors msg))
))
