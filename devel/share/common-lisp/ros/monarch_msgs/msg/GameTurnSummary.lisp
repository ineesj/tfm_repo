; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GameTurnSummary.msg.html

(cl:defclass <GameTurnSummary> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (players
    :reader players
    :initarg :players
    :type (cl:vector cl:string)
   :initform (cl:make-array 0 :element-type 'cl:string :initial-element ""))
   (playerCell
    :reader playerCell
    :initarg :playerCell
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (feasible
    :reader feasible
    :initarg :feasible
    :type cl:boolean
    :initform cl:nil)
   (coherent
    :reader coherent
    :initarg :coherent
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass GameTurnSummary (<GameTurnSummary>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GameTurnSummary>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GameTurnSummary)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GameTurnSummary> is deprecated: use monarch_msgs-msg:GameTurnSummary instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GameTurnSummary>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'players-val :lambda-list '(m))
(cl:defmethod players-val ((m <GameTurnSummary>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:players-val is deprecated.  Use monarch_msgs-msg:players instead.")
  (players m))

(cl:ensure-generic-function 'playerCell-val :lambda-list '(m))
(cl:defmethod playerCell-val ((m <GameTurnSummary>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:playerCell-val is deprecated.  Use monarch_msgs-msg:playerCell instead.")
  (playerCell m))

(cl:ensure-generic-function 'feasible-val :lambda-list '(m))
(cl:defmethod feasible-val ((m <GameTurnSummary>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:feasible-val is deprecated.  Use monarch_msgs-msg:feasible instead.")
  (feasible m))

(cl:ensure-generic-function 'coherent-val :lambda-list '(m))
(cl:defmethod coherent-val ((m <GameTurnSummary>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:coherent-val is deprecated.  Use monarch_msgs-msg:coherent instead.")
  (coherent m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GameTurnSummary>) ostream)
  "Serializes a message object of type '<GameTurnSummary>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'players))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((__ros_str_len (cl:length ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) ele))
   (cl:slot-value msg 'players))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'playerCell))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    ))
   (cl:slot-value msg 'playerCell))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'feasible) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'coherent) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GameTurnSummary>) istream)
  "Deserializes a message object of type '<GameTurnSummary>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'players) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'players)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:aref vals i) __ros_str_idx) (cl:code-char (cl:read-byte istream))))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'playerCell) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'playerCell)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256)))))))
    (cl:setf (cl:slot-value msg 'feasible) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'coherent) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GameTurnSummary>)))
  "Returns string type for a message object of type '<GameTurnSummary>"
  "monarch_msgs/GameTurnSummary")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GameTurnSummary)))
  "Returns string type for a message object of type 'GameTurnSummary"
  "monarch_msgs/GameTurnSummary")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GameTurnSummary>)))
  "Returns md5sum for a message object of type '<GameTurnSummary>"
  "039b28755b0528a72719ae3dda8f697e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GameTurnSummary)))
  "Returns md5sum for a message object of type 'GameTurnSummary"
  "039b28755b0528a72719ae3dda8f697e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GameTurnSummary>)))
  "Returns full string definition for message of type '<GameTurnSummary>"
  (cl:format cl:nil "#use standard header~%Header header~%~%string[] players  		 # name of the player~%int8[] playerCell	 	 # player cell~%bool feasible		~%bool coherent ~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GameTurnSummary)))
  "Returns full string definition for message of type 'GameTurnSummary"
  (cl:format cl:nil "#use standard header~%Header header~%~%string[] players  		 # name of the player~%int8[] playerCell	 	 # player cell~%bool feasible		~%bool coherent ~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GameTurnSummary>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'players) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4 (cl:length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'playerCell) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GameTurnSummary>))
  "Converts a ROS message object to a list"
  (cl:list 'GameTurnSummary
    (cl:cons ':header (header msg))
    (cl:cons ':players (players msg))
    (cl:cons ':playerCell (playerCell msg))
    (cl:cons ':feasible (feasible msg))
    (cl:cons ':coherent (coherent msg))
))
