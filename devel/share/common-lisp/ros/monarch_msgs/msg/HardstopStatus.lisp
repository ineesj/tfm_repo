; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude HardstopStatus.msg.html

(cl:defclass <HardstopStatus> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (hardstopActive
    :reader hardstopActive
    :initarg :hardstopActive
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass HardstopStatus (<HardstopStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HardstopStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HardstopStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<HardstopStatus> is deprecated: use monarch_msgs-msg:HardstopStatus instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <HardstopStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'hardstopActive-val :lambda-list '(m))
(cl:defmethod hardstopActive-val ((m <HardstopStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:hardstopActive-val is deprecated.  Use monarch_msgs-msg:hardstopActive instead.")
  (hardstopActive m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HardstopStatus>) ostream)
  "Serializes a message object of type '<HardstopStatus>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'hardstopActive) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HardstopStatus>) istream)
  "Deserializes a message object of type '<HardstopStatus>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'hardstopActive) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HardstopStatus>)))
  "Returns string type for a message object of type '<HardstopStatus>"
  "monarch_msgs/HardstopStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HardstopStatus)))
  "Returns string type for a message object of type 'HardstopStatus"
  "monarch_msgs/HardstopStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HardstopStatus>)))
  "Returns md5sum for a message object of type '<HardstopStatus>"
  "3412317af39c611e13dbf190f9c5926d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HardstopStatus)))
  "Returns md5sum for a message object of type 'HardstopStatus"
  "3412317af39c611e13dbf190f9c5926d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HardstopStatus>)))
  "Returns full string definition for message of type '<HardstopStatus>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#True means hardstop is active and robot motors are disabled~%bool hardstopActive~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HardstopStatus)))
  "Returns full string definition for message of type 'HardstopStatus"
  (cl:format cl:nil "#use standard header~%Header header~%~%#True means hardstop is active and robot motors are disabled~%bool hardstopActive~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HardstopStatus>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HardstopStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'HardstopStatus
    (cl:cons ':header (header msg))
    (cl:cons ':hardstopActive (hardstopActive msg))
))
