; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude ChargerStatus.msg.html

(cl:defclass <ChargerStatus> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (pc2BatteryCharging
    :reader pc2BatteryCharging
    :initarg :pc2BatteryCharging
    :type cl:boolean
    :initform cl:nil)
   (pc1BatteryCharging
    :reader pc1BatteryCharging
    :initarg :pc1BatteryCharging
    :type cl:boolean
    :initform cl:nil)
   (motorsBatteryCharging
    :reader motorsBatteryCharging
    :initarg :motorsBatteryCharging
    :type cl:boolean
    :initform cl:nil)
   (electronicsBatteryCharging
    :reader electronicsBatteryCharging
    :initarg :electronicsBatteryCharging
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass ChargerStatus (<ChargerStatus>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ChargerStatus>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ChargerStatus)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<ChargerStatus> is deprecated: use monarch_msgs-msg:ChargerStatus instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ChargerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'pc2BatteryCharging-val :lambda-list '(m))
(cl:defmethod pc2BatteryCharging-val ((m <ChargerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:pc2BatteryCharging-val is deprecated.  Use monarch_msgs-msg:pc2BatteryCharging instead.")
  (pc2BatteryCharging m))

(cl:ensure-generic-function 'pc1BatteryCharging-val :lambda-list '(m))
(cl:defmethod pc1BatteryCharging-val ((m <ChargerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:pc1BatteryCharging-val is deprecated.  Use monarch_msgs-msg:pc1BatteryCharging instead.")
  (pc1BatteryCharging m))

(cl:ensure-generic-function 'motorsBatteryCharging-val :lambda-list '(m))
(cl:defmethod motorsBatteryCharging-val ((m <ChargerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:motorsBatteryCharging-val is deprecated.  Use monarch_msgs-msg:motorsBatteryCharging instead.")
  (motorsBatteryCharging m))

(cl:ensure-generic-function 'electronicsBatteryCharging-val :lambda-list '(m))
(cl:defmethod electronicsBatteryCharging-val ((m <ChargerStatus>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:electronicsBatteryCharging-val is deprecated.  Use monarch_msgs-msg:electronicsBatteryCharging instead.")
  (electronicsBatteryCharging m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ChargerStatus>) ostream)
  "Serializes a message object of type '<ChargerStatus>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pc2BatteryCharging) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pc1BatteryCharging) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'motorsBatteryCharging) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'electronicsBatteryCharging) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ChargerStatus>) istream)
  "Deserializes a message object of type '<ChargerStatus>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'pc2BatteryCharging) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'pc1BatteryCharging) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'motorsBatteryCharging) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'electronicsBatteryCharging) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ChargerStatus>)))
  "Returns string type for a message object of type '<ChargerStatus>"
  "monarch_msgs/ChargerStatus")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ChargerStatus)))
  "Returns string type for a message object of type 'ChargerStatus"
  "monarch_msgs/ChargerStatus")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ChargerStatus>)))
  "Returns md5sum for a message object of type '<ChargerStatus>"
  "61b6d896e82fb136d1f4e3a110ba953f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ChargerStatus)))
  "Returns md5sum for a message object of type 'ChargerStatus"
  "61b6d896e82fb136d1f4e3a110ba953f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ChargerStatus>)))
  "Returns full string definition for message of type '<ChargerStatus>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each status. True means corresponding battery is charging.~%bool pc2BatteryCharging~%bool pc1BatteryCharging~%bool motorsBatteryCharging~%bool electronicsBatteryCharging~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ChargerStatus)))
  "Returns full string definition for message of type 'ChargerStatus"
  (cl:format cl:nil "#use standard header~%Header header~%~%#One bool for each status. True means corresponding battery is charging.~%bool pc2BatteryCharging~%bool pc1BatteryCharging~%bool motorsBatteryCharging~%bool electronicsBatteryCharging~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ChargerStatus>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ChargerStatus>))
  "Converts a ROS message object to a list"
  (cl:list 'ChargerStatus
    (cl:cons ':header (header msg))
    (cl:cons ':pc2BatteryCharging (pc2BatteryCharging msg))
    (cl:cons ':pc1BatteryCharging (pc1BatteryCharging msg))
    (cl:cons ':motorsBatteryCharging (motorsBatteryCharging msg))
    (cl:cons ':electronicsBatteryCharging (electronicsBatteryCharging msg))
))
