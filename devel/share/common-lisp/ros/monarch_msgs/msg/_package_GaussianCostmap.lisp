(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          ID-VAL
          ID
          MEAN_X-VAL
          MEAN_X
          MEAN_Y-VAL
          MEAN_Y
          SIGMA_X-VAL
          SIGMA_X
          SIGMA_Y-VAL
          SIGMA_Y
))