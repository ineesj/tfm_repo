; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude GaussianCostmap.msg.html

(cl:defclass <GaussianCostmap> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (id
    :reader id
    :initarg :id
    :type cl:integer
    :initform 0)
   (mean_x
    :reader mean_x
    :initarg :mean_x
    :type cl:float
    :initform 0.0)
   (mean_y
    :reader mean_y
    :initarg :mean_y
    :type cl:float
    :initform 0.0)
   (sigma_x
    :reader sigma_x
    :initarg :sigma_x
    :type cl:float
    :initform 0.0)
   (sigma_y
    :reader sigma_y
    :initarg :sigma_y
    :type cl:float
    :initform 0.0))
)

(cl:defclass GaussianCostmap (<GaussianCostmap>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GaussianCostmap>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GaussianCostmap)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<GaussianCostmap> is deprecated: use monarch_msgs-msg:GaussianCostmap instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:id-val is deprecated.  Use monarch_msgs-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'mean_x-val :lambda-list '(m))
(cl:defmethod mean_x-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mean_x-val is deprecated.  Use monarch_msgs-msg:mean_x instead.")
  (mean_x m))

(cl:ensure-generic-function 'mean_y-val :lambda-list '(m))
(cl:defmethod mean_y-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:mean_y-val is deprecated.  Use monarch_msgs-msg:mean_y instead.")
  (mean_y m))

(cl:ensure-generic-function 'sigma_x-val :lambda-list '(m))
(cl:defmethod sigma_x-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:sigma_x-val is deprecated.  Use monarch_msgs-msg:sigma_x instead.")
  (sigma_x m))

(cl:ensure-generic-function 'sigma_y-val :lambda-list '(m))
(cl:defmethod sigma_y-val ((m <GaussianCostmap>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:sigma_y-val is deprecated.  Use monarch_msgs-msg:sigma_y instead.")
  (sigma_y m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GaussianCostmap>) ostream)
  "Serializes a message object of type '<GaussianCostmap>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'mean_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'mean_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'sigma_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'sigma_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GaussianCostmap>) istream)
  "Deserializes a message object of type '<GaussianCostmap>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'mean_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'mean_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sigma_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sigma_y) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GaussianCostmap>)))
  "Returns string type for a message object of type '<GaussianCostmap>"
  "monarch_msgs/GaussianCostmap")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GaussianCostmap)))
  "Returns string type for a message object of type 'GaussianCostmap"
  "monarch_msgs/GaussianCostmap")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GaussianCostmap>)))
  "Returns md5sum for a message object of type '<GaussianCostmap>"
  "e4c3712f2991d35b606bf4a38ef56487")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GaussianCostmap)))
  "Returns md5sum for a message object of type 'GaussianCostmap"
  "e4c3712f2991d35b606bf4a38ef56487")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GaussianCostmap>)))
  "Returns full string definition for message of type '<GaussianCostmap>"
  (cl:format cl:nil "#use standard header~%~%Header header~%int32 id          # id of person associated to the costmap~%~%float32 mean_x    # x coordinate of the center of the costmap in map frame~%float32 mean_y   #  y coordinate of the center of the costmap in map frame~%float32 sigma_x    #  sigma of the costmap in x direction~%float32 sigma_y   #  sigma of the costmap in y direction~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GaussianCostmap)))
  "Returns full string definition for message of type 'GaussianCostmap"
  (cl:format cl:nil "#use standard header~%~%Header header~%int32 id          # id of person associated to the costmap~%~%float32 mean_x    # x coordinate of the center of the costmap in map frame~%float32 mean_y   #  y coordinate of the center of the costmap in map frame~%float32 sigma_x    #  sigma of the costmap in x direction~%float32 sigma_y   #  sigma of the costmap in y direction~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GaussianCostmap>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GaussianCostmap>))
  "Converts a ROS message object to a list"
  (cl:list 'GaussianCostmap
    (cl:cons ':header (header msg))
    (cl:cons ':id (id msg))
    (cl:cons ':mean_x (mean_x msg))
    (cl:cons ':mean_y (mean_y msg))
    (cl:cons ':sigma_x (sigma_x msg))
    (cl:cons ':sigma_y (sigma_y msg))
))
