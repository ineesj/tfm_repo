; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude EyesControl.msg.html

(cl:defclass <EyesControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (lr
    :reader lr
    :initarg :lr
    :type cl:fixnum
    :initform 0)
   (lg
    :reader lg
    :initarg :lg
    :type cl:fixnum
    :initform 0)
   (lb
    :reader lb
    :initarg :lb
    :type cl:fixnum
    :initform 0)
   (lchange_time
    :reader lchange_time
    :initarg :lchange_time
    :type cl:float
    :initform 0.0)
   (rr
    :reader rr
    :initarg :rr
    :type cl:fixnum
    :initform 0)
   (rg
    :reader rg
    :initarg :rg
    :type cl:fixnum
    :initform 0)
   (rb
    :reader rb
    :initarg :rb
    :type cl:fixnum
    :initform 0)
   (rchange_time
    :reader rchange_time
    :initarg :rchange_time
    :type cl:float
    :initform 0.0))
)

(cl:defclass EyesControl (<EyesControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <EyesControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'EyesControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<EyesControl> is deprecated: use monarch_msgs-msg:EyesControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'lr-val :lambda-list '(m))
(cl:defmethod lr-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:lr-val is deprecated.  Use monarch_msgs-msg:lr instead.")
  (lr m))

(cl:ensure-generic-function 'lg-val :lambda-list '(m))
(cl:defmethod lg-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:lg-val is deprecated.  Use monarch_msgs-msg:lg instead.")
  (lg m))

(cl:ensure-generic-function 'lb-val :lambda-list '(m))
(cl:defmethod lb-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:lb-val is deprecated.  Use monarch_msgs-msg:lb instead.")
  (lb m))

(cl:ensure-generic-function 'lchange_time-val :lambda-list '(m))
(cl:defmethod lchange_time-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:lchange_time-val is deprecated.  Use monarch_msgs-msg:lchange_time instead.")
  (lchange_time m))

(cl:ensure-generic-function 'rr-val :lambda-list '(m))
(cl:defmethod rr-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rr-val is deprecated.  Use monarch_msgs-msg:rr instead.")
  (rr m))

(cl:ensure-generic-function 'rg-val :lambda-list '(m))
(cl:defmethod rg-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rg-val is deprecated.  Use monarch_msgs-msg:rg instead.")
  (rg m))

(cl:ensure-generic-function 'rb-val :lambda-list '(m))
(cl:defmethod rb-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rb-val is deprecated.  Use monarch_msgs-msg:rb instead.")
  (rb m))

(cl:ensure-generic-function 'rchange_time-val :lambda-list '(m))
(cl:defmethod rchange_time-val ((m <EyesControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rchange_time-val is deprecated.  Use monarch_msgs-msg:rchange_time instead.")
  (rchange_time m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <EyesControl>) ostream)
  "Serializes a message object of type '<EyesControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lr)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lg)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lb)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'lchange_time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rr)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rg)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rb)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'rchange_time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <EyesControl>) istream)
  "Deserializes a message object of type '<EyesControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lr)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lg)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'lb)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'lchange_time) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rr)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rg)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rb)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'rchange_time) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<EyesControl>)))
  "Returns string type for a message object of type '<EyesControl>"
  "monarch_msgs/EyesControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'EyesControl)))
  "Returns string type for a message object of type 'EyesControl"
  "monarch_msgs/EyesControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<EyesControl>)))
  "Returns md5sum for a message object of type '<EyesControl>"
  "5fa4cafda84baaf3cbc1d872860f181a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'EyesControl)))
  "Returns md5sum for a message object of type 'EyesControl"
  "5fa4cafda84baaf3cbc1d872860f181a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<EyesControl>)))
  "Returns full string definition for message of type '<EyesControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint8 lr              #red intensity percentage~%uint8 lg              #green intensity percentage~%uint8 lb              #blue intensity percentage~%float32 lchange_time  #time to reach intensity~%uint8 rr              #red intensity percentage~%uint8 rg              #green intensity percentage~%uint8 rb              #blue intensity percentage~%float32 rchange_time  #time to reach intensity~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'EyesControl)))
  "Returns full string definition for message of type 'EyesControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint8 lr              #red intensity percentage~%uint8 lg              #green intensity percentage~%uint8 lb              #blue intensity percentage~%float32 lchange_time  #time to reach intensity~%uint8 rr              #red intensity percentage~%uint8 rg              #green intensity percentage~%uint8 rb              #blue intensity percentage~%float32 rchange_time  #time to reach intensity~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <EyesControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     4
     1
     1
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <EyesControl>))
  "Converts a ROS message object to a list"
  (cl:list 'EyesControl
    (cl:cons ':header (header msg))
    (cl:cons ':lr (lr msg))
    (cl:cons ':lg (lg msg))
    (cl:cons ':lb (lb msg))
    (cl:cons ':lchange_time (lchange_time msg))
    (cl:cons ':rr (rr msg))
    (cl:cons ':rg (rg msg))
    (cl:cons ':rb (rb msg))
    (cl:cons ':rchange_time (rchange_time msg))
))
