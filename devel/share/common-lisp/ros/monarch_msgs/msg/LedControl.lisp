; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude LedControl.msg.html

(cl:defclass <LedControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (device
    :reader device
    :initarg :device
    :type cl:fixnum
    :initform 0)
   (r
    :reader r
    :initarg :r
    :type cl:fixnum
    :initform 0)
   (g
    :reader g
    :initarg :g
    :type cl:fixnum
    :initform 0)
   (b
    :reader b
    :initarg :b
    :type cl:fixnum
    :initform 0)
   (change_time
    :reader change_time
    :initarg :change_time
    :type cl:float
    :initform 0.0))
)

(cl:defclass LedControl (<LedControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <LedControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'LedControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<LedControl> is deprecated: use monarch_msgs-msg:LedControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'device-val :lambda-list '(m))
(cl:defmethod device-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:device-val is deprecated.  Use monarch_msgs-msg:device instead.")
  (device m))

(cl:ensure-generic-function 'r-val :lambda-list '(m))
(cl:defmethod r-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:r-val is deprecated.  Use monarch_msgs-msg:r instead.")
  (r m))

(cl:ensure-generic-function 'g-val :lambda-list '(m))
(cl:defmethod g-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:g-val is deprecated.  Use monarch_msgs-msg:g instead.")
  (g m))

(cl:ensure-generic-function 'b-val :lambda-list '(m))
(cl:defmethod b-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:b-val is deprecated.  Use monarch_msgs-msg:b instead.")
  (b m))

(cl:ensure-generic-function 'change_time-val :lambda-list '(m))
(cl:defmethod change_time-val ((m <LedControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:change_time-val is deprecated.  Use monarch_msgs-msg:change_time instead.")
  (change_time m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <LedControl>) ostream)
  "Serializes a message object of type '<LedControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'device)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'r)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'g)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'b)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'change_time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <LedControl>) istream)
  "Deserializes a message object of type '<LedControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'device)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'r)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'g)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'b)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'change_time) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<LedControl>)))
  "Returns string type for a message object of type '<LedControl>"
  "monarch_msgs/LedControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LedControl)))
  "Returns string type for a message object of type 'LedControl"
  "monarch_msgs/LedControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<LedControl>)))
  "Returns md5sum for a message object of type '<LedControl>"
  "33c2d9ea0709bea316a36920c1d7cb22")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'LedControl)))
  "Returns md5sum for a message object of type 'LedControl"
  "33c2d9ea0709bea316a36920c1d7cb22")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<LedControl>)))
  "Returns full string definition for message of type '<LedControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint8 device         # index of the device to control~%# 0 - LeftEyeLeds~%# 1 - RightEyeLeds~%# 2 - CheeksLeds~%# 3 - BaseRightLeds~%# 4 - BaseFrontLeds~%# 5 - BaseLeftLeds~%uint8 r              #red intensity percentage (0-100)~%uint8 g              #green intensity percentage~%uint8 b              #blue intensity percentage~%float32 change_time  #time in seconds to reach intensity~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'LedControl)))
  "Returns full string definition for message of type 'LedControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%uint8 device         # index of the device to control~%# 0 - LeftEyeLeds~%# 1 - RightEyeLeds~%# 2 - CheeksLeds~%# 3 - BaseRightLeds~%# 4 - BaseFrontLeds~%# 5 - BaseLeftLeds~%uint8 r              #red intensity percentage (0-100)~%uint8 g              #green intensity percentage~%uint8 b              #blue intensity percentage~%float32 change_time  #time in seconds to reach intensity~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <LedControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <LedControl>))
  "Converts a ROS message object to a list"
  (cl:list 'LedControl
    (cl:cons ':header (header msg))
    (cl:cons ':device (device msg))
    (cl:cons ':r (r msg))
    (cl:cons ':g (g msg))
    (cl:cons ':b (b msg))
    (cl:cons ':change_time (change_time msg))
))
