(cl:in-package monarch_msgs-msg)
(cl:export '(NAME-VAL
          NAME
          INSTANCE_ID-VAL
          INSTANCE_ID
          ROBOTS-VAL
          ROBOTS
          PARAMETERS-VAL
          PARAMETERS
          RESOURCES-VAL
          RESOURCES
          ACTIVE-VAL
          ACTIVE
))