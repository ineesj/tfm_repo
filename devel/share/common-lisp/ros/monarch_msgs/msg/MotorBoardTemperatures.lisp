; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude MotorBoardTemperatures.msg.html

(cl:defclass <MotorBoardTemperatures> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (leftFrontMotorTemp
    :reader leftFrontMotorTemp
    :initarg :leftFrontMotorTemp
    :type cl:fixnum
    :initform 0)
   (rightFrontMotorTemp
    :reader rightFrontMotorTemp
    :initarg :rightFrontMotorTemp
    :type cl:fixnum
    :initform 0)
   (leftRearMotorTemp
    :reader leftRearMotorTemp
    :initarg :leftRearMotorTemp
    :type cl:fixnum
    :initform 0)
   (rightRearMotorTemp
    :reader rightRearMotorTemp
    :initarg :rightRearMotorTemp
    :type cl:fixnum
    :initform 0)
   (leftFrontDriverTemp
    :reader leftFrontDriverTemp
    :initarg :leftFrontDriverTemp
    :type cl:fixnum
    :initform 0)
   (rightFrontDriverTemp
    :reader rightFrontDriverTemp
    :initarg :rightFrontDriverTemp
    :type cl:fixnum
    :initform 0)
   (leftRearDriverTemp
    :reader leftRearDriverTemp
    :initarg :leftRearDriverTemp
    :type cl:fixnum
    :initform 0)
   (rightRearDriverTemp
    :reader rightRearDriverTemp
    :initarg :rightRearDriverTemp
    :type cl:fixnum
    :initform 0)
   (leftFrontMotorTempIsHigh
    :reader leftFrontMotorTempIsHigh
    :initarg :leftFrontMotorTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (rightFrontMotorTempIsHigh
    :reader rightFrontMotorTempIsHigh
    :initarg :rightFrontMotorTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (leftRearMotorTempIsHigh
    :reader leftRearMotorTempIsHigh
    :initarg :leftRearMotorTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (rightRearMotorTempIsHigh
    :reader rightRearMotorTempIsHigh
    :initarg :rightRearMotorTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (leftFrontDriverTempIsHigh
    :reader leftFrontDriverTempIsHigh
    :initarg :leftFrontDriverTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (rightFrontDriverTempIsHigh
    :reader rightFrontDriverTempIsHigh
    :initarg :rightFrontDriverTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (leftRearDriverTempIsHigh
    :reader leftRearDriverTempIsHigh
    :initarg :leftRearDriverTempIsHigh
    :type cl:boolean
    :initform cl:nil)
   (rightRearDriverTempIsHigh
    :reader rightRearDriverTempIsHigh
    :initarg :rightRearDriverTempIsHigh
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass MotorBoardTemperatures (<MotorBoardTemperatures>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotorBoardTemperatures>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotorBoardTemperatures)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<MotorBoardTemperatures> is deprecated: use monarch_msgs-msg:MotorBoardTemperatures instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'leftFrontMotorTemp-val :lambda-list '(m))
(cl:defmethod leftFrontMotorTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFrontMotorTemp-val is deprecated.  Use monarch_msgs-msg:leftFrontMotorTemp instead.")
  (leftFrontMotorTemp m))

(cl:ensure-generic-function 'rightFrontMotorTemp-val :lambda-list '(m))
(cl:defmethod rightFrontMotorTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFrontMotorTemp-val is deprecated.  Use monarch_msgs-msg:rightFrontMotorTemp instead.")
  (rightFrontMotorTemp m))

(cl:ensure-generic-function 'leftRearMotorTemp-val :lambda-list '(m))
(cl:defmethod leftRearMotorTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftRearMotorTemp-val is deprecated.  Use monarch_msgs-msg:leftRearMotorTemp instead.")
  (leftRearMotorTemp m))

(cl:ensure-generic-function 'rightRearMotorTemp-val :lambda-list '(m))
(cl:defmethod rightRearMotorTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightRearMotorTemp-val is deprecated.  Use monarch_msgs-msg:rightRearMotorTemp instead.")
  (rightRearMotorTemp m))

(cl:ensure-generic-function 'leftFrontDriverTemp-val :lambda-list '(m))
(cl:defmethod leftFrontDriverTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFrontDriverTemp-val is deprecated.  Use monarch_msgs-msg:leftFrontDriverTemp instead.")
  (leftFrontDriverTemp m))

(cl:ensure-generic-function 'rightFrontDriverTemp-val :lambda-list '(m))
(cl:defmethod rightFrontDriverTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFrontDriverTemp-val is deprecated.  Use monarch_msgs-msg:rightFrontDriverTemp instead.")
  (rightFrontDriverTemp m))

(cl:ensure-generic-function 'leftRearDriverTemp-val :lambda-list '(m))
(cl:defmethod leftRearDriverTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftRearDriverTemp-val is deprecated.  Use monarch_msgs-msg:leftRearDriverTemp instead.")
  (leftRearDriverTemp m))

(cl:ensure-generic-function 'rightRearDriverTemp-val :lambda-list '(m))
(cl:defmethod rightRearDriverTemp-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightRearDriverTemp-val is deprecated.  Use monarch_msgs-msg:rightRearDriverTemp instead.")
  (rightRearDriverTemp m))

(cl:ensure-generic-function 'leftFrontMotorTempIsHigh-val :lambda-list '(m))
(cl:defmethod leftFrontMotorTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFrontMotorTempIsHigh-val is deprecated.  Use monarch_msgs-msg:leftFrontMotorTempIsHigh instead.")
  (leftFrontMotorTempIsHigh m))

(cl:ensure-generic-function 'rightFrontMotorTempIsHigh-val :lambda-list '(m))
(cl:defmethod rightFrontMotorTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFrontMotorTempIsHigh-val is deprecated.  Use monarch_msgs-msg:rightFrontMotorTempIsHigh instead.")
  (rightFrontMotorTempIsHigh m))

(cl:ensure-generic-function 'leftRearMotorTempIsHigh-val :lambda-list '(m))
(cl:defmethod leftRearMotorTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftRearMotorTempIsHigh-val is deprecated.  Use monarch_msgs-msg:leftRearMotorTempIsHigh instead.")
  (leftRearMotorTempIsHigh m))

(cl:ensure-generic-function 'rightRearMotorTempIsHigh-val :lambda-list '(m))
(cl:defmethod rightRearMotorTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightRearMotorTempIsHigh-val is deprecated.  Use monarch_msgs-msg:rightRearMotorTempIsHigh instead.")
  (rightRearMotorTempIsHigh m))

(cl:ensure-generic-function 'leftFrontDriverTempIsHigh-val :lambda-list '(m))
(cl:defmethod leftFrontDriverTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftFrontDriverTempIsHigh-val is deprecated.  Use monarch_msgs-msg:leftFrontDriverTempIsHigh instead.")
  (leftFrontDriverTempIsHigh m))

(cl:ensure-generic-function 'rightFrontDriverTempIsHigh-val :lambda-list '(m))
(cl:defmethod rightFrontDriverTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightFrontDriverTempIsHigh-val is deprecated.  Use monarch_msgs-msg:rightFrontDriverTempIsHigh instead.")
  (rightFrontDriverTempIsHigh m))

(cl:ensure-generic-function 'leftRearDriverTempIsHigh-val :lambda-list '(m))
(cl:defmethod leftRearDriverTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:leftRearDriverTempIsHigh-val is deprecated.  Use monarch_msgs-msg:leftRearDriverTempIsHigh instead.")
  (leftRearDriverTempIsHigh m))

(cl:ensure-generic-function 'rightRearDriverTempIsHigh-val :lambda-list '(m))
(cl:defmethod rightRearDriverTempIsHigh-val ((m <MotorBoardTemperatures>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:rightRearDriverTempIsHigh-val is deprecated.  Use monarch_msgs-msg:rightRearDriverTempIsHigh instead.")
  (rightRearDriverTempIsHigh m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotorBoardTemperatures>) ostream)
  "Serializes a message object of type '<MotorBoardTemperatures>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFrontMotorTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFrontMotorTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftRearMotorTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightRearMotorTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFrontDriverTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFrontDriverTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftRearDriverTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightRearDriverTemp)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftFrontMotorTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightFrontMotorTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftRearMotorTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightRearMotorTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftFrontDriverTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightFrontDriverTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'leftRearDriverTempIsHigh) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rightRearDriverTempIsHigh) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotorBoardTemperatures>) istream)
  "Deserializes a message object of type '<MotorBoardTemperatures>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFrontMotorTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFrontMotorTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftRearMotorTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightRearMotorTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftFrontDriverTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightFrontDriverTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'leftRearDriverTemp)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'rightRearDriverTemp)) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'leftFrontMotorTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightFrontMotorTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'leftRearMotorTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightRearMotorTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'leftFrontDriverTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightFrontDriverTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'leftRearDriverTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'rightRearDriverTempIsHigh) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotorBoardTemperatures>)))
  "Returns string type for a message object of type '<MotorBoardTemperatures>"
  "monarch_msgs/MotorBoardTemperatures")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotorBoardTemperatures)))
  "Returns string type for a message object of type 'MotorBoardTemperatures"
  "monarch_msgs/MotorBoardTemperatures")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotorBoardTemperatures>)))
  "Returns md5sum for a message object of type '<MotorBoardTemperatures>"
  "a95db1a9481d06ad241dc4741ebfa8b0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotorBoardTemperatures)))
  "Returns md5sum for a message object of type 'MotorBoardTemperatures"
  "a95db1a9481d06ad241dc4741ebfa8b0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotorBoardTemperatures>)))
  "Returns full string definition for message of type '<MotorBoardTemperatures>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Temperatures measured by each sensor in degrees celsius and booleans for temperatures higher than 40 degrees celsius.~%uint8 leftFrontMotorTemp~%uint8 rightFrontMotorTemp~%uint8 leftRearMotorTemp~%uint8 rightRearMotorTemp~%uint8 leftFrontDriverTemp~%uint8 rightFrontDriverTemp~%uint8 leftRearDriverTemp~%uint8 rightRearDriverTemp~%bool leftFrontMotorTempIsHigh~%bool rightFrontMotorTempIsHigh~%bool leftRearMotorTempIsHigh~%bool rightRearMotorTempIsHigh~%bool leftFrontDriverTempIsHigh~%bool rightFrontDriverTempIsHigh~%bool leftRearDriverTempIsHigh~%bool rightRearDriverTempIsHigh~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotorBoardTemperatures)))
  "Returns full string definition for message of type 'MotorBoardTemperatures"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Temperatures measured by each sensor in degrees celsius and booleans for temperatures higher than 40 degrees celsius.~%uint8 leftFrontMotorTemp~%uint8 rightFrontMotorTemp~%uint8 leftRearMotorTemp~%uint8 rightRearMotorTemp~%uint8 leftFrontDriverTemp~%uint8 rightFrontDriverTemp~%uint8 leftRearDriverTemp~%uint8 rightRearDriverTemp~%bool leftFrontMotorTempIsHigh~%bool rightFrontMotorTempIsHigh~%bool leftRearMotorTempIsHigh~%bool rightRearMotorTempIsHigh~%bool leftFrontDriverTempIsHigh~%bool rightFrontDriverTempIsHigh~%bool leftRearDriverTempIsHigh~%bool rightRearDriverTempIsHigh~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotorBoardTemperatures>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotorBoardTemperatures>))
  "Converts a ROS message object to a list"
  (cl:list 'MotorBoardTemperatures
    (cl:cons ':header (header msg))
    (cl:cons ':leftFrontMotorTemp (leftFrontMotorTemp msg))
    (cl:cons ':rightFrontMotorTemp (rightFrontMotorTemp msg))
    (cl:cons ':leftRearMotorTemp (leftRearMotorTemp msg))
    (cl:cons ':rightRearMotorTemp (rightRearMotorTemp msg))
    (cl:cons ':leftFrontDriverTemp (leftFrontDriverTemp msg))
    (cl:cons ':rightFrontDriverTemp (rightFrontDriverTemp msg))
    (cl:cons ':leftRearDriverTemp (leftRearDriverTemp msg))
    (cl:cons ':rightRearDriverTemp (rightRearDriverTemp msg))
    (cl:cons ':leftFrontMotorTempIsHigh (leftFrontMotorTempIsHigh msg))
    (cl:cons ':rightFrontMotorTempIsHigh (rightFrontMotorTempIsHigh msg))
    (cl:cons ':leftRearMotorTempIsHigh (leftRearMotorTempIsHigh msg))
    (cl:cons ':rightRearMotorTempIsHigh (rightRearMotorTempIsHigh msg))
    (cl:cons ':leftFrontDriverTempIsHigh (leftFrontDriverTempIsHigh msg))
    (cl:cons ':rightFrontDriverTempIsHigh (rightFrontDriverTempIsHigh msg))
    (cl:cons ':leftRearDriverTempIsHigh (leftRearDriverTempIsHigh msg))
    (cl:cons ':rightRearDriverTempIsHigh (rightRearDriverTempIsHigh msg))
))
