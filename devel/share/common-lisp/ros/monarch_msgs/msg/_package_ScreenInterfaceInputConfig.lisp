(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          ENABLE_INPUT-VAL
          ENABLE_INPUT
          NUMBER_BUTTONS-VAL
          NUMBER_BUTTONS
          BUTTONS_CONFIG-VAL
          BUTTONS_CONFIG
))