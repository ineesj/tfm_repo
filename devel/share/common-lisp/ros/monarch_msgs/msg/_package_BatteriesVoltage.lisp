(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          CHARGER-VAL
          CHARGER
          ELECTRONICS-VAL
          ELECTRONICS
          MOTORS-VAL
          MOTORS
))