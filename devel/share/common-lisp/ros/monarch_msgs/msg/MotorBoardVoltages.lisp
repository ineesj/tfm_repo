; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude MotorBoardVoltages.msg.html

(cl:defclass <MotorBoardVoltages> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (motorVoltage
    :reader motorVoltage
    :initarg :motorVoltage
    :type cl:float
    :initform 0.0)
   (driverVoltage
    :reader driverVoltage
    :initarg :driverVoltage
    :type cl:float
    :initform 0.0)
   (electronicsVoltage
    :reader electronicsVoltage
    :initarg :electronicsVoltage
    :type cl:float
    :initform 0.0)
   (motorDriversEnabled
    :reader motorDriversEnabled
    :initarg :motorDriversEnabled
    :type cl:boolean
    :initform cl:nil)
   (motorPowerOk
    :reader motorPowerOk
    :initarg :motorPowerOk
    :type cl:boolean
    :initform cl:nil)
   (driverPowerOk
    :reader driverPowerOk
    :initarg :driverPowerOk
    :type cl:boolean
    :initform cl:nil)
   (electronicsPowerOk
    :reader electronicsPowerOk
    :initarg :electronicsPowerOk
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass MotorBoardVoltages (<MotorBoardVoltages>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotorBoardVoltages>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotorBoardVoltages)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<MotorBoardVoltages> is deprecated: use monarch_msgs-msg:MotorBoardVoltages instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'motorVoltage-val :lambda-list '(m))
(cl:defmethod motorVoltage-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:motorVoltage-val is deprecated.  Use monarch_msgs-msg:motorVoltage instead.")
  (motorVoltage m))

(cl:ensure-generic-function 'driverVoltage-val :lambda-list '(m))
(cl:defmethod driverVoltage-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:driverVoltage-val is deprecated.  Use monarch_msgs-msg:driverVoltage instead.")
  (driverVoltage m))

(cl:ensure-generic-function 'electronicsVoltage-val :lambda-list '(m))
(cl:defmethod electronicsVoltage-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:electronicsVoltage-val is deprecated.  Use monarch_msgs-msg:electronicsVoltage instead.")
  (electronicsVoltage m))

(cl:ensure-generic-function 'motorDriversEnabled-val :lambda-list '(m))
(cl:defmethod motorDriversEnabled-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:motorDriversEnabled-val is deprecated.  Use monarch_msgs-msg:motorDriversEnabled instead.")
  (motorDriversEnabled m))

(cl:ensure-generic-function 'motorPowerOk-val :lambda-list '(m))
(cl:defmethod motorPowerOk-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:motorPowerOk-val is deprecated.  Use monarch_msgs-msg:motorPowerOk instead.")
  (motorPowerOk m))

(cl:ensure-generic-function 'driverPowerOk-val :lambda-list '(m))
(cl:defmethod driverPowerOk-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:driverPowerOk-val is deprecated.  Use monarch_msgs-msg:driverPowerOk instead.")
  (driverPowerOk m))

(cl:ensure-generic-function 'electronicsPowerOk-val :lambda-list '(m))
(cl:defmethod electronicsPowerOk-val ((m <MotorBoardVoltages>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:electronicsPowerOk-val is deprecated.  Use monarch_msgs-msg:electronicsPowerOk instead.")
  (electronicsPowerOk m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotorBoardVoltages>) ostream)
  "Serializes a message object of type '<MotorBoardVoltages>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'motorVoltage))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'driverVoltage))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'electronicsVoltage))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'motorDriversEnabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'motorPowerOk) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'driverPowerOk) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'electronicsPowerOk) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotorBoardVoltages>) istream)
  "Deserializes a message object of type '<MotorBoardVoltages>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'motorVoltage) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'driverVoltage) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'electronicsVoltage) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'motorDriversEnabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'motorPowerOk) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'driverPowerOk) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'electronicsPowerOk) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotorBoardVoltages>)))
  "Returns string type for a message object of type '<MotorBoardVoltages>"
  "monarch_msgs/MotorBoardVoltages")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotorBoardVoltages)))
  "Returns string type for a message object of type 'MotorBoardVoltages"
  "monarch_msgs/MotorBoardVoltages")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotorBoardVoltages>)))
  "Returns md5sum for a message object of type '<MotorBoardVoltages>"
  "545bce40716a53d23dfd53844be36f09")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotorBoardVoltages)))
  "Returns md5sum for a message object of type 'MotorBoardVoltages"
  "545bce40716a53d23dfd53844be36f09")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotorBoardVoltages>)))
  "Returns full string definition for message of type '<MotorBoardVoltages>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltages of motors, drivers and electronics lines and bools for states.~%float32 motorVoltage~%float32 driverVoltage~%float32 electronicsVoltage~%bool motorDriversEnabled~%bool motorPowerOk~%bool driverPowerOk~%bool electronicsPowerOk~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotorBoardVoltages)))
  "Returns full string definition for message of type 'MotorBoardVoltages"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Values for voltages of motors, drivers and electronics lines and bools for states.~%float32 motorVoltage~%float32 driverVoltage~%float32 electronicsVoltage~%bool motorDriversEnabled~%bool motorPowerOk~%bool driverPowerOk~%bool electronicsPowerOk~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotorBoardVoltages>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotorBoardVoltages>))
  "Converts a ROS message object to a list"
  (cl:list 'MotorBoardVoltages
    (cl:cons ':header (header msg))
    (cl:cons ':motorVoltage (motorVoltage msg))
    (cl:cons ':driverVoltage (driverVoltage msg))
    (cl:cons ':electronicsVoltage (electronicsVoltage msg))
    (cl:cons ':motorDriversEnabled (motorDriversEnabled msg))
    (cl:cons ':motorPowerOk (motorPowerOk msg))
    (cl:cons ':driverPowerOk (driverPowerOk msg))
    (cl:cons ':electronicsPowerOk (electronicsPowerOk msg))
))
