; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude PersonLocalizationTrackingDataArray.msg.html

(cl:defclass <PersonLocalizationTrackingDataArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (locations
    :reader locations
    :initarg :locations
    :type (cl:vector monarch_msgs-msg:PersonLocalizationTrackingData)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:PersonLocalizationTrackingData :initial-element (cl:make-instance 'monarch_msgs-msg:PersonLocalizationTrackingData))))
)

(cl:defclass PersonLocalizationTrackingDataArray (<PersonLocalizationTrackingDataArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PersonLocalizationTrackingDataArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PersonLocalizationTrackingDataArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<PersonLocalizationTrackingDataArray> is deprecated: use monarch_msgs-msg:PersonLocalizationTrackingDataArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PersonLocalizationTrackingDataArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'locations-val :lambda-list '(m))
(cl:defmethod locations-val ((m <PersonLocalizationTrackingDataArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:locations-val is deprecated.  Use monarch_msgs-msg:locations instead.")
  (locations m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PersonLocalizationTrackingDataArray>) ostream)
  "Serializes a message object of type '<PersonLocalizationTrackingDataArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'locations))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'locations))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PersonLocalizationTrackingDataArray>) istream)
  "Deserializes a message object of type '<PersonLocalizationTrackingDataArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'locations) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'locations)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:PersonLocalizationTrackingData))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PersonLocalizationTrackingDataArray>)))
  "Returns string type for a message object of type '<PersonLocalizationTrackingDataArray>"
  "monarch_msgs/PersonLocalizationTrackingDataArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PersonLocalizationTrackingDataArray)))
  "Returns string type for a message object of type 'PersonLocalizationTrackingDataArray"
  "monarch_msgs/PersonLocalizationTrackingDataArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PersonLocalizationTrackingDataArray>)))
  "Returns md5sum for a message object of type '<PersonLocalizationTrackingDataArray>"
  "497667d68a3e2d68a8c4afb2a4d4dd8a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PersonLocalizationTrackingDataArray)))
  "Returns md5sum for a message object of type 'PersonLocalizationTrackingDataArray"
  "497667d68a3e2d68a8c4afb2a4d4dd8a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PersonLocalizationTrackingDataArray>)))
  "Returns full string definition for message of type '<PersonLocalizationTrackingDataArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of PersonLocalizationTrackingData data structure~%PersonLocalizationTrackingData[] locations~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonLocalizationTrackingData~%#use standard header~%Header header~%~%uint32 id                              # tracker id~%geometry_msgs/PoseWithCovariance pose  # tracked person estimated pose~%uint32[] groupFellows                  # Who is this person in a group with~%geometry_msgs/Point vel                # Velocity of the person in x,y,z~%~%int32 ACT_WALK=0                       # if isActive[ACT_WALK]==true, the person is detected as walking~%int32 ACT_RUN=1                        # Running~%int32 ACT_CRAFT=2                      # The person is doing handwork at a table~%int32 ACT_TV=3                         # The person is watching TV~%int32 NUM_ACT=4                        # Length of the isActive array. The idea is that code can check whether any activity is~%                                       #    being detected without updating the code as new activities are added to the list.~%bool[4] isActive                       # SIZE MUST BE UPDATED AS NUM_ACT IS UPDATED~%                                       # What activities are discovered for this person ~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PersonLocalizationTrackingDataArray)))
  "Returns full string definition for message of type 'PersonLocalizationTrackingDataArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of PersonLocalizationTrackingData data structure~%PersonLocalizationTrackingData[] locations~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/PersonLocalizationTrackingData~%#use standard header~%Header header~%~%uint32 id                              # tracker id~%geometry_msgs/PoseWithCovariance pose  # tracked person estimated pose~%uint32[] groupFellows                  # Who is this person in a group with~%geometry_msgs/Point vel                # Velocity of the person in x,y,z~%~%int32 ACT_WALK=0                       # if isActive[ACT_WALK]==true, the person is detected as walking~%int32 ACT_RUN=1                        # Running~%int32 ACT_CRAFT=2                      # The person is doing handwork at a table~%int32 ACT_TV=3                         # The person is watching TV~%int32 NUM_ACT=4                        # Length of the isActive array. The idea is that code can check whether any activity is~%                                       #    being detected without updating the code as new activities are added to the list.~%bool[4] isActive                       # SIZE MUST BE UPDATED AS NUM_ACT IS UPDATED~%                                       # What activities are discovered for this person ~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PersonLocalizationTrackingDataArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'locations) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PersonLocalizationTrackingDataArray>))
  "Converts a ROS message object to a list"
  (cl:list 'PersonLocalizationTrackingDataArray
    (cl:cons ':header (header msg))
    (cl:cons ':locations (locations msg))
))
