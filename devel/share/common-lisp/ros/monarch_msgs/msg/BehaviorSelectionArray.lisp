; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude BehaviorSelectionArray.msg.html

(cl:defclass <BehaviorSelectionArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (array
    :reader array
    :initarg :array
    :type (cl:vector monarch_msgs-msg:BehaviorSelection)
   :initform (cl:make-array 0 :element-type 'monarch_msgs-msg:BehaviorSelection :initial-element (cl:make-instance 'monarch_msgs-msg:BehaviorSelection))))
)

(cl:defclass BehaviorSelectionArray (<BehaviorSelectionArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BehaviorSelectionArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BehaviorSelectionArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<BehaviorSelectionArray> is deprecated: use monarch_msgs-msg:BehaviorSelectionArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <BehaviorSelectionArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'array-val :lambda-list '(m))
(cl:defmethod array-val ((m <BehaviorSelectionArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:array-val is deprecated.  Use monarch_msgs-msg:array instead.")
  (array m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BehaviorSelectionArray>) ostream)
  "Serializes a message object of type '<BehaviorSelectionArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'array))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'array))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BehaviorSelectionArray>) istream)
  "Deserializes a message object of type '<BehaviorSelectionArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'array) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'array)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'monarch_msgs-msg:BehaviorSelection))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BehaviorSelectionArray>)))
  "Returns string type for a message object of type '<BehaviorSelectionArray>"
  "monarch_msgs/BehaviorSelectionArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BehaviorSelectionArray)))
  "Returns string type for a message object of type 'BehaviorSelectionArray"
  "monarch_msgs/BehaviorSelectionArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BehaviorSelectionArray>)))
  "Returns md5sum for a message object of type '<BehaviorSelectionArray>"
  "b6d77918ad9ed36409ea717eb11b9a45")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BehaviorSelectionArray)))
  "Returns md5sum for a message object of type 'BehaviorSelectionArray"
  "b6d77918ad9ed36409ea717eb11b9a45")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BehaviorSelectionArray>)))
  "Returns full string definition for message of type '<BehaviorSelectionArray>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of BehaviorSelection data structure~%BehaviorSelection[] array ~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/BehaviorSelection~%string name                  # ID of behavior, as defined by the constants above~%uint32 instance_id		     # ID of specific instance of the behavior being requested~%int8[] robots                # the numbers here correspond to mbotX. At least one robot should be selected.~%KeyValuePair[] parameters    # parameters needed by the behavior, given as key/value pairs~%string[] resources           # robot resources needed for execution of behavior. Taken from list of Functionalities (see Joyful Warden Technical Script): RNAV,RNAVFC,RArmMotion,...~%bool active		     # indicates the execution/cancelation status of the request behavior (true = behavior to be execute, false = behavior to be canceled)~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BehaviorSelectionArray)))
  "Returns full string definition for message of type 'BehaviorSelectionArray"
  (cl:format cl:nil "#use standard header~%Header header~%~%#Array of BehaviorSelection data structure~%BehaviorSelection[] array ~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: monarch_msgs/BehaviorSelection~%string name                  # ID of behavior, as defined by the constants above~%uint32 instance_id		     # ID of specific instance of the behavior being requested~%int8[] robots                # the numbers here correspond to mbotX. At least one robot should be selected.~%KeyValuePair[] parameters    # parameters needed by the behavior, given as key/value pairs~%string[] resources           # robot resources needed for execution of behavior. Taken from list of Functionalities (see Joyful Warden Technical Script): RNAV,RNAVFC,RArmMotion,...~%bool active		     # indicates the execution/cancelation status of the request behavior (true = behavior to be execute, false = behavior to be canceled)~%================================================================================~%MSG: monarch_msgs/KeyValuePair~%#use standard header~%Header header~%~%#string values for key and its value~%string key~%string value~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BehaviorSelectionArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'array) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BehaviorSelectionArray>))
  "Converts a ROS message object to a list"
  (cl:list 'BehaviorSelectionArray
    (cl:cons ':header (header msg))
    (cl:cons ':array (array msg))
))
