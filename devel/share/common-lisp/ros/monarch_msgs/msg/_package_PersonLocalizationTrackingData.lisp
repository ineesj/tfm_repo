(cl:in-package monarch_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          ID-VAL
          ID
          POSE-VAL
          POSE
          GROUPFELLOWS-VAL
          GROUPFELLOWS
          VEL-VAL
          VEL
          ISACTIVE-VAL
          ISACTIVE
))