; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude FourWheelOmniDelta.msg.html

(cl:defclass <FourWheelOmniDelta> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (dtheta1
    :reader dtheta1
    :initarg :dtheta1
    :type cl:float
    :initform 0.0)
   (dtheta2
    :reader dtheta2
    :initarg :dtheta2
    :type cl:float
    :initform 0.0)
   (dtheta3
    :reader dtheta3
    :initarg :dtheta3
    :type cl:float
    :initform 0.0)
   (dtheta4
    :reader dtheta4
    :initarg :dtheta4
    :type cl:float
    :initform 0.0))
)

(cl:defclass FourWheelOmniDelta (<FourWheelOmniDelta>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FourWheelOmniDelta>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FourWheelOmniDelta)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<FourWheelOmniDelta> is deprecated: use monarch_msgs-msg:FourWheelOmniDelta instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <FourWheelOmniDelta>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'dtheta1-val :lambda-list '(m))
(cl:defmethod dtheta1-val ((m <FourWheelOmniDelta>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:dtheta1-val is deprecated.  Use monarch_msgs-msg:dtheta1 instead.")
  (dtheta1 m))

(cl:ensure-generic-function 'dtheta2-val :lambda-list '(m))
(cl:defmethod dtheta2-val ((m <FourWheelOmniDelta>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:dtheta2-val is deprecated.  Use monarch_msgs-msg:dtheta2 instead.")
  (dtheta2 m))

(cl:ensure-generic-function 'dtheta3-val :lambda-list '(m))
(cl:defmethod dtheta3-val ((m <FourWheelOmniDelta>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:dtheta3-val is deprecated.  Use monarch_msgs-msg:dtheta3 instead.")
  (dtheta3 m))

(cl:ensure-generic-function 'dtheta4-val :lambda-list '(m))
(cl:defmethod dtheta4-val ((m <FourWheelOmniDelta>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:dtheta4-val is deprecated.  Use monarch_msgs-msg:dtheta4 instead.")
  (dtheta4 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FourWheelOmniDelta>) ostream)
  "Serializes a message object of type '<FourWheelOmniDelta>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'dtheta1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'dtheta2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'dtheta3))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'dtheta4))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FourWheelOmniDelta>) istream)
  "Deserializes a message object of type '<FourWheelOmniDelta>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dtheta1) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dtheta2) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dtheta3) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dtheta4) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FourWheelOmniDelta>)))
  "Returns string type for a message object of type '<FourWheelOmniDelta>"
  "monarch_msgs/FourWheelOmniDelta")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FourWheelOmniDelta)))
  "Returns string type for a message object of type 'FourWheelOmniDelta"
  "monarch_msgs/FourWheelOmniDelta")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FourWheelOmniDelta>)))
  "Returns md5sum for a message object of type '<FourWheelOmniDelta>"
  "a5f96c6877c04ae84a9e783ca09b7b3b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FourWheelOmniDelta)))
  "Returns md5sum for a message object of type 'FourWheelOmniDelta"
  "a5f96c6877c04ae84a9e783ca09b7b3b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FourWheelOmniDelta>)))
  "Returns full string definition for message of type '<FourWheelOmniDelta>"
  (cl:format cl:nil "#use standard header~%Header header~%~%#one float for each wheel. measurements in radians~%float64 dtheta1~%float64 dtheta2~%float64 dtheta3~%float64 dtheta4~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FourWheelOmniDelta)))
  "Returns full string definition for message of type 'FourWheelOmniDelta"
  (cl:format cl:nil "#use standard header~%Header header~%~%#one float for each wheel. measurements in radians~%float64 dtheta1~%float64 dtheta2~%float64 dtheta3~%float64 dtheta4~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FourWheelOmniDelta>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FourWheelOmniDelta>))
  "Converts a ROS message object to a list"
  (cl:list 'FourWheelOmniDelta
    (cl:cons ':header (header msg))
    (cl:cons ':dtheta1 (dtheta1 msg))
    (cl:cons ':dtheta2 (dtheta2 msg))
    (cl:cons ':dtheta3 (dtheta3 msg))
    (cl:cons ':dtheta4 (dtheta4 msg))
))
