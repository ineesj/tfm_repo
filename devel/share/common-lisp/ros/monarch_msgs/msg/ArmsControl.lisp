; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude ArmsControl.msg.html

(cl:defclass <ArmsControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (torque_enable
    :reader torque_enable
    :initarg :torque_enable
    :type cl:boolean
    :initform cl:nil)
   (left_arm
    :reader left_arm
    :initarg :left_arm
    :type cl:float
    :initform 0.0)
   (right_arm
    :reader right_arm
    :initarg :right_arm
    :type cl:float
    :initform 0.0)
   (movement_time
    :reader movement_time
    :initarg :movement_time
    :type cl:float
    :initform 0.0))
)

(cl:defclass ArmsControl (<ArmsControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ArmsControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ArmsControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<ArmsControl> is deprecated: use monarch_msgs-msg:ArmsControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ArmsControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'torque_enable-val :lambda-list '(m))
(cl:defmethod torque_enable-val ((m <ArmsControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:torque_enable-val is deprecated.  Use monarch_msgs-msg:torque_enable instead.")
  (torque_enable m))

(cl:ensure-generic-function 'left_arm-val :lambda-list '(m))
(cl:defmethod left_arm-val ((m <ArmsControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:left_arm-val is deprecated.  Use monarch_msgs-msg:left_arm instead.")
  (left_arm m))

(cl:ensure-generic-function 'right_arm-val :lambda-list '(m))
(cl:defmethod right_arm-val ((m <ArmsControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:right_arm-val is deprecated.  Use monarch_msgs-msg:right_arm instead.")
  (right_arm m))

(cl:ensure-generic-function 'movement_time-val :lambda-list '(m))
(cl:defmethod movement_time-val ((m <ArmsControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:movement_time-val is deprecated.  Use monarch_msgs-msg:movement_time instead.")
  (movement_time m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ArmsControl>) ostream)
  "Serializes a message object of type '<ArmsControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'torque_enable) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'left_arm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'right_arm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'movement_time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ArmsControl>) istream)
  "Deserializes a message object of type '<ArmsControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'torque_enable) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'left_arm) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'right_arm) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'movement_time) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ArmsControl>)))
  "Returns string type for a message object of type '<ArmsControl>"
  "monarch_msgs/ArmsControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ArmsControl)))
  "Returns string type for a message object of type 'ArmsControl"
  "monarch_msgs/ArmsControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ArmsControl>)))
  "Returns md5sum for a message object of type '<ArmsControl>"
  "dde05d3755c32d4861fd095e6003c4d7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ArmsControl)))
  "Returns md5sum for a message object of type 'ArmsControl"
  "dde05d3755c32d4861fd095e6003c4d7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ArmsControl>)))
  "Returns full string definition for message of type '<ArmsControl>"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool torque_enable     # indicates if torque is enabled or not~%float32 left_arm       # angle in radians, 0 is pointing down, positive rotates end of arm backwards~%float32 right_arm      # angle in radians, 0 is pointing down, positive rotates end of arm backwards~%float32 movement_time  # number of seconds until movement is complete~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ArmsControl)))
  "Returns full string definition for message of type 'ArmsControl"
  (cl:format cl:nil "#use standard header~%Header header~%~%bool torque_enable     # indicates if torque is enabled or not~%float32 left_arm       # angle in radians, 0 is pointing down, positive rotates end of arm backwards~%float32 right_arm      # angle in radians, 0 is pointing down, positive rotates end of arm backwards~%float32 movement_time  # number of seconds until movement is complete~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ArmsControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ArmsControl>))
  "Converts a ROS message object to a list"
  (cl:list 'ArmsControl
    (cl:cons ':header (header msg))
    (cl:cons ':torque_enable (torque_enable msg))
    (cl:cons ':left_arm (left_arm msg))
    (cl:cons ':right_arm (right_arm msg))
    (cl:cons ':movement_time (movement_time msg))
))
