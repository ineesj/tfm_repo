; Auto-generated. Do not edit!


(cl:in-package monarch_msgs-msg)


;//! \htmlinclude PersonFrontofRobot.msg.html

(cl:defclass <PersonFrontofRobot> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (id
    :reader id
    :initarg :id
    :type cl:integer
    :initform 0)
   (person_orientation
    :reader person_orientation
    :initarg :person_orientation
    :type cl:float
    :initform 0.0)
   (direction_to_person
    :reader direction_to_person
    :initarg :direction_to_person
    :type cl:float
    :initform 0.0)
   (distance
    :reader distance
    :initarg :distance
    :type cl:float
    :initform 0.0))
)

(cl:defclass PersonFrontofRobot (<PersonFrontofRobot>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PersonFrontofRobot>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PersonFrontofRobot)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name monarch_msgs-msg:<PersonFrontofRobot> is deprecated: use monarch_msgs-msg:PersonFrontofRobot instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PersonFrontofRobot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:header-val is deprecated.  Use monarch_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <PersonFrontofRobot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:id-val is deprecated.  Use monarch_msgs-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'person_orientation-val :lambda-list '(m))
(cl:defmethod person_orientation-val ((m <PersonFrontofRobot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:person_orientation-val is deprecated.  Use monarch_msgs-msg:person_orientation instead.")
  (person_orientation m))

(cl:ensure-generic-function 'direction_to_person-val :lambda-list '(m))
(cl:defmethod direction_to_person-val ((m <PersonFrontofRobot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:direction_to_person-val is deprecated.  Use monarch_msgs-msg:direction_to_person instead.")
  (direction_to_person m))

(cl:ensure-generic-function 'distance-val :lambda-list '(m))
(cl:defmethod distance-val ((m <PersonFrontofRobot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader monarch_msgs-msg:distance-val is deprecated.  Use monarch_msgs-msg:distance instead.")
  (distance m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PersonFrontofRobot>) ostream)
  "Serializes a message object of type '<PersonFrontofRobot>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'person_orientation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'direction_to_person))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PersonFrontofRobot>) istream)
  "Deserializes a message object of type '<PersonFrontofRobot>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'person_orientation) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'direction_to_person) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'distance) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PersonFrontofRobot>)))
  "Returns string type for a message object of type '<PersonFrontofRobot>"
  "monarch_msgs/PersonFrontofRobot")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PersonFrontofRobot)))
  "Returns string type for a message object of type 'PersonFrontofRobot"
  "monarch_msgs/PersonFrontofRobot")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PersonFrontofRobot>)))
  "Returns md5sum for a message object of type '<PersonFrontofRobot>"
  "d4293743d7094413aadc57f7d038281c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PersonFrontofRobot)))
  "Returns md5sum for a message object of type 'PersonFrontofRobot"
  "d4293743d7094413aadc57f7d038281c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PersonFrontofRobot>)))
  "Returns full string definition for message of type '<PersonFrontofRobot>"
  (cl:format cl:nil "#use standard header~%Header header~%~%int32 id                     # id of person in front of robot. For now, this is not linked to the ids assigned by the overhead cameras~%float32 person_orientation    # relative orientation of the person with respect to robot (0 degrees if person looking at robot, 180 degrees if facing away)~%float32 direction_to_person   # relative orientation of the person with respect to robot (0 degrees if straight in front of the head of the robot).~%float32 distance              # relative distance from the robot to the person~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PersonFrontofRobot)))
  "Returns full string definition for message of type 'PersonFrontofRobot"
  (cl:format cl:nil "#use standard header~%Header header~%~%int32 id                     # id of person in front of robot. For now, this is not linked to the ids assigned by the overhead cameras~%float32 person_orientation    # relative orientation of the person with respect to robot (0 degrees if person looking at robot, 180 degrees if facing away)~%float32 direction_to_person   # relative orientation of the person with respect to robot (0 degrees if straight in front of the head of the robot).~%float32 distance              # relative distance from the robot to the person~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PersonFrontofRobot>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PersonFrontofRobot>))
  "Converts a ROS message object to a list"
  (cl:list 'PersonFrontofRobot
    (cl:cons ':header (header msg))
    (cl:cons ':id (id msg))
    (cl:cons ':person_orientation (person_orientation msg))
    (cl:cons ':direction_to_person (direction_to_person msg))
    (cl:cons ':distance (distance msg))
))
