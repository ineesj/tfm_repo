#!/bin/bash

unset ROS_NAMESPACE
unset ROS_IP
# export ROS_MASTER_URI=http://localhost:11311
export ROS_MASTER_URI=http://localhost:11311

temp=$(rostopic list)

if [ $temp = '']; then
roscore &
PID_core=$!
fi

sleep 4

rosparam load watchdog_UC3M.xml

export ROS_NAMESPACE=$(rosparam get /wd/Namespace)

export ROS_MASTER_URI='http://'$(rosparam get /wd/IP_robot)':11311/'

rosparam load watchdog_UC3M.xml

IP_value=$(/sbin/ifconfig eth1 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}$')
export ROS_IP=$IP_value

roslaunch wd_server_launch.launch

kill -2 $PID_core
