#!/bin/bash

export ROS_NAMESPACE=mbot01
rosparam load watchdog_IST.xml

roslaunch wd_launch.launch &
PID_1=$!


trap ctrl_c INT

function ctrl_c() {
        echo "** Trapped CTRL-C"
        kill -2 $PID_1
}

