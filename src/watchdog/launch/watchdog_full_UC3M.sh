#!/bin/bash

export ROS_NAMESPACE=mbot10
rosparam load watchdog_UC3M.xml

roslaunch wd_launch.launch &
PID_1=$!


trap ctrl_c INT

function ctrl_c() {
        echo "** Trapped CTRL-C"
        kill -2 $PID_1
}

