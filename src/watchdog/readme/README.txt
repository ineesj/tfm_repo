Package built by IST - João Mendes (mendes.joao.p@gmail.com)


GENERAL DESCRIPTION

This watchdog was built to control some variables and situations that might occour during autonomous trials at IPOL and report if something unexpected happens. The base idea is to report (via email) to list of people if something strange happens that might require remote or on site support. The watchdog consists in subscribing to some specific topics (bateries for example) and produce an error message if some conditions are met.

If you are only interested in launching the watchdog go to the "HOW TO LAUNCH THE WATCHDOG AS STAND ALONE" part of the readme.

Be aware from this point on, that the watchdog is only ready to work in a ROBOT+SERVER (2 different machines). No more robots are, for now, supported.

LIST OF FILES:

XML: This XML contains the threshold definition for all the parameters. All the magic values are defined here. The namespace of the robot where the watchdog is supposed to run is also set here as well as the IPs of the robot+server ont he environment (different XMLs for IST and IPOL are provided)

send_mail.py: The script responsible for sending the mails. On line 39 is defined the list of emails that will receive notifications.

src: the src file names' are self-explanatory

*.launch: Files to launch the watchdog WITHOUT NEEDED VARIABLES! Do not use explicitly without knowing what you are doing

*.sh: Bash script used to set-up the environment and launch the watchdog.



SERVER VS ROBOT

The watchdog as presented in the diagram is separated in two parts. One part is running on the robot and the other on the server. The key ideia of this approach is to decouple the report of the problem from the action to take. The best example is if the robot loses network connection to the server. With our approach, the server-part of the watchdog will detect and send an email reporting the situation while the robot is also able to detect the problem and, if instructed to, act accordingly. 

Robot side: Detection of the problems and, if implemented in the behaviour, action.
Server side: Detection of different problems and report what was identified by the robot.


-------------    /mbotXX/error    --------------
|	Robot	| <--------------->   |  Server    |       Email
|			|					  |			   |  ---------------->
|			|   /mbotXX/state     |			   |
|			| ---------------->   |			   |
-------------                     --------------

The /mbotXX/error is considered to be directional as one can remotely enter the server and send an error message (correspondant to the stop behaviour). Further explained






HOW TO LAUNCH THE WATCHDOG AS STAND ALONE

A one click bash script is provided both for the robot and for the server side. This script is in charge of setting up the environment, load the XML and launch the different components of the watchdog. It is considered that the XML is coherent with the robot+IP used (True for IST and IPOL). The watchdog is standalone and keeps running until killed (ctrl+c on the terminal. Bash script handles signal and kills everything). IT is advised to launch using the screen function. 

While the watchdog on the server is capable of running without known-issues continuously and regardless of the robot being used, the watchdog on the robot should be re-inicialized before each test. This is crutial IF AND ONLY IF you are using the watchdog to evaluate the state of the robot (check further). If you are only using it to keep track of the robot's variables, the software should endure not being restarted.

On the server (IST):

roscd watchdog/launch
watchdog_server_full.sh

On the robot (IST):

roscd watchdog/launch
watchdog_full.sh


On the server (IPOL):

roscd watchdog/launch
watchdog_server_full_IPOL.sh

On the robot (IPOL):

roscd watchdog/launch
watchdog_full_IPOL.sh

If errors appear check the next point in this tutorial.

OUTPUT:

The "physical" output of the program is already described. The scripts will output a message to the shell (both on the robot and on the server side) everytime an error occours. The server will send an email everythime the shell outputs "First time error X".

Both the server and the robot side will output a ROS_ERROR everytime the watchdog detects an event. The output of those ROS_ERRORS are normal. On the robot side you will be able to see the error detected (Explicitly) while on the server side you'll see the number of the error detected and, as explained, if an email will be sent or not.

Obviously, if no ROS_ERROR is outputed no event was detected.


NODES AND TOPICS:

The name of the nodes are self-explanatory (wd_*type*)

The topics usd are:

mbotxx/error: To publish errors 
/sar/mcentral/error_found: To publish errors (to use in a SAM context. On going work)
mbotxx/error: To subscribe to the state of the robot


EXAMPLE OF HOW TO USE IN A BEHAVIOUR

In order to test the watchdog a simple patrolling state machine was built. The robot undocks and starts a patrolling behaviour. This patrol behaviour is stopped when the watchdog reports an error (see bellow). After receiving the error, the robot will go to the dock station and attempt to dock.

This SM might be used as an example of coordination between a behaviour and the watchdog. The watchdog reports the error and the behaviour takes the appropriate action. Most of the code is self-explanatory. Notice that the SM is ready to receive the error of both the lasers failling and perform an emergency stop.


ERRORS IDENTIFIED BY THE ROBOT:

As described above, the watchdog keeps track of some data present in the robot. Given that data, and in the case of some inconsistency/lack of information/error, it will issue a message in the mbotxx/error topic. This message, as explained before, will be interpreted by the server part.

The errors detected by the robot are the following: (And associated number error)

 1 - Motors button pushed (OFF). Error sent after certain time
 2 - Laser readings missing. One of the LRF might be compromised. 
 3 - Robot lost. Not yet implemented
 4 - Batery bellow a certain threshold. This value is supposed to be higher than the shutdown value. This way it is possible to prevent the robot from dying.
 5 - A certain amount of ping signals lost or the mean ping is too high
 6 - Bumpers activated
 7 - The robot should be moving but it is not. This error is only raised if the robot is recognized to be in a patroling/go_to state. Check below.
 8 - Should have docked but failed. As in the previous case, if the robot is in a trying_to_dock state this error is raised if it failed.
 9 - Should have undocked but failed. Unlike the previous, this error is issued if the robot should be moving but is still in the docking station. If this happens, it means the robot failed to undock. The trigger is, once again, the comutation between states.
 10 - Both lasers failed! The robot will be fully stopped! Still under testing. The idea behind it is to fully stop the robot if he stops receiving laser data from BOTH LRFs. 

*EXTRA VALUES:*

This topic is also used to pass more information. This was particularly usefull in the testing of the watchdog. The "error" number is the following:

0 - Send the robot to the dock station (MUST be implemented in your own state machine). Stops the behaviour and docks
99 - Robot docked.


*STATES:*

The watchdog also interprets states. If you want the watchdog to inform/keep track of the state of the robot publish a integer in mbotXX/state. For now, this is mostly used as a report tool. A constant check on the actions of the robot is also possible as it is implemented for state 1.

The states implemented are:

# - state
1 - Moving. The robot in the state should be moving. Another part of the watchdog will recognize this state and check if the robot is indeed moving. Important for patrolling/move_to actions. 

2 - Trying to dock. As in the previous case

3 - Robot fully stopped due to a major problem.


SERVER SIDE:

The server part of the server identifies by itself the following problems:

 - Ping: If the connection is lost (the server won't be able to identify the errors from the robot therefore such identification is needed on the server)
 - ROS_liveness: If the roscore of the robot dies

The server will also perform a simple evaluation of the errors received and will only parse some information to the email. The commited version does not make any distinction between the weight of the errors but it is prepared to do so. It can take different actions according to the seriousness of the error.

It also has a time decay based approach where if an error is not seen during X seconds the watchdog, from that point on, will act as it never received the error. If the same error appears before the "timeout" occours, the time counter will reset and the error will NOT be emailed. IF after the "timeout" the error reapears, the watchdog will act as a new error and will report by email.




MAGIC VARIABLES:

IP_server:  IP of the mpc (distinguished from IPOL AND IST)
IP_robot: IP of the robot where the watchdog will run (mbot01 on IST and mbot09 on IPOL)

Namespace: Namespace of the robot where the watchdog will run (mbot01 on IST and mbot09 on IPOL). This should be changed in the future so that it is not necessary of automatically aquired


Size_window: Size of the pseudo-sliding window used to compute the mean of the pings
TH_mean_ping: Threshold for the allowed mean of pings
TH_ping_failed: Threshold for the allowed failed pings


TH_button_off: Threshold for the allowed time for the button to be off      
TH_batteries: Threshold for the allowed battery levels
TH_time_stopped: Threshold for the allowed time for the button to be stopped if it whoud be moving         
TH_scan: Threshold for the allowed laser readings to fail. THis number hsould be high as the topic is at 10Hz


TO DO:

These are the suggested next steps for developing the watchdog:

- Test the "both lasers failling" scenario. It was found to be unreliable (outputing false positives) and it is commented on the code.
- The need of having the namespace on the xml is a hard constraint and somehow redundant. This variable should be aquired by other methods
- SAM integrations needs further testing
- Multiple robots 
- Better parsing and readability of the errors. Mostly focused on the email sent. 