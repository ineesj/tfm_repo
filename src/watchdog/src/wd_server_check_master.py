#!/usr/bin/env python

# Watchdog to run on the robot

import rospy, rospkg
import rosgraph.masterapi
import time
import datetime
import os

report = False

print "CHECK_CORE process starting"
master = rosgraph.masterapi.Master('/rostopic') 

while 1:
	rospy.logerr(master.is_online())
	if not master.is_online():   # Caso em que o core morre
        # dead_core()
		if report is False:
			message = str(datetime.datetime.now())  + 'ROSCORE OR ROBOT DEAD!'
			print "fcked up!"

			tmp2=rospkg.RosPack()
			tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
			os.system(tmp)
			report = True

	else:
		if report is True:
			rospy.logerr('Roscore came back')
			print "BACK!"
			message = str(datetime.datetime.now())  + 'ROSCORE CAME BACK!!'

			tmp2=rospkg.RosPack()
			tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
			os.system(tmp)
			report = False

		print "Still alive.sleeping"
		time.sleep(8)

