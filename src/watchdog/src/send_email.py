#!/usr/bin/env python

# Watchdog to run on the server

import rospy
import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import time
import datetime
from threading import Thread, Lock
import rosgraph.masterapi
import multiprocessing
import subprocess

mutex = Lock()

message = ' error list: \n 1- button pushed \n 2- Laser readings missing \n 3 - robot lost\n 4 -  Bateries bellow TH\n 5- Ping lost or too high\n 6- Bump\n 7 - Should be moving but it isnt\n 8- should have docked but failed\n 9- shuld have undocked but failed \n 10 - both lasers failed! robot stoped!'

message += ' If you want the robot to dock: enter the server and publish 1 in the error topic'


if len(sys.argv) is 1:
    print "Something awfully wrong. 1 str argument (message to send)"
    # rospy.logerr("Something awfully wrong. 1 argument (message to send)")
    sys.exit()

aux = ' '.join(sys.argv[1:])
# print aux

server = smtplib.SMTP('smtp.gmail.com:587')
server.ehlo()
server.starttls()
server.login("mbot.ipo.monarch", "hominibus")


fromaddr = "mbot.ipo.monarch@gmail.com"
#toaddr = ['mendes.joao.p@gmail.com', 'joao.s.sequeira@gmail.com']
nspace = rospy.get_namespace()
toaddr = rospy.get_param(nspace + 'wd/' + 'dest_emails')

msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = ", ".join(toaddr)
msg['Subject'] = "Rescue me!"

# parte que faz coisas

message += ' \n\n' + aux

msg.attach(MIMEText(message, 'plain'))

text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
