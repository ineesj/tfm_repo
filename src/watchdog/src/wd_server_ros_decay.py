#!/usr/bin/env python

# Watchdog to run on the server

import rospy, rospkg
from std_msgs.msg import UInt64
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os, time, datetime
from threading import Thread, Lock
import rosgraph.masterapi
import multiprocessing
import subprocess

mutex=Lock()

global message
message=''

TH_decay= rospy.get_param("wd/TH_decay")
IP_robot= rospy.get_param("wd/IP_robot")
Size_window= rospy.get_param("wd/Size_window")
TH_mean_ping= rospy.get_param("wd/TH_mean_ping")
TH_ping_failed= rospy.get_param("wd/TH_ping_failed")

vec_critical_messages=[1,2,3,4,5,10]
vec_req_check_messages=[6,7,8,9]

global error_vec
error_vec=[0]*(len(vec_req_check_messages) + len(vec_critical_messages))

# callback that interprets errors sent by the robot
def cb_error_interpret(data):
	global message, error_vec

	now = rospy.get_rostime()

	mutex.acquire(1)
	message+='  '+ str(datetime.datetime.now())  +' ROSTIME secs =' +str(now.secs)+'  nsecs='+str(now.nsecs) +' error number '+ str(data)
	mutex.release()

	aux='error message received'+str(data.data)
	rospy.logerr(aux)

	if data.data == 99:    # If the code is 99 the robot has succesfully docked
			rospy.logerr("DOCKED")
			tmp3 = 'Now Im docked'
			tmp2=rospkg.RosPack()
			tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ tmp3
			os.system(tmp)

	elif data.data==0:    # If the code is 0 the robot has been sent to dock
		mutex.acquire(1)
		message+='Message to dock sent '
		mutex.release()

	else:     # Error

		if error_vec[data.data-1] is 0:   
		# First time the error occours
			rospy.logerr("Fist time error "+ str(data.data))

			if data.data in vec_critical_messages:
				# rospy.logerr("critical message")

				tmp2=rospkg.RosPack()
				tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
				os.system(tmp)

			elif data.data in vec_req_check_messages:
				# rospy.logerr("NON-critical message")

				tmp2=rospkg.RosPack()
				tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
				os.system(tmp)
		error_vec[data.data-1] = time.time()



# callback that reports status sent by the robot

def cb_state_interpret(data):
	global message

	now = rospy.get_rostime()
	mutex.acquire(1)

	if data.data==1:
		message+='  '+ str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs) +' STATUS UNDOCKED!'
		mutex.release()
		rospy.logerr("STATUS UNDOCKED Finished")

	elif data.data==2:
		message+='  '+ str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs) +' STATUS DOCK! Check if docked was sucessfull  '
		mutex.release()
		rospy.logerr("STATUS DOCKED Finished")

		tmp2=rospkg.RosPack()
		tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
		os.system(tmp)
		message=''       # Cleans message after each run

	else:
		rospy.logerr("I don't think I should be here...Status code not adressed")

		message+='  '+ str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs) +' ON A STRANGE STATUS! '
		mutex.release()

		tmp2=rospkg.RosPack()
		tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
		os.system(tmp)


if __name__ == '__main__':

	global error_vec

	rospy.init_node('cerberus_server', anonymous=True)     

	rospy.Subscriber("error", UInt64, cb_error_interpret)
	rospy.Subscriber("state", UInt64, cb_state_interpret)

	rate=rospy.Rate(1)
	while not rospy.is_shutdown(): 
		# Decay part. Error "disapears" after TH_decay
		aux = [error_vec[x]-time.time()+TH_decay if error_vec[x] != 0 else error_vec[x] for x in xrange(len(error_vec))]
		error_vec = [0 if aux[x] < 0 else error_vec[x] for x in xrange(len(error_vec))]		

		rate.sleep()
	rospy.spin()
	
 