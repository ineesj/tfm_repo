#!/usr/bin/env python

# Watchdog to run on the robot
# Check README for more info

import rospy
from std_msgs.msg import String, UInt64
from sensor_msgs.msg import LaserScan
from monarch_msgs.msg import MotorBoardVoltages, BatteriesVoltage, BumpersReadings, AuxiliaryBatteriesVoltage
from geometry_msgs.msg import PoseWithCovarianceStamped
import rostopic
import rosgraph.masterapi 
import time
import os
from threading import Thread, Lock


TH_button_off = rospy.get_param("wd/TH_button_off")
TH_batteries = rospy.get_param("wd/TH_batteries")
TH_time_stopped = rospy.get_param("wd/TH_time_stopped")
TH_scan = rospy.get_param("wd/TH_scan")
Namespace = rospy.get_param("wd/Namespace")

global counter_button_off
global sub_motor
global start_time
global pub_error
global prev_time
global counter_bat_ele
global counter_bat_pc
global moving

counter_bat_ele=0
counter_bat_pc=0
start_time=0
counter_button_off=0
prev_id=0
counter_scan=0
prev_time=0
moving = False

mutex=Lock()


# CALLBACKS USED BY THE SUBSCRIBERS

def cb_button(data):           
    global counter_button_off

    if data.motorPowerOk == False and data.electronicsVoltage<16:  #DOES NOT DETECT IF THE BUTTON IS OFF ON THE DOCK
        counter_button_off+=1

        if counter_button_off>TH_button_off:  # Off for more than X secs
        	pub_error.publish(1)
        	pub_error_sam.publish(1)

        	rospy.logerr("Button pushed!")
        	time.sleep(0.5)
    else:
        counter_button_off=0



def cb_batteries_eletronics(data):           
	global counter_bat_ele

	if data.electronics<TH_batteries:
		counter_bat_ele+=1
		if counter_bat_ele>10: # hack to avoid situation when the robot is leaving the dock and gives false positive
			pub_error.publish(4)
			pub_error_sam.publish(4)
			rospy.logerr("Batteries going down!! (electronics)")

			time.sleep(0.5)



def cb_batteries_pcs(data):            
	global counter_bat_pc

	if (data.pc1<TH_batteries and not data.pc1 == 0) or (data.pc2<TH_batteries and not data.pc2 == 0):
		counter_bat_pc+=1
		if counter_bat_pc>10:   # hack to avoid situation when the robot is leaving the dock and gives false positive
			rospy.logerr("Batteries going down!! (PC)")
			pub_error.publish(4)
			pub_error_sam.publish(4)

			time.sleep(0.5)
			

def cb_bumpers(data):            

	if data.leftFrontBump==True or data.rightFrontBump==True or data.leftRearBump==True or data.rightRearBump==True:
		rospy.logerr("Bumped!")
		pub_error.publish(6)
		pub_error_sam.publish(6)



def cb_scan(data):  
	global counter_scan    
	global prev_id
	global prev_time


	if data.header.frame_id==prev_id:
		counter_scan+=1
		if counter_scan>TH_scan:
			rospy.logerr("laser might is probably compromised. Sending error")
			pub_error.publish(2)
			pub_error_sam.publish(2)

			time.sleep(0.5)
			
	else:
		counter_scan=0
		prev_id=data.header.frame_id
		prev_time=rospy.get_rostime().secs



def cb_state_check(data):
	global moving, sub_motor

	# data=1: should be undocked;
	# data=2: should be docked

	if data.data==1:
		time.sleep(5) #HACK!! to avoid locks! and false info
		sub_motor = rospy.Subscriber("batteries_voltage", BatteriesVoltage, check_motors_board_once)
		time.sleep(2)    #HACK!! to avoid locks!
		if inf_dock==1:

			rospy.logerr("error 9. Failed undock")
			pub_error.publish(9)
			pub_error_sam.publish(9)

			time.sleep(1)
		else:
			moving = True
			rospy.loginfo("Undock SUCCEDDED")


	elif data.data==2:
		moving = False

		sub_motor = rospy.Subscriber("batteries_voltage", BatteriesVoltage, check_motors_board_once)
		time.sleep(5)  #HACK!! to avoid locks! and false info

		if inf_dock==0:
			pub_error.publish(8)
			pub_error_sam.publish(8)

			rospy.logerr("Dock failed")
			time.sleep(1)

		else:
			rospy.loginfo("Dock SUCCEDDED")
			pub_error.publish(99)
			pub_error_sam.publish(99)
			time.sleep(1)



def check_motors_board_once(data):  # Used to aquire only one measure from the motors board
	global inf_dock

	if data.charger>0:
		inf_dock=1      # its docked
	else:
		inf_dock=0      # its undocked
	sub_motor.unregister()


def check_movement_cb(data):
	global start_time
	mutex.acquire(1)
	start_time=time.time()
	mutex.release()


if __name__ == '__main__':

	rospy.init_node('cerberus_ros', anonymous=True)     

	global pub_error, pub_error_sam
	pub_error = rospy.Publisher('error', UInt64, queue_size=10, latch=True)

	topic_aux='/sar/'+Namespace+'/error_found'
	pub_error_sam = rospy.Publisher(topic_aux, UInt64, queue_size=10, latch=True)

	rospy.Subscriber("motor_board_voltages",MotorBoardVoltages, cb_button)
	rospy.Subscriber("batteries_voltage",BatteriesVoltage, cb_batteries_eletronics)
	rospy.Subscriber("auxiliary_batteries_voltage",AuxiliaryBatteriesVoltage, cb_batteries_pcs)
	rospy.Subscriber("bumpers",BumpersReadings, cb_bumpers)
	rospy.Subscriber("scan", LaserScan, cb_scan)
	rospy.Subscriber("state", UInt64, cb_state_check)
	rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, check_movement_cb)


	r = rospy.Rate(1) 
	while not rospy.is_shutdown():

		# Checks if robot is moving when it should
		if moving is True:
			mutex.acquire(1)
			time_passed=time.time()-start_time
			mutex.release()

			if time_passed>TH_time_stopped and start_time is not 0:
				rospy.logerr("Should be moving but it is NOT")
				pub_error.publish(7)
				pub_error_sam.publish(7)



		# Checks both lasers
		#
		#	NOT WORKING!
		#
		# if rospy.get_rostime().secs-prev_time > 5 and prev_time is not 0:
		# 	print "2 LASERS COMPROMISED"
		# 	rospy.logerr("2 LASER COMPROMISED")
 
		# 	pub_error.publish(10)
		# 	pub_error_sam.publish(10)

		# 	# NOT TESTED!

		r.sleep()

	rospy.spin()


