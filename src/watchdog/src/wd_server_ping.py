#!/usr/bin/env python

# Watchdog to run on the server

import rospy
from std_msgs.msg import UInt64
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os, time, datetime
from threading import Thread, Lock
import rosgraph.masterapi
import multiprocessing
import subprocess
import rospkg
import sys, signal

IP_robot= rospy.get_param("wd/IP_robot")
Size_window= rospy.get_param("wd/Size_window")
TH_mean_ping= rospy.get_param("wd/TH_mean_ping")
TH_ping_failed= rospy.get_param("wd/TH_ping_failed")

# Variable to control if an email has been sent 
global report
report=False

def check_ping():
	global report
	counter=0
	aux_vec=[]

	while 1:

		proc = subprocess.Popen(['ping -c 1 '+ IP_robot+' | grep rtt | cut -d "/" -f5'], stdout=subprocess.PIPE, shell=True)
		(aux, err) = proc.communicate()

		if aux=='':     # ping failed!
			counter+=1
			if counter>TH_ping_failed and report is False:

				rospy.logerr("Robot not reachable..")
	
				now = rospy.get_rostime()
	
				message = str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs)
				message += " Ping sequence failed " + str(counter) + " times" + " Robot not reachable!!!"
	
				tmp2=rospkg.RosPack()
				tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
				os.system(tmp)
	
				report=True   

		else:
			if report is True:  # Ping is back!
				rospy.logerr("Robot returned!")
				message = str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs)
				message += " Robot returned to the network!"
				
				tmp2=rospkg.RosPack()
				tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
				os.system(tmp)
				report = False


			aux_vec.append(float(aux))
			counter=0

		if len(aux_vec)==Size_window and report is False:    #Computes and checks mean
			mean_ping=sum(aux_vec)/Size_window

			if mean_ping>TH_mean_ping:

					now = rospy.get_rostime()

					message = str(datetime.datetime.now())  +' ROS TIME:secs =' +str(now.secs)+'  nsecs='+str(now.nsecs)
					message += " Ping mean too high " + str(counter) + " times" + " SM may be stopping"
					
					tmp2=rospkg.RosPack()
					tmp='python '+tmp2.get_path('watchdog')+'/src/send_email.py '+ message
					os.system(tmp)
					
			aux_vec=[]


def signal_handler(signal, frame):
	sys.exit(0)

if __name__ == '__main__':

	rospy.init_node('cerberus_server_ping', anonymous=True)     

	signal.signal(signal.SIGINT, signal_handler)

	check_ping()
