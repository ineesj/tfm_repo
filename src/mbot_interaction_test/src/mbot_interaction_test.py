#!/usr/bin/env python
import rospy
from monarch_msgs.msg import EmotionExpression, NonVerbalSound, Utterance
from geometry_msgs.msg import Twist
from math import sin, pi, floor, cos
import time

def mbot_interaction_test():
  rospy.init_node('mbot_actuators_test');
  r = rospy.Rate(0.2) # 0.2 hz
  while not rospy.is_shutdown():
    publishEmotion('normal')
    publishSaySentence("Emoção normal.")
    r.sleep()
    publishEmotion('happy')
    publishSaySentence("Emoção feliz.")
    publishNonVerbalSound()
    r.sleep()
    publishEmotion('surprise')
    publishSaySentence("Emoção surpreso.")
    r.sleep()
    publishNonVerbalSound()
    r.sleep()



def publishEmotion(emotion):
  emotionPub = rospy.Publisher('express_emotion', EmotionExpression, queue_size=10)
  ee = EmotionExpression()
  ee.emotion = emotion
  ee.arms = True
  ee.head = True
  ee.mouth = True
  ee.eyes = True
  ee.audio = True
  ee.image = True
  ee.ar = True
  emotionPub.publish(ee)

def publishNonVerbalSound():
  nvsoundPub = rospy.Publisher('play_sound', NonVerbalSound, queue_size=10)
  nvs = NonVerbalSound()
  nvs.soundname = 'r2d2'
  nvs.action = 'play_immediately'
  nvs.volume = 50
  nvs.repetitions = 1
  nvs.fadein = 0
  nvs.pitch = 0
  nvs.duration = 1000
  nvsoundPub.publish(nvs)

def publishSaySentence(sentence):
  sentencePub = rospy.Publisher('say_sentence', Utterance, queue_size=10)
  u = Utterance();
  u.sentence = sentence
  u.emotion = 'normal'
  u.volume = 50
  u.agent = ''
  u.priority = Utterance.APPEND
  sentencePub.publish(u)

