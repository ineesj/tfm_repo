#!/usr/bin/env python

# Description: BMCAWantPlay action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import * #BehaviorSelection, KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_behaviors.srv import *
from std_msgs.msg import *


class BMCAWantPlay(StateAction):

    def __init__(self, monarchState, robots=[0], game='interactive_game'):
        # BMCAWantPlay initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type robots: an array of integers
        @type game: a variable of type string
        @param monarchState: the MonarchState to which this action belongs
        @param robots: the number of the robots to execute the behaviour
        @param game: the name of the game that the robot will ask to play."""
        super(BMCAWantPlay, self).__init__(monarchState, robots)
        rospy.wait_for_service('/mcentral/request_behavior')  

        self.behaviour_selection = BehaviorSelection()
        self.behaviour_selection.name = 'dispatchca'
        self.behaviour_selection.instance_id = 0
        self.behaviour_selection.robots = robots
        self.behaviour_selection.resources = ['RVocSnd']
        self.behaviour_selection.active = True
        self.behaviour_selection.parameters = []

        self.parameter = KeyValuePair()
        self.parameter.key = 'activated_cas'
        self.parameter.value = 'ca01'
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'question_to_user'
        self.parameter.value = 'want_play_game'
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'name_of_game'
        self.parameter.value = game
        self.behaviour_selection.parameters.append(self.parameter)

        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # BMCAWantPlay execute
        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            super(BMCAWantPlay, self).notify_action(True)
        else:
            super(BMCAWantPlay, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return