#!/usr/bin/env python

# Description: SAM reader action is a state action designed to create a SAM reader 
#              for a user-defined SAM slot. It receives the SAM slot data and notifies 
#              the MonarchState sending that data.		   


from mosmach.state_action import StateAction
from sam_helpers.reader import SAMReader

class SamReaderAction(StateAction):

  def __init__(self, monarchState, samSlotName, samAgentName, condition):
    # SamReaderAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type samSlotName: string
    @type samAgentName: string
    @type condition: function
    @param monarchState: the MonarchState to which this action belongs
    @param samSlotName: name of the SAM slot to read
    @param samAgentName: name of the agent of the SAM slot
    @param condition: the function that will act as the sam reader callback and enable to save the data in a user-defined variable."""
    super(SamReaderAction, self).__init__(monarchState, condition)
    self.samSlotName = samSlotName
    self.samAgentName = samAgentName
    self.samReader = ''

  def execute(self):
    self.samReader = SAMReader(self.samSlotName, self.execute_cb, self.samAgentName)

  def execute_cb(self, data):
    self._condition(data, self._userdata)
    super(SamReaderAction, self).notify_action(True)

  def stop(self):
    if self.samReader != '':
        self.samReader.remove()
    return