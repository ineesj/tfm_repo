#!/usr/bin/env python

# Description: Navigation By Name action is a state action designed to create a simple
#              actionlib client using the NavigationByNameAction.


import rospy
import actionlib
import rospkg
import os.path
import sys

rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

from mosmach.state_action import StateAction
from scout_msgs.msg import *


class NavByNameAction(StateAction):

  def __init__(self, monarchState, target_name):
    # NavByNameAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type target_name: string
    @param monarchState: the MonarchState to which this action belongs
    @param target_name: name of the target."""
    super(NavByNameAction, self).__init__(monarchState)
    self.target_name = target_name

    self.client = actionlib.SimpleActionClient('navigation_by_name', NavigationByNameAction) 

  def execute(self):
    # NavByNameAction execute
    dtarget_goal = NavigationByNameGoal()
    target_goal.name = self.target_name

    self.client.wait_for_server()
    self.client.send_goal_and_wait(target_goal)
    super(NavByNameAction, self).notify_action(True)
    
  def stop(self):
    # send actionlib abort
    self.client.cancel_goal()
    return

