#!/usr/bin/env python

# Description: Set mouth led action is a state action designed to create a topic publisher
#              to send data to the cmd_mouth topic. By sending the data it is possible
#              to control the leds of the mouth, of the MOnarCH robot.


import rospy

from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class SetMouthAction(StateAction):
  
  def __init__(self, monarchState, mouthData, is_dynamic=False):
    # RunMouthAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type mouthData: boolean array [256] or function returning a boolean array [256]
    @type is_dynamic: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param mouthData: a list of booleans to control the mouth leds
    @param is_dynamic: check if the data is dynamic, if True pass a function instead of a boolean array."""
    super(SetMouthAction, self).__init__(monarchState, mouthData)
    self.mouthLedControl = MouthLedControl()
    self.topicWriter = rospy.Publisher('cmd_mouth', MouthLedControl, queue_size=10)
    rospy.sleep(1)
    self.mouthLedControl.data = mouthData
    self.is_dynamic = is_dynamic

  def execute(self):
    # RunMouthAction execute
    rate = rospy.Rate(1)
    
    if self.is_dynamic:
        self.mouthLedControl = self._condition(self._userdata)

    self.topicWriter.publish(self.mouthLedControl)
    rate.sleep()
    super(SetMouthAction, self).notify_action(True)
    
  def stop(self):
    self.topicWriter.unregister()
    return

