#!/usr/bin/env python

# Description: Run CE action is a state action designed to create a topic publisher
#              to send data to the express_gesture topic. By sending the data it is possible
#              to control the "communicative expressions" of the MOnarCH robot.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class RunCeAction(StateAction):

  # CE's available:
  #   - give_greetings_child
  #   - give_greetings_male
  #   - give_greetings_female
  #   - give_greetings_staff
  #   - mbot_ask_for_room_number
  #   - mbot_behavior_stopped
  #   - mbot_behavior_succeeded
  #   - mbot_reject_command
  #   - mbot_start_walking
  #   - mbot_acknowledge_stop_request
  #   - mbot_ask_for_destination
  #   - mbot_follow_robot
  #   - mbot_warm_expression
  #   - mbot_give_thanks
  
  def __init__(self, monarchState, ce, is_dynamic=False):
    # RunCeAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type ce: string or function returning a string
    @param monarchState: the MonarchState to which this action belongs
    @param ce: the name of the ce to activate
    @param is_dynamic: check if the message is dynamic, if True pass a function instead of ce."""
    super(RunCeAction, self).__init__(monarchState, ce)
    self.gestureCE = GestureExpression()
    self.gestureCE.gesture = ce
    self.is_dynamic = is_dynamic

    self.topicWriter = rospy.Publisher('express_gesture', GestureExpression, queue_size=10)
    rospy.sleep(1)

  def execute(self):
    # RunCeAction execute
    rate = rospy.Rate(1)

    if self.is_dynamic:
      self.gestureCE.gesture = self._condition(self._userdata)

    self.topicWriter.publish(self.gestureCE)
    rate.sleep()
    super(RunCeAction, self).notify_action(True)
    
  def stop(self):
    #self.topicWriter.unregister()
    return

