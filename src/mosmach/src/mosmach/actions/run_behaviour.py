#!/usr/bin/env python

# Description: Run Behaviour action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import *
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import BehaviorSelection, KeyValuePair
from monarch_behaviors.srv import *
from std_msgs.msg import *


class RunBehaviour(StateAction):

    def __init__(self, monarchState, behaviour):
        # RunBehaviour initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type behaviour: a message of the type BehaviorSelection
        @param monarchState: the MonarchState to which this action belongs
        @param behaviour: a BehaviorSelection message."""
        super(RunBehaviour, self).__init__(monarchState, behaviour)
        rospy.wait_for_service('/mcentral/request_behavior')  

        self.behaviour_selection = behaviour
        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # RunBehaviour execute
        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            rospy.loginfo('Behavior sent')
            super(RunBehaviour, self).notify_action(True)
        else:
            rospy.loginfo('Behavior not sent')
            super(RunBehaviour, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return