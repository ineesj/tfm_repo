#!/usr/bin/env python

# Description: BMGetRfid action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import * #BehaviorSelection, KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_behaviors.srv import *
from std_msgs.msg import *


class BMGetRfid(StateAction):

    def __init__(self, monarchState, robots=[0], person_id=1001, is_dynamic=False):
        # BMGetRfid initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type robots: an array of integers
        @type person_id: a variable of type integer
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param robots: the number of the robots to execute the behaviour
        @param person_id: the person id for the robot to getrfid from
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of parameters individually."""
        super(BMGetRfid, self).__init__(monarchState, person_id)
        rospy.wait_for_service('/mcentral/request_behavior')  

        self.behaviour_selection = BehaviorSelection()
        self.behaviour_selection.name = 'getrfid'
        self.behaviour_selection.instance_id = 0
        self.behaviour_selection.robots = robots
        self.behaviour_selection.active = True
        self.behaviour_selection.parameters = []

        self.parameter = KeyValuePair()
        self.parameter.key = 'camera_id'
        self.parameter.value = person_id
        self.behaviour_selection.parameters.append(self.parameter)

        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # BMGetRfid execute
        if self.is_dynamic:
            self.parameter.value = self._condition(self._userdata)
            self.behaviour_selection.parameters.append(self.parameter)

        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            super(BMGetRfid, self).notify_action(True)
        else:
            super(BMGetRfid, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return