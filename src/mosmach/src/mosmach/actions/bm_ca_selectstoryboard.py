#!/usr/bin/env python

# Description: BMCASelectStoryboard action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import * #BehaviorSelection, KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_behaviors.srv import *
from std_msgs.msg import *


class BMCASelectStoryboard(StateAction):

    def __init__(self, monarchState, robots=[0]):
        # BMCASelectStoryboard initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type robots: an array of integers
        @param monarchState: the MonarchState to which this action belongs
        @param robots: the number of the robots to execute the behaviour."""
        super(BMCASelectStoryboard, self).__init__(monarchState, robots)
        rospy.wait_for_service('/mcentral/request_behavior')  

        self.behaviour_selection = BehaviorSelection()
        self.behaviour_selection.name = 'dispatchca'
        self.behaviour_selection.instance_id = 0
        self.behaviour_selection.robots = robots
        self.behaviour_selection.resources = ['RVocSnd']
        self.behaviour_selection.active = True
        self.behaviour_selection.parameters = []

        self.parameter = KeyValuePair()
        self.parameter.key = 'activated_cas'
        self.parameter.value = 'ca01'
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'question_to_user'
        self.parameter.value = 'select_storyboard'
        self.behaviour_selection.parameters.append(self.parameter)

        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # BMCASelectStoryboard execute
        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            super(BMCASelectStoryboard, self).notify_action(True)
        else:
            super(BMCASelectStoryboard, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return