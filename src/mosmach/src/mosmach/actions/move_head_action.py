#!/usr/bin/env python

# Description: Move head action is a state action designed to create a topic publisher
#              to send data to the cmd_head topic. By sending the data it is possible
#              to move the head of the MOnarCH robot.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class MoveHeadAction(StateAction):
    def __init__(self, monarchState, headPosition=0, movementSpeed=0, data_cb='', is_dynamic=False):
        # MoveHeadAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type headPosition: float
        @type movementSpeed: integer
        @type data_cb: function
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param headPosition: the position which the head will move to
        @param movementSpeed: the velocity of the head movement
        @param data_cb: callback function for dynamic data returning a HeadControl message
        @param is_dynamic: check if the data is dynamic, if True pass a function instead of parameters individually."""
        super(MoveHeadAction, self).__init__(monarchState,data_cb)
        self.headControl = HeadControl()
        self.topicWriter = rospy.Publisher('cmd_head', HeadControl, queue_size=10)
        rospy.sleep(1)
        self.headControl.head_pos = headPosition
        self.headControl.movement_speed = movementSpeed
        self.is_dynamic = is_dynamic

    def execute(self):
        # MoveHeadAction execute
        rate = rospy.Rate(1)
        
        if self.is_dynamic:
            self.headControl = self._condition(self._userdata)

        self.topicWriter.publish(self.headControl)
        rate.sleep()
        super(MoveHeadAction, self).notify_action(True)
        
    def stop(self):
        #self.topicWriter.unregister()
        return

