#!/usr/bin/env python

# Description: Navigation By VelRef Action is a state action designed to create a simple
#              actionlib client using the NavigationByVelRefAction.


import rospy
import actionlib
import rospkg
import os.path
import sys

rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

from mosmach.state_action import StateAction
from scout_msgs.msg import *


class NavByVelRefAction(StateAction):

  def __init__(self, monarchState, topic_name):
    # NavByVelRefAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type topic_name: string
    @param monarchState: the MonarchState to which this action belongs
    @param topic_name: topic name where a geometry_msgs/TwistStamped with the reference velocity is published."""
    super(NavByVelRefAction, self).__init__(monarchState)
    self.topic_name = topic_name

    self.client = actionlib.SimpleActionClient('navigation_by_velref', NavigationByVelRefAction) 

  def execute(self):
    # NavByVelRefAction execute
    target_goal = NavigationByVelRefGoal()
    target_goal.topic_name = self.topic_name

    self.client.wait_for_server()
    self.client.send_goal_and_wait(target_goal)
    super(NavByVelRefAction, self).notify_action(True)
    
  def stop(self):
    # send actionlib abort
    self.client.cancel_goal()
    return

