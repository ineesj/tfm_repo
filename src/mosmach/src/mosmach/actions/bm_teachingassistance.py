#!/usr/bin/env python

# Description: BMTeachingAssistance action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import * #BehaviorSelection, KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_behaviors.srv import *
from std_msgs.msg import *


class BMTeachingAssistance(StateAction):

    def __init__(self, monarchState, robots=[0]):
        # BMTeachingAssistance initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type robots: an array of integers
        @param monarchState: the MonarchState to which this action belongs
        @param robots: the number of the robots to execute the behaviour."""
        super(BMTeachingAssistance, self).__init__(monarchState, robots)
        rospy.wait_for_service('/mcentral/request_behavior')  

        self.behaviour_selection = BehaviorSelection()
        self.behaviour_selection.name = 'teachingassistance'
        self.behaviour_selection.instance_id = 0
        self.behaviour_selection.robots = robots
        self.behaviour_selection.active = True
        self.behaviour_selection.parameters = []

        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # BMTeachingAssistance execute
        if self.is_dynamic:
            self.parameter.value = self._condition(self._userdata)
            self.behaviour_selection.parameters.append(self.parameter)

        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            super(BMTeachingAssistance, self).notify_action(True)
        else:
            super(BMTeachingAssistance, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return