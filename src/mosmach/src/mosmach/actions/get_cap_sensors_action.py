#!/usr/bin/env python

# Description: Get Cap Sensors Action is a state action designed to create a topic subscriber
#              to get data from cap_sensors topic. 


import rospy

from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class GetCapSensorsAction(StateAction):

    def __init__(self, monarchState, data_cb):
        # GetCapSensorsAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type data_cb: function
        @param monarchState: the MonarchState to which this action belongs
        @param data_cb: callback function for dynamic data returning a CapacitiveSensorsReadings message."""
        super(GetCapSensorsAction, self).__init__(monarchState, data_cb)
        self.react_time_in_secs=0.5

        self.topicName = "cap_sensors"
        self.topicMsgType = CapacitiveSensorsReadings
        self.sub = ''
        self.head_counter = 0
        self.left_shoulder_counter = 0
        self.right_shoulder_counter = 0
        self.left_arm_counter = 0
        self.right_arm_counter = 0
        self.notify = False
        self.counter_max = self.react_time_in_secs/0.05
        self.cap_filtered_msg = CapacitiveSensorsReadings()

    def execute(self):
        self.sub = rospy.Subscriber(self.topicName, self.topicMsgType, self.execute_cb)

    def execute_cb(self, data):
        if data.head == True:
            self.head_counter = self.head_counter + 1
            if self.head_counter >= self.counter_max:
                self.cap_filtered_msg.head = True
                self.notify = True
        else:
            self.head_counter = 0
            self.cap_filtered_msg.head = False

        if data.left_shoulder == True:
            self.left_shoulder_counter = self.left_shoulder_counter + 1
            if self.left_shoulder_counter >= self.counter_max:
                self.cap_filtered_msg.left_shoulder = True
                self.notify = True
        else:
            self.left_shoulder_counter = 0
            self.cap_filtered_msg.left_shoulder = False

        if data.right_shoulder == True:
            self.right_shoulder_counter = self.right_shoulder_counter + 1
            if self.right_shoulder_counter >= self.counter_max:
                self.cap_filtered_msg.right_shoulder = True
                self.notify = True
        else:
            self.right_shoulder_counter = 0
            self.cap_filtered_msg.right_shoulder = False
        
        if data.left_arm == True:
            self.left_arm_counter = self.left_arm_counter + 1
            if self.left_arm_counter >= self.counter_max:
                self.cap_filtered_msg.left_arm = True
                self.notify = True
        else:
            self.left_arm_counter = 0
            self.cap_filtered_msg.left_arm = False

        if data.right_arm == True:
            self.right_arm_counter = self.right_arm_counter + 1
            if self.right_arm_counter >= self.counter_max:
                self.cap_filtered_msg.right_arm = True
                self.notify = True
        else:
            self.right_arm_counter = 0
            self.cap_filtered_msg.right_arm = False

        if self.notify == True:
            value = self._condition(self.cap_filtered_msg, self._userdata)
            super(GetCapSensorsAction, self).notify_action(True) 
            self.notify = False

    def stop(self):
        if self.sub != '':
            self.sub.unregister()
        return

