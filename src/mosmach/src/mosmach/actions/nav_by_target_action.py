#!/usr/bin/env python

# Description: Navigation By Target action is a state action designed to create a simple
#              actionlib client using the NavigationByTargetAction.


import rospy
import actionlib
import rospkg
import os.path
import sys

rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

from mosmach.state_action import StateAction
from scout_msgs.msg import *


class NavByTargetAction(StateAction):

    def __init__(self, monarchState, target_frame='' ,heading_frame='', proximity=0, follow_target=0, data_cb='', is_dynamic = False):
        # NavByTargetAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type target_frame: string
        @type heading_frame: string
        @type proximity: integer
        @type follow_target: boolean
        @type data_cb: function 
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param target_frame: name of the TF frame of the target
        @param heading_frame: name of the TF frame the robot body should head towards
        @param proximity: distance to target robot should maintain
        @param follow_target: whether the action keeps running or terminates once the goal is reached
        @param data_cb: callback function for dynamic goal returning a NavigationByTargetGoal message
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of parameters individually."""
        super(NavByTargetAction, self).__init__(monarchState, data_cb)
        self.target_frame = target_frame
        self.heading_frame = heading_frame
        self.proximity = proximity
        self.follow_target = follow_target
        self.is_dynamic = is_dynamic

        self.client = actionlib.SimpleActionClient('navigation_by_target', NavigationByTargetAction) 

    def execute(self):
        # NavByTargetAction execute
        target_goal = NavigationByTargetGoal()
        target_goal.target_frame = self.target_frame
        target_goal.heading_frame = self.heading_frame
        target_goal.proximity = self.proximity
        target_goal.follow_target = self.follow_target 

        if self.is_dynamic:
            target_goal = self._condition(self._userdata)

        self.client.wait_for_server()
        self.client.send_goal_and_wait(target_goal)
        super(NavByTargetAction, self).notify_action(True)

    def stop(self):
        # send actionlib abort
        self.client.cancel_goal()
        return

