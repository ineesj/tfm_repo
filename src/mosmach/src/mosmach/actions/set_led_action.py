#!/usr/bin/env python

# Description: Set led action is a state action designed to create a topic publisher
#              to send data to the cmd_leds topic. By sending the data it is possible
#              to control the leds of the eyes, the cheeks and the base, of the MOnarCH robot.


import rospy

from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class SetLedAction(StateAction):

  def __init__(self, monarchState, deviceName=0, redIntensity=0, greenIntensity=0, blueIntensity=0, changeTime=0, data_cb='', is_dynamic=False):
    # SetLedAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type deviceName: string/int
    @type redIntensity: integer
    @type greenIntensity: integer
    @type blueIntensity: integer
    @type changeTime: float
    @type data_cb: function
    @type is_dynamic: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param deviceName: the name of the device to control
    @param redIntensity: LEDs red intensity level
    @param greenIntensity: LEDs green intensity level
    @param blueIntensity: LEDs blue intensity level
    @param changeTime: the velocity that the LEDs will change intensity
    @param data_cb: callback function for dynamic data returning a LedControl message
    @param is_dynamic: check if the data is dynamic, if True pass a function instead of the parameters individually."""
    super(SetLedAction, self).__init__(monarchState, data_cb)
    self.ledControl = LedControl() 
    self.topicWriter = rospy.Publisher('cmd_leds', LedControl, queue_size=10)
    rospy.sleep(1)
    if deviceName == 'leftEye':
      self.ledControl.device = 0
    elif deviceName == 'rightEye':
      self.ledControl.device = 1
    elif deviceName == 'cheeks':
      self.ledControl.device = 2
    elif deviceName == 'baseRight':
      self.ledControl.device = 3
    elif deviceName == 'baseFront':
      self.ledControl.device = 4
    elif deviceName == 'baseLeft':
      self.ledControl.device = 5
    elif isinstance(deviceName,int):
      self.ledControl.device = deviceName      
    self.ledControl.r = redIntensity 
    self.ledControl.g = greenIntensity
    self.ledControl.b = blueIntensity
    self.ledControl.change_time = changeTime
    self.is_dynamic = is_dynamic
    
  def execute(self):
    # SetLedAction execute
    rate = rospy.Rate(1)

    if self.is_dynamic:
        self.ledControl = self._condition(self._userdata)
  
    self.topicWriter.publish(self.ledControl)
    rate.sleep()
    super(SetLedAction, self).notify_action(True)
    
  def stop(self):
    self.topicWriter.unregister()
    return

