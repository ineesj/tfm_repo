#!/usr/bin/env python

# Description: SAM writer action is a state action designed to create a SAM writer 
#              for a user-defined SAM slot. It writes the data in the defined SAM slot.


import rospy

from mosmach.state_action import StateAction
from sam_helpers.writer import SAMWriter


class SamWriterAction(StateAction):

    def __init__(self, monarchState, samSlotName, samAgentName, message, is_dynamic=False):
        # SamWriterAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type samSlotName: string
        @type samAgentName: string
        @type message: SAM slot message or function returning a SAM slot message
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param samSlotName: name of the SAM slot to write
        @param samAgentName: name of the agent of the SAM slot
        @param message: set the message to send / callback function for a dynamic message
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of a message."""
        super(SamWriterAction, self).__init__(monarchState, message)
        self.message = message
        self.is_dynamic = is_dynamic
        self.samWriter = SAMWriter(samSlotName,samAgentName)
        #rospy.sleep(0.1)

    def execute(self):
        # SamWriterAction execute
        rospy.sleep(0.5)
        rate = rospy.Rate(1)

        if self.is_dynamic:
            self.message = self._condition(self._userdata, self.samWriter)

        self.samWriter.publish(self.message)
        rate.sleep()
        super(SamWriterAction, self).notify_action(True)

    def stop(self):
        if self.samWriter != '':
            self.samWriter.remove()
        return
