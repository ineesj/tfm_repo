#!/usr/bin/env python

# Description: Graph Base Controller Action is a state action designed to create a simple
#              actionlib client using the ControllerGBAction.


import rospy
import actionlib

from mosmach.state_action import StateAction
from graph_based_formations.msg import *


class GBControllerAction(StateAction):

    def __init__(self, monarchState, bias_length=0 ,formation_id=0, max_cc=0, leader_id=0, follower_1=0 , follower_2=0, follower_3=0, number_of_robots=0, switch_enabled=0, data_cb='', is_dynamic = False):
        # GBControllerAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type bias_length: float
        @type formation_id: integer
        @type max_cc: integer
        @type leader_id: integer
        @type follower_1: integer
        @type follower_2: integer
        @type follower_3: integer
        @type number_of_robots: integer
        @type switch_enabled: integer
        @type data_cb: function
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param bias_length: the desired formation spacing. Use to change the extent of the formation
        @param formation_id: shape of the formation (1 - line; 2- square; 3 - diamond; 4 - column)
        @param max_cc: define the action timeout in the number of steps. The main loop is executed at 10 Hz
        @param leader_id: second digit from mbot name; to make mbot03 of mbot13 a leader choose leader_id=3
        @param follower_1: as above; if there are two robots, then specify follower_1
        @param follower_2: if there are three robots, specify follower_1 and follower_2
        @param follower_3: if there are four robots, specify follower_1, follower_2 and follower_3
        @param number_of_robots: between 2 and 4; includes the leader, so that if 2, then the formation is composed of a leader and one follower
        @param switch_enabled: todo
        @param data_cb: callback function for dynamic goal returning a ControllerGBGoal message
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of parameters individually."""
        super(GBControllerAction, self).__init__(monarchState, data_cb)

        self.bias_length = bias_length
        self.formation_id = formation_id
        self.max_cc = max_cc
        self.leader_id = leader_id
        self.follower_1 = follower_1
        self.follower_2 = follower_2
        self.follower_3 = follower_3
        self.number_of_robots = number_of_robots
        self.switch_enabled = switch_enabled
        self.is_dynamic = is_dynamic

        self.client = actionlib.SimpleActionClient('graph_based_controller', ControllerGBAction) 

    def execute(self):
        # GBControllerAction execute
        target_goal = ControllerGBGoal()
        target_goal.bias_length = self.bias_length
        target_goal.formation_id = self.formation_id
        target_goal.max_cc = self.max_cc
        target_goal.leader_id = self.leader_id 
        target_goal.follower_1 = self.follower_1
        target_goal.follower_2 = self.follower_2
        target_goal.follower_3 = self.follower_3
        target_goal.number_of_robots = self.number_of_robots 
        target_goal.switch_enabled = self.switch_enabled 

        if self.is_dynamic:
            target_goal = self._condition(self._userdata)

        self.client.wait_for_server()
        self.client.send_goal_and_wait(target_goal)
        super(GBControllerAction, self).notify_action(True)

    def stop(self):
        # send actionlib abort
        self.client.cancel_goal()
        return