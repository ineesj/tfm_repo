#!/usr/bin/env python

# Description: BMInteractiveGame action is a state action designed to send a request
#              to global behaviour manager. By sending the request it is possible
#              to launch a behaviour of the MOnarCH robot.


import rospy
import roslib; roslib.load_manifest('monarch_behaviors')

from mosmach.state_action import StateAction
from monarch_msgs.msg import * #BehaviorSelection, KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_behaviors.srv import *
from std_msgs.msg import *


class BMInteractiveGame(StateAction):

    def __init__(self, monarchState, robots=[0,9], players='mbot09 human2', rows='3', cols='4', level='0', tracking='1', interaction='nonverbal', active=True, data_cb='', is_dynamic=False):
        # BMGetRfid initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type robots: an array of integers
        @type players: string
        @type rows: string
        @type cols: string
        @type level: string
        @type tracking: string
        @type interaction: string
        @type data_cb: function
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param robots: the number of the robots to execute the behaviour
        @param players: name of the players
        @param rows: number of rows
        @param cols: number of cols
        @param level: level of the game (0 - tutorial, 1 - reactive)
        @param tracking: tracking mode (0 - OFF, 1 - ON)
        @param interaction: level of interaction ('none','nonverbal','verbal')
        @param data_cb: callback function for dynamic game parameters
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of parameters individually."""
        super(BMInteractiveGame, self).__init__(monarchState, data_cb)
        rospy.wait_for_service('/mcentral/request_behavior')  
        self.is_dynamic = is_dynamic

        self.behaviour_selection = BehaviorSelection()
        self.behaviour_selection.name = 'interactivegame'
        self.behaviour_selection.instance_id = 0
        self.behaviour_selection.robots = robots
        self.behaviour_selection.resources = ['RNAV','interaction_interfaces']
        self.behaviour_selection.active = active
        self.behaviour_selection.parameters = []

        self.parameter = KeyValuePair()
        self.parameter.key = 'players'
        self.parameter.value = players # human1 - girl / human2 - boy
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'rows'
        self.parameter.value = rows
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'cols'
        self.parameter.value = cols
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'level'
        self.parameter.value = level
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'tracking'
        self.parameter.value = tracking
        self.behaviour_selection.parameters.append(self.parameter)

        self.parameter = KeyValuePair()
        self.parameter.key = 'interaction'
        self.parameter.value = interaction
        self.behaviour_selection.parameters.append(self.parameter)


        self.request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

        rospy.sleep(1)

    def execute(self):
        # BMInteractiveGame execute
        if self.is_dynamic:
            self.parameter = self._condition(self._userdata)
            self.behaviour_selection.parameters.append(self.parameter)

        answer = self.request_behavior(self.behaviour_selection)

        if answer.success is True:
            super(BMInteractiveGame, self).notify_action(True)
        else:
            super(BMInteractiveGame, self).notify_action(False)

        rospy.sleep(0.5)

    def stop(self):
        return