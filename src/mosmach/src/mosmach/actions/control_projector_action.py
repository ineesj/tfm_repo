#!/usr/bin/env python

# Description: ControlProjectorAction is a state action designed to create a topic publisher
#              to send data to the /cmd_videoProjector/ topic. By sending the data it is possible
#              to turn on/off the MOnarCH robot projector.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *
from std_msgs.msg import *


class ControlProjectorAction(StateAction):

  # Commands available:
  #   True - turn on projector
  #   False - turn off projector

  def __init__(self, monarchState, msg_cmd, is_dynamic=False):
    # ControlProjectorAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type msg_cmd: boolean or function returning a boolean
    @type is_dynamic: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param msg_cmd: command to turn on/off the projector
    @param is_dynamic: check if the message is dynamic, if True pass a function instead of a boolean."""
    super(ControlProjectorAction, self).__init__(monarchState, msg_cmd)
    self.projectorCmd = msg_cmd
    self.is_dynamic = is_dynamic
    self.message = VideoProjectorControl()

    self.topicWriter = rospy.Publisher('cmd_videoProjector', VideoProjectorControl, queue_size=10)
    rospy.sleep(1)

  def execute(self):
    # ControlProjectorAction execute
    rate = rospy.Rate(1)

    if self.is_dynamic:
      self.projectorCmd = self._condition(self._userdata)

    self.message.videoProjector = self.projectorCmd
    self.topicWriter.publish(self.message)
    rate.sleep()
    super(ControlProjectorAction, self).notify_action(True)
  
  def stop(self):
    #if self.topicWriter != '':
    #  self.topicWriter.unregister()
    return

