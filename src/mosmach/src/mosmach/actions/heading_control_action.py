#!/usr/bin/env python

# Description: Heading Control action is a state action designed to create a simple
#              actionlib client using the HeadingControlAction.


import rospy
import actionlib
import rospkg
import os.path
import sys

rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

from mosmach.state_action import StateAction
from scout_msgs.msg import *


class HControlAction(StateAction):

  def __init__(self, monarchState, topic_name, heading, body_frame):
    # HControlAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type topic_name: string
    @type heading: integer
    @type body_frame: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param topic_name: topic name where a std_msgs/Float64 with the desired heading value is published
    @param heading: a fixed heading value
    @param body_frame: whether the heading is specified in body frame of world frame."""
    super(HControlAction, self).__init__(monarchState)
    self.topic_name = topic_name
    self.heading = heading
    self.body_frame = body_frame
    
    self.client = actionlib.SimpleActionClient('heading_control', HeadingControlAction) 

  def execute(self):
    # HControlAction executes
    target_goal = HeadingControlGoal()
    target_goal.topic = self.topic_name
    target_goal.heading = self.heading
    target_goal.is_body_frame = self.body_frame

    self.client.wait_for_server()
    self.client.send_goal_and_wait(target_goal)
    super(HControlAction, self).notify_action(True)
    
  def stop(self):
    # send actionlib abort
    self.client.cancel_goal()
    return

