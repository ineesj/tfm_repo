#!/usr/bin/env python

# Description: SetProjectorAction is a state action designed to create a topic publisher
#              to send data to the projector/show_hide topic. By sending the data it is possible
#              to hide or show the MOnarCH robot projector.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *
from std_msgs.msg import *


class SetProjectorAction(StateAction):

  # Commands available:
  #   hide - hide projector
  #   show - show projector
  #   fullscreen - show projector in fullscreen

  def __init__(self, monarchState, msg_cmd, is_dynamic=False):
    # SetProjectorAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type msg_cmd: string or function returning a string
    @type is_dynamic: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param msg_cmd: the name of the command to execute
    @param is_dynamic: check if the message is dynamic, if True pass a function instead of a string."""
    super(SetProjectorAction, self).__init__(monarchState, msg_cmd)
    self.projectorCmd = msg_cmd
    self.is_dynamic = is_dynamic

    self.topicWriter = rospy.Publisher('projector/show_hide', String, queue_size=10)
    rospy.sleep(1)

  def execute(self):
    # SetProjectorAction execute
    rate = rospy.Rate(1)

    if self.is_dynamic:
      self.projectorCmd = self._condition(self._userdata)

    self.topicWriter.publish(self.projectorCmd)
    rate.sleep()
    super(SetProjectorAction, self).notify_action(True)
  
  def stop(self):
    self.topicWriter.unregister()
    return

