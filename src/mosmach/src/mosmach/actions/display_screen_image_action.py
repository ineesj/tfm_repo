#!/usr/bin/env python

# Description: DisplayScreenImageAction is a state action designed to create a topic publisher
#              to send data to the screen/display_image topic. By sending the data it is possible
#              to display images on the MOnarCH robot screen.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *
from std_msgs.msg import *


class DisplayScreenImageAction(StateAction):

    # Images available - ../hri_interfaces/screen/data/ (19/05/2015):
    #   ok.png
    #   ok2.png
    #   thanks.png
    #   caution.png
    #   cross.png
    #   heart.png
    #   hello.png
    #   hello2.png
    #   questionmark.png
    #   shhh.png
    #   monarch.jpg

    def __init__(self, monarchState, msg_cmd, is_dynamic=False):
        # DisplayScreenImageAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type msg_cmd: string or function returning a string
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param msg_cmd: the name of the image to display
        @param is_dynamic: check if the message is dynamic, if True pass a function instead of string."""
        super(DisplayScreenImageAction, self).__init__(monarchState, msg_cmd)
        self.screenCmd = msg_cmd
        self.is_dynamic = is_dynamic

        self.topicWriter = rospy.Publisher('screen/display_image', String, queue_size=10)
        rospy.sleep(1)

    def execute(self):
        # DisplayScreenImageAction execute
        rate = rospy.Rate(1)

        if self.is_dynamic:
            self.screenCmd = self._condition(self._userdata)

        self.topicWriter.publish(self.screenCmd)
        rate.sleep()
        super(DisplayScreenImageAction, self).notify_action(True)

    def stop(self):
        self.topicWriter.unregister()
        return

