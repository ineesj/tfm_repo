#!/usr/bin/env python

# Description: Move arms action is a state action designed to create a topic publisher
#              to send data to the cmd_arms topic. By sending the data it is possible
#              to move the arms of the MOnarCH robot.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *


class MoveArmsAction(StateAction):

    def __init__(self, monarchState, setTorque=0, leftArmPosition=0, rightArmPosition=0, movementSpeed=0, data_cb='', is_dynamic=False):
        # MoveArmsAction initialization
        """Constructor.
        @type monarchState: an object of type MonarchState
        @type setTorque: boolean
        @type leftArmPosition: float
        @type rightArmPosition: float
        @type movementSpeed: float
        @type data_cb: function
        @type is_dynamic: boolean
        @param monarchState: the MonarchState to which this action belongs
        @param setTorque: the state of the torque of the arm's motors (off, on or break)
        @param leftArmPosition: the position which the left arm will move to
        @param rightArmPosition: the position which the right arm will move to
        @param movementSpeed: the velocity of the arms movement
        @param data_cb: callback function for dynamic data returning ArmsControl messsage
        @param is_dynamic: check if the data is dynamic, if True pass a fuction instead of parameters individually."""
        super(MoveArmsAction, self).__init__(monarchState, data_cb)
        self.armsControl = ArmsControl()
        self.topicWriter = rospy.Publisher('cmd_arms', ArmsControl, queue_size=10)
        rospy.sleep(1)
        self.armsControl.torque_enable = setTorque
        self.armsControl.left_arm = leftArmPosition
        self.armsControl.right_arm = rightArmPosition
        self.armsControl.movement_time = movementSpeed
        self.is_dynamic = is_dynamic

    def execute(self):
        # MoveArmsAction execute
        rate = rospy.Rate(1)
        
        if self.is_dynamic:
            self.armsControl = self._condition(self._userdata)

        self.topicWriter.publish(self.armsControl)
        rate.sleep()
        super(MoveArmsAction, self).notify_action(True)

    def stop(self):
        self.topicWriter.unregister()
        return