#!/usr/bin/env python

# Description: Run CA action is a state action designed to create a topic publisher
#              to send data to the ca_activations topic. By sending the data it is possible
#              to control the "communicative acts" of the MOnarCH robot.


import rospy
from mosmach.state_action import StateAction
from monarch_msgs.msg import *
from std_msgs.msg import *
from monarch_msgs_utils import key_value_pairs as kvpa


class RunCaAction(StateAction):

  # CA's available:
  #   ca01 - Ask for Person Information
  #   ca02 - Ask for Room Information
  #   ca03 - Ask for Object Information
  #   ca04 - Give Information Related to a Person
  #   ca05 - Give Information Related to a Room
  #   ca06 - Give Information Related to an Object
  #   ca07 - Block an area
  #   ca08 - Tell child to Go To Class
  #   ca09 - Ask Person to Walk Slower
  #   ca10 - Ask Person to Step Aside
  #   ca11 - Ask for Help
  #   ca12 - Gather a Group of Children
  #   ca13 - Ask for Person Information
  #   ca14 - Ask for Room Information
  #   ca15 - Give Greetings
  #   ca16 - Show a Warm Expression
  #   ca17 - Show Happiness
  #   ca18 - Try to Engage a Person
  #   ca19 - Apologize
  #   ca20 - Congratulate Somebody
  #   ca21 - Express Gratitude
  #   ca22 - Tell a Person that the Robot cannot see him/her During Interaction
  #   ca23 - Tell a Person that it did not understood what he/she has said
  #   ca24 - Back Channel
  #   ca25 - Show Aliveness
  #   ca26 - Express Current Task

  def __init__(self, monarchState, ca, is_dynamic=False):
    # RunCaAction initialization
    """Constructor.
    @type monarchState: an object of type MonarchState
    @type ca: KeyVaulePairArray or function returning a KeyValuePairArray
    @type is_dynamic: boolean
    @param monarchState: the MonarchState to which this action belongs
    @param ca: the name of the ca to activate
    @param is_dynamic: check if the message is dynamic, if True pass a fuction instead of ca."""
    super(RunCaAction, self).__init__(monarchState, ca)
    self.gestureCA = ca
    self.is_dynamic = is_dynamic

    self.topicWriter = rospy.Publisher('ca_activations', KeyValuePairArray, queue_size=10)
    rospy.sleep(1)

  def execute(self):
    # RunCaAction execute
    rate = rospy.Rate(1)

    if self.is_dynamic:
      self.gestureCA = self._condition(self._userdata)

    self.topicWriter.publish(self.gestureCA)
    rate.sleep()
    super(RunCaAction, self).notify_action(True)
  
  def stop(self):
    #if self.topicWriter != '':
    #  self.topicWriter.unregister()
    return

