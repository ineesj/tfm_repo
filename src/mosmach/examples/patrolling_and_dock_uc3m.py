#!/usr/bin/env python
"""
State Machine for patrolling and docking in UC3M Roboticslab facilities.

:version: 0.1
:date: January 2015
:author: Victor Gonzalez-Pacheco
"""


import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from itertools import islice

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.change_conditions.topic_condition import TopicCondition

from std_msgs.msg import Bool
from monarch_msgs.msg import (BatteriesVoltage,
                              RfidReading,
                              SetStateAuxiliaryPowerBattery,
                              SetStateElectronicPower,
                              SetStateMotorsPower)

import rospy_utils as rpyu
from monarch_behaviors import behavior_utils as butils

import sys
import os.path
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))
from scout_msgs.srv import TeleOpSrv

# Move to Waypoint State:
class MoveToWaypoint(MonarchState):

    """State to move to a point."""

    def __init__(self, waypoint):
        """Constructor of the State Class.

        @param waypoint: dict-like definition of a waypoint. See example below.

        Example of a waypoint:

            >>> wp = {id: '1.3.C12_charger',
                      name: docking_station,
                      x: 14.23,
                      y: 33.95,
                      z: 0.0,
                      theta: 0}
        """
        MonarchState.__init__(self, state_outcomes=['succeeded', 'preempted'],
                              input_keys=['user_goal'],
                              output_keys=['user_goal'])
        rospy.loginfo("Init  MoveTo{}State".format(waypoint['name']))

        x, y, theta = butils.unpack_waypoint(waypoint)
        nav_move_to = MoveToAction(self, x, y, theta)
        self.add_action(nav_move_to)


# Get RFID tag State Machine
class GetRfidTagState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded', 'preempted'],
                              input_keys=['user_goal'],
                              output_keys=['user_goal'])
        rospy.loginfo("Init GetRfidTagState")
        topicName = 'rfid_tag'
        conditionOutcomes = ['succeeded']
        topicCondition = \
            TopicCondition(self, topicName, RfidReading, self.rfidCondition)
        self.add_change_condition(topicCondition, conditionOutcomes)

    def rfidCondition(self, data, userdata):
        print "TopicCondition userdata"
        rospy.loginfo('user_goal = '+str(userdata.user_goal))
        value = 'succeeded'
        return value


class StateDocking(smach.State):
    """Moves the robot back for 5 seconds to secure it to the charger."""
    def __init__(self):
        smach.State.__init__(self, outcomes=['completed'])
        self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

    def execute(self, userdata):
        self.teleop_srv(1, -0.2, 0)
        rospy.sleep(4)
        self.teleop_srv(0, 0, 0)
        return 'completed'


# Undock State Machine
class StateUndocking(smach.State):
    """Disconnects the robot from charger power and moves it forward."""
    def __init__(self):
        """Constructor."""
        smach.State.__init__(self, outcomes=['completed'])

        self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
        self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power",
                                            SetStateAuxiliaryPowerBattery,
                                            latch=True),
                            rospy.Publisher("set_state_aux_batt2_power",
                                            SetStateAuxiliaryPowerBattery,
                                            latch=True),
                            rospy.Publisher("set_state_electronics_power",
                                            SetStateElectronicPower,
                                            latch=True),
                            rospy.Publisher("set_state_motors_power",
                                            SetStateMotorsPower,
                                            latch=True)]

    def disconnect_from_power(self):
        RELAY_DELAY = 0.25
        (aux1, aux2, elec, moto) = self.undock_pubs
        rospy.sleep(RELAY_DELAY)
        aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
        rospy.sleep(RELAY_DELAY)
        aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
        rospy.sleep(RELAY_DELAY)
        elec.publish(SetStateElectronicPower(status=2))
        rospy.sleep(RELAY_DELAY)
        moto.publish(SetStateMotorsPower(status=2))
        rospy.sleep(RELAY_DELAY)

    def execute(self, userdata):
        self.disconnect_from_power()
        self.teleop_srv(1, 0.2, 0)
        rospy.sleep(4.0)
        self.teleop_srv(0, 0, 0)

        return 'completed'


######### Check batteries voltage State Machine
class BatteriesVoltageState(MonarchState):
    """State that monitors the batteries Voltage."""
    def __init__(self):
        """Constructor."""
        MonarchState.__init__(self, state_outcomes=['succeeded', 'preempted',
                                                    'batteries_ok',
                                                    'batteries_low', 'aborted'])
        rospy.loginfo("Init BatteriesVoltageState")
        topicName = 'batteries_voltage'
        # conditionOutcomes = ['succeeded', 'preempted', 'batteries_ok']
        conditionOutcomes = ['batteries_low', 'batteries_ok']
        topicCondition = TopicCondition(self, topicName,
                                        BatteriesVoltage,
                                        self.batteriesCondition)
        self.add_change_condition(topicCondition, conditionOutcomes)

    def batteriesCondition(self, data, userdata):
        """Check the level of the batteries to see if robot needs to charge."""
        if data.motors == 0.0:
            return 'batteries_ok'  # It's just that the back button is pressed.
        if data.electronics <= 12.5 or data.motors <= 12.5:
            return 'batteries_low'
        return 'batteries_ok'


# Restart State
class ChargingState(MonarchState):
    """Charging State.

        It has two conditions to exit:
            - RFID Tag received.
            - BatteriesVoltage indicates the robot is fully charged

        :TODO: implement transition when batteries charged
    """
    def __init__(self):
        """Constructor."""
        MonarchState.__init__(self,
                              state_outcomes=['succeeded', 'tag_detected',
                                              'charging', 'not_charging'])
        rospy.loginfo("Init ChargingState")

        topicCondition = TopicCondition(self, 'rfid_tag', RfidReading,
                                        self.rfidCondition)
        self.add_change_condition(topicCondition, ['tag_detected'])

        bat_condition = TopicCondition(self, 'batteries_voltage',
                                       BatteriesVoltage, self.charging_cond)
        self.add_change_condition(bat_condition, ['charging', 'not_charging'])

    def rfidCondition(self, data, userdata):
        """Notify when an RFID Tag has been detected."""
        value = 'tag_detected'
        return value

    def charging_cond(self, data, userdata):
        """Analize whether the robot is charging or not."""
        if data.charger > 0:
            return 'charging'
        return 'not_charging'


# Helper function
def sliding_window(seq, n=2):
    """Return a sliding window (of width n) over data from the iterable.

    Example:
    --------
    s -> (s0, s1, ...,s[n-1]), (s1, s2, ..., sn), ...
    """
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result


def make_patrolling_sm():
    """Create a Patrolling State Machine."""
    sm_patrolling = smach.StateMachine(outcomes=['preempted', 'aborted'])
    sm_patrolling.userdata.sm_patrolling_data = 0

    with sm_patrolling:
        # Get route waypoints
        places = next(rpyu.load_params('places'))
        route = next(rpyu.load_params('patrolling_route'))
        # waypoints = list(butils.get_route_waypoints(places, route))
        waypoints = [places[wp] for wp in route]
        waypoints.append(waypoints[0])  # After last wp, return to the 1st one.
        for wp, next_wp in sliding_window(waypoints):
            wp_name = 'MOVE_TO_{}'.format(wp['name'].upper())
            next_wp_name = 'MOVE_TO_{}'.format(next_wp['name'].upper())
            sm_patrolling.add(wp_name, MoveToWaypoint(wp),
                              transitions={'succeeded': next_wp_name},
                              remapping={'user_goal': 'sm_patrolling_data'})
    return sm_patrolling


# Main function
def main():
    rospy.init_node("patrolling_and_dock")

    # State Machine Patrolling
    sm_patrolling = make_patrolling_sm()

    # State Machine Get RFID
    sm_getrfid = smach.StateMachine(outcomes=['succeeded', 'preempted',
                                              'aborted'])
    sm_getrfid.userdata.sm_getrfid_data = 0
    with sm_getrfid:
        sm_getrfid.add('GETRFID', GetRfidTagState(),
                       transitions={'succeeded': 'succeeded',
                                    'preempted': 'succeeded'},
                       remapping={'user_goal': 'sm_getrfid_data'})

    # State Machine UnDock
    sm_undock = smach.StateMachine(outcomes=['succeeded',
                                             'preempted', 'aborted'])
    with sm_undock:
        sm_undock.add('UNDOCKING', StateUndocking(),
                      transitions={'completed': 'succeeded'})

#   State Machine Dock
    sm_dock = smach.StateMachine(outcomes=['succeeded', 'preempted', 'aborted'])
    with sm_dock:
        # sm_dock.add('GOTO_DOCK', SendDockState(),
        places = next(rpyu.load_params('places'))
        charger_waypoint = places['1.3.C12_charger']
        sm_dock.add('GOTO_DOCK',
                    MoveToWaypoint(charger_waypoint),
                    transitions={'succeeded': 'DOCKING'})
        sm_dock.add('DOCKING',
                    StateDocking(),
                    transitions={'completed': 'succeeded'})

    # State Machine for Charging
    sm_charging = smach.StateMachine(outcomes=['succeeded',
                                               'preempted', 'aborted'])
    with sm_charging:
        sm_charging.add('CHARGING', ChargingState(),
                        transitions={'tag_detected': 'preempted',
                                     'not_charging': 'preempted',
                                     'charging': 'CHARGING'})
        sm_charging.set_initial_state(['CHARGING'])

    # State Machine check batteries
    sm_batteries = smach.StateMachine(outcomes=['succeeded',
                                                'preempted', 'aborted'])
    with sm_batteries:
        sm_batteries.add('BATTERIES', BatteriesVoltageState(),
                         transitions={'batteries_ok': 'BATTERIES',
                                      'batteries_low': 'preempted'})
        # TODO: check if we need the rest of the transitions

    def termination_cm_cb(outcome_map):
        if any([outcome_map['BATTERIES_SM'] == 'suceeded',
                outcome_map['BATTERIES_SM'] == 'preempted',
                outcome_map['PATROLLING_SM'] == 'preempted',
                outcome_map['PATROLLING_SM'] == 'succeeded',
                outcome_map['PATROLLING_SM'] == 'aborted']):
            return True
        return False

    def outcome_cm_cb(outcome_map):
        if any([outcome_map['BATTERIES_SM'] == 'succeeded',
                outcome_map['BATTERIES_SM'] == 'preempted',
                outcome_map['PATROLLING_SM'] == 'preempted']):
            return 'preempted'
        if outcome_map['PATROLLING_SM'] == 'succeeded':
            return 'succeeded'
        if outcome_map['PATROLLING_SM'] == 'aborted':
            return 'aborted'
        return 'succeeded'

    cm = smach.Concurrence(outcomes=['succeeded', 'preempted', 'aborted'],
                           default_outcome='succeeded',
                           child_termination_cb=termination_cm_cb,
                           outcome_cb=outcome_cm_cb)
    cm.userdata.cm_data = 0

    with cm:
        cm.add('PATROLLING_SM', sm_patrolling)
        cm.add('BATTERIES_SM', sm_batteries)

    # Main State Machine
    sm_msm = smach.StateMachine(outcomes=['succeeded', 'preempted', 'aborted'])

    with sm_msm:
        # sm_msm.add('FIRST_DOCK', sm_dock,
        #            transitions={'succeeded': 'UNDOCK_SM'})
        sm_msm.add('UNDOCK_SM', sm_undock,
                   transitions={'succeeded': 'SIMPLE_PATROLLING'})
        sm_msm.add('SIMPLE_PATROLLING', cm,
                   transitions={'preempted': 'DOCK_SM'})
        sm_msm.add('DOCK_SM', sm_dock,
                   transitions={'succeeded': 'CHARGING_SM'})
        sm_msm.add('CHARGING_SM', sm_charging,
                   transitions={'succeeded': 'SIMPLE_PATROLLING',
                                'preempted': 'UNDOCK_SM',
                                'aborted': 'SIMPLE_PATROLLING'})
        sm_msm.set_initial_state(['CHARGING_SM'])

    sis = smach_ros.IntrospectionServer('patrolling_and_dock',
                                        sm_msm, '/SM_ROOT')
    sis.start()

    outcome = sm_msm.execute()


if __name__ == '__main__':
    main()
