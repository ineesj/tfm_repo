#!/usr/bin/env python

import rospy

from mosmach.monarch_state import MonarchState
from mosmach.actions.run_ca_action import RunCaAction

from monarch_msgs.msg import KeyValuePairArray
from monarch_msgs.msg import KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa

class RunGreetingCaState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'])
    rospy.loginfo("RunGreetingCaState")

    ca_dict = {"activated_cas":"ca15"}
    kvpa_msg = kvpa.from_dict(ca_dict)
    runGreetingCaState = RunCaAction(self, kvpa_msg)
    self.add_action(runGreetingCaState)
    

######### Main function
def main():
  rospy.init_node("run_greeting_ca")
  s1 = RunGreetingCaState()
  s1.execute('')

if __name__ == '__main__':
	main()
