#!/usr/bin/env python

# SYS
import sys
import os.path

# ROS
import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

# MOSMACH
from mosmach.util import pose2pose_stamped
from mosmach.monarch_state import MonarchState

# MOSMACH Actions
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.actions.runtime_action import RunTimeAction
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.actions.topic_writer_action import TopicWriterAction
from mosmach.actions.control_projector_action import ControlProjectorAction
from mosmach.actions.move_head_action import MoveHeadAction

# MOSMACH Actions Behaviours
from mosmach.actions.bm_ca_selectstoryboard import BMCASelectStoryboard
from mosmach.actions.bm_ca_wantplay import BMCAWantPlay
from mosmach.actions.bm_ca_givegreetings import BMCAGiveGreetings
from mosmach.actions.bm_teachingassistance import BMTeachingAssistance
from mosmach.actions.bm_getrfid import BMGetRfid
from mosmach.actions.bm_interactivegame import BMInteractiveGame

# MOSMACH Change Conditions
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.change_conditions.sam_condition import SamCondition
from mosmach.change_conditions.get_cap_sensors_condition import GetCapSensorsCondition

# Messages, services, utils
from move_base_msgs.msg import MoveBaseGoal
from gesture_player_utils.msg import GestureStatus

from monarch_msgs.msg import * #RfidReading, BatteriesVoltage, KeyValuePairArray, KeyValuePair, SetStateAuxiliaryPowerBattery, SetStateElectronicPower, SetStateMotorsPower
from monarch_msgs_utils import key_value_pairs as kvpa

from std_msgs.msg import *
from scout_msgs.msg import *
from scout_msgs.srv import *

 	
# Joyfull Warden - uncomment this for joyful_warden_demo
import joyful_warden_demo as JW


ROBOTS = [0,9]

#####################################################################
###################### State Machine Undock #########################
#####################################################################
class UndockState(smach.State):
	def __init__(self):
		"""UndockState disconnects the robot from charger power and moves it forward."""
		smach.State.__init__(self,outcomes = ['succeeded'])

		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
		self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_aux_batt2_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_electronics_power", SetStateElectronicPower, latch=True),
							rospy.Publisher("set_state_motors_power", SetStateMotorsPower, latch=True)]

	def disconnect_from_power(self):
		RELAY_DELAY = 0.25
		(aux1, aux2, elec, moto) = self.undock_pubs
		rospy.sleep(RELAY_DELAY)
		aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		elec.publish(SetStateElectronicPower(status=2))
		rospy.sleep(RELAY_DELAY)
		moto.publish(SetStateMotorsPower(status=2))
		rospy.sleep(RELAY_DELAY)

	def execute(self, userdata):
		self.disconnect_from_power()
		self.teleop_srv(1, 0.2, 0)
		rospy.sleep(2.0)
		self.teleop_srv(0, 0, 0)

		return 'succeeded'




#####################################################################
######################## State Machine Dock #########################
#####################################################################
class DockState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init DockState")

		self.add_action(MoveToAction(self, -0.36,0.50,-1.33)) # IPOL


class FinishDock(smach.State):
	def __init__(self):
		"""StateDock moves the robot back for 5 seconds to secure it to the charger."""
		smach.State.__init__(self, outcomes = ['succeeded'])
		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

	def execute(self, userdata):
		self.teleop_srv(1, -0.2, 0)
		rospy.sleep(4)
		self.teleop_srv(0, 0, 0)
		return 'succeeded'


class RestartState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['not_succeeded','tag_detected'])
		rospy.loginfo("Init RestartState")

		self.add_change_condition(TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition), ['tag_detected'])

	def rfidCondition(self, data, userdata):
		if data.tag_id == 5305:
			return 'tag_detected'
		else:
			return 'not_succeeded'




#####################################################################
##################### State Machine Patrolling ######################
#####################################################################
class ActivateCA15State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init ActivateCA15State")

		#di = {"activated_cas":"ca15"}
		#kvpa_msg = kvpa.from_dict(di)
		#self.add_action(RunCaAction(self, kvpa_msg))
		self.add_action(RunTimeAction(self,1))
		#self.add_action(BMCAGiveGreetings(self, robots=ROBOTS))


class TouchInteractionState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("TouchInteractionState")

		self.add_action(RunCeAction(self, self.greeting_name_callback, is_dynamic = True))
		self.add_action(RunTimeAction(self, 4))

	def greeting_name_callback(self, userdata):
		if userdata.sensor_activated == 'head_touched':
			userdata.sensor_activated = ''
			return 'mbot_warm_expression'
		elif userdata.sensor_activated == 'left_arm_touched':
			userdata.sensor_activated = ''
			return 'give_greetings_child'
		elif userdata.sensor_activated == 'right_arm_touched':
			userdata.sensor_activated = ''
			return 'mbot_follow_robot'
		elif userdata.sensor_activated == 'left_shoulder_touched':
			userdata.sensor_activated = ''
			return 'mbot_behavior_succeeded'
		elif userdata.sensor_activated == 'right_shoulder_touched':
			userdata.sensor_activated = ''
			return 'mbot_behavior_succeeded'


class MoveToWPState1(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP1State")

		self.add_action(MoveToAction(self, 1.55, -0.40, 1.65))
		self.add_action(RunCeAction(self, 'mbot_start_walking'))
		self.add_change_condition(GetCapSensorsCondition(self, self.cap_callback), ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


class RunCEState1(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		self.add_action(RunCeAction(self, 'mbot_warm_expression'))
		self.add_action(RunTimeAction(self, 2))


class MoveToWPState2(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP2State")

		self.add_action(MoveToAction(self, 1.05, 11.30, -3.06))
		self.add_action(RunCeAction(self, 'mbot_start_walking'))
		self.add_change_condition(GetCapSensorsCondition(self, self.cap_callback), ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


class RunCEState2(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		self.add_action(RunCeAction(self, 'mbot_give_greetings'))
		self.add_action(RunTimeAction(self, 2))


class MoveToWPState3(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP3State")

		self.add_action(MoveToAction(self, 0.40, 18.05, -1.50))
		self.add_action(RunCeAction(self, 'mbot_start_walking'))
		self.add_change_condition(GetCapSensorsCondition(self, self.cap_callback), ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


class RunCEState3(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		self.add_action(RunCeAction(self, 'mbot_give_greetings'))
		self.add_action(RunTimeAction(self, 2))


class MoveToWPState4(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP4State")

		self.add_action(MoveToAction(self, 0.85, 10.65, -1.52))
		self.add_action(RunCeAction(self, 'mbot_start_walking'))
		self.add_change_condition(GetCapSensorsCondition(self, self.cap_callback), ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


class RunCEState4(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		self.add_action(RunCeAction(self, 'mbot_warm_expression'))
		self.add_action(RunTimeAction(self, 2))


class MoveToWPState5(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP5State")

		self.add_action(MoveToAction(self, 1.80, -0.15, -2.93))
		self.add_action(RunCeAction(self, 'mbot_start_walking'))
		self.add_change_condition(GetCapSensorsCondition(self, self.cap_callback), ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


class RunCEState5(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE5State")

		self.add_action(RunCeAction(self, 'mbot_give_greetings'))
		self.add_action(RunTimeAction(self, 2))




#####################################################################
################### Batteries status + Get Rfid #####################
#####################################################################
class RFIDandBatteriesState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['dock','select_storyboard','batteries_low'])
		rospy.loginfo("Init RFIDandBatteriesState")

		self.add_change_condition(TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition), ['select_storyboard','dock'])
		self.add_change_condition(TopicCondition(self, 'batteries_voltage', BatteriesVoltage, self.batteriesCondition), ['batteries_low'])
		self.add_change_condition(TopicCondition(self, '/sar/mbot09/error_found', Int64, self.errorFoundCondition), ['dock'])
		
	def rfidCondition(self, data, userdata):
		print data.tag_id

		while True:
			if data.tag_id == 5284:
				value = 'dock'
				break
			elif data.tag_id == 5267:
				value = 'select_storyboard'
				break
			else:
				rospy.sleep(0.1)
		return value

	def batteriesCondition(self, data, userdata):
		while True:
			if data.electronics <= 12.0 or data.motors <= 12.0:
				value = 'batteries_low'
				break
			else:
				rospy.sleep(0.1)
		return value

	def errorFoundCondition(self, data, userdata):
	 	while True:
	 		# 1 - Button pushed
	 		# 2 - Laser readings missing
	 		# 3 - Robot lost
	 		# 4 - Bateries bellow TH
	 		# 5 - Ping lost or too high
	 		# 6 - Bump
	 		# 7 - Should be moving but it isnt
	 		# 8 - Should have docked but failed
	 		# 9 - Should have undocked but failed 
	 		# 10 - Both lasers failed! robot stopped!
	 		if data.data == 4:
				value = 'dock'
				print "@@@@@@@@@@@@@@@@@@@@@@ Bateries bellow TH"
				break

			else:
				if data.data == 1:
					print "@@@@@@@@@@@@@@@@@@@@@@ Button pushed"

				elif data.data == 2:
					print "@@@@@@@@@@@@@@@@@@@@@@ Laser readings missing"

				elif data.data == 3:
					print "@@@@@@@@@@@@@@@@@@@@@@ Robot lost"

				elif data.data == 5:
					print "@@@@@@@@@@@@@@@@@@@@@@ Ping lost or too high"

				elif data.data == 6:
					print "@@@@@@@@@@@@@@@@@@@@@@ Bump"

				elif data.data == 7:
					print "@@@@@@@@@@@@@@@@@@@@@@ Should be moving but it isn't"

				elif data.data == 8:
					print "@@@@@@@@@@@@@@@@@@@@@@ Should have docked but failed"

				elif data.data == 9:
					print "@@@@@@@@@@@@@@@@@@@@@@ should have undocked but failed"

				elif data.data == 10:
					print "@@@@@@@@@@@@@@@@@@@@@@ Both lasers failed! Robot stopped!"
	 			
	 			rospy.sleep(0.1)

	 	return value


#####################################################################
###################### Storyboard State Machine #####################
#####################################################################
class CAStoryBoardState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init CAStoryBoardState")
		
		di = {'activated_cas':'ca01','question_to_user':'select_storyboard'}
		kvpa_msg = kvpa.from_dict(di)
		self.add_action(RunCaAction(self, kvpa_msg))


class WaitCAStoryBoardState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init WaitCAStoryBoardState")

		self.add_action(RunTimeAction(self,8))


class StoryboardState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['interactive_game','joyful_warden','teaching_assistant'])
		rospy.loginfo("Init StoryboardState")

		self.add_change_condition(TopicCondition(self, 'screen_touched', KeyValuePairArray, self.CommandCondition), ['interactive_game','joyful_warden','teaching_assistant'])

	def CommandCondition(self, data, userdata):
		while True:
			for n in range(0,len(data.array)):
				if data.array[n].key == 'storyboard':
					value = data.array[n].value
					break

			if value != '':
				break
			else:
				rospy.sleep(0.1)

		return value


class GetChildRfidState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init GetChildRfidState")

		self.add_action(RunTimeAction(self, 5))

	def person_id_cb(self, data, userdata):
		person_id = 1001
		return person_id



#####################################################################
################# Interactive Game State Machine ####################
#####################################################################
class AnnouceGameState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init AnnouceGameState")

		self.add_action(RunCeAction(self, 'mbot_warm_expression'))
		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

	def execute(self, userdata):
		self.teleop_srv(2, 1, 0) #cmd= 2 rotate, vel = 0.2
		rospy.sleep(15)
		self.teleop_srv(0, 0, 0)
		return 'succeeded'


class GameSafeZoneState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init GameSafeZoneState")

		self.add_action(MoveToAction(self, -2.10,-7.30,-1.69)) #IPOL


class CAAskGameState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init CAAskGameState")
		
		di = {'activated_cas':'ca01','question_to_user':'want_play_game','name_of_game':'interactive_game'}
		kvpa_msg = kvpa.from_dict(di)
		self.add_action(RunCaAction(self, kvpa_msg))


class AskGameState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['yes','no'])
		rospy.loginfo("Init AskGameState")

		#self.add_change_condition(SamCondition(self, 'command_from_user', 'mbot09', self.gameCondition), ['yes','no'])
		self.add_change_condition(TopicCondition(self, 'screen_touched', KeyValuePairArray, self.gameCondition), ['yes','no'])

	def gameCondition(self, data, userdata):
		while True:
			for n in range(0,len(data.array)):
				if data.array[n].key == 'yes_no_answer':
					value = data.array[n].value
					break

			if value != '':
				break				
			else:
				rospy.sleep(0.1)

		return value


class InteractiveGameStartState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init InteractiveGameStartState")

		self.add_action(TopicWriterAction(self, '/sar/mcentral/behavior_to_request', String, 'interactive_game'))


class WaitGameStartState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init WaitGameStartState")

		self.add_action(RunTimeAction(self, 5))


class InteractiveGameFinishState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init InteractiveGameFinishState")

		self.add_change_condition(TopicCondition(self, '/sar/mcentral/behavior_status', String, self.waitInteractiveGameCondition), ['succeeded'])


	def waitInteractiveGameCondition(self, data, userdata):
		return 'succeeded'


#####################################################################
################### Joyful Warden State Machine #####################
#####################################################################
class JoyfulWardenState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init JoyfulWardenState")

		self.add_action(RunTimeAction(self, 5))



#####################################################################
############### Teaching Assistance State Machine ###################
#####################################################################
class TeachingSafeZoneState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init TeachingSafeZoneState")
		self.add_action(MoveToAction(self,-2.10,-9.00,-0.93)) #IPOL


class EscortState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init EscortState")
		self.add_action(RunTimeAction(self, 5))


class MoveToProjectionAreaState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Move to Projection Area State")
        self.add_action(MoveToAction(self,-0.15,-12.25,-1.57))


class DisableHRIInterfacesState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("DisableHRIInterfacesState")
        self.add_action(TopicWriterAction(self, 'allowed_hri_interfaces', String, 'arms|audio|cheeks|eyes|image|leds_base|mouth|projector|voice'))


class MoveHeadRightState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("MoveHeadRightState")
        self.add_action(MoveHeadAction(self, 1.5, 50))


class MoveHeadCentralState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("MoveHeadCentralState")
        self.add_action(MoveHeadAction(self, 0, 50))


class AllowHRIInterfacesState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("AllowHRIInterfacesState")
        self.add_action(TopicWriterAction(self, 'allowed_hri_interfaces', String, 'arms|audio|cheeks|eyes|head|image|leds_base|mouth|projector|voice'))


class TurnOnTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn On the Projector State")
        self.add_action(ControlProjectorAction(self, True))


class TurnOffTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn Off the Projector State")
        self.add_action(ControlProjectorAction(self, False))


class TeachingAssistanceCaActivationState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Teaching Assistance State")

        kvpa = KeyValuePairArray()
        kvp = KeyValuePair()
        kvp.key = 'activated_cas'
        kvp.value = 'ca14.4'
        kvpa.array.append(kvp)
        kvp = KeyValuePair()
        kvp.key = 'media_menu'
        kvp.value = 'euronews_menu'
        kvpa.array.append(kvp)

        self.add_action(RunCaAction(self, kvpa))


class TeachingAssistanceState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['finished', 'reactivate'])
        rospy.loginfo("Teaching Assistance State")

        self.add_change_condition(TopicCondition(self, 'screen_touched', KeyValuePairArray, self.finishCondition), ['finished', 'reactivate'])

    def finishCondition(self, data, userdata):
        keyValEl0 = data.array[0]

        if keyValEl0.key == 'command':
            if keyValEl0.value == 'stop_projecting':
                value = 'finished'

            elif keyValEl0.value == 'select_media_content':
                value = 'reactivate'

            else:
                value = 'reactivate'

        else:
            value = 'reactivate'

        return value


class WaitTeachingAssistanceCaActivationState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("WaitTeachingAssistanceCaActivationState")

        self.add_action(RunTimeAction(self,10))


#####################################################################
############################ Main function ##########################
#####################################################################
def main():
	rospy.init_node("IPOL_demo")


	#####################################################################
	##################### State Machine Patrolling ######################
	#####################################################################
	sm_patrolling = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	sm_patrolling.userdata.sm_sensor_activated = 0
	number_of_waypoints = 5
	next_ca = 'CA1'
	with sm_patrolling:
		for i in range(1,number_of_waypoints+1):
			if i >= number_of_waypoints:
				next_ca = 'CA1'
			else:
				next_ca = 'CA%s'%(i+1)
			sm_patrolling.add('CA%s'%i, ActivateCA15State(), transitions = {'succeeded':'SelectAndMoveTo%s'%i})
			sm_patrolling.add('SelectAndMoveTo%s'%i, globals()['MoveToWPState%s'%i](), transitions = {'succeeded':'CE%s'%i,'tag_detected':'succeeded','touch_detected':'TouchInteract%s'%i}, remapping={'sensor_activated':'sm_sensor_activated'})
			sm_patrolling.add('CE%s'%i, globals()['RunCEState%s'%i](), transitions = {'succeeded':next_ca})
			sm_patrolling.add('TouchInteract%s'%i, TouchInteractionState(), transitions={'succeeded':'SelectAndMoveTo%s'%i}, remapping={'sensor_activated':'sm_sensor_activated'})




	#####################################################################
	######################## State Machine Dock #########################
	#####################################################################
	dock_sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with dock_sm:
		dock_sm.add('DOCK',DockState(),transitions = {'succeeded':'FINISH'})
		dock_sm.add('FINISH',FinishDock(),transitions = {'succeeded':'RESTART'})
		dock_sm.add('RESTART',RestartState(),transitions = {'tag_detected':'succeeded','not_succeeded':'RESTART'})




	#####################################################################
	############ Patrolling + Batteries status + Get Rfid ###############
	#####################################################################
	def termination_cm_cb(outcome_map):
		if outcome_map['BATTERIES_RFID'] == 'dock':
			return True
		if outcome_map['BATTERIES_RFID'] == 'select_storyboard':
			return True
		if outcome_map['BATTERIES_RFID'] == 'batteries_low':
			return True
		if outcome_map['PATROLLING'] == 'succeeded':
			return True
		if outcome_map['PATROLLING'] == 'aborted':
			return True
		else:
			return False
	
	def outcome_cm_cb(outcome_map):
		if outcome_map['BATTERIES_RFID'] == 'dock':
			return 'preempted'
		if outcome_map['BATTERIES_RFID'] == 'select_storyboard':
			return 'select_storyboard'
		if outcome_map['BATTERIES_RFID'] == 'batteries_low':
			return 'preempted'
		if outcome_map['PATROLLING_SM'] == 'succeeded':
			return 'succeeded'
		if outcome_map['PATROLLING_SM'] == 'aborted':
			return 'preempted'
		else:
			return 'succeeded'

	patrolling_sm = smach.Concurrence(outcomes = ['succeeded','select_storyboard','preempted'], default_outcome = 'select_storyboard', child_termination_cb = termination_cm_cb, outcome_cb = outcome_cm_cb)
	with patrolling_sm:
		patrolling_sm.add('PATROLLING',sm_patrolling)
		patrolling_sm.add('BATTERIES_RFID',RFIDandBatteriesState())




	#####################################################################
	################# Interactive Game State Machine ####################
	#####################################################################
	interactive_game_sm = smach.StateMachine(outcomes = ['succeeded'])
	with interactive_game_sm:
		interactive_game_sm.add('ANNOUNCE_GAME', AnnouceGameState(), transitions={'succeeded':'MOVE_TO_SAFE_ZONE1'})
		interactive_game_sm.add('MOVE_TO_SAFE_ZONE1', GameSafeZoneState(), transitions={'succeeded':'CA_ASK'})
		interactive_game_sm.add('CA_ASK', CAAskGameState(), transitions={'succeeded':'ASK_WANT_TO_PLAY'})
		interactive_game_sm.add('ASK_WANT_TO_PLAY', AskGameState(), transitions={'yes':'START_GAME','no':'succeeded'})
		interactive_game_sm.add('START_GAME', InteractiveGameStartState(), transitions={'succeeded':'WAIT_GAME'})
		interactive_game_sm.add('WAIT_GAME', WaitGameStartState(), transitions={'succeeded':'FINISH_GAME'})
		interactive_game_sm.add('FINISH_GAME', InteractiveGameFinishState(), transitions={'succeeded':'MOVE_TO_SAFE_ZONE2'})
		interactive_game_sm.add('MOVE_TO_SAFE_ZONE2', GameSafeZoneState(), transitions={'succeeded':'CA_ASK'})




	#####################################################################
	############### Teaching Assistance State Machine ###################
	#####################################################################
	teaching_assistance_sm = smach.StateMachine(outcomes = ['succeeded'])
	with teaching_assistance_sm:
		teaching_assistance_sm.add('MOVE_TO_SAFE_ZONE1', TeachingSafeZoneState(), transitions={'succeeded':'MOVE_TO_PROJECTION_AREA'})
		teaching_assistance_sm.add('MOVE_TO_PROJECTION_AREA', MoveToProjectionAreaState(), transitions={'succeeded':'DISABLE_HRI'})
		teaching_assistance_sm.add('DISABLE_HRI', DisableHRIInterfacesState(), transitions={'succeeded':'MOVE_HEAD_RIGHT'})
		teaching_assistance_sm.add('MOVE_HEAD_RIGHT', MoveHeadRightState(), transitions={'succeeded':'TURN_PROJECTOR_ON'})
		teaching_assistance_sm.add('TURN_PROJECTOR_ON', TurnOnTheProjectorState(), transitions={'succeeded':'TA_CA'})
		teaching_assistance_sm.add('TA_CA', TeachingAssistanceCaActivationState(), transitions={'succeeded':'TA'})
		teaching_assistance_sm.add('TA', TeachingAssistanceState(), transitions={'finished':'TURN_PROJECTOR_OFF', 'reactivate':'REACTIVATE'})
		teaching_assistance_sm.add('REACTIVATE', WaitTeachingAssistanceCaActivationState(), transitions={'succeeded':'TA_CA'})
		teaching_assistance_sm.add('TURN_PROJECTOR_OFF', TurnOffTheProjectorState(), transitions={'succeeded':'MOVE_HEAD_CENTER'})
		teaching_assistance_sm.add('MOVE_HEAD_CENTER', MoveHeadCentralState(), transitions={'succeeded':'ALLOW_HRI'})
		teaching_assistance_sm.add('ALLOW_HRI', AllowHRIInterfacesState(), transitions={'succeeded':'MOVE_TO_SAFE_ZONE2'})
		teaching_assistance_sm.add('MOVE_TO_SAFE_ZONE2', TeachingSafeZoneState(), transitions={'succeeded':'succeeded'})




	#####################################################################
	###################### Storyboard State Machine #####################
	#####################################################################
	storyboard_sm = smach.StateMachine(outcomes = ['succeeded'])
	with storyboard_sm:
		storyboard_sm.add('WAITDISPATCHCA', WaitCAStoryBoardState(), transitions={'succeeded':'DISPATCHCA'})
		storyboard_sm.add('DISPATCHCA', CAStoryBoardState(), transitions={'succeeded':'STORYBOARD_SELECTED'})	
		storyboard_sm.add('STORYBOARD_SELECTED', StoryboardState(), transitions={'interactive_game':'INTERACTIVE_GAME','joyful_warden':'JOYFUL_WARDEN','teaching_assistant':'TEACHING_ASSISTANCE'})
		storyboard_sm.add('INTERACTIVE_GAME', interactive_game_sm, transitions={'succeeded':'succeeded'})
		storyboard_sm.add('TEACHING_ASSISTANCE', teaching_assistance_sm, transitions={'succeeded':'succeeded'})
		
		# Comment this state for joyful_warden_demo
		#storyboard_sm.add('JOYFUL_WARDEN', JoyfulWardenState(), transitions={'succeeded':'succeeded'})
		# Uncomment this state for joyful_warden_demo
		storyboard_sm.add('JOYFUL_WARDEN', JW.jw_sm(), transitions={'succeeded':'WAITDISPATCHCA'})
		

	#####################################################################
	####################### Main State Machine ##########################
	#####################################################################
	main_sm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])
	with main_sm:
		main_sm.add('UNDOCK_SM', UndockState(), transitions={'succeeded':'PATROLLING_SM'})
		main_sm.add('PATROLLING_SM', patrolling_sm, transitions={'preempted':'DOCK_SM','select_storyboard':'STORYBOARD_SM'})
		main_sm.add('STORYBOARD_SM', storyboard_sm, transitions={'succeeded':'PATROLLING_SM'})
		main_sm.add('DOCK_SM', dock_sm, transitions={'succeeded':'UNDOCK_SM'})


	sis = smach_ros.IntrospectionServer('IPOL_demo', main_sm, '/SM_ROOT')
	sis.start()
	
	outcome = main_sm.execute()

	rospy.spin()

if __name__ == '__main__':
	main()
