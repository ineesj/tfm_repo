#! /usr/bin/env python

# Author: Jose Nuno Pereira
# Description: Creates state machine that forces sequence of behaviors sequentially.

import roslib; roslib.load_manifest('mosmach')
import rospy
import math
import smach
import smach_ros
from functools import partial

from std_msgs.msg import *
from monarch_behaviors.srv import BehaviorRequest
from monarch_msgs.msg import BehaviorSelection, KeyValuePair, KeyValuePairArray

from sam_helpers.reader import SAMReader

from mosmach.monarch_state import MonarchState
from mosmach.actions.topic_writer_action import TopicWriterAction
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.bm_interactivegame import BMInteractiveGame
from mosmach.actions.bm_idle import BMIdle
from mosmach.actions.runtime_action import RunTimeAction
		 

class WaitBehaviorState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['interactive_game','aborted','preempted'], input_keys=['behavior_name'], output_keys=['behavior_name'])
		rospy.loginfo("Init WaitBehaviorState")

		self.add_change_condition(TopicCondition(self, '/sar/mcentral/behavior_to_request', String, self.waitBehaviorCondition), ['interactive_game','aborted'])

	def waitBehaviorCondition(self, data, userdata):
		userdata.behavior_name = data.data
		if data.data == 'interactive_game':
			return data.data
		else:
			return 'aborted'


class InteractiveGameState(MonarchState):
	"""Executes interactivegame behavior"""
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted'])
		rospy.loginfo("Init InteractiveGameState")

		self.add_action(BMInteractiveGame(self, robots=[0,9], players='mbot09 human2', rows='3', cols='4', level='0', tracking='1', interaction='none', active=True))


class WaitInteractiveGameToFinishState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','waiting','preempted'])
		rospy.loginfo("Init WaitInteractiveGameToFinishState")

		self.add_change_condition(TopicCondition(self, '/sar/mpc01/CurrentBehaviorResult', KeyValuePairArray, self.waitInteractiveGameToFinishCB), ['succeeded','waiting'])

	def waitInteractiveGameToFinishCB(self, data, userdata):
		for n in range(0,len(data.array)):
			if data.array[n].key == 'behavior_name':
				if data.array[n].value == 'interactivegame':
					return 'succeeded'
				else:
					return 'waiting'
			else:
				return 'waiting'			


class SetRobotIdleState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init SetRobotIdleState")

		self.add_action(BMIdle(self, robots=[9]))


class SetGamePreemptState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init SetGamePreemptState")

		self.add_action(BMInteractiveGame(self, robots=[0,9], players='mbot09 human2', rows='3', cols='4', level='0', tracking='1', interaction='none', active=False))


class WaitSetRobotIdleState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init WaitSetRobotIdleState")

		self.add_action(RunTimeAction(self, 5))

class InteractiveGameFinishState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init InteractiveGameFinishState")

		self.add_action(TopicWriterAction(self, '/sar/mcentral/behavior_status', String, 'finished'))


class RestartMainSM(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RestartMainSM")

		self.add_action(RunTimeAction(self, 2))


class CancelState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init CancelState")

		self.add_change_condition(TopicCondition(self, 'cancel_plan', Bool, self.cancel_cb), ['succeeded','preempted'])

	def cancel_cb(self, data, userdata):
		if data.data == True:
			return 'preempted'
		else:
			return 'succeeded'


#####################################################################
############################ Main function ##########################
#####################################################################
def main():
	rospy.init_node('IPOL_demo_server')

	sm_sequence = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_sequence:
		sm_sequence.add('WAIT_REQUEST', WaitBehaviorState(), transitions = {'interactive_game':'INTERACTIVEGAME','aborted':'WAIT_REQUEST'})
		sm_sequence.add('INTERACTIVEGAME', InteractiveGameState(), transitions = {'succeeded':'WAIT_TO_FINISH','aborted':'WAIT_REQUEST','preempted':'preempted'})
		sm_sequence.add('WAIT_TO_FINISH', WaitInteractiveGameToFinishState(), transitions = {'succeeded':'WAIT_SET_ROBOT_IDLE','waiting':'WAIT_TO_FINISH'})
		sm_sequence.add('WAIT_SET_ROBOT_IDLE', WaitSetRobotIdleState(), transitions = {'succeeded':'SET_ROBOT_IDLE'})
		sm_sequence.add('SET_ROBOT_IDLE', SetRobotIdleState(), transitions = {'succeeded':'FINISH_GAME'})
		sm_sequence.add('FINISH_GAME', InteractiveGameFinishState(), transitions = {'succeeded':'WAIT_REQUEST'})
	

	def term_cb(outcome_map):
		return True

	def out_cb(outcome_map):
		if outcome_map['SEQUENCE'] == 'succeeded':
			return 'succeeded'
		if outcome_map['CANCELSEQUENCE'] == 'preempted':
			return 'preempted'
		if outcome_map['CANCELSEQUENCE'] == 'succeeded':
			return 'succeeded'
		else:
			return 'succeeded'

	sm_conc = smach.Concurrence(outcomes = ['succeeded', 'preempted'], default_outcome = 'succeeded', child_termination_cb = term_cb, outcome_cb = out_cb)

	with sm_conc:
		sm_conc.add('SEQUENCE',sm_sequence)
		sm_conc.add('CANCELSEQUENCE',CancelState())


	sm_main = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_main:
		sm_main.add("MAIN_SM", sm_conc, transitions = {'succeeded':'MAIN_SM','preempted':'PREEMPT_GAME'})
		sm_main.add("PREEMPT_GAME", SetGamePreemptState(), transitions = {'succeeded':'WAIT_SET_ROBOT_IDLE'})
		sm_main.add('WAIT_SET_ROBOT_IDLE', WaitSetRobotIdleState(), transitions = {'succeeded':'SET_ROBOT_IDLE'})
		sm_main.add('SET_ROBOT_IDLE', SetRobotIdleState(), transitions = {'succeeded':'FINISH_GAME'})
		sm_main.add('FINISH_GAME', InteractiveGameFinishState(), transitions = {'succeeded':'MAIN_SM'})


	sm_main.execute()
	rospy.spin()

if __name__ == '__main__':
	main()