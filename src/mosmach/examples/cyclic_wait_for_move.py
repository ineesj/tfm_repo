#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from geometry_msgs.msg import Point
from move_base_msgs.msg import MoveBaseGoal
from mosmach.monarch_state import MonarchState
from mosmach.change_condition import ChangeCondition
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.util import *

class ScreenNavigationPositionState(MonarchState):

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted'], input_keys=['user_goal','goal_set'],output_keys=['user_goal','goal_set'])
		rospy.loginfo("ScreenNavigationPositionState")	

		moveToAction = MoveToAction(self, data_cb=self.set_goal_cb, is_dynamic = True)
		self.add_action(moveToAction)
		topicReader = TopicReaderAction(self, 'screen_send_nav', Point, self.topicReader_cb)
		self.add_action(topicReader)

	def set_goal_cb(self, userdata):
		while userdata.goal_set == False:
			continue
		userdata.goal_set = False
		return userdata.user_goal
		
	def topicReader_cb(self, data, userdata):
		if userdata.goal_set == False:
			userdata.user_goal = pose2pose_stamped(data.x,data.y,data.z)
			userdata.goal_set = True
			return userdata.user_goal


######### Main function
def main():
	rospy.init_node("cyclic_wait_for_move")

	# State Machine     	
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	sm.userdata.sm_user_goal_data = MoveBaseGoal()
	sm.userdata.sm_goal_set_data = False

	with sm:
		sm.add('ScreenNavigationState',
			   ScreenNavigationPositionState(),
			   transitions = {'succeeded':'ScreenNavigationState'},
			   remapping={'user_goal':'sm_user_goal_data',
			              'goal_set':'sm_goal_set_data'})
	
	outcome = sm.execute()

if __name__ == '__main__':	
	main()
