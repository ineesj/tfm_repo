#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.actions.topic_writer_action import TopicWriterAction
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.topic_writer_action import TopicWriterAction
from mosmach.actions.control_projector_action import ControlProjectorAction
from mosmach.actions.move_head_action import MoveHeadAction
from monarch_msgs.msg import KeyValuePairArray
from monarch_msgs.msg import KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from std_msgs.msg import String

class IdleState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init TA IdleState")

        # TODO: display screen so the teacher can give projection command
        dipslay_action_name = "TODO: some CA, CE for projection"
        runCeState = RunCeAction(self, display_action_name)
        self.add_action(runCeState)

### Navigation states ###
class MoveToProjectionAreaState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Move to Projection Area State")

        #Enter right coordinates
        navMoveToProjectionArea = MoveToAction(self,0.20,-14.37,1.64)
        self.add_action(navMoveToProjectionArea)

        # Publish the topic that allows interfaces to be used
        pubOnTopicAction = TopicWriterAction(self, 'allowed_hri_interfaces', String, 'arms|audio|cheeks|eyes|image|leds_base|mouth|projector|voice')
        self.add_action(pubOnTopicAction)

class MoveToHomeAreaState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Move to Home Area State")
        
        #TODO: enter the right coordinates
        navMoveToHomeArea = MoveToAction(self,-0.55,-10.75,-1.39)
        self.add_action(navMoveToProjectionArea)

### Head Movement ###
class MoveHeadRightState(MonarchState):
    def __init__(self):
	MonarchState.__init__(self, state_outcomes=['succeeded'])
	rospy.loginfo("MoveHeadState")
	moveHeadAction = MoveHeadAction(self, -1.6, 50)
	self.add_action(moveHeadAction)


### TODO: Fobbid Head Movement ###
class ForbidMoveHeadState(MonarchState):
    def __init__(self):
	MonarchState.__init__(self, state_outcomes=['succeeded'])
	rospy.loginfo("MoveHeadState")
	moveHeadAction = MoveHeadAction(self, -1.6, 50)
	self.add_action(moveHeadAction)


### Projector Control ###
class TurnOnTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn On the Projector State")
        
        turnOnTheProjectorAction = ControlProjectorAction(self, True)
        self.add_action(turnOnTheProjectorAction)

class TurnOffTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn Off the Projector State")

        turnOffTheProjectorAction = ControlProjectorAction(self, False)
        self.add_action(turnOffTheProjectorAction)

        

class TeachingAssistanceCaActivationState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Teaching Assistance State")

##1 Activate state to show the menu
        di = {"activated_cas":"ca14.4"}
        kvpa_msg = kvpa.from_dict(di)
        activationCA = RunCaAction(self, kvpa_msg)
        self.add_action(activationCA)

class TeachingAssistanceState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['finished', 'reactivate'])
        rospy.loginfo("Teaching Assistance State")
        
##2 Is TA finished?
        topicCondition = TopicCondition(self, 'command_from_user', KeyValuePairArray, self.finishCondition)
        self.add_change_condition(topicCondition, ['finished', 'reactivate'])

    def finishCondition(self, data, userdata):
	rospy.loginfo("Finish Condition Callback")
	keyValEl0 = data.array[0]
	if keyValEl0.key == 'command':
            if keyValEl0.value == 'stop_projecting':
		value = 'finished'
                #rospy.loginfo("stop projecting - returning fiinsh")
            elif keyValEl0.value == 'select_media_content':
		value = 'reactivate'
                #rospy.loginfo("reactivating - returning reactivate")
	    else:
		value = 'reactivate'
        else:
	   value = 'reactivate'

        rospy.sleep(5)
        return value

##3 Is projecting button pressed?

def main():
	rospy.init_node("teaching_assistance_demo")

        # State TA
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
        
        with sm:
            sm.add('MoveToProjectionArea', MoveToProjectionAreaState(), transitions={'succeeded':'MoveHeadRight'})
            sm.add('MoveHeadRight', MoveHeadRightState(), transitions={'succeeded':'TurnOnTheProjector'})
            sm.add('TurnOnTheProjector', TurnOnTheProjectorState(), transitions={'succeeded':'TeachingAssistanceCaActivation'})
            sm.add('TeachingAssistanceCaActivation', TeachingAssistanceCaActivationState(), transitions={'succeeded':'TeachingAssistance'})
            sm.add('TeachingAssistance', TeachingAssistanceState(), transitions={'finished':'TurnOffTheProjector', 'reactivate':'TeachingAssistanceCaActivation'})
            sm.add('TurnOffTheProjector', TurnOffTheProjectorState(), transitions={'succeeded':'succeeded'})
            

        outcome = sm.execute()
        rospy.spin()


if __name__ == '__main__':
    main()

