#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.get_topic_action import GetTopicAction
from mosmach.actions.set_topic_action import SetTopicAction

from monarch_msgs.msg import RfidReading

#import patrolling_and_dock

# State Machine
class GetTopicDataState(MonarchState):

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'], input_keys=[''], output_keys=['rfid_tag_output'])
		rospy.loginfo("Init GetTopicDataState")

		topicName = 'rfid_tag'

		getTopicDataAction = GetTopicAction(self, topicName, RfidReading)
		self.add_action(getTopicDataAction)

class SetTopicDataState(MonarchState):

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=[''], output_keys=[''])
		rospy.loginfo("Init GetTopicDataState")

		topicName = 'rfid_tag'
		message = RfidReading()
		message.tag_id = 100000

		setTopicDataAction = SetTopicAction(self, topicName, RfidReading, message)
		self.add_action(setTopicDataAction)


def reuse_behavior(behavior_name):
	imported_behavior = __import__(behavior_name)
	return imported_behavior


# Main function
def main():
	#rospy.init_node("behaviors_reuse_test")
	
	# State Machine   
	counter = 1
	
	while True:
		patrolling_and_dock.main()
		rospy.loginfo("Patrolling and dock end (%d).. restart",counter)
		if counter == 5:
			rospy.loginfo("Bye bye...")
			break
		counter = counter + 1

	return 0

if __name__ == '__main__':
	patrolling_and_dock = reuse_behavior("patrolling_and_dock")
	main()
