#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_arms_action import MoveArmsAction
from mosmach.actions.move_head_action import MoveHeadAction

class MoveArmsUpState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("MoveArmsUpState")
		moveArmsAction = MoveArmsAction(self, True, -1.5, -1.5, 2)
		self.add_action(moveArmsAction)
	

class MoveHeadLeftState(MonarchState):

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("MoveHeadState")
		moveHeadAction = MoveHeadAction(self, 1, 50)
		self.add_action(moveHeadAction)


class MoveArmsDownState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("MoveArmsUpState")
		moveArmsAction = MoveArmsAction(self, True, 1.5, 1.5, 2)
		self.add_action(moveArmsAction)


class MoveHeadRightState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("MoveHeadState")
		moveHeadAction = MoveHeadAction(self, -1, 50)
		self.add_action(moveHeadAction)


######### Main function
def main():
	rospy.init_node("cyclic_arm_and_head")
	
	# State Machine     
	
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	
	with sm:
		sm.add('ARMSUP',MoveArmsUpState(),transitions = {'succeeded':'HEADLEFT'})
		sm.add('HEADLEFT',MoveHeadLeftState(),transitions = {'succeeded':'ARMSDOWN'})
		sm.add('ARMSDOWN',MoveArmsDownState(),transitions = {'succeeded':'HEADRIGHT'})
		sm.add('HEADRIGHT',MoveHeadRightState(),transitions = {'succeeded':'ARMSUP'})
		
	outcome = sm.execute()


if __name__ == '__main__':	
	main()
