#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.get_cap_sensors_action import GetCapSensorsAction
from mosmach.actions.run_ce_action import RunCeAction


class GetCapSensorsActionState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("GetCapSensorsActionState")
    
    getCapSensorsAction = GetCapSensorsAction(self, self.cap_callback)
    self.add_action(getCapSensorsAction)

  def cap_callback(self, data, userdata):
    if data.head == True:
      userdata.sensor_activated = 'head_touched'
    elif data.left_shoulder == True:
      userdata.sensor_activated = 'left_shoulder_touched'
    elif data.right_shoulder == True:
      userdata.sensor_activated = 'right_shoulder_touched'
    elif data.left_arm == True:
      userdata.sensor_activated = 'left_arm_touched'
    elif data.right_arm == True:
      userdata.sensor_activated = 'right_arm_touched'


class TouchInteractionState(MonarchState):
  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("TouchInteractionState")

    runCeState = RunCeAction(self, self.greeting_name_callback, is_dynamic = True)
    self.add_action(runCeState)

  def greeting_name_callback(self, userdata):
    if userdata.sensor_activated == 'head_touched':
      return 'mbot_warm_expression'
    elif userdata.sensor_activated == 'left_arm_touched':
      return 'give_greetings_child'
    elif userdata.sensor_activated == 'right_arm_touched':
      return 'mbot_follow_robot'
    elif userdata.sensor_activated == 'left_shoulder_touched':
      return 'mbot_behavior_succeeded'
    elif userdata.sensor_activated == 'right_shoulder_touched':
      return 'mbot_behavior_succeeded'


######### Main function
def main():
  rospy.init_node("get_cap_data")


  # State Machine Patrolling
  sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
  sm.userdata.sm_sensor_activated = 0

  with sm:
    #for i in range(1,3):

      #if i >= max(range(1,3)):
      #  next_get_cap_sensor = 'GetCapSensors1'
      #else:  
     #  next_get_cap_sensor = 'GetCapSensors%s'%(i+1)

     # next_touch_interaction = 'TouchInteraction%s'%i

      sm.add('GetCapSensors', GetCapSensorsActionState(), transitions={'succeeded':'TouchInteraction'}, remapping={'sensor_activated':'sm_sensor_activated'})
      sm.add('TouchInteraction', TouchInteractionState(), transitions={'succeeded':'succeeded'}, remapping={'sensor_activated':'sm_sensor_activated'})

  #sis = smach_ros.IntrospectionServer('get_cap_data', sm, '/SM_ROOT')
  #sis.start()
  
  outcome = sm.execute()

  rospy.spin()

if __name__ == '__main__':
  main()