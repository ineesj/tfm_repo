#! /usr/bin/env python

# Behavior implemented: interactive_game
# Author: Lorenzo Sarti
# Description: interactive_game is a behavior designed for activating the state mahchine
#				on the robot involved in tha games which takes care of handling all the 
#				goals sent by the game core that runs on the mcentral machine. This state 
#				also provides navigation feedbacks via SAM slots to the game core which 
#				keeps track of the state of the game.			 

import roslib; roslib.load_manifest('monarch_behaviors')
import rospy
import smach
import smach_ros
import math

from smach_ros import simple_action_state

from move_base_msgs.msg import *
from std_msgs.msg import Int16, Header
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from monarch_behaviors.msg import InteractiveGameRobotAction

from functools import partial
from monarch_situational_awareness.srv import RemoveReader

from mosmach.monarch_state import MonarchState
from mosmach.change_conditions.sam_condition import SamCondition
from mosmach.actions.sam_writer_action import SamWriterAction
from mosmach.actions.sam_reader_action import SamReaderAction
from mosmach.actions.move_to_action import MoveToAction

from mosmach.util import pose2pose_stamped


# define state handleGoal
class handleGoal(MonarchState):
	""" This state registers a callback on the SAM slot in order to receive Goals. 
		If the goal is valid then it transition to the actionlib client that sends 
		the received goal to the navigation system.   """

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'], input_keys=['goal_in'], output_keys=['goal_out','action_result'])
		self.goalReceived = False
		self.goalStatus = 'not_valid'

		robot_id = rospy.get_namespace()
		robot_id = robot_id.strip('/')

		# checkGoal
		self.goalReader = SamReaderAction(self, '['+robot_id+'] flowfreeGoal','',self.receiveGoal_cb)
		self.add_action(self.goalReader)

		# moveRobot
		self.moveToAction = MoveToAction(self, data_cb=self.sendGoal_cb, is_dynamic=True)
		self.add_action(self.moveToAction)


	def receiveGoal_cb(self, data, userdata):
		# check if a goal is received and is valid
		if userdata.goal_in != data.target_pose:
			self.goalReceived = True
			userdata.goal_out = data.target_pose

		if self.goalReceived and self.isGoalValid(userdata.goal_in):
			self.goalReceived = False
			self.goalStatus = 'valid'
			return False
		
		self.goalReceived = False
		self.goalStatus = 'not_valid'

	def isGoalValid(self, goal):
		if goal.pose.position.x < -500 and \
			goal.pose.position.y < -500:
			return False 
		return True

	def sendGoal_cb(self, userdata):
		# if the received goal is valid, move robot
		while True:
			if self.goalStatus == 'not_valid':
				userdata.action_result = 'NoGoal'
				continue
			elif self.goalStatus == 'valid':
				break

		userdata.action_result = 'Succeeded'
		return userdata.goal_out


# define state watchState
class watchState(MonarchState):
	""" This state runs concurrently with the SM HANDLEGOAL and registers a callback
		on the SAM slot that receives the goal. This state provides navigation feedbacks
		to the game core running on mcentral"""

	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['preempted'], input_keys=['feedback_in','goal_in'], output_keys=['goal_out', 'feedback_out'])
		
		self.newGoalReceived = False
		self.goalActive = False

		robot_id = rospy.get_namespace()
		robot_id = robot_id.strip('/')

		self.goalReader = SamReaderAction(self, "["+robot_id+"] flowfreeGoal", '', self.receiveGoal_cb)
		self.feedbackWriter = SamWriterAction(self, "["+robot_id+"] flowfreeGoalFeedback", '', self.feedbackWriter_cb, is_dynamic = True)

	def receiveGoal_cb(self, data, extra):
		if extra.goal_in != data.target_pose:
			self.newGoalReceived = True
			extra.goal_out = data.target_pose

	def isGoalValid(self, goal):
		if goal.pose.position.x < -500 and \
			goal.pose.position.y < -500:
			return False 
		return True

	def feedbackWriter_cb(self, userdata):
		if self.newGoalReceived and self.isGoalValid(userdata.goal_in):
			self.newGoalReceived = False
			userdata.feedback_out = 'Active'

		elif self.newGoalReceived and not self.isGoalValid(userdata.goal_in):
			self.newGoalReceived = False
			userdata.feedback_out = 'NoGoal'

		return userdata.feedback_in

#####
def child_term_cb(outcome_map):

	if outcome_map['HANDLEGOAL']:
		return True

def main():
	rospy.init_node('behavior_game_robot')

	###################################################
	############## MAIN STATE MACHINE #################
	###################################################

	# Create the top level SMACH state machine
	sm_top = smach.StateMachine(outcomes=['preempted'])
	
	with sm_top:

		###################################################
		################ CONCURRENT STATE #################
		###################################################

		sm_con = smach.Concurrence(outcomes=['succeeded','preempted'],
								   default_outcome='preempted',
								   outcome_map={'preempted':{'HANDLEGOAL':'preempted'},
								   				'succeeded':{'HANDLEGOAL':'succeeded'}},
								   child_termination_cb = child_term_cb)

		sm_con.userdata.feedback_sm = 'NoGoal'
		sm_con.userdata.goal_sm = pose2pose_stamped(-1000, -1000, -1000)

		with sm_con:

			#sm_con.add('HANDLEGOAL', handleGoal(),transitions={'succeeded':'HANDLEGOAL'},remapping={'goal_in':'goal_sm','goal_out':'goal_sm','action_result':'feedback_sm'})
			sm_con.add('HANDLEGOAL', handleGoal(),remapping={'goal_in':'goal_sm','goal_out':'goal_sm','action_result':'feedback_sm'})
			sm_con.add('WATCHSTATE', watchState(),remapping={'feedback_in':'feedback_sm','goal_in':'goal_sm','feedback_out':'feedback_sm','goal_out':'goal_sm'})

		
		smach.StateMachine.add('GAME_INTERFACE', sm_con, transitions={'preempted':'preempted','succeeded':'GAME_INTERFACE'})


	action_name = rospy.get_name()  
	asw = smach_ros.ActionServerWrapper(action_name,
										InteractiveGameRobotAction,
										wrapped_container = sm_top,
										preempted_outcomes = ['preempted'])


	sis = smach_ros.IntrospectionServer('server_name', sm_top, '/SM_MINE')
	sis.start()
	asw.run_server()
	rospy.spin()


if __name__ == '__main__':
	main()
