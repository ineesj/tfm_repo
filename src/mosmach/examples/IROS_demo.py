#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.actions.runtime_action import RunTimeAction
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.change_conditions.get_cap_sensors_condition import GetCapSensorsCondition

from monarch_msgs.msg import RfidReading, BatteriesVoltage
from move_base_msgs.msg import MoveBaseGoal
from mosmach.util import pose2pose_stamped
from gesture_player_utils.msg import GestureStatus
from monarch_msgs.msg import KeyValuePairArray
from monarch_msgs.msg import KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa


import sys
import os.path
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))
from scout_msgs.msg import *
from scout_msgs.srv import *
from monarch_msgs.msg import SetStateAuxiliaryPowerBattery, SetStateElectronicPower, SetStateMotorsPower


######### Run CE's State Machine
class RunCEState1(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		greeting_name = 'mbot_warm_expression'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCEState2(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE2State")

		greeting_name = 'give_greetings_child'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)


######### Activate CA's State Machine
class ActivateCA15State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init ActivateCA15State")

		di = {"activated_cas":"ca15"}
		kvpa_msg = kvpa.from_dict(di)
		activateCAState = RunCaAction(self, kvpa_msg)
		self.add_action(activateCAState)


######### Patrolling State Machine
class MoveToWPState1(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP1State")

		navMoveToOneAction = MoveToAction(self,3.00,12.95,-2.64) #IST
		self.add_action(navMoveToOneAction)

		runCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runCeState)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

		getCapSensorsCondition = GetCapSensorsCondition(self, self.cap_callback)
		self.add_change_condition(getCapSensorsCondition, ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'


	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 6865:
				value = 'tag_detected'
				break
			else:
				rospy.sleep(0.1)
		return value

class MoveToWPState2(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted','touch_detected','tag_detected'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("Init MoveToWP2State")

		navMoveToTwoAction = MoveToAction(self,-2.65,10.65,-1.88) #IST
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

		getCapSensorsCondition = GetCapSensorsCondition(self, self.cap_callback)
		self.add_change_condition(getCapSensorsCondition, ['touch_detected'])

  	def cap_callback(self, data, userdata):
	    if data.head == True:
	    	data.head = False
	    	userdata.sensor_activated = 'head_touched'
	    	return 'touch_detected'
	    elif data.left_shoulder == True:
	    	data.left_shoulder = False
	    	userdata.sensor_activated = 'left_shoulder_touched'
	    	return 'touch_detected'
	    elif data.right_shoulder == True:
	    	data.right_shoulder = False
	    	userdata.sensor_activated = 'right_shoulder_touched'
	    	return 'touch_detected'
	    elif data.left_arm == True:
			data.left_arm = False
			userdata.sensor_activated = 'left_arm_touched'
			return 'touch_detected'
	    elif data.right_arm == True:
			data.right_arm = False
			userdata.sensor_activated = 'right_arm_touched'
			return 'touch_detected'

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 6865:
				value = 'tag_detected'
				break
			else:
				rospy.sleep(0.1)
		return value


######### Dock State Machine
class SendDockState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init DockState")

		navSendToDockAction = MoveToAction(self, -6.95,8.65,-1.13) #IST
		self.add_action(navSendToDockAction)

class FinishDock(smach.State):
  """StateDock moves the robot back for 5 seconds to secure it to the charger."""
  def __init__(self):
      smach.State.__init__(self, outcomes = ['completed'])

      self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

  def execute(self, userdata):
      self.teleop_srv(1, -0.2, 0)
      rospy.sleep(4)
      self.teleop_srv(0, 0, 0)

      return 'completed'


######### UnDock State Machine
class StateUndock(smach.State):
	"""StateUndock disconnects the robot from charger power and moves it forward."""
	def __init__(self):
		smach.State.__init__(self,outcomes = ['completed'])

		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
		self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_aux_batt2_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_electronics_power", SetStateElectronicPower, latch=True),
							rospy.Publisher("set_state_motors_power", SetStateMotorsPower, latch=True)]

	def disconnect_from_power(self):
		RELAY_DELAY = 0.25
		(aux1, aux2, elec, moto) = self.undock_pubs
		rospy.sleep(RELAY_DELAY)
		aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		elec.publish(SetStateElectronicPower(status=2))
		rospy.sleep(RELAY_DELAY)
		moto.publish(SetStateMotorsPower(status=2))
		rospy.sleep(RELAY_DELAY)

	def execute(self, userdata):
		self.disconnect_from_power()
		# args: (cmd, vel, tout), where cmd=1=front/back, vel=-0.2 m/s, tout=0=none
		self.teleop_srv(1, 0.2, 0)
		rospy.sleep(4.0)
		self.teleop_srv(0, 0, 0)

		return 'completed'


######### Check batteries voltage State Machine
class BatteriesVoltageState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted','aborted'])
		rospy.loginfo("Init BatteriesVoltageState")
		topicName = 'batteries_voltage'
		conditionOutcomes = ['succeeded']
		topicCondition = TopicCondition(self, topicName, BatteriesVoltage, self.batteriesCondition)
		self.add_change_condition(topicCondition, conditionOutcomes)

	def batteriesCondition(self, data, userdata):
		while True:
			if data.electronics <= 12.0 or data.motors <= 12.0:
				value = 'preempted'
				break
			else:
				rospy.sleep(0.1)
		return value


######### Restart State
class RestartState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init RestartState")

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		value = 'tag_detected'
		return value	


######### Get Capacitive Sensors Interaction
class TouchInteractionState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
		rospy.loginfo("TouchInteractionState")

		runCeState = RunCeAction(self, self.greeting_name_callback, is_dynamic = True)
		self.add_action(runCeState)

		runTimeAction = RunTimeAction(self, 4)
		self.add_action(runTimeAction)

	def greeting_name_callback(self, userdata):
		if userdata.sensor_activated == 'head_touched':
			userdata.sensor_activated = ''
			return 'mbot_warm_expression'
		elif userdata.sensor_activated == 'left_arm_touched':
			userdata.sensor_activated = ''
			return 'give_greetings_child'
		elif userdata.sensor_activated == 'right_arm_touched':
			userdata.sensor_activated = ''
			return 'mbot_follow_robot'
		elif userdata.sensor_activated == 'left_shoulder_touched':
			userdata.sensor_activated = ''
			return 'mbot_behavior_succeeded'
		elif userdata.sensor_activated == 'right_shoulder_touched':
			userdata.sensor_activated = ''
			return 'mbot_behavior_succeeded'


# Main function
def main():
	rospy.init_node("IROS_demo")
	number_of_waypoints = 2
	next_ca = 'CA1'
	
	# State Machine UnDock
	sm_undock = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_undock:
		sm_undock.add('UNDOCK',StateUndock(),transitions = {'completed':'succeeded'})


	# State Machine Patrolling   
	sm_patrolling = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	sm_patrolling.userdata.sm_sensor_activated = 0

	with sm_patrolling:
		for i in range(1,number_of_waypoints+1):
			if i >= number_of_waypoints:
				next_ca = 'CA1'

			else:
				next_ca = 'CA%s'%(i+1)

			sm_patrolling.add('CA%s'%i, ActivateCA15State(), transitions = {'succeeded':'WAYPOINT%s'%i})
			sm_patrolling.add('WAYPOINT%s'%i, globals()['MoveToWPState%s'%i](), transitions = {'succeeded':'CE%s'%i,'tag_detected':'succeeded','touch_detected':'TOUCH_INTERACT%s'%i}, remapping={'sensor_activated':'sm_sensor_activated'})
			sm_patrolling.add('CE%s'%i, globals()['RunCEState%s'%i](), transitions = {'succeeded':next_ca})
			sm_patrolling.add('TOUCH_INTERACT%s'%i, TouchInteractionState(), transitions={'succeeded':'WAYPOINT%s'%i}, remapping={'sensor_activated':'sm_sensor_activated'})


	# State Machine Dock
	sm_dock = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_dock:
		sm_dock.add('DOCK',SendDockState(),transitions = {'succeeded':'FINISH'})
		sm_dock.add('FINISH',FinishDock(),transitions = {'completed':'succeeded'})


	# State Machine check batteries
	sm_batteries = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_batteries:
		sm_batteries.add('BATTERIES',BatteriesVoltageState())


	# Concurrence State Machine: Patrolling + Batteries status
	def termination_cm_cb(outcome_map):
		if outcome_map['BATTERIES_SM'] == 'suceeded':
			return True
		if outcome_map['BATTERIES_SM'] == 'preempted':
			return True
		if outcome_map['PATROLLING_SM'] == 'preempted':
			return True
		if outcome_map['PATROLLING_SM'] == 'succeeded':
			return True
		if outcome_map['PATROLLING_SM'] == 'aborted':
			return True
		else:
			return False

	def outcome_cm_cb(outcome_map):
		if outcome_map['BATTERIES_SM'] == 'succeeded':
			return 'preempted'
		if outcome_map['BATTERIES_SM'] == 'preempted':
			return 'preempted'
		if outcome_map['PATROLLING_SM'] == 'preempted':
			return 'preempted'
		if outcome_map['PATROLLING_SM'] == 'succeeded':
			return 'succeeded'
		if outcome_map['PATROLLING_SM'] == 'aborted':
			return 'aborted'
		else:
			return 'succeeded'


	cm = smach.Concurrence(outcomes = ['succeeded','preempted','aborted'],
						   default_outcome = 'succeeded',
						   child_termination_cb = termination_cm_cb,
						   outcome_cb = outcome_cm_cb)

	with cm:
		cm.add('PATROLLING_SM',sm_patrolling)
		cm.add('BATTERIES_SM',sm_batteries)

	# Main State Machine
	sm_msm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])

	with sm_msm:
		sm_msm.add('FIRST_DOCK',sm_dock,transitions={'succeeded':'UNDOCK_SM'})
		sm_msm.add('UNDOCK_SM',sm_undock,transitions={'succeeded':'SIMPLEPATROLLING'})
		sm_msm.add('SIMPLEPATROLLING',cm,transitions={'preempted':'DOCK_SM'})
		sm_msm.add('DOCK_SM',sm_dock,transitions={'succeeded':'RESTART'})
		sm_msm.add('RESTART', RestartState(), transitions = {'tag_detected':'UNDOCK_SM'})


	sis = smach_ros.IntrospectionServer('IROS_demo', sm_msm, '/SM_ROOT')
	sis.start()
	
	outcome = sm_msm.execute()

	rospy.spin()

if __name__ == '__main__':
	main()
