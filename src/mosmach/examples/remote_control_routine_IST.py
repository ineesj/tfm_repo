#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.runtime_action import RunTimeAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading, BatteriesVoltage
from move_base_msgs.msg import MoveBaseGoal
from mosmach.util import pose2pose_stamped
from gesture_player_utils.msg import GestureStatus

import sys
import os.path
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))
from scout_msgs.msg import *
from scout_msgs.srv import *
from monarch_msgs.msg import SetStateAuxiliaryPowerBattery, SetStateElectronicPower, SetStateMotorsPower


RUNTIME = 1800 # runtime in seconds

######### Run CE's State Machine
class RunCE1State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE1State")

		greeting_name = 'mbot_warm_expression'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCE2State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunCE2State")

		greeting_name = 'give_greetings_child'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)


######### Activate CA's State Machine
class ActivateCA15State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init ActivateCA15State")

		activateCAState = RunCaAction(self, 'ca15')
		self.add_action(activateCAState)


######### Patrolling State Machine
class MoveToWP1State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected','preempted'])
		rospy.loginfo("Init MoveToWP1State")

		navMoveToOneAction = MoveToAction(self,2.45,12.30,-2.70) #IST
		#navMoveToOneAction = MoveToAction(self,-92.65,-83.90,-0.04)
		self.add_action(navMoveToOneAction)

		runCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runCeState)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP2State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected','preempted'])
		rospy.loginfo("Init MoveToWP2State")

		navMoveToTwoAction = MoveToAction(self,-3.20, 9.5, 0.48) #IST
		#navMoveToTwoAction = MoveToAction(self,-90.55,-83.50,-2.94)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value


######### RunTime State Machine
class RunTimeState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init RunTimeState")

		timeCondition = RunTimeAction(self, RUNTIME)
		self.add_action(timeCondition)


######### Dock State Machine
class SendDockState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init DockState")

		navSendToDockAction = MoveToAction(self, -6.99,8.58,-1.03) #IST
		#navSendToDockAction = MoveToAction(self, -92.00,-83.95,-0.08)
		self.add_action(navSendToDockAction)

class FinishDock(smach.State):
  """StateDock moves the robot back for 5 seconds to secure it to the charger."""
  def __init__(self):
      smach.State.__init__(self, outcomes = ['completed'])

      self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

  def execute(self, userdata):
      # args: (cmd, vel, tout), where cmd=1=front/back, vel=-0.2 m/s, tout=0=none
      self.teleop_srv(1, -0.2, 0)
      rospy.sleep(5)
      self.teleop_srv(0, 0, 0)

      return 'completed'


######### UnDock State Machine
class StateUndock(smach.State):
	"""StateUndock disconnects the robot from charger power and moves it forward."""
	def __init__(self):
		smach.State.__init__(self,outcomes = ['completed'])

		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
		self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_aux_batt2_power", SetStateAuxiliaryPowerBattery, latch=True),
							rospy.Publisher("set_state_electronics_power", SetStateElectronicPower, latch=True),
							rospy.Publisher("set_state_motors_power", SetStateMotorsPower, latch=True)]

	def disconnect_from_power(self):
		RELAY_DELAY = 0.25
		(aux1, aux2, elec, moto) = self.undock_pubs
		rospy.sleep(RELAY_DELAY)
		aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
		rospy.sleep(RELAY_DELAY)
		elec.publish(SetStateElectronicPower(status=2))
		rospy.sleep(RELAY_DELAY)
		moto.publish(SetStateMotorsPower(status=2))
		rospy.sleep(RELAY_DELAY)

	def execute(self, userdata):
		self.disconnect_from_power()
		# args: (cmd, vel, tout), where cmd=1=front/back, vel=-0.2 m/s, tout=0=none
		self.teleop_srv(1, 0.2, 0)
		rospy.sleep(2.0)
		self.teleop_srv(0, 0, 0)

		return 'completed'


######### Check batteries voltage State Machine
class BatteriesVoltageState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init BatteriesVoltageState")
		topicName = 'batteries_voltage'
		conditionOutcomes = ['succeeded']
		topicCondition = TopicCondition(self, topicName, BatteriesVoltage, self.batteriesCondition)
		self.add_change_condition(topicCondition, conditionOutcomes)

	def batteriesCondition(self, data, userdata):

		while True:
			if data.electronics <= 12.0 or data.motors <= 12.0:
				value = 'succeeded'
				break

			else:
				rospy.sleep(0.1)
		return value


# Main function
def main():
	rospy.init_node("patrolling_and_dock")

	# State Machine UnDock
	sm_undock = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_undock:
		sm_undock.add('UNDOCK',StateUndock(),transitions = {'completed':'succeeded'})


	# State Machine Patrolling   
	sm_patrolling = smach.StateMachine(outcomes = ['preempted','aborted'])
	with sm_patrolling:
		sm_patrolling.add('CA1', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT1'})
		sm_patrolling.add('WAYPOINT1', MoveToWP1State(), transitions = {'succeeded':'CE1','tag_detected':'preempted'})
		sm_patrolling.add('CE1', RunCE1State(), transitions = {'succeeded':'CA2'})

		sm_patrolling.add('CA2', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT2'})
		sm_patrolling.add('WAYPOINT2', MoveToWP2State(), transitions = {'succeeded':'CE2','tag_detected':'preempted'})
		sm_patrolling.add('CE2', RunCE2State(), transitions = {'succeeded':'CA1'})


	# State Machine RunTime
	sm_runtime = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_runtime:
		sm_runtime.add('RUNTIME',RunTimeState(),transitions={'succeeded':'succeeded'})


	# State Machine Dock
	sm_dock = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_dock:
		sm_dock.add('DOCK',SendDockState(),transitions = {'succeeded':'FINISH'})
		sm_dock.add('FINISH',FinishDock(),transitions = {'completed':'succeeded'})


	# State Machine check batteries
	sm_batteries = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	with sm_batteries:
		sm_batteries.add('DOCK',BatteriesVoltageState())


	# Concurrence State Machine: Patrolling + Get RFID	
	def termination_cm_cb(outcome_map):
		if outcome_map['RUNTIME_SM'] == 'succeeded':
			return True
		if outcome_map['BATTERIES_SM'] == 'succeeded':
			return True
		if outcome_map['PATROLLING_SM'] == 'preempted':
			return True
		if outcome_map['PATROLLING_SM'] == 'aborted':
			return True
		else:
			return False

	def outcome_cm_cb(outcome_map):
		if outcome_map['RUNTIME_SM'] == 'succeeded':
			return 'succeeded'
		if outcome_map['BATTERIES_SM'] == 'succeeded':
			return 'succeeded'
		if outcome_map['PATROLLING_SM'] == 'preempted':
			return 'preempted'
		if outcome_map['PATROLLING_SM'] == 'aborted':
			return 'aborted'
		else:
			return 'succeeded'


	cm = smach.Concurrence(outcomes = ['succeeded','preempted','aborted'],
						   default_outcome = 'succeeded',
						   child_termination_cb = termination_cm_cb,
						   outcome_cb = outcome_cm_cb)

	with cm:
		cm.add('PATROLLING_SM',sm_patrolling)
		cm.add('RUNTIME_SM',sm_runtime)
		cm.add('BATTERIES_SM',sm_batteries)

	# Main State Machine
	sm_msm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])

	with sm_msm:
		sm_msm.add("UNDOCK_SM",sm_undock,transitions={'succeeded':'SIMPLEPATROLLING'})
		sm_msm.add("SIMPLEPATROLLING",cm,transitions={'succeeded':'DOCK_SM'})
		sm_msm.add("DOCK_SM",sm_dock,transitions={'succeeded':'succeeded'})


	#sis = smach_ros.IntrospectionServer('patrolling_and_dock', sm_msm, '/SM_ROOT')
	#sis.start()
	
	outcome = sm_msm.execute()

	rospy.spin()

if __name__ == '__main__':
	main()
