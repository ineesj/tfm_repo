#!/usr/bin/env python

# SYS
import sys
import os.path

# ROS
import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))

#etc etc
from random import randint
import time,subprocess
#bla bla bla
from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.get_cap_sensors_action import GetCapSensorsAction
from mosmach.actions.move_head_action import MoveHeadAction
from monarch_msgs.msg import RfidReading
from geometry_msgs.msg import PoseStamped,Pose
from scout_msgs.srv import TeleOpSrv


class GetRfid(MonarchState):
    def __init__(self):
        MonarchState.__init__(self,state_outcomes=['rfid_detected','rfid_not_detected'])
        rospy.loginfo("Init Rfid Detection")
        condition_outcomes = ['rfid_detected','rfid_not_detected']
        
        tc = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition_cb)
        self.add_change_condition(tc, condition_outcomes)

    def rfidCondition_cb(self, data,userdata):
        if data.tag_id == 28223: #Test tag 2
            value = 'rfid_detected'
            rospy.loginfo("Let's play Catch and Touch!")
        else:
            value = 'rfid_not_detected'
        return value


#### Play Voice "shall we play catch-n-tounch? Touch on my arm to start!" ####
class PlayVoice1(MonarchState):
    def __init__(self):
        MonarchState.__init__(self,state_outcomes=['succeeded'])
        pass
    def execute(self,userdata):
        subprocess.call("mpg321 mp3/catch1.mp3",shell=True)
        return 'succeeded'


class GetCapSensorsToStart(MonarchState):
  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("GetCapSensorsToStart")
    
    getCapSensorsAction = GetCapSensorsAction(self, self.cap_callback)
    self.add_action(getCapSensorsAction)

  def cap_callback(self, data, userdata):
    if data.head == True:
      userdata.sensor_activated = 'head_touched'
    elif data.left_shoulder == True:
      userdata.sensor_activated = 'left_shoulder_touched'
    elif data.right_shoulder == True:
      userdata.sensor_activated = 'right_shoulder_touched'
    elif data.left_arm == True:
      userdata.sensor_activated = 'left_arm_touched'
    elif data.right_arm == True:
      userdata.sensor_activated = 'right_arm_touched'


#### Play Voice "ahah you won't get me!" (and move head toward touched arm) ###
class PlayVoice2(MonarchState):
    def __init__(self):
        MonarchState.__init__(self,state_outcomes=['succeeded'],input_keys=['sm_sensor_activated'])
        pass
    def execute(self,userdata):
        if userdata.sm_sensor_activated == 'left_arm_touched':
            ang = 0.75
        elif userdata.sm_sensor_activated == 'right_arm_touched':
            ang = -0.75
        self.add_action(MoveHeadAction(self, ang, 50))
        rospy.sleep(0.5)
        subprocess.call("mpg321 mp3/catch2.mp3",shell=True)
        return 'succeeded'


######### Random Goal State Machine #############
class MoveToGoal(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['pose_data'], output_keys=['pose_data'])
        rospy.loginfo("Init MoveToWP1State")

        navMoveToOneAction = MoveToAction(self, data_cb=self.move_to_cb, is_dynamic=True)
        self.add_action(navMoveToOneAction)

        self.ind = 0 #Index to make sure there is not consecutive goal point which are the same. Robot may block if so.

    #ISR#
    def move_to_cb(self,userdata):
        while True: 
            rand = randint(1,2)
            if rand != self.ind:
                self.ind = rand
                break
        #print rand
        #-7.15,7.60,-1.72
        #-1.80,10.75,-2.54
        if rand == 1:
            userdata.pose_data.pose.position.x = 1.90 #down in IPOL
            userdata.pose_data.pose.position.y = 0.05
            userdata.pose_data.pose.orientation.z = 1.57
        elif rand == 2:
            userdata.pose_data.pose.position.x = 0.90 #middle corridor
            userdata.pose_data.pose.position.y = 10.15
            userdata.pose_data.pose.orientation.z = 1.65
        return userdata.pose_data


class GetCapSensorsToFinish(MonarchState):
  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("GetCapSensorsToFinish")
    
    getCapSensorsAction = GetCapSensorsAction(self, self.cap_callback)
    self.add_action(getCapSensorsAction)

  def cap_callback(self, data, userdata):
    if data.head == True:
      userdata.sensor_activated = 'head_touched'
    elif data.left_shoulder == True:
      userdata.sensor_activated = 'left_shoulder_touched'
    elif data.right_shoulder == True:
      userdata.sensor_activated = 'right_shoulder_touched'
    elif data.left_arm == True:
      userdata.sensor_activated = 'left_arm_touched'
    elif data.right_arm == True:
      userdata.sensor_activated = 'right_arm_touched'


#### Play Voice when robot arrive at goal without being caught (and Stops the robot with teleop) ####
class PlayVoice3(MonarchState):
    def __init__(self):
        MonarchState.__init__(self,state_outcomes=['succeeded'])
        self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
    def execute(self,userdata):
        self.teleop_srv(0, 0, 0)
        subprocess.call("mpg321 mp3/catch3.mp3",shell=True)
        return 'succeeded'


#### Play Voice when kid get the robot ####
class PlayVoice4(MonarchState):
    def __init__(self):
        MonarchState.__init__(self,state_outcomes=['succeeded'])
        pass
    def execute(self,userdata):
        subprocess.call("mpg321 mp3/catch4.mp3",shell=True)
        return 'succeeded'



### Callback for Catch-n-touch concurrence states ###
def catch_touch_cb(outcome_map):
  # terminate all running states if MoveToGoal finished with outcome 'succeeded'
  if outcome_map['MoveToGoal'] == 'succeeded':
    return True
  # terminate all running states if GetCapSensorsToFinish finished with outcome 'succeeded'
  if outcome_map['GetCapSensorsToFinish'] == 'succeeded':
    return True
  # in all other case, just keep running, don't terminate anything
  return False


######### Main function ##########
def main():

    rospy.init_node("joyful_warden_simple")
    rospy.loginfo('Starting up Joyful Warden Simple')

    sm = smach.StateMachine(outcomes=['done'])
    sm.userdata.sm_sensor_activated = 0
    sm.userdata.sm_data = PoseStamped()

    with sm:
        sm.add('GetRFID',GetRfid(),transitions={'rfid_detected':'PlayVoice1','rfid_not_detected':'GetRFID'})
        sm.add('PlayVoice1',PlayVoice1(),transitions={'succeeded':'GetCapSensors'})
        sm.add('GetCapSensors', GetCapSensorsToStart(), transitions={'succeeded':'PlayVoice2'}, remapping={'sensor_activated':'sm_sensor_activated'})
        sm.add('PlayVoice2',PlayVoice2(),transitions={'succeeded':'CON'})

        sm_con = smach.Concurrence(outcomes=['not_caught','caught','default'],
                                   default_outcome='default',
                                   outcome_map={'not_caught':{'MoveToGoal':'succeeded'},
                                                'caught':{'GetCapSensorsToFinish':'succeeded'}},
                                   input_keys=['sm_data'],
                                   child_termination_cb = catch_touch_cb)
        with sm_con:
            smach.Concurrence.add('MoveToGoal', MoveToGoal(),remapping={'pose_data':'sm_data'})
            smach.Concurrence.add('GetCapSensorsToFinish', GetCapSensorsToFinish(),remapping={'sensor_activated':'sm_sensor_activated'})

        sm.add('CON', sm_con,transitions={'default':'CON','not_caught':'PlayVoice3','caught':'PlayVoice4'})
        sm.add('PlayVoice3',PlayVoice3(),transitions={'succeeded':'GetRFID'})
        sm.add('PlayVoice4',PlayVoice4(),transitions={'succeeded':'GetRFID'})

    outcome = sm.execute()

def jw_sm():

    sm = smach.StateMachine(outcomes = ['succeeded'])
    sm.userdata.sm_sensor_activated = 0
    sm.userdata.sm_data = PoseStamped()

    with sm:
        sm.add('GetRFID',GetRfid(),transitions={'rfid_detected':'PlayVoice1','rfid_not_detected':'GetRFID'})
        sm.add('PlayVoice1',PlayVoice1(),transitions={'succeeded':'GetCapSensors'})
        sm.add('GetCapSensors', GetCapSensorsToStart(), transitions={'succeeded':'PlayVoice2'}, remapping={'sensor_activated':'sm_sensor_activated'})
        sm.add('PlayVoice2',PlayVoice2(),transitions={'succeeded':'CON'})

        sm_con = smach.Concurrence(outcomes=['not_caught','caught','default'],
                                   default_outcome='default',
                                   outcome_map={'not_caught':{'MoveToGoal':'succeeded'},
                                                'caught':{'GetCapSensorsToFinish':'succeeded'}},
                                   input_keys=['sm_data'],
                                   child_termination_cb = catch_touch_cb)
        with sm_con:
            smach.Concurrence.add('MoveToGoal', MoveToGoal(),remapping={'pose_data':'sm_data'})
            smach.Concurrence.add('GetCapSensorsToFinish', GetCapSensorsToFinish(),remapping={'sensor_activated':'sm_sensor_activated'})

        sm.add('CON', sm_con,transitions={'default':'CON','not_caught':'PlayVoice3','caught':'PlayVoice4'})
        sm.add('PlayVoice3',PlayVoice3(),transitions={'succeeded':'succeeded'})
        sm.add('PlayVoice4',PlayVoice4(),transitions={'succeeded':'succeeded'})

    return sm


if __name__ == '__main__':
    main()