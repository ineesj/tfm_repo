#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from monarch_msgs.msg import RfidReading
from mosmach.monarch_state import MonarchState
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.change_condition import ChangeCondition
from mosmach.change_conditions.topic_condition import TopicCondition


class FindRfidTag(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes = ['greetingChild','greetingStaff','askForRoomNumber','behaviorSucceeded','rejectCommand','startWalking','followRobot','tellLowerYourVoice','giveThanks','warmExpression','rfid_tag_not_found','shutdown'])
		rospy.loginfo("FindRfidTag")
		condition_outcomes = ['greetingChild','greetingStaff','askForRoomNumber','behaviorSucceeded','rejectCommand','startWalking','followRobot','tellLowerYourVoice','giveThanks','warmExpression','rfid_tag_not_found','shutdown']
		
		tc = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition_cb)
		self.add_change_condition(tc, condition_outcomes)

	def rfidCondition_cb(self, data, userdata):
		if data.tag_id == 5267:
			value = 'greetingChild'
		elif data.tag_id == 5291:
			value = 'greetingStaff'	
		elif data.tag_id == 5337:
			value = 'askForRoomNumber'
		elif data.tag_id == 5275:
			value = 'behaviorSucceeded'
		elif data.tag_id == 6892:
			value = 'rejectCommand'
		elif data.tag_id == 6865:
			value = 'startWalking'
		elif data.tag_id == 5284:
			value = 'followRobot'
		elif data.tag_id == 5305:
			value = 'tellLowerYourVoice'
		elif data.tag_id == 5329:
			value = 'giveThanks'
		elif data.tag_id == 5308:
			value = 'warmExpression'
		elif data.tag_id == 5922:
			value = 'shutdown'
		else:
			value = 'rfid_tag_not_found'

		return value


class GiveGreetingChildState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("GiveGreetingChildState")
		runGreetingCeState = RunCeAction(self, 'give_greetings_child')
		self.add_action(runGreetingCeState)

class GiveGreetingStaffState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("GiveGreetingStaffState")
		runGreetingCeState = RunCeAction(self, 'give_greetings_staff')
		self.add_action(runGreetingCeState)
		
class AskForRoomNumberState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("AskForRoomNumberState")
		runGreetingCeState = RunCeAction(self, 'mbot_ask_for_room_number')
		self.add_action(runGreetingCeState)

class BehaviorSucceededState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("BehaviorSucceededState")
		runGreetingCeState = RunCeAction(self, 'mbot_behavior_succeeded')
		self.add_action(runGreetingCeState)

class RejectCommandState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("RejectCommandState")
		runGreetingCeState = RunCeAction(self, 'mbot_reject_command')
		self.add_action(runGreetingCeState)
		
class StartWalkingState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("StartWalkingState")
		runGreetingCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runGreetingCeState)

class FollowRobotState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("FollowRobotState")
		runGreetingCeState = RunCeAction(self, 'mbot_follow_robot')
		self.add_action(runGreetingCeState)
		
class GiveThanksState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("GiveThanksState")
		runGreetingCeState = RunCeAction(self, 'mbot_give_thanks')
		self.add_action(runGreetingCeState)
		
class WarmExpressionState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("WarmExpressionState")
		runGreetingCeState = RunCeAction(self, 'mbot_warm_expression')
		self.add_action(runGreetingCeState)

class TellLowerYourVoiceState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("TellLowerYourVoiceState")
		runGreetingCeState = RunCeAction(self, 'mbot_tell_lower_your_voice')
		self.add_action(runGreetingCeState)


######### Main function
def main():
	rospy.init_node("cyclic_detect_RFID_tag")

	# State Machine     
	
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	
	with sm:
		sm.add('GetRfidTag',FindRfidTag(),transitions = {'greetingChild':'GreetingChild','greetingStaff':'GreetingStaff','askForRoomNumber':'AskForRoomNumber','behaviorSucceeded':'BehaviorSucceeded','rejectCommand':'RejectCommand','startWalking':'StartWalking','followRobot':'FollowRobot','tellLowerYourVoice':'TellLowerYourVoice','giveThanks':'GiveThanks','warmExpression':'WarmExpression','rfid_tag_not_found':'GetRfidTag','shutdown':'succeeded'})
		sm.add('GreetingChild',GiveGreetingChildState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('GreetingStaff',GiveGreetingStaffState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('AskForRoomNumber',AskForRoomNumberState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('BehaviorSucceeded',BehaviorSucceededState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('RejectCommand',RejectCommandState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('StartWalking',StartWalkingState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('FollowRobot',FollowRobotState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('TellLowerYourVoice',TellLowerYourVoiceState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('GiveThanks',GiveThanksState(),transitions = {'succeeded':'GetRfidTag'})
		sm.add('WarmExpression',WarmExpressionState(),transitions = {'succeeded':'GetRfidTag'})

	
	outcome = sm.execute()


if __name__ == '__main__':	
	main()
