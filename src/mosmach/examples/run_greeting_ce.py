#!/usr/bin/env python

import rospy

from mosmach.monarch_state import MonarchState
from mosmach.actions.run_ce_action import RunCeAction

class RunGreetingCeState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'])
    
    greeting_name = 'give_greetings_child'
    runGreetingCeState = RunCeAction(self, greeting_name)
    self.add_action(runGreetingCeState)
    

######### Main function
def main():
  rospy.init_node("run_greeting_ce")
  s1 = RunGreetingCeState()
  s1.execute('')

if __name__ == '__main__':
	main()
