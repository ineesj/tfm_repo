#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.set_mouth_action import SetMouthAction
from monarch_msgs.msg import MouthLedControl

import numpy
import random

class SetSmileMouthState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['mouthled_data'], output_keys=['mouthled_data'])
    rospy.loginfo("SetMouthAction")
    
    #mouthData = [0]*32*8
    #setMouthAction = SetMouthAction(self, mouthData)
    #self.add_action(setMouthAction)

    setMouthAction = SetMouthAction(self, self.mouthled_data_cb, is_dynamic=True)
    self.add_action(setMouthAction)

  def mouthled_data_cb(self, userdata):
    userdata.mouthled_data.data = numpy.random.randint(2,size=32*8)
    return userdata.mouthled_data

class ResetSmileMouthState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['mouthled_data'])
    rospy.loginfo("ResetMouthAction")

    resetEyeLedState = SetMouthAction(self, self.mouthled_data_cb, is_dynamic=True)
    self.add_action(resetEyeLedState)

  def mouthled_data_cb(self, userdata):
    userdata.mouthled_data.data = [0]*32*8
    return userdata.mouthled_data


######### Main function
def main():
  rospy.init_node("change_mouth_led")

  # State Machine Patrolling
  sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
  sm.userdata.sm_data = MouthLedControl()

  with sm:
    sm.add('SetMouthLED',SetSmileMouthState(),transitions = {'succeeded':'ResetMouthLED'}, remapping={'mouthled_data':'sm_data'})
    sm.add('ResetMouthLED',ResetSmileMouthState(),transitions = {'succeeded':'SetMouthLED'}, remapping={'mouthled_data':'sm_data'})

  #sis = smach_ros.IntrospectionServer('change_mouth_led', sm, '/SM_ROOT')
  #sis.start()
  
  outcome = sm.execute()


if __name__ == '__main__':
  main()
