#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.topic_writer_action import TopicWriterAction
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.control_projector_action import ControlProjectorAction
from mosmach.actions.move_head_action import MoveHeadAction
from monarch_msgs.msg import KeyValuePairArray
from monarch_msgs.msg import KeyValuePair
from monarch_msgs_utils import key_value_pairs as kvpa
from std_msgs.msg import String
from monarch_behaviors.msg import TeachingAssistanceAction, TeachingAssistanceResult


### Navigation states ###
class MoveToProjectionAreaState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Move to Projection Area State")

        # IPOL: classroom
        self.add_action(MoveToAction(self,-0.4,-12.12,-1.53))

        # Publish the topic that allows interfaces to be used
        self.add_action(TopicWriterAction(self, 'allowed_hri_interfaces', String, 'arms|audio|cheeks|eyes|image|leds_base|mouth|projector|voice'))


### Head Movement ###
class MoveHeadRightState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("MoveHeadRightState")
        self.add_action(MoveHeadAction(self, 1.5, 50))


class MoveHeadCentralState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("MoveHeadCentralState")
        self.add_action(MoveHeadAction(self, 0, 50))

        # Allow the usage of the Head again
        self.add_action(TopicWriterAction(self, 'allowed_hri_interfaces', String, 'arms|audio|cheeks|eyes|head|image|leds_base|mouth|projector|voice'))


### Projector Control ###
class TurnOnTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn On the Projector State")
        self.add_action(ControlProjectorAction(self, True))


class TurnOffTheProjectorState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Turn Off the Projector State")
        self.add_action(ControlProjectorAction(self, False))

        
### Teaching Assistance ###
class TeachingAssistanceCaActivationState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Teaching Assistance State")

        ##1 Activate state to show the menu
        di = {"activated_cas":"ca14.4"}
        kvpa_msg = kvpa.from_dict(di)
        self.add_action(RunCaAction(self, kvpa_msg))


class TeachingAssistanceState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['finished', 'reactivate'])
        rospy.loginfo("Teaching Assistance State")

        ##2 Is TA finished?
        self.add_change_condition(TopicCondition(self, 'screen_touched', KeyValuePairArray, self.finishCondition), ['finished', 'reactivate'])

    def finishCondition(self, data, userdata):
        keyValEl0 = data.array[0]

        if keyValEl0.key == 'command':
            if keyValEl0.value == 'stop_projecting':
                value = 'finished'

            elif keyValEl0.value == 'select_media_content':
                value = 'reactivate'

            else:
                value = 'reactivate'

        else:
            value = 'reactivate'

        return value


### MAIN ###
def ta_sm():
    # State TA
    sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
    with sm:
        sm.add('MoveToProjectionArea', MoveToProjectionAreaState(), transitions={'succeeded':'MoveHeadRight'})
        sm.add('MoveHeadRight', MoveHeadRightState(), transitions={'succeeded':'TurnOnTheProjector'})
        sm.add('TurnOnTheProjector', TurnOnTheProjectorState(), transitions={'succeeded':'TeachingAssistanceCaActivation'})
        sm.add('TeachingAssistanceCaActivation', TeachingAssistanceCaActivationState(), transitions={'succeeded':'TeachingAssistance'})
        sm.add('TeachingAssistance', TeachingAssistanceState(), transitions={'finished':'TurnOffTheProjector', 'reactivate':'TeachingAssistanceCaActivation'})
        sm.add('TurnOffTheProjector', TurnOffTheProjectorState(), transitions={'succeeded':'MoveHeadCentral'})
        sm.add('MoveHeadCentral', MoveHeadCentralState(), transitions={'succeeded':'succeeded'})

    return sm

if __name__ == '__main__':
    ta_sm()