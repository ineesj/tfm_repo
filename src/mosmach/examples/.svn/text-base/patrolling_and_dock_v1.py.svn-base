#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading
from move_base_msgs.msg import MoveBaseGoal
from mosmach.util import pose2pose_stamped

# Patrolling State Machine
class MoveToOneState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'],input_keys=['user_goal'], output_keys=['user_goal'])
		rospy.loginfo("Init MoveTo1State")

		navMoveToOneAction = MoveToAction(self,-92.35, -83.90, -0.11)
		self.add_action(navMoveToOneAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		userdata.user_goal = pose2pose_stamped(-89.50,-83.75,-2.21)
		#rospy.sleep(0.5)
		value = 'tag_detected'
		return value

class MoveToTwoState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'], input_keys=['user_goal'], output_keys=['user_goal'])
		rospy.loginfo("Init MoveTo2State")

		navMoveToTwoAction = MoveToAction(self, -88.95, -84.80, 3.03)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		userdata.user_goal = pose2pose_stamped(-90.10, -85.30, 1.45)
		#rospy.sleep(0.5)
		value = 'tag_detected'
		return value


# Run Dock State Machine
class SendDockState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'], input_keys=['user_goal'], output_keys=['user_goal'])
		rospy.loginfo("Init SendDockState")

		navSendToDockAction = MoveToAction(self, data_cb=self.goal_cb, is_dynamic=True)
		self.add_action(navSendToDockAction)

	def goal_cb(self,userdata):
		rospy.loginfo('>>> GOT DATA! user_goal = '+str(userdata.user_goal))
		return userdata.user_goal


# Main function
def main():
	rospy.init_node("patrolling_and_dock")

	# State Machine Patrolling
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	#sm.userdata.sm_data = MoveBaseGoal()
	sm.userdata.sm_data = pose2pose_stamped(0, 0, 0)

	with sm:
		sm.add('WAYPOINT1',MoveToOneState(),transitions = {'succeeded':'WAYPOINT2','tag_detected':'DOCK'}, remapping={'user_goal':'sm_data'})
		sm.add('WAYPOINT2',MoveToTwoState(),transitions = {'succeeded':'WAYPOINT1','tag_detected':'DOCK'}, remapping={'user_goal':'sm_data'})
		sm.add('DOCK',SendDockState(), transitions = {'succeeded':'WAYPOINT2'}, remapping={'user_goal':'sm_data'})

	#sis = smach_ros.IntrospectionServer('patrolling_and_dock', sm, '/SM_ROOT')
	#sis.start()
	
	outcome = sm.execute()


if __name__ == '__main__':
	main()
