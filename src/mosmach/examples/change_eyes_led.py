#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.set_led_action import SetLedAction
from monarch_msgs.msg import LedControl

import random

class SetEyesLedState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['led_data'], output_keys=['led_data'])
    rospy.loginfo("SetEyesLedState")
    
    #setLeftEyeLedState = SetLedAction(self, 'leftEye', 100, 0, 0, 0)
    #self.add_action(setLeftEyeLedState)
    setEyeLedState = SetLedAction(self, data_cb=self.led_data_cb, is_dynamic=True)
    self.add_action(setEyeLedState)

  def led_data_cb(self, userdata):
    userdata.led_data.device = random.randrange(0,2)
    userdata.led_data.r = random.randrange(0,101)
    userdata.led_data.g = random.randrange(0,101)
    userdata.led_data.b = random.randrange(0,101)
    userdata.led_data.change_time = 0
    return userdata.led_data

class ResetEyesLedState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['led_data'])
    rospy.loginfo("SetEyesLedState")

    #resetLeftEyeLedState = SetLedAction(self, 'leftEye', 0, 0, 0, 0)
    #self.add_action(resetLeftEyeLedState)
    resetEyeLedState = SetLedAction(self, data_cb=self.led_data_cb, is_dynamic=True)
    self.add_action(resetEyeLedState)


  def led_data_cb(self, userdata):
    userdata.led_data.r = 0
    userdata.led_data.g = 0
    userdata.led_data.b = 0
    return userdata.led_data

######### Main function
def main():
  rospy.init_node("change_eyes_led")

  # State Machine Patrolling
  sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
  sm.userdata.sm_data = LedControl()

  with sm:
    sm.add('SetLED',SetEyesLedState(),transitions = {'succeeded':'ResetLED'}, remapping={'led_data':'sm_data'})
    sm.add('ResetLED',ResetEyesLedState(),transitions = {'succeeded':'SetLED'}, remapping={'led_data':'sm_data'})

  #sis = smach_ros.IntrospectionServer('change_eyes_led', sm, '/SM_ROOT')
  #sis.start()
  
  outcome = sm.execute()


if __name__ == '__main__':
  main()
