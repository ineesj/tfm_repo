#! /usr/bin/env python

# Author: Jose Nuno Pereira
# Description: Creates state machine that forces sequence of behaviors sequentially.

import roslib; roslib.load_manifest('mosmach')
import rospy
import math
import smach
import smach_ros
from functools import partial

from std_msgs.msg import *
from monarch_behaviors.srv import BehaviorRequest
from monarch_msgs.msg import BehaviorSelection, KeyValuePair, KeyValuePairArray

from sam_helpers.reader import SAMReader

from mosmach.monarch_state import MonarchState
from mosmach.change_conditions.topic_condition import TopicCondition



class BehaviorExecutionStatus:
	def __init__(self):
		self.behavior_finished = False
		self.time = rospy.Time.now()

	def set_behavior_finished(self, value):
		self.behavior_finished = value

	def get_behavior_finished(self):
		return self.behavior_finished

	def set_time(self, new_time):
		self.time = new_time

	def get_time(self):
		return self.time
		 

class WaitBehaviorState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['interactive_game','aborted'], input_keys=['behavior_name'], output_keys=['behavior_name'])
		rospy.loginfo("Init WaitBehaviorState")

		self.add_change_condition(TopicCondition(self, '/sar/mcentral/behavior_to_request', String, self.waitBehaviorCondition), ['interactive_game','aborted'])

	def waitBehaviorCondition(self, data, userdata):
		userdata.behavior_name = data.data
		if data.data == 'interactive_game':
			return data.data
		else:
			return 'aborted'


class InteractiveGameState(smach.State):
	"""Executes interactivegame behavior"""
	def __init__(self):
		smach.State.__init__(self,
							 outcomes = ['succeeded',
							 			 'preempted',
							 			 'aborted'])

	def execute(self, userdata):
		
		behavior = BehaviorSelection()
		behavior.name = 'interactivegame'
		behavior.instance_id = 0
		behavior.robots = [9, 0]
		behavior.resources = ['RNAV','interaction_interfaces']

		parameter = KeyValuePair()
		parameter.key = 'players'
		parameter.value = 'mbot09 human2'
		behavior.parameters.append(parameter)

		parameter = KeyValuePair()
		parameter.key = 'rows'
		parameter.value = '3'
		behavior.parameters.append(parameter)

		parameter = KeyValuePair()
		parameter.key = 'cols'
		parameter.value = '4'
		behavior.parameters.append(parameter)

		parameter = KeyValuePair()
		parameter.key = 'level'
		parameter.value = '0'
		behavior.parameters.append(parameter)

		parameter = KeyValuePair()
		parameter.key = 'tracking'
		parameter.value = '1'
		behavior.parameters.append(parameter)

		parameter = KeyValuePair()
		parameter.key = 'interaction'
		parameter.value = 'nonverbal'
		behavior.parameters.append(parameter)

		behavior.active = True

		response = request_behavior(behavior)
		if response.success != True:
			return 'aborted'

		r = rospy.Rate(5)
		while not rospy.is_shutdown():
			
			if self.preempt_requested() or rospy.is_shutdown():
				behavior.active = False
				request_behavior(behavior)
				behavior_result_reader.remove()
				behavior_mcentral_result_reader.remove()

				self.service_preempt()
				return 'preempted'

			else:
				return 'succeeded'

			r.sleep()


class CancelState(smach.State):
	"""cancels the plan """
	def __init__(self):
		smach.State.__init__(self,
							 outcomes = ['preempted'])

		self.subscriber = rospy.Subscriber('cancel_plan', Bool, self.cancel_cb)
		self.cancel = False

	def cancel_cb(self, data):
		if data.data == True:
			self.cancel = True

	def execute(self, userdata):


		r = rospy.Rate(5)
		while not rospy.is_shutdown():
			
			if self.preempt_requested() or self.cancel:
				self.subscriber.unregister()
				self.service_preempt()
				return 'preempted'

			r.sleep()


#
#   Main
#
    
if __name__ == '__main__':
  rospy.init_node('IPOL_demo_server')
  rospy.loginfo('Starting up')

  robot_id = '09'

  rospy.wait_for_service('/mcentral/request_behavior')
  request_behavior = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

  sm_sequence = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
  sm_sequence.userdata.behavior_name = 0
  with sm_sequence:

  	sm_sequence.add('WAIT_REQUEST', WaitBehaviorState(), transitions = {'interactive_game':'INTERACTIVEGAME','aborted':'WAIT_REQUEST'})
  	sm_sequence.add('INTERACTIVEGAME', InteractiveGameState(), transitions = {'succeeded':'WAIT_REQUEST','aborted':'WAIT_REQUEST','preempted':'preempted'})


  sm_cancel = smach.StateMachine(outcomes = ['preempted'])

  with sm_cancel: 
  	sm_cancel.add('CANCEL',CancelState())


  def term_cb(outcome_map):
  	return True

  def out_cb(outcome_map):
  	if outcome_map['SEQUENCE'] == 'succeeded':
  		return 'succeeded'
  	if outcome_map['CANCELSEQUENCE'] == 'preempted':
  		return 'preempted'
  	else:
  		return 'succeeded'

  sm_conc = smach.Concurrence(outcomes = ['succeeded',
  										   'preempted'],
  							   default_outcome = 'succeeded',
  							   child_termination_cb = term_cb,
  							   outcome_cb = out_cb)

  with sm_conc:
  	sm_conc.add('SEQUENCE',sm_sequence)
  	sm_conc.add('CANCELSEQUENCE',sm_cancel)


  sis = smach_ros.IntrospectionServer('IPOL_demo_server', sm_conc, '/SM_ROOT')
  sis.start()

  sm_conc.execute()
  rospy.spin()

