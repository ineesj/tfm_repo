#! /usr/bin/env python 

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
import sys
import threading

from mosmach.monarch_state import MonarchState
from mosmach.actions.sam_reader_action import SamReaderAction

from mosmach.util import pose2pose_stamped
from std_msgs.msg import *
from monarch_behaviors.msg import *
from move_base_msgs.msg import *
from monarch_msgs.msg import *
from geometry_msgs.msg import *
from monarch_msgs_utils import key_value_pairs as kvpa
from gesture_player_utils.msg import *

from sam_helpers.reader import SAMReader

class ReadSlots(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init ReadSlots")
		self.parameters_list = []
		self.starting_time = rospy.get_rostime()
		self.time_now = 0
		self.loadConfigParameters()
		
		self.number_sam_slots = int(self.parameters_list[0])
		self.sam_slot_prefix = str(self.parameters_list[1])
		self.sam_msg_type = str(self.parameters_list[2])
		self.publish_rate = int(self.parameters_list[3])

	def execute(self,userdata):
		# for slot in range(1,int(self.number_sam_slots)+1):  
		# 	self.sam_slot_name = str(self.sam_slot_prefix) + str(slot)
		self.sam_slot_name= 'People_Localization_Tracker_2'
		action_thread = threading.Thread(target = self.createReaders, args=(self.readerCondition,)).start()

		return "succeeded"

	def createReaders(self, callback):
		samReader = SAMReader(self.sam_slot_name , callback, '')


	def readerCondition(self, data):
		# self.time_now = rospy.get_rostime()

		# if hasattr(data, 'header'):
		# 	threadName = threading.current_thread().name.split("/")[3].split("_")[0]
		# 	self.writeDataLogs(threadName, data)

		print "received SAM data"


		return True

	def writeDataLogs(self, threadName, data):
		f = open("logs/"+str(threadName)+"_"+str(self.starting_time)+".txt", "a")
		self.time_secs_in_ms = (self.time_now.secs-data.header.stamp.secs)*1000
		self.time_nsecs_in_ms = (self.time_now.nsecs-data.header.stamp.nsecs)/1000000
		f.write("Reader tstamp: %s | Writer tsamp: %s | Diff tstamp: %s ms\n" % (str(self.time_now), str(data.header.stamp), str(self.time_secs_in_ms+self.time_nsecs_in_ms)))
		f.close()


	def loadConfigParameters(self):
		fopen = open('sam_test_config.txt', "r")
		lines = fopen.readlines()

		for line in lines:
			lline = line.splitlines()
			parameters = lline[0].split(" = ")
			self.parameters_list.append(str(parameters[1]))

		fopen.close()

	
def main():
	rospy.init_node('sam_reader_latency_test')

	# State Machine SAM Reader latency test
	sm = smach.StateMachine(outcomes = ['succeeded'])

	with sm:
		sm.add('READ_SAM_SLOTS', ReadSlots(), transitions = {'succeeded':'succeeded'})
	
	outcome = sm.execute()
	
	rospy.spin()


if __name__ == '__main__':
11	main()