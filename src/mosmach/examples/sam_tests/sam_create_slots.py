#! /usr/bin/env python 

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
import sys

from mosmach.monarch_state import MonarchState

from mosmach.util import pose2pose_stamped
from std_msgs.msg import *
from monarch_behaviors.msg import *
from move_base_msgs.msg import *
from monarch_msgs.msg import *
from geometry_msgs.msg import *
from monarch_msgs_utils import key_value_pairs as kvpa
from gesture_player_utils.msg import *


from monarch_situational_awareness.srv import CreateSlot
from monarch_situational_awareness.msg import SlotProperties

class CreateSlots(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init CreateSlots")
		self.parameters_list = []

		self.LoadConfigParameters()

		self.number_sam_slots = int(self.parameters_list[0])
		self.sam_slot_prefix = str(self.parameters_list[1])
		self.sam_msg_type = str(self.parameters_list[2])

	def execute(self, userdata):
		rospy.wait_for_service('create_slot')
		create_slot = rospy.ServiceProxy('create_slot', CreateSlot)

		for slot in range(1,int(self.number_sam_slots)+1):
			self.sam_slot_name = str(self.sam_slot_prefix)+str(slot)
			print self.sam_slot_name
			sp = SlotProperties(name = self.sam_slot_name, description = 'SAM tests', type_str = self.sam_msg_type, is_shared = True, is_latched = False)    
			r = create_slot(sp)

		return 'succeeded'

	def LoadConfigParameters(self):
		fopen = open('sam_test_config.txt', "r")
		lines = fopen.readlines()

		for line in lines:
			lline = line.splitlines()
			parameters = lline[0].split(" = ")
			self.parameters_list.append(str(parameters[1]))

		fopen.close()


def main():
	rospy.init_node('sam_create_slots')

	# State Machine SAM create slots
	sm = smach.StateMachine(outcomes = ['succeeded'])

	with sm:
		sm.add('CREATE_SAM_SLOTS', CreateSlots(), transitions = {'succeeded':'succeeded'})

	outcome = sm.execute()
	
	rospy.spin()


if __name__ == '__main__':
	main()
