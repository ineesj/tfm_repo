#! /usr/bin/env python 

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
import sys
from random import randint

from mosmach.monarch_state import MonarchState
from mosmach.actions.sam_writer_action import SamWriterAction

from mosmach.util import pose2pose_stamped
from std_msgs.msg import *
from monarch_behaviors.msg import *
from move_base_msgs.msg import *
from monarch_msgs.msg import *
from geometry_msgs.msg import *
from monarch_msgs_utils import key_value_pairs as kvpa
from gesture_player_utils.msg import *

from sam_helpers.writer import SAMWriter

class WriteSlots(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init WriteSlots")
		self.parameters_list = []

		self.LoadConfigParameters()
		
		self.number_sam_slots = int(self.parameters_list[0])
		self.sam_slot_prefix = str(self.parameters_list[1])
		self.sam_msg_type = str(self.parameters_list[2])
		self.publish_rate = int(self.parameters_list[3])

		for slot in range(1,int(self.number_sam_slots)+1):
			self.sam_slot_name = str(self.sam_slot_prefix) + str(slot)
			samSlotWriter = SamWriterAction(self, self.sam_slot_name ,'', self.writerCondition, is_dynamic = True)
			#samSlotWriter = SamWriterAction(self, self.sam_slot_name ,'', self.writerCondition, is_dynamic = True)
			self.add_action(samSlotWriter)

	def writerCondition(self, userdata, samWriter):
		msg_type = self.sam_msg_type.split("/")
		self.message = eval(msg_type[1]+"()")
		
		rate=rospy.Rate(self.publish_rate)
		while not rospy.is_shutdown():
			if hasattr(self.message, 'header'):
				self.message.header.stamp = rospy.get_rostime()
				self.message.pose.position.x = randint(0,100)
				self.message.pose.position.y = randint(0,100)
				self.message.pose.position.z = randint(0,100)
				self.message.pose.orientation.x = randint(0,100)
				self.message.pose.orientation.y = randint(0,100)
				self.message.pose.orientation.z = randint(0,100)
			
			samWriter.publish(self.message)
			rate.sleep()

		return self.message

	def LoadConfigParameters(self):
		fopen = open('sam_test_config.txt', "r")
		lines = fopen.readlines()

		for line in lines:
			lline = line.splitlines()
			parameters = lline[0].split(" = ")
			self.parameters_list.append(str(parameters[1]))

		fopen.close()

	
def main():
	rospy.init_node('sam_writer_latency_test')

	# State Machine SAM Writer latency test
	sm = smach.StateMachine(outcomes = ['succeeded'])

	with sm:
		sm.add('WRITE_SAM_SLOTS', WriteSlots())
	
	outcome = sm.execute()
	
	rospy.spin()


if __name__ == '__main__':
	main()