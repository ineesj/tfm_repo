#!/usr/bin/env python

import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.run_ca_action import RunCaAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading
from move_base_msgs.msg import MoveBaseGoal
from mosmach.util import pose2pose_stamped
from gesture_player_utils.msg import GestureStatus


######### Run CE's State Machine
class RunCE1State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init RunCE1State")

		greeting_name = 'mbot_warm_expression'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCE2State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init RunCE2State")

		greeting_name = 'give_greetings_child'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCE3State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init RunCE3State")

		greeting_name = 'mbot_follow_robot'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCE5State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init RunCE5State")

		greeting_name = 'give_greetings_child'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)

class RunCE7State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init RunCE7State")

		greeting_name = 'mbot_behavior_succeeded'
		runCeState = RunCeAction(self, greeting_name)
		self.add_action(runCeState)


######### Activate CA's State Machine
class ActivateCA15State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'])
		rospy.loginfo("Init ActivateCA15State")

		activateCAState = RunCaAction(self, 'ca15')
		self.add_action(activateCAState)



######### Patrolling State Machine
class MoveToWP1State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP1State")

		navMoveToOneAction = MoveToAction(self,2.45,12.30,-2.70)
		self.add_action(navMoveToOneAction)

		runCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runCeState)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP2State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP2State")

		navMoveToTwoAction = MoveToAction(self, -3.20, 9.5, 0.48)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP3State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP3State")

		navMoveToTwoAction = MoveToAction(self, 8.95, 32.95, -1.38)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP4State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP4State")

		navMoveToTwoAction = MoveToAction(self, 10.80, 22.95, -1.34)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP5State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP5State")

		navMoveToTwoAction = MoveToAction(self, 13.65, 9.85, 1.74)
		self.add_action(navMoveToTwoAction)

		runCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runCeState)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP6State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP6State")

		navMoveToTwoAction = MoveToAction(self, 13.05,19.90,1.76)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP7State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP7State")

		navMoveToTwoAction = MoveToAction(self, 11.20, 29.95, 1.69)
		self.add_action(navMoveToTwoAction)

		runCeState = RunCeAction(self, 'mbot_start_walking')
		self.add_action(runCeState)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value

class MoveToWP8State(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init MoveToWP8State")

		navMoveToTwoAction = MoveToAction(self, 7.80, 37.50, 1.88)
		self.add_action(navMoveToTwoAction)

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		while True:
			if data.tag_id == 5922:
				value = 'tag_detected'
				break

		return value


######### Dock State Machine
class SendDockState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
		rospy.loginfo("Init DockState")

		navSendToDockAction = MoveToAction(self, 5.15,42.60,0.22)
		self.add_action(navSendToDockAction)



######### Restart State
class RestartState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded','tag_detected'])
		rospy.loginfo("Init RestartState")

		topicCondition = TopicCondition(self, 'rfid_tag', RfidReading, self.rfidCondition)
		self.add_change_condition(topicCondition, ['tag_detected'])

	def rfidCondition(self, data, userdata):
		value = 'tag_detected'
		return value	



######### Main function
def main():
	rospy.init_node("patrolling_HRI_demo")


	# State Machine Patrolling + HRI
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])

	with sm:
		sm.add('CA1', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT1'})
		sm.add('WAYPOINT1', MoveToWP1State(), transitions = {'succeeded':'CE1','tag_detected':'DOCK'})
		sm.add('CE1', RunCE1State(), transitions = {'succeeded':'CA2'})

		sm.add('CA2', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT2'})
		sm.add('WAYPOINT2', MoveToWP2State(), transitions = {'succeeded':'CE2','tag_detected':'DOCK'})
		sm.add('CE2', RunCE2State(), transitions = {'succeeded':'CA3'})

		sm.add('CA3', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT3'})
		sm.add('WAYPOINT3', MoveToWP3State(), transitions = {'succeeded':'CE3','tag_detected':'DOCK'})
		sm.add('CE3', RunCE3State(), transitions = {'succeeded':'CA4'})
		
		sm.add('CA4', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT4'})
		sm.add('WAYPOINT4', MoveToWP4State(), transitions = {'succeeded':'CA1','tag_detected':'DOCK'})

		#sm.add('CA5', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT5'})
		#sm.add('WAYPOINT5', MoveToWP5State(), transitions = {'succeeded':'CE5','tag_detected':'DOCK'})
		#sm.add('CE5', RunCE5State(), transitions = {'succeeded':'CA6'})

		#sm.add('CA6', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT6'})
		#sm.add('WAYPOINT6', MoveToWP6State(), transitions = {'succeeded':'CA7','tag_detected':'DOCK'})

		#sm.add('CA7', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT7'})
		#sm.add('WAYPOINT7', MoveToWP7State(), transitions = {'succeeded':'CE7','tag_detected':'DOCK'})
		#sm.add('CE7', RunCE7State(), transitions = {'succeeded':'WAYPOINT8'})

		#sm.add('CA8', ActivateCA15State(), transitions = {'succeeded':'WAYPOINT8'})
		#sm.add('WAYPOINT8', MoveToWP8State(), transitions = {'succeeded':'CA1','tag_detected':'DOCK'})

		sm.add('DOCK', SendDockState(), transitions = {'succeeded':'RESTART'})
		sm.add('RESTART', RestartState(), transitions = {'tag_detected':'WAYPOINT1'})

	sis = smach_ros.IntrospectionServer('patrolling_and_dock', sm, '/SM_ROOT')
	sis.start()
	
	outcome = sm.execute()
	
	rospy.spin()


if __name__ == '__main__':
	main()
