#!/usr/bin/env python
import rospy
from monarch_msgs.msg import ArmsControl, LedControl, HeadControl, MouthLedControl
from geometry_msgs.msg import Twist
from math import sin, pi, floor, cos
import time

def mbot_actuators_test():
  rospy.init_node('mbot_actuators_test');
  r = rospy.Rate(20) # 20hz

#create publishers

  while not rospy.is_shutdown():
    str = "publishing info at %s"%rospy.get_time()
    rospy.loginfo(str)
    publish_arms()
    publish_leds()
    publish_mouth()
    publish_head()
    publish_vel()
    r.sleep()


def publish_arms():
  armsPub = rospy.Publisher('cmd_arms', ArmsControl, queue_size=10)
  ac = ArmsControl()
  t = time.time()
  ac.header.stamp = rospy.Time.now()
  ac.torque_enable = 1
  ac.left_arm = sin(t) * pi/4.0
  ac.right_arm = -sin(t) * pi/4.0
  ac.movement_time = 0.2
  armsPub.publish(ac)

def publish_leds():
  ledsPub = rospy.Publisher('cmd_leds', LedControl, queue_size=10)
  t = time.time()
  onIndex = floor((t/6.28) % 6)

  for n in range(0, 6):
    lc = LedControl()
    lc.header.stamp = rospy.Time.now()
    lc.device = n
    if n == onIndex:
      lc.r = (sin(t) + 1) * 128
      lc.g = (sin(t+1) + 1) * 128
      lc.b = (sin(t+2) + 1) * 128
      lc.change_time = 0
      ledsPub.publish(lc)
    else:
      lc.r = 0
      lc.g = 0
      lc.b = 0
      lc.change_time = 0
      ledsPub.publish(lc)


def publish_mouth():
  mouthPub = rospy.Publisher('cmd_mouth', MouthLedControl, queue_size=10)
  t = time.time()
  onIndex = floor( (t*10) % 255 )
  mc = MouthLedControl()
  mc.header.stamp = rospy.Time.now()
  mc.data = [False]*256
  mc.data[int(onIndex)] = True
  mouthPub.publish(mc)

def publish_head():
  headPub = rospy.Publisher('cmd_head', HeadControl, queue_size=10)
  t = time.time()
  hc = HeadControl()
  hc.header.stamp = rospy.Time.now()
  hc.head_pos = sin(t) * pi/2.0
  hc.movement_speed = 50
  headPub.publish(hc)

def publish_vel():
  velPub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
  t = time.time()
  tw = Twist()
  tw.linear.x = 0.5 * cos(t)
  tw.linear.y = 0.5 * sin(t)
  velPub.publish(tw)



