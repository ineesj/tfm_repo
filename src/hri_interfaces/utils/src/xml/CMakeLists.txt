add_definitions (-fpermissive)

add_library(mxml_utils mxmlUtils.cpp)
target_link_libraries(mxml_utils mxml)

add_library(xml_document XmlDocument.cpp)
target_link_libraries(xml_document string_utils ${catkin_LIBRARIES})

install(TARGETS mxml_utils xml_document
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

add_executable(tutorial_xml_document.exe tutorial_xml_document.cpp)
target_link_libraries(tutorial_xml_document.exe xml_document boost_system)
