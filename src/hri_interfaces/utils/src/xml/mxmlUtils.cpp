/**
 * \file mxmlUtils.cpp
 *
 * \date 24/06/2010
 * \author Victor Gonzalez Pacheco (mailto:vgonzale@ing.uc3m.es)
 */

#include "xml/mxmlUtils.h"

using std::string;

/**
 * \brief function to convert from a long int to a string
 * \param i the long int to convert to string
 * \return the stringised long integer
 */
string XmlUtils::intToString(long int i) {
    std::stringstream auxString;

    auxString.clear(std::stringstream::goodbit); // Clearing the stream flags
    auxString.str(""); // Clearing the stream data
    auxString << i; // Converting the long integer to ascii
    return (auxString.str());
}

/**
 * \brief function to convert from a double to a string
 * \param d the double to convert to string
 * \return the stringised double
 */
string XmlUtils::doubleToString(double d) {
    std::stringstream auxString;

    auxString.clear(std::stringstream::goodbit); // Clearing the stream flags
    auxString.str(""); // Clearing the stream data
    auxString << d; // Converting the double to ascii
    return (auxString.str());

}

/**
 * \brief Function used to extract the data inside one XML tag.
 *
 * Function used to extract the data inside one XML tag.
 * Example: in the tag \code<test>dummy</test>\endcode
 * this function extracts the "dummy" string
 *
 * \param *index The index where to find the parameter
 * \param *param a string the tag name
 * \param *value a string with the content inside the tag
 * \return ERR_PARAM_NF if the parameter is not found or the data field is empty
 * \return ERR_NOERR if success
 */
int XmlUtils::xmlFindParam(mxml_index_t *index, std::string param, std::string *value) {
    mxml_node_t *currentParam;
    mxmlIndexReset(index);
    currentParam = mxmlIndexFind(index, param.data(), NULL);

    if ((currentParam != NULL) && (currentParam->child != NULL)
            && (currentParam->child->value.element.name == NULL)) { // The node can't be an Element Node
        // It must be a text node
        value->assign(currentParam->child->value.text.string); // Extracting the text of the Text MXML Node
    } else {
        std::cout << "Did not enter in the if" << std::endl;
        //return (ERR_PARAM_NF); // Parameter not found
        return -1;
    }
    //return (ERR_NOERR); // Success
    return 0;
}

/**
 * Callback function to allow saving the driver configuration into xml file with human readable format
 *
 * This callback allows formating the xml file in a tree form. Each new element is displayed in a new line.
 * Also it prints as many "\t" characters before opening a tab as many parents the node has
 *
 * \see http://minixml.org/documentation.php/advanced.html for more information
 */
const char * XmlUtils::tabbed_cb(mxml_node_t *node, int where) {
    static const int maxParents = 10;
    mxml_node_t *currentNode;
    int numParents = 0;
    string returnFormat = "\n";

    // Writing as many "\t" characters (tabulations) as parents has the tab before opening the tab
    if (where == MXML_WS_BEFORE_OPEN) {
        currentNode = node;
        while ((numParents < maxParents) && (currentNode->parent != NULL)
                && (currentNode->parent->parent != NULL)) {
            returnFormat.append("\t");
            currentNode = currentNode->parent;
            numParents++;
        }
        return (returnFormat.data());

    } else if (where == MXML_WS_BEFORE_CLOSE) {

        // Before closing a node it should be written as many "\t" as parents the node has
        if (node->child->value.element.name != NULL) {
            currentNode = node;
            returnFormat.clear();
            returnFormat = "\n";
            while ((numParents < maxParents) && (currentNode->parent != NULL)
                    && (currentNode->parent->parent != NULL)) {
                returnFormat.append("\t");
                currentNode = currentNode->parent;
                numParents++;
            }

            return (returnFormat.data());
        }
    }

    return (NULL);
}

