/*!
  \file        tutorial_xml_document.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2013/2/21
  
________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

A tutorial for learning how to use the XmlDocument class.
 */

#include "xml/XmlDocument.h"

int main() {
  printf("tutorial_xml_document\n");
  printf("In this tutorial, we will learn how to handle XML documents.\n\n");

  printf("1. we will create a new document from scratch using:\n");
  printf("| XmlDocument doc;\n");
  XmlDocument doc;
  printf("The document now looks like:\n| doc.to_string();\n%s", doc.to_string().c_str());

  printf("2. we add this empty document a node using:\n");
  printf("| doc.add_node(doc.root(), \"node_name\", \"\");\n");
  doc.add_node(doc.root(), "node_name", "");
  printf("The document now looks like:\n| doc.to_string();\n%s", doc.to_string().c_str());

  printf("3. we get a pointer to this node and set an attribute to it:\n");
  printf("| XmlDocument::Node* node = doc.get_node_at_direction(doc.root(), \"node_name\");\n");
  printf("| doc.set_node_attribute<int>(node, \"attr\", 123);\n");
  XmlDocument::Node* node = doc.get_node_at_direction(doc.root(), "node_name");
  doc.set_node_attribute(node, "attr", 123);
  printf("The document now looks like:\n| doc.to_string();\n%s", doc.to_string().c_str());

  printf("4. we can get all nodes with the same direction in the XML tree:\n");
  printf("| std::vector<XmlDocument::Node*> nodes;\n");
  printf("| doc.get_all_nodes_at_direction(doc.root(), \"node_name\", nodes);\n");
  std::vector<XmlDocument::Node*> nodes;
  doc.get_all_nodes_at_direction(doc.root(), "node_name", nodes);
  printf("Here we have only %i node at this direction.\n\n", nodes.size());

  printf("5. These nodes can be processed:\n");
  printf("| for (unsigned int node_idx = 0; node_idx < nodes.size(); ++node_idx)\n");
  printf("|   doc.set_node_attribute(nodes[node_idx], \"new_attr\", 456);\n");
  for (unsigned int node_idx = 0; node_idx < nodes.size(); ++node_idx)
    doc.set_node_attribute(nodes[node_idx], "new_attr", 456);
  printf("The document now looks like:\n| doc.to_string();\n%s", doc.to_string().c_str());

  printf("6. Saving the document in an XML file is a breeze:\n");
  printf("| doc.write_to_file(\"/tmp/doc.xml\");\n\n");
  doc.write_to_file("/tmp/doc.xml");

  printf("7. Loading a document from an XML file is as easy:\n");
  printf("| XmlDocument doc2(\"/tmp/doc.xml\");\n");
  XmlDocument doc2("/tmp/doc.xml");
  printf("The new document is identical:\n| doc2.to_string();\n%s\n", doc2.to_string().c_str());
  return 0;
}
