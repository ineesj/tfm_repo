/**
 * \file mxmlUtils.h
 *
 * Some utilities and callbacks to use the libraries Mini-XML
 * \see http://www.minixml.org
 *
 * \date 24/06/2010
 * \author Victor González Pacheco (mailto:vgonzale@ing.uc3m.es)
 */

#ifndef MXMLUTILS_H_
#define MXMLUTILS_H_

// mxml
#include <mxml.h>

// std
#include <string>
#include <iostream>
#include <stdio.h>      // printf
#include <stdlib.h>
#include <sstream>

// maggie
//#include "definitions/ADerrors.h"

class XmlUtils {
public:
    /*
     * victor stuff
     */
    //#ifdef __cplusplus
    //extern "C" {
    //#endif

    static std::string intToString(long int i);
    static std::string doubleToString(double d);

    static int
    xmlFindParam(mxml_index_t *index, std::string param,
            std::string *value);
    static const char * tabbed_cb(mxml_node_t *node, int where);

    //#ifdef __cplusplus
    //}
    //#endif

    /* end vector stuff */

private:
};

#endif /* MXMLUTILS_H_ */

