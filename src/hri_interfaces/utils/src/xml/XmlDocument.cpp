/*!
 * \file XmlDocument.cpp
 *
 * The implementation of XmlDocument.h
 *
 * \date Dec 19, 2010
 * \author Arnaud Ramey
 */

#include "xml/XmlDocument.h"
#include "debug/debug_utils.h"
#include "string/extract_utils.h"

#include <string.h>

typedef XmlDocument::Node Node;

////////////////////////////////////////////////////////////////////////////////

XmlDocument::XmlDocument() {
  maggieDebug3("ctor");
  _path = "";
  _folder = "";

#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  _root = NULL;
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
#endif
}

////////////////////////////////////////////////////////////////////////////////

XmlDocument::XmlDocument(const std::string & filename) {
  //~ maggieDebug3("ctor");
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  _root = NULL;
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
#endif
  load_from_file(filename);
}

////////////////////////////////////////////////////////////////////////////////

XmlDocument::~XmlDocument() {
  //~ maggieDebug3("dtor");
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  mxmlDelete(_root);
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  doc.remove_all_nodes();
#endif
}

////////////////////////////////////////////////////////////////////////////////

void XmlDocument::write_to_file(const std::string & filename) const {
  maggieDebug3("write_to_file('%s')", filename.c_str());
  StringUtils::save_file(filename, to_string());
}

////////////////////////////////////////////////////////////////////////////////

bool XmlDocument::load_from_file(const std::string & filename) {
  maggieDebug3("load_from_file('%s')", filename.c_str());

  // store path
  _path = filename;
  _folder = filename;
  std::string::size_type slash_pos = _folder.find_last_of("/");
  if (slash_pos == std::string::npos)
    _folder = "";
  else
    _folder = _folder.substr(0, slash_pos + 1); // keep the last "/"

#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  // clean if needed
  if (root() != NULL)
    mxmlDelete(root());
#endif

  // load file
  std::string file_content;
  bool success = StringUtils::retrieve_file(filename, file_content);
  if (!success) {
    maggiePrint("Error while retrieving file '%s'. Aborting.",
                filename.c_str());
    return false;
  }
  return load_from_string(file_content);
}

////////////////////////////////////////////////////////////////////////////////

bool XmlDocument::load_from_string(const std::string & file_content) {
  maggieDebug3("load_from_string('%s')", file_content.c_str());

#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  _root = mxmlLoadString(NULL, file_content.c_str(), MXML_NO_CALLBACK);
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  // make a safe-to-modify copy of input_xml
  // (you should never modify the contents of an std::string directly)
  xml_copy = std::vector<char> (file_content.begin(), file_content.end());
  xml_copy.push_back('\0');

  // parse_no_data_nodes prevents RapidXML from using the somewhat surprising
  // behavior of having both values and data nodes, and having data nodes take
  // precedence over values when printing
  // >>> note that this will skip parsing of CDATA nodes <<<
  try {
    doc.parse<rapidxml::parse_no_data_nodes>(&xml_copy[0]);
  } catch (rapidxml::parse_error & e) {
    maggiePrint("Error while parsing '%s':%s",
                string_utils::extract(file_content).c_str(),
                e.what());
    return false;
  }
  return true;
#endif
}

////////////////////////////////////////////////////////////////////////////////

std::string XmlDocument::get_value(const Node* start, const std::string & path) const {
  maggieDebug3("get_value('%s')", path.c_str());
  Node* node = get_node_at_direction(start, path);
  if (node == NULL) {
    maggieDebug3("The node ''%s'' does not exist in the xml doc", path.c_str());
    return "";
  }
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  //maggieDebug2("type:%i", node->type);
  //maggieDebug2("type:'%s'", node->child->value.text.string);
  // get the child
  node = node->child;
  if (node == NULL) {
    maggieDebug2("The node ''%s'' does not have any child", path.c_str());
    return "";
  }
  // get the value
  return node->value.text.string;
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  return node->value();
#endif
}

std::string XmlDocument::get_node_name(const Node *node)  const {
  maggieDebug2("get_node_name()");
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  if (node->type == MXML_ELEMENT)
    return node->value.element.name;
  return "";
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  return node->name();
#endif
}

////////////////////////////////////////////////////////////////////////////////

std::string XmlDocument::get_node_attribute(const Node *node,
                                            const std::string & attribute)  const {
  //maggieDebug2("get_node_attribute('%s')", attribute.c_str());
  if (node == NULL) {
    maggieDebug2("The node is NULL !");
    return "";
  }

#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  std::string ans = mxmlElementGetAttr(node, attribute.c_str());
  return ans;
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  rapidxml::xml_attribute<char>* attrib = node->first_attribute(attribute.c_str());
  if (attrib == NULL) {
    //~ maggieDebug1("The node does not contain a '%s' attribute !", attribute.c_str());
    return "";
  }
  return attrib->value();
#endif
}

////////////////////////////////////////////////////////////////////////////////

Node* XmlDocument::get_node_at_direction(const Node* start,
                                         const std::string & path)  const {
  //~ maggieDebug3("get_node_at_direction('%s')", path.c_str());
  if (start == NULL) {
    //~ maggieDebug2("The node is NULL !");
    return NULL;
  }

  // cut the path into words
  std::vector<std::string> words;
  StringUtils::StringSplit(path, ".", &words);

  Node *current_node = start;
  for (std::vector<std::string>::const_iterator word = words.begin(); word
       != words.end(); ++word) {
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
    current_node = mxmlFindElement(current_node, current_node,
                                   word->c_str(), NULL, NULL, MXML_DESCEND);
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
    current_node = current_node->first_node(word->c_str());
#endif
    if (current_node == NULL) {
      //~ maggieDebug1("We didin't find the word '%s' in the xml doc", word->c_str());
      return NULL;
    } // end if current == NULL
  } // end loop word
  return current_node;
}

////////////////////////////////////////////////////////////////////////////////

Node* XmlDocument::get_node_at_direction(const Node* start,
                                         const std::string & path,
                                         const std::string & attribute_name,
                                         const std::string & attribute_value) const {
  //~ maggieDebug3("get_node_at_direction(path:'%s', attribute_name:'%s', attribute_value:'%s')",
               //~ path.c_str(), attribute_name.c_str(), attribute_value.c_str());

  // get all the children
  std::vector<Node*> children_at_path;
  get_all_nodes_at_direction(start, path, children_at_path);

  // see if there is a child with the wanted attribute
  for(std::vector<Node*>::const_iterator child = children_at_path.begin();
      child < children_at_path.end() ; ++child) {
    std::string child_attribute_value = get_node_attribute(*child, attribute_name);
    //  maggieDebug2("child_attribute_value:'%s', attribute_value:'%s'",
    //               child_attribute_value.c_str(), attribute_value.c_str());
    if (child_attribute_value == attribute_value)
      return *child;
  } // end loop son

  // if we arrived here, we didn't find the child
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////

void XmlDocument::get_all_nodes_at_direction(const Node* start,
                                             const std::string & path,
                                             std::vector<Node*>& ans)  const {
  //~ maggieDebug3("get_all_nodes_at_direction('%s')", path.c_str());

  // find the first sibling
  Node* sibling = get_node_at_direction(start, path);
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  Node* father = sibling->parent;
#endif
  ans.clear();

  // find the last word
  std::vector<std::string> words;
  StringUtils::StringSplit(path, ".", &words);
  std::string last_word = words.back();

  while (sibling != NULL) {
    ans.push_back(sibling);
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
    sibling = mxmlFindElement(sibling, father, last_word.c_str(), NULL,
                              NULL, MXML_DESCEND);
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
    sibling = sibling->next_sibling(last_word.c_str());
#endif
  } // end while sibling != NULL
}

////////////////////////////////////////////////////////////////////////////////

void XmlDocument::get_all_children(Node* father, std::vector<Node*>& ans)  const {
  maggieDebug2("get_all_children()");
  ans.clear();
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  Node* current = father->child;
  while (current != NULL) {
    ans.push_back(current);
    current = current->next;
  }
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  Node* current = father->first_node();
  while (current != NULL) {
    ans.push_back(current);
    current = current->next_sibling();
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////

Node* XmlDocument::root() const {
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  return _root;
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  return (Node*) &doc;
#endif
}

////////////////////////////////////////////////////////////////////////////////

Node* XmlDocument::add_node(Node* father, Node* node_to_add, const std::string & path) {
  maggieDebug3("add_node(path:'%s')", path.c_str());
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  maggieError("Not implemented");
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  if (path == "") {
    // append it
    father->append_node(node_to_add);
    return node_to_add;
  } else {
    // split the first word and the rest
    // example "a.b.c" -> "a" and "b.c"
    size_t dot_pos = path.find('.');
    std::string first_word = (dot_pos == std::string::npos ? path : path.substr(0, dot_pos-1));
    std::string path_end = (dot_pos == std::string::npos ? "" : path.substr(dot_pos+1));

    // add the first son if needed
    Node* first_son = get_node_at_direction(father, first_word);
    if (first_son == NULL)
      first_son = add_node(father, first_word.c_str(), "");

    // add what is left recursively
    return add_node(first_son, node_to_add, path_end);
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////

Node* XmlDocument::add_node(Node* father, const std::string & node_name, const std::string & path) {
  maggieDebug3("add_node(node_name:'%s', path:'%s')", node_name.c_str(), path.c_str());
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  maggieError("Not implemented");
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  /* make a copy of the std::string into an allocated char array */
  char* node_name_array = doc.allocate_string(node_name.c_str());
  /* create the node */
  Node* son = doc.allocate_node(rapidxml::node_element, node_name_array);

  /* call the Node version of the function */
  return add_node(father, son, path);
#endif
}

////////////////////////////////////////////////////////////////////////////////

//inline void XmlDocument::set_node_value(Node* node, const std::string & value) {
//    maggieDebug2("set_node_value(value:'%s')", value.c_str());
//#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
//    maggieError("Not implemented");
//#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
//    maggieError("Not implemented");
//#endif
//}

////////////////////////////////////////////////////////////////////////////////

void XmlDocument::set_node_attribute(Node* node, const std::string & attr_name, const std::string & value) {
  maggieDebug3("set_node_value(attr_name:'%s', value:'%s')", attr_name.c_str(), value.c_str());
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  maggieError("Not implemented");
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  // check if we need to remove the attribute beforehand
  rapidxml::xml_attribute<char>* attrib = node->first_attribute(attr_name.c_str());
  if (attrib != NULL) {
    maggieDebug3("The node already had a '%s' attribute, removing it", attr_name.c_str());
    node->remove_attribute(attrib);
  }

  // make a copy of the std::string into an allocated char array
  char* attr_name_array = doc.allocate_string(attr_name.c_str());
  char* value_array = doc.allocate_string(value.c_str());
  // create the node
  rapidxml::xml_attribute<>* new_attr = doc.allocate_attribute(attr_name_array, value_array);
  // append it
  node->append_attribute(new_attr);
#endif
}

////////////////////////////////////////////////////////////////////////////////

std::string XmlDocument::to_string() const {
  maggieDebug3("to_string()");
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  maggieError("Not implemented");
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  std::ostringstream stream;
  stream << "<?xml version=\"1.0\" ?>" << std::endl;
  stream << doc;
  return stream.str();
#endif
  //return to_string_node(root());
}

////////////////////////////////////////////////////////////////////////////////

std::string XmlDocument::to_string_node(XmlDocument::Node* node) const {
  maggieDebug2("to_string_node()");
#if XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_MXML
  maggieError("Not implemented");
#elif XML_USED_IMPLEMENTATION == XML_IMPLEMENTATION_RAPID_XML
  std::ostringstream stream;
  stream << *node;
  return stream.str();
#endif
}
