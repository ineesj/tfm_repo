/**
 * @file timer.cpp
 * @brief Software API to use time counters, timeouts, etc.
 *        The implementation of "timers.h"
 * @author Víctor Gonzalez vgonzale@ing.uc3m.es
 * @date 28 January 2009
 */

#include "time/timer.h"

/**
 * @brief Returns a time difference in microseconds
 *
 * @param before is the reference time value to compare
 * @param after is the time wich we want to compare
 *
 * @return the diference between before and after in microseconds
 */
long int timeDifferenceUsec(struct timeval * before,
        struct timeval * after) {
    /** @TODO Error control here */
    return ((after->tv_sec - before->tv_sec) * 1000000 + (after->tv_usec
            - before->tv_usec));
}

/**
 * @brief   Returns a time difference in milliseconds
 *
 * @param before is the reference time value to compare
 * @param after is the time wich we want to compare
 *
 * @return the diference between before and after in milliseconds
 */
long int timeDifferenceMsec(struct timeval * before,
        struct timeval * after) {
    /** @TODO Error control here */
    return ((after->tv_sec - before->tv_sec) * 1000 + (after->tv_usec
            - before->tv_usec) / 1000);
}

long int getSeconds() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec;
}

long int getMiliSeconds() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_usec;
}


#ifdef __cplusplus // Fer C++ function
std::string getDateTime(){
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X // %d-%m-%Y", &tstruct);
    return std::string(buf);
}
#endif

