/*!*
 * \file test_Timer.cpp
 *
 * A test for the different timers
 *
 * \date 10/11/2010
 * \author Arnaud Ramey
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "time/timer.h"
#include "debug/debug_utils.h"

void test_time_differences() {
    maggieDebug2("test_time_differences()");

    struct timeval before, after;
    //char semFile[128];
    char keyboardInput[16];
    suseconds_t microsec;
    long int timeDifferenceUsecs, timeDifferenceMsecs;

    printf("How much time do you want to sleep?\n");
    printf("Write a number in micro seconds (useconds):");
    if (!scanf("%s", keyboardInput))
        exit(-1);

    microsec = atoi(keyboardInput);
    printf("You entered: %d useconds\n", (int) microsec);
    gettimeofday(&before, 0);
    printf("Time Now: %d.%d\n", (int) before.tv_sec, (int) before.tv_usec);

    usleep(microsec);

    gettimeofday(&after, 0);
    printf("Time after: %d.%d\n", (int) after.tv_sec, (int) after.tv_usec);

    timeDifferenceUsecs = timeDifferenceUsec(&before, &after);
    timeDifferenceMsecs = timeDifferenceMsec(&before, &after);

    printf("Time diference in milliseconds: %ld\n", timeDifferenceMsecs);
    printf("Time diference in microseconds: %ld\n", timeDifferenceUsecs);
}

void test_timers2() {
    long int seconds1 = getSeconds();
    usleep(2000000);
    long int seconds2 = getSeconds();
    printf("Han pasado %li segundos\n", seconds2 - seconds1);
}

int main() {
    test_time_differences();

}

