/*!
  \file        NetworkUtils.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        Feb 6, 2011

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

 */

#include "network/NetworkUtils.h"
#include "debug/error.h"
// std c++
#include <sstream>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int NetworkUtils::read_from_socket(const int fd, const int command_size,
                                   std::string & ans) {
  maggieDebug2("read_from_socket(command_size:%i)", command_size);

  // Lectura del nuevo tipo enviado
  char packet_item [command_size + 1];
  int total_read_bytes = 0;
  std::ostringstream total_packet;

  /*
     * read the wanted number of bytes.
     * It is possible we don't read enough bytes the first time
     * (not all of them accessible at once),
     * so we do it in a loop
     */
  while (true) {
    // FIXME add a select here in case the socket dies

    int read_bytes = read(fd, packet_item, command_size);
    maggieDebug2("Bytes read on this loop : %i", read_bytes);
    // check if we received an error
    if (read_bytes == -1) {
      maggiePrint("Error: read() returned an error:'%s'", //
                  strerror(errno));
      return -1;
    } else if (read_bytes == 0) {
      maggieError("We read 0 bytes in read()");
      return -1;
    }
    // copy what we read in the buffer
    packet_item[read_bytes] = '\0'; // terminate C string
    total_packet << std::string(packet_item, read_bytes);
    total_read_bytes += read_bytes;
    // quit if we have read enough
    if (total_read_bytes >= command_size)
      break;
    //      if (read_bytes != command_size) {
    //          maggiePrint(
    //                  "Error: we received an incorrect nb of bytes: read_bytes=%i != cmd.tamDatos=%i.", //
    //                  (int) read_bytes, command_size);
    //          return write_error_code(socket, MCP_RETURN_INCORRECT_NB_BYTES);
    //      }
  } // end loop read()
  //delete[] packet_item;
  //maggieDebug2("Finished reading packet.");
  maggieDebug2("packet:'%s'", packet_item);

  // copy to the answer string
  ans = total_packet.str();
  return 0;
}

int NetworkUtils::write_to_socket(const int fd, const std::string & to_write) {
  maggieDebug2("write_to_socket(length():%i)", (int) to_write.length());
  /*
     * write the wanted number of bytes.
     * It is possible we don't write enough bytes the first time
     * (pipe saturation),
     * so we do it in a loop
     */
  int total_written_bytes = 0;
  int total_to_write = to_write.length();
  while (true) {
    std::string item_packet_left = to_write.substr(total_written_bytes);
    maggieDebug2("Bytes to write on this loop : %i:'%s'",
                 (int) item_packet_left.length(), item_packet_left.c_str());
    // FIXME add a select here in case the socket dies

    int written_bytes = write(fd, item_packet_left.c_str(), item_packet_left.length());
    maggieDebug2("Bytes written on this loop : %i", written_bytes);
    if (written_bytes == -1) {
      maggiePrint("Error: write() returned an error:'%s'", //
                  strerror(errno));
      return -1;
    }
    total_written_bytes += written_bytes;
    // quit if have read enough
    if (total_written_bytes >= total_to_write || written_bytes == 0) {
      maggieDebug2("written_bytes:%i, total_written_bytes:%i, total_to_write:%i. Breaking.",
                   written_bytes, total_written_bytes, total_to_write);
      break;
    }
  }
  return 0;
}

