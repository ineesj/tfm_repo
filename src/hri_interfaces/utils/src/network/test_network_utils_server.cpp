/*!
  \file        test_network_utils_client.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        Feb 6, 2011

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

Some tests for NetworkUtils.
Based on
http://www.linuxhowtos.org/C_C++/socket.htm
substituting read() and write() by their NetworkUtils equivalents.
 */

// C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
// AD
#include "network/NetworkUtils.h"
#include "debug/debug_utils.h"

#define MYPORT 13892 // maggie port

void error(const char *msg) {
  perror(msg);
  exit(1);
}

int main() {
  int sockfd, newsockfd, portno = MYPORT;
  socklen_t clilen;
  char buffer[256];
  struct sockaddr_in serv_addr, cli_addr;
  int n;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");
  bzero((char *) &serv_addr, sizeof(serv_addr));
  // portno = atoi(argv[1]);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
  if (bind(sockfd, (struct sockaddr *) &serv_addr,
           sizeof(serv_addr)) < 0)
    error("ERROR on binding");
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0)
    error("ERROR on accept");

  bzero(buffer,256);
  n = read(newsockfd,buffer,255);
  // we don't know how much we will receive, so cannot use NetworkUtils::read_from_socket()
  //// std::string buffer_str;
  //// n = NetworkUtils::read_from_socket(newsockfd, 255, buffer_str);
  if (n < 0)
    error("ERROR reading from socket");
  //// memcpy(buffer, buffer_str.data(), (1+buffer_str.size()) * sizeof(char)); // convert to C-string
  printf("Here is the message: '%s'\n",buffer);

  n = write(newsockfd,"I got your message",18);
  //std::string message("I got your message\n");
  //n = NetworkUtils::write_to_socket(sockfd, message.c_str());

  if (n < 0)
    error("ERROR writing to socket");
  close(newsockfd);
  close(sockfd);
  return 0;
}
