/*!
  \file        test_network_utils_client.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        Feb 6, 2011

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

Some tests for NetworkUtils.
Based on
http://www.linuxhowtos.org/C_C++/socket.htm
substituting read() and write() by their NetworkUtils equivalents.
 */

// C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
// AD
#include "network/NetworkUtils.h"
#include "debug/debug_utils.h"

#define MYPORT 13892 // maggie port

void error(const char *msg) {
  perror(msg);
  exit(1);
}

int main() {
  int sockfd, portno = MYPORT, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;

  char buffer[256];
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");

  //server = gethostbyname(argv[1]);
  server = gethostbyname("localhost");
  if (server == NULL) {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
  }
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,
        (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);
  serv_addr.sin_port = htons(portno);
  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    error("ERROR connecting");
  printf("Please enter the message:\n");
  bzero(buffer,256);
  char* ignored = fgets(buffer,255,stdin);
  if (ignored) {} // get rid of the flag

  //n = write(sockfd,buffer,strlen(buffer));
  n = NetworkUtils::write_to_socket(sockfd, std::string(buffer));

  if (n < 0)
    error("ERROR writing to socket");

  bzero(buffer,256);
  //n = read(sockfd,buffer,255);
  //// here we know we wait for 18 characters
  std::string buffer_str;
  n = NetworkUtils::read_from_socket(sockfd, 18, buffer_str);
  if (n < 0)
    error("ERROR reading from socket");
  strcpy(buffer, buffer_str.data()); // convert to string
  printf("Buffer read from sockfd: '%s'\n",buffer);
  close(sockfd);
  return 0;
}
