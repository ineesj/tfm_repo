#ifndef T_TO_STRING_H_
#define T_TO_STRING_H_

#include <sstream>

/**
 * Plantilla para covertir casi cualquier tipo a string
 * source: http://notfaq.wordpress.com/2006/08/30/c-convert-int-to-string/
 */
template <class T>
inline std::string to_string (const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

#endif /*T_TO_STRING_H_*/
