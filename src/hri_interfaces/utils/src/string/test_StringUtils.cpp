/*!
 * \file test_StringUtils.cpp
 *
 * Some tests for the StringUtils
 *
 * \date Dec 5, 2010
 * \author Arnaud Ramey
 */

#include "string/StringUtils.h"
#include "string/replace_utils.h"
#include "debug/debug_utils.h"
#include "string/T_to_map.h"
#include "system/system_utils.h"
#include "time/timer.h"
#include <iostream>
using namespace std;

void test_StringSplit(const std::string & input) {
  cout << endl;
  maggiePrint("test_StringSplit('%s')", input.c_str());
  vector<std::string> values;
  StringUtils::StringSplit(input, "||", &values);
  for (std::vector<std::string>::const_iterator it = values.begin();
       it != values.end(); ++it)
    maggiePrint("Element:'%s'", it->c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_StringSplit_() {
  cout << endl;
  maggiePrint("test_StringSplit_()");
  string input = "1.2 xxx 2.5 xxx    -1";
  maggiePrint("Before StringSplit_:'%s'", input.c_str());
  vector<float> values;
  StringUtils::StringSplit_<float>(input, "xxx", &values);
  for (std::vector<float>::const_iterator it = values.begin(); it
       != values.end(); ++it)
    maggiePrint("Element:'%f'", *it);
}

////////////////////////////////////////////////////////////////////////////////

void test_find_and_replace() {
  cout << endl;
  maggiePrint("test_find_and_replace()");
  string input = "The ball is redred with blue stripes. blue";
  maggiePrint("Before find_and_replace:'%s'", input.c_str());
  StringUtils::find_and_replace(input, "red", "blue");
  StringUtils::find_and_replace(input, "blue", "black");
  maggiePrint("After find_and_replace:'%s'", input.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_clean_spanish() {
  cout << endl;
  maggiePrint("test_clean_spanish();");
  string input = "Weird characters: ¿¡áéàúüñ : One in a word : niño.";
  maggiePrint("Before clean_spanish_chars:'%s'", input.c_str());
  StringUtils::clean_spanish_chars(input);
  maggiePrint("After clean_spanish_chars:'%s'", input.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_retrieve_url() {
  cout << endl;
  maggiePrint("test_retrieve_url();");
  string content;
  StringUtils::retrieve_url("http://www.google.es/", content);
  maggiePrint("Beginning of url:'%s'", content.substr(0, 200).c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_retrieve_file() {
  cout << endl;
  maggiePrint("test_retrieve_file()");
  string content;

  string filename = "~/.bashrc";
  maggiePrint("Reading '%s'", filename.c_str());
  StringUtils::retrieve_file(filename, content);
  maggiePrint("Beginning of %s:'%s'", filename.c_str(), content.substr(0, 50).c_str());

  filename = "/foo/bar.txt";
  maggiePrint("Reading '%s'", filename.c_str());
  StringUtils::retrieve_file(filename, content);
  maggiePrint("Beginning of %s:'%s'", filename.c_str(), content.substr(0, 50).c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_retrieve_file_split() {
  cout << endl;
  vector<string> content;
  string filename = "~/.bashrc";
  maggiePrint("Reading '%s'", filename.c_str());
  StringUtils::retrieve_file_split(filename, content);
  maggiePrint("5 first lines of '%s'", filename.c_str());
  for (int lineIdx = 0; lineIdx < 5; ++lineIdx) {
    maggiePrint("#%i:'%s'", lineIdx, content.at(lineIdx).c_str());
  }
}

////////////////////////////////////////////////////////////////////////////////

void test_convert_string_encoding() {
  cout << endl;
  maggiePrint("test_convert_string_encoding()");

  string input = "Mañana";
  maggiePrint("Initial value:'%s'", input.c_str());
  StringUtils::convert_string_encoding_utf_to_iso(input);
  maggiePrint("After convert utf -> iso:'%s'", input.c_str());
  StringUtils::convert_string_encoding_iso_to_utf(input);
  maggiePrint("After convert iso -> utf:'%s'", input.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_clean_string(const std::string & input) {
  cout << endl;
  maggiePrint("test_clean_string(%s)", input.c_str());

  string output1 = input, output2 = input;
  maggiePrint("Initial value:'%s'", input.c_str());
  StringUtils::remove_beginning_spaces(output1);
  maggiePrint("After remove_beginning_spaces:'%s'", output1.c_str());
  StringUtils::remove_trailing_spaces(output2);
  maggiePrint("After remove_beginning_spaces:'%s'", output2.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_save() {
  cout << endl;
  maggiePrint("test_save()");

  std::ostringstream m;
  m << "foo" << 123;
  string filepath = "/home/user/foo.txt";
  string m_str = m.str();
  StringUtils::save_file(filepath, m_str);
}

////////////////////////////////////////////////////////////////////////////////

void test_cast() {
  cout << endl;
  maggiePrint("test_cast()");

  // from string
  bool success;
  maggiePrint("to_cast:('%s', to_cast_str:%i)", //
              "123", StringUtils::cast_from_string<int>("123", success));
  maggiePrint("to_cast:('%s', to_cast_str:%f)", //
              "105.2", StringUtils::cast_from_string<double>("105.2", success));

  // to string -> round trip
  int to_cast = 567;
  string to_cast_str = StringUtils::cast_to_string<int>(to_cast, success);
  maggiePrint("to_cast:%i, to_cast_str:'%s', to_cast_str -> int:%i", //
              to_cast, to_cast_str.c_str(), StringUtils::cast_from_string<int>(to_cast_str, success));
}

////////////////////////////////////////////////////////////////////////////////

void test_read_and_write() {
  cout << endl;
  maggiePrint("test_read_and_write()");

  std::string filename = "/tmp/foo_test.tmp";
  std::string filename2 = "/tmp/foo_test2.tmp";

  // generate the file
  ostringstream command;
  command << "echo 1 > " << filename;
  system_utils::exec_system(command.str());
  command.str("");
  command << "echo 2 >> " << filename;
  system_utils::exec_system(command.str());

  // read it and write it into another file
  std::string content;
  StringUtils::retrieve_file(filename, content);
  StringUtils::save_file(filename2, content);

  // use command diff
  command.str("");
  command << "diff " << filename << " " << filename2;
  system_utils::exec_system(command.str());

  // do the same thing with a vector
  std::vector<std::string> lines;
  StringUtils::retrieve_file_split(filename, lines);
  StringUtils::save_file_split(filename2, lines);

  // use command diff
  command.str("");
  command << "diff " << filename << " " << filename2;
  system_utils::exec_system(command.str());
}

////////////////////////////////////////////////////////////////////////////////

void test_to_map(const std::string & input) {
  typedef std::map<std::string,float> t_my_map;
  cout << endl;
  maggiePrint("test_to_pair('%s')", input.c_str());
  t_my_map m = to_map<std::string,float>(input,":,");
  //t_my_map m = to_map<std::string,float>(input); //utiliza los separadores por defecto
  cout << "map contains:\n";
  for (t_my_map::iterator it=m.begin() ; it != m.end(); it++ )
    cout << (*it).first << " => " << (*it).second << endl;
}

////////////////////////////////////////////////////////////////////////////////

void test_replace_all_values_between_tags() {
  cout << endl;
  maggiePrint("test_replace_all_values_between_tags()");
  std::string in = "> hello <foo> hello2 > hello3 <";
  Timer timer;
  int nb_times = string_utils::replace_all_values_between_tags
      (in, "<", ">", "***");
  maggiePrint("%g ms : in:'%s' (%i times)", timer.time(), in.c_str(), nb_times);

  in = "<> <> >< <>";
  nb_times = string_utils::replace_all_values_between_tags
      (in, "<", ">", "***");
  maggiePrint("%g ms : in:'%s' (%i times)", timer.time(), in.c_str(), nb_times);
}

////////////////////////////////////////////////////////////////////////////////

int main() {
  test_StringSplit("");
  test_StringSplit("||");
  test_StringSplit("a");
  test_StringSplit("a||");
  test_StringSplit("||a");
  test_StringSplit("||a||b||c||");
  test_StringSplit_();
  test_find_and_replace();
  test_clean_spanish();
  test_retrieve_url();
  test_retrieve_file();
  test_retrieve_file_split();
  test_convert_string_encoding();
  test_clean_string(" a ");
  test_clean_string("b ");
  test_clean_string(" c");
  test_save();
  test_cast();
  test_read_and_write();
  test_to_map("alvaro castro gonzalez:29,raul castro gonzalez:34,sergio castro gonzalez:2");
  test_replace_all_values_between_tags();
  maggiePrint("time_stamp:%s", StringUtils::timestamp().c_str());
}

