#ifndef T_TO_PAIR_H_
#define T_TO_PAIR_H_

#include <sstream>
#include <map>
#include <string>
#include <string.h>
#include "string/string_casts.h"

/**
 * Tome un string formateado como A1:A2,B1|B2;etc... y devuelve un mapa cuyas claves se corresponden con *1 y los valores serán *2.
 * En la plantilla se indicarán los dos tipos a los que hay que convertir cada uno de los campos.
 * T1 será el tipo de la clave del mapa, y T2 será el tipo del valor asociado.
 * Como separadores de campos, se toma por defecto los incluidos en esta cadena " ;:,|#".
 * Si se quieren modificar sólo tienen que pasárselos como segundo parámetro a la función.
 */
template <class T1, class T2>
std::map<T1,T2> to_map(const std::string& st, std::string separadores=" ;:,|#")
{
    typedef std::map<T1,T2> t_my_map;
    t_my_map m;
    std::pair<typename t_my_map::iterator,bool> ret; //this is used to, after inserting, check  if the new element alreay existed in the map.

    //Tokenizamos el string para obtener todos los campos
    int longitud = st.size()+1;
    char cadena[longitud];
    strcpy(cadena,st.c_str());
    char *p = strtok(cadena,separadores.c_str());
    while (p!=NULL) //in this loop, values are read 2by2
    {
        bool success1, success2;
        std::string s1(p);
        T1 t1 = StringUtils::cast_from_string<T1>(s1, success1);
        p=strtok(NULL,separadores.c_str());
        if(p!=NULL)
        {
            std::string s2(p);
            T2 t2 = StringUtils::cast_from_string<T2>(s2, success2);
            p=strtok(NULL,separadores.c_str());

            //std::cout << t1 << " - " << t2 << std::endl;
            if(success1 && success2)
                m.insert(std::pair<T1,T2>(t1,t2));
        }
    }
    return m;
}

#endif /*T_TO_PAIR_H_*/
