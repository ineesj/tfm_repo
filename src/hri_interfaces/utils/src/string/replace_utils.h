#ifndef REPLACE_UTILS_H
#define REPLACE_UTILS_H

#include <string>
#include <sstream>

namespace string_utils {
/*!
  Replace all the utterances of a pattern starting with block_begin
  and end with block_end
 \param content
          where to replace the pattern between the blocks
 \param block_begin
          how the searched pattern starts
 \param block_end
          how the searched pattern ends
 \param new_pattern
          what will be between block_begin and block_end at the end
 \return int the number of times it was replaced
*/
int replace_all_values_between_tags(std::string & content,
                                    const std::string & block_begin,
                                    const std::string & block_end,
                                    const std::string new_pattern = "",
                                    std::string::size_type pos_begin = 0) {
  std::string::size_type pos_end;
  int nb_times = 0;
  while (true) {
    pos_begin = content.find(block_begin, pos_begin);
    if (pos_begin == std::string::npos)
      return nb_times;
    pos_end = content.find(block_end, pos_begin);
    if (pos_end == std::string::npos)
      return nb_times;
    // both block_begin and block_end have now been found
    content.replace(pos_begin, pos_end + block_end.size() - pos_begin, new_pattern);
    //maggiePrint("content:'%s'", content.c_str());
    ++nb_times;
  } // end while (true)
}

} // end namespace string_utils

#endif // REPLACE_UTILS_H
