/*!
  \file        test_string_utils_ros.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2012/12/11

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

\todo Description of the file
*/

#include "string_utils_ros.h"
#include "time/timer.h"

#include <std_msgs/Int8.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/String.h>

template<class _T>
inline void test_io(const _T & obj) {
  _T obj2;
  Timer timer;
  for (unsigned int i = 0; i < 100; ++i) {
    string_utils_ros::ros_object_to_file("/tmp/obj.dat", obj);
    string_utils_ros::ros_object_from_file("/tmp/obj.dat", obj2);
  } // end loop i
  timer.printTime_factor("ros_object_to_file -> ros_object_from_file", 100);
  ROS_WARN("obj=== obj2? %i", obj.data == obj2.data);
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
  srand(time(NULL));
  ros::init(argc, argv, "test_string_utils_ros");
  ROS_INFO("test_string_utils_ros");
  std_msgs::Int8 obj8;
  obj8.data = rand();
  test_io(obj8);

  std_msgs::Int16 obj16;
  obj16.data = rand();
  test_io(obj16);

  std_msgs::Int32 obj32;
  obj32.data = rand();
  test_io(obj32);

  std_msgs::Int64 obj64;
  obj64.data = rand();
  test_io(obj64);

  std_msgs::String obj_string;
  obj_string.data = "foo"; /*StringUtils::random_string(rand() % 1000)*/;
  test_io(obj_string);
  return 0;
}



