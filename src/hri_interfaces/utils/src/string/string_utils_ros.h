/*!
  \file        string_utils_ros.h
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2012/12/11

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

Some utils for serializing ROS objects.

 */

#ifndef STRING_UTILS_ROS_H
#define STRING_UTILS_ROS_H

#include "string/file_io.h"

namespace string_utils_ros {

template<class _T>
std::string ros_object_to_string(const _T & object) {
  uint32_t serial_size = ros::serialization::serializationLength(object);
  //boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  //ros::serialization::OStream stream_out(buffer.get(), serial_size);
  uint8_t buffer_uint[serial_size];
  ros::serialization::OStream stream_out(buffer_uint, serial_size);
  // ros::serialization::serialize(stream, object);
  ros::serialization::Serializer<_T>::write(stream_out, object);

  //char* buffer_signed = reinterpret_cast<char*>(buffer.get());
  // std::string object_str(buffer_signed, serial_size);
  //std::string object_str((char*) buffer_uint, serial_size);
  //  ROS_WARN("serial_size:%i, object_str size:%i",
  //           serial_size, object_str.size());
  return std::string((char*) buffer_uint, serial_size);
} // end ros_object_to_string()

////////////////////////////////////////////////////////////////////////////////

template<class _T>
void ros_object_to_file(const std::string & filename,
                        const _T & object) {
  StringUtils::save_file(filename, ros_object_to_string(object));
}

////////////////////////////////////////////////////////////////////////////////

template<class _T>
void ros_object_from_string(const std::string & object_str,
                            _T & my_value) {
  // deserialize
  //_T my_value;
  //uint32_t serial_size2 = ros::serialization::serializationLength(my_value);
  uint32_t serial_size2 = object_str.size();
  //boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  // ROS_WARN("serial_size2:%i", serial_size2);

  // Fill buffer with a serialized UInt32
  // ros::serialization::IStream stream_in(buffer.get(), serial_size2);
  //ros::serialization::IStream stream_in(buffer_uint, serial_size2);
  ros::serialization::IStream stream_in((unsigned char*) object_str.data(), serial_size2);
  //ros::serialization::deserialize(stream, my_value);
  ros::serialization::Serializer<_T>::read(stream_in, my_value);
}

////////////////////////////////////////////////////////////////////////////////

template<class _T>
void ros_object_from_file(const std::string & filename,
                          _T & object) {
  std::string object_str;
  StringUtils::retrieve_file(filename, object_str);
  ros_object_from_string(object_str, object);
}

} // end namespace string_utils_ros


#endif // STRING_UTILS_ROS_H
