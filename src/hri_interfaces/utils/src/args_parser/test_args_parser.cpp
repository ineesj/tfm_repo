#include <iostream>
#include "debug/debug_utils.h"
#include "args_parser/args_parser.h"

int main(int argc, char** argv) {
    maggiePrint("main()");

    // build a fancy string help
    std::ostringstream help_stream;
    help_stream
    << "A test program for ArgsParser." << std::endl
    << "This program can be tested with   "
    << argv[0]
    << "--param1 10 --foo --param3 fooop3 --foo2 --param2 5.1 --flag1 param1=15";

    // create the parser
    ArgsParser parser(argc, argv, help_stream.str());

    // add the flags and parameters to the parser
    ArgsFlag* flag1 = new ArgsFlag(
                parser, "flag1", "a test flag, true as default", true);
    ArgsParam<int>* param1 = new ArgsParam<int>(
                parser, "param1", "a first param test, int", 42);
    ArgsParam<double>* param2 = new ArgsParam<double>(
                parser, "param2", "a second param test, double", 3.14);
    ArgsParam<std::string>* param3 = new ArgsParam<std::string>(
                parser, "param3", "a third param test, string", "default");

    // make the parsing
    parser.parse();
    //    if (parser.parse())
    //        return 0;


    // direct method
    maggiePrint("Reading values through direct instances of the parameters");
    maggiePrint("flag1:           %i - is_defined()=%i", flag1->get_value(), flag1->is_defined());
    maggiePrint("param1:          %i - is_defined()=%i", param1->get_value(), param1->is_defined());
    maggiePrint("param2:          %f - is_defined()=%i", param2->get_value(), param2->is_defined());
    maggiePrint("param3:          '%s' - is_defined()=%i", param3->get_value().c_str(), param3->is_defined());
    std::cout << std::endl;

    // indirect method 1 - get a pointer to the param DEPRECATED
    //    maggiePrint("Reading values through a pointer to the param");
    //    const ArgsParam<int>* param1_cp = NULL;
    //    bool param_exists = parser.get_param<const ArgsParam<int>* >("param1", param1_cp);
    //    maggiePrint("param_exists:    %i", param_exists);
    //    maggiePrint("param1_cp:       %i - is_defined()=%i", param1_cp->get_value(), param1_cp->is_defined());
    //    std::cout << std::endl;

    // indirect method 2 - directly get the values
    maggiePrint("Reading values through a search within the parser");
    maggiePrint("flag1 M2:        %i - is_defined()=%i",
                parser.get_value_flag("flag1"),
                parser.is_param_defined("flag1"));
    maggiePrint("param2 Method 2: %f - is_defined()=%i",
                parser.get_value_param<double>("param2"),
                parser.is_param_defined("param2"));
}
