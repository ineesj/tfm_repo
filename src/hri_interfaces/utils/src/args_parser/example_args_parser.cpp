#include "debug/debug_utils.h"
#include "args_parser/args_parser.h"

int main(int argc, char** argv) {
    maggiePrint("main()");
    maggiePrint("This is a test program for the ArgsParser lib.");
    maggiePrint("It has four defined parameters: a flag. and three params.");
    maggiePrint("You can get all these parameters by calling.'%s --help'", argv[0]);
    maggiePrint("This program can be tested with '%s %s'", argv[0],
                "--param1 10 --foo --param3 fooop3 --foo2 --param2 5.1 --flag1 --param1 15");

    // create the parser
    ArgsParser parser(argc, argv, "An example program for ArgsParser.");

    // add the flags and parameters to the parser
    // for a flag, the syntax is new ArgsFlag(parser, <flag name>, <caption>, <default value>);
    new ArgsFlag(parser, "flag1", "a test flag, true as default", true);

    // add the flags and parameters to the parser
    // for a flag, the syntax is new ArgsFlag(parser, <flag name>, <caption>, <default value>);
    new ArgsParam<int>(parser, "param1", "a first parameter, a int", 42);
    new ArgsParam<double>(parser, "param2", "a second parameter, a double", 3.14);
    new ArgsParam<std::string>(parser, "param3", "a third parameter, a string", "default_value");

    // the --help flag is automatically added, nothing to do here

    // make the parsing
    parser.parse();

    // get the values
    maggiePrint("flag1: value:%i \t\tis_defined()=%i",
                parser.get_value_flag("flag1"),
                parser.is_param_defined("flag1"));
    maggiePrint("param1: value:%i \t\tis_defined()=%i",
                parser.get_value_param<int>("param1"),
                parser.is_param_defined("param1"));
    maggiePrint("param2: value:%g \t\tis_defined()=%i",
                parser.get_value_param<double>("param2"),
                parser.is_param_defined("param2"));
    maggiePrint("param3: value:'%s' \tis_defined()=%i",
                parser.get_value_param<std::string>("param3").c_str(),
                parser.is_param_defined("param3"));

    // the new flags will be cleaned when the ArgsParser destructor is called
    // so nothing to do to clean!
}
