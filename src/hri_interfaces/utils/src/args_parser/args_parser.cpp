#include "args_parser/args_parser.h"


ArgsGenericParam::ArgsGenericParam(ArgsParser & arg_parser,
                                   const std::string & name,
                                   const std::string & caption) :
  _name(name),
  _caption(caption),
  _is_defined(0) {
  //    maggieDebug2("ctor");
  arg_parser.add_param(this);
}

////////////////////////////////////////////////////////////////////////////////

std::string ArgsFlag::get_caption_full() const {
  maggieDebug3("get_caption_full()");
  std::ostringstream info;
  info << "flag: "  << get_caption() << " ["
       << StringUtils::cast_to_string(_is_activated_by_default) << "]";
  return info.str();
}

////////////////////////////////////////////////////////////////////////////////

bool ArgsFlag::read_from_string(const int , char** argv, int & argi) {
  // chagen the param as defined by the user's argc, argv
  set_defined();
  // switch the flag
  _is_activated = !_is_activated;

  // nothing more to do if the param is just a flag
  maggieDebug3(" * '%s' : found, it is a flag.", argv[argi]);
  return true; // always success
}

////////////////////////////////////////////////////////////////////////////////

ArgsParser::ArgsParser(int argc, char** argv,
                       const std::string & general_help /*= ""*/) :
  _max_param_name_length(0),
  _general_help(general_help) {
  _argc = argc;
  _argv = argv;
  help_param = new ArgsFlag(*this, "help", "display this help");
}

////////////////////////////////////////////////////////////////////////////////

ArgsParser::~ArgsParser() {
  maggieDebug3("dtor - params.size():%i", _params.size());
  for (int var = 0; var < _params.size(); ++var)
    delete _params[var];
  _params.clear();
}

////////////////////////////////////////////////////////////////////////////////

bool ArgsParser::parse() {
  // maggieDebug2("parse(_params.size:%i, _argc:%i)", _params.size(), _argc);

  bool error_parsing = false;

  // check all the arguments
  for(int argi = 1 ; argi < _argc ; ++argi) {
    std::string current_arg ( _argv[argi] );
    std::string current_arg_no_prefix;
    maggieDebug3("current_arg:'%s'", current_arg.c_str());

    // check if the current arg is a valid argument
    // do nothing if it is the first argument
    if (argi == 0) {
      maggieDebug2(" * '%s' : ignored as first argument.", current_arg.c_str());
      continue;
    }
    // old style: --param value
    else if (current_arg.substr(0, 2) == "--") {
      maggieDebug3(" * '%s' : an arg, (old) style: --paramName paramValue", current_arg.c_str());
      current_arg_no_prefix = current_arg.substr(2);
    }
    // ROS hidden args: starting with _ : ignore
    else if (current_arg[0] == '_') {
      maggieDebug2(" * '%s' : starts with a '_', probably a ROS hidden arg, ignored.",
                   current_arg.c_str());
      continue;
    }
    // (ROS) style: paramName=paramValue
    else if (current_arg.find("=") != std::string::npos) {
      maggieDebug3(" * '%s' : an arg, (ROS) style: paramName=paramValue", current_arg.c_str());
      current_arg_no_prefix = current_arg.substr(0, current_arg.find("="));
      //maggiePrint("current_arg_no_prefix:'%s'", current_arg_no_prefix.c_str());
    }
    // otherwise generate an error
    else {
      maggiePrint(" * '%s' : not a vaild parameter.", current_arg.c_str());
      error_parsing = true; // to print help
      break;
    } // end check

    bool current_arg_was_recognized = false;

    // iteratively search an param corresponding to this arg
    for (ArgsParamList::iterator param_it = _params.begin();
         param_it != _params.end() ; ++param_it) {
      // check if the names are equal
      if ((*param_it)->get_name() != current_arg_no_prefix)
        continue;

      current_arg_was_recognized = true;
      // parse the next arguments
      bool success = (*param_it)->read_from_string(_argc, _argv, argi);
      if (success) {
        //  maggieDebug2(" * '%s' : read_from_string() succeeded, value:'%s'",
        //               current_arg_no_prefix.c_str(),
        //               (*param_it)->get_value_as_string().c_str() );
      }
      else {
        maggiePrint(" * '%s' : read_from_string() failed.",
                    current_arg_no_prefix.c_str());
        error_parsing = true; // to print help
        break;
      }
    } // end loop param_it

    if (!current_arg_was_recognized) {
      maggiePrint(" * '%s' : not in the possible parameters.", current_arg_no_prefix.c_str());
      error_parsing = true; // to print help
      break;
    }
  } // end loop argi

  // display help if needed
  if (error_parsing) {
    maggiePrint("There was an error while parsing...");
  }

  if (help_param->is_defined() == false && error_parsing == false)
    return false;

  maggiePrint("Displaying help...");
  // print the general help if defined
  if (_general_help.size() > 0) {
    printf("\n%s\n\n", _general_help.c_str());
  }

  // print the help for each argument
  for (ArgsParamList::const_iterator param_it = _params.begin();
       param_it != _params.end() ; ++param_it) {
    int space_length = _max_param_name_length + HELP_SPACER_LENGTH
        - (*param_it)->get_name().size();
    std::string spacer(space_length , ' ');
    printf("--%s%s%s\n",
           (*param_it)->get_name().c_str(),
           spacer.c_str(),
           (*param_it)->get_caption_full().c_str());
  } // end loop param

  exit(0);
  return true;
}

////////////////////////////////////////////////////////////////////////////////

void ArgsParser::add_param(ArgsGenericParam* param) {
  maggieDebug3("add_param('%s')", param->get_name().c_str());
  //maggieDebug2("size:%i", _params.size());
  // update the max length
  _max_param_name_length = std::max(_max_param_name_length, param->get_name().length());
  _params.push_back(param);
  //maggieDebug2("OK");
}

////////////////////////////////////////////////////////////////////////////////

bool ArgsParser::is_param_defined(const std::string & param_name) const {
  maggieDebug3("is_param_defined('%s')", param_name.c_str());

  // try to get the param
  ArgsGenericParam* param = NULL;
  bool param_exists = get_param<ArgsGenericParam*>(param_name, param);
  if (!param_exists)
    return false;
  // read its value
  return param->is_defined();
}

////////////////////////////////////////////////////////////////////////////////

bool ArgsParser::get_value_flag(const std::string & param_name) const {
  maggieDebug3("get_value_flag('%s')", param_name.c_str());
  ArgsFlag* param = NULL;
  get_param< ArgsFlag* >(param_name, param);
  return param->get_value();
}
