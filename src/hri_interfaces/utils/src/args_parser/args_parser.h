#ifndef ARGS_PARSER_H
#define ARGS_PARSER_H

/*!
   A generic command-line arguments parsing.
   It comes with a comprehensive documentation
   and some tests and examples.
   To see how to use it, cf "example_args_parser.cpp"

   \author arnaud
   \date 2011
  */

////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <list>
#include <cstdlib>

#include "args_parser/args_parser.h"
#include "string/string_casts.h"
#include "debug/debug_utils.h"

//! a forward definition before ArgsGenericParam
class ArgsParser;

////////////////////////////////////////////////////////////////////////////////

/*!
 * A generic class describing a parameter for the parser.
 * It has two sons: ArgsFlag and ArgsParam
 * ArgsGenericParam shoud not be instantiated, but these two sons yes.
 * In such a manner, we are able to make a vector of them.
 *
 * \see ArgsFlag
 * \see ArgsParam
 */
class ArgsGenericParam {
public:

  /*! ctor
      \param name name of the param, for instance "foo" for "--foo"
      \param caption a sentence to explain what the flag is about
     */
  ArgsGenericParam(ArgsParser & arg_parser,
                   const std::string & name,
                   const std::string & caption);

  virtual ~ArgsGenericParam() {
    maggieDebug3("dtor:%s", get_name().c_str());
  }


  // to be defined by sons:

  /*! \return the value of the parameter */
  template<class _T>
  inline _T get_value() const {
  }
  /*! \return the value of the parameter as string */
  inline virtual std::string get_value_as_string() const {
    return "";
  }

  /*! get a caption string containing also the default value
        \example "--flag: an param for changing the foo param" for --foo */
  virtual std::string get_caption_full() const {
    return "";
  }

  // defined for ArgsGenericParam

  /*! \return the name of the param
        \example "foo" for --foo */
  inline std::string get_name() const {
    return _name;
  }

  /*! \return the caption of the param
        \example "an param for changing the foo param" for --foo */
  inline std::string get_caption() const {
    maggieDebug3("get_caption()");
    return _caption;
  }

  /*! \return true if the param was defined by the user's argc, argv
        \warning This should not be mistaken with get_value() for a flag.
        For instance, if the flag "--foo" that is by default enabled,
        and the user puts "--foo" in his command line instruction,
        is_defined() is true,
        but get_value("foo") is false.
    */
  bool is_defined() const {
    return _is_defined;
  }

protected:
  friend class ArgsParser; // to access read_from_string()

  /*! read from the command line args
        \param argi it is already pointing at the word after the flag
        \return true if the read was succesful */
  virtual inline bool read_from_string(const int , char** argv, int & argi) {
    maggieDebug2("read_from_string('%s')", argv[argi]);
    return true;
  }

  //! the name of the param
  std::string _name;

  //! the caption of the param
  std::string _caption;

  /*! set the param as defined by the user's argc, argv */
  inline void set_defined() {
    _is_defined = true;
  }
  bool _is_defined;
};

////////////////////////////////////////////////////////////////////////////////

/*!
    A flag is a parameter that does not need a value.
    For instance, --Wall
 */
class ArgsFlag : public ArgsGenericParam {
public:
  /*!
      \see ArgsGenericParam::ArgsGenericParam()
      \param is_activated_by_default true if the flag is set by default
      */
  ArgsFlag(ArgsParser & arg_parser,
           const std::string & name,
           const std::string & caption,
           const bool is_activated_by_default = false) :
    ArgsGenericParam(arg_parser, name, caption),
    _is_activated(is_activated_by_default) ,
    _is_activated_by_default(is_activated_by_default) {
  }

  ~ArgsFlag() {
    maggieDebug3("dtor:%s", get_name().c_str());
  }


  /*! get a caption string containing also the default value
        \example "--flag: an param for changing the foo param" for --foo */
  std::string get_caption_full() const;

  /*! \see ArgsGenericParam::get_value() */
  inline bool get_value() const {
    return _is_activated;
  }
  /*! \see ArgsGenericParam::get_value_as_string()
        \return "true" if get_value()=true, "false" otherwise */
  inline std::string get_value_as_string() const {
    return StringUtils::cast_to_string(get_value());
  }

private:
  /*! \see ArgsGenericParam::read_from_string() */
  bool read_from_string(const int argc, char** argv, int & argi);

  bool _is_activated;
  bool _is_activated_by_default;
};

////////////////////////////////////////////////////////////////////////////////

/*! a generic type of param */
template<class _T>
class ArgsParam : public ArgsGenericParam {
public:
  /*!
      \see ArgsGenericParam::ArgsGenericParam()
      \param default_value the default value
      */
  ArgsParam(ArgsParser & arg_parser,
            const std::string & name,
            const std::string & caption,
            const _T default_value) :
    ArgsGenericParam(arg_parser, name, caption),
    _value(default_value),
    _default_value(default_value) {
  }
  ~ArgsParam() {
    maggieDebug3("dtor:%s", get_name().c_str());
  }

  /*! \see ArgsGenericParam::get_value() */
  inline _T get_value() const {
    return _value;
  }
  /*! \see ArgsGenericParam::get_value_as_string()
        \example "3.14" if get_value() == 3.14 */
  inline std::string get_value_as_string() const {
    return StringUtils::cast_to_string<_T>(get_value());
  }

  //! an overload to contain the type caption
  inline std::string get_caption_full() const {
    maggieDebug3("get_caption_full()");
    std::ostringstream info;
    info << "param"
         << "<" << StringUtils::cast_type_to_string<_T>() << ">: "
         << get_caption() << " ["
         << StringUtils::cast_to_string<_T>(_default_value) << "]";
    return info.str();
  }

private:

  /*! \see ArgsGenericParam::read_from_string() */
  inline bool read_from_string(const int argc,
                               char** argv,
                               int & argi) {
    bool convert_success;

    /*
     * first try (ROS) style: paramName=paramValue
     */
    // get after :=
    std::string current_arg ( argv[argi] );
    std::size_t equal_pos = current_arg.find("=");
    if (equal_pos != std::string::npos) {
      std::string value_string = current_arg.substr(equal_pos + 1);
      //maggiePrint("value_string:'%s'", value_string.c_str());
      // try to convert it, but in a copy of value so as not to lose the default
      // value if it fails
      _T _value_new = StringUtils::cast_from_string<_T> (value_string, convert_success);
      if (convert_success) {
        _value = _value_new;
        set_defined();
        // we should not skip any parameter with that style
        return true;
      } // end if convert_success
    }

    /*
     * then try style --paramName paramValue
     */
    // check bounds
    if (argi >= argc - 1) {
      maggieDebug1("Out of bounds !");
      return false;
    }
    // if it is a param, we need to read the next param
    std::string next_arg ( argv[argi + 1] );
    // try to convert it, but in a copy of value so as not to lose the default
    // value if it fails
    _T _value_new = StringUtils::cast_from_string<_T> (next_arg, convert_success);
    if (convert_success) {
      _value = _value_new;
      set_defined();
      argi++; // skip the parameter for the next parsing
      return true;
    } // end if convert_success

    return false;
  } // end read_from_string()

  _T _value;
  _T _default_value;
};

////////////////////////////////////////////////////////////////////////////////

/*! a class for parsing the arguments */
class ArgsParser {
public:
  /*! ctor - to be fed with the inputs
    \param argc argc from command line
    \param argv argv from command line
    \param general_help the help string that will be displayed before
    the detail of the arguments when calling "--help"
    */
  ArgsParser(int argc, char** argv, const std::string & general_help = "");

  //! dtor
  ~ArgsParser();

  /*! add an param for the parser
    \arg param a pointer to the param.
    It will always be a new ArgsFlag(...) or a new ArgsParam(...)
    \see ArgsFlag
    \see ArgsParam
     */
  void add_param(ArgsGenericParam* param);

  /*!
     parse the arguments - the main function
     Must be called:
     - AFTER all add_param()
     - BEFORE all get_value_*(), is_param_defined(), etc
     \return true if help was asked
    */
  bool parse();

  /*!
    Know if one param was user defined in the argc, argv
    \param name the name of the param
    \return true if the param is defined
    \warning This should not be mistaken with get_value() for a flag.
    For instance, if the flag "--foo" that is by default enabled,
    and the user puts "--foo" in his command line instruction,
    is_defined() is true,
    but get_value("foo") is false.
    */
  bool is_param_defined(const std::string & param_name) const ;

  /*!
     Know if one flag is set
     \param name the name of the flag
     \return true if the flag is set
    */
  bool get_value_flag(const std::string & param_name) const ;

  /*!
     Get the value of a parameter
     \param name the name of the param
     \return the value of the parameter
    */
  template<class _T>
  _T get_value_param(const std::string & param_name) const {
    ArgsParam<_T>* param = NULL;
    get_param< ArgsParam<_T>* >(param_name, param);
    return param->get_value();
  }

private:
  /*!
     Returns a pointer to a loaded parameter
     \param param_name name of the parameter
     \param ans the pointer that will be modified
     \return bool true if the parameter name was found
    */
  template<class _T>
  inline bool get_param(const std::string & param_name, _T & ans) const {
    maggieDebug3("get_param('%s') - _params:%i elems",
                 param_name.c_str(), (int) _params.size());
    for (ArgsParamList::const_iterator param_it = _params.begin();
         param_it != _params.end() ; ++param_it) {
      if ((*param_it)->get_name() == param_name) {
        //const ArgsGenericParam* param_ptr = &(*param_it);
        ans = dynamic_cast<_T>(*param_it);
        return true;
      }
    } // end loop param

    // we failed to find the param
    maggieDebug2("get_param('%s') -> not found !", param_name.c_str());
    ans = dynamic_cast<_T>(_params.front());
    return false;
  }

  typedef std::vector<ArgsGenericParam*> ArgsParamList;

  //! the space between the param name and its caption in the help
  static const int HELP_SPACER_LENGTH = 4;

  //! the longest length of the name params
  size_t _max_param_name_length;
  /*! the vector of wanted params - the class has pointers to the params
        They are cleaned in the ArgsParser destructor (delete) */
  ArgsParamList _params;
  /*! the help param - must be declared after params */
  ArgsFlag* help_param;

  /*! the general caption for the program*/
  std::string _general_help;
  //! storage of argc
  int _argc;
  //! storage of argv
  char** _argv;

};

#endif // ARGS_PARSER_H
