
#include "debug/error.h"
#include "translator/Translator.h"
#include "string/StringUtils.h"

void test_translate_sentence(const std::string & test_sentence,
                             const Translator::LanguageId & language_orig,
                             const Translator::LanguageId & language_dest) {
  std::cout << std::endl;
  std::string res = Translator::translate(test_sentence,
                                          language_orig,
                                          language_dest);
  maggiePrint("res:'%s'", res.c_str());
}

void test_translate() {
  test_translate_sentence("dog",
                          Translator::LANGUAGE_ENGLISH, Translator::LANGUAGE_SPANISH);
  test_translate_sentence("Welcome in my house.",
                          Translator::LANGUAGE_ENGLISH, Translator::LANGUAGE_SPANISH);
  test_translate_sentence("Te veo mañana.",
                          Translator::LANGUAGE_SPANISH, Translator::LANGUAGE_ENGLISH);
  test_translate_sentence("¿Quien es?",
                          Translator::LANGUAGE_SPANISH, Translator::LANGUAGE_ENGLISH);

  // unknown input
  test_translate_sentence("I am happy.",
                          Translator::LANGUAGE_UNKNOWN, Translator::LANGUAGE_SPANISH);
  test_translate_sentence("Han är på besök hos sin syster.",
                          Translator::LANGUAGE_UNKNOWN, Translator::LANGUAGE_ENGLISH);
  test_translate_sentence("Ca semble fonctionner, super.",
                          Translator::LANGUAGE_UNKNOWN, Translator::LANGUAGE_ENGLISH);
}

void test_extract_language_id() {
  Translator::LanguageMap map;
  Translator::build_languages_map(map);

  Translator::CountryDomain ans_prefix;
  Translator::LanguageId ans_id;
  bool success;

  for (int sentence_idx = 0; sentence_idx < 2; ++sentence_idx) {
    std::string sentence = (sentence_idx == 0 ? "en:this is english dude !" :
                                                "zh-CN:a sentence in chinese...");
    success = Translator::_extract_country_domain(sentence, ans_prefix);
    maggiePrint("success:%i, _extract_country_domain('%s') = '%s'",
                success, sentence.c_str(), ans_prefix.c_str());
    success = Translator::_extract_language_id(sentence, map, ans_id);
    maggiePrint("success:%i, _extract_language_id('%s') = %i",
                success, sentence.c_str(), ans_id);
  } // end loop
}

void test_find_given_language_in_multilanguage_line() {
  Translator::LanguageMap map;
  Translator::build_languages_map(map);

  std::string input = "en:english|es:spanish|fr:French";
  std::vector<std::string> versions;
  StringUtils::StringSplit(input, "|", &versions);
  std::string ans;
  bool success;

  Translator::LanguageId current_language_id = Translator::LANGUAGE_SPANISH;
  success = Translator::_find_given_language_in_multilanguage_line(
        versions, current_language_id, map, ans);
  maggieDebug2("_find_given_language_in_multilanguage_line() with lang=%i: success:%i, '%s'",
               current_language_id, success, ans.c_str());

  current_language_id = Translator::LANGUAGE_FRENCH;
  success = Translator::_find_given_language_in_multilanguage_line(
        versions, current_language_id, map, ans);
  maggieDebug2("_find_given_language_in_multilanguage_line() with lang=%i: success:%i, '%s'",
               current_language_id, success, ans.c_str());
}

int main() {
  maggiePrint("main()");
  test_translate();
  //test_extract_language_id();
  //test_find_given_language_in_multilanguage_line();

  //    std::string gibberish = Translator::_translateToGibberishLanguage("Hello I am testing the translation");
  //    printf("Gibberish Language: %s\n",gibberish.c_str());
  return 0;
}
