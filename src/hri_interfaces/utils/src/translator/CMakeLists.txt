add_library(translator Translator.cpp)
target_link_libraries(translator network_utils string_utils boost_system)

install(TARGETS translator
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

add_executable(test_translator.exe test_Translator.cpp)
target_link_libraries(test_translator.exe translator ${catkin_LIBRARIES})
