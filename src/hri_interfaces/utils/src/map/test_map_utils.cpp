/*!
  \file        test_map_utils.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2012/5/28

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

A test for map_utils.h
 */

#include <string>
#include <sstream>
#include <iostream>
#include "map_utils.h"

using namespace std;

int main(int argc, char** argv) {
  map<int, string> map;
  map.insert(pair<int, string>(1, "value1"));
  map.insert(pair<int, string>(2, "value2"));
  map.insert(pair<int, string>(3, "value3"));
  map.insert(pair<int, string>(4, "value4"));

  // direct search
  for (int key = 0; key < 7; ++key) {
    string value = "";
    bool success = map_utils::direct_search(map, key, value);
    cout << "direct_search(" << key << "): "
         << "success=" << success << ", value='" << value <<"'" << endl;
  } // end loop key

  // reverse search
  for (int key = 0; key < 7; ++key) {
    ostringstream value; value << "value" << key;
    int found_key = -1;
    bool success = map_utils::reverse_search(map, value.str(), found_key);
    cout << "reverse_search(" << value.str() << "): "
         << "success=" << success << ", found_key='" << found_key <<"'" << endl;
  } // end loop key

  return 0;
}


