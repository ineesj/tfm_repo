
#include "debug/debug_utils.h"
#include "debug/error.h"
#include <iostream>

#if AD_USE_ROS
#include "ros/ros.h"
#endif

#include "debug/error.h"

int main(int argc, char** argv) {
  maggieDebug2("main()");

#if AD_USE_ROS
  ros::init(argc, argv, "test_debug_utils");
  ros::NodeHandle foo;
  sleep(2);
#endif

  maggiePrint("This is a maggiePrint() ");
  maggiePrint("- it will be printed whatever might be MAGGIE_debug_LEVEL. ");
  maggiePrint(" It is more or less equivalent to a printf()");

  std::cout << std::endl;
  maggiePrint("Now comes a test for a maggieDebug1()");
  maggiePrint("- it will be printed if MAGGIE_debug_LEVEL <= 1.");
  maggieDebug1("This is a maggieDebug1()");

  std::cout << std::endl;
  maggiePrint("Now comes a test for a maggieDebug2()");
  maggiePrint("- it will be printed if MAGGIE_debug_LEVEL <= 2.");
  maggieDebug2("This is a maggieDebug2()");

  std::cout << std::endl;
  maggiePrint("Now comes a test for a maggieDebug3()");
  maggiePrint("- it will be printed if MAGGIE_debug_LEVEL <= 3.");
  maggieDebug3("This is a maggieDebug3()");

  std::cout << std::endl;
  maggiePrint("All maggiePrint() and maggieDebugX() can print arguments too");
  std::string string_example = "foo";
  maggiePrint("Example: int:%i, float:%g, string:'%s'",
              10, 3.14159, string_example.c_str());

  std::cout << std::endl;
  maggieWeakError("A test for maggieWeakError");
}
