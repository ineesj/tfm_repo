####
#### the messages sent to use the ETTS engine, via the topic "etts"
####
#### Apart from the info contained in this kind of message,
#### these ROS parameters, written by the skill, can be useful for information:
#### "etts_language" :  for example "es"
#### "etts_emotion" :   for example "happy"
#### "etts_primitive" : for example "google"
#### "etts_volume" : integer in [VOLUME_MIN, VOLUME_MAX]
#### "etts_queue_size" : for example "2"
#### "etts_speaking_now" : true or false, set at the beginning and end of each sentence
#### "etts_current_sentence": the sentence given to the low level primitive, for instance "Hola"
####
#### And the parameters written by the different APIs:
#### "etts_kidnapped" : for example "" (not kidnapped) or "node_name"


### useful for the stamp
Header header

### the real sentence to be said
# A simple example: text="Hello world, how are you?"
#
# -> multi-languages supported, example: text="es:hola|en:hi|en:Hello|fr:bonjour"
# -> snippets supported, example: text="Yes, \NLG=OK let us do it"
# -> It can also be extended with metadata tags, for instance:
#    text="\emotion=Happy I speak with an happy voice."
# -> if using NONVERBAL primitive, use one of the semantic keys of non_verbal.h:
#    "SINGING"|"CONFIRMATION"|"THINKING"|"WARNING"|"DIALOG"|"HELLO"|"ERROR"|"AMAZING"
# -> if using  MUSIC_SCORE primitive, use a music score (cf etts_music_score.h)
#    for example, text="BPM=85,A6,{},Ab2,C5,1/4-C3"
#    You can also use a filepath, it must end with ".score".
#    For example text="/tmp/mymusic.score".
#    This file must contain a valid music score, for instance "C4,D4,E4,D4,C4"
#
# Note that for NONVERBAL and MUSIC_SCORE,
#   the multi-language and the snippets are not used.
#   However, the language and the emotion fields of the message are applied as usual.
string text

### language domain
# "en" for english, "es" for spanish, etc.
# empty ("") for keeping the same language
string language

int16 PRIM_LAST_USED = 0
int16 PRIM_NOVOICE = 1
int16 PRIM_GOOGLE = 3
int16 PRIM_MICROSOFT = 5
int16 PRIM_NONVERBAL = 6
int16 PRIM_ESPEAK = 9


# the wanted low-level voice engine: one of the previous PRIM_*
# leave empty (PRIM_LAST_USED) for keeping the same primitive
int16 primitive

int16 EMOTION_LAST_USED = 0
int16 EMOTION_HAPPY = 1
int16 EMOTION_SAD = 2
int16 EMOTION_TRANQUILITY = 3
int16 EMOTION_NERVOUS = 4
### the wanted emotion, if supported by the primitive
# use one of the previous EMOTION_*
# leave empty (EMOTION_LAST_USED) for keeping the same emotion
int16 emotion

int16 VOLUME_LAST_USED = 0
int16 VOLUME_MIN = 1
int16 VOLUME_MAX = 100
### the desired volume output, if supported by the primitive
# volume must be between VOLUME_MIN and VOLUME_MAX
# leave empty (VOLUME_LAST_USED) for no change
int16 volume

int16 QUEUE_SENTENCE = 0
int16 SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE = 1
int16 SHUTUP_AND_SAY_SENTENCE = 2
int16 PAUSE = 3
int16 RESUME = 4
### by default, queue the sentence and say it when the previous ones have been said.
#  Use one of the previous orders to change this behaviour.
#  Note that if using the special instructions PAUSE and RESUME,
#  all the other fields of the message (including text) are discarded.
int16 priority
