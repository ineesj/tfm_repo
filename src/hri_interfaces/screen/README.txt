------------------------------------------------------------------------
- Description
------------------------------------------------------------------------
The "screen" node consists of a widget which can display different images.
It can also be configured as an input interface with different menus.
Each menu can have different buttons.


------------------------------------------------------------------------
- How to launch
------------------------------------------------------------------------
This node should be launched locally in the computer where the chest-screen 
and the projector are connected.    
    
There are two possible parameters that can be configured when this node is launched:

	$ param "full_screen" --> boolean; set to true for the widget to 
							  occupy the full screen. (default:= true)
	$ param "projector"   --> boolean; if true, a second widget is launched
							  in the projector, showing the same image
							  as in the robot's screen. (default:= false)
							  
For example:
	
	$ roslaunch screen screen.launch  
	
Will only launch one full-screen widget in the robot's chest screen.

	$ roslaunch screen screen.launch projector:=true
	
Will also launch another widget in the projector; also full-screen.


------------------------------------------------------------------------
- How to use the screen and projector as displays
------------------------------------------------------------------------
The screen node is subscribed to the topics: 

 - "screen/display_image"            --> for the chest screen
 - "projector/display_image"		 --> for the projector	

Both expect messages of type "std_msgs/String" with the name of the 
picture to be displayed. They work independently.

The files of the pictures which can be displayed are located in the folder:
'/hri_interfaces/screen/data/'

So, if we want the robot to show a picture (i.e. "ok.png") in the
chest screen we will publish:

    $ rostopic pub /mbotXX/screen/display_image std_msgs/String "ok.png" -1
    
The same picture, but in the projector, would be:

	$ rostopic pub /mbotXX/projector/display_image std_msgs/String "ok.png" -1
	
*Note: this also works without the extension of the image (only with its name)
    
There is also a default image with the logo of the MOnarCH project. 
If we want either the screen or the projector to "go back" to the default 
image, we can use the word "reset" in the message.

------------------------------------------------------------------------
- How to use the projector as a video player
------------------------------------------------------------------------

The screen node is subscribed to the topics: 
 - "projector/display_video"		 --> for the projector	

It expects a message of type "std_msgs/String" with the name of the 
video to play.

The video files that can be played are located in the folder:
'/hri_interfaces/screen/data/video'

So, if we want the robot to play a video (i.e. "BigBuckBunny_320x180.mp4") in the
projector screen we will publish:

    $ rostopic pub /mbotXX/projector/display_video std_msgs/String "BigBuckBunny_320x180.mp4" -1
	
*Note: you have to include the extension of the video file

**Note: The video formats supported by the player depend on the codecs installed in the system.
Tested video formats: avi, mpeg4, m4v, wmv, mov
    
If we want the projector to return to the default image, we can use the word "reset" in the message.	

------------------------------------------------------------------------
- How to hide or show the screen and projector as displays
------------------------------------------------------------------------
Both displays can be hidden and shown back again independently via topic:

	$ rostopic pub /mbotXX/screen/show_hide std_msgs/String "hide" -1
	$ rostopic pub /mbotXX/projector/show_hide std_msgs/String "hide" -1
	
The options are: "hide", "show" or "fullscreen"


------------------------------------------------------------------------
- How to use the screen as input interface
------------------------------------------------------------------------
Currently, there are two possible menus that can be used:
- yes_no_menu (2 buttons)
- storyboard_meny (3 buttons)
- projector_menu (4 buttons)
- location_menu	(5 buttons)
- euronews_menu (5 buttons)

In order to activate, for example, the location menu, just publish:

$ rostopic pub /mbot14/hri_interface_config monarch_msgs/KeyValuePairArray "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
array:
- key: 'show_screen_menu'
  value: 'location_menu'" -1
  
  
In order to disable it, substitute the value 'location_menu' by 'no_menu'
  
  
This package allows to create menus dynamically (from 1 to 5 buttons). In order to do it, you have to publish the topic enable_screen_input. Below you can see an example with 2 buttons: the first one contains text and the second one an image.

rostopic pub /mbotXX/enable_screen_input monarch_msgs/ScreenInterfaceInputConfig "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
enable_input: true
number_buttons: 2
buttons_config:
- button_name: 'text;Accept'
  topic_name: '/mbotXX/selection'
  msg_value: 'accepted'
- button_name: 'picture;cancel.png'
  topic_name: '/mbotXX/selection'
  msg_value: 'cancel'" -1

Another way for disabling the menu is through /mbotXX/enable_screen_input topic:

rostopic pub /mbotXX/enable_screen_input monarch_msgs/ScreenInterfaceInputConfig "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
enable_input: false" -1
