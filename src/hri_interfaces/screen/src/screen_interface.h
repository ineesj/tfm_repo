#ifndef SCREEN_INTERFACE_H
#define SCREEN_INTERFACE_H

#include <QWidget>
#include <QRect>
#include <QIcon>
#include <QMovie>
#include <QDesktopWidget>
#include <phonon>
#include <VideoPlayer>
#include <MediaObject>

//ROS
#include "ros/ros.h"
#include "std_msgs/String.h"

//Monarch
#include "monarch_msgs/ScreenButton.h"
#include "src/ui_screen_interface.h"


#define NO_IMAGE_REQUEST "reset"
#define NO_VIDEO_REQUEST "reset"
#define DEFAULT_IMAGE_NAME "monarch.jpg"

namespace Ui {
class ScreenInterface;
}

class ScreenInterface : public QWidget{
    Q_OBJECT

public:
    explicit ScreenInterface(ros::NodeHandle nh, std::string images_folder,
							 QRect default_geometry, QWidget *parent = 0);
    ~ScreenInterface();

    //Auxiliar functions to be called outside the widget
    void adjustWidgetGeometry(int widget);
    void adjustImageGeometryToParent(int margin);

public slots:
    void on_display_image_request_received(QString image_name);
    void on_display_video_request_received(QString video_name);
    void on_enable_input_interface_received(bool enable_input,
                                            int num_buttons,
                                            std::vector<monarch_msgs::ScreenButton> buttons_config);
    void on_show_hide_display_received(QString show_hide_display);

    void on_adjust_widget_geometry_received(int widget);




private slots:
    void on_button1_clicked();
    void on_button2_clicked();
    void on_button3_clicked();
    void on_button4_clicked();
    void on_button5_clicked();


private:
    Ui::ScreenInterface *ui;

    QString current_image_name_, images_folder_, icons_folder_, current_video_name_, videos_folder_;

    ros::NodeHandle nh_;
    ros::Publisher buttons_pubs_[5];
    std::string buttons_data_[5];
    int current_num_buttons_;

    //Auxiliar functions
    void setCurrentImage();
    void setGif();
    void disableInput(unsigned int num_buttons);
    void adjustButtonsGeometry(int num_buttons, int margin);
    void setButtonsInRows(int num_buttons, int margin);
    void configButton(QPushButton* b, int button_index,
                      monarch_msgs::ScreenButton button_config);
    void sendCorrespondingMessage(int index);

	Phonon::MediaObject * video;
    Phonon::VideoWidget *videoWidget;
	void setCurrentVideo();
	void stop_and_hide_video();
};

#endif // SCREEN_INTERFACE_H
