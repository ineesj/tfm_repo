#include "ros_subscriber_thread.h"

RosSubscriberThread::RosSubscriberThread(ros::NodeHandle nh, QObject *parent) :
    QThread(parent){

    image_touch_screen_sub_ =     nh.subscribe("screen/display_image", 1,
                                          &RosSubscriberThread::imageTouchScreenCallback, this);

    show_hide_touch_screen_sub_ = nh.subscribe("screen/show_hide", 1,
                                          &RosSubscriberThread::showHideTouchScreenCallback, this);

    input_interface_sub_ =        nh.subscribe("enable_screen_input", 1,
                                          &RosSubscriberThread::inputInterfaceCallback, this);

    image_projector_sub_ =        nh.subscribe("projector/display_image", 1,
                                          &RosSubscriberThread::imageProjectorCallback, this);

	video_projector_sub_ =        nh.subscribe("projector/display_video", 1,
                                          &RosSubscriberThread::videoProjectorCallback, this);

    show_hide_projector_sub_ =    nh.subscribe("projector/show_hide", 1,
                                          &RosSubscriberThread::showHideProjectorCallback, this);

    projector_control_sub_ =      nh.subscribe("cmd_videoProjector", 1,
                                          &RosSubscriberThread::projectorControlCallback, this);


}

void RosSubscriberThread::run(){
    ros::spin();
}

///////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
///////////////////////////////////////////////////////////////////////////////


void RosSubscriberThread::imageTouchScreenCallback(const std_msgs::String::ConstPtr& msg){
    ROS_INFO("Received msg to display image on screen\n");
    QString image_name = QString::fromUtf8(msg->data.c_str());
    emit display_image_touch_screen(image_name);
}

void RosSubscriberThread::showHideTouchScreenCallback(const std_msgs::String::ConstPtr& msg){
    ROS_INFO("Received msg to make the chest screen: [%s]\n", msg->data.c_str());
    QString show_hide = QString::fromUtf8(msg->data.c_str());
    emit show_hide_touch_screen(show_hide);
}

void RosSubscriberThread::inputInterfaceCallback(const monarch_msgs::ScreenInterfaceInputConfig::ConstPtr& msg){
    ROS_INFO("Received msg to enable/disable\n");
    emit enable_input_interface(msg->enable_input, msg->number_buttons, msg->buttons_config);
}

void RosSubscriberThread::imageProjectorCallback(const std_msgs::String::ConstPtr& msg){
    ROS_INFO("Received msg to display image on projector\n");
    QString image_name = QString::fromUtf8(msg->data.c_str());
    emit display_image_projector(image_name);
}

void RosSubscriberThread::videoProjectorCallback(const std_msgs::String::ConstPtr& msg){
    ROS_INFO("Received msg to play a video on projector\n");
    QString video_name = QString::fromUtf8(msg->data.c_str());
    emit display_video_projector(video_name);
}

void RosSubscriberThread::showHideProjectorCallback(const std_msgs::String::ConstPtr& msg){
    ROS_INFO("Received msg to make the projector: [%s]\n", msg->data.c_str());
    QString show_hide = QString::fromUtf8(msg->data.c_str());
    emit show_hide_projector(show_hide);
}

void RosSubscriberThread::projectorControlCallback(const monarch_msgs::VideoProjectorControl::ConstPtr &msg){
    if (msg->videoProjector == true){
        ROS_INFO("The projector has been turned on\n");
        sleep(5);
        emit adjust_widget_geometry(1);
        emit show_hide_projector(QString("fullscreen"));
    }
    else{
        ROS_INFO("The projector has been turned off\n");
        emit display_image_projector(QString("reset"));
        emit show_hide_projector(QString("hide"));
    }
}



