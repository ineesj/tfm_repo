#include "screen_interface.h"

ScreenInterface::ScreenInterface(ros::NodeHandle nh, std::string images_folder,
                              QRect default_geometry, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScreenInterface){
    ui->setupUi(this);

    nh_ = nh;

    //Changing geometry depending on parameter
	this->setGeometry(default_geometry);
    adjustImageGeometryToParent(40);

    this->setStyleSheet("background-color:white;");

    //Make the buttons transparent
//    ui->button1->setFlat(true);
//    ui->button2->setFlat(true);
//    ui->button3->setFlat(true);
//    ui->button4->setFlat(true);
//    ui->button5->setFlat(true);

	current_video_name_ = NO_VIDEO_REQUEST;
    current_image_name_ = DEFAULT_IMAGE_NAME;    
    images_folder_ = QString(images_folder.c_str());
    videos_folder_ = QString(images_folder.c_str())+"videos/";
    icons_folder_ = images_folder_+"icons/";

    setCurrentImage();
    
    this->video = Phonon::createPlayer(Phonon::VideoCategory);
    this->videoWidget = new Phonon::VideoWidget(this);
    this->stop_and_hide_video();
    Phonon::createPath(this->video, this->videoWidget);
    
    current_num_buttons_ = 0;
    //By default, the input interface is disabled
    disableInput(current_num_buttons_);

}


ScreenInterface::~ScreenInterface(){
    delete ui;
}


///////////////////////////////////////////////////////////////////////
///AUXILIAR FUNCTIONS
///////////////////////////////////////////////////////////////////////

void ScreenInterface::adjustWidgetGeometry(int widget){
    ROS_INFO("Adjusting geometry\n");
    QRect geom = QApplication::desktop()->screenGeometry(widget);
    this->move(QPoint(geom.x(), geom.y()));
    this->resize(geom.width(), geom.height());
    adjustImageGeometryToParent(40);
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::adjustImageGeometryToParent(int margin){
	
	//The image is a little smaller than the widget containing it
	//to have margins
	QRect widget_geom = this->geometry();
    ui->image->setGeometry(margin,
                           margin,
                           widget_geom.width()- 2*margin,
                           widget_geom.height()- 2*margin);
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::setCurrentImage(){
    QPixmap current_image(images_folder_+current_image_name_);
    ui->image->setPixmap(current_image.scaled(ui->image->size(), Qt::KeepAspectRatio));
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::setCurrentVideo(){
	ROS_INFO("Playing video %s%s", videos_folder_.toStdString().c_str(), current_video_name_.toStdString().c_str());
    this->videoWidget->setGeometry(ui->image->geometry());//The area to display a video is the same that we use to display an image
    this->videoWidget->show();
	this->video->setCurrentSource(Phonon::MediaSource(videos_folder_+current_video_name_));
    this->video->play();
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::stop_and_hide_video()
{
	this->video->stop();
	this->videoWidget->hide();
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::setGif(){
    QMovie *movie = new QMovie(images_folder_+current_image_name_);
    QPixmap current_image(images_folder_+current_image_name_);
    QSize gif_size = current_image.scaled(ui->image->size(), Qt::KeepAspectRatio).size();
    movie->setScaledSize(gif_size);
    ui->image->setMovie(movie);
    movie->start();
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::disableInput(unsigned int num_buttons){
    ui->button1->hide();
    ui->button2->hide();
    ui->button3->hide();
    ui->button4->hide();
    ui->button5->hide();

    for (unsigned int i=0; i<num_buttons; i++){
        buttons_pubs_[i].shutdown();
        buttons_data_[i] = "";
    }

    current_num_buttons_ = 0;

	this->stop_and_hide_video();
    ui->image->setStyleSheet("QLabel { background-color : white;}");
    ui->image->show();
    current_image_name_ = DEFAULT_IMAGE_NAME;
    setCurrentImage();
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::configButton(QPushButton *b, int button_index,
                                   monarch_msgs::ScreenButton button_config){

    QString button_name(button_config.button_name.c_str());
    QStringList name_options = button_name.split(";");

    if (name_options.first() == "text")  {
        b->setIcon(QIcon());
        b->setText(name_options.last());
    }
    else if(name_options.first() == "picture"){
        b->setText("");
        b->setIcon(QIcon(icons_folder_+name_options.last()));
        b->setIconSize(b->size());
        //Set a mask so the button only "paints" the part where the icon is
//        QPixmap b_pix = QPixmap(icons_folder_+name_options.last()).scaled(b->size(), Qt::KeepAspectRatio);
//        b->setIcon(QIcon(b_pix));
//        QRegion mask = b_pix.mask();
//        b->setMask(mask);
    }
    buttons_pubs_[button_index] = nh_.advertise<std_msgs::String>(button_config.topic_name,1);
    buttons_data_[button_index] = std::string(button_config.msg_value);

    b->show();

}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::sendCorrespondingMessage(int index){
    std_msgs::String msg;
    msg.data = buttons_data_[index];
    buttons_pubs_[index].publish(msg);

}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::adjustButtonsGeometry(int num_buttons, int margin){

    QRect widget_geom = this->geometry();

    if(num_buttons == 1 || num_buttons == 2 ||num_buttons == 3){
        setButtonsInRows(num_buttons, margin);
    }

    else if (num_buttons == 4){
        ui->button1->setGeometry(margin,
                                 margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/4 - 5/4*margin);

        ui->button2->setGeometry(margin,
                                 widget_geom.height()/4 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/4 - 5/4*margin);

        ui->button3->setGeometry(margin,
                                 widget_geom.height()*2/4 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/4 - 5/4*margin);

        ui->button4->setGeometry(margin,
                                 widget_geom.height()*3/4 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/4 - 5/4*margin);

    }

    else if (num_buttons == 5){
        ui->button1->setGeometry(margin,
                                 margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/5 - 6/5*margin);

        ui->button2->setGeometry(margin,
                                 widget_geom.height()/5 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/5 - 6/5*margin);

        ui->button3->setGeometry(margin,
                                 widget_geom.height()*2/5 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/5 - 6/5*margin);

        ui->button4->setGeometry(margin,
                                 widget_geom.height()*3/5 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/5 - 6/5*margin);

        ui->button5->setGeometry(margin,
                                 widget_geom.height()*4/5 + 0.5*margin,
                                 widget_geom.width() - 2*margin,
                                 widget_geom.height()/5 - 6/5*margin);

    }

}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::setButtonsInRows(int num_buttons, int margin){

    QRect widget_geom = this->geometry();

    int button_width = widget_geom.width() - 2*margin;
    int num_margins = num_buttons +1;

    int button_height = widget_geom.height()/num_buttons - num_margins/num_buttons*margin;

    ui->button1->setGeometry(margin,
                             margin,
                             button_width,
                             button_height);

    ui->button2->setGeometry(margin,
                             widget_geom.height()/num_buttons + 0.5*margin,
                             button_width,
                             button_height);

    ui->button3->setGeometry(margin,
                             widget_geom.height()*2/num_buttons + 0.5*margin,
                             button_width,
                             button_height);

    ui->button4->setGeometry(margin,
                             widget_geom.height()*3/num_buttons + 0.5*margin,
                             button_width,
                             button_height);

    ui->button5->setGeometry(margin,
                             widget_geom.height()*4/num_buttons + 0.5*margin,
                             button_width,
                             button_height);


}

///////////////////////////////////////////////////////////////////////
///SLOTS
///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_display_image_request_received(QString image_name){

    //This means that if there is a menu active, do not show any image
    if (current_num_buttons_ != 0)  return;

	this->stop_and_hide_video();
	
    if (image_name == NO_IMAGE_REQUEST){
        current_image_name_ = DEFAULT_IMAGE_NAME;
        setCurrentImage();
    }

    else if(image_name.endsWith(".gif")){
        current_image_name_ = image_name;
        setGif();
    }

    else{
        current_image_name_ = image_name;
        setCurrentImage();
    }
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_display_video_request_received(QString video_name){

    //This means that if there is a menu active, do not show any image
    if (current_num_buttons_ != 0)  return;

    if (video_name == NO_VIDEO_REQUEST){ // we can reset the display showing the default image
        this->on_display_image_request_received(NO_IMAGE_REQUEST);
    }    

    else{
        current_video_name_ = video_name;
        this->setCurrentVideo();
    }
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_show_hide_display_received(QString show_hide_display){

    if (show_hide_display == "hide"){
        this->hide();
    }
    else if (show_hide_display == "show"){
        this->show();
    }
    else if (show_hide_display == "fullscreen" || show_hide_display == "default"){
        this->showFullScreen();
    }
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_adjust_widget_geometry_received(int widget){
    adjustWidgetGeometry(widget);
}

///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_button1_clicked(){
    sendCorrespondingMessage(0);
}

void ScreenInterface::on_button2_clicked(){
    sendCorrespondingMessage(1);
}

void ScreenInterface::on_button3_clicked(){
    sendCorrespondingMessage(2);
}

void ScreenInterface::on_button4_clicked(){
    sendCorrespondingMessage(3);
}

void ScreenInterface::on_button5_clicked(){
    sendCorrespondingMessage(4);
}


///////////////////////////////////////////////////////////////////////

void ScreenInterface::on_enable_input_interface_received(bool enable_input,
                                        int num_buttons,
                                        std::vector<monarch_msgs::ScreenButton> buttons_config){

    disableInput(current_num_buttons_);

    if (enable_input) {

        current_num_buttons_ = num_buttons;

        //set background image to a plain color
        ui->image->setStyleSheet("QLabel { background-color : white;}");
        ui->image->setPixmap(QPixmap());

        switch (num_buttons){

        case 1:
            if (buttons_config.size() < 1)  return;
            adjustButtonsGeometry(1, 100);
            configButton(ui->button1, 0, buttons_config[0]);
            current_num_buttons_ = 1;
            break;

        case 2:
            if (buttons_config.size() < 2)  return;
            adjustButtonsGeometry(2, 150);
            configButton(ui->button1, 0, buttons_config[0]);
            configButton(ui->button2, 1, buttons_config[1]);
            current_num_buttons_ = 2;
            break;

        case 3:
            if (buttons_config.size() < 3)  return;
            adjustButtonsGeometry(3, 100);
            configButton(ui->button1, 0, buttons_config[0]);
            configButton(ui->button2, 1, buttons_config[1]);
            configButton(ui->button3, 2, buttons_config[2]);
            current_num_buttons_ = 3;
            break;

        case 4:
            if (buttons_config.size() < 4)  return;
            adjustButtonsGeometry(4, 50);
            configButton(ui->button1, 0, buttons_config[0]);
            configButton(ui->button2, 1, buttons_config[1]);
            configButton(ui->button3, 2, buttons_config[2]);
            configButton(ui->button4, 3, buttons_config[3]);
            current_num_buttons_ = 4;
            break;

        case 5:
            if (buttons_config.size() < 5)  return;
            adjustButtonsGeometry(5, 50);
            configButton(ui->button1, 0, buttons_config[0]);
            configButton(ui->button2, 1, buttons_config[1]);
            configButton(ui->button3, 2, buttons_config[2]);
            configButton(ui->button4, 3, buttons_config[3]);
            configButton(ui->button5, 4, buttons_config[4]);
            current_num_buttons_ = 5;
            break;
        }

    }

}





