//Qt
#include <QApplication>
#include <QObject>
#include <QPalette>

//ROS
#include "ros/package.h"


//Screen
#include "screen_interface.h"
#include "ros_subscriber_thread.h"



////////////////////////////////////////////////////////////////////////////////
///MAIN
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv){
	
	///Ros node initialization
    ros::init(argc, argv, "screen_ui");
    ros::NodeHandle nh_public;
    ros::NodeHandle nh_private("~");
    
    ///Retrieving parameters from ros private nodehandle 
    //Get the information about the folder for the images
    std::string images_folder = ros::package::getPath("screen") + "/data/";
    nh_private.param("images_folder", images_folder, images_folder);
    ROS_INFO("Param images_folder: [%s]\n", images_folder.c_str());

    //If we want the app to be full screen or not
    bool full_screen = true;
    nh_private.param("full_screen", full_screen, full_screen);

    //If we want a second widget for the projector (true) or not (false)
    bool projector = false;
    nh_private.param("projector", projector, projector);
    ROS_INFO("Param projector: [%s]", projector?"true":"false"); 
    
    ///Creating Qt application
    QApplication app(argc, argv);
    

    //The QWidget for the chest of the robot where the images are going to be displayed
    //~ ScreenInterface* chestScreen = new ScreenInterface(nh_public, images_folder, QRect(35,10,650,1100));
    QRect screen_geom = QApplication::desktop()->screenGeometry(0);
    ScreenInterface* chestScreen = new ScreenInterface(nh_public, images_folder, screen_geom);
    
    //A second thread is necessary for ros communication mechanisms since the thread
    //of the widget is blocked and the communication must be performed via "signals"
    //and "slots"
    RosSubscriberThread* msg_receiver = new RosSubscriberThread(nh_public);
    QObject::connect(msg_receiver, SIGNAL(display_image_touch_screen(QString)),
                     chestScreen, SLOT(on_display_image_request_received(QString)));

    QObject::connect(msg_receiver, SIGNAL(show_hide_touch_screen(QString)),
                     chestScreen, SLOT(on_show_hide_display_received(QString)));

    qRegisterMetaType<monarch_msgs::ScreenButton>("monarch_msgs::ScreenButton");
    qRegisterMetaType<std::vector<monarch_msgs::ScreenButton> >("std::vector<monarch_msgs::ScreenButton>");
    QObject::connect(msg_receiver, SIGNAL(enable_input_interface(bool, int, std::vector<monarch_msgs::ScreenButton>)),
                     chestScreen, SLOT(on_enable_input_interface_received(bool, int, std::vector<monarch_msgs::ScreenButton>)));
                     

    if (projector){
		//Second QWidget to be shown on the projector
        QRect projector_geom = QApplication::desktop()->screenGeometry(1);
        ScreenInterface* projector = new ScreenInterface(nh_public, images_folder, projector_geom);
        projector->adjustWidgetGeometry(1);

		//Connect the projector also to the slot for sending images
        QObject::connect(msg_receiver, SIGNAL(display_image_projector(QString)),
                         projector, SLOT(on_display_image_request_received(QString)));
        QObject::connect(msg_receiver, SIGNAL(display_video_projector(QString)),
                         projector, SLOT(on_display_video_request_received(QString)));                         
        QObject::connect(msg_receiver, SIGNAL(show_hide_projector(QString)),
                         projector, SLOT(on_show_hide_display_received(QString)));
        QObject::connect(msg_receiver, SIGNAL(adjust_widget_geometry(int)),
                         projector, SLOT(on_adjust_widget_geometry_received(int)));

        projector->showFullScreen();
	}
	
	
	if (full_screen){
        chestScreen->showFullScreen();
    }
    else{
        chestScreen->show();
    }
    

    msg_receiver->start();

    return app.exec();
}
