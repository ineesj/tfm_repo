#include "ros/ros.h"
#include "std_msgs/String.h"
#include "monarch_msgs/ScreenButton.h"
#include "monarch_msgs/ScreenInterfaceInputConfig.h"
#include "monarch_msgs/KeyValuePair.h"
#include "monarch_msgs/KeyValuePairArray.h"
#include "monarch_msgs/GestureExpression.h"

//Temporary include for iw3 demo
#include "geometry_msgs/Point.h"
#include "monarch_msgs/RecognizedSpeech.h"


/** This node acts as a translator between dialogs and the screen node
 * in order to configure and use the input interface. The screen node
 * has the possibility to set different configurable menus, but the specific
 * contents and behaviours of the menus have to be defined elsewhere.
 *
 * Here, we perform two functions:
 *  - Subscribe to the dialog, in the topic hri_interface_config, which
 *    specifies the menu to set. For each menu, its configuration is stored here.
 *
 *  - When a button of the menu is pressed, it sends a message of type
 *    std_msgs/String, so this node also translates it to a monarch_msgs/KeyValuePairArray
 *    with all the necessary information
 *
 *    ----------           -------------           ----------
 *    - Dialog -  ---1---  - Translator-  ---2---  - Screen -
 *    ----------           -------------           ----------
*/


// 1. Topics from dialog to config a menu and to receive the information when
// a button is touched
#define DIALOG_TOPIC_TO_CONFIG_MENU         "hri_interface_config"
#define DIALOG_TOPIC_WHEN_SCREEN_TOUCHED    "screen_touched"

ros::Subscriber dialog_sub_;
ros::Publisher dialog_pub_;


// 2. Internal topics with the screen
#define INTERNAL_TOPIC_TO_CONFIG_MENU       "enable_screen_input"
#define INTERNAL_TOPIC_WHEN_SCREEN_TOUCHED  "button_touched"

ros::Publisher input_iface_conf_pub_;
ros::Subscriber input_iface_sub_;


//Possible menus
#define NO_MENU         "no_menu"
#define LOCATION_MENU   "location_menu"
#define YES_NO_MENU     "yes_no_menu"
#define STORYBOARD_MENU "storyboard_menu"
#define PROJECTOR_MENU  "projector_menu"
#define EURONEWS_MENU   "euronews_menu"


//State variables
std::string current_menu_ = NO_MENU;        //In order to know which menu is active currently
std::string name_of_game_ = "";             //When a yes/no menu is used, this provides context


//For iw3 demo -- In order to override the dialog for testing
ros::Publisher navigation_pub_;
ros::Subscriber asr_sub_;
bool using_dialog_ = false;



///////////////////////////////////////////////////////////////////////////////
/// AUX FUNCTIONS
///////////////////////////////////////////////////////////////////////////////
void configInput(std::string menu_name){

    monarch_msgs::ScreenInterfaceInputConfig input_config_msg;
    monarch_msgs::ScreenButton aux;

    aux.topic_name = INTERNAL_TOPIC_WHEN_SCREEN_TOUCHED;

    if (menu_name == NO_MENU){
        input_config_msg.enable_input = false;
    }

    else if (menu_name == YES_NO_MENU){
        input_config_msg.enable_input = true;
        input_config_msg.number_buttons = 2;
        aux.button_name = "picture;yes_button";
        aux.msg_value = "yes";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;no_button";
        aux.msg_value = "no";
        input_config_msg.buttons_config.push_back(aux);
    }

    else if (menu_name == STORYBOARD_MENU){
        input_config_msg.enable_input = true;
        input_config_msg.number_buttons = 3;
        aux.button_name = "picture;classroom";
        aux.msg_value = "teaching_assistant";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;catch_and_touch";
        aux.msg_value = "joyful_warden";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;interactive_game";
        aux.msg_value = "interactive_game";
        input_config_msg.buttons_config.push_back(aux);
    }

    else if (menu_name == PROJECTOR_MENU){
        input_config_msg.enable_input = true;
        input_config_msg.number_buttons = 4;
        aux.button_name = "picture;tree";
        aux.msg_value = "tree.png";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;train";
        aux.msg_value = "train.png";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;dog";
        aux.msg_value = "dog.png";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "text;Stop Projecting";
        aux.msg_value = "stop_projecting";
        input_config_msg.buttons_config.push_back(aux);
    }



    else if (menu_name == LOCATION_MENU){
        input_config_msg.enable_input = true;
        input_config_msg.number_buttons = 5;
        aux.button_name = "picture;classroom";
        aux.msg_value = "classroom";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;playroom";
        aux.msg_value = "playroom";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;main_corridor";
        aux.msg_value = "corridor";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;dormitory";
        aux.msg_value = "dormitory";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;cantina";
        aux.msg_value = "cantina";
        input_config_msg.buttons_config.push_back(aux);

    }

    else if (menu_name == EURONEWS_MENU){
        input_config_msg.enable_input = true;
        input_config_msg.number_buttons = 5;
        aux.button_name = "picture;clip_euronews1";
        aux.msg_value = "euronews1.mp4;video";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;clip_euronews2";
        aux.msg_value = "euronews2.mp4;video";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;clip_euronews3";
        aux.msg_value = "euronews3.mp4;video";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "picture;clip_euronews4";
        aux.msg_value = "euronews4.mp4;video";
        input_config_msg.buttons_config.push_back(aux);
        aux.button_name = "text;Stop Projecting";
        aux.msg_value = "stop_projecting;video";
        input_config_msg.buttons_config.push_back(aux);

    }

    input_iface_conf_pub_.publish(input_config_msg);

}


///////////////////////////////////////////////////////////////////////////////

void sendRobotToPlace(std::string place){

    geometry_msgs::Point nav_msg;

    if (place == "classroom"){
        nav_msg.x = 6.75;
        nav_msg.y = -4.14;
        nav_msg.z = 0.64;
    }
    else if (place == "playroom"){
        nav_msg.x = 2.25;
        nav_msg.y = -2.24;
        nav_msg.z = 0.96;
    }
    else if (place == "corridor"){
        nav_msg.x = -1.8;
        nav_msg.y = 0.26;
        nav_msg.z = 0.7;
    }
    else if (place == "dormitory"){
        nav_msg.x = 4.25;
        nav_msg.y = 11.41;
        nav_msg.z = 0.69;
    }
    else if (place == "cantina"){
        nav_msg.x = 1.0;
        nav_msg.y = 5.96;
        nav_msg.z = 0.80;
    }

    navigation_pub_.publish(nav_msg);

}

///////////////////////////////////////////////////////////////////////////////

void relayScreenTouchedToDialog(std::string button_touched){

    monarch_msgs::KeyValuePairArray kvpa_msg;
    monarch_msgs::KeyValuePair aux;

	ROS_DEBUG("relayScreenTouchedToDialog: current_menu_=%s - name_of_game_=%s\n", current_menu_.c_str(), name_of_game_.c_str());

    if(current_menu_ == YES_NO_MENU && name_of_game_ != ""){
        aux.key = "command";
        aux.value = "want_play_game";
        kvpa_msg.array.push_back(aux);

        aux.key = "yes_no_answer";
        aux.value = button_touched;
        kvpa_msg.array.push_back(aux);

        aux.key = "name_of_game";
        aux.value = name_of_game_;
        kvpa_msg.array.push_back(aux);

        dialog_pub_.publish(kvpa_msg);
    }

    else if(current_menu_ == LOCATION_MENU){
        aux.key = "command";
        aux.value = "goto_place";
        kvpa_msg.array.push_back(aux);

        aux.key = "location";
        aux.value = button_touched;
        kvpa_msg.array.push_back(aux);

        dialog_pub_.publish(kvpa_msg);
    }

    else if(current_menu_ == STORYBOARD_MENU){
        aux.key = "command";
        aux.value = "select_storyboard";
        kvpa_msg.array.push_back(aux);

        aux.key = "storyboard";
        aux.value = button_touched;
        kvpa_msg.array.push_back(aux);

        dialog_pub_.publish(kvpa_msg);
    }

    else if(current_menu_ == PROJECTOR_MENU || current_menu_ == EURONEWS_MENU){

        if(button_touched == "stop_projecting"){
            aux.key = "command";
            aux.value = button_touched;
            kvpa_msg.array.push_back(aux);
        }

        else{
            aux.key = "command";
            aux.value = "select_media_content";
            kvpa_msg.array.push_back(aux);

            aux.key = "media_content";
            aux.value = button_touched;
            kvpa_msg.array.push_back(aux);
        }

        dialog_pub_.publish(kvpa_msg);
    }

}


///////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
///////////////////////////////////////////////////////////////////////////////
void dialogCallback(const monarch_msgs::KeyValuePairArray::ConstPtr& msg){

    using_dialog_ = false;
    name_of_game_ = "";

    for (unsigned int i = 0; i<msg->array.size(); i++){
        if (msg->array[i].key == "show_screen_menu"){
            configInput(msg->array[i].value);   
            current_menu_ = msg->array[i].value;
        }
        else if (msg->array[i].key == "using_dialog"){
            using_dialog_ = true;
        }

        else if (msg->array[i].key == "name_of_game"){
            name_of_game_ = msg->array[i].value;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
void inputIfaceCallback(const std_msgs::String::ConstPtr& msg){

    //Special case for iw4 to override the dialog with the location menu
    if ( (!using_dialog_) && (msg->data == "classroom" ||
                 msg->data == "playroom"  ||
                 msg->data == "corridor"  ||
                 msg->data == "dormitory" ||
                 msg->data == "cantina") )
    {
         sendRobotToPlace(msg->data);
    }
    else{
         relayScreenTouchedToDialog(msg->data);

    }

}

void asrCallback(const monarch_msgs::RecognizedSpeech::ConstPtr& msg){

    if (!using_dialog_){
        if (msg->semantic == "LOCATION-CLASSROOM")          sendRobotToPlace("classroom");
        else if (msg->semantic == "LOCATION-PLAYINGROOM")   sendRobotToPlace("playroom");
        else if (msg->semantic == "LOCATION-CORRIDOR")      sendRobotToPlace("corridor");
        else if (msg->semantic == "LOCATION-ROOM")          sendRobotToPlace("dormitory");
        else if (msg->semantic == "LOCATION-CANTINA")       sendRobotToPlace("cantina");
    }
}



///////////////////////////////////////////////////////////////////////////////
/// MAIN
///
int main(int argc, char **argv){

  ros::init(argc, argv, "ce_to_input_screen_translator");
  ros::NodeHandle nh_public;


  //Translation of the message sent from the dialog_manager with the desired menu
  //to the corresponding buttons with their text or pictures
  dialog_sub_ = nh_public.subscribe(DIALOG_TOPIC_TO_CONFIG_MENU, 0, dialogCallback);

  input_iface_conf_pub_ = nh_public.advertise<monarch_msgs::ScreenInterfaceInputConfig>
                                                        (INTERNAL_TOPIC_TO_CONFIG_MENU, 1);


  //Translation of the message received from the menu (which contains only the button
  // touched) to a more complex message with more information for the dialog
  input_iface_sub_ = nh_public.subscribe(INTERNAL_TOPIC_WHEN_SCREEN_TOUCHED, 0, inputIfaceCallback);

  dialog_pub_ = nh_public.advertise<monarch_msgs::KeyValuePairArray>(DIALOG_TOPIC_WHEN_SCREEN_TOUCHED,1);



  //For iw3 demo in order to show the usage of the input interface
  navigation_pub_ = nh_public.advertise<geometry_msgs::Point>("screen_send_nav",1);

  //For iw3 demo to use asr
  asr_sub_ = nh_public.subscribe("audio/speech_rec", 0, asrCallback);


  while (ros::ok()){
    ros::spin();
  }

  return 0;
}
