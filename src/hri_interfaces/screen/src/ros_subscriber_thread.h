#ifndef ROS_SUBSCRIBER_THREAD_H
#define ROS_SUBSCRIBER_THREAD_H

#include <QThread>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "monarch_msgs/ScreenButton.h"
#include "monarch_msgs/ScreenInterfaceInputConfig.h"
#include "monarch_msgs/VideoProjectorControl.h"


class RosSubscriberThread : public QThread{
    Q_OBJECT

public:
    explicit RosSubscriberThread(ros::NodeHandle nh, QObject *parent = 0);


signals:
    void display_image_touch_screen(QString image_name);
    void show_hide_touch_screen(QString show_hide);
    void enable_input_interface(bool enable_input, int number_buttons, std::vector<monarch_msgs::ScreenButton> buttons_config);

    void display_image_projector(QString image_name);
    void display_video_projector(QString video_name);
    void show_hide_projector(QString show_hide);
    void adjust_widget_geometry(int widget);

protected:
    virtual void run ();

private:
    ros::Subscriber image_touch_screen_sub_, show_hide_touch_screen_sub_, input_interface_sub_,
    image_projector_sub_, video_projector_sub_, show_hide_projector_sub_, projector_control_sub_;

    void imageTouchScreenCallback(const std_msgs::String::ConstPtr& msg);
    void showHideTouchScreenCallback(const std_msgs::String::ConstPtr& msg);
    void inputInterfaceCallback(const monarch_msgs::ScreenInterfaceInputConfig::ConstPtr& msg);

    void imageProjectorCallback(const std_msgs::String::ConstPtr& msg);
    void videoProjectorCallback(const std_msgs::String::ConstPtr& msg);
    void showHideProjectorCallback(const std_msgs::String::ConstPtr& msg);
    void projectorControlCallback(const monarch_msgs::VideoProjectorControl::ConstPtr& msg);

};
#endif // ROS_SUBSCRIBER_THREAD_H
