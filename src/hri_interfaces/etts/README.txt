------------------------------------------------------------------------
- Description
------------------------------------------------------------------------
The "etts" package is in charge of providing text to speech functionalities.
Three different ETTS  softwares have been integrated: Google, Microsoft, Espeak.
Microsoft TTS is the default TTS software.

------------------------------------------------------------------------
- How to test speaker
------------------------------------------------------------------------
	$>speaker-test
	
This command perform a white noise to test the speaker output.
After a few seconds making noises, stop the program using Ctrl+C.
If you do not listen the white noise, check the setup of the sound configuration.
	
------------------------------------------------------------------------
- How to test TTS
------------------------------------------------------------------------
Run in a terminal the etts_skill:
 
	$>roslaunch etts etts_test.launch robot:=mbotXX

In another terminal test the communication with the skill (api):
	$>roslanch etts etts_api_test.launch robot:=mbotX


In the last terminal (api), you can change the TTS engine (Google, Microsoft, ESpeak), the language, or just write down a text to convert in aloud voice.

You can add effects to the shyntesized voice using the script etts/scripts/voiceEffects.sh. This script makes a new output devices with several audio effects. You can switch between the different audio devices using Ubuntu sound interface or using the script etts/scripts/changeSoundCardDevice.bash included in this project.



