add_library(ad_voice_skill ad_voice_skill.cpp)
target_link_libraries(ad_voice_skill 
					ad_skill event mcp 
                                        robot_config args_parser non_verbal ${catkin_LIBRARIES})

add_executable(test_ad_voice_skill.exe
				test_ad_voice_skill.cpp foo_ad_voice_skill.h)
target_link_libraries(test_ad_voice_skill.exe ad_voice_skill)

add_executable(launcher_ad_voice_skill.exe
				launcher_ad_voice_skill.cpp foo_ad_voice_skill.h)
target_link_libraries(launcher_ad_voice_skill.exe ad_voice_skill)

install(TARGETS ad_voice_skill
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

