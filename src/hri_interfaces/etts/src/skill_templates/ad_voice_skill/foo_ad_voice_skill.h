/*!
  \file        foo_ad_voice_skill.h
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2012/6/29

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

\todo Description of the file

\section Parameters
  - \b "foo"
        [string] (default: "bar")
        Description of the parameter.

\section Subscriptions
  - \b "/foo"
        [xxx]
        Descrption of the subscription

\section Publications
  - \b "~foo"
        [xxx]
        Descrption of the publication

 */

#ifndef FOO_AD_VOICE_SKILL_H
#define FOO_AD_VOICE_SKILL_H

#include "skill_templates/ad_voice_skill/ad_voice_skill.h"

/*!
 \class FooAdVoiceSkill
 A foo class for illustrating the way an AdVoiceSkill works
*/
class FooAdVoiceSkill : public AdVoiceSkill {
public:
    //! ctor
    FooAdVoiceSkill(unsigned long time_cycle_us,
                    RobotConfig* robot_config) :
        AdVoiceSkill(time_cycle_us,
                     "FOO_AD_VOICE_SKILL_START",
                     "FOO_AD_VOICE_SKIL_STOP", robot_config) {
    }

    //! Function called when initiating
    void custom_launch() {
        maggieDebug2("custom_launch()");
    }

    //! Function called in the loop
    void proceso()  {
        maggieDebug2("proceso()");
    }

    //! Function called when stopping
    void custom_end()  {
        maggieDebug2("custom_end()");
    }
};

#endif // FOO_AD_VOICE_SKILL_H
