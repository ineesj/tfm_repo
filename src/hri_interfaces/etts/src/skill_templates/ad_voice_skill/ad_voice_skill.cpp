
#include "skill_templates/ad_voice_skill/ad_voice_skill.h"
#include "debug/debug.h"

////////////////////////////////////////////////////////////////////////////////

AdVoiceSkill::AdVoiceSkill(unsigned long time_cycle_us,
                 const std::string & signal_start,
                 const std::string & signal_stop,
                 RobotConfig* robot_config) :
AdSkill(time_cycle_us, signal_start, signal_stop, robot_config) { //
    maggieDebug2("AdVoiceSkill ctor(SIGNAL_START:'%s', SIGNAL_STOP:'%s')",
                 SIGNAL_START.c_str(), SIGNAL_STOP.c_str());
}

////////////////////////////////////////////////////////////////////////////////

AdVoiceSkill::~AdVoiceSkill() {
    maggieDebug2("AdVoiceSkill dtor");
}

////////////////////////////////////////////////////////////////////////////////

void AdVoiceSkill::apply_options_from_args_parser(ArgsParser & parser) {
    maggieDebug2("apply_options_from_args_parser()");
}

////////////////////////////////////////////////////////////////////////

