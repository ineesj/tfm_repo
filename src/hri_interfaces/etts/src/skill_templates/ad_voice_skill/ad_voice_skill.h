#ifndef AD_VOICE_SKILL_H
#define AD_VOICE_SKILL_H

/***************************************************************************//**
 * \class AdVoiceSkill
 *
 * \author Arnaud Ramey ( arnaud.ramey@m4x.org )
 *
 * \date 03/2011
 *******************************************************************************/

///// Maggie imports
#include "ad_skill/AdSkill.h"
#include "api/etts_api.h"
// std
#include <string>

class AdVoiceSkill : public AdSkill {
public:
    /** constructor */
    AdVoiceSkill(unsigned long time_cycle_us,
                 const std::string & signal_start,
                 const std::string & signal_stop,
                 RobotConfig* robot_config);
    /** destructor */
    ~AdVoiceSkill();

    /*! configure the voice thanks to the parameters obtained with the parser */
    void apply_options_from_args_parser(ArgsParser & parser);

    /*
     * things to implement - inherited from Etts / Etts
     */
    virtual void custom_launch() = 0;
    virtual void proceso() = 0;
    virtual void custom_end() = 0;


    ///////// static functions for tests

    /*!
     Creates a test for a AdVoiceSkill
     \param argc the argc from command line args
     \param argv the argv from command line args
     \param time_cycle_us the periodic time on the loop
    */
    template<class _AdVoiceSkillType>
    static void test_simple(int argc, char** argv,
                            unsigned long time_cycle_us);

    /*!
     Creates a launcher for a AdVoiceSkill
     \param argc the argc from command line args
     \param argv the argv from command line args
     \param time_cycle_us the periodic time on the loop (units Nano Seconds)
    */
    template<class _AdVoiceSkillType>
    static void launcher_simple(int argc, char** argv,
                                unsigned long time_cycle_us);

    /*! cf AdSkill::test_prepair_parser() */
    inline static ArgsParser test_prepair_parser(int argc, char** argv);

    /*! cf AdSkill::test_create_skill_after_parsing() */
    template<class _AdVoiceSkillType>
    static _AdVoiceSkillType* test_create_skill_after_parsing(ArgsParser & parser,
                                                              unsigned long time_cycle_us);

protected:
    /*! Para poder hablar - it should be protected but we wanna configure it
      from command line */
    EttsApi etts;
};

////////////////////////////////////////////////////////////////////////////////
// template implementations
////////////////////////////////////////////////////////////////////////////////

inline ArgsParser AdVoiceSkill::test_prepair_parser(int argc, char** argv) {
    maggieDebug2("AdVoiceSkill::test_prepair_parser()");
    ArgsParser parser = AdSkill::test_prepair_parser(argc, argv);
    //EttsApi::put_options_in_args_parser(parser);
    return parser;
}

////////////////////////////////////////////////////////////////////////////////

template<class _AdVoiceSkillType>
_AdVoiceSkillType* AdVoiceSkill::test_create_skill_after_parsing(ArgsParser & parser,
                                                                 unsigned long time_cycle_us) {
    _AdVoiceSkillType* skill = AdSkill::test_create_skill_after_parsing
        <_AdVoiceSkillType>(parser, time_cycle_us);
    skill->apply_options_from_args_parser(parser);
    return skill;
}

////////////////////////////////////////////////////////////////////////////////

template<class _AdVoiceSkillType>
void AdVoiceSkill::test_simple(int argc, char** argv,
                               unsigned long time_cycle_us) {
    maggieDebug2("AdVoiceSkill::test_simple()");

    ArgsParser parser = AdVoiceSkill::test_prepair_parser(argc, argv);
    parser.parse(); // parse the command line args
    _AdVoiceSkillType* skill = test_create_skill_after_parsing
        <_AdVoiceSkillType>(parser, time_cycle_us);

    test_send_events(skill); // launch the events
    delete skill;
}

////////////////////////////////////////////////////////////////////////////////

template<class _AdVoiceSkillType>
void AdVoiceSkill::launcher_simple(int argc, char** argv,
                                   unsigned long time_cycle_us) {
    maggieDebug2("AdVoiceSkill::launcher_simple()");

    ArgsParser parser = AdVoiceSkill::test_prepair_parser(argc, argv);
    test_add_args_to_parser_for_launcher(parser);
    parser.parse(); // parse the command line args
    _AdVoiceSkillType* skill = test_create_skill_after_parsing
        <_AdVoiceSkillType>(parser, time_cycle_us);

    launcher_loop(parser, skill);
}
////////////////////////////////////////////////////////////////////////////////


#endif

