#include "skill/etts_skill.h"

int main(int argc, char ** argv) {
  ros::init(argc, argv, "etts_skill");
  ros::AsyncSpinner spinner(0);
  spinner.start();
  EttsSkill skill;
  while(ros::ok()) {
    sleep(1);
  }
  return 0;
}
