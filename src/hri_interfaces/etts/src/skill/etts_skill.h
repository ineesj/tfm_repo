/*!
  @autor Fernando Alonso Martin
  Skill: Emotional Text To Speech
  *
  * This skill enables to generate voice on real time using several TTS systems
  *

\section ROS params (if AD_USE_ROS activated):
  - \b "etts_language"
        [string] (default: "")
        The startup language at startup.
        For instance, "en", "fr"

  - \b "etts_emotion"
        [string] (default: "")
        The startup emotion at startup.
        Possible values: "happy", "sad", "tranquility", "nervous"

  - \b "etts_primitive"
        [string] (default: "")
        The startup primitive at startup.
        Possible values: "google", "novoice", "loquendo",
                         "microsoft", "festival", "pico","espeak"

\section Subscriptions
  - \b "etts"
        [etts_msgs::Utterance]
        The queue of utterances

\section Publications
  - \b "SPEAKING_START"
        [std_msgs::Int16]
        Published when the skill starts to say an utterance.

  - \b "SPEAKING_END"
        [std_msgs::Int16]
        Published when the skill ends an utterance.
*/

#ifndef ETTS_SKILL_H_
#define ETTS_SKILL_H_

// #define DEBUG // comment to remove all the printf()

// maggie
#include "utils/speech_snippets/speech_snippets.h"

//#include "etts/etts_options.h"

// google
#include "primitives/google/etts_google.h"

// novoice
#include "primitives/novoice/etts_novoice.h"

//nonverbal (robotic sounds)
#include "primitives/nonverbal/non_verbal.h"

// microsoft
#include "primitives/microsoft/etts_microsoft.h"

// espeak
#include "primitives/espeak/etts_espeak.cpp"

#include "etts_msgs/Utterance.h"


static const std::string PATH = ros::package::getPath("etts");


/*! En esta clase se definen las funciones comunes a todas las primitivas de voz (motores de síntesis) */
class EttsPrimitiveInterface;

class EttsSkill {
public:
  /*! Constructor de  la habilidad alternativo, que recibe por argumento la coniguracion
     * por defecto del robot, que nos sirve para saber que robot se esta usando y por lo tanto
     * fijar el tono de voz a ese determinado robot (para distinguirlo de otros) */
  EttsSkill();
  /*! dtor */
  ~EttsSkill();

  //! \return the current language ID
  inline Translator::LanguageId get_current_language() const
  { return _current_language; }
  inline Translator::CountryDomain get_current_language_domain() const
  { return _current_language_domain; }
  //! \return the current emotion ID
  inline utterance_utils::Emotion get_current_emotion() const
  { return _current_emotion; }
  //! \return the current primitive ID
  inline utterance_utils::PrimitiveId get_current_primitive() const
  { return _current_primitive; }
  //! \return Recent audo volume, between VOLUME_MIN and VOLUME_MAX
  inline utterance_utils::Volume get_current_volume() const
  { return _current_volume; }
  //! \return the total number of sentences said since constructor
  inline unsigned int get_nb_sentences_said() const
  { return _nb_sentences_said; }
  inline std::string get_last_sentence_said() const
  { return _last_sentence_said; }

private:

  //! the function that will really pronounciate an utterance
  void utterance_cb(const etts_msgs::Utterance & utterance);

  pthread_t _id_thread;;
  static void* reading_thread(void * this_ptr);
  void process_utterance(const etts_msgs::Utterance & utterance);

  void setVolume(utterance_utils::Volume new_v);

  //! read the language, emotion, primitive, volume params from the ROS param sever
  void read_params();

  //! write the different fields to ROS param server
  void write_param_language() {
    debugPrintf("write_param_language('%s')\n", _current_language_domain.c_str());
    _nh_public.setParam("etts_language", _current_language_domain);
  }
  void write_param_emotion() {
    debugPrintf("write_param_emotion(%i)\n", _current_emotion);
    _nh_public.setParam("etts_emotion",
                        utterance_utils::emotion_to_full_string(_current_emotion));
  }
  void write_param_primitive() {
    debugPrintf("write_param_primitive(%i)\n", _current_primitive);
    _nh_public.setParam("etts_primitive",
                        utterance_utils::primitive_to_full_string(_current_primitive));
  }
  void write_param_queue_size() {
    debugPrintf("write_param_queue_size()\n");
    _nh_public.setParam("etts_queue_size", (int) _utterance_queue.size());
  }
  void write_param_volume() {
    debugPrintf("write_param_volume(%i)\n", _current_volume);
    _nh_public.setParam("etts_volume", _current_volume);
  }
  /*! Actualiza la cola de locuciones pendientes de sintetizar, que se mantiene en memoria a corto plazo */
  void write_params();

  /*! Establece el motor de sintesis que se usa por defecto: por ej. Loquendo, Festival, Google, NoVoice, etc */
  void setPreferedPrimitive(const utterance_utils::PrimitiveId primitive_id);





  void reset_etts_sub() {
    _etts_sub = _nh_public.subscribe("etts", 10, &EttsSkill::utterance_cb, this);
  }

  /*! To date, there is only one possible metadata tag:
  syntax: \emotion=HAPPY || SAD || TRANQUILITY || NERVOUS
    */
  void apply_metadata_tags(const std::string & sentence);

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

  //! the current language ID
  Translator::LanguageId _current_language;
  Translator::CountryDomain _current_language_domain;

  //! the current emotion ID
  utterance_utils::Emotion _current_emotion;

  //! the current primitive ID
  utterance_utils::PrimitiveId _current_primitive;

  //! Recent audo volume, between VOLUME_MIN and VOLUME_MAX
  utterance_utils::Volume _current_volume;

  //! google primitive
  EttsGoogle _etts_google;
  //! novoice primitive
  EttsNovoice _etts_novoice;
  //! nonverbal (robotic sounds) primitive
  ad::tts::NonVerbal _etts_non_verbal;
  //! microsoft primitive
  EttsMicrosoft _etts_microsoft;
  //! espeak primitive
  EttsEspeak _etts_espeak;


  std::string _last_sentence_said;
  unsigned int _nb_sentences_said;

  //! a pointer to the currently used ETTS primitive
  EttsPrimitiveInterface* _current_primitive_ptr;

  //! for the snippets
  Translator::LanguageMap _languages_map;

  std::deque<etts_msgs::Utterance> _utterance_queue;

  ros::NodeHandle _nh_public, _nh_private;
  ros::Subscriber _etts_sub;
  ros::Publisher _speaking_start_pub, _speaking_stop_pub;
};

#endif
