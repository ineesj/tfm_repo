/** Lee una line por teclado y la sintetiza
 */
 
#include "skill/etts_skill.h"
#include "long_term_memory/ltm_path.h"


/** Manejador del evento que se lanza cuando se acaba de decir una locucion*/
void end_of_sentence_static(void*, int p) {
    maggiePrint("\n\nLa locucion %d a acabado de decirse\n", p);
}

/** Esto es una prueba para probar la habilidad de etts, usando para ello
 * en lugar de la habilidad directamene usamos el llamado WrapperSkill o también se puede ver como el API de la voz
 *
 *NECESITA: NO LANZAR EL CONTENEDOR DE MAIN CONTROL, "SOLO" LANZAR SERVIDOR DE EVENTOS Y SERVIDOR DE MEMORIA COMPARITDA
 * */

void test_interactive(int argc, char ** argv) {
    // create an instance of EttsSkill
    // get the robot config from command line
    RobotConfig robot_config = RobotConfig::from_command_line_args(argc, argv);
    EttsSkill skill (50 * 1000, &robot_config);

    CEventManager event_manager;
    event_manager.subscribe("SPEAKING_END", end_of_sentence_static, NULL);

    //frases por defecto para probar la voz
    std::string fraseFeliz =
            "\\item=Ollah Hola soy el robot personal y asistencial de la universidad carlos tercero de madrid, \\item=Cough  me gustaria que jugaras conmigo,  animate!!  ademas me han dejado mucho mas delgadita y guapa \\item=Whistle_01   Un saludo y gracias!  \\item=Smack";
    std::string fraseTriste =
            "\\item=Breath Hola soy un robot que se encuentra muy triste \\item=Breath_02 La verdad es que te echo mucho de menos!   No se que va a ser de mi vida y  si me van a sustituir por otro robot mas joven  \\item=Breath_06  ";
    std::string fraseTranquila =
            "\\item=Yawn Hola me acabo de despertar y estoy todavia un poco adormilada, \\item=Yawn_01 te voy a dejar que me voy a dar un paseo; lo siento!";
    std::string fraseNerviosa =
            "Estoy un poco nervisa, por eso hablo tan rapido, y es porque creo que me estan persiguiendo, socorro!!";

    bool need_exit = false;
    do {

        maggiePrint("\nIntroduzca una frase por teclado "
                    "(\\fraseFeliz \\fraseTriste \\fraseTranquila \\fraseNerviosa"
                    "\\fin \\callar):");
        char input_line_char[300];
        std::cin.getline(input_line_char, 300);
        std::string input_line(input_line_char);

        utterance_utils::Emotion user_emotion = etts_msgs::Utterance::EMOTION_HAPPY;
        std::string user_text = "foo";

        if (input_line == "\\fin") {
            need_exit = true;
            continue;
        }
        if (input_line == "\\callar") {
            event_manager.emit("ETTS_SHUT_UP");
            continue;
        }

        if (input_line == "\\fraseFeliz") {
            user_text = fraseFeliz;
            user_emotion = etts_msgs::Utterance::EMOTION_HAPPY;
        }
        else if (input_line == "\\fraseTriste") {
            user_text = fraseTriste;
            user_emotion = etts_msgs::Utterance::EMOTION_SAD;
        }
        else if (input_line == "\\fraseTranquila") {
            user_text = fraseTranquila;
            user_emotion = etts_msgs::Utterance::EMOTION_TRANQUILITY;
        }
        else if (input_line == "\\fraseNerviosa") {
            user_text = fraseNerviosa;
            user_emotion = etts_msgs::Utterance::EMOTION_NERVOUS;
        }
        else if (input_line_char[0] != '\0') {
            maggiePrint("Ready to say '%s'. write emotion "
                        "(happy,sad,nervous,tranquility):",
                        user_text.c_str());
            user_text = input_line;
            char emotion_cstr[50];
            std::cin.getline(emotion_cstr, 50);
            std::string emotion_str(emotion_str);
            if (emotion_str == "happy") {
                user_emotion = etts_msgs::Utterance::EMOTION_HAPPY;
            } else if (emotion_str == "sad") {
                user_emotion = etts_msgs::Utterance::EMOTION_SAD;
            } else if (emotion_str == "tranquility") {
                user_emotion = etts_msgs::Utterance::EMOTION_TRANQUILITY;
            } else if (emotion_str == "nervous") {
                user_emotion = etts_msgs::Utterance::EMOTION_NERVOUS;
            }
        }
        else{
            maggiePrint("Escribe algo, sino escribe \\fin ");
            continue;
        }

        // create the wanted utterance
        utterance_utils sentence(1,
                                 ROBOT_MAGGIE,
                                 Translator::LANGUAGE_SPANISH,
                                 user_emotion,
                                 false,
                                 user_text
                                 );
        // say the wanted sentence
        skill.add_utterance_to_queue(sentence);
    } while (!need_exit);


    maggiePrint("\n\nPresiona INTRO para finalizar la prueba\n");
    std::cin.get();
}

void test_skill(int argc, char ** argv) {
    AdSkill::test_simple<EttsSkill>(argc, argv, 500 * 1000);
}


int main(int argc, char ** argv) {
    //test_interactive(argc, argv);
    test_skill(argc, argv);

    return 0;
}
