#include "skill/etts_skill.h"
#include "definitions/utterance_params.h"
#include <ros/package.h>
//#include "definitions/maggieoptions.h" // for AD_USE_ROS
#define DEBUG
#include "debug/debug2.h"
// ROS
#include <std_msgs/Int16.h>


////////////////////////////////////////////////////////////////////////////////

EttsSkill::EttsSkill() : _nh_private("~") {
  // build the map
  Translator::build_languages_map(_languages_map);

  // default values
  _current_language = utterance_utils::DEFAULT_LANGUAGE;
  Translator::get_country_domain_from_language_id
      (_languages_map, _current_language, _current_language_domain);
  _current_emotion = utterance_utils::DEFAULT_EMOTION;
  setPreferedPrimitive(utterance_utils::DEFAULT_PRIMITIVE);
  setVolume(utterance_utils::DEFAULT_VOLUME);

  // check the parameters and try to apply them
  read_params();

  // share the definitive parameters
  write_params();
  _nh_private.setParam("etts_kidnapped", "");
  _nh_private.setParam("etts_speaking_now", false);

  // init all the primitives
  _nb_sentences_said = 0;
  _etts_google.init();
  _etts_novoice.init();
  _etts_non_verbal.init();
  _etts_microsoft.init();
  _etts_espeak.init();

  // change instrument here if needed
 




  // create publisers and subscribers
  reset_etts_sub();
  _speaking_start_pub = _nh_public.advertise<std_msgs::Int16>("SPEAKING_START", 1);
  _speaking_stop_pub = _nh_public.advertise<std_msgs::Int16>("SPEAKING_END", 1);

  maggiePrint("EttsSkill ctor: getting utterances on '%s', language:%s, emotion:%s, primitive '%s'",
              _etts_sub.getTopic().c_str(),
              Translator::get_language_full_name(_current_language).c_str(),
              utterance_utils::emotion_to_full_string(_current_emotion).c_str(),
              utterance_utils::primitive_to_full_string(_current_primitive).c_str());

  if (pthread_create(&_id_thread, NULL,
                     &EttsSkill::reading_thread, (void*) this)) {
    ROS_FATAL("EttsSkill: return code from pthread_create() is not zero\n");
    ros::shutdown();
  }
  debugPrintf("end of EttsSkill ctor\n");
}

////////////////////////////////////////////////////////////////////////////////

EttsSkill::~EttsSkill() {
  debugPrintf("dtor\n");
  pthread_cancel(_id_thread);
  _current_primitive_ptr->stop();
}

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::utterance_cb(const etts_msgs::Utterance &msg) {
  printf("utterance_cb('%s')\n",
         utterance_utils::utterance2string(msg).c_str());

  // check priority
  if (msg.priority == etts_msgs::Utterance::SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE) {
    maggieDebug1("CALLAR INMEDIATAMENTE");
    _current_primitive_ptr->shutUp();
    reset_etts_sub();
  }
  else if (msg.priority == etts_msgs::Utterance::SHUTUP_AND_SAY_SENTENCE) {
    maggieDebug1("CALLAR PERO NO INMEDIATAMENTE");
    reset_etts_sub();
  }
  else if (msg.priority == etts_msgs::Utterance::PAUSE) { //pausar la voz
    maggieDebug1("PAUSAR LA VOZ");
    _current_primitive_ptr->pauseSpeech();
    return;
  }
  else if (msg.priority == etts_msgs::Utterance::RESUME) { //seguir hablando
    maggieDebug1("RESUME SPEAK");
    _current_primitive_ptr->resumeSpeech();
    return;
  }
  _utterance_queue.push_back(msg);
  write_param_queue_size();
} // end utterance_cb()

////////////////////////////////////////////////////////////////////////////////

void* EttsSkill::reading_thread(void * this_ptr) {
  EttsSkill* thisp = (EttsSkill*) this_ptr;
  while (ros::ok()) {
    while(!thisp->_utterance_queue.empty()) {
      etts_msgs::Utterance utt = thisp->_utterance_queue.front();
      thisp->process_utterance(utt);
      thisp->_utterance_queue.pop_front();
      thisp->write_param_queue_size();
    }
    usleep(100 * 1000);
  }
  pthread_exit(NULL);
} // end reading_thread()

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::process_utterance(const etts_msgs::Utterance &utt) {
  printf("process_utterance('%s')\n",
         utterance_utils::utterance2string(utt).c_str());

  std::string cleaned_sentence = utt.text;

  // change the language if needed
  // this needs to be done before removing the natural language tags
  Translator::LanguageId utt_lang;
  if (!utt.language.empty()
      && Translator::get_language_id_from_country_domain(_languages_map, utt.language, utt_lang)
      && _current_language != utt_lang) {
    _current_language = utt_lang;
    _current_language_domain = utt.language;
    write_param_language();
  }

  // change the emotion if needed
  if (utt.emotion != etts_msgs::Utterance::EMOTION_LAST_USED
      && _current_emotion != utt.emotion) {
    _current_emotion = utt.emotion;
    write_param_emotion();
  }

  //La elección del motor de síntesis lo hacemos por cada frase en
  // concreto (ya que una puede querer festival, otra loquendo,
  // otra no verbal)
  // change the engine if needed - must be done before cleaning loquendo tags
  //debugPrintf("_current_primitive:%i, utt.primitive:%i\n", _current_primitive, utt.primitive);
  utterance_utils::PrimitiveId previous_primitive = _current_primitive;
  if (utt.primitive != etts_msgs::Utterance::PRIM_LAST_USED
      && _current_primitive != utt.primitive) {
    setPreferedPrimitive(utt.primitive);
    write_param_primitive();
  }
  bool is_primitive_verbal = (utt.primitive != etts_msgs::Utterance::PRIM_NONVERBAL);

  // check volume
  if (utt.volume != etts_msgs::Utterance::VOLUME_LAST_USED
      && _current_volume != utt.volume) {
    setVolume(utt.volume);
    write_param_volume();
  }

  // check if there is a real sentence to say
  if (cleaned_sentence.empty()) {
    debugPrintf("process_utterance('%s'): empty string, nothing to say.\n",
                utterance_utils::utterance2string(utt).c_str());
    return;
  }

  // generate the natural language if needed
  if (is_primitive_verbal)
    utterance_utils::replace_natural_langugage_tags
        (cleaned_sentence, _current_language, _languages_map);

  // use the tags of the sentence
  if (is_primitive_verbal)
    apply_metadata_tags(cleaned_sentence);

  // extract the wanted language
  // has to be done before strip_metadata_tags()
  if (is_primitive_verbal
      && !Translator::_build_given_language_in_multilanguage_line
      (cleaned_sentence, _current_language, _languages_map, cleaned_sentence)) {
    // the sentence does not respect the sayTextNL(), but go on.
    //return;
  }

   utterance_utils::strip_metadata_tags(cleaned_sentence);
  

  // the cleaned sentence might be empty (for instance, "\item=xxx",
  // or an empty sentence to change language...)
  // in that case, skip it
  if (cleaned_sentence.empty()) {
    debugPrintf("The sentence is empty after removing Loquendo tags, skipping it.\n");
    // the lock will be removed after the return
    return;
  }

  debugPrintf("3. escribimos en memoria que estamos hablando\n");
  _nh_private.setParam("etts_speaking_now", true);

  // write the current sentence (without tags) to MCP
  _nh_public.setParam("etts_current_sentence", cleaned_sentence);

  debugPrintf("4. emitimos el evento de que empezamos a hablar y el identinficiador de la locucion\n");
  std_msgs::Int16 msg;
  msg.data = 0;
  _speaking_start_pub.publish(msg);

  debugPrintf("5. usamos las primitivas de voz para sintetizar la voz\n");
  debugPrintf("Ready to say '%s'\n", cleaned_sentence.c_str());
  // TODO current_robot_id value ?
  RobotId current_robot_id = ROBOT_MAGGIE;
  _current_primitive_ptr->generateSpeech(cleaned_sentence,
                                         current_robot_id,
                                         _current_language,
                                         _current_emotion);

  // if it was non verbal, restore the primitive
  if (!is_primitive_verbal) {
    debugPrintf("This sentence was non verbal, restoring the previous primitive\n");
    setPreferedPrimitive(previous_primitive);
    write_param_primitive();
  }

  debugPrintf("6. escribimos en memoria compartida que ya no estamos hablando\n");
  _nh_private.setParam("etts_speaking_now", false);

  debugPrintf("7. emitimos el evento de que acabamos de hablar y el identificador de la locucion\n");
  _speaking_stop_pub.publish(msg);
  _last_sentence_said = cleaned_sentence;
  ++_nb_sentences_said;
} // end process_utterance()

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::read_params() {
  debugPrintf("read_params()\n");
  // language
  std::string etts_domain = "";
  _nh_public.param("etts_language", etts_domain, etts_domain);
  if (etts_domain.size() > 0) {
    Translator::LanguageId lang;
    if (Translator::get_language_id_from_country_domain(_languages_map, etts_domain, lang)) {
      _current_language = lang;
      _current_language_domain = etts_domain;
    }
  }
  // emotion
  std::string etts_emotion = "";
  _nh_public.param("etts_emotion", etts_emotion, etts_emotion);
  if (etts_emotion.size() > 0) {
    utterance_utils::Emotion e;
    if (utterance_utils::string_to_emotion(etts_emotion, e))
      _current_emotion = e;
  }
  // primitive
  std::string etts_primitive = "";
  _nh_public.param("etts_primitive", etts_primitive, etts_primitive);
  if (etts_primitive.size() > 0) {
    utterance_utils::PrimitiveId p;
    if (utterance_utils::string_to_primitive(etts_primitive, p)) {
      _current_primitive = p;
      setPreferedPrimitive(_current_primitive);
    }
  }
  // volume
  utterance_utils::Volume etts_volume = -1;
  _nh_public.param("etts_volume", etts_volume, etts_volume);
  //printf("etts_volume:%i\n", etts_volume);
  if (etts_volume < 0) { // try with string version
    std::string etts_volume_str = "";
    _nh_public.param("etts_volume", etts_volume_str, etts_volume_str);
    debugPrintf("etts_volume_str:'%s'\n", etts_volume_str.c_str());
    if (etts_volume_str.size() > 0)
      etts_volume = StringUtils::cast_from_string
          <utterance_utils::Volume>(etts_volume_str);
  }
  if (etts_volume > 0)
    setVolume(etts_volume);
} // end read_params()

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::write_params() {
  debugPrintf("write_params()\n");
  write_param_language();
  write_param_emotion();
  write_param_primitive();
  write_param_queue_size();
  write_param_volume();
}

//////////////////////////////////////////////////////////////////////////////////

void EttsSkill::setPreferedPrimitive(const utterance_utils::PrimitiveId primitive_id) {
  debugPrintf("setPreferedPrimitive(%s)\n",
              utterance_utils::primitive_to_full_string(primitive_id).c_str());




  if (primitive_id == etts_msgs::Utterance::PRIM_NOVOICE) {
    // this is always OK
    _current_primitive = etts_msgs::Utterance::PRIM_NOVOICE;
    _current_primitive_ptr = &_etts_novoice;
  }

  else if (primitive_id == etts_msgs::Utterance::PRIM_GOOGLE) {
    // this is always OK
    _current_primitive = etts_msgs::Utterance::PRIM_GOOGLE;
    _current_primitive_ptr = &_etts_google;
  }

  else if (primitive_id == etts_msgs::Utterance::PRIM_NONVERBAL) {
    // this is always OK
    _current_primitive = etts_msgs::Utterance::PRIM_NONVERBAL;
    _current_primitive_ptr = &_etts_non_verbal;
  }


  else if (primitive_id == etts_msgs::Utterance::PRIM_MICROSOFT) {
    // this is always OK
    _current_primitive = etts_msgs::Utterance::PRIM_MICROSOFT;
    _current_primitive_ptr = &_etts_microsoft;
  }

  else if (primitive_id == etts_msgs::Utterance::PRIM_ESPEAK) {
    // this is always OK
    _current_primitive = etts_msgs::Utterance::PRIM_ESPEAK;
    _current_primitive_ptr = &_etts_espeak;
  }


  else
    debugPrintf("setPreferedPrimitive(): unkown primitive %i\n", primitive_id);
}

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::setVolume(utterance_utils::Volume new_v){
  debugPrintf("setVolume(%i)\n", new_v);
  if (new_v > 100)
    _current_volume = 100;
  else if (new_v < 0)
    _current_volume = 0;
  else
    _current_volume = new_v;
  _current_primitive_ptr->setVolume(_current_volume);
}

////////////////////////////////////////////////////////////////////////////////

void EttsSkill::apply_metadata_tags(const std::string & sentence) {
  // first extract all tags
  std::string sentence_out_no_metadata;
  std::vector<utterance_utils::ValuedTag> tags;
  utterance_utils::get_all_metadata_tags
      (sentence, sentence_out_no_metadata, &tags);
  // check if there is an emotion tag
  utterance_utils::Emotion sentence_emotion;
  if (utterance_utils::tags_contain_emotion_switch(tags, sentence_emotion)) {
    debugPrintf("There is an emotion metadata tag, changing emotion for '%s'\n",
                utterance_utils::emotion_to_full_string(sentence_emotion).c_str());
    _current_emotion = sentence_emotion;
    write_param_emotion();
  }
  // other actions
  // nothing till now...
}
