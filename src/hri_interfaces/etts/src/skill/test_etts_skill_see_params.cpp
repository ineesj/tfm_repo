

#include "api/etts_api.h"
#include "skill/etts_skill.h"

int main(int argc, char * argv[]) {

    Etts etts; //skill wrapper para poder usar la habilidad de manera sencilla sin preocuparnos de escribir en MCP

    printf("\n");

    if (etts.isVoiceKidnapped())
        printf("Voz secuestrada\n");
    else
        printf("Voz NO secuestrada\n");

    if (etts.isSpeakingNow())
        printf("El robot esta ahora mismo hablando\n");
    else
        printf("El robot no esta hablando\n");

    if (etts.canSpeakInmediatly())
        printf("Puedo hablar inmediatamente\n");
    else
        printf("NO puedo hablar inmediatamente\n");

    printf("Numero de locuciones en cola: %d\n", etts.numSpeechWaiting());
}

