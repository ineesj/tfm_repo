/*!
 * \file etts_say_sentence.cpp
 * Simple ros node that receives a message and uses the
 * EttsApi class to reproduce the sentence contained in this message.
 *
 * It receives two types of message: only a String, indicating
 * the text to say. Or a "Utterance" in which, optionally,
 * the emotion, language and primitive to be used can be indicated.
 *
 * \date Sept 11, 2013
 * \author Irene Perez
 *
 * \section Subscriptions
 * - \b "/ETTS_SAY_PLAIN_TEXT"
 *       [std_msgs/String]
 *       Carries a message to be said with etts.
 *
 */

#include "etts_msgs/Utterance.h"
#include "api/etts_api.h"
#include <std_msgs/String.h>

EttsApi* etts;

/*!
  * Reproduces the specified sentence (plain text) in the message
  */
void sayPlainTextCallback(const std_msgs::String::ConstPtr& msg) {
  ROS_INFO("Sentence to be said: [%s]", msg->data.c_str());
  etts->sayTextNL(msg->data.c_str());
}

//! Initializes this node and etts and subscribes to the ETTS_SAY_PLAIN_TEXT topic
int main(int argc, char** argv) {
  ros::init(argc, argv, "etts_say_sentence");
  ros::NodeHandle nh_public;
  etts = new EttsApi();
  //Subscribe to ETTS_SAY_PLAIN_TEXT topic
  ros::Subscriber say_plain_text = nh_public.subscribe
      ("ETTS_SAY_PLAIN_TEXT", 100, sayPlainTextCallback);
  ros::spin();
  delete etts;
  return 0;
}
