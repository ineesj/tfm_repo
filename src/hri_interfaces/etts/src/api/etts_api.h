#define USE_ETTS_API_ROS
#ifdef USE_ETTS_API_ROS
#include "etts_api_ros.h"
#else // USE_ETTS_API_ROS

#ifndef ETTS_SKILL_WRAPPER_H_
#define ETTS_SKILL_WRAPPER_H_

// C
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>

// C++
#include <string>
#include <vector>
#include <iostream>
// maggie
#include "communications/shareMemory/memCortoPlazo.h"
#include "communications/events/cevent.h"
#include "definitions/protoAD.h"
#include "definitions/RobotConfig.h"
#include "definitions/utterance_params.h"
#include "utils/speech_snippets/speech_snippets.h"
#include "primitives/nonverbal/non_verbal.h"
#include "semaphoreFile/semaphoreFile.h"

/**
 *  Esta clase sirve para que las habilidades puedan usar los mecanismos de
 *  sntesis de voz de manera sencilla y eficaz, sin tener que trabajar directamente
 *  con la memoria compartida,puesto que para eso esta esta clase,
 *  para hacer la tarea mas sencilla
 *
 *
 *  @author Fernando Alonso Martin
 *  @date 2009
 *
 */
//class Etts : public EttsSkillWrapperInterface {

using namespace ad::tts;


class EttsApi {
public:

    //! num max de locuciones que se pueden decir a la vez (encolar)
    const static int ETTS_MAX_QUEU = 10;

    //! num max de caracteres que puede tener una locucion
    const static int ETTS_MAX_CHARS_TEXT = 500;

    SemaphoreFile semaphoreFile;  //semaforo asociado a fichero

    ////////////////////////////////////////////////////////////////////////////
    /** Constructor, pone por defecto que no esta secuestrada la voz por el mismo (el atributo privado) */
    EttsApi();

    /** Destructor, elimina el semaforo creado */
    ~EttsApi();

    /*! in order to work, the API needs an event manager
      and a MCP client.
      They must be provided from somewhere else, for instance from a skill */
    void set_pointers(CEventManager* event_manager_ptr,
                      CmemCortoPlazo* MCP_ptr,
                      RobotConfig* robot_config_ptr,
                      bool want_init = true);

    /** Este metodo emite el evento que activa la habilidad etts */
    void init();

    /** Este método desactiva la habilidad*/
    void end();

    ///// command line stuff

    //! put in the command line parser the parameters for robot config
    static void put_options_in_args_parser(ArgsParser & parser);

    //! do the stuff associated with the parsing of the command line
    void apply_options_from_args_parser(const ArgsParser & parser);

    ///// end command line stuff

    /*!
      Para elegir el idioma con el que se va a hablar
     \param language_id
                the language identifier.
                They are defined in utterance_params.h
    */
    void setLanguage(const Translator::LanguageId language_id);

    /** Para elegir el idioma con el que se va a hablar
    \param internatinal_prefix
                    for instance "en" for english
                    or "es" for spanish
        **/
    void setLanguage(const std::string & internatinal_prefix);

    /** Devuelve el idioma establecido **/
    inline Translator::LanguageId getCurrentLanguage() const
    {return currentLanguage;}

    /*!
      Establece con que emocion hablar
     \param emotion
                They are defined in utterance_params.h
    */
    void setEmotion(const utterance_utils::Emotion emotion);

    //! \return the current emotion
    inline utterance_utils::Emotion getCurrentEmotion() const
    {return currentEmotion;}


    /*!
      Establece con que Engine sintetizar:
     \param primitive_id
                Loquendo, Festival, Google ...
                They are defined in utterance_params.h
    */
    void setPreferedPrimitive(const utterance_utils::PrimitiveId primitive_id);

    //! \return the current primitive
    inline utterance_utils::PrimitiveId getCurrentPrimitive() const
    {return _current_primitive;}

    /** Sintetiza el texto pasado como argumento en el idioma,
      la emoción y con el engine establecidos
      \param textCurrentLanguage
                the sentence to say.
                It can be extended with metadata, for instance:
                "\emotion=Happy I speak with an happy voice."
      \see apply_metadata_tags() for the supported tags.
      \param synthesisEngine
              permite elegir con que engine se va a sintetizar.
              Esto permite que distintas frases puedan ser sintetizadas
              con engines distintos, con cambios instantaneos
              entre un motor de sintesis y otro
      \return the timestamp of the utterance
        */
    long sayText(const std::string & textCurrentLanguage,
                 utterance_utils::PrimitiveId primitive = etts_msgs::Utterance::PRIM_LAST_USED,
                 bool emergency = false);

    /** Sintetiza el texto pasado como argumento con el idioma y
      el engine establecido,
      y con la emocion pasada por argumentos
      \return the timestamp of the utterance
    */
    long sayTextWithEmotion(const std::string & textCurrentLanguage,
                            utterance_utils::Emotion emotion,
                            bool emergency = false);

    /** Sintetiza el texto pasado por argumento, en el cual se puede indicar
        que idioma usar independientemente del idioma prefijado
      \example "es:hola|en:hi|fr:bonjour"
      Multiple instances of a given language are supported,
      one of these will be chosen randomly.
      \example textMultiLanguage="en:Hello|en:Hi|fr:Salut"
                 if (target_language == LANGUAGE_ENGLISH),
                   will return randomly "Hello" or "Hi"
                 if (target_language == LANGUAGE_FRENCH)
                   will return "Salut"
      \return the timestamp of the utterance
    */
    long sayTextNL(const std::string & textMultiLanguage);

    /*!
      * Says a random predefined snippet of text.
      * The random snippet is selected from an entered category
      * \param key the category in which the random snippet will be said
      * \return the same as EttsApi::sayTextNL()
      * \return -1 if the entered parameter has no snippets associated to it.
      * \see ad::tts::SpeechSnippets to check the available keys
      */
    long sayRandomSnippet(const ad::tts::SpeechSnippets::semanticKey  key);

    /**
      Permite traduccir una frase escrita en cualquier idioma a "Gibberish Language", el cual
      esta en un punto intermedio entre lenguaje verbal y no verbal, puesto que el significado
      de la oraccion es dificilmente entendible. Finalmente se sintetiza mediante el sistema
      de TTS establecido
      \return the timestamp of the utterance
      */
    long sayTextGibberishLanguage(const std::string & sourceString);

    /**
      Permite sintetizar sonidos no verbales
      \return the timestamp of the utterance
      */
    long sayNonVerbalSound (const NonVerbal::semanticKey semantic);


    /** @return TRUE si se puede hablar inmediatamente si llamamos a SayText, esto es porque no hay locuciones pendientes de decir y porque la voz no este secuestrada, o si lo esta lo esta por mi mismo y ademas no esta diciendo nada ahora mismo */
    bool canSpeakInmediatly();

    /** @return true si se esta generando voz en este mismo instante y false en caso contrario*/
    bool isSpeakingNow();

    /** Calla la voz dejando acabar la locucion actual */
    void shutUp();

    /** Calla la voz, cortando la locucion actual */
    void shutUpImmediatly();

    /** Pausa la voz dejandola lista para seguir en cuanto se quiera */
    void pauseSpeaking();

    /** Vuelve a hablar justo desde el momento que se pauso */
    void resumeSpeaking();

    /** Aumenta o disminuye el volumen en funcion de la cantidad positiva o negativa indicada
      that is, new volume = min(0, max(100, current volume + variation))
    */
    void changeVolume(int variation);

    /** Pone el volumen de la voz al maximo (100) */
    void setVolumeMax();

    /** Pone el volumen del robot al minimo (0) */
    void setVolumeMin();

    /**
      *  @return El numero de locuciones que estan actualmente encoladas esperando
      *  para ser sintetizadas
      */
    int numSpeechWaiting();

    /** Este metodo sirve para "secuestrar" la voz, es decir, nadie mas podra decir textos
     * mientras la voz este secuentrada por alguien
     * @return TRUE si ha podido secuestrarla y FALSE si no ha podido
     */
    bool kidnapSpeech();

    /**
     * Este metodo sirve para liberar la voz que estaba secuestrada, nada mas que puede
     * ser liberada por la misma instancia de esta clase que la secuestro
     * @return TRUE si ha podido libera la voz y FALSE si no ha podido
     */
    bool freeSpeech();

    /** @return TRUE si la voz ha sido secuestrada y FALSE en caso contrario */
    bool isVoiceKidnapped();

    // /////////////////////////////////////////////////////////////////////////
    // some convenient short hand functions
    // /////////////////////////////////////////////////////////////////////////

    /*! \return true if the current language is english */
    inline bool isCurrentLanguageEnglish() const {
        return (getCurrentLanguage() == Translator::LANGUAGE_ENGLISH);
    }
    /*! \return true if the current language is spanish */
    inline bool isCurrentLanguageSpanish() const {
        return (getCurrentLanguage() == Translator::LANGUAGE_SPANISH);
    }

    //! the map of all existing languages
    Translator::LanguageMap languages_map;

protected:
    /////////////////////////// stuff shared with EttsSkill

    //! the current language ID
    Translator::LanguageId currentLanguage;

    //! the current emotion ID
    utterance_utils::Emotion currentEmotion;

    //! the current primitive ID
    utterance_utils::PrimitiveId _current_primitive;

    /////////////////////////// end of stuff shared with EttsSkill

    /** the static function called when received "ETTS_SET_LANGUAGE".
        It changes the parameter currentLanguage **/
    static void setLanguage_static(void* aux, int p);
    /** the static function called when received "ETTS_SET_EMOTION".
        It changes the parameter currentEmotion **/
    static void setEmotion_static(void* aux, int p);
    /** the static function called when received "ETTS_SET_PREFERED_PRIMITIVE".
        It changes the parameter _current_primitive **/
    static void setPreferedPrimitive_static(void* aux, int p);

    /*! To date, there is only one possible metadata tag:
    syntax: \emotion=HAPPY || SAD || TRANQUILITY || NERVOUS
      */
    void apply_metadata_tags(const std::string & sentence);

    //! para saber si esta secuestrada por mi mismo la voz
    bool kidnapped_for_me;


    //! true when init() was sone
    bool init_done;

    //! EventManager
    CEventManager* event_manager_ptr;
    //! Memoria a corto plazo
    CmemCortoPlazo* MCP_ptr;
    //! the config of the robot
    RobotConfig* robot_config_ptr;

};

#endif
#endif // USE_ETTS_API_ROS
