/*!
  \file  etts_api_ros.h
  \author  Arnaud Ramey <arnaud.a.ramey@gmail.com>
  -- Robotics Lab, University Carlos III of Madrid
  \date  2014/2/16

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
abool with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

A convenient API for handling etts_msgs::Utterance messages.

\section Parameters
  Cf Utterance.msg

\section Subscriptions, Publications
  None.
 */
#ifndef ETTS_API_ROS_H
#define ETTS_API_ROS_H

#include <ros/node_handle.h>
#include <ros/this_node.h>
// AD
#include "translator/Translator.h"
#include "string/string_casts.h"
#include "etts_msgs/Utterance.h"
#include "definitions/utterance_params.h"
#include "utils/speech_snippets/speech_snippets.h"
#include "primitives/nonverbal/non_verbal.h"

class EttsApi {
public:
  EttsApi() : _nh_private("~"){
    _etts_pub = _nh_public.advertise<etts_msgs::Utterance>("etts", 10);
    Translator::build_languages_map(_languages_map);
  }

  //////////////////////////////////////////////////////////////////////////////

  /*!
  Para elegir el idioma con el que se va a hablar
  \param language_id: the language identifier.
  Translator::LANGUAGE_ENGLISH, Translator::LANGUAGE_SPANISH, etc.
  They are defined in Translator.h
  */
  bool setLanguage(const Translator::LanguageId language_id) const {
    etts_msgs::Utterance msg;

    if (!Translator::get_country_domain_from_language_id
        (_languages_map, language_id, msg.language))
      return false;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Para elegir el idioma con el que se va a hablar
  \param country_domain
  for instance "en" for english
  or "es" for spanish
  **/
  bool setLanguage(const Translator::CountryDomain & country_domain) const {
    etts_msgs::Utterance msg;
    msg.language = country_domain;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Devuelve el idioma establecido **/
  Translator::LanguageId getCurrentLanguage() const {
    std::string etts_domain = "";
    _nh_public.param("etts_language", etts_domain, etts_domain);
    if (etts_domain.empty()) {
      printf("Could not read param 'etts_language', etts skill started?\n");
      return utterance_utils::DEFAULT_LANGUAGE;
    }
    Translator::LanguageId lang;
    if (!Translator::get_language_id_from_country_domain(_languages_map, etts_domain, lang))
      return utterance_utils::DEFAULT_LANGUAGE;
    return lang;
  }

  //////////////////////////////////////////////////////////////////////////////

  /*!
  Establece con que emocion hablar
  \param emotion
  EMOTION_HAPPY, EMOTION_SAD, etc. They are defined in Utterance.h
  */
  bool setEmotion(const utterance_utils::Emotion emotion) const {
    etts_msgs::Utterance msg;
    msg.emotion = emotion;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  //! \return the current emotion
  utterance_utils::Emotion getCurrentEmotion() const {
    std::string etts_emotion = "";
    _nh_public.param("etts_emotion", etts_emotion, etts_emotion);
    if (etts_emotion.empty()) {
      printf("Could not read param 'etts_emotion', etts skill started?\n");
      return utterance_utils::DEFAULT_EMOTION;
    }
    utterance_utils::Emotion e;
    if (!utterance_utils::string_to_emotion(etts_emotion, e))
      return utterance_utils::DEFAULT_EMOTION;
    return e;
  }

  //////////////////////////////////////////////////////////////////////////////

  /*!
  Establece con que Engine sintetizar:
  \param primitive_id
  PRIM_LOQUENDO, PRIM_FESTIVAL, PRIM_GOOGLE ...
  They are defined in Utterance.h
  */
  bool setPreferedPrimitive(const utterance_utils::PrimitiveId primitive) const {
    etts_msgs::Utterance msg;
    msg.primitive = primitive;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  //! \return the current primitive
  utterance_utils::PrimitiveId getCurrentPrimitive() const {
    std::string etts_primitive = "";
    _nh_public.param("etts_primitive", etts_primitive, etts_primitive);
    if (etts_primitive.empty()) {
      printf("Could not read param 'etts_primitive', etts skill started?\n");
      return utterance_utils::DEFAULT_PRIMITIVE;
    }
    utterance_utils::PrimitiveId p;
    if (!utterance_utils::string_to_primitive(etts_primitive, p))
      return utterance_utils::DEFAULT_PRIMITIVE;
    return p;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Sintetiza el texto pasado como argumento en el idioma,
  la emoción y con el engine establecidos
  \param textCurrentLanguage
  the sentence to say.
  It can be extended with metadata, for instance:
  "\emotion=Happy I speak with an happy voice."
  \see apply_metadata_tags() for the supported tags.
  \param synthesisEngine
  permite elegir con que engine se va a sintetizar.
  Esto permite que distintas frases puedan ser sintetizadas
  con engines distintos, con cambios instantaneos
  entre un motor de sintesis y otro
  \return the timestamp of the utterance
  */
  bool sayText(const std::string & textCurrentLanguage,
               utterance_utils::PrimitiveId primitive = etts_msgs::Utterance::PRIM_LAST_USED,
               bool emergency = false) const {
    if (isVoiceKidnappedByOther())
      return false;
    etts_msgs::Utterance msg;
    msg.text = textCurrentLanguage;
    msg.primitive = primitive;
    if (emergency)
      msg.priority = etts_msgs::Utterance::SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Sintetiza el texto pasado como argumento con el idioma y
  el engine establecido,
  y con la emocion pasada por argumentos
  \return the timestamp of the utterance
  */
  bool sayTextWithEmotion(const std::string & textCurrentLanguage,
                          utterance_utils::Emotion emotion,
                          bool emergency = false) const {
    if (isVoiceKidnappedByOther())
      return false;
    etts_msgs::Utterance msg;
    msg.text = textCurrentLanguage;
    msg.emotion = emotion;
    if (emergency)
      msg.priority = etts_msgs::Utterance::SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Sintetiza el texto pasado por argumento, en el cual se puede indicar
  que idioma usar independientemente del idioma prefijado
  \example "es:hola|en:hi|fr:bonjour"
  Multiple instances of a given language are supported,
  one of these will be chosen randomly.
  \example textMultiLanguage="en:Hello|en:Hi|fr:Salut"
  if (target_language == LANGUAGE_ENGLISH),
  will return randomly "Hello" or "Hi"
  if (target_language == LANGUAGE_FRENCH)
  will return "Salut"
  \return the timestamp of the utterance
  */
  bool sayTextNL(const std::string & textMultiLanguage) const {
    if (isVoiceKidnappedByOther())
      return false;
    etts_msgs::Utterance msg;
    msg.text = textMultiLanguage;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /**
  Permite sintetizar sonidos no verbales
  \return the timestamp of the utterance
  */
  bool sayNonVerbalSound(const ad::tts::NonVerbal::semanticKey & semantic) const {
    return sayText(semantic, etts_msgs::Utterance::PRIM_NONVERBAL);
  }

  //////////////////////////////////////////////////////////////////////////////


  /** @return TRUE si se puede hablar inmediatamente si llamamos a SayText,
  * esto es porque no hay locuciones pendientes de decir y
  * porque la voz no este secuestrada,
  * o si lo esta lo esta por mi mismo y ademas no esta diciendo nada ahora mismo */
  bool canSpeakInmediatly() const {
    if (numSpeechWaiting() > 0)
      return false;
    if (isVoiceKidnappedByOther())
      return false;
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** @return true si se esta generando voz en este mismo instante y false en caso contrario*/
  bool isSpeakingNow() const {
    bool speaking_now = false;
    _nh_public.param("etts_speaking_now", speaking_now, speaking_now);
    return speaking_now;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Calla la voz dejando acabar la locucion actual */
  bool shutUp() const {
    return send_priority_order(etts_msgs::Utterance::SHUTUP_AND_SAY_SENTENCE);
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Calla la voz, cortando la locucion actual */
  bool shutUpImmediatly() const {
    return send_priority_order(etts_msgs::Utterance::SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE);
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Pausa la voz dejandola lista para seguir en cuanto se quiera */
  bool pauseSpeaking() const {
    return send_priority_order(etts_msgs::Utterance::PAUSE);
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Vuelve a hablar justo desde el momento que se pauso */
  bool resumeSpeaking() const {
    return send_priority_order(etts_msgs::Utterance::RESUME);
  }

  //////////////////////////////////////////////////////////////////////////////

  utterance_utils::Volume get_current_volume() const {
    utterance_utils::Volume etts_volume = -1;
    _nh_public.param("etts_volume", etts_volume, etts_volume);
    if (etts_volume > 0)
      return etts_volume;
    std::string etts_volume_str = "";
    _nh_public.param("etts_volume", etts_volume_str, etts_volume_str);
    if (etts_volume_str.size() > 0)
      return StringUtils::cast_from_string
          <utterance_utils::Volume>(etts_volume_str);
    printf("Could not read param 'etts_volume', etts skill started?\n");
    return utterance_utils::DEFAULT_VOLUME;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Aumenta o disminuye el volumen en funcion de la cantidad positiva o negativa indicada
  that is, new volume = min(0, max(100, current volume + variation))
  */
  bool changeVolume(int variation) const {
    return set_volume(get_current_volume() + variation);
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Pone el volumen de la voz al maximo (100) */
  bool setVolumeMax() const {
    return set_volume(etts_msgs::Utterance::VOLUME_MAX);
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Pone el volumen del robot al minimo (0) */
  bool setVolumeMin() const {
    return set_volume(etts_msgs::Utterance::VOLUME_MIN);
  }

  //////////////////////////////////////////////////////////////////////////////

  /**
  *  @return El numero de locuciones que estan actualmente encoladas esperando
  *  para ser sintetizadas
  */
  int numSpeechWaiting() const {
    int queue_size = 0;
    _nh_public.param("etts_queue_size", queue_size, queue_size);
    return queue_size;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** Este metodo sirve para "secuestrar" la voz, es decir, nadie mas podra decir textos
  * mientras la voz este secuentrada por alguien
  * @return TRUE si ha podido secuestrarla y FALSE si no ha podido
  */
  bool kidnapSpeech() const {
    _nh_private.setParam("etts_kidnapped", ros::this_node::getName());
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /**
  * Este metodo sirve para liberar la voz que estaba secuestrada, nada mas que puede
  * ser liberada por la misma instancia de esta clase que la secuestro
  * @return TRUE si ha podido libera la voz y FALSE si no ha podido
  */
  bool freeSpeech() const {
    _nh_private.setParam("etts_kidnapped", "");
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  /** @return TRUE si la voz ha sido secuestrada y FALSE en caso contrario */
  bool isVoiceKidnappedByOther() const {
    std::string etts_kidnapped = "";
    _nh_private.param("etts_kidnapped", etts_kidnapped, etts_kidnapped);
    if (etts_kidnapped.empty())
      return false;
    return (etts_kidnapped == _nh_private.getNamespace());
  }

  //////////////////////////////////////////////////////////////////////////////

  // /////////////////////////////////////////////////////////////////////////
  // some convenient short hand functions
  // /////////////////////////////////////////////////////////////////////////

  /*! \return true if the current language is english */
  bool isCurrentLanguageEnglish() const {
    return (getCurrentLanguage() == Translator::LANGUAGE_ENGLISH);
  }
  /*! \return true if the current language is spanish */
  bool isCurrentLanguageSpanish() const {
    return (getCurrentLanguage() == Translator::LANGUAGE_SPANISH);
  }

  //////////////////////////////////////////////////////////////////////////////

protected:

  //////////////////////////////////////////////////////////////////////////////

  bool send_priority_order(int order) const {
    if (isVoiceKidnappedByOther())
      return false;
    etts_msgs::Utterance msg;
    msg.priority = order;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  bool set_volume(utterance_utils::Volume v) const {
    if (isVoiceKidnappedByOther())
      return false;
    etts_msgs::Utterance msg;
    msg.volume = v;
    _etts_pub.publish(msg);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////

  //! the map of all existing languages
  Translator::LanguageMap _languages_map;
  ros::NodeHandle _nh_public, _nh_private;
  ros::Publisher _etts_pub;
}; // end class EttsApi

#endif // ETTS_API_ROS_H
