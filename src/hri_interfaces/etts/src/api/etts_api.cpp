/**
 *  Esta clase sirve para que las habilidades puedan usar los mecanismos de
 *  sntesis de voz de manera sencilla y eficaz, sin tener que trabajar directamente
 *  con la memoria compartida, puesto que para eso esta esta clase,
 *  para hacer la tarea mas sencilla
 *
 *
 *  @author Fernando Alonso Martin
 *  @date 2009
 *
 */

#include "api/etts_api.h"
#ifndef USE_ETTS_API_ROS

//#include "long_term_memory/ltm_path.h"

#include "utils/utterance_utils.h"
#include "debug/error.h"
#include "string/StringUtils.h"
#include "translator/Translator.h"


////////////////////////////////////////////////////////////////////////////////

EttsApi::EttsApi() {
  init_done = false;
  Translator::build_languages_map( languages_map );
  semaphoreFile.binarySemaphoreInitialize();
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::set_pointers(CEventManager* event_manager_pt,
                           CmemCortoPlazo* MCP_pt,
                           RobotConfig* robot_config_pt,
                           bool want_init /*= true*/) {
  maggieDebug2("set_pointers()");
  event_manager_ptr = event_manager_pt;
  MCP_ptr = MCP_pt;
  robot_config_ptr = robot_config_pt;
  if (want_init)
    init();
}

////////////////////////////////////////////////////////////////////////////////

EttsApi::~EttsApi() {
  //end();
  semaphoreFile.binarySemaphoreDestroy();
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::put_options_in_args_parser(ArgsParser & parser) {
  maggieDebug2("put_options_in_args_parser()");
  new ArgsFlag(parser, "novoice", "etts:use no ETTS system");
  new ArgsFlag(parser, "festival", "etts:use festival ETTS system");
  new ArgsFlag(parser, "loquendo", "etts:use Loquendo ETTS system");
  new ArgsFlag(parser, "google", "etts:use Google ETTS system");
  new ArgsFlag(parser, "microsoft", "etts:use microsoft ETTS system");

  Translator::LanguageMap _languages_map;
  Translator::build_languages_map(_languages_map);
  std::ostringstream lang_help_msg;
  lang_help_msg << "etts: change language among ";
  for(Translator::LanguageMap::const_iterator id = _languages_map.begin() ;
      id != _languages_map.end() ; ++id)
    lang_help_msg << id->second << ", ";
  new ArgsParam<std::string>(parser, "lang", lang_help_msg.str(), "es");

}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::apply_options_from_args_parser(const ArgsParser & parser) {
  maggieDebug2("apply_options_from_args_parser()");

  /*
     * there is no longer any default option
     */

  if (parser.get_value_flag("novoice"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_NOVOICE);
  else if (parser.get_value_flag("festival"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_FESTIVAL);
  else if (parser.get_value_flag("loquendo"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_LOQUENDO);
  else if (parser.get_value_flag("google"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_GOOGLE);
  else if (parser.get_value_flag("microsoft"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_MICROSOFT);
  else if (parser.get_value_flag("pico"))
    setPreferedPrimitive(etts_msgs::Utterance::PRIM_PICO);

  // do not change language if not set
  if (parser.is_param_defined("lang")) {
    // load language
    std::string lang_string = parser.get_value_param<std::string>("lang");
    bool success;
    Translator::LanguageId lang_id = Translator::get_language_id_from_country_domain(
                                       languages_map, lang_string, success);
    if (success) {
      maggieDebug2("Option for language %i ('%s') detected.",
                   lang_id, lang_string.c_str());
      setLanguage(lang_id);
    } // end if success
  }
}


////////////////////////////////////////////////////////////////////////////////

void EttsApi::init() {
  maggieDebug2("init()");

  //inicializamos el semaforo asociado a fichero, para que no se puedan hacer varios sayText simultaneos
  //binarySemaphoreInitialize();

  // subscriptions
  event_manager_ptr->subscribe( "ETTS_SET_LANGUAGE", setLanguage_static, this);
  event_manager_ptr->subscribe( "ETTS_SET_EMOTION", setEmotion_static, this);
  event_manager_ptr->subscribe( "ETTS_SET_PREFERED_PRIMITIVE", setPreferedPrimitive_static, this);

  kidnapped_for_me = false;

  /*
     * init from MCP
     */
  std::string mpc_string_buffer;
  /* language */
  // get the string version
  int return_code = MCP_ptr->get_current_item("ETTS_LANGUAGE",mpc_string_buffer);
  // convert to int
  currentLanguage = Translator::LANGUAGE_ENGLISH;
  if (return_code == 0)
    currentLanguage = (Translator::LanguageId) StringUtils::cast_from_string<int>(mpc_string_buffer);
  else {
    ROS_WARN("Impossible to get the current language (is the etts skill started?),"
             "setting it to '%s'",
             Translator::get_language_full_name(currentLanguage).c_str());
  }

  /* emotion */
  // get the string version
  return_code = MCP_ptr->get_current_item("ETTS_EMOTION",mpc_string_buffer);
  // convert to int
  currentEmotion = etts_msgs::Utterance::EMOTION_HAPPY;
  if (return_code == 0)
    currentEmotion = (utterance_utils::Emotion) StringUtils::cast_from_string<int>(mpc_string_buffer);
  else {
    ROS_WARN("Impossible to get the current emotion (is the etts skill started?),"
             "setting it to '%s'",
             utterance_utils::emotion_to_full_string(currentEmotion).c_str());
  }

  /* Primitive */
  // get the string version
  return_code = MCP_ptr->get_current_item("ETTS_PRIMITIVE",mpc_string_buffer);
  // convert to int
  _current_primitive = etts_msgs::Utterance::PRIM_NOVOICE;
  if (return_code == 0)
    _current_primitive = (utterance_utils::PrimitiveId) StringUtils::cast_from_string<int>(mpc_string_buffer);
  else {
    ROS_WARN("Impossible to get the current primitive (is the etts skill started?),"
             "setting it to '%s'",
             utterance_utils::primitive_to_full_string(_current_primitive).c_str());
  }

  // emissions
  std::vector<std::string> possible_events;
  possible_events.push_back("ETTS_SET_LANGUAGE");
  possible_events.push_back("ETTS_SET_EMOTION");
  possible_events.push_back("ETTS_SET_PREFERED_PRIMITIVE");
  possible_events.push_back("ETTS_START");
  possible_events.push_back("ETTS_STOP");
  possible_events.push_back("ETTS_NEW_SENTENCE_IN_MCP");
  possible_events.push_back("ETTS_SHUT_UP");
  event_manager_ptr->advertise(possible_events);

  // emit event
  event_manager_ptr->emit("ETTS_START");

  //leave time for the setXXX() to be done
#ifndef AD_USE_ROS
  //    maggieDebug2("Sleeping one second...");
  sleep(1);  // not needed in ROS as it will be done in the advertise()
#endif

  // save the fact we did init();
  init_done = true;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::end() {
  maggieDebug2("end()");


  if (init_done) {
    event_manager_ptr->unsubscribe("ETTS_SET_LANGUAGE");
    event_manager_ptr->unsubscribe("ETTS_SET_EMOTION");
    event_manager_ptr->emit("ETTS_STOP");
  }

  //binarySemaphoreDestroy();
}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayTextWithEmotion(const std::string & textCurrentLanguage,
                                 utterance_utils::Emotion emotion,
                                 bool emergency /*= false*/) {
  setEmotion(emotion);
  return sayText(textCurrentLanguage, _current_primitive, emergency);
}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayText(const std::string & textCurrentLanguage,
                      utterance_utils::PrimitiveId primitive
                      /*= etts_msgs::Utterance::PRIM_LAST_USED*/,
                      bool emergency /*= false*/) {
  maggieDebug2("sayText('%s', engine:%i, emergency:%i)",
               textCurrentLanguage.c_str(),
               primitive, emergency);

  // this must be the key function

  semaphoreFile.binarySemaphoreDown();

  if (!init_done)
    maggieError("init() not done !");

  if (isVoiceKidnapped() && kidnapped_for_me == false) {
    maggiePrint("Sorry but the voice is kidnapped, impossible to speak.");
    return -1;
  }

  // load the last primitive if needed
  if (primitive == etts_msgs::Utterance::PRIM_LAST_USED)
    primitive = _current_primitive;

  // use the metadata tags
  apply_metadata_tags(textCurrentLanguage);

  // build the utterance_utils object for MCP
  utterance_utils utter(
        time(NULL),
        currentLanguage,
        currentEmotion,
        emergency,
        textCurrentLanguage,
        primitive);

  // share it to MCP - concatenate it at the end of the queue
  MCP_ptr->replace_item("ETTS_UTTERANCES_QUEUE", utter.to_string(), true);

  // increment the queue size
  //        MCP_ptr->replace_item("ETTS_UTTERANCES_QUEUE_SIZE",
  //                              StringUtils::cast_to_string<int>(1 + numSpeechWaiting()));

  // read it, just for tests
  //    std::string read_queue;
  //    MCP_ptr->get_current_item("ETTS_UTTERANCES_QUEUE", read_queue);
  //    maggiePrint("new_queue:%s", read_queue.c_str());

  // send the event
  event_manager_ptr->emit("ETTS_NEW_SENTENCE_IN_MCP");

  semaphoreFile.binarySemaphoreUp();

  return utter.mtype;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::apply_metadata_tags(const std::string & sentence) {
  // first extract all tags
  std::string sentence_out_no_metadata;
  std::vector<utterance_utils::ValuedTag> tags;
  utterance_utils::get_all_metadata_tags(sentence, sentence_out_no_metadata, &tags);
  // check if there is an emotion tag
  utterance_utils::Emotion sentence_emotion;
  if (utterance_utils::tags_contain_emotion_switch(tags, sentence_emotion)) {
    maggieDebug2("There is an emotion metadata tag, changing emotion for '%s'",
                 utterance_utils::emotion_to_full_string(sentence_emotion).c_str());
    setEmotion(sentence_emotion);
  }
  // other actions
  // nothing till now...
}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayTextNL(const std::string & textMultiLanguage) {
  maggieDebug2("sayTextNL(textMultiLanguage:'%s') - current-language:%i",
               textMultiLanguage.c_str(),
               getCurrentLanguage());

  std::string wanted_sentence;
  Translator::_build_given_language_in_multilanguage_line
      (textMultiLanguage, currentLanguage, languages_map, wanted_sentence);
  return sayText(wanted_sentence, _current_primitive);
}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayRandomSnippet(const ad::tts::SpeechSnippets::semanticKey key){
  std::string snippet;

  if (snippet.compare("ROBOT_NAME") == 0){
    return sayText(robot_config_ptr->sw.voice_name);
  }

  snippet = ad::tts::SpeechSnippets::getRandomSnippet(key);
  if (!snippet.empty()){
    return (sayTextNL(snippet));
  }
  maggieDebug2("Error: entered parameter has no available snippets");
  return -1; // in case of error
}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayNonVerbalSound (const NonVerbal::semanticKey semantic){

  //int ttsEngineFixed = getPreferedPrimitive();
  //maggiePrint("Se esta usando el engine de voz num %d",ttsEngineFixed);

  //ponemos el motor de sintesis no verbal
  //setPreferedPrimitive(etts_msgs::Utterance::PRIM_NONVERBAL);
  //decimos el sonido
  int aDevolver = sayText(semantic, etts_msgs::Utterance::PRIM_NONVERBAL);

  //volvemos a dejar la misma primitva de voz que estaba
  //const int aux = ttsEngineFixed;
  //setPreferedPrimitive(aux);

  return aDevolver;

}

////////////////////////////////////////////////////////////////////////////////

long EttsApi::sayTextGibberishLanguage(const std::string & sourceString){
  std::string inGibberish = Translator::_translateToGibberishLanguage(sourceString);
  return sayText(inGibberish);
}

////////////////////////////////////////////////////////////////////////////////

int EttsApi::numSpeechWaiting(){
  std::string queue_size_str;
  int return_value = MCP_ptr->get_current_item("ETTS_UTTERANCES_QUEUE_SIZE", queue_size_str);
  if (return_value == 0)
    return StringUtils::cast_from_string<int>(queue_size_str);
  maggieDebug1("Impossible to get the queue size (is the etts skill started?),"
               "returning 0");
  return 0;
}

////////////////////////////////////////////////////////////////////////////////

bool EttsApi::canSpeakInmediatly() {
  if (isSpeakingNow())
    return false;
  if (numSpeechWaiting() != 0)
    return false;
  if (isVoiceKidnapped() && kidnapped_for_me == false)
    return false;
  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool EttsApi::isSpeakingNow() {
  std::string ans_str;
  int return_value = MCP_ptr->get_current_item("etts_speaking_now", ans_str);
  if (return_value == 0)
    return StringUtils::cast_from_string<bool>(ans_str);
  maggieDebug1("Impossible to get the etts_speaking_now item (is the etts skill started?),"
               "returning false");
  return false;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::shutUpImmediatly() {
  maggieDebug2("shutUpImmediatly()");
  event_manager_ptr->emit("ETTS_SHUT_UP", 0);
  // wait two seconds if we use loquendo
  //  if (_current_primitive == etts_msgs::Utterance::PRIM_LOQUENDO) {
  //    maggieDebug2("Sleeping two secs after shutUpImmediatly() "
  //                 "for Loquendo buffer to get cleaned");
  //    sleep(1);
  //  }
  //  else {
  maggieDebug2("Sleeping a bit after shutUpImmediatly() "
               "for audio buffer to be freed");
  usleep(300 * 1000);
  //  }
} // end shutUpImmediatly()

////////////////////////////////////////////////////////////////////////////////

void EttsApi::shutUp() {
  maggieDebug2("shutUp()");
  event_manager_ptr->emit("ETTS_SHUT_UP", 1);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::pauseSpeaking() {
  maggieDebug2("pauseSpeaking()");
  event_manager_ptr->emit("ETTS_SHUT_UP", 2);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::resumeSpeaking() {
  maggieDebug2("resumeSpeaking()");
  event_manager_ptr->emit("ETTS_SHUT_UP", 3);
}

////////////////////////////////////////////////////////////////////////////////

bool EttsApi::kidnapSpeech() {
  maggieDebug2("kidnapSpeech()");
  if (!isVoiceKidnapped()) {
    MCP_ptr->replace_item("ETTS_KIDNAP", StringUtils::cast_to_string<bool>(true));
    kidnapped_for_me = true;
    return true;
  } else {
    return false;
  }
}

////////////////////////////////////////////////////////////////////////////////

bool EttsApi::freeSpeech() {
  maggieDebug2("freeSpeech()");
  if (kidnapped_for_me) {
    //escribimos en mcp que lo liberamos
    MCP_ptr->replace_item("ETTS_KIDNAP", StringUtils::cast_to_string<bool>(false));
    kidnapped_for_me = false;
    return true;
  } else {
    return false;
  }
}

////////////////////////////////////////////////////////////////////////////////

bool EttsApi::isVoiceKidnapped() {
  if (kidnapped_for_me)
    return true;

  std::string ans_str;
  int return_value = MCP_ptr->get_current_item("ETTS_KIDNAP", ans_str);
  if (return_value == 0)
    return StringUtils::cast_from_string<bool>(ans_str);
  maggieDebug1("Impossible to get the ETTS_KIDNAP item (is the etts skill started?),"
               "returning false");
  return false;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setVolumeMax(){
  event_manager_ptr->emit("ETTS_SET_VOLUME",100);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setVolumeMin(){
  event_manager_ptr->emit("ETTS_SET_VOLUME",0);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::changeVolume (int cantidad){
  event_manager_ptr->emit("ETTS_CHANGE_VOLUME", cantidad);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setLanguage(const std::string & internatinal_prefix) {
  maggieDebug2("setLanguage('%s')", internatinal_prefix.c_str());
  // try to convert the string to a language id
  bool success;
  Translator::LanguageId language_id = Translator::get_language_id_from_country_domain
                                       (languages_map, internatinal_prefix, success);
  // do nothing if it failed
  if (!success) {
    maggiePrint("Impossible to identify the language '%s'",
                internatinal_prefix.c_str());
    return;
  }
  // otherwise, perform the change
  setLanguage(language_id);
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setLanguage(const Translator::LanguageId language_id) {
  maggieDebug2("setLanguage('%s')",
               Translator::get_language_full_name(language_id).c_str());
  if (currentLanguage == language_id) {
    maggieDebug2("'%s' was already the active language, doing nothing.",
                 Translator::get_language_full_name(language_id).c_str());
    return;
  }

  // change it locally first, to go faster
  currentLanguage = language_id;
  // then send the event
  event_manager_ptr->emit("ETTS_SET_LANGUAGE", language_id);
  // save it to MCP for other EttsApi initializations
  MCP_ptr->replace_item("ETTS_LANGUAGE",StringUtils::cast_to_string<int>(language_id));
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setEmotion(const utterance_utils::Emotion emotion) {
  maggieDebug2("setEmotion(%s)",
               utterance_utils::emotion_to_full_string(emotion).c_str());
  if (currentEmotion == emotion) {
    maggieDebug2("'%s' was already the active emotion, doing nothing.",
                 utterance_utils::emotion_to_full_string(emotion).c_str());
    return;
  }

  // first locally, to go faster
  currentEmotion = emotion;
  // then send the event
  event_manager_ptr->emit("ETTS_SET_EMOTION", emotion);
  // save it to MCP for other EttsApi initializations
  MCP_ptr->replace_item("ETTS_EMOTION",StringUtils::cast_to_string<int>(emotion));

}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setPreferedPrimitive(const utterance_utils::PrimitiveId primitive_id) {
  maggieDebug2("setPreferedPrimitive(%s)",
               utterance_utils::primitive_to_full_string(primitive_id).c_str());
  if (primitive_id == etts_msgs::Utterance::PRIM_NONVERBAL) {
    maggiePrint("You should not use the primitive PRIM_NONVERBAL in setPreferedPrimitive();");
    return;
  }
  if (_current_primitive == primitive_id) {
    maggieDebug2("'%s' was already the active primitive, doing nothing.",
                 utterance_utils::primitive_to_full_string(primitive_id).c_str());
    return;
  }

  // first locally, to go faster
  _current_primitive = primitive_id;
  // then send the event
  event_manager_ptr->emit("ETTS_SET_PREFERED_PRIMITIVE", primitive_id);
  // save it to MCP for other EttsApi initializations
  MCP_ptr->replace_item("ETTS_PRIMITIVE",StringUtils::cast_to_string<int>(primitive_id));
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setLanguage_static(void* aux, int p){
  maggieDebug2("setLanguage_static(%i)", p);
  ((EttsApi *) aux)->currentLanguage = p;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setEmotion_static(void* aux, int p){
  maggieDebug2("setEmotion_static(%i)", p);
  ((EttsApi *) aux)->currentEmotion = (utterance_utils::Emotion) p;
}

////////////////////////////////////////////////////////////////////////////////

void EttsApi::setPreferedPrimitive_static(void* aux, int p){
  maggieDebug2("setPreferedPrimitive_static(%i)", p);
  ((EttsApi *)aux)->_current_primitive = (utterance_utils::PrimitiveId) p;
}
#endif // USE_ETTS_API_ROS
