/*!
 * \file test__etts->cpp
 *
 * Some tests for the voice system
 *
 * \date Dec 3, 2010
 * \author Arnaud Ramey
 */
#include "api/etts_api.h"
#include "utils/primitive_test.h"
#include "string/StringUtils.h"
#include <ros/package.h>

std::string PATH = ros::package::getPath("etts");
/*!
  * This class is a Test for the EttsApi, in it, all funcionalities about ETTS are tested
  */

class ApiTest : public EttsTest {
public:
  ApiTest(EttsApi* etts)
    : EttsTest(false), _etts(etts) {
    // set the language option
    run();
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_emotion(const utterance_utils::Emotion emot) {
    _etts->setEmotion(emot);
    sleep(1);
  }

  ////////////////////////////////////////////////////////////////////////////////

  void set_primitive(const utterance_utils::PrimitiveId primitive) {
    _etts->setPreferedPrimitive(primitive);
    sleep(1);
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void say_multi_language(const std::string &line) {
    _etts->sayTextNL(line);
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_language(const Translator::LanguageId lang) {
    maggieDebug2("change_language(%s)",
                 Translator::get_language_full_name(lang).c_str());
    _etts->setLanguage(lang);
    sleep(1);
  }
  virtual Translator::LanguageId get_language() const
  {return _etts->getCurrentLanguage();}

  //////////////////////////////////////////////////////////////////////////////

  virtual void print_menu(const Translator::LanguageMap & languagmap) {
    EttsTest::print_menu(languagmap);
    printf("[voice system]       10:Novoice  13:Google    14:Microsoft 16:Espeak \n");
    printf("[non verbal]         31:singing  32:confirmation  33:thinking  34:warning\n");
    printf("                     35:dialog   36:hello         37:error     38:amazing\n");
    printf("[music scores]       *:play what you enter (example: '*A6,B6')\n");
    printf("                     50:octaves 51:happy_birthday 52:happy_birthday2  53:tetris\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void say(const std::string &line) {
      _etts->sayText(line);
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void int_choice(int choice) {
    EttsTest::int_choice(choice);
    // engines
    if (choice == 10)
      set_primitive(etts_msgs::Utterance::PRIM_NOVOICE);
    else if (choice == 13)
      set_primitive(etts_msgs::Utterance::PRIM_GOOGLE);
    else if (choice == 14)
      set_primitive(etts_msgs::Utterance::PRIM_MICROSOFT);
    else if (choice == 16)
      set_primitive(etts_msgs::Utterance::PRIM_ESPEAK);
    // non verbal
    else if (choice ==31)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::SINGING);
    else if (choice ==32)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::CONFIRMATION);
    else if (choice ==33)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::THINKING);
    else if (choice ==34)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::WARNING);
    else if (choice ==35)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::DIALOG);
    else if (choice ==36)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::HELLO);
    else if (choice ==37)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::ERROR);
    else if (choice ==38)
      _etts->sayNonVerbalSound(ad::tts::NonVerbal::AMAZING);

  }

  //////////////////////////////////////////////////////////////////////////////

  virtual std::string info_string() {
    std::ostringstream info_str;
    info_str << Translator::get_language_full_name(_etts->getCurrentLanguage()) << ", ";
    info_str << utterance_utils::emotion_to_full_string(_etts->getCurrentEmotion()) << ", ";
    info_str << utterance_utils::primitive_to_full_string(_etts->getCurrentPrimitive());
    return info_str.str();
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void shutup() {_etts->shutUp();}
  virtual void shutup_immediatly() {_etts->shutUpImmediatly();}
  virtual void pause() {_etts->pauseSpeaking();}
  virtual void resume() {_etts->resumeSpeaking();}

  //////////////////////////////////////////////////////////////////////////////

private:
  EttsApi* _etts;
}; // end class ApiTest

////////////////////////////////////////////////////////////////////////////////

void interface(int argc, char **argv) {
  maggieDebug2("interface()");
  EttsApi api;
  ApiTest test(&api);
}

////////////////////////////////////////////////////////////////////////////////

#define NUM_THREADS     3
struct ThreadData {
  EttsApi* tts;
  int thread_id;
};

void* thread_speaker(void *thread_data) {
  ThreadData* data = (ThreadData*) thread_data;
  for (int var = 0; var < 4; ++var) {
    printf("thread #%i - repreat %i!\n", 0, var);
    std::ostringstream sentence;
    sentence << data->thread_id << "." << var;
    data->tts->sayText(sentence.str());
    usleep(10 * 1000);
  }
  pthread_exit(NULL);
}

void dummy_test_loop(int argc, char **argv) {
  EttsApi tts;
  pthread_t threads[NUM_THREADS];
  for(long thread_idx = 0; thread_idx < NUM_THREADS; thread_idx++){
    //maggiePrint("In main: creating thread %ld", t);
    ThreadData data;
    data.tts = &tts;
    data.thread_id = thread_idx;
    int rc = pthread_create(&threads[thread_idx], NULL, thread_speaker, &data);
    if (rc){
      maggiePrint("ERROR; return code from pthread_create() is %d", rc);
      exit(-1);
    }
  }

  maggiePrint("enter a number to terminate");
  int foo;
  std::cin >> foo;

  /* Last thing that main() should do */
  pthread_exit(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void dummy_test_accents(int argc, char **argv) {
  EttsApi tts;
  std::string txt = "hóla. nos vemos mañna.";
  tts.sayText(txt);

  std::string txt_copy_cleaned = txt;
  StringUtils::clean_spanish_chars(txt_copy_cleaned);
  maggieDebug2("txt_copy_cleaned:%s", txt_copy_cleaned.c_str());
  tts.sayText(txt_copy_cleaned);

  sleep(8); // leave time for the second sentence
}

////////////////////////////////////////////////////////////////////////////////

//void dummy_test_file(int argc, char **argv) {
//  EttsApi tts;
//  tts.setLanguage(Translator::LANGUAGE_ENGLISH);
//  std::string filename;
//  //if (argc == 1)
//  filename = PATH + "data/books/she_walks_in_beauty.poem";
//  //else
//  //filename = argv[1];
//  std::vector<std::string> file_lines;
//  StringUtils::retrieve_file_split(filename, file_lines);
//  for (unsigned int i = 0; i < file_lines.size(); ++i) {
//    tts.sayText(file_lines.at(i));
//    maggieDebug3("Press enter for next line");
//    char line_input [10];
//    std::cin.getline(line_input, 10);
//  }
//}

////////////////////////////////////////////////////////////////////////////////

void test_multilanguage(int argc, char **argv) {
  EttsApi etts;
  etts.sayTextNL("|es:Buenos dias.");

  int numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  etts.setPreferedPrimitive(etts_msgs::Utterance::PRIM_GOOGLE);
  etts.setLanguage(Translator::LANGUAGE_SPANISH);
  etts.sayText("1. Probando a decir Hola Mundo");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);


  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_ENGLISH);
  etts.sayText("2. Testing with Hello World 2. No translation.");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_SPANISH);
  etts.sayTextNL("es: 3. Esto es un Hola Mundo 3. No traduccion.");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_ENGLISH);
  etts.sayTextNL("es: 4. Hola Mundo 4, seleccion, no traduccion."
                 "|en: 4. Hello world 4, selection, no translation.");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_ENGLISH);
  etts.sayTextNL("es: 5. Hola Mundo 5, no seleccion, traduccion.");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_FRENCH);
  etts.sayTextNL("es: 6. Hola Mundo 6, seleccion, traduccion."
                 "|en: 6. Hello world 6, selection, translation.");
  numeroLocuciones = etts.numSpeechWaiting();
  maggiePrint("Numero locuciones cola %d",numeroLocuciones);

  //progamos la generacion automática de voz
  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_SPANISH);
  etts.sayTextNL("\\NLG=OK");

  //progamos la generacion automática de voz
  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_ENGLISH);
  etts.sayTextNL("\\NLG=OK");

  //probamos SayTextNL sin lenguaje
  //sleep(2);
  etts.setLanguage(Translator::LANGUAGE_SPANISH);
  etts.sayTextNL("es:\\item=Breath_05");

  //sleep(2);
  //probamos la generacion de lenguaje no veral
  etts.sayNonVerbalSound(ad::tts::NonVerbal::HELLO);

  //probamos la generacion de lenguaje no veral
  etts.sayNonVerbalSound(ad::tts::NonVerbal::AMAZING);

  etts.sayText("Volvemos a hablar con Google");


  // read a foo int to wait
  int foo;
  std::cin >> foo;
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_etts_api");
  srand(time(NULL));
  //dummy_test_loop(argc, argv);
  //dummy_test_accents(argc, argv);
  //dummy_test_file(argc, argv);
  //dummy_test_utterance_to_string();
  //test_multilanguage(argc, argv);
  interface(argc, argv);
}

