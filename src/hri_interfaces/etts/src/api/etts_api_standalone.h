/*!
  \file        etts_api_standalone.h
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2013/3/1

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

EttsApi is a thin wrapper around EttsApi
that will contain everything EttsApi needs to work,
such as an event client, a MCP client, etc.

It is aimed at making easier using the EttsApi outside of AD skills.
*/

#ifndef ETTS_API_STANDALONE_H
#define ETTS_API_STANDALONE_H
#include "api/etts_api.h"
#endif // ETTS_API_STANDALONE_H
