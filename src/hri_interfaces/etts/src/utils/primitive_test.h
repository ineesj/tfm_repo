#ifndef PRIMITIVE_TEST_H
#define PRIMITIVE_TEST_H

#include "etts_msgs/Utterance.h"
#include "definitions/utterance_params.h"
#include "string/string_casts.h"

class EttsTest {
public:

  static const std::string SENTENCE_SHORT;
  static const std::string SENTENCE_HAPPY;
  static const std::string SENTENCE_SAD;
  static const std::string SENTENCE_TRANQUILITY;
  static const std::string SENTENCE_NERVOUS;
  static const std::string SENTENCE_EFFECTS;

  //////////////////////////////////////////////////////////////////////////////

  EttsTest(bool remove_loquendo_tags = true) {
    _remove_loquendo_tags = remove_loquendo_tags;
    Translator::build_languages_map(_language_map);
  }

  //////////////////////////////////////////////////////////////////////////////

  void run() {
    std::string lineFromKeyboard;
    while (true) {
      std::cout << std::endl;
      printf("--------------------\n");
      print_menu(_language_map);
      printf("-------------------- 0: quit\n");

      ROS_INFO("[%s] Type your text or choice",
                  info_string().c_str());
      // prompt
      std::cout << "> ";
      std::cout.flush();
      getline(std::cin, lineFromKeyboard);
      // empty line -> exiting
      if (lineFromKeyboard.length() == 0)
        break;
      // analyze the results
      bool success;
      int choice = StringUtils::cast_from_string<int>(lineFromKeyboard, success);
      if (success) {
        if (choice == 0) // exit
          break;
        int_choice(choice);
      } // end if success
      else { // the input was not an int
        say(lineFromKeyboard);
      }
    } // end while true

  } // end ctor

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_emotion(const utterance_utils::Emotion emot) = 0;

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_language(const Translator::LanguageId lang) = 0;
  virtual Translator::LanguageId get_language() const = 0;

  //////////////////////////////////////////////////////////////////////////////

  virtual void say(const std::string &text) = 0;

  //////////////////////////////////////////////////////////////////////////////

  virtual void shutup() = 0;
  virtual void shutup_immediatly() = 0;
  virtual void pause() = 0;
  virtual void resume() = 0;

  //////////////////////////////////////////////////////////////////////////////

  virtual std::string info_string() = 0;

  //////////////////////////////////////////////////////////////////////////////

  virtual void print_menu(const Translator::LanguageMap & languagmap) {
    // emotion
    printf("[emotion]            1:HAPPY 2:SAD 3:TRANQUILITY 4:NERVOUS\n");
    printf("[control]            5:shut up (6:immediatly) 7:pause 8:resume\n");

    // language menu
    std::ostringstream languages;
    int idx = 0;
    for(Translator::LanguageMap::const_iterator id = languagmap.begin() ;
        id != languagmap.end() ; ++id) {
      languages << 100 + id->first << ":" << id->second << "  ";
      if ((++idx) % 10 == 0) // endline
        languages << std::endl << std::string(21, ' ');
    }
    printf("[language]           %s\n", languages.str().c_str());

    // predef sentences
    printf("[predef sentences]   20:short 21:happy, 22:sad, 23:tranquility, "
           "24:nervous, 25:effects\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void int_choice(int choice) {
    // emotions
    if (choice == 1)
      set_emotion(etts_msgs::Utterance::EMOTION_HAPPY);
    else if (choice == 2)
      set_emotion(etts_msgs::Utterance::EMOTION_SAD);
    else if (choice == 3)
      set_emotion(etts_msgs::Utterance::EMOTION_TRANQUILITY);
    else if (choice == 4)
      set_emotion(etts_msgs::Utterance::EMOTION_NERVOUS);
    // conrtol
    else if (choice == 5)
      shutup();
    else if (choice == 6)
      shutup_immediatly();
    else if (choice == 7)
      pause();
    else if (choice == 8)
      resume();
    // language
//    else if (_language_map.count(choice - 100) > 0) {
//      set_language(choice -100);
    else if (choice > 100) {
      Translator::LanguageId lang = (Translator::LanguageId) (choice-100);
      set_language(lang);
    }

    // predef sentences
    else if (choice == 20)
      say_multi_language(SENTENCE_SHORT);
    else if (choice == 21)
      say_multi_language(SENTENCE_HAPPY);
    else if (choice == 22)
      say_multi_language(SENTENCE_SAD);
    else if (choice == 23)
      say_multi_language(SENTENCE_TRANQUILITY);
    else if (choice == 24)
      say_multi_language(SENTENCE_NERVOUS);
    else if (choice == 25)
      say_multi_language(SENTENCE_EFFECTS);
  } // end int_choice();

  //////////////////////////////////////////////////////////////////////////////

  virtual void say_multi_language(const std::string &line) {
    // get the wanted language
    std::string sentence_curr_lang = "";
    Translator::_build_given_language_in_multilanguage_line
        (line, get_language(), _language_map, sentence_curr_lang);
    // say that
    say(sentence_curr_lang);
  }

  //////////////////////////////////////////////////////////////////////////////

protected:
  Translator::LanguageMap _language_map;

  //! strip the Loquendo tags
  bool _remove_loquendo_tags;
}; // end class PrimitiveTest

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const std::string EttsTest::SENTENCE_SHORT =
    "|en:This is a test sentence. My name is Maggie."
    //
    "|es:Eso es una frase de prueba. Mi nombre es Magui.";

const std::string EttsTest::SENTENCE_HAPPY =
    "|es:\\emotion=HAPPY "
    "\\item=Ollah soy el robot personal y asistencial "
    "de la universidad carlos tercero de madrid, \\item=Cough . "
    "Me gustaria que jugaras conmigo, animate!! "
    "Ademas me han dejado mucho mas delgadita y guapa \\item=Whistle_01 . "
    "Un saludo y gracias! \\item=Smack"
    //
    "|en:\\emotion=HAPPY "
    "\\item=Ollah I am the personal robot "
    "of the University Carlos Tercero of Madrid, \\item=Cough . "
    "I would like you to play with me, come on!! "
    "Furthermore, I had a complete relooking. \\item=Whistle_01 . "
    "See you, thanks! \\item=Smack";

const std::string EttsTest::SENTENCE_SAD =
    "|es:\\emotion=SAD "
    "\\item=Breath Hola soy un robot que se encuentra muy triste. "
    "\\item=Breath_02 La verdad es que te echo mucho de menos! "
    "No se que va a ser de mi vida y si me van a sustituir "
    "por otro robot mas joven \\item=Breath_06 ."
    //
    "|en:\\emotion=SAD "
    "\\item=Breath Hello, I am a very sad robot. "
    "\\item=Breath_02 To be honest, I really miss you! "
    "I don't know what I will become, and if they will replace me "
    "by a younger robot \\item=Breath_06 .";

const std::string EttsTest::SENTENCE_TRANQUILITY =
    "|es:\\emotion=TRANQUILITY "
    "\\item=Yawn Hola me acabo de despertar y "
    "estoy todavia un poco adormilada. "
    "\\item=Yawn_01 te voy a dejar que me voy a dar un paseo; "
    "lo siento!"
    //
    "|en:\\emotion=TRANQUILITY "
    "\\item=Yawn Hello. I just woke up, and "
    "I am somewhat still sleepy, "
    "\\item=Yawn_01 I'm gonna leave you for a walk; "
    "I am sorry!";

const std::string EttsTest::SENTENCE_NERVOUS =
    "|es:\\emotion=NERVOUS "
    "Estoy un poco nerviosa, por eso hablo tan rapido, "
    "y es porque creo que me estan persiguiendo, socorro!!"
    //
    "|en:\\emotion=NERVOUS "
    "I am a bit nervous, that is why I speak so fast, "
    "and that's because I think they chase me, help!";

const std::string EttsTest::SENTENCE_EFFECTS =
    "|en:Hi! Let me introduce myself! "
    "I can say anything you want me to say. "
    "I can even say certain phrases with more expression. "
    "That's brilliant; or, That's brilliant! "
    "Amazing; or, Amazing! "
    "You can also put in various effects: "
    "\\item=Argh What was that?! "
    "\\item=Whistle_01 Who's there?! "
    "\\item=Snore_01 Weird! Very strange! Right. "
    "I'm off. Bye! Take care! Good luck!."
    //
    "|es:Hola! Dejame presentarme! "
    "Puedo decir cualquier cosa que me pides. "
    "Aun puedo decir algunas frases con mas espresividad. "
    "Increible; o, Increible! "
    "Perfecto; o, Perfecto! "
    "Tambien puedes anadir efectos: "
    "\\item=Argh Que era eso?! "
    "\\item=Whistle_01 Hay alguien?! "
    "\\item=Snore_01 Raro! Muy extraño. Vale. "
    "Pues me voy. Hasta luego, suerte!";

////////////////////////////////////////////////////////////////////////////////

class PrimitiveTest : public EttsTest {
public:
  PrimitiveTest(EttsPrimitiveInterface* prim,
                bool remove_loquendo_tags = true) :
    EttsTest(remove_loquendo_tags) {
    _prim = prim;
    if (prim != NULL)
      _prim->init();

    // set values
    _current_robot = ROBOT_MAGGIE;
    set_emotion(etts_msgs::Utterance::EMOTION_HAPPY);
    set_language(Translator::LANGUAGE_ENGLISH);

    run();
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_emotion(const utterance_utils::Emotion emot) {
    ROS_DEBUG("change_emotion()");
    _current_emotion = emot;
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void set_language(const Translator::LanguageId lang) {
    ROS_DEBUG("change_language(%s)",
                 Translator::get_language_full_name(lang).c_str());
    _current_language = lang;
  }
  virtual Translator::LanguageId get_language() const
  {return _current_language;}

  //////////////////////////////////////////////////////////////////////////////

  virtual void say(const std::string &text) {
    std::string clean_text = text;
    // remove loquendo tags
    if (_remove_loquendo_tags)
      utterance_utils::strip_metadata_tags(clean_text);
    // say that
    _prim->generateSpeech(clean_text,
                          _current_robot, _current_language, _current_emotion);
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void print_menu(const Translator::LanguageMap & languagmap) {
    EttsTest::print_menu(languagmap);
    // robot id
    printf("[robot]              10:Mopi 11:Maggie 12:Flory\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual std::string info_string() {
    std::ostringstream info_str;
    info_str << Translator::get_language_full_name(_current_language) << ", ";
    info_str << utterance_utils::emotion_to_full_string(_current_emotion) << ", ";
    info_str << robot_id_to_full_string(_current_robot);
    return info_str.str();
  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void int_choice(int choice) {
    EttsTest::int_choice(choice);
    // robots
    if (choice == 10)
      _current_robot = ROBOT_MOPI;
    else if (choice == 11)
      _current_robot = ROBOT_MAGGIE;
    else if (choice == 12)
        _current_robot = ROBOT_FLORY;

  }

  //////////////////////////////////////////////////////////////////////////////

  virtual void shutup() {_prim->shutUp();}
  virtual void shutup_immediatly() {_prim->shutUp();}
  virtual void pause() {_prim->pauseSpeech();}
  virtual void resume() {_prim->resumeSpeech();}

protected:
  //! the tested primitive
  EttsPrimitiveInterface* _prim;

  //! the current robot id
  RobotId _current_robot;

  //! the current emotion ID
  utterance_utils::Emotion _current_emotion;

  //! the current language ID
  Translator::LanguageId _current_language;


}; // end class PrimitiveTest

#endif // PRIMITIVE_TEST_H
