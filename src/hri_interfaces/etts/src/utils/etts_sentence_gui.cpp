// AD
#include "debug/error.h"
#include "communications/events/cevent.h"
#include "communications/shareMemory/memCortoPlazo.h"
#include "string/StringUtils.h"
#include "definitions/maggieoptions.h"
// QT
#include <QApplication>
#include <QLabel>
#include <QTextEdit>
// C
#include <signal.h>

class EttsSentenceGui : public QWidget {
public:
  EttsSentenceGui(QWidget *parent = 0) : QWidget(parent) {
    setWindowTitle("EttsSentenceGui");
    // create the label
    label = new QLabel(this);
    label->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    label->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
    label->setGeometry(2, 2, 640, 480);
    //label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    // set the color
    label->setStyleSheet("QLabel { "
                         "background-color: #CCFFCC;"
                         "}");
    label->setWordWrap(true);
    // set the font
    QFont sansFont("Helvetica [Cronyx]", 20);
    label->setFont(sansFont);

    // subscribe to events
    event_client.subscribe("SPEAKING_START", speaking_start_static, this);
    event_client.subscribe("SPEAKING_END", speaking_stop_static, this);

    // put a random text
    set_text("...");
  }

  //////////////////////////////////////////////////////////////////////////////

  inline void set_text(const std::string & sentence) {
    std::ostringstream sentence_and_color;
    sentence_and_color << "<font color=\"black\">"
                       << sentence << "</font>";
    label->setText(QString::fromStdString(sentence_and_color.str()));
  }

  //////////////////////////////////////////////////////////////////////////////

  static void speaking_start_static(void* aux, int /*sentence_timestamp*/) {
    EttsSentenceGui* this_ptr = (EttsSentenceGui*) aux;
    //this_ptr->set_text(StringUtils::cast_to_string(sentence_timestamp));
    // get the current sentence from mcp
    std::string sentence;
    int ret = this_ptr->mcp_client.get_current_item("ETTS_CURRENT_SENTENCE", sentence);
    if (ret < 0) {
      maggiePrint("get_current_item('ETTS_CURRENT_SENTENCE') returned an error!");
      sentence = "(could not read the sentence)";
    }
    // replace the "." with some breaklines
    StringUtils::convert_string_encoding_utf_to_iso(sentence);
    StringUtils::find_and_replace(sentence, ". ", ".\n");
    StringUtils::find_and_replace(sentence, "! ", ".\n");
    StringUtils::find_and_replace(sentence, "? ", ".\n");

    this_ptr->set_text(sentence);
  }

  //////////////////////////////////////////////////////////////////////////////

  static void speaking_stop_static(void* aux, int /*sentence_timestamp*/) {
    EttsSentenceGui* this_ptr = (EttsSentenceGui*) aux;
    this_ptr->set_text("...");
  }

private:
  CmemCortoPlazo mcp_client;
  CEventManager event_client;
  // Qt objects
  QLabel* label;
}; // end class EttsSentenceGui

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  maggieDebug2("main()");

  QApplication app(argc, argv);

#if AD_USE_ROS
  ros::init(argc, argv, "EttsSentenceGui", ros::init_options::NoSigintHandler);
#endif // AD_USE_ROS

  EttsSentenceGui gui;
  gui.show();

#if AD_USE_ROS
  ros::AsyncSpinner spinner(1);
  spinner.start();
#endif // AD_USE_ROS

  app.exec();

#if AD_USE_ROS
  spinner.stop();
#endif // AD_USE_ROS

  return 0;
}
