#include "debug/debug.h"
#include "time/timer.h"
#include "utterance_utils.h"

void test_strip_metadata_tags(std::string sentence_to_clean) {
  maggiePrint("test_strip_metadata_tags('%s')", sentence_to_clean.c_str());
  Timer timer;
  utterance_utils::strip_metadata_tags(sentence_to_clean);
  //utterance_utils::get_all_metadata_tags(sentence_to_clean, sentence_to_clean, NULL, false);
  maggiePrint("time: %g ms, sentence_to_clean:%s",
              timer.time(), sentence_to_clean.c_str());

}

////////////////////////////////////////////////////////////////////////////////

void test_replace_natural_langugage_tags(std::string snt,
                                         const Translator::LanguageId lang) {
  std::cout << std::endl << std::endl;
  maggiePrint("test_replace_natural_langugage_tags('%s')", snt.c_str());
  Translator::LanguageMap map;
  Translator::build_languages_map(map);
  utterance_utils::replace_natural_langugage_tags(snt, lang, map);
  maggiePrint("snt:%s", snt.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void test_get_all_metadata_tags(const std::string snt) {
  std::cout << std::endl << std::endl;
  maggiePrint("test_get_all_metadata_tags('%s')", snt.c_str());
  std::vector<utterance_utils::ValuedTag> tags;
  std::string sentence_clean;
  Timer timer;
  utterance_utils::get_all_metadata_tags(snt, sentence_clean, &tags);

  maggiePrint("time: %g ms, sentence_clean:'%s'",
              timer.time(), sentence_clean.c_str());
  for (unsigned int tag_idx = 0; tag_idx < tags.size(); ++tag_idx) {
    maggiePrint("tag %i:'%s' = '%s'", tag_idx,
                tags.at(tag_idx).first.c_str(),
                tags.at(tag_idx).second.c_str());

  } // end loop tag_idx
}

////////////////////////////////////////////////////////////////////////////////

void test_contains_emotion(const std::string snt) {
  std::cout << std::endl << std::endl;
  maggiePrint("test_contains_emotion('%s')", snt.c_str());
  std::vector<utterance_utils::ValuedTag> tags;
  std::string sentence_clean;
  utterance_utils::Emotion emotion_out;

  Timer timer;
  utterance_utils::get_all_metadata_tags(snt, sentence_clean, &tags);
  bool success = utterance_utils::tags_contain_emotion_switch(tags, emotion_out);
  maggiePrint("time: %g ms, sentence_clean:'%s', success:%i, emotion_out:'%s'",
              timer.time(), sentence_clean.c_str(), success,
              utterance_utils::emotion_to_full_string(emotion_out).c_str());
}

////////////////////////////////////////////////////////////////////////////////

int main(int, char**) {
  maggieDebug2("main()");

  //  test_replace_natural_langugage_tags
  //      ("Yes, \\NLG=OK let us do it \\NLG=NOT_INPUT",
  //       Translator::LANGUAGE_ENGLISH);
  //  test_replace_natural_langugage_tags
  //      ("Si, \\NLG=OK la hacemos \\NLG=NOT_INPUT",
  //       Translator::LANGUAGE_SPANISH);
  //  test_replace_natural_langugage_tags
  //      ("Oui, \\NLG=OK on le fait \\NLG=NOT_INPUT",
  //       Translator::LANGUAGE_FRENCH);

  test_get_all_metadata_tags("\\param1=v1 word1 \\param2 word2 \\param3=v3");
  test_get_all_metadata_tags("\\data1=v1 word1 \\data2");
  test_get_all_metadata_tags("\\speed=120wm I speak fast \\pause=500 . "
                        "\\speed=80wm Now slow.");

  test_get_all_metadata_tags("Si quieres que haga alguna cosa solo tienes que "
                        "decirmelo. \\pause=500 Mientras ve voy a dormir. "
                        "\\item=Breath_03");

  test_strip_metadata_tags("Si quieres que haga alguna cosa solo tienes que "
                           "decirmelo. \\pause=500 Mientras ve voy a dormir. "
                           "\\item=Breath_03");

  test_contains_emotion("Hello \\emotion=Happy");
  test_contains_emotion("\\emotion=Sad Hello2 \\emotion=Happy");
  test_contains_emotion("\\emotion=Sad Hello2 \\emotion=Happy");

  return 0;
}
