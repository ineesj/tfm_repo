/*!
   @file test_speechSnippets_random.cpp
   @brief A simple test of the ad::tts::SpeechSnippets class which prints random snippets
   Prints all the keys and asks the user to introduce a key to print one of its random values
   @author Victor Gonzalez-Pacheco   vgonzale@ing.uc3m.es
   @version 1.0
   @date june, 2011
*/

#include "utils/speech_snippets/speech_snippets.h"
using namespace ad::tts;
using std::cout;
using std::cin;
using std::endl;

int main (){
    std::string option;
    std::string randomSnippet;

    //maggieDebug2 ("Printing All the snippets");
    ad::tts::SpeechSnippets::printAll();

    while (1){
        cout << endl;
        cout << "Available keys:" << endl;
        ad::tts::SpeechSnippets::printKeys();

        cout << "Choose an option to show a random snippet "
                "or press Ctrl-C to exit." << endl;
        cout << "Option: ";
        cin >> option;
        cout << endl;
        randomSnippet = ad::tts::SpeechSnippets::getRandomSnippet(option);
        cout << "Random snippet:" << randomSnippet << cout;
        cout << endl;
    }

    return 0;
}
