/*!
   @file speechsnippets.h
   @brief header for the ad::tts::SpeechSnippets class
   @author Victor Gonzalez-Pacheco   vgonzale@ing.uc3m.es
   @version 0.1
   @date June, 2011
*/


#ifndef SPEECHSNIPPETS_H
#define SPEECHSNIPPETS_H

#include <string>
#include <map>
#include <utility>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <sys/time.h>


namespace ad{
    namespace tts{

        /** A pair containing the types used by ad::tts::snippetMap **/
        typedef std::pair <std::string, std::string> snippetPair;
        /** A map <std::string, std::string> */
        typedef std::multimap <std::string, std::string> snippetMap;
        /** An iterator of an ad::tts::snippetMap **/
        typedef snippetMap::const_iterator snIterator;
        /** A pair of snippetMap iterators */
        typedef std::pair <snIterator, snIterator> snIteratorPair;



        /**
         * @class Provides some static functions to genereate random text snippets from some semantic keys
         */
        class SpeechSnippets
        {
        public:

            typedef std::string semanticKey;


            static const semanticKey NOT_LISTENED;
            static const semanticKey NOT_INPUT;
            static const semanticKey YES;
            static const semanticKey NO;
            static const semanticKey OK;
            static const semanticKey BYE;
            static const semanticKey HELLO;
            static const semanticKey JOKE;
            static const semanticKey NO_IDEA;
            static const semanticKey BIRTHDAY;
            static const semanticKey NO_OBJ;
            static const semanticKey ROUTINE;
            static const semanticKey MULTIM_LOCAT;
            static const semanticKey MULTIM_PERS;
            static const semanticKey MULTIM_GENER;

            /** \brief constructor **/
            SpeechSnippets();

            /** \brief destructor **/
            ~SpeechSnippets();

            /*!
             \brief returns a random text snippet associated to the entered parameter
             \param semantic the text to be randomized
             \return A random text snippet if the parameter is found in the snippets set. null if it is not found
             \see ad::tts::SpeechSnippets::createMap() for the available text snippets and their semantics
            */
            static std::string getRandomSnippet(std::string semantic);

            /*!
             * \brief Prints all the text snippets with its semantic keys
             */
            static void printAll();

            /*!
             * \brief Prints all the keys of the snippets map
             */
            static void printKeys();

            /*!
             * \brief Prints all the snippets of a single semantic key
             * \param key the key of the snippets to print
             */
            static void printSnippets(std::string key);

            /**
             * A map with all the snippets
             */
            static const snippetMap SNIPPETS;// = createMap();

        private:
            static const snippetMap createMap();


        };

    } // End namespace utils
} // End namespace ad


#endif // SPEECHSNIPPETS_H
