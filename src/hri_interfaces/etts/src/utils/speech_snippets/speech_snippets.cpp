/*!
   @file speechsnippets.h
   @brief implementation the ad::tts::SpeechSnippets class
   @author Victor Gonzalez-Pacheco   vgonzale@ing.uc3m.es
   @version 0.1
   @date June, 2011
*/


#include "utils/speech_snippets/speech_snippets.h"
#include <ros/package.h>

using namespace ad::tts;

//para iniciarlizar las constantes semanticas declaradas eh el .h
const SpeechSnippets::semanticKey SpeechSnippets::NOT_LISTENED = "NOT_LISTENED";
const SpeechSnippets::semanticKey SpeechSnippets::YES = "YES";
const SpeechSnippets::semanticKey SpeechSnippets::NO =  "NO";
const SpeechSnippets::semanticKey SpeechSnippets::OK = "OK";
const SpeechSnippets::semanticKey SpeechSnippets::NOT_INPUT = "NOT_INPUT";
const SpeechSnippets::semanticKey SpeechSnippets::BYE = "BYE";
const SpeechSnippets::semanticKey SpeechSnippets::HELLO = "HELLO";
const SpeechSnippets::semanticKey SpeechSnippets::JOKE = "JOKE";
const SpeechSnippets::semanticKey SpeechSnippets::NO_IDEA = "NO_IDEA";
const SpeechSnippets::semanticKey SpeechSnippets::BIRTHDAY = "BIRTHDAY";
const SpeechSnippets::semanticKey SpeechSnippets::NO_OBJ = "NO_OBJ";
const SpeechSnippets::semanticKey SpeechSnippets::ROUTINE = "ROUTINE";
const SpeechSnippets::semanticKey SpeechSnippets::MULTIM_LOCAT = "MULTIM_LOCAT";
const SpeechSnippets::semanticKey SpeechSnippets::MULTIM_PERS = "MULTIM_PERS";
const SpeechSnippets::semanticKey SpeechSnippets::MULTIM_GENER = "MULTIM_GENER";

const snippetMap SpeechSnippets::SNIPPETS = SpeechSnippets::createMap();

////////////////////////////////////////////////////////////////////////////////

SpeechSnippets::SpeechSnippets(){
    //ctor
}

////////////////////////////////////////////////////////////////////////////////

SpeechSnippets::~SpeechSnippets(){
    //dtor
}

////////////////////////////////////////////////////////////////////////////////

const snippetMap SpeechSnippets::createMap(){

    snippetMap snippets;
    std::string s;

    //// NOT LISTENED ////
    s = "|es:Como dices? Repite por favor."
        "|en:What did you say? Please repeat it.";
    snippets.insert(snippetPair(NOT_LISTENED, s));


    s = "|es:Dilo un poco mas claro y a un volumen normal."
        "|en:Say it a bit clearer and at a normal volume, "
                     "please";
    snippets.insert (snippetPair(NOT_LISTENED,s));

    s = "|es:Repitemelo por favor"
        "|en:Could you repeat it, please?";
    snippets.insert (snippetPair(NOT_LISTENED, s));


    s = "|es:Como? repite."
        "|en:sorry?";
    snippets.insert (snippetPair(NOT_LISTENED, s));


    //// NOT INPUT ////
    s = "es:Hay alguien?"
        "|en:Anyone?";
    snippets.insert(snippetPair(NOT_INPUT, s));

    s = "es:\\item=Throat "
        "|en:\\item=Throat ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:Me aburro"
        "|en:I am bored";
    snippets.insert (snippetPair(NOT_INPUT,s));

    s = "es:Nadie me hace caso"
        "|en:Nobody plays with me";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Mhm_02 "
        "|en:\\item=Mhm_02 ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Mhm_01 "
        "|en:\\item=Mhm_01 ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Throat_03 "
        "|en:\\item=Throat_03 ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Cough "
        "|en:\\item=Cough ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Breath "
        "|en:\\item=Breath ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Yawn "
        "|en:\\item=Yawn ";
    snippets.insert (snippetPair(NOT_INPUT, s));

    s = "es:\\item=Smack "
        "|en:\\item=Smack ";
    snippets.insert (snippetPair(NOT_INPUT, s));


    //// YES ////
    s = "|es:si."
        "|en:yes.";
    snippets.insert (snippetPair(YES, s));

    s = "|es:sip."
        "|en:yep.";
    snippets.insert (snippetPair(YES, s));

    s = "|es:aja."
        "|en:aha.";
    snippets.insert (snippetPair(YES, s));

    s = "|es:siii."
        "|en:yeah.";
    snippets.insert (snippetPair(YES, s));

    //// NO ////
    s = "|es:no."
        "|en:no.";
    snippets.insert (snippetPair(NO, s));

    s = "|es:nop."
        "|en:nop.";
    snippets.insert (snippetPair(NO, s));

    //// OK ////
    s = "|es:okey."
        "|en:OK.";
    snippets.insert (snippetPair(OK, s));

    s = "|es:vale."
        "|en:okidoky.";
    snippets.insert (snippetPair(OK, s));

    s = "|es:perfecto."
        "|en:fine.";
    snippets.insert (snippetPair(OK, s));

    s = "|es:de acuerdo."
        "|en:alright.";
    snippets.insert (snippetPair(OK, s));

    //// BYE ////
    s = "|es:adios."
        "|en:bye.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Hasta pronto."
        "|en:Good bye.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Nos vemos pronto."
        "|en:cheers.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Hasta otra."
        "|en:See you.";
    snippets.insert (snippetPair(BYE, s));

    //// BYE ////
    s = "|es:adios."
        "|en:bye.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Hasta pronto."
        "|en:Good bye.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Nos vemos pronto."
        "|en:cheers.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Hasta otra."
        "|en:See you.";
    snippets.insert (snippetPair(BYE, s));

    s = "|es:Adios."
        "|en:Bye. Take care.";
    snippets.insert (snippetPair(BYE, s));

    //// HELLO ////
    s = "|es:Hola!"
        "|en:Hi!.";
    snippets.insert (snippetPair(HELLO, s));

    s = "|es:Hola. Estoy encantado de verte."
        "|en:Hello.";
    snippets.insert (snippetPair(HELLO, s));

    s = "|es:Que alegria verte."
        "|en:I am glad to see you.";
    snippets.insert (snippetPair(HELLO, s));

    //// JOKE ////
    s = "|es:Era se una vez un jugador de futbol tan malo, tan malo... que metio un gol y en la repeticion lo fallo. ";
    snippets.insert (snippetPair(JOKE, s));

    s = "|es:Que es un bumerang que no vuelve?  \\pause = 1000 Un palo. ";
    snippets.insert (snippetPair(JOKE, s));

    s = "|es:que hace un señor gateando en un supermercado? \\pause=1000 buscando precios bajos.";
    snippets.insert (snippetPair(JOKE, s));


    //// NO_IDEA ////
    s = "|es:No te puedo decir nada al respecto. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:No tengo informacion al respecto. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:De eso no se nada.";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:Concretamente de eso, no tengo ni idea. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:No se nada. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:Me pillas en fuera de juego. Sobre eso no tengo ni idea.";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:Me pillas en blanco. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:No se de que me hablas. ";
    snippets.insert (snippetPair(NO_IDEA, s));

    s = "|es:No te se decir nada. No conozco.";
    snippets.insert (snippetPair(NO_IDEA, s));


    //// BIRTHDAY ////
    s = "|es:# cumple hoy $ años.";
    snippets.insert (snippetPair(BIRTHDAY, s));

    s = "|es:Hoy es el cumpleaños de #.Cumple $ años. ";
    snippets.insert (snippetPair(BIRTHDAY, s));

    s = "|es:Felicita a # que es su cumpleaños y cumple $ años.";
    snippets.insert (snippetPair(BIRTHDAY, s));


    //// NO_OBJ ////
    s = "|es:No se dónde está ese objeto. ";
    snippets.insert (snippetPair(NO_OBJ, s));

    s = "|es:No encuentro el objeto que me has dicho";
    snippets.insert (snippetPair(NO_OBJ, s));


    //// ROUTINE ////
    s = "|es:Es hora de #.";
    snippets.insert (snippetPair(ROUTINE, s));

    s = "|es:Ahora toca #. ";
    snippets.insert (snippetPair(ROUTINE, s));

    s = "|es:Hay que #, que ya es la hora.";
    snippets.insert (snippetPair(ROUTINE, s));


    //// MULTIM_LOCAT ////
    s = "|es:Mira, esta foto está hecha en #.";
    snippets.insert (snippetPair(MULTIM_LOCAT, s));

    s = "|es:Esta es de  #. ";
    snippets.insert (snippetPair(MULTIM_LOCAT, s));

    s = "|es:Me gusta esta foto de #.";
    snippets.insert (snippetPair(MULTIM_LOCAT, s));


    //// MULTIM_PERS ////
    s = "|es: Aquí aparece #.";
    snippets.insert (snippetPair(MULTIM_PERS, s));

    s = "|es: En está foto está #.";
    snippets.insert (snippetPair(MULTIM_PERS, s));

    s = "|es: Mira que bien sale #.";
    snippets.insert (snippetPair(MULTIM_PERS, s));


    //// MULTIM_GENER ////
    s = "|es:Me gusta la foto que te estoy enseñando ahora.";
    snippets.insert (snippetPair(MULTIM_LOCAT, s));

    s = "|es:Has visto que foto tan bonita.";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    s = "|es: ¿Te gustan las fotos?";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    s = "|es: Mira esta";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    s = "|es: Esta no la había visto.";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    s = "|es: Ay, que bonita.";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    s = "|es: A ver cual es la siguiente foto.";
    snippets.insert (snippetPair(MULTIM_GENER, s));

    return snippets;
}

////////////////////////////////////////////////////////////////////////////////

// What this function does is the following.
// First checks if the entered parameter is in the snippetMap
// After it, retrieves the first ocurrence of semantic in the snippetMap
// and navigates among a random number of occurrences of the Map.
// finally, it returns the last navigated occurence.
std::string SpeechSnippets::getRandomSnippet(std::string semantic){
    if (SNIPPETS.find(semantic) != SNIPPETS.end()){

        timeval timeStamp;
        gettimeofday( &timeStamp, NULL );
        srand(timeStamp.tv_usec); // seeding the random number generator

        std::pair <snippetMap::const_iterator, snippetMap::const_iterator> range;
        range = SNIPPETS.equal_range(semantic);
        snippetMap::const_iterator iter = range.first;

        // getting the distance between both iterators
        int distance = std::distance (range.first, range.second);
        //printf("Distance: %d\n",distance);
        //printf("Aleatorio: %d\n", rand());
        //printf("Resultado: %d\n",(rand()%distance));
        int randomNumber = (rand() % distance);
        // navigating from the first snippet to a random one
        for (int i=0 ; i < randomNumber  ; i++ ){
            iter++;
        }

        return iter->second; // returning the random snippet
        //        return range.first.second;
        //        std::advance( SNIPPETS.lower_bound(semantic),
        //                      rand() % (int)SNIPPETS.count(semantic) );
        //        return((*iter).second); // returning the random snippet
    }

    return "";
}

////////////////////////////////////////////////////////////////////////////////

void SpeechSnippets::printAll(){
    //    snIterator it;
    snippetMap::const_iterator it;
    // show content:
    for ( it = SNIPPETS.begin() ; it != SNIPPETS.end(); it++ ){
        std::cout << (*it).first << " => " << (*it).second << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////

void SpeechSnippets::printKeys(){
    snippetMap::const_iterator it;
    std::string key = "NULL";
    // show content:
    for ( it = SNIPPETS.begin() ; it != SNIPPETS.end(); it++ ){
//        if (key.compare((*it).first) != 0){
            std::cout << (*it).first << std::endl;
//            key = (*it).first;
//        }
    }

}

////////////////////////////////////////////////////////////////////////////////

void SpeechSnippets::printSnippets(std::string key){
    if (SNIPPETS.find(key) != SNIPPETS.end()){
        snIteratorPair range;
        range = SNIPPETS.equal_range(key);
        //        snIterator keyIterator = range.first;

        for (snIterator it=range.first; it!=range.second; ++it){
            std::cout << " " << (*it).second << std::endl;
        }
        std::cout << std::endl;
    } else {
        std::cout << "Key: " + key + " doesn't exist in the SNIPPETS set" << std::endl;
    }
}


