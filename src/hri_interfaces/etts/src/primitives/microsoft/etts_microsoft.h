/*!
 * \file EttsMicrosoft.cpp
 *
 * Para sintitezar voz usanod el servicio web de Microsoft TTS
 *
 * \date Dec 9, 2010
 * \author Arnaud Ramey
 */

#include "api/etts_api.h"
#include "debug/debug2.h"
#include "system/system_utils.h"
#include "map/cached_files_map.h"
#include "definitions/etts_primitive_interface.h"

////////////////////////////////////////////////////////////////////////////////

class EttsMicrosoft: public EttsPrimitiveInterface {
public:

  /*! Constructor */
  EttsMicrosoft() : _wav_map(CACHE_WAV_CSV_FILE) {
    debugPrintf("EttsMicrosoft ctor\n");
    Translator::build_languages_map(languages_map);
  }

  ////////////////////////////////////////////////////////////////////////////////


  /*! Destructor */
  ~EttsMicrosoft() {
    debugPrintf("EttsMicrosoft dtor\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Inicializa  sesión*/
  void init() {
    debugPrintf("init()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Finaliza sesión */
  void stop() {
    debugPrintf("stop()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Genera la voz usando las primitivas de microsft */
  int generateSpeech(const std::string & text,
                     const RobotId robot_id,
                     const Translator::LanguageId wanted_language,
                     const utterance_utils::Emotion emotion);

  ////////////////////////////////////////////////////////////////////////////////

  /*! Para parar la sintesis de la frase en curso */
  void shutUp() {
    debugPrintf("shutUp() en primitivas de EttsMicrosoft\n");
    system_utils::exec_system("killall mplayer");
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Para pausar la síntesis de voz */
  void pauseSpeech() {
    debugPrintf("pauseSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Para reanudar la síntesis de voz pausada */
  void resumeSpeech() {
    debugPrintf("resumeSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  /*! Para establecer un volumen de la síntesis de voz */
  void setVolume( int vol) {
    debugPrintf("changeVolume(%i)\n", vol);
    // does nothing
  }

private:
  const static std::string WAV_FILENAME;
  const static std::string API_KEY;
  Translator::LanguageMap languages_map;
  const static std::string CACHE_WAV_CSV_FILE;
  CachedFilesMap _wav_map;
};
