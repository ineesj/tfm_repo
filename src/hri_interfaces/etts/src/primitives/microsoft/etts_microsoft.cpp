#include "etts_microsoft.h"
#include "string/find_and_replace.h"

const std::string EttsMicrosoft::WAV_FILENAME = "/tmp/etts_microsoft.wav";
const std::string EttsMicrosoft::API_KEY = "6844AE3580856D2EC7A64C79F55F11AA47CB961B";
const std::string EttsMicrosoft::CACHE_WAV_CSV_FILE
= ros::package::getPath("etts")+ "/data/microsoft_wav_cache/index.csv";

////////////////////////////////////////////////////////////////////////////////

int EttsMicrosoft::generateSpeech(const std::string & text,
                                  const RobotId robot_id,
                                  const Translator::LanguageId wanted_language,
                                  const utterance_utils::Emotion emotion) {
  debugPrintf("generateSpeech(texto:'%s', robot_id:%s, current_language:%s, emotion:%s)\n",
              text.c_str(),
              robot_id_to_full_string(robot_id).c_str(),
              Translator::get_language_full_name(wanted_language).c_str(),
              utterance_utils::emotion_to_full_string(emotion).c_str());

  std::string country_domain = languages_map.at(wanted_language);
  std::string curr_wav_filename = WAV_FILENAME;
  CachedFilesMap::Key key = country_domain + ":" + text;
  if (_wav_map.has_cached_file(key)
      && _wav_map.get_cached_file(key, curr_wav_filename)) {
    printf("EttsMicrosoft: sentence '%s' was already in cache:'%s'\n",
           text.c_str(), curr_wav_filename.c_str());
  }
  else {
    // clean the sentence
    std::string sentence_clean = text;
    StringUtils::find_and_replace(sentence_clean, " ", "%20");
    StringUtils::find_and_replace(sentence_clean, "\n", "%0A");
    // exceptions not respecting the country domains
    // available at:
    // http://api.microsofttranslator.com/V2/Http.svc/GetLanguagesForSpeak?appId=6844AE3580856D2EC7A64C79F55F11AA47CB961B
    if (wanted_language == Translator::LANGUAGE_JAPANESE)
      country_domain = "ja";

    if (wanted_language == Translator::LANGUAGE_PORTUGUESE)
      country_domain = "pt-pt";
    // http://api.microsofttranslator.com/v2/Http.svc/Speak?appId=6844AE3580856D2EC7A64C79F55F11AA47CB961B&text=Hello,%20my%20friend!&language=en
    std::ostringstream wget_command;
    wget_command << "wget ";
    wget_command << "\"http://api.microsofttranslator.com/v2/Http.svc/Speak?";
    wget_command << "appId=" << API_KEY;
    wget_command << "&language=" << country_domain;
    wget_command << "&text=" << sentence_clean;
    wget_command << "\"";
    wget_command << " --output-document=" << WAV_FILENAME;
    wget_command << " --no-verbose";
    system_utils::exec_system(wget_command.str());
    _wav_map.add_cached_file(key, WAV_FILENAME);
  } // end if no cache

  // read it with VLC or mplayer
  std::ostringstream play_command;
  play_command << "mplayer " << curr_wav_filename;
  play_command << "  -msglevel all=1";
  // increase volume
  play_command << "  -softvol -softvol-max 800 -volume 100";
  system_utils::exec_system(play_command.str());
  return 0;
}
