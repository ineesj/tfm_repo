#include "etts_google.h"
#include "utils/primitive_test.h"

void test_chunks(const std::string text, const int & max_size) {
  std::cout << std::endl;
  debugPrintf("test_chunks('%s', max_size:%i)\n", text.c_str(), max_size);

  std::vector<std::string> chunks;
  EttsGoogle::cut_string_into_chunks2(text, max_size, chunks);
  for (unsigned int idx = 0; idx < chunks.size(); ++idx) {
    debugPrintf("#%i (size:%i): '%s'\n", idx, chunks[idx].size(), chunks[idx].c_str());
  } // end loop idx
}

////////////////////////////////////////////////////////////////////////////////

int main() {
  debugPrintf("main()\n");
//  test_chunks("Sentence1. "
//              "Sentence2. "
//              "Sentence3. "
//              "Sentence4. ", 10);

//  test_chunks("aaaa b cc ddddd", 6);

  EttsGoogle fest;
  PrimitiveTest test(&fest);
  return 0;
}


