#include "etts_google.h"

const std::string::size_type EttsGoogle::MAX_CHUNK_SIZE = 100;
const std::string EttsGoogle::MP3_FILENAME = "/tmp/EttsGoogle.mp3";
const std::string EttsGoogle::CACHE_MP3_CSV_FILE
= ros::package::getPath("etts")+ "/data/google_mp3_cache/index.csv";

//////////////////////////////////////////////////////////////////////////////

static inline bool cut_with_string_at_maxpos(const std::string & text,
                                             const std::string & delimiter,
                                             const std::string::size_type & max_size,
                                             std::vector<std::string> & chunks) {
  chunks.clear();
  // if string short enough, do nothing
  if (text.size() <= max_size) {
    chunks.push_back(text);
    return true;
  }

  // first search near the half
  std::string::size_type tag_pos = text.rfind(delimiter,
                                              max_size);

  // if there is no tag, fail miserably
  if (tag_pos == std::string::npos)
    return false;

  // push both halves
  chunks.push_back(text.substr(0, tag_pos));
  if (tag_pos + delimiter.size() < text.size())
    chunks.push_back(text.substr(tag_pos + delimiter.size()));
  return true;
}

//////////////////////////////////////////////////////////////////////////////

//! try to cut the text into chunks by dichotomy
bool EttsGoogle::cut_string_into_chunks2(const std::string & text,
                                         const std::string::size_type & max_size,
                                         std::vector<std::string> & chunks) {
  std::string text_left = text;
  std::vector<std::string> minichunks;
  while(true) {
    minichunks.clear();
    bool success = cut_with_string_at_maxpos(text_left, ". ", max_size, minichunks);
    if (!success)
      success = cut_with_string_at_maxpos(text_left, ", ", max_size, minichunks);
    if (!success)
      success = cut_with_string_at_maxpos(text_left, " ", max_size, minichunks);
    if (!success) {
      debugPrintf("Could not cut the chunk '%s'\n", text_left.c_str());
      return false;
    }
    // add the first chunk
    chunks.push_back(minichunks.front());
    if (minichunks.size() == 1) // only one word -> we finished!
      return true;
    text_left = minichunks.back();
  } // end while true
} // end cut_string_into_chunks2
