/*!
 * \file EttsGoogle.cpp
 *
 * Cf http://weston.ruter.net/projects/google-tts/
 * Send a petition such as
 * http://translate.google.com/translate_tts?tl=en&q=text
 *
 * Esta primitiva sintetiza voz usando el web service de goolge TTS. Para ello se hace una petición (wget) y se reproduce el fichero de audio que devuelve la petición
 * \date April 2011
 * \author Arnaud Ramey
 */

#include "definitions/etts_primitive_interface.h"
#include "debug/debug2.h"
#include "string/StringUtils.h"
#include "system/system_utils.h"
#include "map/cached_files_map.h"
#include "api/etts_api.h"

////////////////////////////////////////////////////////////////////////////

class EttsGoogle: public EttsPrimitiveInterface {
public:
  //! maximum chunk (this size is still OK, but 1 more is not!)
  const static std::string::size_type MAX_CHUNK_SIZE;

  /*! Constructor */
  EttsGoogle() : _mp3_map(CACHE_MP3_CSV_FILE) {
    debugPrintf("EttsGoogle ctor\n");
    Translator::build_languages_map(languages_map);
  }


  ////////////////////////////////////////////////////////////////////////////

  /*! Desctructor*/
  ~EttsGoogle() {
    debugPrintf("EttsGoogle dtor\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Inicializa la sesión */
  void init() {
    debugPrintf("init()\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  /*! Finaliza la sesion*/
  void stop() {
    debugPrintf("stop()\n");
    // does nothing
  }

  //! try to cut the text into chunks by dichotomy
  static bool cut_string_into_chunks2(const std::string & text,
                                      const std::string::size_type & max_size,
                                      std::vector<std::string> & chunks);

  ////////////////////////////////////////////////////////////////////////////

  /*! Genera la locución de voz usando el servicio web de google
    @param texto a sintetizar
    @param robot_id el robot que va a sintetizar la voz (en este caso da igual)
    @param wanted_language lenguaje en el que se va a sinteziar
    @param emotion con la que se sintetiza (en este caso da igual, siempre la misma)
    */
  int generateSpeech(const std::string & text,
                     const RobotId robot_id,
                     const Translator::LanguageId wanted_language,
                     const utterance_utils::Emotion emotion) {
    debugPrintf("generateSpeech(texto:'%s', robot_id:%s, current_language:%s, emotion:%s)\n",
                text.c_str(),
                robot_id_to_full_string(robot_id).c_str(),
                Translator::get_language_full_name(wanted_language).c_str(),
                utterance_utils::emotion_to_full_string(emotion).c_str());
    std::string country_domain = "en";

    // check if we have this language in the languages_map.
    if (languages_map.find(wanted_language) != languages_map.end()) {
      country_domain = languages_map.at(wanted_language);
    }
    // persian exception -> use arabic
    if (wanted_language == Translator::LANGUAGE_PERSIAN)
      country_domain = "ar";

    /*
     *cut the sentence in chunks of less than MAX_CHUNK_SIZE chars
     */
    std::vector<std::string> chunks;
    bool success = cut_string_into_chunks2(text, MAX_CHUNK_SIZE, chunks);

    if (!success) {
      debugPrintf("The sentence '%s' is too long, it should be less than "
                  "%i characters.\n", text.c_str(), MAX_CHUNK_SIZE);
      return -1;
    }

    // say each chunk
    _need_exit_from_chunk_loop = false;
    for (std::string::size_type idx = 0; idx < chunks.size(); ++idx) {
      say_short_chunk(country_domain, chunks.at(idx), emotion);
      if (_need_exit_from_chunk_loop)
        break;
    } // end loop idx

    return 0;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para para la locución de voz */
  void shutUp() {
    debugPrintf("shutUp() en primitivas de Google\n");
    system_utils::exec_system("killall /usr/bin/vlc");
    _need_exit_from_chunk_loop = true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para pausar la voz */
  void pauseSpeech() {
    debugPrintf("pauseSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para reanudar la voz */
  void resumeSpeech() {
    debugPrintf("resumeSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para establecer el nuevo volumen */
  void setVolume( int vol) {
    debugPrintf("setVolume(%i)\n", vol);
    // does nothing
  }

private:
  const static std::string MP3_FILENAME;
  Translator::LanguageMap languages_map;
  bool _need_exit_from_chunk_loop;
  const static std::string CACHE_MP3_CSV_FILE;
  CachedFilesMap _mp3_map;

  //! for sentence less than MAX_CHUNK_SIZE characters
  void say_short_chunk(const std::string & country_domain,
                       const std::string & sentence,
                       const utterance_utils::Emotion emotion) {
    debugPrintf("say_short_chunk('%s':'%s' (size:%i))\n",
                country_domain.c_str(),
                sentence.c_str(), sentence.size());
    std::string curr_mp3_filename = MP3_FILENAME;
    CachedFilesMap::Key key = country_domain + "_" + sentence;
    if (_mp3_map.has_cached_file(key)
        && _mp3_map.get_cached_file(key, curr_mp3_filename)) {
      printf("EttsGoogle: sentence '%s' was already in cache:'%s'\n",
             sentence.c_str(), curr_mp3_filename.c_str());
    }
    else {
      // clean the sentence
      std::string sentence_clean = sentence;
      StringUtils::find_and_replace(sentence_clean, " ", "%20");
      StringUtils::find_and_replace(sentence_clean, "\n", "%0A");

      // build the url
      std::ostringstream wget_command;
      wget_command << "wget ";
      wget_command << "\"http://translate.google.com/translate_tts?";
      wget_command << "tl=" << country_domain;
      wget_command << "&q=" << sentence_clean;
      wget_command << "&ie=UTF-8&total=1&idx=0&client=uc3m";
      wget_command << "\"";
      wget_command << " --output-document=" << MP3_FILENAME;
      wget_command << " --no-verbose";
      // trick Google API with no referrer
      // cf http://www.askapache.com/dreamhost/wget-header-trick.html
      wget_command << " --referer=\"\"";
      wget_command << " --user-agent=\"Mozilla/5.0 (Windows; U; Windows NT 5.1; "
                 "en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\"";

      // get the content
      system_utils::exec_system(wget_command.str());
      _mp3_map.add_cached_file(key, MP3_FILENAME);
    } // end if no cache

    std::ostringstream play_cmd;
#if 0
    // read it with VLC or mplayer
    play_cmd.str("");
    play_cmd << "cvlc " << curr_mp3_filename;
    play_cmd << " --verbose 0";
    play_cmd << " --play-and-exit";
    // speed up the reading as Google TTS got really slow
    if (emotion == etts_msgs::Utterance::EMOTION_NERVOUS)
      play_cmd << " --rate 1.8";
    else
      play_cmd << " --rate 1.4";

#else
    // read it with mplayer
    play_cmd.str("");
    play_cmd << "mplayer " << curr_mp3_filename << " -really-quiet ";
    // speed up the reading as Google TTS got really slow
    if (emotion == etts_msgs::Utterance::EMOTION_NERVOUS)
      play_cmd << " -speed 1.8 -af scaletempo";
    else
      play_cmd << " -speed 1.4 -af scaletempo";
    play_cmd << " 2> /dev/null ";
#endif

    system_utils::exec_system(play_cmd.str());
  } // end say_short_chunk();
}; // end class EttsGoogle
