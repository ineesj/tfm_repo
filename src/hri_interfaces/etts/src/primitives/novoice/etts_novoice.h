/*!
 * \file EttsNovoice.cpp
 *
 * Simplemente imprime en pantalla lo que se debería sintetizar por voz (no se sintetiza)
 *
 * \date Dec 9, 2010
 * \author Arnaud Ramey
 */

#include "api/etts_api.h"
#include "definitions/etts_primitive_interface.h"
#define DEBUG
#include "debug/debug2.h"

////////////////////////////////////////////////////////////////////////////////

class EttsNovoice: public EttsPrimitiveInterface {
public:
  EttsNovoice() {
    debugPrintf("EttsNovoice ctor\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  ~EttsNovoice() {
    debugPrintf("EttsNovoice dtor\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  void init() {
    debugPrintf("init()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  void stop() {
    debugPrintf("stop()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  int generateSpeech(const std::string & text,
                     const RobotId robot_id,
                     const Translator::LanguageId wanted_language,
                     const utterance_utils::Emotion emotion) {
    debugPrintf("generateSpeech(texto:'%s', robot_id:%s, current_language:%s, emotion:%s)\n",
                 text.c_str(),
                 robot_id_to_full_string(robot_id).c_str(),
                 Translator::get_language_full_name(wanted_language).c_str(),
                 utterance_utils::emotion_to_full_string(emotion).c_str());

    printf("\n\nnovoice: %s\n\n\n", text.c_str());
    // wait a bit for time to be "read"
    int sleep_time = 1 + text.size() / 20;
    printf("EttsNovoice: sleeping %i sec.\n", sleep_time);
    sleep(sleep_time);
    return 0;
  }

  ////////////////////////////////////////////////////////////////////////////////

  void shutUp() {
    debugPrintf("shutUp() en primitivas de NoVoice\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  void pauseSpeech() {
    debugPrintf("pauseSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  void resumeSpeech() {
    debugPrintf("resumeSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////////

  void setVolume( int vol) {
    debugPrintf("changeVolume(%i)\n", vol);
    // does nothing
  }
};
