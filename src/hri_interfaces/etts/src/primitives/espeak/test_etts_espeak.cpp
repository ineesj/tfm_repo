#include "etts_espeak.cpp"
#include "utils/primitive_test.h"


////////////////////////////////////////////////////////////////////////////////

int main() {
  debugPrintf("main()\n");


  EttsEspeak etts;
  etts.generateSpeech("Olá. este é um teste.",
                                   ROBOT_MAGGIE,
                                   Translator::LANGUAGE_PORTUGUESE,
                                   etts_msgs::Utterance::EMOTION_HAPPY);


  etts.generateSpeech("Hola. Esto es una prueba.",
                                   ROBOT_MAGGIE,
                                   Translator::LANGUAGE_SPANISH,
                                   etts_msgs::Utterance::EMOTION_HAPPY);


  etts.generateSpeech("Hello. This is a test sentence.",
                                   ROBOT_MAGGIE,
                                   Translator::LANGUAGE_ENGLISH,
                                   etts_msgs::Utterance::EMOTION_HAPPY);


  return 0;
}
