/*!
 * \file EttsEspeak.cpp
 *
 * \date 2015
 * \author Fernando Alonso Martin
 */

#include "definitions/etts_primitive_interface.h"
#include "debug/debug2.h"
#include "string/StringUtils.h"
#include "system/system_utils.h"
#include "api/etts_api.h"
#include <string>

////////////////////////////////////////////////////////////////////////////

class EttsEspeak: public EttsPrimitiveInterface {
public:

  /*! Constructor */
  EttsEspeak() {
    debugPrintf("EttsEspeak ctor\n");  
  }


  ////////////////////////////////////////////////////////////////////////////

  /*! Desctructor*/
  ~EttsEspeak() {
    debugPrintf("EttsEspeak dtor\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Inicializa la sesión */
  void init() {
    debugPrintf("init()\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  /*! Finaliza la sesion*/
  void stop() {
    debugPrintf("stop()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Genera la locución de voz usando el programa Espeak
    @param texto a sintetizar
    @param robot_id el robot que va a sintetizar la voz (en este caso da igual)
    @param wanted_language lenguaje en el que se va a sinteziar
    @param emotion con la que se sintetiza (en este caso da igual, siempre la misma)
    */
  int generateSpeech(const std::string & text,
                     const RobotId robot_id,
                     const Translator::LanguageId wanted_language,
                     const utterance_utils::Emotion emotion) {

      // possible languages : 'en-US', 'en-GB', 'de-DE', 'es-ES', 'fr-FR', 'it-IT'
      std::string country_domain = "pt-pt";

      switch (wanted_language) {
      case Translator::LANGUAGE_ENGLISH:
        country_domain = "en"; break;
      case Translator::LANGUAGE_GERMAN:
        country_domain = "de"; break;
      case Translator::LANGUAGE_SPANISH:
        country_domain = "es"; break;
      case Translator::LANGUAGE_FRENCH:
        country_domain = "fr"; break;
      case Translator::LANGUAGE_ITALIAN:
        country_domain = "it"; break;
      case Translator::LANGUAGE_PORTUGUESE:
        country_domain = "pt-pt"; break;
      default:
        printf("EttsPico: language '%s' not supported!\n",
               Translator::get_language_full_name(wanted_language).c_str());
        return -1;
      }

      std::string play_cmd = "espeak -v "+ country_domain + " \"" + text + "\"";
      ROS_DEBUG("Command to play text: %s",play_cmd.c_str());
      system_utils::exec_system(play_cmd);



    return 0;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para para la locución de voz */
  void shutUp() {
    debugPrintf("shutUp() en primitivas de Espeak\n");
    system_utils::exec_system("pkill -f espeak");
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para pausar la voz */
  void pauseSpeech() {
    debugPrintf("pauseSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para reanudar la voz */
  void resumeSpeech() {
    debugPrintf("resumeSpeech()\n");
    // does nothing
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Para establecer el nuevo volumen */
  void setVolume( int vol) {
    debugPrintf("setVolume(%i)\n", vol);
    // does nothing
  }


}; // end class EttsEspeak
