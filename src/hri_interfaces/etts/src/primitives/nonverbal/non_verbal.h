/*!
   @author Fernando Alonso Martin
   @version 0.1
   @date July, 2011
*/


#ifndef NONVERBAL_H
#define NONVERBAL_H

//#include "debug/debug2.h"
//#include "long_term_memory/ltm_path.h"
#include <ros/package.h>
#include "definitions/etts_primitive_interface.h"

namespace ad{
namespace tts{

typedef std::pair <std::string, std::string> soundPair;
typedef std::multimap <std::string, std::string> nonVerbalMap;
typedef nonVerbalMap::const_iterator snIterator;
typedef std::pair <snIterator, snIterator> snIteratorPair;

std::string PATH = ros::package::getPath("etts");


/*!
  * Primitivas de voz que usa sintetizar sonidos no verbales pregenerados
  */
class NonVerbal:  public EttsPrimitiveInterface{
public:

  typedef std::string semanticKey;

  static const semanticKey AMAZING;
  static const semanticKey ANGRY;
  static const semanticKey ATTENTION;
  static const semanticKey CONFIRMATION;
  static const semanticKey DIALOG;
  static const semanticKey ERROR;
  static const semanticKey HELLO;
  static const semanticKey SINGING;
  static const semanticKey THINKING;
  static const semanticKey WARNING;
  static const semanticKey MUSIC_BDAY;


  //monarch tags
  static const semanticKey GREET_GENERIC;
  static const semanticKey GREET_STAFF;
  static const semanticKey GREET_BABY;

  static const semanticKey AMAZING_1;
  static const semanticKey AMAZING_2;
  static const semanticKey AMAZING_3;
  static const semanticKey AMAZING_4;
  static const semanticKey AMAZING_5;

  static const semanticKey ANGRY_1;
  static const semanticKey ANGRY_2;
  static const semanticKey ANGRY_3;
  static const semanticKey ANGRY_4;
  static const semanticKey ANGRY_5;

  static const semanticKey ATTENTION_1;
  static const semanticKey ATTENTION_2;
  static const semanticKey ATTENTION_3;
  static const semanticKey ATTENTION_4;
  static const semanticKey ATTENTION_5;

  static const semanticKey CONFIRMATION_1;
  static const semanticKey CONFIRMATION_2;
  static const semanticKey CONFIRMATION_3;
  static const semanticKey CONFIRMATION_4;
  static const semanticKey CONFIRMATION_5;

  static const semanticKey DIALOG_1;
  static const semanticKey DIALOG_2;
  static const semanticKey DIALOG_3;
  static const semanticKey DIALOG_4;
  static const semanticKey DIALOG_5;

  static const semanticKey ERROR_1;
  static const semanticKey ERROR_2;
  static const semanticKey ERROR_3;
  static const semanticKey ERROR_4;
  static const semanticKey ERROR_5;

  static const semanticKey THINKING_1;
  static const semanticKey THINKING_2;
  static const semanticKey THINKING_3;
  static const semanticKey THINKING_4;
  static const semanticKey THINKING_5;

  static const semanticKey WARNING_1;
  static const semanticKey WARNING_2;
  static const semanticKey WARNING_3;
  static const semanticKey WARNING_4;
  static const semanticKey WARNING_5;



  /** \brief constructor */
  NonVerbal();

  /** \brief destructor */
  ~NonVerbal();

  /*! Inicializa el motor */
  void init();

  /*! Para parar la síntesis (en este caso no hace nada) */
  void shutUp();

  /*! Para finalizar la sesión */
  void stop();

  /*! Para genera voz, en este caso no hace nada */
  int generateSpeech(const std::string & text,
                     const RobotId robot_id,
                     const Translator::LanguageId language,
                     const utterance_utils::Emotion emotion);

  /*! Pausa la sintesis */
  void pauseSpeech();

  /*! Resume la sintesis */
  void resumeSpeech();

  //! between 0 and 100
  void setVolume (int vol);


  /*!
     \brief returns a random text snippet associated to the entered parameter
     \param semantic the text to be randomized
     \return A random text snippet if the parameter is found in the snippets set. null if it is not found
     \see ad::tts::SpeechSnippets::createMap() for the available text snippets and their semantics
    */
  static std::string getRandomSound(std::string semantic);

  /*!
     * \brief Prints all the text snippets with its semantic keys
     */
  static void printAll();

  /*!
     * \brief Prints all the keys of the snippets map
     */
  static void printSemantics();

  /*!
     * \brief Prints all the snippets of a single semantic key
     * \param key the key of the snippets to print
     */
  static void printSounds(std::string semantic);


private:



  static const nonVerbalMap createMap();

  /**
      * A map with all the snippets
      */
  static const nonVerbalMap SOUNDS;// = createMap();
};

}
}

#endif // NONVERBAL_H
