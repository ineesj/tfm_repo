
#include "primitives/nonverbal/non_verbal.h"
#include "debug/error.h"
#include "system/system_utils.h"
#include <sstream>
#include "ros/ros.h"

using namespace ad::tts;

//GENERIC AT RANDOM
const NonVerbal::semanticKey NonVerbal::ATTENTION = "ATTENTION";
const NonVerbal::semanticKey NonVerbal::ANGRY = "ANGRY";
const NonVerbal::semanticKey NonVerbal::AMAZING = "AMAZING";
const NonVerbal::semanticKey NonVerbal::CONFIRMATION = "CONFIRMATION";
const NonVerbal::semanticKey NonVerbal::DIALOG =  "DIALOG";
const NonVerbal::semanticKey NonVerbal::ERROR = "ERROR";
const NonVerbal::semanticKey NonVerbal::HELLO = "HELLO";
const NonVerbal::semanticKey NonVerbal::MUSIC_BDAY = "MUSIC_BDAY";
const NonVerbal::semanticKey NonVerbal::SINGING = "SING";
const NonVerbal::semanticKey NonVerbal::THINKING = "THINKING";
const NonVerbal::semanticKey NonVerbal::WARNING = "WARNING";


//MONARCH,NOT RANDOM
const NonVerbal::semanticKey NonVerbal::GREET_GENERIC="GREET_GENERIC";
const NonVerbal::semanticKey NonVerbal::GREET_STAFF="GREET_STAFF";
const NonVerbal::semanticKey NonVerbal::GREET_BABY="GREET_BABY";

const NonVerbal::semanticKey NonVerbal::AMAZING_1="AMAZING_1";
const NonVerbal::semanticKey NonVerbal::AMAZING_2="AMAZING_2";
const NonVerbal::semanticKey NonVerbal::AMAZING_3="AMAZING_3";
const NonVerbal::semanticKey NonVerbal::AMAZING_4="AMAZING_4";
const NonVerbal::semanticKey NonVerbal::AMAZING_5="AMAZING_5";

const NonVerbal::semanticKey NonVerbal::ANGRY_1="ANGRY_1";
const NonVerbal::semanticKey NonVerbal::ANGRY_2="ANGRY_2";
const NonVerbal::semanticKey NonVerbal::ANGRY_3="ANGRY_3";
const NonVerbal::semanticKey NonVerbal::ANGRY_4="ANGRY_4";
const NonVerbal::semanticKey NonVerbal::ANGRY_5="ANGRY_5";

const NonVerbal::semanticKey NonVerbal::ATTENTION_1="ATTENTION_1";
const NonVerbal::semanticKey NonVerbal::ATTENTION_2="ATTENTION_2";
const NonVerbal::semanticKey NonVerbal::ATTENTION_3="ATTENTION_3";
const NonVerbal::semanticKey NonVerbal::ATTENTION_4="ATTENTION_4";
const NonVerbal::semanticKey NonVerbal::ATTENTION_5="ATTENTION_5";

const NonVerbal::semanticKey NonVerbal::CONFIRMATION_1="CONFIRMATION_1";
const NonVerbal::semanticKey NonVerbal::CONFIRMATION_2="CONFIRMATION_2";
const NonVerbal::semanticKey NonVerbal::CONFIRMATION_3="CONFIRMATION_3";
const NonVerbal::semanticKey NonVerbal::CONFIRMATION_4="CONFIRMATION_4";
const NonVerbal::semanticKey NonVerbal::CONFIRMATION_5="CONFIRMATION_5";

const NonVerbal::semanticKey NonVerbal::DIALOG_1="DIALOG_1";
const NonVerbal::semanticKey NonVerbal::DIALOG_2="DIALOG_2";
const NonVerbal::semanticKey NonVerbal::DIALOG_3="DIALOG_3";
const NonVerbal::semanticKey NonVerbal::DIALOG_4="DIALOG_4";
const NonVerbal::semanticKey NonVerbal::DIALOG_5="DIALOG_5";

const NonVerbal::semanticKey NonVerbal::ERROR_1="ERROR_1";
const NonVerbal::semanticKey NonVerbal::ERROR_2="ERROR_2";
const NonVerbal::semanticKey NonVerbal::ERROR_3="ERROR_3";
const NonVerbal::semanticKey NonVerbal::ERROR_4="ERROR_4";
const NonVerbal::semanticKey NonVerbal::ERROR_5="ERROR_5";

const NonVerbal::semanticKey NonVerbal::THINKING_1="THINKING_1";
const NonVerbal::semanticKey NonVerbal::THINKING_2="THINKING_2";
const NonVerbal::semanticKey NonVerbal::THINKING_3="THINKING_3";
const NonVerbal::semanticKey NonVerbal::THINKING_4="THINKING_4";
const NonVerbal::semanticKey NonVerbal::THINKING_5="THINKING_5";

const NonVerbal::semanticKey NonVerbal::WARNING_1="WARNING_1";
const NonVerbal::semanticKey NonVerbal::WARNING_2="WARNING_2";
const NonVerbal::semanticKey NonVerbal::WARNING_3="WARNING_3";
const NonVerbal::semanticKey NonVerbal::WARNING_4="WARNING_4";
const NonVerbal::semanticKey NonVerbal::WARNING_5="WARNING_5";



const nonVerbalMap NonVerbal::SOUNDS = NonVerbal::createMap();

////////////////////////////////////////////////////////////////////////////////

NonVerbal::NonVerbal(){
  //ctor
}

////////////////////////////////////////////////////////////////////////////////

NonVerbal::~NonVerbal(){
  //dtor
}

///////////////////////////////////////////////////////////////////

void NonVerbal::init(){}

void NonVerbal::stop(){}

void NonVerbal::shutUp(){
  ROS_DEBUG("shutUp() en primitivas de NonVerbal\n");
  system_utils::exec_system("killall play");
}



int NonVerbal::generateSpeech(const std::string & text,
                              const RobotId robot_id,
                              const Translator::LanguageId language,
                              const utterance_utils::Emotion emotion){
  ROS_DEBUG("generateSpeech(texto:'%s', robot_id:%s, current_language:%s, emotion:%s)\n",
               text.c_str(),
               robot_id_to_full_string(robot_id).c_str(),
               Translator::get_language_full_name(language).c_str(),
               utterance_utils::emotion_to_full_string(emotion).c_str());

  try{
    ROS_INFO("Generacion no verbal con tag semantico: %s\n",text.c_str());

    std::string mySound = this->getRandomSound(text);
    //        if (mySound == NULL){
    //            ROS_DEBUG("Tag semantico no reconocido para generacon de lenguaje no verbal\n");
    //            return -1;
    //        }
    ROS_INFO("Se ha encontrado el sonido %s\n",mySound.c_str());
    const std::string FICH_AUDIO =  std::string(PATH + "/data/") +  mySound;
    ROS_DEBUG("FICH_AUDIO:%s\n", FICH_AUDIO.c_str());

    // read it with mplayer
    std::ostringstream command;
    command.str("");
    command << "play --no-show-progress --volume 1.5 " << FICH_AUDIO;
    //command << "  -msglevel all=1";
    ROS_DEBUG("command:'%s'\n", command.str().c_str());
    int return_value = system(command.str().c_str());
    if (return_value != 0){
      ROS_DEBUG("The instruction '%s' returned %i != 0\n",command.str().c_str(), return_value);
    }
    return return_value;
  }catch (std::exception& e){
    ROS_DEBUG ("Se ha producido una excepción al generar comunicacion no verbal\n");
    maggieError("%s", e.what());
    return -1;
  }

}


void NonVerbal::pauseSpeech(){}

void NonVerbal::resumeSpeech(){}

//! between 0 and 100
void NonVerbal::setVolume (int vol){}



////////////////////////////////////////////////////////////////////////////////

/**
  Donde especificamos la ruta donde estan asociados cada fichero de audio a su tag semantico
  -La ruta debe partir desde DATA
  */
#define OGGDIR "/nonVerbal-L/"
const nonVerbalMap NonVerbal::createMap(){
  nonVerbalMap sounds;


  //GENERIC SOUNDS
  sounds.insert (soundPair(AMAZING, OGGDIR "IWohoo1.ogg"));
  sounds.insert (soundPair(AMAZING, OGGDIR "IWohoo2.ogg"));
  sounds.insert (soundPair(AMAZING, OGGDIR "IWohoo3.ogg"));
  sounds.insert (soundPair(AMAZING, OGGDIR "IWooHoo1.ogg"));

  sounds.insert (soundPair(ANGRY, OGGDIR "IErrgh1.ogg"));
  sounds.insert (soundPair(ANGRY, OGGDIR "IOh1.ogg"));
  sounds.insert (soundPair(ANGRY, OGGDIR "IOoh1.ogg"));

  sounds.insert (soundPair(ATTENTION, OGGDIR "IUh1.ogg"));
  sounds.insert (soundPair(ATTENTION, OGGDIR "IWohoo1.ogg"));
  sounds.insert (soundPair(ATTENTION, OGGDIR "IWohoo2.ogg"));
  sounds.insert (soundPair(ATTENTION, OGGDIR "IWohoo3.ogg"));
  sounds.insert (soundPair(ATTENTION, OGGDIR "IWooHoo1.ogg"));
  sounds.insert(soundPair(ATTENTION, OGGDIR "IAttn1.ogg"));
  sounds.insert(soundPair(ATTENTION, OGGDIR "IAttn2.ogg"));
  sounds.insert(soundPair(ATTENTION, OGGDIR "IAw1.ogg"));
  sounds.insert(soundPair(ATTENTION, OGGDIR "IExcuseMe1.ogg"));

  sounds.insert(soundPair(CONFIRMATION, OGGDIR "IAha1.ogg"));
  sounds.insert(soundPair(CONFIRMATION, OGGDIR "IAha2.ogg"));
  sounds.insert(soundPair(CONFIRMATION, OGGDIR "IAha3.ogg"));
  sounds.insert(soundPair(CONFIRMATION, OGGDIR "INoProb1.ogg"));
  sounds.insert(soundPair(CONFIRMATION, OGGDIR "IAck1.ogg"));

  sounds.insert(soundPair(DIALOG, OGGDIR "IAw1.ogg"));
  sounds.insert(soundPair(DIALOG, OGGDIR "I0h2.ogg"));
  sounds.insert(soundPair(DIALOG, OGGDIR "IDiag1.ogg"));
  sounds.insert(soundPair(DIALOG, OGGDIR "IDiag2.ogg"));
  sounds.insert(soundPair(DIALOG, OGGDIR "IDiag3.ogg"));
  sounds.insert(soundPair(DIALOG, OGGDIR "IDiagAlrt1.ogg"));

  sounds.insert (soundPair(ERROR, OGGDIR "IFailure1.ogg"));
  sounds.insert (soundPair(ERROR, OGGDIR "IFailure2.ogg"));
  sounds.insert (soundPair(ERROR, OGGDIR "IFailure3.ogg"));

  sounds.insert (soundPair(HELLO, OGGDIR "IHello1.ogg"));
  sounds.insert (soundPair(HELLO, OGGDIR "IHey1.ogg"));
  sounds.insert (soundPair(HELLO, OGGDIR "IHey2.ogg"));

  sounds.insert (soundPair(MUSIC_BDAY, "BPM:4,"
                           "1/2-G4,1/2-G4,A4,G4,C5,2-B4,"
                           "1/2-G4,1/2-G4,A4,G4,D5,2-C5,"
                           "1/2-G4,1/2-G4,G5,E5,C5,B4,1.5-A4,"
                           "1/2-F5,1/2-F5,E5,C5,D5,2-C5"));

  sounds.insert (soundPair(SINGING, "musicVocaloid/Canon.ogg"));
  //sounds.insert (soundPair(SINGING, "musicVocaloid/MujerContraMujerVocaloid.ogg"));

  sounds.insert (soundPair(THINKING, OGGDIR "IThink1.ogg"));
  sounds.insert (soundPair(THINKING, OGGDIR "IMmm1.ogg"));
  sounds.insert (soundPair(THINKING, OGGDIR "IMmm2.ogg"));

  sounds.insert(soundPair(WARNING, OGGDIR "IAttention1.ogg"));
  sounds.insert(soundPair(WARNING, OGGDIR "IBatWarn1.ogg"));
  sounds.insert(soundPair(WARNING, OGGDIR "IBatWarn2.ogg"));
  sounds.insert(soundPair(WARNING, OGGDIR "IBatWarn3.ogg"));
  sounds.insert(soundPair(WARNING, OGGDIR "IBatWarn4.ogg"));



  //MONARCH,NOT RANDOM
  sounds.insert (soundPair(GREET_GENERIC,OGGDIR "GreetGeneric.ogg"));
  sounds.insert (soundPair(GREET_STAFF,OGGDIR "GreetStaff.ogg"));
  sounds.insert (soundPair(GREET_BABY,OGGDIR "GreetBaby.ogg"));

  sounds.insert (soundPair(AMAZING_1,OGGDIR "IWohoo1.ogg"));
  sounds.insert (soundPair(AMAZING_2,OGGDIR "IWohoo2.ogg"));
  sounds.insert (soundPair(AMAZING_3,OGGDIR "IWohoo3.ogg"));
  sounds.insert (soundPair(AMAZING_4,OGGDIR "IWooHoo1.ogg"));
  sounds.insert (soundPair(AMAZING_5,OGGDIR "IWohoo3.ogg"));

  sounds.insert (soundPair(ANGRY_1,OGGDIR "IErrgh1.ogg"));
  sounds.insert (soundPair(ANGRY_2,OGGDIR "IOh1.ogg"));
  sounds.insert (soundPair(ANGRY_3,OGGDIR "IOoh1.ogg"));
  sounds.insert (soundPair(ANGRY_4,OGGDIR "IErrgh1.ogg"));
  sounds.insert (soundPair(ANGRY_5,OGGDIR "IOoh1.ogg"));

  sounds.insert (soundPair(ATTENTION_1,OGGDIR "IUh1.ogg"));
  sounds.insert (soundPair(ATTENTION_2,OGGDIR "IWohoo1.ogg"));
  sounds.insert (soundPair(ATTENTION_3,OGGDIR "IWohoo3.ogg"));
  sounds.insert (soundPair(ATTENTION_4,OGGDIR "IWooHoo1.ogg"));
  sounds.insert (soundPair(ATTENTION_5,OGGDIR "IUh1.ogg"));

  sounds.insert (soundPair(CONFIRMATION_1,OGGDIR "IAha1.ogg"));
  sounds.insert (soundPair(CONFIRMATION_2,OGGDIR "IAha2.ogg"));
  sounds.insert (soundPair(CONFIRMATION_3,OGGDIR "IAha3.ogg"));
  sounds.insert (soundPair(CONFIRMATION_4,OGGDIR "INoProb1.ogg"));
  sounds.insert (soundPair(CONFIRMATION_5,OGGDIR"IAck1.ogg"));

  sounds.insert (soundPair(DIALOG_1,OGGDIR "IAw1.ogg"));
  sounds.insert (soundPair(DIALOG_2,OGGDIR "I0h2.ogg"));
  sounds.insert (soundPair(DIALOG_3,OGGDIR "IDiag1.ogg"));
  sounds.insert (soundPair(DIALOG_4,OGGDIR "IDiagAlrt1.ogg"));
  sounds.insert (soundPair(DIALOG_5,OGGDIR "IDiag2.ogg"));

  sounds.insert (soundPair(ERROR_1,OGGDIR "IFailure1.ogg"));
  sounds.insert (soundPair(ERROR_2,OGGDIR "IFailure2.ogg"));
  sounds.insert (soundPair(ERROR_3,OGGDIR "IFailure3.ogg"));
  sounds.insert (soundPair(ERROR_4,OGGDIR "IFailure1.ogg"));
  sounds.insert (soundPair(ERROR_5,OGGDIR "IFailure2.ogg"));

  sounds.insert (soundPair(THINKING_1,OGGDIR "IThink1.ogg"));
  sounds.insert (soundPair(THINKING_2,OGGDIR "IMmm1.ogg"));
  sounds.insert (soundPair(THINKING_3,OGGDIR "IMmm2.ogg"));
  sounds.insert (soundPair(THINKING_4,OGGDIR "IMmm1.ogg"));
  sounds.insert (soundPair(THINKING_5,OGGDIR "IThink1.ogg"));

  sounds.insert (soundPair(WARNING_1,OGGDIR "IAttention1.ogg"));
  sounds.insert (soundPair(WARNING_2,OGGDIR "IBatWarn1.ogg"));
  sounds.insert (soundPair(WARNING_3,OGGDIR "IBatWarn2.ogg"));
  sounds.insert (soundPair(WARNING_4,OGGDIR "IBatWarn3.ogg"));
  sounds.insert (soundPair(WARNING_5,OGGDIR "IBatWarn4.ogg"));




  return sounds;
}

////////////////////////////////////////////////////////////////////////////////

// What this function does is the following.
// First checks if the entered parameter is in the snippetMap
// After it, retrieves the first ocurrence of semantic in the snippetMap
// and navigates among a random number of occurrences of the Map.
// finally, it returns the last navigated occurence.
std::string NonVerbal::getRandomSound(std::string semantic){
  if (SOUNDS.find(semantic) != SOUNDS.end()){

    timeval timeStamp;
    gettimeofday( &timeStamp, NULL );
    srand(timeStamp.tv_usec); // seeding the random number generator

    std::pair <nonVerbalMap::const_iterator, nonVerbalMap::const_iterator> range;
    range = SOUNDS.equal_range(semantic);
    nonVerbalMap::const_iterator iter = range.first;

    // getting the distance between both iterators
    int distance = std::distance (range.first, range.second);
    //ROS_NFO("Distance: %d\n",distance);
    //ROS_NFO("Aleatorio: %d\n", rand());
    //ROS_NFO("Resultado: %d\n",(rand()%distance));
    int randomNumber = (rand() % distance);
    // navigating from the first snippet to a random one
    for (int i=0 ; i < randomNumber  ; i++ ){
      iter++;
    }

    return iter->second; // returning the random snippet
    //        return range.first.second;
    //        std::advance( sounds.lower_bound(semantic),
    //                      rand() % (int)sounds.count(semantic) );
    //        return((*iter).second); // returning the random snippet
  }

  ROS_DEBUG ("String %s was not found in the sounds map\n", semantic.c_str());
  return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void NonVerbal::printAll(){
  //    snIterator it;
  nonVerbalMap::const_iterator it;
  // show content:
  for ( it = SOUNDS.begin() ; it != SOUNDS.end(); it++ ){
    std::cout << (*it).first << " => " << (*it).second << std::endl;
  }
}

////////////////////////////////////////////////////////////////////////////////

void NonVerbal::printSemantics(){
  nonVerbalMap::const_iterator it;
  std::string key = "NULL";
  // show content:
  for ( it = SOUNDS.begin() ; it != SOUNDS.end(); it++ ){
    //        if (key.compare((*it).first) != 0){
    std::cout << (*it).first << std::endl;
    //            key = (*it).first;
    //        }
  }

}

////////////////////////////////////////////////////////////////////////////////

void NonVerbal::printSounds(std::string semantic){
  if (SOUNDS.find(semantic) != SOUNDS.end()){
    snIteratorPair range;
    range = SOUNDS.equal_range(semantic);
    //        snIterator keyIterator = range.first;

    for (snIterator it=range.first; it!=range.second; ++it){
      std::cout << " " << (*it).second << std::endl;
    }
    std::cout << std::endl;
  } else {
    std::cout << "Key: " + semantic + " doesn't exist in the sounds set" << std::endl;
  }
}


