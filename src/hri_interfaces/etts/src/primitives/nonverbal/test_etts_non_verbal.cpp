/*!
   @file test_speechSnippets.cpp
   @brief A simple test of the ad::tts::SpeechSnippets class
   Prints all the keys and asks the user to introduce a key to print all its values
   @author Victor Gonzalez-Pacheco   vgonzale@ing.uc3m.es
   @version 1.0
   @date june, 2011
*/


#include "etts_msgs/Utterance.h"
#include "primitives/nonverbal/non_verbal.h"
using namespace ad::tts;
using std::cout;
using std::cin;
using std::endl;

int main () {
  std::string option;

  ROS_DEBUG ("Printing All the snippets\n");
  NonVerbal::printAll();

  while (1){
    cout << endl;
    cout << "Available semantics:" << endl;
    NonVerbal::printSemantics();

    cout << "Choose a semantic to show all its sounds "
            "or press Ctrl-C to exit." << endl;
    cout << "Option: ";
    cin >> option;
    NonVerbal nonVerbal;
    nonVerbal.generateSpeech(option,
                             // some foo params (unused)
                             ROBOT_MAGGIE,
                             Translator::LANGUAGE_CATALAN,
                             etts_msgs::Utterance::EMOTION_HAPPY);
    cout << endl;
    cout << endl;
  }

  return 0;
}

