#ifndef TTSPRIMITIVEINTERFACE_H
#define TTSPRIMITIVEINTERFACE_H

#include "definitions/robot_id.h"
#include "definitions/utterance_params.h"

/*! Esta clase establece las funciones mínimas que debe tener cada motor/primitivas de síntesis verbal/sonora. Es usada por la habilidad Etts para
  sintetizar la voz con diferentes motores de síntesis */
class EttsPrimitiveInterface {
public:

    /*! Inicializa el motor de síntesis */
    virtual void init() = 0;

    /*! Libera los recursos usados por el motor de síntesis */
    virtual void stop() = 0;

    /*! Genera voz usando la primitiva/motor de síntesis específico
      @param text texto a sintetizar
      @param robot_id robot en el que se está sintetizando la voz, cada robot tiene diferente timbre de voz
      @param language idioma en el que se va a sintetizar la voz
      @param emotion emocion en la que se va a sintetizar la locución (en el caso de que la primitiva de voz sea capaz de sintetizar voz con diferentes emociones
      @return 0 if success, <0 or >0 otherwise
      */
    virtual int generateSpeech(const std::string & text,
                               const RobotId robot_id,
                               const Translator::LanguageId language,
                               const utterance_utils::Emotion emotion) = 0;

     /*! Para callar la síntesis */
    virtual void shutUp() = 0;

    /*! Para pausar la síntesis */
    virtual void pauseSpeech() = 0;

    /*! Para reanudar la síntesis una vez que había sido pausada */
    virtual void resumeSpeech() = 0;

    /*! Para establecer el volumen al que se va a sintetizar la locución */
    virtual void setVolume (int cantidad) = 0;

protected:
};

#endif // TTSPRIMITIVEINTERFACE_H
