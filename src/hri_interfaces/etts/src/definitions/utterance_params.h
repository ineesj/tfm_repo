#ifndef UTTERANCEPARAMS_H
#define UTTERANCEPARAMS_H

#include "etts_msgs/Utterance.h"
#include "utils/speech_snippets/speech_snippets.h"
#include "translator/Translator.h"
#include "string/find_and_replace.h"
#include "ros/package.h"
#include "ros/ros.h"

//! estructura para almacenar la locucion en memoria compartida
namespace utterance_utils {
typedef int Emotion;
typedef int PrimitiveId;
typedef int Volume;

static const Translator::LanguageId DEFAULT_LANGUAGE =
    Translator::LANGUAGE_PORTUGUESE;
static const Emotion DEFAULT_EMOTION =
    etts_msgs::Utterance::EMOTION_HAPPY;
static const PrimitiveId DEFAULT_PRIMITIVE =
    etts_msgs::Utterance::PRIM_MICROSOFT;
static const Volume DEFAULT_VOLUME = 100;

//! convert a emotion to a string
const std::string emotion_to_full_string(const Emotion & emotion) {
  switch (emotion) {
    case etts_msgs::Utterance::EMOTION_LAST_USED:
      return "last_used"; break;
    case etts_msgs::Utterance::EMOTION_HAPPY:
      return "happy"; break;
    case etts_msgs::Utterance::EMOTION_SAD:
      return "sad"; break;
    case etts_msgs::Utterance::EMOTION_NERVOUS:
      return "nervous"; break;
    case etts_msgs::Utterance::EMOTION_TRANQUILITY:
      return "tranquility"; break;
    default:
      ROS_INFO("emotion_to_full_string(): unknown emotion %i\n", emotion);
      return emotion_to_full_string(DEFAULT_EMOTION);
  } // end switch emotion
}

/*!
   \param emotion_str the string version,
      for instance "HAPPY" or "EMOTION_HAPPY", or even "happy"
   \param emotion_out the output found
   \return bool true if success
  */
bool string_to_emotion(const std::string & emotion_str, Emotion & emotion_out) {
  std::string emotion_str_uppercase = emotion_str;
  // case changing
  std::transform(emotion_str_uppercase.begin(), emotion_str_uppercase.end(),
                 emotion_str_uppercase.begin(), ::toupper);

  // add "EMOTION_" at the beginning if it does not start with that
  if (emotion_str_uppercase.find("EMOTION_") != 0)
    emotion_str_uppercase = std::string("EMOTION_") + emotion_str_uppercase;

  // now the proper switch
  if (emotion_str_uppercase == "EMOTION_HAPPY") {
    emotion_out = etts_msgs::Utterance::EMOTION_HAPPY; return true;
  } else if (emotion_str_uppercase == "EMOTION_SAD") {
    emotion_out = etts_msgs::Utterance::EMOTION_SAD; return true;
  } else if (emotion_str_uppercase == "EMOTION_TRANQUILITY") {
    emotion_out = etts_msgs::Utterance::EMOTION_TRANQUILITY; return true;
  } else if (emotion_str_uppercase == "EMOTION_NERVOUS") {
    emotion_out = etts_msgs::Utterance::EMOTION_NERVOUS; return true;
  } // end switch emotion_str
  emotion_out = DEFAULT_EMOTION;
  ROS_INFO("string_to_emotion(): unknown emotion '%s'\n", emotion_str.c_str());
  return false;
}

////////////////////////////////////////////////////////////////////////////////

//! convert a primitive to a string
std::string primitive_to_full_string(const PrimitiveId & primitive) {
  switch (primitive) {
    case etts_msgs::Utterance::PRIM_LAST_USED:
      return "last_used"; break;
    case etts_msgs::Utterance::PRIM_NOVOICE:
      return "novoice"; break;
    case etts_msgs::Utterance::PRIM_GOOGLE:
      return "google"; break;
    case etts_msgs::Utterance::PRIM_NONVERBAL:
      return "non_verbal"; break;
    case etts_msgs::Utterance::PRIM_MICROSOFT:
      return "microsoft"; break;
    case etts_msgs::Utterance::PRIM_ESPEAK:
        return "espeak"; break;
    default:
      ROS_INFO("primitive_to_full_string(): unknown primitive %i\n", primitive);
      return primitive_to_full_string(DEFAULT_PRIMITIVE);
  } // end switch primitive
}

//! convert a string to a primitve
bool string_to_primitive(const std::string & s,
                         PrimitiveId & primitive) {
  std::string s_upp = s;
  std::transform(s_upp.begin(), s_upp.end(), s_upp.begin(), ::toupper);
  // add "PRIM" at the beginning if it does not start with that
  if (s_upp.find("PRIM_") != 0)
    s_upp = std::string("PRIM_") + s_upp;

  if (s_upp == "PRIM_GOOGLE")
    primitive = etts_msgs::Utterance::PRIM_GOOGLE;
  else if (s_upp == "PRIM_MICROSOFT")
    primitive = etts_msgs::Utterance::PRIM_MICROSOFT;
  else if (s_upp == "PRIM_NONVERBAL" || s_upp == "PRIM_NON_VERBAL")
    primitive = etts_msgs::Utterance::PRIM_NONVERBAL;
  else if (s_upp == "PRIM_NOVOICE" || s_upp == "PRIM_NO_VOICE")
    primitive = etts_msgs::Utterance::PRIM_NOVOICE;
  else if (s_upp == "PRIM_ESPEAK")
    primitive = etts_msgs::Utterance::PRIM_ESPEAK;
  else {
    primitive = DEFAULT_PRIMITIVE;
    ROS_INFO("Unknwon primitive '%s'\n", s.c_str());
    return false;
  }
  return true;
}

////////////////////////////////////////////////////////////////////////////////

std::string utterance2string(const etts_msgs::Utterance & msg) {
  std::ostringstream out;
  out << "[";
  out << "text: '" << msg.text;
  out << "', emotion:" << msg.emotion;
  out << ", primitive:" << msg.primitive;
  out << ", language:'" << msg.language;
  out << "'', priority:" << msg.priority;
  out << ", volume:" << msg.volume;
  out << "]";
  return out.str();
}

////////////////////////////////////////////////////////////////////////////////

inline std::string string_beginning(const std::string & excerpt,
                                    const unsigned int size) {
  if (excerpt.size() < size)
    return excerpt;
  return excerpt.substr(size) + std::string("...");
}

////////////////////////////////////////////////////////////////////////////////

//! a short hand for a pair of string
typedef std::pair<std::string, std::string> ValuedTag;


/*! Extract all the metadata tags
 \param sentence_in the input sentence
 \param sentence_out_no_metadata the output sentence,
        stripped from all the metadata tags. It can be equal to sentence_in
 \param tags
        the list of tags with their values. NULL for not extracting them
*/
inline void get_all_metadata_tags(const std::string & sentence_in,
                                  std::string & sentence_out_no_metadata,
                                  std::vector<ValuedTag>* tags = NULL) {
  unsigned int tag_name_beginning_pos = 0, tag_equal_pos = 0;
  std::string tag_beginning = "\\";
  unsigned int tag_beginning_size = tag_beginning.size();
  std::ostringstream sentence_out_no_metadata_str;
  bool extract_tags =  (tags != NULL);
  if (extract_tags)
    tags->clear();

  while(true) {
    // try to find the next tag
    tag_name_beginning_pos = sentence_in.find
        (tag_beginning, tag_name_beginning_pos);
    // if no NLG, it is over
    if (tag_name_beginning_pos == std::string::npos) {
      // add the end
      sentence_out_no_metadata_str << sentence_in.substr(tag_equal_pos);
      sentence_out_no_metadata = sentence_out_no_metadata_str.str();
      StringUtils::remove_beginning_spaces(sentence_out_no_metadata);
      StringUtils::remove_trailing_spaces(sentence_out_no_metadata);
      return;
    }
    // otherwise add the non tag content
    sentence_out_no_metadata_str << sentence_in.substr
                                    (tag_equal_pos, tag_name_beginning_pos - tag_equal_pos);

    // find the space after the tag
    tag_name_beginning_pos += tag_beginning_size;
    unsigned int tag_and_value_end_pos =
        sentence_in.find(' ', tag_name_beginning_pos);
    // if no space, take the end of the string
    if (tag_and_value_end_pos == std::string::npos)
      tag_and_value_end_pos = sentence_in.size();

    // extract tag name and value
    std::string tag, value;
    tag_equal_pos = sentence_in.find("=", tag_name_beginning_pos);
    if (tag_equal_pos == std::string::npos
        || tag_equal_pos > tag_and_value_end_pos) {
      //ROS_INFO("= not found!");
      tag = sentence_in.substr
          (tag_name_beginning_pos, tag_and_value_end_pos - tag_name_beginning_pos);
      value = "";
    }
    else {
      tag = sentence_in.substr
          (tag_name_beginning_pos, tag_equal_pos - tag_name_beginning_pos);
      value = sentence_in.substr
          (tag_equal_pos + 1, tag_and_value_end_pos - (tag_equal_pos + 1));
    }

    // put it in the vector
    if (extract_tags)
      tags->push_back(std::make_pair(tag, value));

    // advance the vector (+1 to remove the space)
    tag_name_beginning_pos = std::min(tag_and_value_end_pos + 1, (unsigned int)sentence_in.size());
    tag_equal_pos = tag_name_beginning_pos;
  } // end while true
} // end get_all_metadata_tags();

////////////////////////////////////////////////////////////////////////////////

/*!
 \param tags the metadata extracted from an utterance,
             for instance with get_all_metadata_tags()
 \param emotion_out the Emotion conversion of the "emotion" tag value.
                    If there are several "emotion" tags, the first one is taken
                    into accound.
 \return bool true if an "emotion" tag was found in the tags
 \see get_all_metadata_tags()
*/
inline bool tags_contain_emotion_switch(const std::vector<ValuedTag> & tags,
                                        utterance_utils::Emotion & emotion_out) {
  for (unsigned int idx = 0; idx < tags.size(); ++idx) {
    const ValuedTag* curr_tag = &(tags[idx]);
    if (curr_tag->first != "emotion")
      continue;
    // try to convert the value to an emotion
    if (utterance_utils::string_to_emotion(curr_tag->second, emotion_out))
      return true;
  } // end loop idx
  return false;
} // end tags_contain_emotion_switch()

////////////////////////////////////////////////////////////////////////////////

//! remove the tags from Loquendo
inline void strip_metadata_tags(std::string & sentence_to_clean) {
  ROS_INFO("strip_metadata_tags('%s')\n", sentence_to_clean.c_str());
  get_all_metadata_tags(sentence_to_clean, sentence_to_clean, NULL);
}

////////////////////////////////////////////////////////////////////////////////

/*!
 * replace the natural language snippets with their equivalent
   \tag sentence a sentence in the given language
   \example "Yes, \NLG=OK let us do it"
 */
inline void replace_natural_langugage_tags(std::string & sentence_to_replace,
                                           const Translator::LanguageId dst_lang,
                                           const Translator::LanguageMap & map) {
  unsigned int NLG_pos = 0;
  std::string NLG_tag = "\\NLG=";

  while(true) {
    // try to find the next NLG
    NLG_pos = sentence_to_replace.find(NLG_tag, NLG_pos);
    // if no NLG, it is over
    if (NLG_pos == std::string::npos)
      return;

    // find the space after the NLG_pos
    unsigned int equal_pos = NLG_pos + NLG_tag.size();
    unsigned int space_pos = sentence_to_replace.find(" ", equal_pos);
    // if no space, take the end of the string
    if (space_pos == std::string::npos)
      space_pos = sentence_to_replace.size();

    // extract the tag
    std::string NL_tag = sentence_to_replace.substr(equal_pos, space_pos - equal_pos);
    ROS_INFO("NL_tag:'%s', dst_lang:%s\n",
           NL_tag.c_str(),
           Translator::get_language_full_name(dst_lang).c_str());

    // get the snippet
    std::string snippet_multilang = ad::tts::SpeechSnippets::getRandomSnippet(NL_tag);
    std::string snippet_dst_lang;
    bool snippet_exists = (snippet_multilang != "");
    if (snippet_exists) {
      // get the version in the wanted language of the snippet
      Translator::_build_given_language_in_multilanguage_line
          (snippet_multilang, dst_lang, map, snippet_dst_lang);
    }

    // remove the tag
    sentence_to_replace.erase(NLG_pos, space_pos - NLG_pos);

    if (snippet_exists)
      sentence_to_replace.insert(NLG_pos, snippet_dst_lang);

    // if we reached the end, quit
    if (NLG_pos >= sentence_to_replace.size() - 1)
      return;
  } // end loop
} // end replace_natural_langugage_tags();

} // end namespace utterance_utils

#endif // UTTERANCEPARAMS_H
