#ifndef ROBOT_ID_H
#define ROBOT_ID_H

#include <string>

enum RobotId {
  ROBOT_MAGGIE = 0,
  ROBOT_MOPI = 1,
  ROBOT_FLORY = 2
};

////////////////////////////////////////////////////////////////////////////////

//! convert a robot id to a string
inline std::string robot_id_to_full_string(const RobotId & id) {
  switch (id) {
  case ROBOT_MAGGIE:
    return "Maggie"; break;
  case ROBOT_MOPI:
    return "Mopi"; break;
  case ROBOT_FLORY:
      return "Flory"; break;
  default:
    return "unknwon";
  } // end switch id
} // end get_robot_id_string()


#endif // ROBOT_ID_H
