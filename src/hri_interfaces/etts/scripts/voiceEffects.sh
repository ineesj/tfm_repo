#!/bin/sh
# Add to pulseaudio devices effects to perform a non human voice of tts

#Robot Happy
pacmd load-module module-ladspa-sink sink_name=ladspa_out plugin=tap_pitch label=tap_pitch control=2,2,0,0

pacmd load-module module-ladspa-sink sink_name=ladspa_out plugin=tap_pitch label=tap_pitch control=6,0,0,0

#Vibrando
pacmd load-module module-ladspa-sink sink_name=ladspa_out plugin=tap_vibrato label=tap_vibrato control=10,2,0,0


#To selects this effects you need to change de default audio playback device on pavucontrol or sound control of ubuntu.
