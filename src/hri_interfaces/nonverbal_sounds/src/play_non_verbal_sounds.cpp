#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include <sstream>
#include <string>
#include <ros/package.h>
#include "monarch_msgs/NonVerbalSound.h"

using namespace std;



//void nonVerbalSoundsCallback(const std_msgs::String::ConstPtr& msg){
void nonVerbalSoundsCallback(const monarch_msgs::NonVerbalSound::ConstPtr& msg){

    string soundToPlay = msg->soundname;
    string soundPaths = ros::package::getPath("nonverbal_sounds")+"/data";
    
    //play the sound
    std::ostringstream command;
    command.str("");
    command << "play " << soundPaths <<"/" << soundToPlay;
    ROS_INFO("Sound Command %s", command.str().c_str());
    system(command.str().c_str());

}

string getSoundFromName(string soundname){
	
	string soundToPlay;
	
	if (soundname == "GREET_GENERIC"){
		soundToPlay = "GreetGeneric.ogg";
    }else if (soundname == "GREET_STAFF"){
        soundToPlay ="GreetStaff.ogg";
    }else if (soundname == "GREET_CHILD"){
        soundToPlay ="GreetBaby.ogg";

    }else if (soundname == "AMAZING_1"){
        soundToPlay ="IWohoo1.ogg";
    }else if (soundname == "AMAZING_2"){
        soundToPlay ="IWohoo2.ogg";
    }else if (soundname == "AMAZING_3"){
        soundToPlay ="IWohoo3.ogg";
    }else if (soundname == "AMAZING_4"){
        soundToPlay ="IWooHoo1.ogg";
    }else if (soundname == "AMAZING_5"){
        soundToPlay ="IWohoo3.ogg";

    }else if (soundname == "ANGRY_1"){
        soundToPlay ="IErrgh1.ogg";
    }else if (soundname == "ANGRY_2"){
        soundToPlay ="IOh1.ogg";
    }else if (soundname == "ANGRY_3"){
        soundToPlay ="IOoh1.ogg";
    }else if (soundname == "ANGRY_4"){
        soundToPlay ="IErrgh1.ogg";
    }else if (soundname == "ANGRY_5"){
        soundToPlay ="IOoh1.ogg";


    }else if (soundname == "ATTENTION_1"){
        soundToPlay ="IUh1.ogg";
    }else if (soundname == "ATTENTION_2"){
        soundToPlay ="IWohoo1.ogg";
    }else if (soundname == "ATTENTION_3"){
        soundToPlay ="IWohoo3.ogg";
    }else if (soundname == "ATTENTION_4"){
        soundToPlay ="IWooHoo1.ogg";
    }else if (soundname == "ATTENTION_5"){
        soundToPlay ="IUh1.ogg";


    }else if (soundname == "CONFIRMATION_1"){
        soundToPlay ="IAha1.ogg";
    }else if (soundname == "CONFIRMATION_2"){
        soundToPlay ="IAha2.ogg";
    }else if (soundname == "CONFIRMATION_3"){
        soundToPlay ="IAha3.ogg";
    }else if (soundname == "CONFIRMATION_4"){
        soundToPlay ="INoProb1.ogg";
    }else if (soundname == "CONFIRMATION_5"){
        soundToPlay ="IAck1.ogg";


    }else if (soundname == "DIALOG_1"){
        soundToPlay ="IAw1.ogg";
    }else if (soundname == "DIALOG_2"){
        soundToPlay ="I0h2.ogg";
    }else if (soundname == "DIALOG_3"){
        soundToPlay ="IDiag1.ogg";
    }else if (soundname == "DIALOG_4"){
        soundToPlay ="IDiagAlrt1.ogg";
    }else if (soundname == "DIALOG_5"){
        soundToPlay ="IDiag2.ogg";


    }else if (soundname == "ERROR_1"){
        soundToPlay ="IFailure1.ogg";
    }else if (soundname == "ERROR_2"){
        soundToPlay ="IFailure2.ogg";
    }else if (soundname == "ERROR_3"){
        soundToPlay ="IFailure3.ogg";
    }else if (soundname == "ERROR_4"){
        soundToPlay ="IFailure2.ogg";
    }else if (soundname == "ERROR_5"){
        soundToPlay ="IFailure1.ogg";


    }else if (soundname == "THINKING_1"){
        soundToPlay ="IThink1.ogg";
    }else if (soundname == "THINKING_2"){
        soundToPlay ="IMmm1.ogg";
    }else if (soundname == "THINKING_3"){
        soundToPlay ="IMmm2.ogg";
    }else if (soundname == "THINKING_4"){
        soundToPlay ="IMmm1.ogg";
    }else if (soundname == "THINKING_5"){
        soundToPlay ="IThink1.ogg";


    }else if (soundname == "WARNING_1"){
        soundToPlay ="IAttention1.ogg";
    }else if (soundname == "WARNING_2"){
        soundToPlay ="IBatWarn2.ogg";
    }else if (soundname == "WARNING_3"){
        soundToPlay ="IBatWarn3.ogg";
    }else if (soundname == "WARNING_4"){
        soundToPlay ="IBatWarn4.ogg";
    }else if (soundname == "WARNING_5"){
        soundToPlay ="IBatWarn5.ogg";
    }
	
	return soundToPlay;
	
}

int main(int argc, char **argv){

    ros::init(argc, argv, "interaction_executor_nonverbalsounds");  //enroll the name of the node
    ros::NodeHandle nodo;   //node to perform communication

    ROS_INFO("Started node play_non_verbal_sounds");

    ros::Subscriber subResults = nodo.subscribe("play_sound", 0, nonVerbalSoundsCallback);


    ros::Rate loopRate(100);

    while (ros::ok()){   //until Ctr+C is received in terminal

        //neccesary for callbacks
        ros::spinOnce();

        loopRate.sleep();
    }

    return 0;
}
