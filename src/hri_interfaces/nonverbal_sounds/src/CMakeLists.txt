#non_verbal_sound
add_executable(play_non_verbal_sounds_node.exe
               play_non_verbal_sounds.cpp)

target_link_libraries(play_non_verbal_sounds_node.exe
                    ${catkin_LIBRARIES})

add_dependencies(play_non_verbal_sounds_node.exe monarch_msgs_generate_messages_cpp)
