------------------------------------------------------------------------
- Description
------------------------------------------------------------------------
The "asr" package is in charge of providing automatic speech recognition functionalities.
Two different ASR softwares have been integrated: Google ASR and PocketSphinx.
Google ASR is the default ASR software.

Both require a microphone.

In order to succesfully use ASR functionalities, we need to test the correct operation of the microphone and the ASR software itself.

------------------------------------------------------------------------
- How to test microphone
------------------------------------------------------------------------
After connecting the microphone to the hri computer, run in a terminal form this computer:
	$>rec recordingsfile.wav
	
After a few seconds making noises and utterances, stop the program using Ctrl+C.
Then, play the recording sounds using the next command:
	$>play recordingsfile.wav

The sounds has to be played in the robot. If not, review the setup of the sound configurations and the drivers of the microphone.
	
------------------------------------------------------------------------
- How to test Google ASR
------------------------------------------------------------------------
Run 
	$>roslaunch asr asr_test.launch robot:=mbotXX
	
and start speaking Portuguese to the microphone.

The transcription and the confidence of the recognized text will be displayed in the terminal.

Besides, several topics are published:
/mbotXX/user_speaking_open_grammar : voice activity detection based on energy customizable threshold. A string message is published containing the values "START" or "STOP".
	$>rostopic echo /mbotXX/user_speaking_open_grammar

/mbotXX/open_grammar_results : this topic informs about the literal transcription, the confidence, and the language of the recognized utterance. It uses a message of type asr_msgs/open_grammar_recog_results.
	$>rostopic echo /mbotXX/open_grammar_results 

/mbotXX/audio/speech_rec : this topic provides semantic information about the recognized utterance. The semantic values are defined in the file "asr/data/commands_accepted.txt". The message published in this topic is monarch_msgs/RecognizedSpeech.
	$>rostopic echo /mbotXX/audio/speech_rec

