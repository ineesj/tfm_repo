#include "GoogleASR/open_grammar_asr_skill.h"
#include "debug/error.h"
#include "string/StringUtils.h"
#include "time/timer.h"
#include "system/system_utils.h"
#include "time/timer.h"
#include <errno.h>
#include <time.h>
#include <ros/package.h>

unsigned int SAMPLE_RATE=8000;  //FRECUENCIA MUESTREO
const int WINDOW_SIZE = 1024;   //TAMANYO BUFFER DE MUESTREO
ros::Duration MAX_SECONDS_WITHOUT_VOICE(0.4); // THRESHOLD IN MILISECONDS WITHOUT VOICE SAMPLES TO CONSIDERING END OF SPEECH
const int ZCR_THRESHOLD = 160;   //ZCR LIMTE PARA DIFERENCIAR VOZ DE RUIDO

const std::string OpenGrammarAsrSkill::general_filename_prefix = "/tmp/openASRGrammarLogs_";
const std::string OpenGrammarAsrSkill::raw_filename_suffix = ".raw";
const std::string OpenGrammarAsrSkill::flac_filename_suffix = ".flac";
const std::string OpenGrammarAsrSkill::wget_output_filename_prefix = "wget_output";

////////////////////////////////////////////////////////////////////////////////

OpenGrammarAsrSkill::RecogResults::RecogResults(const std::string & content_,
                                                const double confidence_,
                                                const Translator::LanguageId language_,
                                                Translator::LanguageMap language_map_):
  content(content_), confidence(confidence_), language(language_){
  this->language_map = language_map_;
}

////////////////////////////////////////////////////////////////////////////////

OpenGrammarAsrSkill::RecogResults
OpenGrammarAsrSkill::RecogResults::from_string(const std::string &mcp_string,
                                               Translator::LanguageMap& language_map){
  double confidence;
  Translator::LanguageId lang;
  std::istringstream in_buf(mcp_string);
  in_buf >> confidence >> lang;
  // get everything left in the stream
  int stream_position = (int) in_buf.tellg();
  std::string content = in_buf.str().substr(1 + stream_position);
  return RecogResults(content, confidence, lang, language_map);
}

////////////////////////////////////////////////////////////////////////////////

std::string OpenGrammarAsrSkill::RecogResults::to_string_nice() const {
  std::ostringstream ans;
  ans << "'" << content << "' (lang:" << language << "), conf:" << confidence;
  return ans.str();
}

////////////////////////////////////////////////////////////////////////////////

std::string OpenGrammarAsrSkill::RecogResults::to_string() const {
  std::ostringstream ans;
  ans << confidence << ' ' << language << ' ' << content;
  return ans.str();
}

////////////////////////////////////////////////////////////////////////////////

bool OpenGrammarAsrSkill::RecogResults::match(const std::string & rule) const {
  // extract the language versions
  std::vector<std::string> versions;
  StringUtils::StringSplit(rule, "|", &versions);

  // iterate on the versions
  for (std::vector<std::string>::const_iterator version = versions.begin();
       version != versions.end() ; ++version) {
    // get the language of this version
    Translator::LanguageId version_lang;
    // check if there is match
    if (!Translator::get_language_id_from_country_domain
        (language_map, *version, version_lang))
      continue;
    if (version_lang != language)
      continue;
    if (content.find( version->substr(version->find(':') + 1) )
        == std::string::npos)
      continue;
    //maggieDebug2("Match with the rule '%s'", version->c_str());
    return true;
  } // end loop version
  return false;
}

////////////////////////////////////////////////////////////////////////////////

OpenGrammarAsrSkill::OpenGrammarAsrSkill(){
  current_language = Translator::LANGUAGE_SPANISH;
  Translator::build_languages_map(language_map);
  nh_private_ = ros::NodeHandle("~"); // private node handle --> to pass it params
  int publish_rate;
  nh_private_.param("publish_rate", publish_rate,DEFAULT_LOOP_RATE);
  publisher_Results = nh_public_.advertise<recog_msg> (TOPIC_RESULTS_ASR, DEFAULT_LOOP_RATE);
  publisher_UserSpeaking = nh_public_.advertise<std_msgs::String> (TOPIC_USER_SPEAKING, DEFAULT_LOOP_RATE);
  ROS_INFO("Node %s created",ros::this_node::getName().c_str());
}

////////////////////////////////////////////////////////////////////////////////

OpenGrammarAsrSkill::~OpenGrammarAsrSkill() {
}

////////////////////////////////////////////////////////////////////////////////

void OpenGrammarAsrSkill::custom_launch() {

  ROS_INFO("open_grammar_asr_skill acitvated");

  //boramos las posibles frases temporales de ejecuciones anteiores

  std::ostringstream instr;
  instr << "rm " << general_filename_prefix << "* -rf";
  system_utils::exec_system( instr.str() );

  current_index = 1;

  averageVolume = 0;
  totalVolumeAccumulated=0;
  numIter=0;
  averageVolumeNoise = 0;
  totalVolumenAccumulatedNoise = 0;
  numIterNoise=0;


  //CONFIG THE INPUT STREAM
  ROS_DEBUG("Preparing the input stream to capture audio\n");
  int err;

  //abrimos el dispositivo
  if ((err = snd_pcm_open (&capture_handle,DEVICE_USED,SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    ROS_ERROR ("cannot open audio device %s (%s)", DEVICE_USED, snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
    ROS_ERROR ("cannot allocate hardware parameter structure (%s)", snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params_any (capture_handle, hw_params)) < 0) {
    ROS_ERROR ("cannot initialize hardware parameter structure (%s)", snd_strerror (err));
    return;
  }

  unsigned int resample = 1;
  if ((err = snd_pcm_hw_params_set_rate_resample(capture_handle, hw_params, resample)) < 0){
    ROS_ERROR("cannot fix resample(%s)",snd_strerror(err));
    return;
  }

  if ((err = snd_pcm_hw_params_set_access (capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    ROS_ERROR ("cannot set access type (%s)", snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params_set_format (capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
    ROS_ERROR("cannot set sample format (%s)", snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &SAMPLE_RATE, 0)) < 0) {
    ROS_ERROR("cannot set sample rate (%s)", snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params_set_channels (capture_handle, hw_params, 1)) < 0) {
    ROS_ERROR("cannot set channel count (%s)", snd_strerror (err));
    return;
  }

  if ((err = snd_pcm_hw_params (capture_handle, hw_params)) < 0) {
    ROS_ERROR("cannot set parameters (%s)", snd_strerror (err));
    return;
  }

  snd_pcm_hw_params_free (hw_params);

  if ((err = snd_pcm_prepare (capture_handle)) < 0) {
    ROS_ERROR("cannot prepare audio interface for use (%s)",snd_strerror (err));
    return;
  }
  ROS_DEBUG("Prepared audio capture. All right!");


}



////////////////////////////////////////////////////////////////////////////////

void OpenGrammarAsrSkill::custom_end() {
  snd_pcm_close (capture_handle);
}

////////////////////////////////////////////////////////////////////////////////

void OpenGrammarAsrSkill::proceso() {


  short buf[WINDOW_SIZE];
  int err;
  int volumen = 0; //volumen del buffer de audio leido
  int numEscritos = 0;
  int iterationsWithoutVoice = 0;
  int countCrossingXaxis;
  bool lastSampleWasPossitive=true;
  bool speaking = false;
  int fdFichAudio = 0;  //descriptor del fichero donde se guardan las muestras de audio
  int contadorPalabras = 0;
  ros::Time currentTime;
  ros::Duration milisecondsWithoutVoice;

  std::ostringstream raw_filename_stream;  //para nombrar los ficheros temporales
  std::string raw_filename;  //nombre dle fichero

  std_msgs::String message;




  while(true){


      try{

          //leemos las muestras de audio
          if ((err = snd_pcm_readi (capture_handle, buf, WINDOW_SIZE))  < 0) {
              ROS_ERROR ("Read from audio interface failed (%s), number: %d", snd_strerror (err),err);
              sleep(5);
              if ((err = snd_pcm_recover(capture_handle,err,0)) < 0){
                  ROS_ERROR ("Imposible to recover from audio read error (%s), number: %d", snd_strerror (err),err);
              }
              sleep(5);
              continue;
          }


          //calculamos el volumen de las muestras de audio y el ZCR
          //countCrossingXaxis = 0;      //total counts of amplitude signal (time domain) cross the X axis
          volumen  = 0;
          for( int ind = 0; ind < err; ind++ ){
              volumen += abs( buf[ind] );
              /*if (lastSampleWasPossitive && buf[ind]<0){  //the sign has changed
                  //countCrossingXaxis++;
                  lastSampleWasPossitive=false;
              }else if (!lastSampleWasPossitive && buf[ind]>0){  //the sign has changed
                  countCrossingXaxis++;
                  lastSampleWasPossitive=true;
              }*/
          }
          volumen = volumen / err;

          totalVolumeAccumulated += volumen;
          numIter ++;
          averageVolume = totalVolumeAccumulated / numIter;
          ROS_DEBUG("Volumen: %d\n",volumen);

          //compute Signal To Noise Ratio
          /*if (averageVolumeNoise!= 0){
              SNR = volumen / averageVolumeNoise;
              ROS_DEBUG("Average volume: %d, SNR: %f",averageVolume, SNR);
          }*/

          //SE CORRESPONDE EL BUFFER CON MUESTRAS DE VOZ
          if  ((volumen > VOLUME_THRESHOLD)){ // && (countCrossingXaxis < ZCR_THRESHOLD)){
              //if (SNR > 20){
              if (!speaking){
                  ROS_INFO(MAKE_GREEN "--> USER_SPEAKING_START (google asr) (%d volumen)" RESET_COLOR,volumen);
                  message.data= "START";
                  publisher_UserSpeaking.publish(message);
                  //abrimos el fichero donde vamos a ir almacenando las muestras de audio
                  raw_filename_stream.str("");
                  raw_filename_stream << general_filename_prefix << current_index << raw_filename_suffix;
                  raw_filename = raw_filename_stream.str();
                  ROS_DEBUG("The current recording will be called '%s'", raw_filename.c_str());
                  if  ((fdFichAudio = open(raw_filename.c_str(),O_CREAT |O_RDWR, 0644 )) < 0){
                      ROS_ERROR("No se puede abrir el fichero donde se grabaran los datos de audio");
                  }
              }

              //almacenamos en el fichero el buffer
              numEscritos = write(fdFichAudio,buf,sizeof(short)*err);
              if (numEscritos < 0){
                  ROS_ERROR ("No se han escrito las muestras de audio en el fichero");
                  return;
              }

              time_stamp_last_voice_sample = ros::Time::now();
              iterationsWithoutVoice=0;
              speaking = true;

          //no se considera voz
          }else{

              //ROS_DEBUG("Update background noise ");
              /*totalVolumenAccumulatedNoise += volumen;
              numIterNoise++;
              averageVolumeNoise = totalVolumenAccumulatedNoise / numIterNoise;
              ROS_DEBUG("Average volume noise: %d",averageVolumeNoise);
              */

              currentTime = ros::Time::now();
              milisecondsWithoutVoice = currentTime - time_stamp_last_voice_sample;


              //pausa entre palabras
              if (speaking && (milisecondsWithoutVoice < MAX_SECONDS_WITHOUT_VOICE)){

                  //almacenamos en el fichero el buffer de audio también
                  numEscritos = write(fdFichAudio,buf,sizeof(short)*err);
                  if (numEscritos < 0){
                      ROS_ERROR ("No se han escrito las muestras de audio en el fichero");
                      return;
                  }


                  if (iterationsWithoutVoice==0){
                      contadorPalabras++;
                      ROS_INFO("Palabra num %d", contadorPalabras);
                  }

                  iterationsWithoutVoice++;


              //fin de la locucion
              }else{

                  if (speaking){

                      //almacenamos en el fichero la ultima lectura del buffer de audio
                      numEscritos = write(fdFichAudio,buf,sizeof(short)*err);
                      if (numEscritos < 0){
                          ROS_ERROR ("No se han escrito las muestras de audio en el fichero");
                          return;
                      }


                      ROS_INFO(MAKE_GREEN "<-- USER_SPEAKING_STOP (google asr)" RESET_COLOR);
                      message.data= "STOP";
                      publisher_UserSpeaking.publish(message);
                      close(fdFichAudio);


                      //CONVERTIMOS DE RAW A FLAC
                      //sox -e signed -t raw -r 8k  -b 16  -c 1 in.raw  out.flac

                      std::ostringstream instr;
                      instr << "sox -e signed -t raw -r 8k  -b 16  -c 1 ";
                      instr << raw_filename << " ";
                      instr << general_filename_prefix << current_index << flac_filename_suffix;
                      //maggieDebug2("Converting to flac:  %s.", instr.str().c_str() );
                      //maggiePrint("---> Fichero temporal: %s%d",general_filename_prefix.c_str(),current_index);
                      int return_value = system(instr.str().c_str());
                      //if (return_value != 0) maggieError("The instruction '%s' returned %i != 0",instr.str().c_str(), return_value);



                      //ENVIAMOS LA PETICIONA GOOGLE ASR
                      ROS_DEBUG ("........Peticion a GoogleASR.......");
                      data = new DataThreadServer();
                      data->this_ptr = this;
                      data->sentence_to_analyze_index = current_index;
                      pthread_t thread_index;
                      int returnValue = pthread_create(&thread_index, NULL,thread_communication_server_static, data);
                      if (returnValue == -1)
                          ROS_ERROR("Failed creating the server thread, errror:%s",strerror(errno));
                      pthread_detach(thread_index);
                      //thread_indices.push_back(thread_index);

                      contadorPalabras = 0;
                      current_index++;
                      speaking = false;
                      iterationsWithoutVoice= 0;
                  } //fin if



              } //fin if
          } //fin else
      }catch (std::exception e){
        ROS_ERROR( MAKE_RED "Se ha producido una excepcion '%s'" RESET_COLOR,e.what());
        sleep(3);
   }

  } //fin while
}




////////////////////////////////////////////////////////////////////////////////

void* OpenGrammarAsrSkill::thread_communication_server_static(void* ptr) {
  //maggieDebug2("thread_communication_server_static()");
  DataThreadServer* data = (DataThreadServer*) ptr;
  data->this_ptr->thread_communication_server(data->sentence_to_analyze_index);
  delete data;
  pthread_exit(NULL);
}

////////////////////////////////////////////////////////////////////////////////


/**
  En este hilo se hace la peticion al servicio de Google ASR
  */
void OpenGrammarAsrSkill::thread_communication_server(int sentence_index) {


  try{

  /*PETICION A GOOGLE ASR:
            wget -U "Mozilla/5.0" --post-file=t.flac --header="Content-Type:
            audio/x-flac; rate=8000" -O OpenGrammarAsrSkill_wget_output1
            "http://www.google.com/speech-api/v1/recognize?lang=es&client=chromium"
    */

  Timer t;

  // generate flac filename and wget filename
  std::ostringstream instr;
  instr << general_filename_prefix << sentence_index << flac_filename_suffix;
  std::string flac_filename = instr.str();
  instr.str("");
  instr << general_filename_prefix << wget_output_filename_prefix << sentence_index;
  std::string wget_output_filename = instr.str();

  // generate the url
  instr.str("");
  //instr << "http://www.google.com/speech-api/v1/recognize?";
  //https://www.google.com/speech-api/v2/recognize?output=json&lang=en-us&key=AIzaSyCnl6MRydhw_5fLXIdASxkLJzcJh5iX0M4'
  instr << "https://www.google.com/speech-api/v2/recognize?";
  //instr << "output=json";
  instr << "lang="<< language_map.at(current_language);
  //instr << "&key=AIzaSyAIt7uz6JmHB6mvnQwzOQ-BCXCoRyVfoiY";
  instr << "&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw";
  //instr << "&client=chromium";
  //instr << "https://www.google.com/speech-api/v2/recognize?output=json&lang=en-us&key=AIzaSyCnl6MRydhw_5fLXIdASxkLJzcJh5iX0M4";
  std::string google_url = instr.str();

  // wget -U "Mozilla/5.0" --post-file=recording.flac --header="Content-Type: audio/x-flac; rate=8000" -O out "http://www.google.com/speech-api/v1/recognize?lang=en-us&client=chromium" ;
  // execute the wget
  instr.str("");
  instr << "wget -U \"Mozilla/5.0\"";
  instr << " --post-file=" << flac_filename;
  instr << " --header=\"Content-Type: audio/x-flac; rate=8000\"";
  instr << " -O " << wget_output_filename;
  instr << " \"" << google_url << "\" > /dev/null 2>&1";

  int return_value = system(instr.str().c_str());
  //if (return_value != 0)  maggiePrint("The instruction '%s' returned %i != 0",instr.str().c_str(), return_value);

  // read the results
  std::string wget_output_content;
  StringUtils::retrieve_file(wget_output_filename, wget_output_content);

  //maggieDebug1("wget_output_content:%s", wget_output_content.c_str());
 if (wget_output_content.size() == 0) {
    ROS_INFO("-->REC_FAIL_OPEN_GRAMMAR");

  } else {

      // extract the fields from the server answer
      ROS_DEBUG("GoogleASR rsponse: %s",wget_output_content.c_str());
      int pos = 0;
      std::string content = StringUtils::extract_from_tags(wget_output_content, "\"transcript\":\"", "\"", pos);
      std::string confidence_str = StringUtils::extract_from_tags(wget_output_content, "\"confidence\":", "}", pos);
      double confidence = StringUtils::cast_from_string<double>(confidence_str);


      // generate the utterance
      RecogResults utterance(content, confidence, current_language, language_map);
      //printf("=== RECOG RESULTS: GOOGLE ASR ===\n");
      if (utterance.content.length() > 0){
          printf("\n\n>>>Google ASR trasncription: '" MAKE_GREEN "%s" RESET_COLOR "' (confidence: %f)\n", utterance.content.c_str(),utterance.confidence);
          publisher_Results.publish(fill_msg(utterance));
      }else{
          ROS_INFO( MAKE_RED "Google ASR no ha podido reconocer nada." RESET_COLOR);
      }


  }

  // clean the audio files generated for this index
#ifndef GENERATE_AUDIO_LOGS
  instr.str("");
  instr << "rm " << general_filename_prefix << sentence_index << "* > /dev/null 2>&1";
  //    maggieDebug2("Executing %s.", instr.str().c_str() );
  system( instr.str().c_str() );
#endif

  // clean the response files generated for this index
  instr.str("");
  instr << "rm " << general_filename_prefix << wget_output_filename_prefix << sentence_index << "*  > /dev/null 2>&1";
  //    maggieDebug2("Executing %s.", instr.str().c_str() );
  system_utils::exec_system( instr.str().c_str() );




  }catch (std::exception e){
       ROS_ERROR( MAKE_RED "Se ha producido una excepcion '%s'" RESET_COLOR,e.what());
  }

}

////////////////////////////////////////////////////////////////////////////////

void OpenGrammarAsrSkill::set_current_language(const Translator::LanguageId language) {
  current_language = language;
}



recog_msg OpenGrammarAsrSkill::fill_msg(const OpenGrammarAsrSkill::RecogResults &recog){
    recog_msg msg;
    msg.content = recog.content;
    msg.confidence = recog.confidence;
    msg.languageID = recog.language;
    return msg;
}
