#ifndef OPENGRAMMARASRSKILL_H
#define OPENGRAMMARASRSKILL_H

#include "translator/Translator.h"
#include "color/terminal_text_colors.h"
#include <pthread.h>
#include <alsa/asoundlib.h>
#include "ros/ros.h"
#include "asr_msgs/open_grammar_recog_results.h"
#include "std_msgs/String.h"
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <curl/curl.h>
#include <string>
#include <set>



#define DEVICE_USED "default"   //NOMBRE DEL MICROFONO A USAR
#define GENERATE_AUDIO_LOGS //si se quieren dejar los ficheros de audio o borrar en cada iteracion


using namespace std;

namespace {                   // anonymous namespace to prevent naming pollution
   static const std::string TOPIC_RESULTS_ASR = "open_grammar_results";
   static const std::string TOPIC_USER_SPEAKING = "user_speaking_open_grammar";

   static const int DEFAULT_LOOP_RATE = 5; // Hz
   typedef asr_msgs::open_grammar_recog_results recog_msg;
}

class OpenGrammarAsrSkill {

public:

    const static std::string general_filename_prefix;
    const static std::string flac_filename_suffix;
    const static std::string raw_filename_suffix;
    const static std::string wget_output_filename_prefix;

    //! the data class that will be shared with the listeners
    class RecogResults {
        public:
           RecogResults(const std::string & content,
                 const double confidence,
                 const Translator::LanguageId language,
                 Translator::LanguageMap language_map);

           //! a generator from a std::string
           RecogResults(){}

           static RecogResults from_string(const std::string & mcp_string,
                                    Translator::LanguageMap& language_map);

           //! the sentence
           std::string content;
           double confidence;
           Translator::LanguageId language;
           //! return true if the content matches the multi-language rule
           bool match(const std::string & rule) const;
           //! transforms the object to a string
           std::string to_string_nice() const;
           std::string to_string() const;
       private:
           Translator::LanguageMap language_map;
   };


    OpenGrammarAsrSkill();
    ~OpenGrammarAsrSkill();

    void set_current_language(const Translator::LanguageId language);

    //! the function that will be called cyclically
    void proceso();

    //! the functions that will be called during launch()
    void custom_launch();

    //! the functions that will be called during end()
    void custom_end();

    //! set a new volume threshold
    void set_volume_threshold(const int threshold_) {
        VOLUME_THRESHOLD = threshold_;
    }

    //! Fills a message with the recognition results
    recog_msg fill_msg(const OpenGrammarAsrSkill::RecogResults &recog);

private:


    //char charBuf[LONG_BUFFER_VOL+1];
    snd_pcm_t* capture_handle;
    snd_pcm_hw_params_t *hw_params;


    int VOLUME_THRESHOLD;
    float SNR;  //Signal to Noise Ratio



    //unsigned int SAMPLE_RATE;  //FRECUENCIA MUESTREO
    //int WINDOW_SIZE;   //TAMANYO BUFFER DE MUESTREO
    //int ZCR_THRESHOLD;
    //int MAX_MILISECONDS_WITHOUT_VOICE; //max time without voice samples (end of speech)
    int framesToRead;
    ros::Time time_stamp_last_voice_sample;

    int averageVolume;
    int totalVolumeAccumulated;
    int numIter;

    int totalVolumenAccumulatedNoise;
    int averageVolumeNoise;
    int standarDesviationVolumeNoise;
    int numIterNoise;


    //! the language we now use
    Translator::LanguageId current_language;
    //! all the possible languages
    Translator::LanguageMap language_map;

    ros::NodeHandle nh_private_, nh_public_;
    ros::Publisher publisher_Results,publisher_UserSpeaking;
    CURL *curl;

    unsigned long int current_index;
    //! this contains the sentences that are currently processed
    //std::set<SentenceIndex> current_sentences;

    //! the data class to feed the thread
    class DataThreadServer {
        public:
            OpenGrammarAsrSkill* this_ptr;
            unsigned long int sentence_to_analyze_index;
    };


    //! the thread for the communication with Google server - static version
    static void* thread_communication_server_static(void* ptr);
    //! the thread for the communication with Google server
    void thread_communication_server(int sentence_index);
    //! the indices of the thread
    std::vector<pthread_t> thread_indices;

    DataThreadServer* data;

    void results_analyzer(const RecogResults & utterance);
    void results_analyzer_language_changer(const RecogResults & utterance);

    //! Save the voice frames to a file
    bool saveVoiceToFile(const string & fileName);

    static void setLanguage_static(void* aux, int p) {
        ROS_DEBUG("setLanguage_static(%i)", p);
        ((OpenGrammarAsrSkill *) aux)->current_language = p;
    }
};

#endif // OPENGRAMMARASRSKILL_H
