
#include "GoogleASR/open_grammar_asr_skill.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "open_grammar_asr_skill");  //enroll the name of the node
    ROS_INFO("Started node open_grammar_asr_skill");

    OpenGrammarAsrSkill skill;

    //VOLUME THRESHOLD SET TO 1200
    skill.set_volume_threshold(1200);

    // Get the language from the parameter server
    ros::NodeHandle nh("~");
    std::string lang;
//    int lang_id = 0;
    Translator::LanguageId lang_id = 0;
    nh.getParam("language", lang);
    ROS_WARN("ASR Language is %s", lang.c_str());

    if (lang.compare("spanish") == 0) {
        lang_id = Translator::LANGUAGE_SPANISH;
    }
    else if (lang.compare("portuguese") == 0) {
        lang_id = Translator::LANGUAGE_PORTUGUESE;
    }

    // Set the language
    skill.set_current_language(lang_id);
    skill.custom_launch(); //for activate the ASR
    skill.proceso();
    ros::waitForShutdown();
    skill.custom_end();

}
