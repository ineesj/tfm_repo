/**
  * @author Fernando Alonso Martin
  * Reads the output of GoogleASR and find voice commands in this transcription
  * The commads are defined on a file /data/commands_accepted.txt"
  * If the command is recognized is published a std_msgs/String topic with the name "asr_command"
  */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include <sstream>
#include <string>
#include <ros/package.h>
#include "asr_msgs/open_grammar_recog_results.h"
#include "monarch_msgs/RecognizedSpeech.h"
#include "color/terminal_text_colors.h"
#include "string/StringUtils.h"
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;

const float CONFIDENCE_THRESHOLD = 0;
vector<string> commandsAccepted;
ros::Publisher commandsPublisher;

void asrResultsCallback(const asr_msgs::open_grammar_recog_results::ConstPtr& msg){

    ROS_INFO("Confidence %f", msg->confidence);
    ROS_INFO("Transcription %s", msg->content.c_str());

    string transcription = msg->content;
    string command;
    string semantic;

    boost::algorithm::to_lower(transcription);

    //search the valid commands on the transcription
    for (int i =0; i < commandsAccepted.size(); ++i){
        command = commandsAccepted.at(i);
        vector<string> litSem;
        StringUtils::StringSplit(command,":",&litSem);
        command =  litSem.at(0);
        boost::algorithm::to_lower(command);

        //ROS_INFO("Command to search: %s", command.c_str());
        semantic = litSem.at(1);
        //ROS_INFO("Semantic of the command: %s", semantic.c_str());

        if (transcription.find(command)  != std::string::npos){  //subset of the transcription
            if (msg->confidence > CONFIDENCE_THRESHOLD ){
                monarch_msgs::RecognizedSpeech commandToPublish;
                commandToPublish.recognized = transcription;
                commandToPublish.confidence = msg->confidence ;
                commandToPublish.semantic = semantic;


                commandsPublisher.publish(commandToPublish);
                ROS_INFO ( MAKE_GREEN "Command recognized: %s" RESET_COLOR, command.c_str() );
            }else{
                ROS_INFO ( MAKE_RED "Command rejected for low confidence: %s" RESET_COLOR, command.c_str() );
            }
        }
    }
}



int main(int argc, char **argv){

    ros::init(argc, argv, "word_spotting_parser");  //enroll the name of the node
    ros::NodeHandle nodo;   //node to perform communication

    ROS_INFO("Started node word_spotting_parser");

    ros::Subscriber subResults = nodo.subscribe("open_grammar_results", 0, asrResultsCallback);


    //commandsPublisher = nodo.advertise<std_msgs::String>("asr_command",0);
    commandsPublisher = nodo.advertise<monarch_msgs::RecognizedSpeech>("audio/speech_rec",0);


    ROS_INFO("Reading accepted commands");
    string commandsToRecognizeFile = ros::package::getPath("asr")+"/data/commands_accepted.txt";
    StringUtils::retrieve_file_split(commandsToRecognizeFile,commandsAccepted,true,true);

    for (int i=0; i < commandsAccepted.size(); ++i){
        ROS_INFO ("%s",commandsAccepted.at(i).c_str());
    }

    ros::spin();

    ros::waitForShutdown();

    return 0;
}
