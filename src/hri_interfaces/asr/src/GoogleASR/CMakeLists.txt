add_library(open_grammar_asr_skill open_grammar_asr_skill.cpp)
target_link_libraries(open_grammar_asr_skill translator
                open_grammar_asr_skill args_parser
                 mxml_utils asound timer string_utils ${catkin_LIBRARIES})

add_dependencies(open_grammar_asr_skill asr_msgs_generate_messages_cpp)


add_executable(launcher_open_grammar_asr_skill.exe launcher_open_grammar_asr_skill.cpp)
target_link_libraries(launcher_open_grammar_asr_skill.exe open_grammar_asr_skill ${catkin_LIBRARIES})


add_executable(word_spotting_parser.exe word_spotting_parser.cpp)
add_dependencies(word_spotting_parser.exe asr_msgs_generate_messages_cpp)
target_link_libraries(word_spotting_parser.exe string_utils ${catkin_LIBRARIES})


install(TARGETS open_grammar_asr_skill word_spotting_parser.exe
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
