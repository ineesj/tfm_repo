
/*!
  Clase que hereda de liveliness_ros.h
  Esta clase se encarga de genereasr los  mensajes para el moviento de los brazos.
  el topic "head/command" se encarga de dar la posición de los brazos y "speed_command_topic"
  se encarga de dar velocidad al motor. 

  */



#include <liveliness.h>
#include "dynamixel_speed/speed_command.h"



using namespace std;


class Livelinees_arms_mini:  public Liveliness {

protected:
  
    ros::Publisher _mesanjeEnviar_position_armLeft; 
    std_msgs::Float64 position_armLeft_msg;

    ros::Publisher _mesanjeEnviar_position_armRight; 
    std_msgs::Float64 position_armRight_msg;

    ros::Publisher _mesanjeEnviar_speed; 
    dynamixel_speed::speed_command speed_msg;

    
    ros::NodeHandle _node;
private: 
    float velocidad;

public:

    Livelinees_arms_mini( ){ 

        _mesanjeEnviar_position_armLeft= _node.advertise<std_msgs::Float64>("leftArm/command", 0);
        _mesanjeEnviar_position_armRight= _node.advertise<std_msgs::Float64>("rightArm/command", 0);
        _mesanjeEnviar_speed = _node.advertise<dynamixel_speed::speed_command>("speed_command_topic", 0);
        
    };


  

    virtual void execution(){

        int f=round((1/frecuencia)*randNum); // frecuencia de muestreo 1/f por un valor random entre 150 y 100
        cout << "valor f: " << f << endl;
        if(contador==f || contador>f){ 

            y=valorY; 
            // a parte de la posicion se quiere cambiar la velocidad en función de la frecuencia, más frecuencia más vel:
            // el valor máximo de velocidad es 0.5 por lo que se normalizara el valor ente 0-0,5 como máxima frecuencia  
            // se pondrá un valor de 30. YA VERE COMO PONGO ESTE VALOR
            velocidad=frecuencia/60;
            generate_msg();
            randNum = rand()%(150-100 + 1) + 100;
            cout << "valor aleatorio: " << randNum << endl;
            contador=0;
        }
        else{  contador=contador+1; }

    }

    
    virtual void generate_msg(){

        if (start == true ){
            cout<<" Publicooooooooooooooooo mensaje"<< endl;
            float angleright = y;
            float angleleft =-(y);
            position_armLeft_msg.data=angleleft;
            position_armRight_msg.data=angleright;
            speed_msg.leftArm= velocidad;
            speed_msg.rightArm= velocidad;


            _mesanjeEnviar_position_armLeft.publish(position_armLeft_msg);
            _mesanjeEnviar_position_armRight.publish(position_armRight_msg);
            _mesanjeEnviar_speed.publish(speed_msg);

        }   
    }

    virtual void liveliness_interfaces(){
        if(armsEnable == false)  { // interface stop: se pone a false el movimiento
            float angleright = 0;
            float angleleft =0;
            position_armLeft_msg.data=angleleft;
            position_armRight_msg.data=angleright;
            speed_msg.leftArm= 0;
            speed_msg.rightArm= 0;


            _mesanjeEnviar_position_armLeft.publish(position_armLeft_msg);
            _mesanjeEnviar_position_armRight.publish(position_armRight_msg);
            _mesanjeEnviar_speed.publish(speed_msg);

            start=false;
        
        }else if( armsEnable == true){ // interface start: se ejecuta main_bucle()

            start = true;
            main_bucle();
        }

    }



      

};


