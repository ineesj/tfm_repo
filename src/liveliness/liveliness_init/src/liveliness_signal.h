/*!
  Clase signal: se encarga de generar la señal de vivacidad,  todos los nodos dependen de esta señal para generar su comportamiento
  	- se subcribe a topic:  
  		"liveliness_signal" en el cual está la información de la amplitud de la señal y de la frecuencia
  	- pueblia los topics:
  	"liveliness_frecuencia" que proporcia la frecuencia de la señal ya que enfunción de ella se hace el muestreo de la señal en cada nodo
	"liveliness_valorY" el valor de la señal por segundo

	methodos:
		- signal_bucle: bucle  principal donde se calculan los segundos y se llama a signal_generation para que genere la señal
		- signal_generation: genera la señal
		
	signalParameters_Callback
  */


#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "liveliness_init/signalParameters.h"

#include <iostream>
#include <time.h>
using namespace std;

class Liveliness_signal{


protected: 

	string mood;
	ros::Publisher _mesanjeEnviarFrecuencia;
	ros::Publisher _mesanjeEnviarValorY;
	ros::Subscriber _SignalParameters;

	ros::NodeHandle _node;

	std_msgs::Float64 mensajeFrecuencia;
	std_msgs::Float64 mensajeValorY;

private: 



	float amplitud;
	float frecuencia;
	float valorY;

public: 
	Liveliness_signal(){

    	_SignalParameters= _node.subscribe("liveliness_signal", 0, &Liveliness_signal::signalParameters_Callback, this); //se subcribe el mensaje
    	_mesanjeEnviarFrecuencia = _node.advertise<std_msgs::Float64>("liveliness_frecuencia",0);   //publica el mensaje
    	_mesanjeEnviarValorY = _node.advertise<std_msgs::Float64>("liveliness_valorY",0);  //publica el mensaje
    	signal_bucle(); 

	}

	void signal_bucle(){    
    
	    time_t start = time(0);
	    ros::Rate rate(1);

	    while (ros::ok()){

	        int seconds= difftime( time(0), start);
	        ROS_INFO("Tiempo %d",seconds);
	        signal_generation(seconds,amplitud,frecuencia);
	        mensajeFrecuencia.data=frecuencia;
	        _mesanjeEnviarFrecuencia.publish(mensajeFrecuencia);
	  
	        ros::spinOnce();
	        rate.sleep();
    	}

	}

	void signalParameters_Callback(const liveliness_init::signalParameters::ConstPtr& msg){ // Estado  de ánimo del robot
    	amplitud=msg->amplitud;
    	frecuencia=msg->frecuencia;
	}


	void signal_generation(int seconds,float amplitud, float frecuencia){

		int pi=3.14159;
		//if ( signal_type==0){
		valorY = amplitud*sin(2*pi*seconds*frecuencia);
		ROS_INFO(" A=%f", valorY);
		mensajeValorY.data=valorY;
	    _mesanjeEnviarValorY.publish(mensajeValorY);

		//}

		/////////////////////////////////////////////// SEÑAL TRIANGULAR///////////////////////////////////////////////////////////////////////
		/*if ( signal_type==1){

			valorY = (2*amplitud/pi)*asin(sin(2*pi*seconds*frecuencia));
			ROS_INFO(" A=%f", valorY);
			return valorY;  

		}
		/////////////////////////////////////////////// SEÑAL CUADRADA ///////////////////////////////////////////////////////////////////////
		if ( signal_type==2){

			float valorY_= 2*amplitud*sin(2*pi*seconds*frecuencia) ;
			ROS_INFO("  A=%f", valorY);
			if (valorY_> 0 ){
				valorY=amplitud;
			}
			if (valorY_< 0 ){
				valorY=-amplitud;
			}
			if (valorY_= 0 ){
				valorY=0;
			}

			return valorY;
			cout<< " A=%f" << valorY<< endl;
		}*/
	}



};

