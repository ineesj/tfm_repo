/*!
  Clase colors: hereda de la calse liveliness_ros.
  clase de la cual heredan los nodos realcionados con los colores, dichos nodos son comunes al resto pero además tiene que leer el valor
  del color de la vivacidad en cada momento.

  Para ello se subcribe el topic  "liveliness_Colors" que contiene el valor de RGB. 

  colorsCallback
  */

#include "liveliness.h"
#include "monarch_msgs/LedControl.h"
#include "liveliness_init/colorsParameters.h"



using namespace std;


class Liveliness_colors:  public Liveliness {


protected:

    int Ry;
    int Gy;
    int By;

    int r;
    int g;
    int b;
        
    int rActual;
    int gActual;
    int bActual;
  
    ros::Subscriber _subscriptor_colors;
    ros::NodeHandle _node;

public:

    Liveliness_colors(){ 

        _subscriptor_colors= _node.subscribe("liveliness_Colors", 0, &Liveliness_colors::colorsCallback, this); //se subcribe el mensaje

        Ry=255;
        Gy=255;
        By=255;
        rActual=255;
        gActual=255;
        bActual=255;
        r =255;
        g =255;
        b =255;

    };

    void colorsCallback(const liveliness_init::colorsParameters::ConstPtr& msg){ 
    
        Ry=msg->R;
        Gy=msg->G;
        By=msg->B;  
    
    }


};


