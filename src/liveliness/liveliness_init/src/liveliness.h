/*!
  Clase madre, esta clase contiene todos lo métodos cumunes a todos los nodos de vivacidad:
    Contiene los subcriptores a los topic:
        - liveliness_frecuencia: que da inforamción de la frecuencia de la señal
        - liveliness_valorY: que da información del valor de Y de la señal cada segundo
        - liveliness_interfaces: que da información de que interfaces están en false(off) o true(on)
    Metodos:
        -main_bucle: contiene el bucle principal, cada segundo ejecuta el método execution
        -execution: este metodo virtual se encarga de interpretar el valor de frecuencia y valor Y en cada uno de los nodos
        -generate_msg: genera el mensaje en función de lo que le ordene execution
        -liveliness_interfaces: se encarga de poner a start o a stop cada nodo en función de si están a true o a false.
    Callback:
        -frecuenciaCallback
        -valorYCallback
        - EnableInterfaceCallback
  */


#include "ros/ros.h"
#include <iostream>
#include <time.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "liveliness_init/colorsParameters.h"
#include "liveliness_init/onOffInterfaces.h"
#include <ros/package.h>

using namespace std;


class Liveliness{


public:

    Liveliness(){
        _subscriptor_frecuecia = _node.subscribe("liveliness_frecuencia", 0, &Liveliness::frecuenciaCallback, this); //se subcribe el mensaje
        _subscriptor_valorY= _node.subscribe("liveliness_valorY", 0, &Liveliness::valorYCallback, this); //se subcribe el mensaje
        _subscriptor_EnableInterface= _node.subscribe("liveliness_interfaces", 0, &Liveliness::EnableInterfaceCallback, this); //se subcribe el mensaje


        contador=0;
        randNum=100;
    };


    void main_bucle(){
       
        time_t start = time(0);
        ros::Rate rate(1);

        while (ros::ok()){
           
            execution();
            
            cout<< frecuencia << endl;
            ros::spinOnce();
            rate.sleep();
        }
        
    }

    virtual void execution()=0;
    virtual void generate_msg()=0;
    virtual void liveliness_interfaces()=0;
   


private:

    bool okFrecuencia;
    bool okValorY;
    bool okColores; 

protected:

    

    int contador;
    int randNum; 
    float frecuencia;
    float y;
    float valorY;
    
    bool start;

    bool headEnable;
    bool armsEnable;
    bool eyesEnable;
    bool baseEnable;
    bool screenEnable;
    bool neckEnable;
    
   
    ros::Subscriber _subscriptor_frecuecia;
    ros::Subscriber _subscriptor_valorY; 
    ros::Subscriber _subscriptor_EnableInterface;
    ros::NodeHandle _node;

    void frecuenciaCallback(const std_msgs::Float64::ConstPtr& msg){ 
        
        frecuencia=msg->data;
       

    }

    void valorYCallback(const std_msgs::Float64::ConstPtr& msg){ 
        valorY=msg->data;  
      
    }



    void EnableInterfaceCallback(const liveliness_init::onOffInterfaces::ConstPtr& msg){ 
    
        headEnable=msg->head;
        armsEnable=msg->arms;
        eyesEnable=msg->eyes;
        screenEnable=msg->screen;
        baseEnable=msg->base;
	    neckEnable=msg->neck;

        liveliness_interfaces();
    
    }

    
    
    





   

      
};
