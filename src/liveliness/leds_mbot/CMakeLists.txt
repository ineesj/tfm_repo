cmake_minimum_required(VERSION 2.8.3)
project(leds_mbot)

find_package(catkin REQUIRED COMPONENTS
  monarch_msgs
  message_generation
  interaction_executor_manager
  roscpp
  roslib
  rospy
  std_msgs
  liveliness_init

)


catkin_package(   INCLUDE_DIRS src   CATKIN_DEPENDS roscpp rospy std_msgs liveliness_init )


include_directories(
  include src 
  ${catkin_INCLUDE_DIRS}
  /liveliness/liveliness_init/src/


)



add_executable(liveliness_eyes_mbot src/liveliness_eyes_mbot.cpp)
add_dependencies(liveliness_eyes_mbot monarch_msgs_generate_messages_cpp )
target_link_libraries(liveliness_eyes_mbot
   ${catkin_LIBRARIES}
)


add_executable(liveliness_base_mbot src/liveliness_base_mbot.cpp)
add_dependencies(liveliness_base_mbot monarch_msgs_generate_messages_cpp )
target_link_libraries(liveliness_base_mbot
   ${catkin_LIBRARIES}
)
