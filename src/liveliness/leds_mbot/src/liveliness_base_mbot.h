/*!
  Clase que hereda de liveliness_ros_colors.h
  Esta clase se encarga de genereasr los  mensajes para el control de las luces de la base
  Publica el topic "cmd_leds"

  */


#include "liveliness_colors.h"
#include "monarch_msgs/LedControl.h"



using namespace std;


class Liveliness_base_mbot: public Liveliness_colors {

private:

    int device;
    int contadorDevice;

protected:
  
    ros::Publisher _mesanjeEnviar;
    monarch_msgs::LedControl mensaje;
    ros::NodeHandle _node;

public:

    Liveliness_base_mbot(){ 

        _mesanjeEnviar = _node.advertise<monarch_msgs::LedControl>("cmd_leds",0);
       
        contadorDevice=0;
        
    };
    
    
    virtual void generate_msg(){
        
        if(start== true){
            mensaje.device=device;
            mensaje.r=r;
            mensaje.g=g;
            mensaje.b=b;
            mensaje.change_time=0;
            cout<< "R:"<< r <<"G :"<< g <<"B : "<< b <<endl ;
            _mesanjeEnviar.publish(mensaje);
      
       }   
    }

     virtual void liveliness_interfaces(){
        if(baseEnable == false)  { // interface stop: se pone todas las luces a 0
            mensaje.device=device;
            mensaje.r=0;
            mensaje.g=0;
            mensaje.b=0;
            mensaje.change_time=0;
            _mesanjeEnviar.publish(mensaje);

            start=false;
        
        }else if( baseEnable == true){ // interface start se ejecuta el main_bucle

            start = true;
            main_bucle();
        }

    }

    virtual void execution(){ // se va activando y cambiando el color de cada una de las luces de la base

        //parpadeo
        int f=round((1/frecuencia)*randNum);
        cout << "valor f: " << f << endl;
        
        if(contador==f || contador>f){ 
        
            randNum = rand()%(150-100 + 1) + 100;
            contadorDevice=contadorDevice+1;

            
            if (contadorDevice==1){ 
                device=3; 
    
            }
            if (contadorDevice==2){
                device=4;
                
            }
            if (contadorDevice==3){
                device=5; 
                contadorDevice=0;
            }
            contador=0;    
            cout<<"device:"<<device<<endl;

            generate_msg();
        }
        else{
            // cambio de color en la base en fución de los colores que se leen en "liveliness_colors"
            contador=contador+1; 
    
            if(Ry == rActual && Gy == gActual  && By == bActual  ) {  r=Ry; g=Gy; b=By; generate_msg(); }
            if(Ry == rActual && Gy == gActual  && By > bActual  )  { if ( bActual <= 255 ) { r=Ry; g=Gy; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry == rActual && Gy == gActual  && By < bActual  )  { if ( bActual >= 0 ) { r=Ry; g=Gy; bActual=bActual-5; b=bActual; generate_msg();  } }
            if(Ry == rActual && Gy < gActual  && By == bActual  )  { if ( gActual >= 0 ) { r=Ry;  gActual=gActual-5; g=gActual;  b=By; generate_msg();} }
            if(Ry == rActual && Gy < gActual  && By < bActual  )  { if ( bActual >= 0 && gActual >= 0 ) { r=Ry; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By < bActual  )  { if ( gActual >= 0 && bActual<=255 ){ r=Ry;  gActual=gActual-5; g=gActual;  bActual=bActual+5; b=bActual;  generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By > bActual  )  { if ( bActual <= 255 && gActual<= 255 ) { r=Ry; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By == bActual  )  { if ( gActual <= 255 ) { r=Ry;  gActual=gActual+5; g=gActual;  b=By; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By < bActual  )  { if ( gActual <= 255 && bActual >= 0){ r=Ry;  gActual=gActual+5; g=gActual;  bActual=bActual-5; b=bActual;generate_msg(); } }

            if(Ry > rActual && Gy > gActual  && By > bActual  )  { if ( rActual <= 255 && gActual <= 255 && bActual <= 255  ) {rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy > gActual  && By == bActual  )  { if ( rActual <= 255 && gActual <= 255) { rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; b=By; generate_msg();} }
            if(Ry > rActual && Gy > gActual  && By < bActual  )  { if ( rActual <= 255 && gActual <= 255 && bActual >= 0  ) {  rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By > bActual  )  { if ( rActual <= 255 && bActual <=255 ) { rActual=rActual+5; r=rActual; g=Gy; bActual=bActual+5; b=bActual;generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By == bActual  )  { if ( rActual <= 255 ) { rActual=rActual+5; r=rActual; g=Gy; b=By;  generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By < bActual  )  { if ( rActual <= 255 && bActual >= 0 ) { rActual=rActual+5; r=rActual; g=Gy; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By > bActual  )  { if ( rActual <= 255 && gActual >= 0 && bActual <=255 ) { rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By == bActual  )  { if ( rActual <= 255 && gActual >= 0 ) { rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; b=By; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By < bActual  )  { if ( rActual <= 255 && gActual >= 0  &&  bActual >= 0 ) {  rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg();} }

            if(Ry < rActual && Gy < gActual  && By < bActual  )  { if ( rActual >= 0 && gActual >= 0 && bActual >= 0 ) {  rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy < gActual  && By == bActual  )  { if ( rActual >= 0 && gActual >= 0) { rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; b=By;generate_msg(); } }
            if(Ry < rActual && Gy < gActual  && By > bActual  )  { if ( rActual >= 0 && gActual >= 0 && bActual <= 255) {  rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual+5; b=bActual;generate_msg();  } }
            if(Ry < rActual && Gy == gActual  && By == bActual  )  { if ( rActual >= 0 ) { rActual=rActual-5; r=rActual; g=Gy; b=By;  } }
            if(Ry < rActual && Gy == gActual  && By < bActual  )  { if ( rActual >= 0 && bActual >= 0 ) { rActual=rActual-5; r=rActual; g=Gy; bActual=bActual-5; b=bActual;generate_msg();  } }
            if(Ry < rActual && Gy == gActual  && By > bActual  )  { if ( rActual >= 0 && bActual <= 255 ) { rActual=rActual-5; r=rActual; g=Gy; bActual=bActual+5; b=bActual; } }
            if(Ry < rActual && Gy > gActual  && By < bActual  )  { if ( rActual >= 0  && gActual <= 255 && bActual >= 0) {  rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy > gActual  && By > bActual  )  { if ( rActual >= 0 && bActual <= 255 && gActual <= 255 ) {rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy > gActual  && By == bActual  )  { if ( rActual >= 0 && gActual <= 255 ) { rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; b=By; generate_msg(); } }

            
            

        }
       

    }


};


