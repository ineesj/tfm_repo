/*!
  Clase que hereda de liveliness_ros_colors.h
  Esta clase se encarga de genereasr los  mensajes para el control de las luces de los ojos ( y control del parpadeo luces a 0)
  Publica el topic "cmd_leds"

  */

#include "liveliness_colors.h"
#include "monarch_msgs/LedControl.h"



using namespace std;


class Liveliness_eyes_mbot:  public Liveliness_colors {


protected:
  
    ros::Publisher _mesanjeEnviar;
    monarch_msgs::LedControl mensaje;
    monarch_msgs::LedControl mensaje2;
    ros::NodeHandle _node;

public:

    Liveliness_eyes_mbot(){ 

        _mesanjeEnviar = _node.advertise<monarch_msgs::LedControl>("cmd_leds",0);
    
        
    };

    virtual void liveliness_interfaces(){ // interface stop se ponen los ojos a 0
        if(eyesEnable == false)  {
            mensaje.device=0;
            mensaje.r=0;
            mensaje.g=0;
            mensaje.b=0;
            mensaje.change_time=0;

            mensaje2.device=1;
            mensaje2.r=0;
            mensaje2.g=0;
            mensaje2.b=0;
            mensaje2.change_time=0;
            cout<< "R:"<< r <<"G :"<< g <<"B : "<< b <<endl ;
            _mesanjeEnviar.publish(mensaje);
            _mesanjeEnviar.publish(mensaje2);
            start=false;
         
        }else if( eyesEnable == true){ // interface start se ejercuta main_bucle

            start = true;
            main_bucle();
        }

    }


    virtual void generate_msg(){
 
        if(start == true ){ 
            mensaje.device=0;
            mensaje.r=r;
            mensaje.g=g;
            mensaje.b=b;
            mensaje.change_time=0;

            mensaje2.device=1;
            mensaje2.r=r;
            mensaje2.g=g;
            mensaje2.b=b;
            mensaje2.change_time=0;
            cout<< "R:"<< r <<"G :"<< g <<"B : "<< b <<endl ;
            _mesanjeEnviar.publish(mensaje);
            _mesanjeEnviar.publish(mensaje2);
        } 
    }

    virtual void execution(){ 

        int f=round((1/frecuencia)*randNum);
        cout << "valor f: " << f << endl;
        
        if(contador==f || contador>f){  // aquí hay que traducir la sinusoidal en parpadeo... R=0 G=0 B=0
            randNum = rand()%(100-50 + 1) + 50;
            cout << "valor aleatorio: " << randNum << endl;        
            r=0;
            g=0;
            b=0;
            cout<< r << endl;
            cout << g << endl;
            cout << b << endl;
            contador=0;
            
            generate_msg();
        }
        else{

            contador=contador+1; 
            // cambio de color en la base en fución de los colores que se leen en "liveliness_colors"
    
            if(Ry == rActual && Gy == gActual  && By == bActual  ) { cout<<"estamos aquí"<<endl; r=Ry; g=Gy; b=By; generate_msg(); }
            if(Ry == rActual && Gy == gActual  && By > bActual  )  { if ( bActual <= 255 ) { r=Ry; g=Gy; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry == rActual && Gy == gActual  && By < bActual  )  { if ( bActual >= 0 ) { r=Ry; g=Gy; bActual=bActual-5; b=bActual; generate_msg();  } }
            if(Ry == rActual && Gy < gActual  && By == bActual  )  { if ( gActual >= 0 ) { r=Ry;  gActual=gActual-5; g=gActual;  b=By; generate_msg();} }
            if(Ry == rActual && Gy < gActual  && By < bActual  )  { if ( bActual >= 0 && gActual >= 0 ) { r=Ry; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By < bActual  )  { if ( gActual >= 0 && bActual<=255 ){ r=Ry;  gActual=gActual-5; g=gActual;  bActual=bActual+5; b=bActual;  generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By > bActual  )  { if ( bActual <= 255 && gActual<= 255 ) { r=Ry; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By == bActual  )  { if ( gActual <= 255 ) { r=Ry;  gActual=gActual+5; g=gActual;  b=By; generate_msg();} }
            if(Ry == rActual && Gy > gActual  && By < bActual  )  { if ( gActual <= 255 && bActual >= 0){ r=Ry;  gActual=gActual+5; g=gActual;  bActual=bActual-5; b=bActual;generate_msg(); } }

            if(Ry > rActual && Gy > gActual  && By > bActual  )  { if ( rActual <= 255 && gActual <= 255 && bActual <= 255  ) {rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy > gActual  && By == bActual  )  { if ( rActual <= 255 && gActual <= 255) { rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; b=By; generate_msg();} }
            if(Ry > rActual && Gy > gActual  && By < bActual  )  { if ( rActual <= 255 && gActual <= 255 && bActual >= 0  ) {  rActual=rActual+5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By > bActual  )  { if ( rActual <= 255 && bActual <=255 ) { rActual=rActual+5; r=rActual; g=Gy; bActual=bActual+5; b=bActual;generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By == bActual  )  { if ( rActual <= 255 ) { rActual=rActual+5; r=rActual; g=Gy; b=By;  generate_msg(); } }
            if(Ry > rActual && Gy == gActual  && By < bActual  )  { if ( rActual <= 255 && bActual >= 0 ) { rActual=rActual+5; r=rActual; g=Gy; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By > bActual  )  { if ( rActual <= 255 && gActual >= 0 && bActual <=255 ) { rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By == bActual  )  { if ( rActual <= 255 && gActual >= 0 ) { rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; b=By; generate_msg(); } }
            if(Ry > rActual && Gy < gActual  && By < bActual  )  { if ( rActual <= 255 && gActual >= 0  &&  bActual >= 0 ) {  rActual=rActual+5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg();} }

            if(Ry < rActual && Gy < gActual  && By < bActual  )  { if ( rActual >= 0 && gActual >= 0 && bActual >= 0 ) {  rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy < gActual  && By == bActual  )  { if ( rActual >= 0 && gActual >= 0) { rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; b=By;generate_msg(); } }
            if(Ry < rActual && Gy < gActual  && By > bActual  )  { if ( rActual >= 0 && gActual >= 0 && bActual <= 255) {  rActual=rActual-5; r=rActual; gActual=gActual-5; g=gActual; bActual=bActual+5; b=bActual;generate_msg();  } }
            if(Ry < rActual && Gy == gActual  && By == bActual  )  { if ( rActual >= 0 ) { rActual=rActual-5; r=rActual; g=Gy; b=By;  } }
            if(Ry < rActual && Gy == gActual  && By < bActual  )  { if ( rActual >= 0 && bActual >= 0 ) { rActual=rActual-5; r=rActual; g=Gy; bActual=bActual-5; b=bActual;generate_msg();  } }
            if(Ry < rActual && Gy == gActual  && By > bActual  )  { if ( rActual >= 0 && bActual <= 255 ) { rActual=rActual-5; r=rActual; g=Gy; bActual=bActual+5; b=bActual; } }
            if(Ry < rActual && Gy > gActual  && By < bActual  )  { if ( rActual >= 0  && gActual <= 255 && bActual >= 0) {  rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual-5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy > gActual  && By > bActual  )  { if ( rActual >= 0 && bActual <= 255 && gActual <= 255 ) {rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; bActual=bActual+5; b=bActual; generate_msg(); } }
            if(Ry < rActual && Gy > gActual  && By == bActual  )  { if ( rActual >= 0 && gActual <= 255 ) { rActual=rActual-5; r=rActual; gActual=gActual+5; g=gActual; b=By; generate_msg(); } }

            
            

        }
       

    }


};


