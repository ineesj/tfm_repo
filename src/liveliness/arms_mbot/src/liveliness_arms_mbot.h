
/*!
  Clase que hereda de liveliness_ros.h
  Esta clase se encarga de genereasr los  mensajes para el moviento de los brazos.
  Publica el topic "cmd_arms"

  */

#include <liveliness.h>
#include "monarch_msgs/ArmsControl.h"


using namespace std;


class Livelinees_arms_mbot:  public Liveliness {

protected:
  
    ros::Publisher _mesanjeEnviar; 
    monarch_msgs::ArmsControl mensaje;
    ros::NodeHandle _node;

public:

    Livelinees_arms_mbot( ){ 

        _mesanjeEnviar = _node.advertise<monarch_msgs::ArmsControl>("cmd_arms",0);
        
    };

  

    virtual void execution(){

        int f=round((1/frecuencia)*randNum); // frecuencia de muestreo 1/f por un valor random entre 150 y 100
        cout << "valor f: " << f << endl;
        if(contador==f || contador>f){ 
            y=valorY;
            generate_msg();
            randNum = rand()%(150-100 + 1) + 100;
            cout << "valor aleatorio: " << randNum << endl;
            contador=0;
        }
        else{  contador=contador+1; }

    }

    
    virtual void generate_msg(){

        if (start == true ){
            cout<<" Publicooooooooooooooooo mensaje"<< endl;
            float angleright = y;
            float angleleft =-(y);
            int executionTime=2;
            bool executionMovement= true;
            mensaje.torque_enable=executionMovement;
            mensaje.left_arm=angleleft;
            mensaje.right_arm=angleright;
            mensaje.movement_time= executionTime;
            _mesanjeEnviar.publish(mensaje);
        }   
    }

    virtual void liveliness_interfaces(){
        if(armsEnable == false)  { // interface stop: se pone a false el movimiento
            float angleright = 0;
            float angleleft =0;
            int executionTime=0;
            bool executionMovement= false;
            mensaje.torque_enable=executionMovement;
            mensaje.left_arm=angleleft;
            mensaje.right_arm=angleright;
            mensaje.movement_time= executionTime;
            _mesanjeEnviar.publish(mensaje);

            start=false;
        
        }else if( armsEnable == true){ // interface start: se ejecuta main_bucle()

            start = true;
            main_bucle();
        }

    }



      

};


