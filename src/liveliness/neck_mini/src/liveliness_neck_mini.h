/*!
  Clase que hereda de liveliness_ros.h
  Esta clase se encarga de genereasr los  mensajes para el moviento de los cabeza.
  Publica el topic "cmd_head"

  */

#include "liveliness.h"
#include "dynamixel_speed/speed_command.h"

using namespace std;


class Liveliness_neck_mini:  public Liveliness {

protected:
    


    ros::Publisher _mesanjeEnviar_position; 
    std_msgs::Float64 position;

    ros::Publisher _mesanjeEnviar_speed; 
    dynamixel_speed::speed_command speed_msg;

    ros::NodeHandle _node;

private: 
    float velocidad;


public:

    Liveliness_neck_mini(){ 

        _mesanjeEnviar_position= _node.advertise<std_msgs::Float64>("neck/command", 0);
        _mesanjeEnviar_speed = _node.advertise<dynamixel_speed::speed_command>("speed_command_topic", 0);
        
    };


    virtual void liveliness_interfaces(){
        if(neckEnable == false)  {// interface stop: se pone a false el movimiento

            position.data=0;
            speed_msg.neck= 0;
    
            _mesanjeEnviar_position.publish(position);
            _mesanjeEnviar_speed.publish(speed_msg);
            start=false;
        
        }else if( neckEnable == true){// interface start: se ejecuta main_bucle()

            start = true;
            main_bucle();
        }

    }


    virtual void execution(){

        int pi=3.14159;
        int f=round((1/frecuencia)*randNum); // frecuencia de muestreo 1/f por un valor random entre 150 y 100// interface stop: se pone a false el movimiento
 // interface start: se ejecuta main_bucle()
        cout << "valor f: " << f << endl;
        if(contador==f || contador>f){ 
            y=valorY;
            velocidad=frecuencia/60;
            generate_msg();
            randNum = rand()%(150-100 + 1) + 100;
            cout << "valor aleatorio: " << randNum << endl;
            contador=0;
        }
        else{  contador=contador+1;  cout << "contadoooorr" << contador << endl;}

    }

    virtual void generate_msg(){
        if(start== true){
            cout<<" Publicooooooooooooooooo mensaje"<< endl;

            position.data=y;
            speed_msg.neck= velocidad;
    
            _mesanjeEnviar_position.publish(position);
            _mesanjeEnviar_speed.publish(speed_msg);
          
    
        }
    }



};


