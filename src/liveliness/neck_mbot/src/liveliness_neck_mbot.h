/*!
  Clase que hereda de liveliness_ros.h
  Esta clase se encarga de genereasr los  mensajes para el moviento de los cabeza.
  Publica el topic "cmd_head"

  */

#include "liveliness.h"
#include "monarch_msgs/HeadControl.h"


using namespace std;


class Liveliness_neck_mbot:  public Liveliness {

protected:
    
    ros::Publisher _mesanjeEnviar; 
    monarch_msgs::HeadControl mensaje;
    ros::NodeHandle _node;

private: 
    float w;

public:

    Liveliness_neck_mbot(){ 

        _mesanjeEnviar = _node.advertise<monarch_msgs::HeadControl>("cmd_head",0);
        
    };


    virtual void liveliness_interfaces(){
        if(neckEnable == false)  {// interface stop: se pone a false el movimiento

            mensaje.head_pos=0;
            mensaje.movement_speed=0;
            _mesanjeEnviar.publish(mensaje);
            start=false;
        
        }else if( neckEnable == true){// interface start: se ejecuta main_bucle()

            start = true;
            main_bucle();
        }

    }


    virtual void execution(){

        int pi=3.14159;
        int f=round((1/frecuencia)*randNum); // frecuencia de muestreo 1/f por un valor random entre 150 y 100// interface stop: se pone a false el movimiento
 // interface start: se ejecuta main_bucle()
        cout << "valor f: " << f << endl;
        if(contador==f || contador>f){ 
            y=valorY;
            w=2*pi*frecuencia;
            generate_msg();
            randNum = rand()%(150-100 + 1) + 100;
            cout << "valor aleatorio: " << randNum << endl;
            contador=0;
        }
        else{  contador=contador+1;  cout << "contadoooorr" << contador << endl;}

    }

    virtual void generate_msg(){
        if(start== true){
            cout<<" Publicooooooooooooooooo mensaje"<< endl;
            mensaje.head_pos=y;
            mensaje.movement_speed=w;
            _mesanjeEnviar.publish(mensaje);
            contador=0;
        }
    }



};


