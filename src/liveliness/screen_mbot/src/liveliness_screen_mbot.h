
/*!
  Clase que hereda de liveliness_ros_colors.h
  Esta clase se encarga de genereasr los  mensajes para el control de la tablet:
    frecuencia de "parpadeo" del gif
    color del centro del corazón del gif
    tamaño del corazón
  Publica el topic "screen/display_image"

  Este nodo coge de Data: blue_heart1.png y blue_heart3.png
  y almacena blue_heart1newColor.png con el color nuevo ( esta imagen se va sobreescribiendo)
  */
#include "liveliness_colors.h"
#include <Magick++.h>
#include <Magick++/Image.h>
#include "std_msgs/String.h"
#include "liveliness_init/signalParameters.h"



using namespace std;
using namespace Magick; 


class Liveliness_screen_mbot: public Liveliness_colors{

private:

float amplitud;
int rInicial;
int bInicial;
int gInicial;
float amplitudInicial;
float frecuenciaIncial;  //variable para comparar la amplitud anterio y la nueva para cambiar el tamaño del corazón
bool cambioColor;
int tamanoIMGscreen;
protected:

    ros::Publisher _mesanjeEnviar;
    ros::Subscriber _amplitudMaxima;
    std_msgs::String mensaje;
    ros::NodeHandle _node;

public:

    Liveliness_screen_mbot(){ 

        _mesanjeEnviar = _node.advertise<std_msgs::String>("screen/display_image",0);
        _amplitudMaxima= _node.subscribe("liveliness_signal", 0, &Liveliness_screen_mbot::maxAmplitud_Callback, this); 
       /*! se subcribe el mensaje "liveliness_signal" con el objetivo de conseguir la ampitudMaxima 
       y normalizar el tamaño de la imagen, para que no sea excesivamente grande */
        rInicial=0;
        bInicial=0;
        gInicial=0;
        amplitudInicial=0;
        frecuenciaIncial=0;
        cambioColor=false;
        tamanoIMGscreen=700; // tamaño imagen tabler incluyendo corazón y margen

        
    };

    virtual void liveliness_interfaces(){
        if(screenEnable == false)  { // interface stop pantalla reset
            mensaje.data="reset";
            _mesanjeEnviar.publish(mensaje);
            start=false;
        
        }else if( screenEnable == true){ // interface start se ejecuta main_bucle

            start = true;
            main_bucle();
        }

    }

    void maxAmplitud_Callback(const liveliness_init::signalParameters::ConstPtr& msg){
        amplitud=msg->amplitud;
       

    }


  

    virtual void execution(){ 

        int f=round((1/frecuencia)*randNum);  
        // frecuencia de muestreo 1/f por un valor random entre 150 y 100  que se itiliza para el "parpadeo" gif. y muestreo de ValorY para cambiar el tamaño imagen.
        cout << "valor f: " << f << endl;
        cout << contador << endl;
        if(contador==f || contador>f){ 
            randNum = rand()%(150-100 + 1) + 100; //para coger la frecuencia de muestreo 
            cout << "valor aleatorio: " << randNum << endl;
            string rutaRead=ros::package::getPath("liveliness_init");
            // En primer lugar cambio de color la el corazón del centro en funcion de los parámetros de color de la vivacidad:

            if( rInicial!=Ry || gInicial!=Gy  || bInicial!=By) {
                Image i;
                cambioColor=true;
                string ImagenI = rutaRead + "/data/blue_heart1.png";
                i.read(ImagenI);
                
                int h = i.rows();
                int w = i.columns();
                // los rangos de RGB en Magick++ son difrentes consultarlos en: See http://www.imagemagick.org/Magick++/Color.html
                // se calcula el rango para que los valores de RGB estén comprendidos entre 0 y 255
                int range = pow(2, i.modulusDepth());
                assert(range > 0); 
                PixelPacket *pixels = i.getPixels(0, 0, w, h);
                // ahora accedemos a cada uno de los píxeles de la imagen para ver que valor tienen:
                int row = 0;
                int column = 0;
                Color color = pixels[w * row + column];
                for(row=0; row<h-1; row++){
                    for(column=0; column<w-1; column++){
                        Color color = pixels[w * row + column];
                        int colorR= (color.redQuantum() / range);
                        int colorB =(color.blueQuantum() / range); 
                        int colorG  =(color.greenQuantum() / range); 
            
                        // como lo que queremos cambiar es el centro de la imgen y sabemos que es azul ponemos la siguiente condición:
                        if( colorB <= 255 && colorB >= 200 && colorR <=30 && colorG<=30){
                            // cambiamos los colores por el valor deseado Ry, Gy, By( todos multiplicados por el valor range propio de magick++)
                            i.pixelColor(column,row, Color(Ry*range,Gy*range,By*range)); 
                        
                        }
                    }

                }
                // se almacena la imagen para luego leerla: 
                string rutaIcolor= rutaRead +  "/data/blue_heart1newColor.png";
                i.write(rutaIcolor);
                rInicial=Ry;
                gInicial=Gy;
                bInicial=By;
                cout<< "rInicial : " << rInicial << "gInicial : " << gInicial << "bInicial : " << bInicial << endl;
            // se genera la lista de imágenes para el gif
            }

            if(frecuenciaIncial!= frecuencia || amplitudInicial!=amplitud || cambioColor==true){
                list<Image> imageList;
                list<Image> coalescedList;
                list<Image> resizeList;
               
                string rutaImagen1 = rutaRead + "/data/blue_heart1newColor.png";
                string rutaImagen2 = rutaRead + "/data/blue_heart3.png";
                readImages(&imageList, rutaImagen1);
                readImages(&imageList, rutaImagen2);
                coalesceImages(&coalescedList, imageList.begin(), imageList.end());
                list<Image>::iterator it = imageList.begin();
                it = coalescedList.begin();
        
                   // se genera el gif con la frecuencia deseada y se redimensiona la imagen
                while(it != coalescedList.end()){
                    Image image = (*it);
                    float factormultiplicador=((amplitud/4)+0.5); // con esto lo que hago es trasforamar la amplitu de esntre 0-2 a 0.5 y 1 y lo uso como factor multiplicador para reescalar la imganen
                    //inferfaces como máximo la amplitud va a se de 2 2 y algo
                    cout<< "factor multiplicador: " << factormultiplicador << endl;
                    if (factormultiplicador < 0.5){ factormultiplicador= 0.5; } // porque se es menor que 350 la imagen se ve muy pequeña
                    if (factormultiplicador > 1){ factormultiplicador= 1; } // por si acaso se pone una amplitud mayor
                    cout<< "factor multiplicador 2 : " << factormultiplicador << endl;
                    int imageResize=tamanoIMGscreen*factormultiplicador;// para normalizarlo entre 0 y 1 para que la imagen no sea muy grande
                    ostringstream convertImage;   
                    convertImage << imageResize;
                    string valorImage=convertImage.str();
                    string newSizeImage= valorImage + "x" +valorImage;
                    image.resize(newSizeImage);
        
                       // Se genera el marco mayor o menor en funcion del tamaño del corazón para que todas la imagenes sean 600*600
                    image.backgroundColor( Magick::Color( 0,0,0 ) );
                    int frameRerize=tamanoIMGscreen;-imageResize;
                    ostringstream convertFrame;  
                    convertFrame << frameRerize;
                    string valorFrame=convertFrame.str();
                       // se hace un cambio de tamaño del corazón en función del valor Y 
                    string newSizeFrame= valorFrame +"x" +valorFrame;
                    image.animationDelay(f*10);
                    image.borderColor("white");
                    image.matteColor("White");
                    image.frame(newSizeFrame);
                  
                       
                    resizeList.push_back(image);
                    ++it;

                } 

                frecuenciaIncial=frecuencia;
                amplitudInicial=amplitud;
                cambioColor=false;
                   // se almacena la imagen en el proyecto screen ya que tiene que estár alli para ser mostrada en la pantalla.
                string rutaWrite=ros::package::getPath("screen") + "/data/corazonVivacidad.gif" ;
                writeImages(resizeList.begin(), resizeList.end(), rutaWrite);
            }
            
    
            generate_msg(); 
            contador=0;
             
        }

        else{ contador=contador+1; }
    }

    virtual void generate_msg(){
        if(start== true){
            mensaje.data="corazonVivacidad.gif";
            _mesanjeEnviar.publish(mensaje); // publica el mensaje con el nombre de la imagen obtenida en este nodo
        }

        
          
    }

};


