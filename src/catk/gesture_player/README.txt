
To launch each interaction experiment you need to run the gesture_player launcher.

    $ roslaunch gesture_player gesture_player.launch  
    
Then publish the following topic:

    $ rostopic pub /mbotXX/keyframe_gesture_filename std_msgs/String file_name -1
    
Where 'file_name' is the name of one XML file, all of them are inside '/interactor_executor/gesture_player/data/'.

  
