#ifndef GESTURE_LEDS_PLAYER_ROS_H
#define GESTURE_LEDS_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

//Add the library's message topic
#include "monarch_msgs/LedControl.h"


/*!
---------------------
LeftEyeLeds
RightEyeLeds
CheeksLeds
BaseRightLeds
BaseFrontLeds
BaseLeftLeds
----------------------
r;g;b;time ==> 255;0;0;0.2
----------------------
  */

class GestureLedsPlayerRos : public GesturePlayerRos {
public:
  GestureLedsPlayerRos() {
//    ROS_INFO("GestureLedsPlayerRos ctor");

    _leds_command_publisher = _nh_public.advertise<monarch_msgs::LedControl>("cmd_leds", 10);

  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    /*! if necessary */
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
	/*! if necessary */
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
      ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
      gesture_io::JointValue key_value = _joint_values.at(key_idx);
//      ROS_INFO("%s: Sleeping till %f, then parameters '%s'...",
//               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
      ros::Time::sleepUntil(key_time);


      //Generate and send message to the leds
      std::vector<std::string> r_g_b_time = string_split(key_value, ';');
      if (r_g_b_time.size() != 4) { //4 = number of necessary msg parameters
          ROS_ERROR("Missing parameters to configure, should be 4 values but this was recived: %s ", key_value.c_str());
          return;	  
      }
      monarch_msgs::LedControl msg;
      msg.device = getDeviceFromKey(_joint_name);
      msg.r = atoi(r_g_b_time[0].c_str());
      msg.g = atoi(r_g_b_time[1].c_str());
      msg.b = atoi(r_g_b_time[2].c_str());
      msg.change_time = atof(r_g_b_time[3].c_str());
	  
      //ROS_INFO("Sending to the %s the following device;r;g;b;change_time: %s", _joint_name.c_str(), key_value.c_str());
	  	  
      _leds_command_publisher.publish(msg);


    } // end loop key_idx
  }
  
   ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
	//TODO
    }
	
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

private:
  //! the publisher in contact with the Leds driver
  ros::Publisher _leds_command_publisher;
  
  int getDeviceFromKey(std::string device){
	if (device.compare("LeftEyeLeds") == 0)
		return 0;
	else if (device.compare("RightEyeLeds") == 0)
		return 1;
	else if (device.compare("CheeksLeds") == 0)
		return 2;
	else if (device.compare("BaseRightLeds") == 0)
		return 3;
	else if (device.compare("BaseFrontLeds") == 0)
		return 4;
	else if (device.compare("BaseLeftLeds") == 0)
		return 5;
  }
};

#endif // GESTURE_LEDS_PLAYER_ROS_H
