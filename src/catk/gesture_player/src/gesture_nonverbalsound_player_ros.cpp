#include "gesture_nonverbalsound_player_ros.h"


/*!
  A simple instantation of a GestureMonarchArmsPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_nonverbalsound_player_ros");
  GestureNonVerbalSoundPlayerRos player;
  ROS_INFO("Starting a GestureNonVerbalSoundPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
