#include "gesture_etts_player_ros.h"

/*!
  A simple instantation of a GestureEttsPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_etts_player_ros");
  GestureEttsPlayerRos player;
  ROS_INFO("Starting a GestureEttsPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
