#ifndef GESTURE_ARMS_PLAYER_ROS_H
#define GESTURE_ARMS_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

//Arms msg
#include "monarch_msgs/ArmsControl.h"

/*!

  */

class GestureArmsPlayerRos : public GesturePlayerRos {
public:
    GestureArmsPlayerRos() {

        _arms_command_publisher = _nh_public.advertise<monarch_msgs::ArmsControl>("cmd_arms", 1);

    } // end ctor

    ////////////////////////////////////////////////////////////////////////////

    /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
    virtual bool gesture_set_custom() {
        //ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
        // nothing to do
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! How to move to initial position.
      It must be blocking.
      */
    virtual void gesture_go_to_initial_position() {
        //ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
    virtual void gesture_play() {
//        ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                        _keytimes.size() << "keytimes");

        for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
            gesture_io::JointValue key_value = _joint_values.at(key_idx);
            double _keytime_it_before = key_idx == 0? 0 : _keytimes[key_idx-1];
            double sleep_time = _keytimes[key_idx] - _keytime_it_before;
//            ROS_INFO("%s: Sleeping %fs, then parameters '%s'...",
//                     _joint_name.c_str(), sleep_time, key_value.c_str());

            //ros::Time::sleepUntil(key_time);
            if(!waitUntil(sleep_time)){
//                ROS_INFO_STREAM(_joint_name << ":gesture_play() stopped");
                return;
            }


            //Generate and send message to the arms
            std::vector<std::string> torq_time_left_right = string_split(key_value, ';');
            if (torq_time_left_right.size() != 4) {
                torq_time_left_right.clear();
                //Default values
                torq_time_left_right.push_back("true");
                torq_time_left_right.push_back("3.0"); //seconds
                torq_time_left_right.push_back("0.0"); //0.0 pointing down, -PI/2 pointing frontt
                torq_time_left_right.push_back("0.0");
            }
            monarch_msgs::ArmsControl msg;
            std::vector<uint8_t> myVector(torq_time_left_right[0].begin(), torq_time_left_right[0].end());
            msg.torque_enable = myVector[0]; //(torq_time_left_right[0].c_str());
            msg.movement_time = atof(torq_time_left_right[1].c_str());
            msg.left_arm = atof(torq_time_left_right[2].c_str());
            msg.right_arm = atof(torq_time_left_right[3].c_str());

            //ROS_WARN("Sending to the arms the following torque;movement_time;left_position;right_position: %s", key_value.c_str());
            _arms_command_publisher.publish(msg);


        } // end loop key_idx
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
		monarch_msgs::ArmsControl msg;
        msg.torque_enable = false;        
        ROS_DEBUG("Sending to the arms the disable command");
        _arms_command_publisher.publish(msg);		
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

private:
    //! the publisher in contact with the monarch arms driver
    ros::Publisher _arms_command_publisher;
};

#endif // GESTURE_ARMS_PLAYER_ROS_H
