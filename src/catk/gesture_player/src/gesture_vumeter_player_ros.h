#ifndef GESTURE_VUMETER_PLAYER_ROS_H
#define GESTURE_VUMETER_PLAYER_ROS_H

#include <string>
// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>
// gesture_player
#include "gesture_player_ros.h"
// mouth vumeter control msg
#include "monarch_msgs/MouthVumeterControl.h"

/*!
 This player commands the mouth vumeter. You can enable/disable it and define the mouth style we use.
 In the xml tags related to the vumenter, the value can be a string containing two ;-separated substrings: "[true|false];[mouth style]"
 true - enables the vumeter
 false - disables the vumeter
 [mouth style] - possible options are "openmouth" and "softwave"
 */


class GestureVumeterPlayerRos : public GesturePlayerRos {
public:
  GestureVumeterPlayerRos() {
//    ROS_INFO("GestureVumeterPlayerRos ctor");

    _vumeter_command_publisher = _nh_public.advertise<monarch_msgs::MouthVumeterControl>("cmd_mouthVumeter", 0);


  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    /*! if necessary */
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
	/*! if necessary */
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
      ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
      gesture_io::JointValue key_value = _joint_values.at(key_idx);
//      ROS_INFO("%s: Sleeping till %f, then parameters '%s'...",
//               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
      ros::Time::sleepUntil(key_time);

      std::vector<std::string> values = string_split(key_value, ';');	// if there is more than one parameter
        if (values.size()>0) {
            monarch_msgs::MouthVumeterControl msg;
            //first parameter enables/disables de vumeter
            if(values[0]=="true") msg.mouthVumeter = true;
            else if (values[0]=="false") msg.mouthVumeter = false;
            else ROS_WARN("Mouth vumeter cannot be enabled/disabled with option %s", values[0].c_str());
            if (values.size()>1) {
                msg.mouthStyle = values[1];
            } else if (values.size()>1) {
                ROS_WARN("Too many parameters for the vumeter gesture player");
            }
            _vumeter_command_publisher.publish(msg);
        } else {
            ROS_WARN("Vumeter gesture player needs at least 1 parameter");
        }
 
    } // end loop key_idx
  }
  
   ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
    }
	
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

private:
	//! the publisher in contact with the Mouth vumeter
	ros::Publisher _vumeter_command_publisher;
	
};

#endif // GESTURE_VUMETER_PLAYER_ROS_H
