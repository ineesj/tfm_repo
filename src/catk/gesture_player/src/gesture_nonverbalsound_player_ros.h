#ifndef GESTURE_NONVERBALSOUND_PLAYER_ROS_H
#define GESTURE_NONVERBALSOUND_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

//Add the library's message topic
#include "monarch_msgs/NonVerbalSound.h"

/*!

  */

class GestureNonVerbalSoundPlayerRos : public GesturePlayerRos {
public:
  GestureNonVerbalSoundPlayerRos() {
//    ROS_INFO("GestureNonVerbalSoundPlayerRos ctor");

    _nonverbalsound_command_publisher = _nh_public.advertise<monarch_msgs::NonVerbalSound>("play_sound", 1);

  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    /*! if necessary */
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
	/*! if necessary */
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
      ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
      gesture_io::JointValue key_value = _joint_values.at(key_idx);
//      ROS_INFO("%s: Sleeping till %f, then parameters '%s'...",
//               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
      ros::Time::sleepUntil(key_time);


      //Generate and send message to the nonverbalsound
      monarch_msgs::NonVerbalSound msg;
      msg.soundname = key_value;
	  
      //ROS_DEBUG("Sending to the NonVerbalSound the following parameter1;parameter2;parameter3;parameter4: %s", key_value.c_str());
	 
	  
      _nonverbalsound_command_publisher.publish(msg);


    } // end loop key_idx
  }
  
   ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
    }
	
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

private:
  //! the publisher in contact with the NonVerbalSound driver
  ros::Publisher _nonverbalsound_command_publisher;
};

#endif // GESTURE_NONVERBALSOUND_PLAYER_ROS_H
