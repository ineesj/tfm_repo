#include "gesture_arms_player_ros.h"

/*!
  A simple instantation of a GestureMonarchArmsPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_arms_player_ros");
  GestureArmsPlayerRos player;
  ROS_INFO("Starting a GestureArmsPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
