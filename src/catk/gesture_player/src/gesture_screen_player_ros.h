#ifndef GESTURE_SCREENS_PLAYER_ROS_H
#define GESTURE_SCREENS_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

/*!
  A gesture player compatible with the low-level SW that controls
  the picture displayed on the screen on mbot

  For any keyframe, the joint_value for the Screen corresponds to
  the name of the picture to be displayed
  (including extension, i.e. "warning.jpg")

  These pictures should be by default in the "data" folder of the
  screen project.
  */

class GestureScreenPlayerRos : public GesturePlayerRos {
public:
  GestureScreenPlayerRos() {
//    ROS_INFO("GestureScreenPlayerRos ctor");

      _topic_name_display_image = _joint_name + "/display_image";
      _topic_name_display_video = _joint_name + "/display_video";

      _screen_command_image_publisher = _nh_public.advertise<std_msgs::String>(_topic_name_display_image, 1);

      _screen_command_video_publisher = _nh_public.advertise<std_msgs::String>(_topic_name_display_video, 1);
  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    // nothing to do
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx)
    {
        ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
        gesture_io::JointValue key_value = _joint_values.at(key_idx);
        //      ROS_INFO("%s: Sleeping till %f, then display image '%s'...",
        //               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
        ros::Time::sleepUntil(key_time);

        //Generate and send message to the screen
        std_msgs::String msg;

        // Hack to allow displaying either images or videos through te same topic
        std::vector<std::string> values = string_split(key_value, ';');
        if(values.size()==1)
        {
            msg.data = values[0];
            _screen_command_image_publisher.publish(msg);
        }
        else if(values.size()==2 && values[1]=="video")
        {
            msg.data = values[0];
            _screen_command_video_publisher.publish(msg);
        }
        else
            ROS_WARN("Wrong number of parameters for the screen gesture player");

    } // end loop key_idx
  }

    ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
		std_msgs::String msg;
		msg.data = "reset";
		ROS_DEBUG("Sending to the screen the reset image");
        _screen_command_image_publisher.publish(msg);
    }
    
  ////////////////////////////////////////////////////////////////////////////

private:
  //! the publisher in contact with the screens driver
  ros::Publisher _screen_command_image_publisher;
  ros::Publisher _screen_command_video_publisher;
  std::string _topic_name_display_image;
  std::string _topic_name_display_video;
};

#endif // GESTURE_SCREENS_PLAYER_ROS_H
