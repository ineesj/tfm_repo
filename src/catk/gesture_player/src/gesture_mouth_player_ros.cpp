#include "gesture_mouth_player_ros.h"

/*!
  A simple instantation of a GestureMouthPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_mouth_player_ros");
  GestureMouthPlayerRos player;
  ROS_INFO("Starting a GestureMouthPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
