#include "gesture_leds_player_ros.h"

/*!
  A simple instantation of a GestureMonarchLedsPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_leds_player_ros");
  GestureLedsPlayerRos player;
  ROS_INFO("Starting a GestureLedsPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
