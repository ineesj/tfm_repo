/*!
  \file        gesture_string_publisher.cpp
  \author      Arnaud Ramey <arnaud.a.ramey@gmail.com>
                -- Robotics Lab, University Carlos III of Madrid
  \date        2012/5/22

________________________________________________________________________________

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
________________________________________________________________________________

This node subscribes to the topic where the wanted gestures are sent,
load the corresponding XML files, and send the loaded gesture
on the corresponding topic.

\section Subscriptions
  - \b gesture_player::gesture_filename_topic
        [std_msgs::String]
        The filenames of the wanted gestures.

\section Publications
  - \b gesture_player::gesture_topic
        [gesture_player_utils::KeyframeGesture]
        The loaded gestures.

 */

#include <queue>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <std_msgs/String.h>

#include "gesture_io.h"
#include "gesture_publisher.h"
#include "gesture_player_utils/KeyframeGesturePriority.h"
#include "gesture_player_utils/GestureStatus.h"

GesturePublisher *gesture_pub;
std::list<gesture_player_utils::KeyframeGesture> _gestures_queue;
std::list<std::string> _gestures_policy_queue; /* list of policies for the gestures to be played */
std::list<gesture_player_utils::KeyframeGesture>::iterator _gestures_queue_it;
bool _stop;
std::string _current_policy; //policy of the running gesture.

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void addGestureToTheQueue(std::string queue_policy, gesture_player_utils::KeyframeGesture gesture){

    /**
      Add gesture at the end of the queue
    */
    if(queue_policy == "queue_end"){
        _gestures_queue.push_back(gesture);
        _gestures_policy_queue.push_back(queue_policy);
    }
    /**
      Add gesture at the beginning of the queue
    */
    else if(queue_policy == "queue_beg"){
        _gestures_queue.push_front(gesture);
        _gestures_policy_queue.push_front(queue_policy);
    }
    /**
      Empty the queye and add gesture
    */
    else if(queue_policy == "empty_queue"){
        _gestures_queue.clear();
        _gestures_queue.push_front(gesture);
        _gestures_policy_queue.clear();
        _gestures_policy_queue.push_front(queue_policy);
    }
    /**
      Stop the current gesture and add gesture at the beginning of the queue
    */
    else if(queue_policy == "preemptive_queue_beg"){
        if(gesture_pub->getStatus() != IDLE){
            gesture_pub->abortWait();
            _stop = true;
        }
        _gestures_queue.push_front(gesture);
        _gestures_policy_queue.push_front(queue_policy);
    }
    /**
      Stop the current gesture, empty the queue and add gesture
    */
    else if(queue_policy == "preemptive_empty_queue"){
        if(gesture_pub->getStatus() != IDLE){
            gesture_pub->abortWait();
            _stop = true;
        }
        _gestures_queue.clear();
        _gestures_queue.push_front(gesture);
        _gestures_policy_queue.clear();
        _gestures_policy_queue.push_front(queue_policy);
    }
    /**
      Add gesture at the beginning of the queue if the current gesture wasn't added with any *_preemptive_* policy. Otherwise, the gesture is discarded.
    */
    else if(queue_policy == "reactive_preemptive_queue_beg"){
        if (_current_policy.find("preemptive")!=std::string::npos) {
            ROS_WARN("Reactive gesture descarted.");
            return;
        }
        else if(gesture_pub->getStatus() != IDLE){
            gesture_pub->abortWait();
            _stop = true;
        }
        _gestures_queue.push_front(gesture);
        _gestures_policy_queue.push_front(queue_policy);
    }
}

////////////////////////////////////////////////////////////////////////////////
/**
  In this callback it is necessary to order the message queue depending on msg demands
*/
void gesture_filename_cb(const std_msgs::StringConstPtr & msg) {
    ROS_DEBUG("gesture_filename_cb('%s')", msg->data.c_str());

    std::vector<std::string> fileName_policy;
    StringUtils::StringSplit(msg->data.c_str(), ";", &fileName_policy);
    //ROS_DEBUG("SIZE  = %d",fileName_policy.size());

    if(fileName_policy.size() == 2){
        //read file
        gesture_player_utils::KeyframeGesture gesture;
        gesture.gesture_name = fileName_policy[0];
        bool success = gesture_pub->readFile(fileName_policy[0], gesture);
        if (!success) {
            ROS_WARN("Impossible to load the gesture '%s'", fileName_policy[0].c_str());
            /*TODO -  Enviar un topic de error? */
            return;
        }

        //Add gesture to the queue
        addGestureToTheQueue(fileName_policy[1],gesture);
    }

    else if(fileName_policy.size() == 1){
        //read file
        gesture_player_utils::KeyframeGesture gesture;
        gesture.gesture_name = fileName_policy[0];
        bool success = gesture_pub->readFile(fileName_policy[0], gesture);
        if (!success) {
            ROS_WARN("Impossible to load the gesture '%s'", fileName_policy[0].c_str());
            /*TODO -  Enviar un topic de error? */
            return;
        }

        //Add gesture at the end of the queue
        addGestureToTheQueue("queue_end",gesture);
    }
    else
        ROS_ERROR("Recived malformed message: '%s'",msg->data.c_str());

} // end gesture_filename_cb()

////////////////////////////////////////////////////////////////////////////////
/**
  In this callback is necessary to order the message queue depending on msg demands
*/
void gesture_KeyframeGesture_cb(const gesture_player_utils::KeyframeGesturePriorityConstPtr & msg) {
    //ROS_INFO("keyframeGesture received");

    std::string queue_policy = msg->priority;
    gesture_player_utils::KeyframeGesture gesture = msg->gesture;
    //Add gesture to the queue
    addGestureToTheQueue(queue_policy,gesture);

} // end gesture_filename_cb()

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
    ros::init(argc, argv, "gesture_string_publisher");
    ros::NodeHandle nh_public;
    
    ros::Publisher gesture_end_pub = nh_public.advertise<gesture_player_utils::GestureStatus>("end_gesture",1);

    ros::Subscriber gesture_filename_sub = nh_public.subscribe<std_msgs::String>
            (gesture_player::gesture_filename_topic, 1, gesture_filename_cb);

    ros::Subscriber gesture_KeyframeGesture_sub = nh_public.subscribe<gesture_player_utils::KeyframeGesturePriority>
            (gesture_player::gesture_with_priority_topic, 1, gesture_KeyframeGesture_cb);


    gesture_pub = new GesturePublisher(nh_public);
    _stop = false;

//    ROS_WARN("gesture_string_publisher: getting XML filenames from '%s', "
//             "republishing them to '%s'",
//             nh_public.resolveName(gesture_player::gesture_filename_topic).c_str(),
//             gesture_pub->get_gesture_topic().c_str());

    //ros::Rate rate(5);
    while (ros::ok()) {

        if(_gestures_queue.size() > 0 && _gestures_policy_queue.size() > 0 && !_stop){
            gesture_player_utils::KeyframeGesture gesture = _gestures_queue.front();
            _current_policy = _gestures_policy_queue.front();
            _gestures_queue.pop_front();
            _gestures_policy_queue.pop_front();
            ROS_DEBUG("START GESTURE named %s with policy %s\n", gesture.gesture_name.c_str(),_current_policy.c_str());
            gesture_pub->publishGesture(gesture);
            ROS_DEBUG("GESTURE END ");
            _current_policy.clear(); // the policy of the current gesture is deleted because the gesture is over
            //Tell the interaction_executor_manager that the gesture is finished
            gesture_player_utils::GestureStatus status_msg;
            status_msg.gesture = gesture.gesture_name;
            status_msg.status = "OK";
            gesture_end_pub.publish(status_msg);
        }

        if(_stop){
            //if(gesture_pub->getStatus() != IDLE)
                gesture_pub->publishStop();
            _stop = false;
        }

        usleep(100 /*ms*/ * 1000 /*us*/);

        ros::spinOnce();

        // call just one callback
        // cf http://www.ros.org/wiki/roscpp/Overview/Callbacks%20and%20Spinning
        //ros::getGlobalCallbackQueue()->callOne(ros::WallDuration(0.1));
        //rate.sleep();
    } // end while (ros::ok())

    delete gesture_pub;
    return 0;
} // end main()
