#ifndef GESTURE_HEAD_PLAYER_ROS_H
#define GESTURE_HEAD_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

//Head msg
#include "monarch_msgs/HeadControl.h"
/*!

  */

class GestureHeadPlayerRos : public GesturePlayerRos {
public:
    GestureHeadPlayerRos() {
//        ROS_INFO("GestureHeadPlayerRos ctor");

        _head_command_publisher = _nh_public.advertise<monarch_msgs::HeadControl>("cmd_head", 1);


    } // end ctor

    ////////////////////////////////////////////////////////////////////////////

    /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
    virtual bool gesture_set_custom() {
//        ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
        // nothing to do
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! How to move to initial position.
      It must be blocking.
      */
    virtual void gesture_go_to_initial_position() {
//        ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
    virtual void gesture_play() {
//        ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                        _keytimes.size() << "keytimes");

        for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
            gesture_io::JointValue key_value = _joint_values.at(key_idx);
            double _keytime_it_before = key_idx == 0? 0 : _keytimes[key_idx-1];
            double sleep_time = _keytimes[key_idx] - _keytime_it_before;
//            ROS_INFO("%s: Sleeping %fs, then parameters '%s'...",
//                     _joint_name.c_str(), sleep_time, key_value.c_str());

            //ros::Time::sleepUntil(key_time);
            if(!waitUntil(sleep_time)){
//                ROS_INFO_STREAM(_joint_name << ":gesture_play() stopped");
                return;
            }

            //Generate and send message to the head
            std::vector<std::string> speed_pos = string_split(key_value, ';');
            if (speed_pos.size() != 2) {
                speed_pos.clear();
                //Default values
                speed_pos.push_back("100.0"); //[=] % of max. velocity
                speed_pos.push_back("0.0"); //[=] 0 front, -PI/2 right, PI/2 left
            }
            monarch_msgs::HeadControl msg;
            msg.movement_speed = atof(speed_pos[0].c_str());
            msg.head_pos = atof(speed_pos[1].c_str());

            //ROS_WARN("Sending to the Head the following speed;position: %s", key_value.c_str());
            _head_command_publisher.publish(msg);


        } // end loop key_idx
    }

    ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
		monarch_msgs::HeadControl msg;
        msg.movement_speed = 0.0;
        msg.head_pos = 0.0;
        ROS_DEBUG("Sending to the Head the stop command");
        _head_command_publisher.publish(msg);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

private:
    //! the publisher in contact with the monarch head driver
    ros::Publisher _head_command_publisher;
};

#endif // GESTURE_HEAD_PLAYER_ROS_H
