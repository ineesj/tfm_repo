#ifndef GESTURE_MOUTH_PLAYER_ROS_H
#define GESTURE_MOUTH_PLAYER_ROS_H

#include <string>

// ros
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>

// gesture_player
#include "gesture_player_ros.h"

//Add the library's message topic
#include "monarch_msgs/MouthLedControl.h"
#include "monarch_msgs/MouthPictureLedControl.h"
//#include "monarch_msgs/MouthStringLedControl.h"

/*!
---------------------
picture;png_file_name_without_extension
----------------------
random
----------------------
file;txt_file_name_without_extension
----------------------
  */

class GestureMouthPlayerRos : public GesturePlayerRos {
public:
  GestureMouthPlayerRos() {
//    ROS_INFO("GestureMouthPlayerRos ctor");

    _mouth_led_command_publisher = _nh_public.advertise<monarch_msgs::MouthLedControl>("cmd_mouth", 0);
	_mouth_picture_command_publisher = _nh_public.advertise<std_msgs::String>("mouth_image", 0);


  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    /*! if necessary */
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");
	/*! if necessary */
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , vector containing the keyframe times in seconds,
          - _joint_values  , vector containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
      ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
      gesture_io::JointValue key_value = _joint_values.at(key_idx);
//      ROS_INFO("%s: Sleeping till %f, then parameters '%s'...",
//               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
      ros::Time::sleepUntil(key_time);


      //Generate and send message to the mouth
      std::vector<std::string> values = string_split(key_value, ';');
      if (values[0].compare("pngfile")==0 && values.size() == 2) {
          sendPicture(values[1]);
      }
	  else if (values[0].compare("random")==0) {
          sendRandomSelection();
      }
	  else if (values[0].compare("txtfile")==0 && values.size() == 2) {
		  sendLedArray(values[1]);
      }
	  else
          ROS_ERROR("Malformed key_value '%s', check XML configuration.", key_value.c_str());
	  
 
    } // end loop key_idx
  }
  
   ////////////////////////////////////////////////////////////////////////////

    /*! When the gesture is stopped, reset necessary parameters.
    */
    virtual void gesture_stop() {
    }
	
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

private:
	//! the publisher in contact with the Mouth driver
	ros::Publisher _mouth_led_command_publisher;
    ros::Publisher _mouth_picture_command_publisher;
    
	////////////////////////////////////////////////////////////////////////////
	
	void sendPicture(std::string pictureName){
      std_msgs::String msg;
      msg.data = pictureName + ".png";
//	  ROS_DEBUG("Sending to the Mouth the following picture_name: %s", pictureName.c_str());
      _mouth_picture_command_publisher.publish(msg);
	}
  
	////////////////////////////////////////////////////////////////////////////
  
	void sendRandomSelection(){
      monarch_msgs::MouthLedControl msg;
      for (int i=0; i<8; i++) {
		for (int j=0; j<32; j++){
			int random = rand() % 100;
            if(random<30) //30% prob to put on the led
                msg.data.push_back(true);
            else
                msg.data.push_back(false);
		}
	  } 
	  
//	  ROS_DEBUG("Sending to the Mouth a random selection");
      _mouth_led_command_publisher.publish(msg);
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	void sendLedArray(std::string fileName){
		std::string ros_path = gesture_player::gesture_files_folder();
		std::string path = ros_path + "mouth_files/" + fileName + ".txt";
		
		std::ifstream file;
		file.open(path.c_str());

		monarch_msgs::MouthLedControl msg;

		std::string line;
		if (file){

			while (getline(file, line)){
				//Read line
				for(int i=0;i<32;i++){
					if(line.at(i) == 'O')
						msg.data.push_back(true);
					else if(line.at(i) == '-')
						msg.data.push_back(false);
				}
			}

			// free resources
			file.close();
		}else{
			ROS_ERROR("Error opening file...");
			return;
		}
		
//		ROS_DEBUG("Sending to the Mouth the following Array: %s", fileName.c_str());
        _mouth_led_command_publisher.publish(msg);
		
/*			
        monarch_msgs::MouthLedControl msg;
		std::ifstream file;
		char *str = new char[200];
		std::string ros_path = gesture_player::gesture_files_folder();
		std::string path = ros_path + "mouth_files/" + fileName + ".txt";

		file.open(path.c_str());

		if (file.is_open()) {
			// read and assign the data
			while(file.good()) {
				//Read line
				file.getline(str, 200);
                for(int i=0;i<200;i++){
                    if(str[i] == 'O')
                      msg.data.push_back(true);
                    else if(str[i] == '-')
                       msg.data.push_back(false);
                }
			}
		}
		else {
			ROS_ERROR("Error opening file...");
			return;
		}	

       // free resources
      file.close();
     
	  ROS_DEBUG("Sending to the Mouth the following Array: %s", fileName.c_str());	    
      _mouth_led_command_publisher.publish(msg);
	
*/
	}
	
};

#endif // GESTURE_MOUTH_PLAYER_ROS_H
