#ifndef GESTURE_VOICE_PLAYER_ROS_H
#define GESTURE_VOICE_PLAYER_ROS_H

// etts_msgs
#include <etts_msgs/Utterance.h>
// gesture_player
#include "gesture_player_ros.h"

/*!
  A gestue player compatible with the voice architecture of AD,
  called ETTS (emotional text to speech).

  For ETTS, the keyframes correspond to sentences supposed to be said
  at given moments.
  For instance: at t=1 sec, say "Hello".

  The sentences are passed to the ETTS skill by using ros messages
  <etts_pub>.publish(<etts_msg>),
  which enables the use of natural language tags within
  the keyframe sentences.
  For instance, "\NLG=OK" will generate random sentences
  that express agreement, such as "OK", "Yes", or "Agreed".
  This gives the possibility of more vivid and less repetitive gestures.
  */
class GestureEttsPlayerRos : public GesturePlayerRos {
public:
  GestureEttsPlayerRos() {
//    ROS_INFO("GestureEttsPlayerRos ctor");
    _etts_publisher = _nh_public.advertise<etts_msgs::Utterance>("etts", 1);

  } // end ctor

  ////////////////////////////////////////////////////////////////////////////

  /*!
      A custom function called even before extracting the keys of the gesture.
      You can access the current gesture with
      _current_gesture
      The gesture data for this joint has been stored in
        _keytimes   and   _joint_values
      */
  virtual bool gesture_set_custom() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_set_custom()");
    // nothing to do
    return true;
  }

  ////////////////////////////////////////////////////////////////////////////

  /*! How to move to initial position.
      It must be blocking.
      */
  virtual void gesture_go_to_initial_position() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_go_to_initial_position()");

  }

  ////////////////////////////////////////////////////////////////////////////

  /*! When the gesture is stopped, reset necessary parameters.
  */
  virtual void gesture_stop() {
  }
  
  ////////////////////////////////////////////////////////////////////////////

  /*! Where the proper gesture playing is done.
        It must be blocking till the gesture is over.
        It should be made use of:
          - _keytimes , containing the keyframe times in seconds,
          - _joint_values  , containing the string values of the wanted joint
      */
  virtual void gesture_play() {
//    ROS_INFO_STREAM(_joint_name << ":gesture_play(), " <<
//                    _keytimes.size() << "keytimes");
    ros::Time start_time = ros::Time::now();

    for (unsigned int key_idx = 0; key_idx < _keytimes.size(); ++key_idx) {
      ros::Time key_time = start_time + ros::Duration(_keytimes[key_idx]);
      gesture_io::JointValue key_value = _joint_values.at(key_idx);
//      ROS_INFO("%s: Sleeping till %f, then saying '%s'...",
//               _joint_name.c_str(), key_time.toSec(), key_value.c_str());
      ros::Time::sleepUntil(key_time);

      //Generate and send message to etts
      std::vector<std::string> etts_msg_fields;
      StringUtils::StringSplit(key_value, ";", &etts_msg_fields);

      etts_msgs::Utterance etts_msg;
      etts_msg.priority = etts_msgs::Utterance::SHUTUP_IMMEDIATLY_AND_SAY_SENTENCE;

      if (etts_msg_fields.size() !=0){
        etts_msg.text = etts_msg_fields[0];
      }

      for (unsigned int i =0; i < etts_msg_fields.size(); i++){
//        ROS_INFO("etts_msgs_fields[%d]: %s",i,etts_msg_fields[i].c_str());
        if (etts_msg_fields[i] == "es") etts_msg.language = "es";
        else if (etts_msg_fields[i] == "pt") etts_msg.language = "pt";
        else if (etts_msg_fields[i] == "en") etts_msg.language = "en";
      }

      _etts_publisher.publish(etts_msg);
    } // end loop key_idx
  }

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

private:

  //! the publisher for communicating with the ETTS skill
  ros::Publisher _etts_publisher;
};

#endif // GESTURE_VOICE_PLAYER_ROS_H
