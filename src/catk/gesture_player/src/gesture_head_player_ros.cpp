#include "gesture_head_player_ros.h"

/*!
  A simple instantation of a GestureHeadPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_head_player_ros");
  GestureHeadPlayerRos player;
  ROS_INFO("Starting a GestureHeadPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
