#ifndef GESTURE_PUBLISHER_H
#define GESTURE_PUBLISHER_H

/*!
  This is a publisher for gesture messages.
  When you publish a gesture, here is what it does in order:

  1) It first publishes the message
  (gesture_player::KeyframeGesture) on the dedicated channel.
  When the gesture players receive it, they will go to the initial
  position of the gesture.
  The way they do it depends on the player:
  a voice player will wait for the voice to be available,
  while a servo will physically move to the initial angle.

  2) It determines the number of acks (acknowledgement) needed to go on.
  It is determined by the number of subscribers to gestures
  at current time.
  As such, if a player dies, this number is decreased by one.

  3) it waits for the acks for all joints.

  4) when all acks are received, it emits a "play" order for all joints,
  giving them the order of starting the execution of gestures.
  */

#include <list>

// ROS
#include <ros/ros.h>
#include <std_msgs/Bool.h>

// gesture_player
#include "gesture_definitions.h"
#include "gesture_io.h"
#include "gesture_player_utils/KeyframeGesture.h"

enum Status {
    IDLE = 0,
    WAITING_ACKS = 1,
    WORKING = 2
};

class GesturePublisher {
public:

    GesturePublisher(ros::NodeHandle & nh) {
        _gesture_publisher = nh.advertise<gesture_player_utils::KeyframeGesture>
                (gesture_player::gesture_topic, 1);
        _ack_subscriber = nh.subscribe
                (gesture_player::ack_topic, 100,
                 &GesturePublisher::ack_callback, this);
        _play_order_publisher = nh.advertise<gesture_player::Ack>
                (gesture_player::play_order_topic, 1);
        _stop_play_order_publisher = nh.advertise<std_msgs::Bool>
                (gesture_player::stop_play_order_topic, 1);
        _status = IDLE;
        _abort_wait = false;
    } // end GesturePublisher

    ///////////////////////////////////////////////////////////////////////////

    void ack_callback(const gesture_player::Ack & ack_msg) {
        if (_status == IDLE) // dismiss the ack
            return;

        // if the ack corresponds to the emitted message,
        // increment the number of acks
        if (ack_msg.stamp != _current_gesture_stamp) {
//            ROS_WARN("GesturePublisher: we received an ack for a gesture with "
//                     "a stamp (%f) different from our stamp (%f).",
//                     ack_msg.stamp.toSec(), _current_gesture_stamp.toSec());
            return;
        }
        _received_acks_names.insert(ack_msg.frame_id);
//        ROS_INFO_STREAM
//                ("Received " << nb_acks() << " acks ("
//                 << StringUtils::iterable_to_string(_received_acks_names)
//                 << ") for gesture with stamp " << _current_gesture_stamp << ", "
//                 << _nb_listeners - nb_acks() << " missing.");

        // if enough acks received, play the gesture
        if (nb_acks() == _nb_listeners) {
            _ack_msg = ack_msg;
            _status = WORKING;
        } // end if enough acks

    } // end ack_callback()


    ///////////////////////////////////////////////////////////////////////////
    void publishGesture(gesture_player_utils::KeyframeGesture gesture) {
        //change status
       _status = WORKING;

        bool success;

        //publish
        ROS_DEBUG("Publish gesture");
        publish_gesture(gesture);

        //wait for ack   (timeout = 3s)
//        ROS_INFO("Waiting the ACKs");
        success = waitForAck(3);
        if (!success) {
            ROS_WARN("Still alive timeout exceeded");
            return;
        }

        //Clear variable
        _received_acks_names.clear();

        //Allow gesture start
        allowPlayeGesture();

        // get gesture duraction
        gesture_io::Time min_time, max_time;
        gesture_io::get_gesture_min_max_times(gesture, min_time, max_time);
        // then wait the needed time
        gesture_io::Time timeout = max_time + 3; // add 3 second for safety
//        ROS_INFO("Gesture with stamp %f: is allowed  (timeoust: %f)",
//                 gesture.header.stamp.toSec(),timeout);
        //wait for ack   (timeout = gesturetime+10s)
        success = waitForAck(timeout);
        if (!success) {
            ROS_WARN("Gesture play timeout exceeded");
            return;
        }

        ROS_DEBUG("Gesture with stamp %f: gesture is over.",
                 gesture.header.stamp.toSec());
        _status = IDLE;
    }

    ///////////////////////////////////////////////////////////////////////////

    void publishStop() {
        //change status
       _status = WORKING;

        //publish stop
        ROS_INFO("Publish stop message");
        publish_stop(true);

        //wait for ack   (timeout = 3s)
//        ROS_INFO("Waiting the ACKs");
        bool success = waitForAck(3);
        if (!success) {
            ROS_WARN("Still alive timeout exceeded");
            return;
        }

        ROS_INFO("Gesture play already stopped");
        _status = IDLE;
    }

    ///////////////////////////////////////////////////////////////////////////
    void publish_gesture(gesture_player_utils::KeyframeGesture & gesture) {
        //complete gesture information
        gesture.header.stamp = ros::Time::now();

        //Reset variables
        _current_gesture_stamp = gesture.header.stamp;
        _received_acks_names.clear();
        //get number of required nodes
        _nb_listeners = getNumberOfNodes(gesture);
//        ROS_DEBUG("Number of involved nodes was: %i",_nb_listeners);

        //publish gesture
        _gesture_publisher.publish(gesture);

        //change status
        _status = WAITING_ACKS;

        // then wait for the listeners to confirm the reception of the gesture
//        ROS_INFO("Gesture with stamp %f: now waiting for %i acks...",
//                 gesture.header.stamp.toSec(), _nb_listeners);
    }

    ///////////////////////////////////////////////////////////////////////////
    void publish_stop(bool msg) {
        std_msgs::Bool message;
        message.data = msg;
        _stop_play_order_publisher.publish(message);
    }

    ///////////////////////////////////////////////////////////////////////////
    inline std::string get_gesture_topic() const {
        return _gesture_publisher.getTopic();
    }

    ///////////////////////////////////////////////////////////////////////////
    Status getStatus() {
        return _status;
    }

    void abortWait(){
        _abort_wait = true;
    }


    ///////////////////////////////////////////////////////////////////////////
    bool readFile(const std::string fileName,
                  gesture_player_utils::KeyframeGesture & gesture) {

        //create path
        std::ostringstream xml_full_path;
        xml_full_path << gesture_player::gesture_files_folder()
                      << fileName << ".xml";

        //read
        bool success = gesture_io::load_from_xml_file
                (gesture, xml_full_path.str());

        return success;
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

private:
    //! the number of received acks for the current gesture
    inline unsigned int nb_acks() {
        return _received_acks_names.size();
    }

    Status _status;
    //! the publisher for gesture messages
    ros::Publisher _gesture_publisher;
    //! the subscriber to acks, emitted by gesture joint players
    ros::Subscriber _ack_subscriber;
    //! the publisher for the play order, once all acks have been received
    ros::Publisher _play_order_publisher;
    //! the publisher for the stop play order
    ros::Publisher _stop_play_order_publisher;
    //! the number of acks we expect
    unsigned int _nb_listeners;

    std::set<std::string> _received_acks_names;
    //! the stamp of the published gesture
    ros::Time _current_gesture_stamp;


    gesture_player::Ack _ack_msg;
    bool _abort_wait;


    ///////////////////////////////////////////////////////////////////////////
    //! return false if aborted or timeout exceeded
    bool waitForAck(double timeout){
        ros::Time end_time = ros::Time::now() + ros::Duration(timeout);

        while(_status == WAITING_ACKS && ros::Time::now() < end_time
              && !_abort_wait){
             usleep(100 /*ms*/ * 1000 /*us*/);
            //ros::Time::sleepUntil(end_time);
            ros::spinOnce();
        }

        if(_abort_wait){
            _abort_wait = false;
            return false;
        }
        else if(_status != WAITING_ACKS)
            return true;
        else
            return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    int getNumberOfNodes(gesture_player_utils::KeyframeGesture & gesture){
        std::list<std::string> differentNames;
        if(gesture.joint_names.size() == 0){
            return 0;
        }

        differentNames.push_back(gesture.joint_names[0]);
        for(unsigned int i=0; i<gesture.joint_names.size();i++){
            std::list<std::string>::iterator it =
                    std::find (differentNames.begin(), differentNames.end(),
                               gesture.joint_names[i]);
            if(differentNames.end() == it) //if not found
                differentNames.push_back(gesture.joint_names[i]); //add it
        }

        return differentNames.size();
    }

    ///////////////////////////////////////////////////////////////////////////
    void allowPlayeGesture() {
        _play_order_publisher.publish(_ack_msg);
         //change status
        _status = WAITING_ACKS;
    }

}; // end class GesturePublisher

#endif // GESTURE_PUBLISHER_H
