#include "gesture_vumeter_player_ros.h"

/*!
  A simple instantation of a GestureVumeterPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_vumeter_player_ros");
  GestureVumeterPlayerRos player;
  ROS_INFO("Starting a GestureVumeterPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
