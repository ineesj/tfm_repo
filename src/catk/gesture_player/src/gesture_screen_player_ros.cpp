#include "gesture_screen_player_ros.h"

/*!
  A simple instantation of a GestureScreenPlayerRos.
  */
int main(int argc, char** argv) {
  ros::init(argc, argv, "gesture_screen_player_ros");
  GestureScreenPlayerRos player;
  ROS_INFO("Starting a GestureScreenPlayerRos with name '%s'",
           player.get_joint_name().c_str());
  ros::spin();
  return 0;
}
