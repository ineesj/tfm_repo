#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <monarch_msgs/ArmsControl.h>

//Global variables
ros::Publisher behaviour_active_pub;

geometry_msgs::Twist last_velocity;


///////////////////////////////////////////////////////////////////////////////
/// AUXILIAR FUNCTIONS
///////////////////////////////////////////////////////////////////////////////
/** These functions move the arms in different ways according to the direction
*   of the robot. The amplitude of the arms movement is related to the robot's
*   velocity.
*/

void robotMovingForward(double factor){

}

///////////////////////////////////////////////////////////////////////////////

void robotMovingBackward(double factor){

}

///////////////////////////////////////////////////////////////////////////////

void robotRotatingRight(double factor){

}

///////////////////////////////////////////////////////////////////////////////

void robotRotatingLeft(double factor){

}

////////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
////////////////////////////////////////////////////////////////////////////////
void velocityCallback(const geometry_msgs::Twist::ConstPtr& msg){

}

///////////////////////////////////////////////////////////////////////////////
/// MAIN
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv){
    ros::init(argc, argv, "liveliness_arms_behaviour");
    ros::NodeHandle nh;

    //subscribers
    ros::Subscriber vel_sub = nh.subscribe("cmd_vel", 0, velocityCallback);


    //publishers
    behaviour_active_pub = nh.advertise<std_msgs::Bool>("liveliness_arms_behaviour", 0);


    ROS_INFO("liveliness_arms_behaviour running");
    ros::spin();

    return 0;
}

