#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <monarch_msgs/GestureExpression.h>
#include <monarch_msgs/BatteriesVoltage.h>
#include <monarch_msgs/CapacitiveSensorsReadings.h>
#include "gesture_player_utils/GestureStatus.h"

#include<iostream>

#define STOPPED         0
#define ACTIVE_VIVID    1
#define ACTIVE_QUIET    2
#define ACTIVE_SLEEPING 3

#define RANDOM_FACTOR   2

/** This node selects gestures randomly from a given list and sends them to the
 * interaction_executor_manager so the robot is constantly doing something
 * (i.e. moving, turning leds on and off, etc.) in order to convey liveliness.
 *
 * This behaviour can be activated or stopped with the topic "liveliness_status_update"
 * with a msg of type Int16: 0 to stop, 1 to activate.
 *
 * If the behaviour is "ACTIVE_XX", then, each time a msg is received on the topic
 * "end_gesture" (published by the gesture_player), the show_liveliness_node selects
 * randomly a gesture to perform. There are two types of gestures used to convey
 * liveliness: more "vivid" gestures, performed less frequently, and more "quiet"
 * gestures, performed by default, if no other gesture is selected (randomly).
 *
 * If state is "ACTIVE_VIVID", vivid gestures are perfomed randomly, but if
 * state is "ACTIVE_QUIET", only quiet gestures are used.
 *
*/


int status_;
bool robot_charging_;
std::vector<std::string> liveliness_touch_gestures_;
std::vector<std::string> sleep_touch_gestures_;
std::string initial_gesture_;
ros::Publisher reactive_pub_;
ros::Subscriber status_sub_, filtered_touch_sub_;


////////////////////////////////////////////////////////////////////////////////

void performTouchGesture(){
    if(liveliness_touch_gestures_.size() == 0 || sleep_touch_gestures_.size() == 0){
        ROS_WARN("TOUCH GESTURES QUEUE EMPTY. ABORT GESTURE.");
        return;
    }
    
    if(status_ == ACTIVE_SLEEPING){
        unsigned int range = floor(sleep_touch_gestures_.size());
        unsigned int rnd_idx = rand() % range;
        std_msgs::String sleep_gesture_msg;
        sleep_gesture_msg.data = sleep_touch_gestures_[rnd_idx];
        ROS_INFO("SLEEP TOUCH GESTURE %s", sleep_gesture_msg.data.c_str());
        std::cout << "Send reactive touch [sleeping] " << sleep_gesture_msg << std::endl;
        reactive_pub_.publish(sleep_gesture_msg);
    }
    else{	// status_ = ACTIVE_VIVID || ACTIVE_QUIET
        unsigned int range = floor(liveliness_touch_gestures_.size());
        unsigned int rnd_idx = rand() % range;
        std_msgs::String gesture_msg;
        gesture_msg.data = liveliness_touch_gestures_[rnd_idx];
        ROS_INFO("LIVELINESS TOUCH GESTURE %s", gesture_msg.data.c_str());
        std::cout << "Send reactive touch " << gesture_msg << std::endl;
        reactive_pub_.publish(gesture_msg);
    }
}


////////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
////////////////////////////////////////////////////////////////////////////////

void statusCallback(const std_msgs::Int16::ConstPtr& msg){
	// Display information about the state and update it
    if (msg->data == ACTIVE_VIVID || msg->data == ACTIVE_QUIET || msg->data == ACTIVE_SLEEPING){
        ROS_INFO("show_liveliness is ACTIVE [%d]\n", msg->data);
        //publishSpecificGesture(initial_gesture_);
    }
    else if (msg->data == STOPPED){
        ROS_INFO("show_liveliness is STOPPED\n");
    }

    status_ = msg->data;

}



////////////////////////////////////////////////////////////////////////////////

void filteredTouchCallback(const monarch_msgs::CapacitiveSensorsReadings::ConstPtr& msg){

    if(status_ == STOPPED)
    {
        ROS_INFO("[TOUCH_RESPONSE] Touch message not displayed. Status = STOPPED");
        return;
    }

    // Touch received
    if(msg->head == true || msg->left_shoulder == true || msg->right_shoulder == true || msg->left_arm==true || msg->right_arm == true){
        ROS_INFO("Perform touch gesture");
        performTouchGesture();
    }
}


////////////////////////////////////////////////////////////////////////////////
/// MAIN
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {

  ros::init(argc, argv, "touch_response_node");
  ros::NodeHandle nh_public;
  ros::NodeHandle nh_private("~");

  /** Get initial gesture - the gesture that sets the "personality" of the robot
  * (such as default mouth, eye colour, image on the screen...)
  */
  //nh_private.getParam("initial_gesture", initial_gesture_);

  /** Get the "random" gestures, which the robot can perform to convey liveliness.
  * The robot selects randomly if it performs a gesture from this list and which
  * one. These are more variated and vivid gestures
  */
  //nh_private.getParam("liveliness_random_gestures", liveliness_random_gestures_);
  // for(unsigned i=0; i < liveliness_random_gestures.size(); i++) {
      // ROS_INFO("liveliness_random_gestures[%d]: %s", i, liveliness_random_gestures[i].c_str());
  // }

  /** Get the "default" gestures: if no random gesture is selected, the robot should't be
   * completely inert, so it selects one of these "quiet, still" gestures, not so vivid
   * as the others.
   */
  //nh_private.getParam("liveliness_default_gestures", liveliness_default_gestures_);
  // for(unsigned i=0; i < liveliness_default_gestures.size(); i++) {
      // ROS_INFO("liveliness_default_gestures[%d]: %s", i, liveliness_default_gestures[i].c_str());
  // }

  /** Get the "touch"gestures: when the robot is touched the robot will react
   * to convey liveliness.
   * Gestures will be randomly selected.
   */
  nh_private.getParam("liveliness_touch_gestures",liveliness_touch_gestures_);
  nh_private.getParam("sleep_touch_gestures",sleep_touch_gestures_);


  //Subscribers and publishers

  status_sub_           = nh_public.subscribe("liveliness_status_update", 1, statusCallback);
  filtered_touch_sub_   = nh_public.subscribe("body_touched",1,filteredTouchCallback);
  
  reactive_pub_         = nh_public.advertise<std_msgs::String>("reactive_gestures", 1);

  status_ = ACTIVE_VIVID;
  robot_charging_ = false;

  srand (time(NULL));

  ros::spin();

  return 0;
}

