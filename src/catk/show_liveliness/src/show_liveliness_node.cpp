#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <monarch_msgs/GestureExpression.h>
#include <monarch_msgs/BatteriesVoltage.h>
#include <monarch_msgs/CapacitiveSensorsReadings.h>
#include "gesture_player_utils/GestureStatus.h"
#include<iostream>

#define STOPPED         0
#define ACTIVE_VIVID    1
#define ACTIVE_QUIET    2
#define ACTIVE_SLEEPING 3

#define RANDOM_FACTOR   2

/** This node selects gestures randomly from a given list and sends them to the
 * interaction_executor_manager so the robot is constantly doing something
 * (i.e. moving, turning leds on and off, etc.) in order to convey liveliness.
 *
 * This behaviour can be activated or stopped with the topic "liveliness_status_update"
 * with a msg of type Int16: 0 to stop, 1 to activate.
 *
 * If the behaviour is "ACTIVE_XX", then, each time a msg is received on the topic
 * "end_gesture" (published by the gesture_player), the show_liveliness_node selects
 * randomly a gesture to perform. There are two types of gestures used to convey
 * liveliness: more "vivid" gestures, performed less frequently, and more "quiet"
 * gestures, performed by default, if no other gesture is selected (randomly).
 *
 * If state is "ACTIVE_VIVID", vivid gestures are perfomed randomly, but if
 * state is "ACTIVE_QUIET", only quiet gestures are used.
 *
*/


int status_;
bool robot_charging_;
std::vector<std::string> liveliness_default_gestures_, liveliness_random_gestures_;
std::string initial_gesture_;
ros::Publisher  gesture_start_pub_, reactive_pub_, liveliness_status_pub_;
ros::Subscriber status_sub_, gesture_end_sub_, batteries_sub_;


////////////////////////////////////////////////////////////////////////////////
/// AUXILIAR FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

bool performRandomGesture(){

    srand (time(NULL));
    //We want the range to be greater than the possible gestures
    //to consider the possibility of doing nothing
    unsigned int range = floor(liveliness_random_gestures_.size()*RANDOM_FACTOR);
    unsigned int rnd_idx = rand() % range;

    if (rnd_idx < liveliness_random_gestures_.size()){
        std_msgs::String gesture_msg;
        gesture_msg.data = liveliness_random_gestures_[rnd_idx];
        //ROS_INFO("LIVELINESS GESTURE %s", gesture_msg.data.c_str());
        gesture_start_pub_.publish(gesture_msg);
        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////////////////////////

void performDefaultGesture(){

    srand (time(NULL));
    unsigned int range = floor(liveliness_default_gestures_.size());
    unsigned int rnd_idx = rand() % range;

    std_msgs::String gesture_msg;
    gesture_msg.data = liveliness_default_gestures_[rnd_idx];
    //ROS_INFO("LIVELINESS GESTURE %s", gesture_msg.data.c_str());
    gesture_start_pub_.publish(gesture_msg);


}

////////////////////////////////////////////////////////////////////////////////

void publishSpecificGesture(std::string gesture_name){
    std_msgs::String msg;
    msg.data = gesture_name;
    gesture_start_pub_.publish(msg);
}


////////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
////////////////////////////////////////////////////////////////////////////////

void statusCallback(const std_msgs::Int16::ConstPtr& msg){
	if(!msg->data == status_){
		if (msg->data == ACTIVE_VIVID || msg->data == ACTIVE_QUIET){
			ROS_INFO("[show_liveliness_node:statusCallbak] show_liveliness is ACTIVE [%d]\n", msg->data);
			publishSpecificGesture(initial_gesture_);
		}else if (msg->data == ACTIVE_SLEEPING){
			publishSpecificGesture("mbot_sleeping");
			ROS_INFO("[show_liveliness_node:statusCallbak] show_liveliness is SLEEPING\n");			
		}
		else if (msg->data == STOPPED){
			ROS_INFO("[show_liveliness_node:statusCallbak] show_liveliness is STOPPED\n");
		}

		status_ = msg->data;
	}

}

////////////////////////////////////////////////////////////////////////////////

void batteriesCallback(const monarch_msgs::BatteriesVoltage::ConstPtr& msg){

    if (status_ == STOPPED)     return;

    if (msg->charger != 0.0 && !robot_charging_){
        ROS_INFO("show_liveliness is SLEEPING\n");
        robot_charging_ = true;
        status_ = ACTIVE_SLEEPING;
        //Activating ACTIVE_SLEEPING
        std_msgs::Int16 msg;
        msg.data = status_;
        liveliness_status_pub_.publish(msg);
        publishSpecificGesture("mbot_sleeping");

    }

    if (msg->charger == 0.0 && robot_charging_){
        ROS_INFO("show_liveliness is ACTIVE_VIVID\n");
        robot_charging_ = false;
        status_ = ACTIVE_VIVID;
        //Activating  ACTIVE_VIVID
        std_msgs::Int16 msg;
        msg.data = status_;
        liveliness_status_pub_.publish(msg);
        publishSpecificGesture(initial_gesture_);
    }

}


////////////////////////////////////////////////////////////////////////////////

void endGestureCallback(const gesture_player_utils::GestureStatus::ConstPtr& msg){

    switch(status_){

    case ACTIVE_VIVID:
      //sleep(0.5);
      if (!performRandomGesture())
         performDefaultGesture();
      break;

    case ACTIVE_QUIET:
      //sleep(0.5);
        performDefaultGesture();
      break;

    case ACTIVE_SLEEPING:
      break;

    case STOPPED:
      break;
    }

}


////////////////////////////////////////////////////////////////////////////////
/// MAIN
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {

  ros::init(argc, argv, "show_liveliness_node");
  ros::NodeHandle nh_public;
  ros::NodeHandle nh_private("~");

  /** Get initial gesture - the gesture that sets the "personality" of the robot
  * (such as default mouth, eye colour, image on the screen...)
  */
  nh_private.getParam("initial_gesture", initial_gesture_);

  /** Get the "random" gestures, which the robot can perform to convey liveliness.
  * The robot selects randomly if it performs a gesture from this list and which
  * one. These are more variated and vivid gestures
  */
  nh_private.getParam("liveliness_random_gestures", liveliness_random_gestures_);


  /** Get the "default" gestures: if no random gesture is selected, the robot should't be
   * completely inert, so it selects one of these "quiet, still" gestures, not so vivid
   * as the others.
   */
  nh_private.getParam("liveliness_default_gestures", liveliness_default_gestures_);




  //Subscribers and publishers
  gesture_start_pub_    = nh_public.advertise<std_msgs::String>("liveliness_gestures", 1);
  liveliness_status_pub_ = nh_public.advertise<std_msgs::Int16>("liveliness_status_update", 0);

  status_sub_           = nh_public.subscribe("liveliness_status_update", 1, statusCallback);
  gesture_end_sub_      = nh_public.subscribe("end_gesture", 1, endGestureCallback);
  batteries_sub_        = nh_public.subscribe("batteries_voltage", 1, batteriesCallback);

  status_ = ACTIVE_VIVID;
  robot_charging_ = false;

  publishSpecificGesture(initial_gesture_);

  ros::spin();

  return 0;
}

