#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <monarch_msgs/GestureExpression.h>
#include <monarch_msgs/BatteriesVoltage.h>
#include <monarch_msgs/BumpersReadings.h>
#include "gesture_player_utils/GestureStatus.h"

#include<iostream>
 
#define STOPPED         0
#define ACTIVE_VIVID    1
#define ACTIVE_QUIET    2
#define ACTIVE_SLEEPING 3

#define RANDOM_FACTOR   2

/** This node selects gestures randomly from a given list and sends them to the
 * interaction_executor_manager so the robot is constantly doing something
 * (i.e. moving, turning leds on and off, etc.) in order to convey liveliness.
 *
 * This behaviour can be activated or stopped with the topic "liveliness_status_update"
 * with a msg of type Int16: 0 to stop, 1 to activate.
 *
 * If the behaviour is "ACTIVE_XX", then, each time a msg is received on the topic
 * "end_gesture" (published by the gesture_player), the show_liveliness_node selects
 * randomly a gesture to perform. There are two types of gestures used to convey
 * liveliness: more "vivid" gestures, performed less frequently, and more "quiet"
 * gestures, performed by default, if no other gesture is selected (randomly).
 *
 * If state is "ACTIVE_VIVID", vivid gestures are perfomed randomly, but if
 * state is "ACTIVE_QUIET", only quiet gestures are used.
 *
*/


int status_;
bool robot_charging_, last_bump_;
std::vector<std::string> liveliness_bump_gestures_;
//std::vector<std::string> sleep_bump_gestures_; // (1) This will be necessary just if different
                                               // bump reactions are considered when charging
std::string initial_gesture_;
ros::Publisher bump_pub_, bump_end_pub_;
ros::Subscriber status_sub_, bumper_sub_, bumper_end_sub_;


////////////////////////////////////////////////////////////////////////////////

void performBumpGesture(){
    srand (time(NULL));
    std::cout << liveliness_bump_gestures_.size() << " tamano" << std::endl;
    if(liveliness_bump_gestures_.size() == 0){ // || sleep_bump_gestures_.size() == 0){ // Check this last part only if different reaction will be triggered when sleeping
        ROS_WARN("[bump_reaction_node: performBumpGesture] BUMP GESTURES QUEUE EMPTY. ABORT GESTURE.");
        return;
    }
    
    //if(status_ == ACTIVE_SLEEPING)
    //{ // In case of collision/bumping the gestures will be the same
      // independently from the awaken/sleeping state of the robot

    //    unsigned int range = floor(sleep_bump_gestures_.size()); // See (1)
    //    //unsigned int range = floor(liveliness_bump_gestures_.size());

    //    unsigned int rnd_idx = rand() % range;
    //    std_msgs::String sleep_gesture_msg;

    //    sleep_gesture_msg.data = sleep_bump_gestures_[rnd_idx];  // See (1)
        //sleep_gesture_msg.data = liveliness_bump_gestures_[rnd_idx];

    //    ROS_INFO("[bump_reaction_node: performBumpGesture]SLEEP BUMP GESTURE %s", sleep_gesture_msg.data.c_str());
    //    std::cout << "Send bump reaction " << sleep_gesture_msg << std::endl;
    //    bump_pub_.publish(sleep_gesture_msg);
    //}
    else{
        unsigned int range = floor(liveliness_bump_gestures_.size());
        unsigned int rnd_idx = rand() % range;
        std_msgs::String gesture_msg;
        gesture_msg.data = liveliness_bump_gestures_[rnd_idx];
        ROS_INFO("[bump_reaction_node: performBumpGesture] LIVELINESS BUMP GESTURE %s", gesture_msg.data.c_str());
        std::cout << "Send reactive bump response " << gesture_msg << std::endl;
        bump_pub_.publish(gesture_msg);
    }
}


////////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
////////////////////////////////////////////////////////////////////////////////

void statusCallback(const std_msgs::Int16::ConstPtr& msg){

    if (msg->data == ACTIVE_VIVID || msg->data == ACTIVE_QUIET || msg->data == ACTIVE_SLEEPING){
        ROS_INFO("[bump_reaction_node: statusCallback]show_liveliness is ACTIVE [%d]\n", msg->data);
        //publishSpecificGesture(initial_gesture_);
    }
    else if (msg->data == STOPPED){
        ROS_INFO("[bump_reaction_node: statusCallback]show_liveliness is STOPPED\n");
    }

    status_ = msg->data;

}

void filteredBumpReleaseCallback(const monarch_msgs::BumpersReadings::ConstPtr& msg){
    if(status_ == STOPPED){
        ROS_INFO("[bump_reaction_node: filteredBumpCallback] Bump message not displayed. Status = STOPPED");
        return;
    }	
     // Transition from collision to non-collision state
	std::cout << "Bump finished" << std::endl;
	last_bump_ = false;

	//TO DO Tell show_liveliness to play the initial gesture
	// nh_public.subscribe("end_gesture", 1, endGestureCallback);
	gesture_player_utils::GestureStatus status_msg;
	status_msg.gesture = "";
	status_msg.status = "OK";
	bump_end_pub_.publish(status_msg);
}

////////////////////////////////////////////////////////////////////////////////
// Filtered bump event callbck
////////////////////////////////////////////////////////////////////////////////
void filteredBumpCallback(const monarch_msgs::BumpersReadings::ConstPtr& msg){
    if(status_ == STOPPED){
        ROS_INFO("[bump_reaction_node: filteredBumpCallback] Bump message not displayed. Status = STOPPED");
        return;
    }
	ROS_INFO("[bump_reaction_node: filteredBumpCallback] Perform touch gesture");
	std::cout << "Bump received" << std::endl;
	last_bump_ = true;
	performBumpGesture();
}

////////////////////////////////////////////////////////////////////////////////
/// MAIN
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {

  ros::init(argc, argv, "bump_response_node");
  ros::NodeHandle nh_public;
  ros::NodeHandle nh_private("~");

  /** Get the "bump" gestures: when the robot collides the robot will react
   * to convey liveliness.
   * Gestures will be randomly selected.
   */
  nh_private.getParam("liveliness_bump_gestures",liveliness_bump_gestures_);
  //nh_private.getParam("liveliness_bump_gestures",sleep_bump_gestures_);

  status_sub_   = nh_public.subscribe("liveliness_status_update", 1, statusCallback);
  bumper_sub_   = nh_public.subscribe("bumper_triggered",1,filteredBumpCallback);
  bumper_end_sub_ = nh_public.subscribe("bumpers_released",1,filteredBumpReleaseCallback);
  bump_pub_     = nh_public.advertise<std_msgs::String>("reactive_gestures", 1);
  bump_end_pub_ = nh_public.advertise<gesture_player_utils::GestureStatus>("end_gesture",1);

  status_       = ACTIVE_VIVID;
  robot_charging_ = false;
  last_bump_    = false;

  ros::spin();

  return 0;
}
