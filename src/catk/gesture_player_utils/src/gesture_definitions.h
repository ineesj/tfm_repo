#ifndef GESTURE_DEFINITIONS_H
#define GESTURE_DEFINITIONS_H

#include <string>
#include <std_msgs/Header.h>
#include <ros/package.h>

/*! This file gathers useful definitions
  for the gesture_player package.
  For instance, the names of possible gestures
  and default used topics are here.
  */

namespace gesture_player {

/*! the default gesture topic.
    There are exchanged messages of type
    gesture_player/KeyframeGesture */
static const std::string gesture_topic = "keyframe_gesture";

/*! the default gesture topic.
    There are exchanged messages of type
    std_msgs::String */
static const std::string gesture_filename_topic = "keyframe_gesture_filename";

//! where the XML gesture files are stored
inline static const std::string gesture_files_folder() {
  return ros::package::getPath("gesture_player_utils") + std::string("/data/");
}

/*! the topic used to confirm the joint is ready to play the gesture.
  The message itself is a std_msgs::Time ,
  it is used as an id for the wanted gesture
  and it is the stamp of the message */
static const std::string ack_topic = "keyframe_ack";

/*! the topic used to start playing the gesture
  once acks have been received.
  The message is a std_msgs::Time, the same as the ack. */
static const std::string play_order_topic = "keyframe_play_order";

/*! the topic used to stop the gesture playing.
  The message is a std_msgs::Bool. */
static const std::string stop_play_order_topic = "keyframe_stop";

/*! the topic used to ask the gesture_player to do some gesture
  The message is a gesture_player_utils::KeyframeGesturePriority. */
static const std::string gesture_with_priority_topic = "keyframe_gesture_priority";


//! defines the acks we use
//typedef std_msgs::Time Ack;
typedef std_msgs::Header Ack;


} // end namespace gesture_player

#endif // GESTURE_DEFINITIONS_H
