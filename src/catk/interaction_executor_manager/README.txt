Run the following command to test the interaction executor project:

	$ roslaunch interaction_executor_manager interaction_executor_tests.launch robot:=mbotXX

This launch file starts other required nodes which are part of the gesture_player project.

Then, you can publish topics to see how the mbot plays gestures:

	$ rostopic pub /mbotXX/express_gesture monarch_msgs/GestureExpression ...

The field "gesture" in the GestureExpression message has to be set to one of the xml file name (without extension) where a gesture is defined (for example give_greentins_child.xml).
These gesture xml files are locate in the "data" folder, inside gesture_player_utils project.
