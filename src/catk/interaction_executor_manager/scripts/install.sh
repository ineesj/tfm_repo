#!/bin/bash
#Install required libraries

sudo apt-get -y install cmake

mkdir ../third_parties
cd ../third_parties
wget https://github.com/jbeder/yaml-cpp/archive/release-0.5.3.tar.gz

tar -xzf release-0.5.3.tar.gz
cd yaml-cpp-release-0.5.3

mkdir build
cd build

cmake ..
sudo make
sudo make install 

