#include <algorithm>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>
#include "monarch_msgs/GestureExpression.h"
#include "monarch_msgs/InteractionExecutorStatus.h"
#include "monarch_msgs/KeyValuePair.h"
#include "monarch_msgs/KeyValuePairArray.h"
#include "gesture_definitions.h"
#include "gesture_io.h"
#include "gesture_player_utils/KeyframeGesture.h"
#include "gesture_player_utils/KeyframeGesturePriority.h"
#include "gesture_player_utils/GestureStatus.h"


//Global variables
bool arms_allowed_;
bool audio_allowed_;
bool head_allowed_;
std::string current_ce_;

// Dictionary to map a semantic value to the literal value corresponding
// to each interface, i.e. interactive_game: jogo interativo
std::map<std::string, std::map<std::string, std::string> > interfaces_params_;

//To make more readable code
typedef gesture_player_utils::KeyframeGesture KeyframeGesture;

//Publisher declarations
ros::Publisher play_gesture_pub_, ie_status_pub_;//, liveliness_status_pub_;

/////////////////////////////////////////////////////////////////////////
//AUX FUNCTIONS
/////////////////////////////////////////////////////////////////////////
std::vector<std::string> stringSplit(std::string input, char splitChar){
    std::istringstream ss(input.c_str());
    std::string token;
    std::vector<std::string> tokens;

    while(std::getline(ss, token, splitChar)) {
        tokens.push_back(token);
    }

    return tokens;
}
/////////////////////////////////////////////////////////////////////////

//Usage:   replace(string, "$name", "Somename");
bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

/////////////////////////////////////////////////////////////////////////

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

/////////////////////////////////////////////////////////////////////////

/**
 * It takes the name of the gesture file, completes the path and loads the
 * xml into the KeyframeGesture structure
 */
bool loadGestureFromFile(const std::string fileName,
              gesture_player_utils::KeyframeGesture & gesture) {

    //create path
    std::ostringstream xml_full_path;
    xml_full_path << gesture_player::gesture_files_folder() << "/"
                  << fileName << ".xml";

    //read
    bool success = gesture_io::load_from_xml_file
            (gesture, xml_full_path.str());

    return success;
}

/////////////////////////////////////////////////////////////////////////
std::vector<std::string> interfacesMapping(std::vector<std::string> high_level_ifaces){

    std::vector<std::string> low_level_ifaces;

    for (unsigned int i = 0; i<high_level_ifaces.size(); i++){
        if (high_level_ifaces.at(i) == "arms"
                || high_level_ifaces.at(i) == "head"
                || high_level_ifaces.at(i) == "mouth"
                || high_level_ifaces.at(i) == "voice"
                || high_level_ifaces.at(i) == "projector"){

            low_level_ifaces.push_back(high_level_ifaces.at(i));
        }
        else if (high_level_ifaces.at(i) == "eyes"){
            low_level_ifaces.push_back("LeftEyeLeds");
            low_level_ifaces.push_back("RightEyeLeds");
        }
        else if (high_level_ifaces.at(i) == "leds_base"){
            low_level_ifaces.push_back("BaseRightLeds");
            low_level_ifaces.push_back("BaseFrontLeds");
            low_level_ifaces.push_back("BaseLeftLeds");
        }
        else if (high_level_ifaces.at(i) == "cheeks"){
            low_level_ifaces.push_back("CheeksLeds");
        }
        else if (high_level_ifaces.at(i) == "image"){
            low_level_ifaces.push_back("screen");
        }
        else if (high_level_ifaces.at(i) == "audio"){
            low_level_ifaces.push_back("nonverbalsound");
        }
    }
    return low_level_ifaces;
}

/////////////////////////////////////////////////////////////////////////

/**
 * It returns a gesture which only takes the allowed_interfaces from the original
 */
KeyframeGesture filterInterfaces(std::vector<std::string> allowed_interfaces, KeyframeGesture original){

    KeyframeGesture filtered_gesture;
    filtered_gesture.gesture_name = original.gesture_name;

    for (unsigned int i = 0; i < original.keytimes.size(); i++) {
        // check if the current joint is any of the allowed interfaces
        if ( (std::count (allowed_interfaces.begin(),
                  allowed_interfaces.end(),
                  original.joint_names[i]) ) != 0){

            filtered_gesture.keytimes.push_back(original.keytimes[i]);
            filtered_gesture.joint_names.push_back(original.joint_names[i]);
            filtered_gesture.joint_values.push_back(original.joint_values[i]);
        }
    }

    gesture_io::sortKeyframeGesture(filtered_gesture);
    return filtered_gesture;

}

/////////////////////////////////////////////////////////////////////////

/**
 * It returns a gesture substituting the parameters with their values,
 * depending on the characteristics of each joint.
 *
 * For a parameter, its
 * name and semantic value are provided; then, the 'literal' values for
 * each joint are obtained from dictionaries
 */
KeyframeGesture substituteGestureParams(std::string param_name, std::string semantic_value, KeyframeGesture original){

    KeyframeGesture result = original;

    for (unsigned int i = 0; i < original.keytimes.size(); i++) {
        replaceAll(result.joint_values[i], param_name, semantic_value);
    }

    return result;

}


/////////////////////////////////////////////////////////////////////////
//CALLBACKS
/////////////////////////////////////////////////////////////////////////

void expression_cb(const monarch_msgs::GestureExpression::ConstPtr& msg){

    KeyframeGesture keyframe_gesture;

    //Save that this is the current communicative expression being executed
    current_ce_ = msg->gesture;

    //load xml file
    loadGestureFromFile(msg->gesture, keyframe_gesture);

    ROS_INFO("[IEM] Requested CE: %s \n", current_ce_.c_str());

    std::vector<std::string> allowed_interfaces;

    std::vector<monarch_msgs::KeyValuePair> kvpa = msg->params.array;

    for (unsigned int i=0; i<kvpa.size(); i++){

        if (kvpa[i].key == "allowed_interfaces"){
            allowed_interfaces = interfacesMapping(stringSplit(kvpa[i].value, '|'));
            keyframe_gesture = filterInterfaces(allowed_interfaces, keyframe_gesture);
        }

        else{
            //Temporal dictionary
            std::string final_value;
            if (kvpa[i].value == "interactive_game")        final_value = "jogo interativo";
            else if (kvpa[i].value == "catch_and_touch")    final_value = "pegar e tocar";
            else                                            final_value = kvpa[i].value;

            keyframe_gesture = substituteGestureParams(kvpa[i].key, final_value, keyframe_gesture);
        }

    }

	gesture_player_utils::KeyframeGesturePriority gesture_msg;
    keyframe_gesture.gesture_name = msg->gesture;
	gesture_msg.gesture = keyframe_gesture;
    gesture_msg.priority = "preemptive_empty_queue";
    play_gesture_pub_.publish(gesture_msg);
}

/////////////////////////////////////////////////////////////////////////

void reactive_cb(const std_msgs::String::ConstPtr& msg){

    KeyframeGesture keyframe_gesture;

    //load xml file
    loadGestureFromFile(msg->data, keyframe_gesture);

    std::vector<std::string> allowed_interfaces;
    allowed_interfaces.push_back("mouth");
    allowed_interfaces.push_back("LeftEyeLeds");
    allowed_interfaces.push_back("RightEyeLeds");
    allowed_interfaces.push_back("BaseRightLeds");
    allowed_interfaces.push_back("BaseFrontLeds");
    allowed_interfaces.push_back("BaseLeftLeds");
    allowed_interfaces.push_back("CheeksLeds");
    allowed_interfaces.push_back("screen");

    if (head_allowed_){
        allowed_interfaces.push_back("head");
    }
    if (arms_allowed_){
                allowed_interfaces.push_back("arms");
        }
    if (audio_allowed_){
                allowed_interfaces.push_back("voice");
                allowed_interfaces.push_back("nonverbalsound");
        }

    keyframe_gesture = filterInterfaces(allowed_interfaces, keyframe_gesture);

    gesture_player_utils::KeyframeGesturePriority gesture_msg;
    keyframe_gesture.gesture_name = msg->data;
    gesture_msg.gesture = keyframe_gesture;
    //Queue the gesture because it has no special meaning, it doesn't matter
    //if the robot takes a while to perform it
    gesture_msg.priority = "reactive_preemptive_queue_beg";
    play_gesture_pub_.publish(gesture_msg);

    ROS_DEBUG("Requested liveliness gesture: %s \n", gesture_msg.gesture.gesture_name.c_str());
}

/////////////////////////////////////////////////////////////////////////

void liveliness_cb(const std_msgs::String::ConstPtr& msg){

    KeyframeGesture keyframe_gesture;

    //load xml file
    loadGestureFromFile(msg->data, keyframe_gesture);
    
    std::vector<std::string> allowed_interfaces;
    allowed_interfaces.push_back("mouth");
    allowed_interfaces.push_back("LeftEyeLeds");
    allowed_interfaces.push_back("RightEyeLeds");
    allowed_interfaces.push_back("BaseRightLeds");
    allowed_interfaces.push_back("BaseFrontLeds");
    allowed_interfaces.push_back("BaseLeftLeds");
    allowed_interfaces.push_back("CheeksLeds");
    allowed_interfaces.push_back("screen");

    if (head_allowed_){
        allowed_interfaces.push_back("head");
    }
    if (arms_allowed_){
		allowed_interfaces.push_back("arms");
	}
    if (audio_allowed_){
		allowed_interfaces.push_back("voice");
		allowed_interfaces.push_back("nonverbalsound");
	}  

    keyframe_gesture = filterInterfaces(allowed_interfaces, keyframe_gesture);

    gesture_player_utils::KeyframeGesturePriority gesture_msg;
    keyframe_gesture.gesture_name = msg->data;
    gesture_msg.gesture = keyframe_gesture;
    // Just in case the gesture is related to the transition to the sleeping state, it is executed with "empty_queue" policy.
    if(msg->data.find("mbot_sleeping")!=std::string::npos) {
	    gesture_msg.priority = "empty_queue";
    }
    else {
		//Queue the gesture because it has no special meaning, it doesn't matter
		//if the robot takes a while to perform it
		gesture_msg.priority = "queue_end";
	}
    play_gesture_pub_.publish(gesture_msg);

    ROS_DEBUG("Requested liveliness gesture: %s \n", gesture_msg.gesture.gesture_name.c_str());
}

/////////////////////////////////////////////////////////////////////////
/*
void arms_behaviour_cb(const std_msgs::Bool::ConstPtr& msg){
    arms_allowed_ = msg->data;
}
*/
/////////////////////////////////////////////////////////////////////////


/**
 * When a gesture is finished, this republishes a message to tell the dialog_manager
 */
void end_gesture_cb(const gesture_player_utils::GestureStatusConstPtr& msg){
    ROS_DEBUG("Gesture from file %s is over with status %s", msg->gesture.c_str(), msg->status.c_str());

    // If it is a liveliness gesture, the dialog_manager is not interested in knowing that it
    // has finished. That is only for communicative expressions
    if (msg->gesture == current_ce_){

        ROS_INFO("[IEM] Ended current CE: %s\n", current_ce_.c_str());
        monarch_msgs::InteractionExecutorStatus ie_status_msg;
        ie_status_msg.expression_name=current_ce_;
        ie_status_msg.status=msg->status;
        ie_status_pub_.publish(ie_status_msg);
        current_ce_ = "";
    }
}

/////////////////////////////////////////////////////////////////////////

void allowed_interfaces_cb(const std_msgs::StringConstPtr& msg){

    std::vector<std::string> allowed_interfaces;
    allowed_interfaces = interfacesMapping(stringSplit(msg->data, '|'));

    //Filter the head for the liveliness gestures
    if (! ( std::find(allowed_interfaces.begin(), allowed_interfaces.end(), "head")
            != allowed_interfaces.end())) {

        head_allowed_ = false;
    }

}

/////////////////////////////////////////////////////////////////////////
//MAIN
/////////////////////////////////////////////////////////////////////////

/**
 * This node receives the msgs from the dialog_manager with the communicative expression
 * to perform and its parameters. Then, it opens the corresponding xml file (with the
 * predefined gestures) and manipulates it according to the parameters received
 */
int main(int argc, char **argv){
    ros::init(argc, argv, "interaction_executor_manager");
    ros::NodeHandle nh_public;
    ros::NodeHandle nh_private("~");

    //subscribers
    ros::Subscriber expression_sub = nh_public.subscribe("express_gesture", 0, expression_cb);
    ros::Subscriber mode_end_sub = nh_public.subscribe("end_gesture", 0, end_gesture_cb);
    ros::Subscriber liveliness_sub = nh_public.subscribe("liveliness_gestures", 0, liveliness_cb);
   // ros::Subscriber arms_behaviour_sub = nh_public.subscribe("liveliness_arms_behaviour", 0, arms_behaviour_cb);
    ros::Subscriber allowed_interfaces_sub = nh_public.subscribe("allowed_hri_interfaces", 0, allowed_interfaces_cb);
    ros::Subscriber reactive_sub = nh_public.subscribe("reactive_gestures", 0, reactive_cb);

    //advertising
    ie_status_pub_ = nh_public.advertise<monarch_msgs::InteractionExecutorStatus>("gesture_status", 0);
    play_gesture_pub_ = nh_public.advertise<gesture_player_utils::KeyframeGesturePriority>
											(gesture_player::gesture_with_priority_topic, 0);

    //liveliness_status_pub_ = nh_public.advertise<std_msgs::Int16>("liveliness_status_update", 0);

    //Get the dictionary to map semantic values to literal values in different interfaces
    //nh_private.getParam("liveliness_random_gestures", interfaces_params_);


    sleep(3);   //Wait until publishers and subscribers are created before publishing a msg

    //init
    audio_allowed_ = true;
    arms_allowed_ = true;
    head_allowed_ = true;

    //Activating liveliness
    //std_msgs::Int16 msg;
    //msg.data = 1; // 1 = liveliness ACTIVE
    //liveliness_status_pub_.publish(msg);
    //ROS_INFO("Activating 'show_liveliness' on topic: liveliness_status_update");

    ROS_INFO("Interaction Executor running");
    ros::spin();

    return 0;
}
