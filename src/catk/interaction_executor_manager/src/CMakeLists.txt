#interaction_executor
add_executable(interaction_executor_node.exe
               interaction_executor_node.cpp)

target_link_libraries(interaction_executor_node.exe
                    xml_document
                    ${catkin_LIBRARIES})

add_dependencies(interaction_executor_node.exe monarch_msgs_generate_messages_cpp)



#qt_interface_for_teleoperation_of_interaction
add_subdirectory(mbot_interaction_teleop)
