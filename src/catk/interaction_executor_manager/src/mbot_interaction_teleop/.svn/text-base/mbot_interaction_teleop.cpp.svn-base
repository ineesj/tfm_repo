#include "mbot_interaction_teleop.h"
#include "src/mbot_interaction_teleop/ui_mbot_interaction_teleop.h"

MbotInteractionTeleop::MbotInteractionTeleop(ros::NodeHandle n, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MbotInteractionTeleop){
    ui->setupUi(this);

    //ROS PUBLISHERS
    screen_show_hide_ =  n.advertise<std_msgs::String>("screen/show_hide", 1);
    screen_image_ = n.advertise<std_msgs::String>("screen/display_image", 1);
    screen_input_menu_ = n.advertise<monarch_msgs::KeyValuePairArray>("hri_interface_config", 1);
    etts_ = n.advertise<etts_msgs::Utterance>("etts", 1);
    gesture_player_ = n.advertise<std_msgs::String>("keyframe_gesture_filename", 1);
    vumeter_ = n.advertise<monarch_msgs::MouthVumeterControl>("cmd_mouthVumeter", 0);


    //Event filter to register key events on different components
    ui->ettsTextEdit->installEventFilter(this);
    ui->screenImageComboBox->installEventFilter(this);
    ui->screenMenuComboBox->installEventFilter(this);
    ui->gesturePlayerComboBox->installEventFilter(this);
}

MbotInteractionTeleop::~MbotInteractionTeleop(){
    delete ui;
}


////////////////////////////////////////////////////////////////////////////////
///GENERAL FUNCTIONALITY
////////////////////////////////////////////////////////////////////////////////
/*
 * Captures enter key events in different widgets in order to do the
 * same action as the corresponding push button
 */
bool MbotInteractionTeleop::eventFilter(QObject *object, QEvent *event){
    if (event->type() == QEvent::KeyPress){
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return){
            if (object == ui->ettsTextEdit)                 this->etts_say_sentence();
            else if (object == ui->screenImageComboBox)     this->display_screen_image();
            else if (object == ui->screenMenuComboBox)      this->show_screen_menu();
            else if (object == ui->gesturePlayerComboBox)   this->make_gesture();
            return true;
        }
    }

    return QObject::eventFilter(object, event);
}


//////////////////////////////////////////////////////////////////////////
// GESTURE_PLAYER
//////////////////////////////////////////////////////////////////////////
void MbotInteractionTeleop::make_gesture(){
    std_msgs::String msg;
    msg.data = ui->gesturePlayerComboBox->currentText().toStdString() + ";preemptive_queue_beg";
    gesture_player_.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
// ETTS
//////////////////////////////////////////////////////////////////////////
void MbotInteractionTeleop::etts_say_sentence(){
    etts_msgs::Utterance msg;
    msg.text = std::string(ui->ettsTextEdit->toPlainText().toLocal8Bit().constData());
    msg.language = ui->ettsLangComboBox->currentText().toStdString();
    if (ui->ettsPrimComboBox->currentText() == "google"){
        msg.primitive = etts_msgs::Utterance::PRIM_GOOGLE;
    }
    else if (ui->ettsPrimComboBox->currentText() == "espeak"){
        msg.primitive = etts_msgs::Utterance::PRIM_ESPEAK;
    }
    etts_.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
// SCREEN
//////////////////////////////////////////////////////////////////////////

void MbotInteractionTeleop::on_hideScreenButton_clicked(){
    show_hide_screen("hide");
}

void MbotInteractionTeleop::on_showScreenButton_clicked(){
    show_hide_screen("show");
}

void MbotInteractionTeleop::on_showFullScreenButton_clicked(){
    show_hide_screen("fullscreen");
}

void MbotInteractionTeleop::show_hide_screen(std::string param){
    std_msgs::String msg;
    msg.data = param;
    screen_show_hide_.publish(msg);
}

void MbotInteractionTeleop::show_screen_menu(){
    monarch_msgs::KeyValuePairArray msg;
    monarch_msgs::KeyValuePair item;
    item.key = "show_screen_menu";
    item.value = ui->screenMenuComboBox->currentText().toStdString();
    msg.array.push_back(item);
    screen_input_menu_.publish(msg);

}

void MbotInteractionTeleop::display_screen_image(){
    std_msgs::String msg;
    msg.data = ui->screenImageComboBox->currentText().toStdString();
    ROS_INFO("Asking the screen to display image: %s", msg.data.c_str());
    screen_image_.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
// VUMETER
//////////////////////////////////////////////////////////////////////////

void MbotInteractionTeleop::on_vumeterONradioButton_clicked(){
    monarch_msgs::MouthVumeterControl msg;
    msg.mouthVumeter = true;
    msg.mouthStyle = "openmouth";
    vumeter_.publish(msg);
}

void MbotInteractionTeleop::on_vumeterOFFradioButton_clicked(){
    monarch_msgs::MouthVumeterControl msg;
    msg.mouthVumeter = false;
    vumeter_.publish(msg);
}
