#ifndef MBOT_INTERACTION_TELEOP_H
#define MBOT_INTERACTION_TELEOP_H

#include <QMainWindow>

//ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include "ros/package.h"

// MONARCH
#include "etts_msgs/Utterance.h"
#include "monarch_msgs/ArmsControl.h"
#include "monarch_msgs/HeadControl.h"
#include "monarch_msgs/LedControl.h"
#include "monarch_msgs/GestureExpression.h"
#include "monarch_msgs/KeyValuePairArray.h"
#include "monarch_msgs/MouthVumeterControl.h"

//Teleop
#include "src/mbot_interaction_teleop/ui_mbot_interaction_teleop.h"

namespace Ui {
class MbotInteractionTeleop;
}

class MbotInteractionTeleop : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MbotInteractionTeleop(ros::NodeHandle n, QWidget *parent = 0);
    ~MbotInteractionTeleop();

public slots:
    bool eventFilter(QObject *object, QEvent *event);
    
private slots:
    void on_hideScreenButton_clicked();
    void on_showScreenButton_clicked();
    void on_showFullScreenButton_clicked();

    void on_vumeterONradioButton_clicked();

    void on_vumeterOFFradioButton_clicked();

private:
    Ui::MbotInteractionTeleop *ui;

    ros::Publisher gesture_player_, gesture_expression_, screen_show_hide_,
    screen_image_, screen_input_menu_, etts_, head_, arms_, vumeter_;

    //aux functions
    void show_hide_screen(std::string param);
    void show_screen_menu();
    void display_screen_image();
    void make_gesture();
    void etts_say_sentence();
};

#endif // MBOT_INTERACTION_TELEOP_H
