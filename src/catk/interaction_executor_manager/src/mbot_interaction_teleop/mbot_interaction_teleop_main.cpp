//Qt
#include <QApplication>
#include <QObject>
#include <QPalette>
#include <QDesktopWidget>
#include <QStringList>
#include "yaml-cpp/yaml.h"

//ROS
#include "ros/package.h"

//mbot_interaction_teleop
#include "mbot_interaction_teleop.h"



////////////////////////////////////////////////////////////////////////////////
///MAIN
////////////////////////////////////////////////////////////////////////////////


int main(int argc, char **argv){
	
	
    ///Ros node initialization
    ros::init(argc, argv, "mbot_interaction_teleop");
    ros::NodeHandle nh_public;

    ///Creating Qt application
    QApplication app(argc, argv);

    MbotInteractionTeleop* mit = new MbotInteractionTeleop(nh_public);
    mit->show();


    return app.exec();
}
