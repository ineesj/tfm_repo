#-------------------------------------------------
#
# Project created by QtCreator 2014-04-09T09:23:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ejer1_factorial
TEMPLATE = app


SOURCES += mbot_interaction_teleop_main.cpp\
        mbot_interaction_teleop.cpp\


HEADERS  += mbot_interaction_teleop.h


FORMS    += mbot_interaction_teleop.ui


