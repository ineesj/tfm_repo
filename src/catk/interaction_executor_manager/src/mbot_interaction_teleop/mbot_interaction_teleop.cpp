

#include "mbot_interaction_teleop.h"
#include "src/mbot_interaction_teleop/ui_mbot_interaction_teleop.h"
#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <QStringList>
#include "QObjectList"
#include <QDir>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <ros/package.h>



using namespace std;
using namespace YAML;



MbotInteractionTeleop::MbotInteractionTeleop(ros::NodeHandle n, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MbotInteractionTeleop){
    ui->setupUi(this);

    //ROS PUBLISHERS
    screen_show_hide_ =  n.advertise<std_msgs::String>("screen/show_hide", 1);
    screen_image_ = n.advertise<std_msgs::String>("screen/display_image", 1);
    screen_input_menu_ = n.advertise<monarch_msgs::KeyValuePairArray>("hri_interface_config", 1);
    interaction_executor_ = n.advertise<monarch_msgs::KeyValuePairArray>("ca_activations", 1);
    Projector_ = n.advertise<monarch_msgs::VideoProjectorControl>("cmd_videoProjector", 1);
    etts_ = n.advertise<etts_msgs::Utterance>("etts", 1);
    gesture_player_ = n.advertise<monarch_msgs::GestureExpression>("express_gesture", 1);
    vumeter_ = n.advertise<monarch_msgs::MouthVumeterControl>("cmd_mouthVumeter", 0);

    string PCE=ros::package::getPath("gesture_player_utils") + "/data";
    QString PathCE=QString::fromStdString(PCE);
    QStringList listaCE=QDir(PathCE).entryList();
    listaCE.replaceInStrings(QRegExp(".xml"),"");

    string PCA=ros::package::getPath("ca_interactive_tests") + "/bagys";
    QString PathCA=QString::fromStdString(PCA);
    QStringList listaCA=QDir(PathCA).entryList();
    //listaCA.replaceInStrings(QRegExp(".bagy"),"");

    string PIM=ros::package::getPath("screen") + "/data";
    QString PathIM=QString::fromStdString(PIM);
    QStringList listaImage=QDir(PathIM).entryList();

    listaImage.replaceInStrings(QRegExp(".gif"),"");
    listaImage.replaceInStrings(QRegExp(".png"),"");
    listaImage.replaceInStrings(QRegExp(".jpg"),"");

    // Para añadir los datos a los comboBox
    ui->gesturePlayerComboBox->addItems(listaCE);
    ui->screenImageComboBox->addItems(listaImage);
    ui->interactionExecutorcomboBox->addItems(listaCA);

}


MbotInteractionTeleop::~MbotInteractionTeleop(){
    delete ui;
}


//////////////////////////////////////////////////////////////////////////
// GESTURE_PLAYER
//////////////////////////////////////////////////////////////////////////

void MbotInteractionTeleop::on_pushButton_GP_clicked()
{

    bool head= ui->headCheckBox->isChecked();
    bool eyes= ui->eyesCheckBox->isChecked();
    bool voice= ui->voiceCheckBox->isChecked();
    bool image= ui->imageCheckBox->isChecked();
    bool mouth= ui->mouthCheckBox->isChecked();
    bool arms= ui->armsCheckBox->isChecked();
    bool projector= ui->projectorCheckBox->isChecked();
    bool leds_base= ui->leds_baseCheckBox->isChecked();
    bool cheeks= ui->cheeksCheckBox->isChecked();
    bool audio= ui->audioCheckBox->isChecked();

    string head_;
    string  arms_;
    string  eyes_;
    string  voice_;
    string  image_;
    string  mouth_;
    string  projector_;
    string leds_base_;
    string  cheeks_;
    string  audio_;
    string  emply;

    if (arms==true)         arms_="arms|";
    if (head==true)         head_="head|";
    if (eyes==true)         eyes_="eyes|";
    if (voice==true)        voice_="voice|";
    if (image==true)        image_="image|";
    if (mouth==true)        mouth_="month|";
    if (audio==true)        audio_="audio|";
    if (projector==true)    projector_="projector|";
    if (leds_base==true)    leds_base_="leds_base|";
    if (cheeks==true)       cheeks_="cheeks|";
    if( (head== false) && (arms== false) && (eyes== false) && (voice== false) && (image== false) &&  (mouth == false) && (projector== false) && (leds_base== false) && (cheeks== false) && (audio==false))
    {
            emply="a";
    }

    string value = emply + arms_+ head_+ mouth_+ eyes_ + voice_ + audio_ + image_ + projector_ + leds_base_ + cheeks_ ;
    int s=value.size();
    value.erase(s-1,s-1);
    make_gesture(value);
    std::cout <<value << std::endl;


}


void MbotInteractionTeleop::make_gesture(string v){

    monarch_msgs::GestureExpression msg;
    monarch_msgs::KeyValuePair item;
    msg.gesture  = ui->gesturePlayerComboBox->currentText().toStdString();// + ";preemptive_queue_beg";
    int s=v.size();
    if (s>1){
        item.key = "allowed_interfaces";
        item.value=v;
        msg.params.array.push_back(item);
    }
    gesture_player_.publish(msg);
    ROS_INFO("Se envia gestur_player");

}


void MbotInteractionTeleop::on_pushButton_IE_clicked()

{
    make_gesture_interaction_executor();
}



void MbotInteractionTeleop::make_gesture_interaction_executor(){

    monarch_msgs::KeyValuePairArray msg;
    monarch_msgs::KeyValuePair item;
    vector<string> vectorkey;
    vector<string> vectorvalue;
    string data = ui->interactionExecutorcomboBox->currentText().toStdString();
    string ruta;

    ruta=ros::package::getPath("ca_interactive_tests") + "/bagys/" + data ;

    vector<Node> doc = YAML::LoadAllFromFile(ruta);

    for (vector<Node>::iterator it= doc.begin(); it != doc.end(); it++) {

      YAML::Node alldoc=*it;
      YAML::Node array=alldoc["array"];

      for (YAML::iterator it = array.begin(); it != array.end(); ++it) {

              YAML::Node array_= *it;

               for (YAML::iterator i = array_.begin(); i != array_.end(); ++i){
                   YAML::Node f=i->first;
                   YAML::Node s=i->second;
                   string key1="key";
                   int k=key1.compare(f.as<string>());
                   if (k==0)    vectorkey.push_back(s.as<string>());
                   else         vectorvalue.push_back(s.as<string>());
           }

      }


    }
        for (int j=0 ; j!=vectorkey.size(); j++){

            item.key = vectorkey[j];
            item.value = vectorvalue[j];
            msg.array.push_back(item);

        }

       interaction_executor_ .publish(msg);
       ROS_INFO("Se envia  interaction_executor");


}


//////////////////////////////////////////////////////////////////////////
// ETTS
//////////////////////////////////////////////////////////////////////////



void MbotInteractionTeleop::on_pushButton_Etts_clicked()
{
    etts_say_sentence();
}

void MbotInteractionTeleop::etts_say_sentence(){
    etts_msgs::Utterance msg;
    msg.text = std::string(ui->ettsTextEdit->toPlainText().toLocal8Bit().constData());
    msg.language = ui->ettsLangComboBox->currentText().toStdString();
    if (ui->ettsPrimComboBox->currentText() == "google"){
        msg.primitive = etts_msgs::Utterance::PRIM_GOOGLE;
        ROS_INFO("Se envia msg google");
    }
    else if (ui->ettsPrimComboBox->currentText() == "espeak"){
        msg.primitive = etts_msgs::Utterance::PRIM_ESPEAK;
        ROS_INFO("Se envia espeak");
    }
    etts_.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
// SCREEN
//////////////////////////////////////////////////////////////////////////

void MbotInteractionTeleop::on_hideScreenButton_clicked(){
    show_hide_screen("hide");
    ROS_INFO("Has pulsado hide");
}

void MbotInteractionTeleop::on_showScreenButton_clicked(){
    show_hide_screen("show");
    ROS_INFO("Has pulsado show");
}

void MbotInteractionTeleop::on_showFullScreenButton_clicked(){
    show_hide_screen("fullscreen");
    ROS_INFO("Has pulsado full");
}

void MbotInteractionTeleop::show_hide_screen(std::string param){
    std_msgs::String msg;
    msg.data = param;
    screen_show_hide_.publish(msg);
    ROS_INFO("Se envia el msg screen_show_hide_full");
}



void MbotInteractionTeleop::on_pushButton_M_clicked()
{
    show_screen_menu();

}
void MbotInteractionTeleop::show_screen_menu(){
    monarch_msgs::KeyValuePairArray msg;
    monarch_msgs::KeyValuePair item;
    item.key = "show_screen_menu";
    item.value = ui->screenMenuComboBox->currentText().toStdString();
    msg.array.push_back(item);
    screen_input_menu_.publish(msg);
    ROS_INFO("Se envia screen_input_menu");

}

void MbotInteractionTeleop::on_pushButton_IM_clicked()
{
    display_screen_image();
}

void MbotInteractionTeleop::display_screen_image(){
    std_msgs::String msg;
    msg.data = ui->screenImageComboBox->currentText().toStdString();
    ROS_INFO("Asking the screen to display image: %s", msg.data.c_str());
    screen_image_.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
// VUMETER
//////////////////////////////////////////////////////////////////////////

void MbotInteractionTeleop::on_vumeterONradioButton_clicked(){
    monarch_msgs::MouthVumeterControl msg;
    msg.mouthVumeter = true;
    msg.mouthStyle = "openmouth";
    vumeter_.publish(msg);
    ROS_INFO("Open mouth");


}

void MbotInteractionTeleop::on_vumeterOFFradioButton_clicked(){
    monarch_msgs::MouthVumeterControl msg;
    msg.mouthVumeter = false;
    vumeter_.publish(msg);
    ROS_INFO("close mouth");

}

//////////////////////////////////////////////////////////////////////////
// Projector
//////////////////////////////////////////////////////////////////////////




void MbotInteractionTeleop::on_ProjectorOn_clicked()
{
    monarch_msgs::VideoProjectorControl msg;
    msg.videoProjector = true;
    Projector_.publish(msg);
    ROS_INFO("Projector On");

}

void MbotInteractionTeleop::on_ProjectorOff_clicked()
{
    monarch_msgs::VideoProjectorControl msg;
    msg.videoProjector = false;
    Projector_.publish(msg);
    ROS_INFO("Projector Off");
}
