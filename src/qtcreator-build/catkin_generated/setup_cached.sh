#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CATKIN_TEST_RESULTS_DIR="/home/ines/catkin_ws/src/qtcreator-build/test_results"
export CMAKE_PREFIX_PATH="/home/ines/catkin_ws/src/qtcreator-build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/ines/catkin_ws/src/qtcreator-build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/ines/catkin_ws/src/qtcreator-build/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/ines/catkin_ws/src/qtcreator-build/devel/bin:/home/ines/catkin_ws/devel/bin:/opt/ros/hydro/bin:/usr/bin:/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"
export PKG_CONFIG_PATH="/home/ines/catkin_ws/src/qtcreator-build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/ines/catkin_ws/src/qtcreator-build"
export PYTHONPATH="/home/ines/catkin_ws/src/qtcreator-build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/ines/catkin_ws/src/qtcreator-build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_MAVEN_DEPLOYMENT_REPOSITORY="/home/ines/catkin_ws/src/qtcreator-build/devel/share/maven"
export ROS_MAVEN_PATH="/home/ines/catkin_ws/src/qtcreator-build/devel/share/maven:$ROS_MAVEN_PATH"
export ROS_PACKAGE_PATH="/home/ines/catkin_ws/src/robot_liveliness:$ROS_PACKAGE_PATH"
export ROS_TEST_RESULTS_DIR="/home/ines/catkin_ws/src/qtcreator-build/test_results"