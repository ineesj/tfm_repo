set(_CATKIN_CURRENT_PACKAGE "robot_liveliness")
set(robot_liveliness_MAINTAINER "ines <ines@todo.todo>")
set(robot_liveliness_DEPRECATED "")
set(robot_liveliness_VERSION "0.0.0")
set(robot_liveliness_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(robot_liveliness_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(robot_liveliness_BUILDTOOL_DEPENDS "catkin")