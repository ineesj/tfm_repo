#!/usr/bin/env python
"""
State Machine for patrolling and docking in UC3M Roboticslab facilities.

:version: 0.2
:date: Feb 2016
:author: Victor Gonzalez-Pacheco
"""


import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from uc3m_behaviors import machines

import sys
import os.path
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))


# Main function
def main():
    rospy.init_node("patrolling_and_dock")

    # State Machines
    sm_initial_pose = machines.make_set_inital_pose_sm()
    sm_undock = machines.make_undock_sm()
    sm_dock = machines.make_dock_sm()
    sm_charging = machines.make_charging_sm()
    sm_patrolling = machines.make_patrolling_sm()
    sm_batteries = machines.make_batteries_monitor_sm()
    sm_simple_patrolling = \
        machines.make_patrolling_and_batt_sm(sm_patrolling, sm_batteries)

    # Main State Machine
    sm_msm = smach.StateMachine(outcomes=['succeeded', 'preempted', 'aborted'])

    with sm_msm:
        sm_msm.add('INITAL_POSE_SM', sm_initial_pose,
                   transitions={'succeeded': 'CHARGING_SM'})
        sm_msm.add('UNDOCK_SM', sm_undock,
                   transitions={'succeeded': 'SIMPLE_PATROLLING'})
        sm_msm.add('SIMPLE_PATROLLING', sm_simple_patrolling,
                   transitions={'low_battery': 'DOCK_SM'})  # ,
                                # 'preempted': 'DOCK_SM'})
        sm_msm.add('DOCK_SM', sm_dock,
                   transitions={'succeeded': 'CHARGING_SM'})
        sm_msm.add('CHARGING_SM', sm_charging,
                   transitions={'succeeded': 'SIMPLE_PATROLLING',
                                'preempted': 'UNDOCK_SM',
                                'aborted': 'SIMPLE_PATROLLING'})
        # sm_msm.set_initial_state(['CHARGING_SM'])
        sm_msm.set_initial_state(['INITAL_POSE_SM'])

    sis = smach_ros.IntrospectionServer('patrolling_and_dock',
                                        sm_msm, '/SM_ROOT')
    sis.start()

    outcome = sm_msm.execute()


if __name__ == '__main__':
    main()
