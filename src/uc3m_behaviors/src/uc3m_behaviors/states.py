"""
Library of States for UC3M behaviors.

:author: Victor Gonzalez Pacheco
:date: Feb 2016
"""

import roslib; roslib.load_manifest('uc3m_behaviors')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import (BatteriesVoltage,
                              ChargerStatus,
                              RfidReading,
                              SetStateAuxiliaryPowerBattery,
                              SetStateElectronicPower,
                              SetStateMotorsPower)

from std_msgs.msg import Header
from geometry_msgs.msg import (Point,
                               Pose,
                               PoseWithCovariance,
                               PoseWithCovarianceStamped,
                               Quaternion)

from monarch_behaviors import behavior_utils as butils

import numpy as np
import sys
import os.path
import rospkg
import math

rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))
from scout_msgs.srv import TeleOpSrv

RRAD = 0.41/2

STDEV_XY = 0.2
STDEV_TH = np.deg2rad(30)

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
COV0 = [STDEV_XY**2, 0, 0, 0, 0, 0,
        0, STDEV_XY**2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, STDEV_TH**2]


def msg_values(msg):
    """Return all values of a ROS msg without the header."""
    return (getattr(msg, msg_field) for msg_field in msg.__slots__[1:])


class StateSetInitialPose(smach.State):
    """Sets Initial Pose of the Robot."""
    def __init__(self, wp):
        self.init_pub = \
            rospy.Publisher("initialpose", PoseWithCovarianceStamped, latch=True)
        smach.State.__init__(self, outcomes=['completed'])
        self.wp = wp

    def execute(self, userdata):
        pose_msg = self._make_pose_msg(self.wp)
        rospy.loginfo('Pose Msg: {}'.format(pose_msg))
        self.init_pub.publish(pose_msg)
        return 'completed'

    def _make_pose_msg(self, wp):
        """Create a PoseWithCovarianceStamped msg form a waypoint."""
        p = Point(0, 0, 0)
        q = Quaternion(0, 0, 0, 1)
        pose = PoseWithCovariance(pose=Pose(position=p, orientation=q),
                                  covariance=COV0)
        pose_msg = PoseWithCovarianceStamped(header=Header(frame_id="/map"),
                                             pose=pose)
        pose_msg.header.stamp = rospy.get_rostime()
        x, y, t = butils.unpack_waypoint(self.wp)
        pose_msg.pose.pose.position.x = x
        pose_msg.pose.pose.position.y = y
        pose_msg.pose.pose.orientation.z = math.sin(t/2.0)
        pose_msg.pose.pose.orientation.w = math.cos(t/2.0)
        return pose_msg


# Move to Waypoint State:
class MoveToWaypoint(MonarchState):

    """State to move to a point."""

    def __init__(self, waypoint):
        """Constructor of the State Class.

        @param waypoint: dict-like definition of a waypoint. See example below.

        Example of a waypoint:

            >>> wp = {id: '1.3.C12_charger',
                      name: docking_station,
                      x: 14.23,
                      y: 33.95,
                      z: 0.0,
                      theta: 0}
        """
        MonarchState.__init__(self, state_outcomes=['succeeded', 'preempted'],
                              input_keys=['user_goal'],
                              output_keys=['user_goal'])
        rospy.loginfo("Init  MoveTo{}State".format(waypoint['name']))

        x, y, theta = butils.unpack_waypoint(waypoint)
        nav_move_to = MoveToAction(self, x, y, theta)
        self.add_action(nav_move_to)


# Get RFID tag State Machine
class GetRfidTagState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded', 'preempted'],
                              input_keys=['user_goal'],
                              output_keys=['user_goal'])
        rospy.loginfo("Init GetRfidTagState")
        topicName = 'rfid_tag'
        conditionOutcomes = ['succeeded']
        topicCondition = \
            TopicCondition(self, topicName, RfidReading, self.rfidCondition)
        self.add_change_condition(topicCondition, conditionOutcomes)

    def rfidCondition(self, data, userdata):
        print "TopicCondition userdata"
        rospy.loginfo('user_goal = '+str(userdata.user_goal))
        value = 'succeeded'
        return value


class StateDocking(smach.State):
    """Moves the robot back for 5 seconds to secure it to the charger."""
    def __init__(self):
        smach.State.__init__(self, outcomes=['completed'])
        self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)

    def execute(self, userdata):
        self.teleop_srv(1, -0.2, 0)
        rospy.sleep(4)
        self.teleop_srv(0, 0, 0)
        return 'completed'


# Undock State Machine
class StateUndocking(smach.State):
    """Disconnects the robot from charger power and moves it forward."""
    def __init__(self):
        """Constructor."""
        smach.State.__init__(self, outcomes=['completed'])

        self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
        self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power",
                                            SetStateAuxiliaryPowerBattery,
                                            latch=True),
                            rospy.Publisher("set_state_aux_batt2_power",
                                            SetStateAuxiliaryPowerBattery,
                                            latch=True),
                            rospy.Publisher("set_state_electronics_power",
                                            SetStateElectronicPower,
                                            latch=True),
                            rospy.Publisher("set_state_motors_power",
                                            SetStateMotorsPower,
                                            latch=True)]

    def disconnect_from_power(self):
        RELAY_DELAY = 0.25
        (aux1, aux2, elec, moto) = self.undock_pubs
        rospy.sleep(RELAY_DELAY)
        aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
        rospy.sleep(RELAY_DELAY)
        aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
        rospy.sleep(RELAY_DELAY)
        elec.publish(SetStateElectronicPower(status=2))
        rospy.sleep(RELAY_DELAY)
        moto.publish(SetStateMotorsPower(status=2))
        rospy.sleep(RELAY_DELAY)

    def execute(self, userdata):
        self.disconnect_from_power()
        self.teleop_srv(1, 0.2, 0)
        rospy.sleep(4.0)
        self.teleop_srv(0, 0, 0)

        return 'completed'


######### Check batteries voltage State Machine
class BatteriesVoltageState(MonarchState):
    """State that monitors the batteries Voltage."""
    def __init__(self):
        """Constructor."""
        MonarchState.__init__(self,
                              state_outcomes=['batteries_ok', 'batteries_low'])
        rospy.loginfo("Init BatteriesVoltageState")
        topicName = 'batteries_voltage'
        # conditionOutcomes = ['succeeded', 'preempted', 'batteries_ok']
        conditionOutcomes = ['batteries_low', 'batteries_ok']
        topicCondition = TopicCondition(self, topicName,
                                        BatteriesVoltage,
                                        self.batteriesCondition)
        self.add_change_condition(topicCondition, conditionOutcomes)

    def batteriesCondition(self, data, userdata):
        """Check the level of the batteries to see if robot needs to charge."""
        if data.motors <= 1.0:
            return 'batteries_ok'  # It's just that the back button is pressed.
        if data.electronics <= 12.2 or data.motors <= 12.2:
            return 'batteries_low'
        return 'batteries_ok'


# Restart State
class ChargingState(MonarchState):
    """Charging State.

        It has two conditions to exit:
            - RFID Tag received.
            - BatteriesVoltage indicates the robot is fully charged

        :TODO: implement transition when batteries charged
    """
    def __init__(self):
        """Constructor."""
        MonarchState.__init__(self,
                              state_outcomes=['succeeded', 'tag_detected',
                                              'charging', 'not_charging'])
        rospy.loginfo("Init ChargingState")

        topicCondition = TopicCondition(self, 'rfid_tag', RfidReading,
                                        self.rfidCondition)
        self.add_change_condition(topicCondition, ['tag_detected'])

        # bat_condition = TopicCondition(self, 'batteries_voltage',
        #                                BatteriesVoltage, self.charging_cond)
        # self.add_change_condition(bat_condition, ['charging', 'not_charging'])

        has_finished_charging = TopicCondition(self, 'charger_status',
                                               ChargerStatus,
                                               self.has_finished_charging_cb)
        self.add_change_condition(has_finished_charging,
                                  ['charging', 'not_charging'])

    def rfidCondition(self, data, userdata):
        """Notify when an RFID Tag has been detected."""
        value = 'tag_detected'
        return value

    def charging_cond(self, data, userdata):
        """Analize whether the robot is charging or not."""
        if data.charger > 0:
            return 'charging'
        return 'not_charging'   # Charger disconnected or Robot not in charger.

    def has_finished_charging_cb(self, data, userdata):
        """Callback to monitor wether the robot has finished charging."""
        if any(msg_values(data)):
            return 'charging'   # Still charging
        return 'not_charging'   # Has finished charging
