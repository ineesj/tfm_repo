"""
State Machines for UC3M behaviors.

:author: Victor Gonzalez
:date: Feb 2016

"""
# import roslib; roslib.load_manifest('uc3m_behaviors')
# import rospy
import smach
# import smach_ros

from itertools import islice

from uc3m_behaviors import states
import rospy_utils as rpyu


# Helper function
def _sliding_window(seq, n=2):
    """Return a sliding window (of width n) over data from the iterable.

    Example:
    --------
    s -> (s0, s1, ...,s[n-1]), (s1, s2, ..., sn), ...
    """
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result


def make_set_inital_pose_sm():
    """Create a State Machine for Undocking the robot."""
    sm_inital_pose = smach.StateMachine(outcomes=['succeeded'])

    places = next(rpyu.load_params('places'))
    initial_pose = places['1.3.C12_initial_pose']

    with sm_inital_pose:
        sm_inital_pose.add('INITIAL_POSE',
                           states.StateSetInitialPose(initial_pose),
                           transitions={'completed': 'succeeded'})
    return sm_inital_pose


def make_patrolling_sm():
    """Create a Patrolling State Machine."""
    sm_patrolling = smach.StateMachine(outcomes=['preempted', 'aborted'])
    sm_patrolling.userdata.sm_patrolling_data = 0

    with sm_patrolling:
        # Get route waypoints
        places = next(rpyu.load_params('places'))
        route = next(rpyu.load_params('patrolling_route'))
        # waypoints = list(butils.get_route_waypoints(places, route))
        waypoints = [places[wp] for wp in route]

        waypoints.append(waypoints[0])  # After last wp, return to the 1st one.
        for wp, next_wp in _sliding_window(waypoints):
            wp_name = 'MOVE_TO_{}'.format(wp['name'].upper())
            next_wp_name = 'MOVE_TO_{}'.format(next_wp['name'].upper())
            sm_patrolling.add(wp_name, states.MoveToWaypoint(wp),
                              transitions={'succeeded': next_wp_name},
                              remapping={'user_goal': 'sm_patrolling_data'})
    return sm_patrolling


def make_undock_sm():
    """Create a State Machine for Undocking the robot."""
    sm_undock = smach.StateMachine(outcomes=['succeeded',
                                             'preempted', 'aborted'])
    with sm_undock:
        sm_undock.add('UNDOCKING', states.StateUndocking(),
                      transitions={'completed': 'succeeded'})
    return sm_undock


def make_dock_sm(dock_wp='1.3.C12_charger'):
    """Create a State Machine to dock the robot Dock.

        Parameters
        ------
        dock_wp: str
            Wapyoint name of the docking station. Default: 1.3.C12_charger
    """
    sm_dock = smach.StateMachine(outcomes=['succeeded', 'preempted', 'aborted'])
    with sm_dock:
        # sm_dock.add('GOTO_DOCK', SendDockState(),
        places = next(rpyu.load_params('places'))
        charger_waypoint = places[dock_wp]
        sm_dock.add('GOTO_DOCK',
                    states.MoveToWaypoint(charger_waypoint),
                    transitions={'succeeded': 'DOCKING'})
        sm_dock.add('DOCKING',
                    states.StateDocking(),
                    transitions={'completed': 'succeeded'})
    return sm_dock


def make_charging_sm():
    """Create a State Machine for charging the robot."""
    sm_charging = smach.StateMachine(outcomes=['succeeded',
                                               'preempted', 'aborted'])
    with sm_charging:
        sm_charging.add('CHARGING', states.ChargingState(),
                        transitions={'tag_detected': 'preempted',
                                     'not_charging': 'preempted',
                                     'charging': 'CHARGING'})
        sm_charging.set_initial_state(['CHARGING'])
    return sm_charging


def make_batteries_monitor_sm():
    """Create a SM that monitors the batteries level."""
    sm_batteries = smach.StateMachine(outcomes=['low_battery'])
    with sm_batteries:
        sm_batteries.add('BATTERIES', states.BatteriesVoltageState(),
                         transitions={'batteries_ok': 'BATTERIES',
                                      'batteries_low': 'low_battery'})
        # TODO: check if we need the rest of the transitions
    return sm_batteries


def make_patrolling_and_batt_sm(patrolling_sm, batteries_sm):
    """Create a patrolling SM that continuously monitors the batteries level.

    Note: This SM is a Concurrence SM which uses as inputs two other SMs

        Parameters
        ----------
        patrolling_sm: smach.StateMachine
            The patrolling SM instance
        batteries_sm: smach.StateMachine
            The batteries monitoring state machine instance

        Example
        -------
            >>> patrolling = make_patrolling_sm()
            >>> batteries = make_batteries_monitor_sm()
            >>> make_patrol_while_batteries_ok_sm(patrolling, batteries)
    """
    def _termination_cm_cb(outcome_map):
        if any([
#                outcome_map['BATTERIES_SM'] == 'suceeded',
#                outcome_map['BATTERIES_SM'] == 'preempted',
                outcome_map['BATTERIES_SM'] == 'low_battery',
                outcome_map['PATROLLING_SM'] == 'preempted',
                outcome_map['PATROLLING_SM'] == 'succeeded',
                outcome_map['PATROLLING_SM'] == 'aborted']):
            return True
        return False

    def _outcome_cm_cb(outcome_map):
#        if any([outcome_map['BATTERIES_SM'] == 'succeeded',
#                outcome_map['BATTERIES_SM'] == 'preempted',
#                outcome_map['PATROLLING_SM'] == 'preempted']):
#	if outcome_map['PATROLLING_SM'] == 'preepmted':
#            return 'preempted'
        if outcome_map['BATTERIES_SM'] == 'low_battery':
            return 'low_battery'
#        if outcome_map['PATROLLING_SM'] == 'succeeded':
#            return 'succeeded'
#        if outcome_map['PATROLLING_SM'] == 'aborted':
#            return 'aborted'
#        return 'succeeded'

#    cm = smach.Concurrence(outcomes=['succeeded', 'preempted', 'aborted',
    cm = smach.Concurrence(outcomes=['low_battery'],
                           #default_outcome='succeeded',
                           default_outcome='low_battery',
                           child_termination_cb=_termination_cm_cb,
                           outcome_cb=_outcome_cm_cb)
    cm.userdata.cm_data = 0

    with cm:
        cm.add('PATROLLING_SM', patrolling_sm)
        cm.add('BATTERIES_SM', batteries_sm)

    return cm
