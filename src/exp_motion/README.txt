********* EXPRESSIVE MOTION PACKAGE *********
Maria Braga - maria.braga@tecnico.ulisboa.pt

This package is organised as follows:

/config - where the expressive trajectories are stored
/launch - launch files used during the thesis

ExpMo.py - where the class of the controller and a state machine for a single run are implemented
ExpMo_CE.py - the same as ExpMo.py but with interaction board CE's integrated
gui.py - rosnode with a graphical interface to pick the trajectory for the robot to perform
ipol.py - mosmach sm for patrolling and ros topic triggers the expressive motion in ipol (coordinates from IPOL map)
lrm.py - the same as ipol.py but with the LRM map coordinates

To use expressive motion pkg:

1. Check the navigation system is launched
   if not:
   1.1 cd /home/monarch/sandbox/monarch/code/trunk/launch
   1.2 export ROS_NAMESPACE=mbot##
   1.3 roslaunch mbot_navigation.launch
2. roslaunch exp_motion sm.launch or sm_gui.launch for experiments at LRM
   roslaunch exp_motion ipol_* for experiments at IPOL


