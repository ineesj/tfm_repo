#!/usr/bin/env python

import sys
#import math
import random
import os # import os.path
import signal

import roslib; 
import rospy
import rospkg
import smach
import smach_ros
import actionlib

from std_srvs.srv import *
from std_msgs.msg import String
from geometry_msgs.msg import *
from nav_msgs.msg import *
from time import *
import tf
import math as m
import numpy as np
import subprocess
import matplotlib.pyplot as plt
import subprocess

sys.path.append( os.path.join(roslib.packages.get_pkg_dir('scout_msgs'), 'src') )
from scout_msgs import *
from scout_msgs.msg import NavigationByVelRefAction, NavigationByVelRefGoal

ros_namespace = ''
exp_num = 1

class StateALibClient (smach.State):

	def __init__ ( self ):
#		global ros_namespace
		smach.State.__init__ ( self, outcomes = ['succeeded'] )
		self.client = actionlib.SimpleActionClient( '/'+ros_namespace + '/navigation_by_velref', NavigationByVelRefAction)

	def execute ( self, data ):

		goal = NavigationByVelRefGoal(topic_name = '/'+ros_namespace + '/velref_topic')
		rospy.init_node('VelRef')
		self.client.wait_for_server()
		self.client.send_goal(goal)
		rospy.loginfo(" --- ALIB CLIENT FINISHED --- ")
		return 'succeeded'

class StateController (smach.State):

	# sub = rospy.Subscriber("bla", PoseWithCovarianceStamped, StateController.controller4)

	def __init__ ( self, traj, pub, direction ):
		smach.State.__init__ ( self, outcomes = ['succeeded'] )
		
		# controller 2

		self.K = [0.6, 0.6, 1] 
		self.K2 = [1.2, 1.2, 1.2]

		self.theta_th = None

		self.epsilon = 0.05
		self.thsh = 0.01

		##
		self.prev_ind = 0
		##

		self.POS = None # position to save
		self.V = None # vels to save

		self.traj = traj

		self.aux_traj_matrix = np.loadtxt(self.traj, delimiter=',')
		self.traj_matrix = []
		self.aux_var_matrix = np.diff(self.aux_traj_matrix, axis=0)

		#print " --- trajectory loaded --- ", self.traj
		#rospy.loginfo(" -- TRAJ SET -- ")
		
		self.direction = direction
		self.to_finish = False
		self.pub = pub

		self.x_inf = None # coordinates to give orientation to the reference
		self.y_inf = None

		if rospy.has_param('/VelRef/max_exp'):
			self.max_exp = rospy.get_param('/VelRef/max_exp')
		else:
			self.max_exp = 1
		# self.r = rospy.Rate(10) # 10hz

	def find_min_traj(self):
		mini = float('inf')
		ind = 0
		for i in range(0,len(self.traj_matrix)):
			d = self.dist(self.traj_matrix[i, 0]-self.x, self.traj_matrix[i, 1]-self.y)
			if d < mini:
				mini = d
				ind = i

		if mini < self.thsh:
			ind = ind + 1

		if ind >= len(self.var_matrix):
			ind = len(self.var_matrix)-1

		elif ind > self.prev_ind+7:
			ind = self.prev_ind#+3
			
		# elif ind <= self.prev_ind:
		elif ind < self.prev_ind:
			if self.prev_ind+1 >= len(self.var_matrix):
				ind = len(self.var_matrix) - 1
			else:
				ind = self.prev_ind # +1
		elif mini <= self.epsilon:
			ind = self.prev_ind + 1

		# print "self.prev_ind, ind = ", [self.prev_ind, ind]

		l = self.compute_l(self.traj_matrix[ind, 0], self.traj_matrix[ind, 1], self.traj_matrix[ind, 2], mini)

		return [ind, l] # [ind, l, Kex]

	def dist(self, a, b):
		return m.sqrt(a**2+b**2)

	def compute_l(self, xref, yref, thetaref, dist):
		vt = [m.cos(thetaref), -m.sin(thetaref), 0]
		vl = [self.x-xref, self.y-yref, 0]

		a = np.cross(vt, vl)
		l = m.copysign(dist, a[2])

		return l

	def normalize(self, angle):
		return m.atan2(m.sin(angle), m.cos(angle))
		
	def angle_diff(self, a, b):
		a = self.normalize(a)
		b = self.normalize(b)
		d1 = a - b
		d2 = 2 * m.pi - m.fabs(d1)
		if d1 > 0:
		  d2 *= -1.0
		if m.fabs(d1) < m.fabs(d2):
		  return d1
		else:
		  return d2

	def check_finished(self):

		x_stop = m.fabs(self.x-self.traj_matrix[-1][0])
		y_stop = m.fabs(self.y-self.traj_matrix[-1][1])
		#print "STOP = ", [x_stop, y_stop]

		if ((x_stop < 0.2) and (y_stop < 0.2)):
			rospy.loginfo(" -- TO_FINISH = True -- ")
			print "DIRECTION = ", self.direction
			self.to_finish = True
		return

	def controller2(self, msg):
		global vx, vy, w, prev_ind, POS, V
		if self.listen:
			(roll, pitch, self.theta) = tf.transformations.euler_from_quaternion([msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z,msg.pose.pose.orientation.w])
			
			self.x = msg.pose.pose.position.x
			self.y = msg.pose.pose.position.y

			if self.x_inf is None and self.y_inf is None:
				if rospy.has_param('/VelRef/x_inf') and rospy.has_param('/VelRef/y_inf'):
					self.x_inf = rospy.get_param('/VelRef/x_inf')
					self.y_inf = rospy.get_param('/VelRef/y_inf')
				else:
					rospy.logerr('---PARAM TO SET REFERENCE ORIENTATION MISSING---')


			theta_inf = m.atan2(self.y_inf-self.y, self.x_inf-self.x)
			
			if self.traj_matrix == []:
				self.traj_matrix.append([self.x, self.y, theta_inf])

				for i, k in enumerate(self.aux_var_matrix):
					# rotation for the robot orientation                                                                                   
                                        x_line = k[0]*m.cos(theta_inf)-k[1]*m.sin(theta_inf)
                                        y_line = k[0]*m.sin(theta_inf)+k[1]*m.cos(theta_inf)

                                        # tranformation     
					x_m = x_line + self.traj_matrix[-1][0]
					y_m = y_line + self.traj_matrix[-1][1]
					t_m = self.aux_traj_matrix[i+1][2]
					self.traj_matrix.append([x_m, y_m, t_m])
				self.traj_matrix = np.asarray(self.traj_matrix)
				self.var_matrix =  np.diff(self.traj_matrix, axis=0)*5 # atencao a este 5

			if self.theta_th is None:
				self.theta_th = theta_inf #self.theta
			self.check_finished()

			if self.POS == None:
				self.POS = [[self.x, self.y, self.theta]]

#			rospy.loginfo(" --- setting the velocities --- ")
			velref_twist = TwistStamped()
			velref_twist.header.frame_id = "base_link"

			if not self.to_finish:
				
				(ind, l) = self.find_min_traj()
				self.prev_ind = ind

				print "index = ", ind, "/ ", len(self.traj_matrix) 

				x_ref = self.traj_matrix[ind, 0]
				y_ref = self.traj_matrix[ind, 1]
				t_ref = (self.traj_matrix[ind, 2] + self.theta_th) % (2*m.pi)
				#print "+++++++++++++++T_REF: ", t_ref

				dot_x = self.var_matrix[ind, 0] + self.K2[0]*(x_ref - self.x)
				dot_y = self.var_matrix[ind, 1] + self.K2[1]*(y_ref - self.y)
				dot_theta = self.angle_diff(t_ref, self.theta)
				#print "+++++++++++++++DOT_THETA: ", dot_theta

				self.vx = self.K[0] * (m.cos(self.theta)*dot_x + m.sin(self.theta)*dot_y)
				self.vy = self.K[1] * (-m.sin(self.theta)*dot_x + m.cos(self.theta)*dot_y)
				self.w = self.K[2] * dot_theta
#				self.w = m.pi /2

				velref_twist.header.stamp = rospy.Time.now()
				velref_twist.twist.linear.x = self.vx
				velref_twist.twist.linear.y = self.vy
				velref_twist.twist.linear.z = 0
				velref_twist.twist.angular.x = 0
				velref_twist.twist.angular.y = 0
				velref_twist.twist.angular.z = self.w

				self.pub.publish(velref_twist)

			elif self.to_finish:
				self.vx = 0
				self.vy = 0
				self.w = 0

				velref_twist.header.stamp = rospy.Time.now()
				velref_twist.twist.linear.x = 0
				velref_twist.twist.linear.y = 0
				velref_twist.twist.linear.z = 0
				velref_twist.twist.angular.x = 0
				velref_twist.twist.angular.y = 0
				velref_twist.twist.angular.z = 0

				self.pub.publish(velref_twist)
				print velref_twist

			v = m.sqrt(self.vx **2 + self.vy **2)
#			print "v = ", v
#			print "c2: (vx, vy, w) = ", [self.vx, self.vy, self.w]
			if self.V == None:
				self.V = [[self.vx, self.vy, self.w]]
			self.POS = np.append(self.POS, [[self.x, self.y, self.theta]], axis = 0)
			self.V = np.append(self.V, [[self.vx, self.vy, self.w]], axis = 0)

	def save_robot_info(self):
		global exp_num

		rospack = rospkg.RosPack ()
		path = rospack.get_path ( 'exp_motion' )
		path += '/results/'
		#path = "/home/monarch/users/mariab/trunk/catkin_ws/src/exp_motion/results/"

		filename = "plot_" + self.direction + str(exp_num) + ".png"
		fullpath = os.path.join(path, filename)
		
		plt.plot(self.traj_matrix[:,0], self.traj_matrix[:,1], 'b', self.POS[:,0], self.POS[:,1], 'r')
		plt.savefig(fullpath)

		filename = "pos_result_" + self.direction + str(exp_num) + ".txt"
		fullpath = os.path.join(path, filename)
		
		np.savetxt(fullpath, self.POS, delimiter = ',')

		filename = "vel_result_" + self.direction + str(exp_num) + ".txt"
		fullpath = os.path.join(path, filename)
		
		np.savetxt(fullpath, self.V, delimiter = ',')

		rospy.loginfo(" --- FINISHED --- ")

	def reset_values(self):
		
		self.to_finish = False

		self.prev_ind = 0
		#self.prev_ts = None
		#self.I_e = np.array([0, 0, 0])
		#self.I_e_pos = np.array([0, 0, 0])
		#self.e_prev = np.array([0, 0, 0])

		self.POS = None # position to save
		self.V = None # vels to save

		self.x_inf = None # coordinates to give orientation to the reference
		self.y_inf = None
		print "reset to_finish = ", self.to_finish


	def execute(self, data):
		global exp_num
		#if self.prev_ts == None:
		#	now = rospy.Time.now()
		#	self.prev_ts = now.nsecs
		print "DIRECTION_EXEC = ", self.direction
		self.listen = True
		
		rospy.loginfo(" --- INIT CONTROLLER --- ")

		# self.sub = rospy.Subscriber("bla", PoseWithCovarianceStamped, self.controller4)
		topic_2_subs = '/'+ros_namespace + "/amcl_pose"
		print "topic to subsc = ", topic_2_subs
		self.sub = rospy.Subscriber(topic_2_subs, PoseWithCovarianceStamped, self.controller2)

		while not self.to_finish:
			pass
			# self.r.sleep()
		
		if self.to_finish:
			#self.save_robot_info()
			self.reset_values()
			self.listen = False
			if self.direction == 'back':
				exp_num = exp_num + 1
				print "Exp_Num = ", exp_num
			if exp_num > self.max_exp:
				stop_all()
			return 'succeeded'

def signal_handler(signal, frame):
		print('You pressed Ctrl+C!')
		# stop_all()
		sys.exit(1)

def stop_all():
	list_cmd = subprocess.Popen("rosnode list", shell = True, stdout = subprocess.PIPE)
	list_output = list_cmd.stdout.read()
	retcode = list_cmd.wait()
	assert retcode == 0, rospy.logwarn("List command returned %d" % retcode)
	for s in list_output.split("\n"):
		if (s.startswith("/record")):
			os.system("rosnode kill " + s)
		elif (s.startswith("/VelRef")):
			os.system("rosnode kill " + s)


def exp_motion_sm():
	global ros_namespace
	signal.signal(signal.SIGINT, signal_handler)

	rospy.init_node('VelRef')

	rospack = rospkg.RosPack ()
	path = rospack.get_path ( 'exp_motion' )
	path += '/config/'

	print "PATH = ", path

	# path = '/home/mia/Desktop/mbot_exp_motion

	#traj_front = path + 'recta_w.txt'
	# print traj_front
	#traj_back = path + 'recta_w_i.txt'
	
	d_f = 'front'
	d_b = 'back'


	list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
	list_output = list_cmd.stdout.read()

	for string in list_output.split("\n"):
		if string != "":
			ros_namespace = string
			break

	topic_2_pub = '/'+ ros_namespace +'/velref_topic'
	print "topic to sub ", topic_2_pub
	pub = rospy.Publisher(topic_2_pub, TwistStamped, latch = True)

	if rospy.has_param('/VelRef/traj_front'):

		s_traj_front = rospy.get_param('/VelRef/traj_front')
		traj_front = path + s_traj_front
		rospy.loginfo("TRAJ FRONT SET")
	else:
		rospy.logerr('trajectory front file not loaded')

	if rospy.has_param('/VelRef/traj_back'):

		s_traj_back = rospy.get_param('/VelRef/traj_back')
		traj_back = path + s_traj_back
		rospy.loginfo("TRAJ BACK SET")

	else:
		rospy.logerr('trajectory back file not loaded')


	sm = smach.StateMachine(['succeeded', 'aborted'])

	with sm:

		if exp_num < 2:
			sm.add('STATE_ALIB_CLIENT_F', StateALibClient(), transitions = { 'succeeded': 'STATE_TRAJ_F' } )

			sm.add('STATE_TRAJ_F', StateController(traj_front, pub, d_f), transitions = { 'succeeded': 'succeeded' } ) #STATE_ALIB_CLIENT_B

#			sm.add('STATE_ALIB_CLIENT_B', StateALibClient(), transitions = { 'succeeded': 'STATE_TRAJ_B' } )

#			sm.add('STATE_TRAJ_B', StateController(traj_back, pub, d_b), transitions = { 'succeeded': 'STATE_ALIB_CLIENT_F' } ) #STATE_ALIB_CLIENT_F
		else:
		 	return 'succeeded'

#run alone
	# Create and start the introspection server
	sis = smach_ros.IntrospectionServer('server_name', sm, '/SM_ROOT')
	sis.start()
	# Execute SMACH plan
	outcome = sm.execute()

	rospy.spin()


if __name__ == '__main__':

	exp_motion_sm()

