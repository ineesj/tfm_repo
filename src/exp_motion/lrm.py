#!/usr/bin/env python

#import roslib; roslib.load_manifest('mosmach')
import roslib; roslib.load_manifest('exp_motion')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.nav_by_velref_action import NavByVelRefAction
from std_msgs.msg import Empty
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.change_conditions.topic_condition import TopicCondition
from geometry_msgs.msg import TwistStamped
import ExpMo as em


# DEFINICAO DE 2 waypoints

class MoveToActionState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted','mw_l', 'mw_r', 's'])
    rospy.loginfo("NavMoveToActionState")
    moveToAction = MoveToAction(self, 5.10, 13.20, -2.66)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'moonwalk_l', Empty, self.mw_l)
    self.add_change_condition(topicCondition, ['mw_l'])

    topicCondition = TopicCondition(self, 'moonwalk_r', Empty, self.mw_r)
    self.add_change_condition(topicCondition, ['mw_r'])

    topicCondition = TopicCondition(self, 's_mov', Empty, self.s)
    self.add_change_condition(topicCondition, ['s'])

  def mw_l(self, data, userdata):

    rospy.loginfo('MOONWALK TURN LEFT')
    value = 'mw_l'

    return value

  def mw_r(self, data, userdata):

    rospy.loginfo('MOONWALK TURN RIGHT')
    value = 'mw_r'

    return value

  def s(self, data, userdata):

    rospy.loginfo('S MOVEMENT')
    value = 's'

    return value

class MoveToActionState2(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted','mw_l', 'mw_r', 's'])
    rospy.loginfo("NavMoveToActionState")
    
    moveToAction = MoveToAction(self, -3.30, 9.40, 0.46)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'moonwalk_l', Empty, self.mw_l)
    self.add_change_condition(topicCondition, ['mw_l'])

    topicCondition = TopicCondition(self, 'moonwalk_r', Empty, self.mw_r)
    self.add_change_condition(topicCondition, ['mw_r'])

    topicCondition = TopicCondition(self, 's_mov', Empty, self.s)
    self.add_change_condition(topicCondition, ['s'])


  def mw_l(self, data, userdata):

    rospy.loginfo('MOONWALK TURN LEFT')
    value = 'mw_l'

    return value

  def mw_r(self, data, userdata):

    rospy.loginfo('MOONWALK TURN RIGHT')
    value = 'mw_r'

    return value

  def s(self, data, userdata):

    rospy.loginfo('S MOVEMENT')
    value = 's'

    return value


   
######### Main function
def main():
  rospy.init_node("move_to")

  # State Machine Patrolling
  sm = smach.StateMachine(outcomes = ['succeeded','preempted', 'aborted'])

  with sm:

    sm.add('MoveTo_1', MoveToActionState(), transitions={'succeeded':'MoveTo_2','mw_l':'Moonwalk_l1', 'mw_r':'Moonwalk_r1', 's':'S_mov_1'})
    sm.add('MoveTo_2', MoveToActionState2(), transitions={'succeeded':'MoveTo_1','mw_l':'Moonwalk_l2', 'mw_r':'Moonwalk_r2', 's':'S_mov_2'})

    sm.add('Moonwalk_l1', em.exp_motion_sm('moonwalk_l.txt', 5.10, 13.20), transitions={'succeeded':'MoveTo_1','aborted':'aborted'})
    sm.add('Moonwalk_r1', em.exp_motion_sm('moonwalk_r.txt', 5.10, 13.20), transitions={'succeeded':'MoveTo_1','aborted':'aborted'})
    sm.add('S_mov_1', em.exp_motion_sm('st_plus.txt', 5.10, 13.20), transitions={'succeeded':'MoveTo_1','aborted':'aborted'})

    sm.add('Moonwalk_l2', em.exp_motion_sm('moonwalk_l.txt', -3.30, 9.40), transitions={'succeeded':'MoveTo_2','aborted':'aborted'})
    sm.add('Moonwalk_r2', em.exp_motion_sm('moonwalk_r.txt', -3.30, 9.40), transitions={'succeeded':'MoveTo_2','aborted':'aborted'})
    sm.add('S_mov_2', em.exp_motion_sm('st_plus.txt', -3.30, 9.40), transitions={'succeeded':'MoveTo_2','aborted':'aborted'})

  sis = smach_ros.IntrospectionServer('move_to', sm, '/SM_ROOT')
  sis.start()
  
  outcome = sm.execute()


if __name__ == '__main__':
  main()
