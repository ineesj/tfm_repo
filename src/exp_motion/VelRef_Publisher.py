#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from geometry_msgs.msg import *
from nav_msgs.msg import *
from time import *
import tf
import math as m
import numpy as np
import subprocess


ros_namespace = ''

# inicializacao das variaveis de velocidades
vx = 0
vy = 0
w = 0

# ganhos do controlador
Kx = 2
K1y = 2
K2y = 2
Kw = 0

traj_matrix = None
vel_matrix = None

to_finish = False #flag to determine whether the trajectory is finished or not
act_p = 1 # variable to save the actual position to be used in the next iteration

POS = None # position to save
V = None # vel to save

pubPoint = rospy.Publisher('idx_point', PointStamped)


def load_traj(file_name):
	global traj_matrix
	
	pub = rospy.Publisher("topic_path", Path)
	traj_matrix = np.loadtxt(file_name, delimiter=',')
	rospy.loginfo("trajectory loaded")
	path = Path()
	cnt = 0
	for l in traj_matrix:
		pose_stamped = PoseStamped()
		pose_stamped.header.seq = cnt
		pose_stamped.header.frame_id = "map"
		pose_stamped.header.stamp = rospy.Time.now()	
		pose_stamped.pose.position.x = l[0]
		pose_stamped.pose.position.y = l[1]
		
		path.poses.append(pose_stamped)
		cnt +=1

	path.header.frame_id = "map"
	path.header.stamp = rospy.Time.now()
	path.header.seq = 0
	sleep(1)
	pub.publish(path)

def traj_vel(traj):
	global vel_matrix

	vel_matrix = np.diff(traj_matrix, axis=0)


def find_min_traj(x, y):
	mini = float('inf')
	ind = 0
	for i in range(0,len(traj_matrix)):
		d = dist(traj_matrix[i, 0]-x, traj_matrix[i, 1]-y)
		if d < mini:
			mini = d
			ind = i
	
	print "act_p, ind = ", [act_p, ind]
	if ind >= len(vel_matrix):
		ind = len(vel_matrix)-1

	elif ind > act_p+7:
		ind = act_p+3
		
	elif ind < act_p:
		ind = act_p+1

	l = compute_l(x, y, traj_matrix[ind, 0], traj_matrix[ind, 1], traj_matrix[ind, 2], mini)
	Kex = compute_kex(x, traj_matrix[ind, 0], traj_matrix[ind+1, 0])
	return [ind, l, Kex]

def dist(a, b):
	return m.sqrt(a**2+b**2)

def compute_l(x, y, xref, yref, thetaref, dist):
	vt = [m.cos(thetaref), -m.sin(thetaref), 0]
	vl = [x-xref, y-yref, 0]

	a = np.cross(vt, vl)
	l = m.copysign(dist, a[2])

	return l

def compute_kex(x, xref, nextx):
	dx1 = x - xref
	dx2 = x - nextx

	dx = m.fabs(dx1)

	d_diff = dx2 - dx1

	if d_diff < 0: # robot em avanco em relacao a traj
		if dx > 0.2:
				Kex = 1/dx
	else: # robot em atraso em relacao a traj
		Kex = 1

	return Kex

	
def handle_pose(msg):
	global vx, vy, w, act_p, POS, V, to_finish

	# pubPoint = rospy.Publisher('idx_point', PointStamped)	
	(roll, pitch, yaw) = tf.transformations.euler_from_quaternion([msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z,msg.pose.pose.orientation.w])
	
	x = msg.pose.pose.position.x
	y = msg.pose.pose.position.y

	x_stop = m.fabs(x-traj_matrix[len(traj_matrix)-1,0])
	y_stop = m.fabs(y-traj_matrix[len(traj_matrix)-1,1])

	if ((x_stop < 1) and (y_stop < 1)):
		vx = 0
		vy = 0
		w = 0
		to_finish = True
	
#	print "(x, y) = ", [x,y]
	
	if POS == None:
		POS = [[x, y, yaw]]

	d_array = find_min_traj(x, y)
	
	idx = d_array[0]
	l = d_array[1]
	Kex = d_array[2]

	point = PointStamped()
	point.header.frame_id = 'map'
	point.header.stamp = rospy.Time.now()
	point.point.x = traj_matrix[idx, 0]
	point.point.y = traj_matrix[idx, 1]
	
	#print point
	pubPoint.publish(point)
	# idx = idx + 10
	if not to_finish:
		
		print "idx = ", idx

		act_p = idx

		dot_x = vel_matrix[idx, 0]
		dot_y = vel_matrix[idx, 1]
		dot_theta = vel_matrix[idx, 2]
		yaw = 0

		vx_ref = (m.cos(yaw)*dot_x + m.sin(yaw)*dot_y)
		vy_ref = (-m.sin(yaw)*dot_x + m.cos(yaw)*dot_y)
		theta_til = traj_matrix[idx, 2] - yaw

		vx = Kex*vx_ref
		vy = -K1y*l*vy_ref - K2y*m.fabs(vy_ref)*theta_til
		w = Kw*dot_theta

		# print "dot_ref = ", [dot_x, dot_y, yaw]
		# vx = Kx*(m.cos(yaw)*dot_x + m.sin(yaw)*dot_y)
		# vy = Ky*(-m.sin(yaw)*dot_x + m.cos(yaw)*dot_y)
		# w = Kw*dot_theta
			
    # x_ref = traj_matrix[idx, 0]
	# y_ref = traj_matrix[idx, 1]
	# theta_ref = traj_matrix[idx, 2]
	
	# vx = k[0]*(m.cos(yaw)*(x_ref - x) - m.sin(yaw)*(y_ref - y))
	# vy = k[1]*(m.sin(yaw)*(x_ref - x) + m.cos(yaw)*(y_ref - y))
	# w = k[2]*(angle_diff(theta_ref, yaw))
	
	print "(vx, vy, w) = ", [vx, vy, w ]
	if V == None:
		V = [[vx, vy, w]]
	POS = np.append(POS, [[x, y, yaw]], axis = 0)
	V = np.append(V, [[vx, vy, w]], axis = 0)
	
def normalize(angle):
	return m.atan2(m.sin(angle), m.cos(angle))
	
def angle_diff(a, b):
    a = normalize(a)
    b = normalize(b)
    d1 = a - b
    d2 = 2 * m.pi - m.fabs(d1)
    if d1 > 0:
      d2 *= -1.0
    if m.fabs(d1) < m.fabs(d2):
      return d1
    else:
      return d2

def controller():
	global pubPoint, Kx, Ky, Kw, ros_namespace

	rospy.loginfo("Controller init!!")

	list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
	list_output = list_cmd.stdout.read()

	ros_namespace = ''
	for string in list_output.split("\n"):
	    if string != "":
			ros_namespace = string
			break

	print  "NAMESPACE = ", ros_namespace

	if rospy.has_param('traj'):
		rospy.init_node('controller', anonymous=True)
		rospy.loginfo("trajectory parameter set")
		traj_file = rospy.get_param('traj')
		#print "traj param = ", traj_file
		load_traj(traj_file)
		traj_vel(traj_matrix)
	
		pub = rospy.Publisher('/'+ros_namespace+'/velref_topic', TwistStamped)

#		print "topic, ", ros_namespace+'/velref_topic'
		r = rospy.Rate(10) # 10hz
		
		if rospy.has_param('kx'):
			Kx = rospy.get_param('kx')
		if rospy.has_param('ky'):
			Ky = rospy.get_param('ky')
		if rospy.has_param('kw'):
			Kw = rospy.get_param('kw')		


		rospy.loginfo("setting the velocities")
		velref_twist = TwistStamped()
		velref_twist.header.frame_id = "base_link"
    
		rospy.Subscriber('/'+ros_namespace + "/amcl_pose", PoseWithCovarianceStamped, handle_pose)
		#rospy.Subscriber("bla", PoseWithCovarianceStamped, handle_pose)
	
		rospy.on_shutdown(save_robot_info)

		while not rospy.is_shutdown():
			
			velref_twist.header.stamp = rospy.Time.now()
			velref_twist.twist.linear.x = vx
			velref_twist.twist.linear.y = vy
			velref_twist.twist.linear.z = 0
			velref_twist.twist.angular.x = 0
			velref_twist.twist.angular.y = 0
			velref_twist.twist.angular.z = w

			pub.publish(velref_twist)
			# rospy.loginfo(" --- Velocity published --- ")
			r.sleep()
			if to_finish == True:
				velref_twist.header.stamp = rospy.Time.now()
				velref_twist.twist.linear.x = 0
				velref_twist.twist.linear.y = 0
				velref_twist.twist.linear.z = 0
				velref_twist.twist.angular.x = 0
				velref_twist.twist.angular.y = 0
				velref_twist.twist.angular.z = 0
				pub.publish(velref_twist)
				print velref_twist
				#sleep(0.5)
				rospy.signal_shutdown('end')
	else:
		rospy.logerr('trajectory file not loaded')

def save_robot_info():
	np.savetxt('pos_result.txt', POS, delimiter = ',')
	np.savetxt('vel_result.txt', V, delimiter = ',')

if __name__ == '__main__':
	try:
		controller()
	except rospy.ROSInterruptException: 
		rospy.loginfo("OUT !!")
		pass
