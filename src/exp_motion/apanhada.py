#!/usr/bin/env python

#import roslib; roslib.load_manifest('mosmach')
import roslib; roslib.load_manifest('exp_motion')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.nav_by_velref_action import NavByVelRefAction
from std_msgs.msg import Empty
from mosmach.actions.topic_reader_action import TopicReaderAction
from mosmach.change_conditions.topic_condition import TopicCondition
from geometry_msgs.msg import TwistStamped
import ExpMo as em


# DEFINICAO DE 2 waypoints

class MoveToActionState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted'])
    rospy.loginfo("NavMoveToActionState")
    moveToAction = MoveToAction(self, 15.5, 2.3, 2.03)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'Publish_here_empty', Empty, self.cb)
    self.add_change_condition(topicCondition, ['aborted'])

  def cb(self, data, userdata):
    print "GOING TO CHANGE!!!"

    value = 'aborted'
    return value


class MoveToActionState2(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted'])
    rospy.loginfo("NavMoveToActionState")
    
    moveToAction = MoveToAction(self, 8.30, 14.40, -2.62)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'Publish_here_empty', Empty, self.cb)
    self.add_change_condition(topicCondition, ['aborted'])

  def cb(self, data, userdata):
    print "GOING TO CHANGE!!!"

    value = 'aborted'
    return value

class MoveToActionState3(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted'])
    rospy.loginfo("NavMoveToActionState")
    
#    moveToAction = MoveToAction(self,-1.80, 10.00, 0.54)
    moveToAction = MoveToAction(self,-0.70, 10.55, 0.50)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'Publish_here_empty', Empty, self.cb)
    self.add_change_condition(topicCondition, ['aborted'])

  def cb(self, data, userdata):
    print "GOING TO CHANGE!!!"

    value = 'aborted'
    return value

class MoveToActionState4(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded','aborted'])
    rospy.loginfo("NavMoveToActionState")
    
    moveToAction = MoveToAction(self, 8.65, 14.20, -0.97)
    self.add_action(moveToAction)

    topicCondition = TopicCondition(self, 'Publish_here_empty', Empty, self.cb)
    self.add_change_condition(topicCondition, ['aborted'])

  def cb(self, data, userdata):
    print "GOING TO CHANGE!!!"

    value = 'aborted'
    return value

   
######### Main function
def main():
  rospy.init_node("move_to")

  # State Machine Patrolling
  sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])

  with sm:
#    sm.add('Start', em.exp_motion_sm('linha_x.txt',15.50, 2.30), transitions={'succeeded':'MoveTo_1','aborted':'aborted'})
    sm.add('MoveTo_1', MoveToActionState(), transitions={'succeeded':'MoveTo_2','aborted':'Exception1'})
    sm.add('MoveTo_2', MoveToActionState2(), transitions={'succeeded':'MoveTo_3','aborted':'Exception2'})
    sm.add('MoveTo_3', MoveToActionState3(), transitions={'succeeded':'MoveTo_4','aborted':'Exception3'})
    sm.add('MoveTo_4', MoveToActionState4(), transitions={'succeeded':'MoveTo_1','aborted':'Exception4'})
    sm.add('Exception1', em.exp_motion_sm('mini_cb.txt', 15.50, 2.30), transitions={'succeeded':'MoveTo_1','aborted':'aborted'})
    sm.add('Exception2', em.exp_motion_sm('mini_cb.txt', 8.65, 14.20), transitions={'succeeded':'MoveTo_2','aborted':'aborted'})
    sm.add('Exception3', em.exp_motion_sm('mini_cb.txt', -1.80, 10.00), transitions={'succeeded':'MoveTo_3','aborted':'aborted'})
    sm.add('Exception4', em.exp_motion_sm('mini_cb.txt', 8.65, 14.20), transitions={'succeeded':'MoveTo_4','aborted':'aborted'})

  sis = smach_ros.IntrospectionServer('move_to', sm, '/SM_ROOT')
  sis.start()
  
  outcome = sm.execute()


if __name__ == '__main__':
  main()
