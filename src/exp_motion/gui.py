#!/usr/bin/python
 
import wx
import rospy
from std_msgs.msg import String
from std_msgs.msg import Empty


class Buttons(wx.Dialog):
    def __init__(self, parent, id, title):
        wx.Dialog.__init__(self, parent, id, title, size=(230,150))

        #panel = wx.Panel(frame, wx.ID_ANY)

        self.close = wx.Button(self, wx.ID_ANY, 'Close', (10, 100))
        self.close.Bind(wx.EVT_BUTTON, self.onClose)

        self.buttonA = wx.Button(self, wx.ID_ANY, 'Moonwalk L', (10, 10))
        self.buttonA.Bind(wx.EVT_BUTTON, self.onButtonA)

        self.buttonB = wx.Button(self, wx.ID_ANY, 'Moonwalk R', (120, 10))
        self.buttonB.Bind(wx.EVT_BUTTON, self.onButtonB)
 
        self.buttonC = wx.Button(self, wx.ID_ANY, 'S mov R', (10, 50))
        self.buttonC.Bind(wx.EVT_BUTTON, self.onButtonC)

        self.buttonD = wx.Button(self, wx.ID_ANY, 'Disabled', (120, 50))
        self.buttonD.Bind(wx.EVT_BUTTON, self.onButtonD)
        
        self.Centre()
        self.ShowModal()
        self.Destroy()

    def onClose(self, event):
        self.Close(True)
 
    def onButtonA(self, event):
        pub_mw_r.publish()
        print "Moonwalk Left"
 
    def onButtonB(self, event):
        pub_mw_l.publish()
        print "Moonwalk Right"

    def onButtonC(self, event):
        pub_s.publish()
        print "Button C pressed."

    def onButtonD(self, event):
        print "Button D pressed."


if __name__ == '__main__':
    global pub_mw_r, pub_mw_l, pub_s

    pub_mw_r = rospy.Publisher('moonwalk_r', Empty)
    pub_mw_l = rospy.Publisher('moonwalk_l', Empty)
    pub_s = rospy.Publisher('s_mov', Empty)

    rospy.init_node('exp_mo_gui')

    app = wx.App(0)
    Buttons(None, -1, 'Expressive Motion')
    app.MainLoop()

#frame = wx.Frame(None, -1, 'Expressive Motion')
#frame.SetDimensions(0,0,200,100)

#frame.Show()
#frame.Centre()
