#! /usr/bin/env python

import sys
import os.path
import roslib;
import rospy
import actionlib
#import subprocess

sys.path.append( os.path.join(roslib.packages.get_pkg_dir('scout_msgs'), 'src') )
from scout_msgs import *
from scout_msgs.msg import NavigationByVelRefAction, NavigationByVelRefGoal

if __name__ == '__main__':

#	list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
#	list_output = list_cmd.stdout.read()

#	ros_namespace = ''
#	for string in list_output.split("\n"):
#	    if string != "":
#			ros_namespace = string
#			break

	rospy.init_node('VelRef')
	client = actionlib.SimpleActionClient('navigation_by_velref', NavigationByVelRefAction)
	client.wait_for_server()

	goal = NavigationByVelRefGoal(topic_name = "velref_topic")
    # Fill in the goal here
	client.send_goal(goal)
	client.wait_for_result(rospy.Duration.from_sec(5.0))
