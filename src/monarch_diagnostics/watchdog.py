import os
import xmlrpclib
import rospy
import roslib, roslib.message
import rosnode
import rosgraph
import std_msgs.msg
import yaml
import re

import importlib
import sys

from diagnostic_msgs.msg import DiagnosticArray,DiagnosticStatus,KeyValue

class Watchdog():

  def __init__(self, nodes_file, topics_file):

    rospy.init_node('watchdog')

    self.pub = rospy.Publisher('diagnostics', DiagnosticArray, queue_size=10)

    stream = open(nodes_file, 'r')
    all_nodes = yaml.load(stream)

    stream = open(topics_file, 'r')
    all_topics = yaml.load(stream)

    self.namespace = rospy.get_namespace()
    self.nodes = None

    # search for current namespace in nodes
    for entry in all_nodes:
      ns_p = re.compile(entry['namespace'])
      if ns_p.match(self.namespace):
        self.nodes = entry
        break

    if self.nodes is None:
      rospy.logerr('Could not find match for namespace \'%s\'', self.namespace)
      exit(1)

    # search for current namespace in topics
    for entry in all_topics:
      ns_p = re.compile(entry)
      if ns_p.match(self.namespace):
        topics = all_topics[entry]
        break

    self.periodic_topic_checkers = []
    self.triggered_topic_checkers = []
    # setup topic checking
    for topic in topics:
      if topic['timing'] == 'periodic':
        ptc = PeriodicTopicChecker(self.namespace, topic)
        self.periodic_topic_checkers.append(ptc)
      elif topic['timing'] == 'triggered':
        ttc = TriggeredTopicChecker(self.namespace, topic)
        self.triggered_topic_checkers.append(ttc)
      else:
        rospy.logerr('Unknown topic checking type:\' %s\'', topic['trigger'])



    # periodic checking
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
      self.nodeAlivenessCheck()
      self.periodicTopicsCheck()
      self.triggeredTopicsCheck()
      r.sleep()

  def nodeAlivenessCheck(self):
    required_nodes = self.nodes['required_nodes']
    optional_nodes = self.nodes['optional_nodes']

    da = DiagnosticArray()
    da.header.stamp = rospy.Time.now()

    #lets ping all nodes
    pinged, unpinged = rosnode.rosnode_ping_all()

    # check if all expected nodes belong to the pinged list
    for node in required_nodes:
      full_node_name = self.namespace + node
      if full_node_name not in pinged:
        # issue error
        rospy.logerr("Did not find required node: %s", full_node_name)
        ds = DiagnosticStatus()
        ds.level = DiagnosticStatus.ERROR
        ds.name = self.namespace + 'watchdog/nodes' + full_node_name
        ds.message = 'Required node not available'
        ds.hardware_id = self.namespace
        da.status.append(ds)
        kv = KeyValue()
        kv.key = 'name'
        kv.value = full_node_name
        ds.values.append(kv)
        da.status.append(ds)

    for node in optional_nodes:
      full_node_name = self.namespace + node
      if full_node_name not in pinged:
        # issue error
        rospy.logerr("Did not find optional node: %s", full_node_name)
        ds = DiagnosticStatus()
        ds.level = DiagnosticStatus.WARN
        ds.name = self.namespace + 'watchdog/nodes' + full_node_name
        ds.message = 'Optional node not available'
        ds.hardware_id = self.namespace
        kv = KeyValue()
        kv.key = 'name'
        kv.value = full_node_name
        ds.values.append(kv)
        da.status.append(ds)

    for node in pinged:
      # issue OK if we know the node
      #if (node in required_nodes) or (node in optional_nodes):
        ds = DiagnosticStatus()
        ds.level = DiagnosticStatus.OK
        ds.name = self.namespace + 'watchdog/nodes' + node
        ds.message = 'OK'
        ds.hardware_id = self.namespace
        kv = KeyValue()
        kv.key = 'name'
        kv.value = node
        ds.values.append(kv)
        da.status.append(ds)

    # issue warning for unpinged nodes
    for node in unpinged:
      # issue warning
      rospy.logwarn("Stale node registration: %s", node)
      ds = DiagnosticStatus()
      ds.level = DiagnosticStatus.WARN
      ds.name = self.namespace + 'watchdog/nodes' + node
      ds.message = 'Stale node registration'
      ds.hardware_id = self.namespace
      da.status.append(ds)
      kv = KeyValue()
      kv.key = 'name'
      kv.value = node
      ds.values.append(kv)
      da.status.append(ds)

    # publish all we got
    self.pub.publish(da)

  def periodicTopicsCheck(self):
    da = DiagnosticArray()
    da.header.stamp = rospy.Time.now()

    for ptc in self.periodic_topic_checkers:
      ds = ptc.VerifyArrivals()
      da.status.append(ds)

    # publish all we got
    self.pub.publish(da)

  def triggeredTopicsCheck(self):
    da = DiagnosticArray()
    da.header.stamp = rospy.Time.now()

    for ttc in self.triggered_topic_checkers:
      ds = ttc.VerifyArrivals()
      da.status.append(ds)

    # publish all we got
    self.pub.publish(da)



class TopicChecker():

  def __init__(self, namespace, receiver_config):
    self.namespace = namespace
    self.config = receiver_config
    self.dataype = None
    self.dataArrivals = []

    # import data type package
    datatype = self.LoadPackageFromType(self.config['type'])

    # subscribe to monitored topic
    self.sub = rospy.Subscriber(self.config['topic'], datatype, DataArrivalCallback, self)

  def LoadPackageFromType(self, t):
    type_part = t.partition('/')
    if(type_part[1] == '/'):
      module_name = type_part[0] + ".msg"
      type_name = type_part[2]
      importlib.import_module(module_name)
      module = __import__(module_name, globals(), locals(), [type_name], -1)
      return eval('module.' + type_name)
    else:
      print 'unexpected type format'
      print t
      exit(1)



class PeriodicTopicChecker(TopicChecker):

  def __init__(self, namespace, receiver_config):
    TopicChecker.__init__(self, namespace, receiver_config)

  def NotifyDataArrival(self, data):
    # register data arrival
    t = rospy.get_rostime()
    self.dataArrivals.append(t)

  def VerifyArrivals(self):
    numArrivals = len(self.dataArrivals)

    ds = DiagnosticStatus()
    ds.name = self.namespace + 'watchdog/topics/' + self.config['topic'] + '/frequency'
    ds.hardware_id = 'watchdog'

    kv = KeyValue()
    kv.key = 'name'
    kv.value = self.config['topic'] 
    ds.values.append(kv)

    kv = KeyValue()
    kv.key = 'frequency'
    kv.value = str(numArrivals)
    ds.values.append(kv)
    
    kv = KeyValue()
    kv.key = 'min_frequency'
    kv.value = str(self.config['min_frequency'])
    ds.values.append(kv)

    kv = KeyValue()
    kv.key = 'max_frequency'
    kv.value = str(self.config['max_frequency'])
    ds.values.append(kv)

    if numArrivals < self.config['min_frequency'] or numArrivals > self.config['max_frequency']:
      ds.level = DiagnosticStatus.ERROR
      ds.message = 'Unexpected frequency'
    else:
      ds.level = DiagnosticStatus.OK
      ds.message = 'OK'

    self.dataArrivals = []
    return ds


class TriggeredTopicChecker(TopicChecker):

  def __init__(self, namespace, receiver_config):
    TopicChecker.__init__(self, namespace, receiver_config)

    self.trigger_list = []
    self.data_list = []

    #setup reception of triggering topics
    datatype = self.LoadPackageFromType(self.config['trigger_type'])
    self.trigger_sub = rospy.Subscriber(self.config['trigger_topic'], datatype, TriggerArrivalCallback, self)

  def NotifyDataArrival(self, data):
    t = rospy.get_rostime()
    self.data_list.append(t)

  def NotifyTriggerArrival(self, trigger):
    t = rospy.get_rostime()
    self.trigger_list.append(t)

  def VerifyArrivals(self):
    ds = DiagnosticStatus()
    ds.name = self.namespace + 'watchdog/topics/' + self.config['topic'] + '/latency'
    ds.hardware_id = 'watchdog'

    kv = KeyValue()
    kv.key = 'name'
    kv.value = self.config['topic'] 
    ds.values.append(kv)

    kv = KeyValue()
    kv.key = 'trigger'
    kv.value = self.config['trigger_topic'] 
    ds.values.append(kv)

    kv = KeyValue()
    kv.key = 'max_latency'
    kv.value = str(self.config['max_latency'])
    ds.values.append(kv)
    t = rospy.get_rostime()

    ok = True

    #go through all triggers
    for trigger in self.trigger_list:
      #has it expired?
      diff = (t - trigger).to_sec()
      max_lat = float(self.config['max_latency']) / 1000.0
      if diff > max_lat:

        #remove data arrivals that are older than trigger
        while (len(self.data_list) > 0 ) and (self.data_list[0] < trigger):
          self.data_list.pop(0)

        #if data is less than max_timeout away from trigger
        if (len(self.data_list) > 0) and ((self.data_list[0] - trigger).to_sec() < max_lat):
          #all is ok, remove data
          self.data_list.pop(0)
        else:
          #something wrong, we have a latency error
          ok = False

    if ok:
      ds.level = DiagnosticStatus.OK
      ds.message = 'OK Latency'
    else:
      ds.level = DiagnosticStatus.ERROR
      ds.message = 'Latency error'
  
    #clear triggers that are too old
    while (len(self.trigger_list) > 0) and ((t - self.trigger_list[0]).to_sec() > max_lat):
      self.trigger_list.pop(0)


    return ds


def DataArrivalCallback(data, o):
  o.NotifyDataArrival(data)

def TriggerArrivalCallback(trigger, o):
  o.NotifyTriggerArrival(trigger)
