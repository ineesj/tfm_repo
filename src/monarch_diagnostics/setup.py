#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
   ##  don't do this unless you want a globally visible script
   # scripts=['bin/myscript'], 
   packages=['monarch_diagnostics'],
   package_dir={'': 'src'},
   requires=['monarch_msgs','std_msgs','rospy']
)

setup(**d)
