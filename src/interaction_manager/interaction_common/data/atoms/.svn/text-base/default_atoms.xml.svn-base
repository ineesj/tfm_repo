<!--

     In this file are all PREDEFINED user slots that could be received by the dialog manager
     from multimodal_fusion_dialog (translators and aggregators).Instead the globals slots are
     unified by iwaki.

     *Note 1: each atom is sended by: one translator (fusion), one aggregator (aggregator), or iwaki (if type globals)

     *Note 2: on user atoms could appears more slot that these predefined slots. For example on
     each semnatic grammars used by grammar-based recognizer usually are defined slots that they are
     used on the dialog but are they are not commun for whatever dialog. in that sense,
     this file is util for defined the possible type of atoms and the predefined slots sended
     by multimodal fussion, but it is possible use on dialogs other slots received.

     *Note 3: is a good practice use this file for everything dialogs, to improve the powerfull
     of the dialog for everything possible dialogs.

     *Note 4: the atoms are in order to the level of abstraction/complexity of the information supplied.

-->

<?xml version="1.0" encoding="US-ASCII"?>

<!-- Slots definde by the inputs modes on Multimodal Fusion Dialog -->
<default_atoms>


  <!-- ############################################################## -->
  <!-- ####### LOW LEVEL OF ABSTRACTON INPUT COMMUNICATIVE ACTS ##### -->
  <!-- ############################################################## -->


  <!--  Slots definded by Touch -->
  <atom>
    <slot name="type" val="touch"/>
    <slot name="subtype" val="user"/>
    <!-- Predicate that tells if touch snesor has been touched  -->
    <slot name="is_touched" value="0" type="number" />
    <slot name="body_part_touched" value="_NO_VALUE_" type="string"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>

<atom>
    <slot name="type" val="rfid"/>
    <slot name="subtype" val="user"/>
    <!-- ID of the RFID Tag  -->
    <slot name="tag_id" value="_NO_VALUE_" type="number" />
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>



  <!-- ############################################################## -->
  <!-- #### MEDIUM LEVEL OF ABSTRACTON INPUT COMMUNICATIVE ACTS ##### -->
  <!-- ############################################################## -->

  <!--  Slots definded by MOnarCH ASR with results OK-->
  <atom>
    <slot name="type" val="asr_OK"/>
    <slot name="subtype" val="user"/>
    <slot name="words" value="_NO_VALUE_" type="string"/>
    <slot name="confidence" value="_NO_VALUE_" type="number"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" value="_NO_VALUE_" type="string"/>
  </atom>


  <!--  Slots definded by Google ASR with results OK -->
  <atom>
    <slot name="type" val="asr_googleOK"/>
    <slot name="subtype" val="user"/>
    <slot name="content" value="_NO_VALUE_" type="string"/>
    <slot name="g_confidence" value="_NO_VALUE_" type="number"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" value="_NO_VALUE_" type="string"/>
  </atom>


  <!--  MOnarCH ASR Atom-->
  <atom>
    <slot name="type" val="asr"/>
    <slot name="subtype" val="user"/>
    <slot name="recognized" value="_NO_VALUE_" type="string"/>
    <slot name="confidence" value="_NO_VALUE_" type="number"/>
    <slot name="semantic" value="_NO_VALUE_" type="string"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" value="_NO_VALUE_" type="string"/>
  </atom>


  <!-- MOnarCH gesture execution status atom -->
  <atom>
    <slot name="type" val="gesture_status"/>
    <slot name="subtype" val="user"/>
    <slot name="expression_name" val="_NO_VALUE_" type="string"/>
    <slot name="status" val="_NO_VALUE_" type="string"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>

  <!-- MOnarCH User info atom -->
  <atom>
    <slot name="type" val="user_info"/>
    <slot name="subtype" val="user"/>
    <!-- uid is the User ID -->
<!--
    <slot name="uid" value='_NO_VALUE_' type="number"/>
    <slot name="name" value='_NO_VALUE_' type="string"/>
    <slot name="surname" value='_NO_VALUE_' type="string"/>
-->
    <!-- Personalized message to say when the robot sees the user -->
<!--
    <slot name="welcome_msg" value='_NO_VALUE_' type="string"/>
-->
    <!-- Personalized non verbal sound the robot plays when sees the user -->
<!--
    <slot name="non_verbal_sound" value='_NO_VALUE_' type="string"/>
-->
    <!-- Personalized greetings expression/gesture name -->
<!--
    <slot name="greetings_expression" value='_NO_VALUE_' type="string"/>
-->
    <!-- Role of that person -->
    <slot name="role" value='_NO_VALUE_' type="string"/>
<!--
    <slot name="gender" value='_NO_VALUE_' type="string"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
-->
    <slot name="consumed" val="false" type="string"/>
  </atom>  


  <!-- ############################################################## -->
  <!-- #######HIGH LEVEL OF ABSTRACTON INPUT COMMUNICATIVE ACTS ##### -->
  <!-- ############################################################## -->

  <!-- One of the users leaves the scene -->
  <atom>
    <slot name="type" val="user_leaving"/>
    <slot name="subtype" val="user"/>
    <slot name="user_id" val="0" type="number" />
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="currentTime" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>


  <!-- Person Detected -->
  <atom>
    <!-- <slot name="type" val="user_come_in"/> -->
    <slot name="type" val="person_detected"/>
    <slot name="subtype" val="user"/>
    <slot name="person_detectedº" val="_NO_VALUE_" type="string" />
    <slot name="person_id" val="_NO_VALUE_" type="string" />
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>


  <!-- One user disengaged -->
  <atom>
    <slot name="type" val="user_disengaged"/>
    <slot name="subtype" val="user"/>
    <slot name="user_id" val="0" type="number" />
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>


  <!-- ############################################################## -->
  <!-- ####### MOnarCH Command Received                         ##### -->
  <!-- ############################################################## -->
  <!--
  <atom>
    <slot name="type"    val="command_received"/>
    <slot name="subtype" val="user"/>
    <slot name="command" val="_NO_VALUE_" type="string" />
    <slot name="params"  val="_NO_VALUE_" type="string" />
  </atom>
  -->
  <atom>
    <slot name="type"      val="command_received"/>
    <slot name="subtype"   val="user"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed"  val="false" type="string"/>
    <slot name="command"   val="_NO_VALUE_" type="string" />
    <!-- Other slots that might come together with the activation -->
    <slot name="name_of_game"          val="_NO_VALUE_" type="string"/>
    <slot name="question_to_user"      val="_NO_VALUE_" type="string"/>
    <slot name="tell_user"             val="_NO_VALUE_" type="string"/>
    <slot name="child_name"            val="_NO_VALUE_" type="string"/>
    <slot name="congratulation_motive" val="_NO_VALUE_" type="string"/>
    <slot name="encouragement_motive"  val="_NO_VALUE_" type="string"/>
  </atom>

  <!-- Yes/No answer from user using touch_interface
       Slots  |  Possible Values
       --------------------------
       answer |     Yes | No
  -->
  <atom>
    <slot name="type"      val="yes_no_answer"/>
    <slot name="subtype"   val="user"/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed"  val="false" type="string"/>
    <slot name="answer"    val="_NO_VALUE_" type="string" />
  </atom>

  <!-- ############################################################## -->
  <!-- ####### MOnarCH CA Activations                           ##### -->
  <!-- ############################################################## -->

  <atom>
    <slot name="type"                  val="ca_activations"/>
    <slot name="subtype"               val="user"/>
    <slot name="timestamp"             value="_NO_VALUE_" type="number"/>
    <slot name="consumed"              val="false"      type="string"/>
    <slot name="activated_cas"         val="_NO_VALUE_" type="string"/>
    <!-- Other slots that might come together with the activation -->
    <slot name="name_of_game"          val="_NO_VALUE_" type="string"/>
    <slot name="question_to_user"      val="_NO_VALUE_" type="string"/>
    <slot name="tell_user"             val="_NO_VALUE_" type="string"/>
    <slot name="child_name"            val="_NO_VALUE_" type="string"/>
    <slot name="congratulation_motive" val="_NO_VALUE_" type="string"/>
    <slot name="encouragement_motive"  val="_NO_VALUE_" type="string"/>
    <slot name="media_menu"            val="_NO_VALUE_" type="string"/>
<!-- 
    <slot name="ca01" val="false" type="string"/>
    <slot name="ca02" val="false" type="string"/>
    <slot name="ca03" val="false" type="string"/>
    <slot name="ca04" val="false" type="string"/>
    <slot name="ca05" val="false" type="string"/>
    <slot name="ca06" val="false" type="string"/>
    <slot name="ca07" val="false" type="string"/>
    <slot name="ca08" val="false" type="string"/>
    <slot name="ca09" val="false" type="string"/>
    <slot name="ca10" val="false" type="string"/>
    <slot name="ca11" val="false" type="string"/>
    <slot name="ca12" val="false" type="string"/>
    <slot name="ca13" val="false" type="string"/>
    <slot name="ca14" val="false" type="string"/>
    <slot name="ca15" val="false" type="string"/>
    <slot name="ca16" val="false" type="string"/>
    <slot name="ca17" val="false" type="string"/>
    <slot name="ca18" val="false" type="string"/>
    <slot name="ca19" val="false" type="string"/>
    <slot name="ca20" val="false" type="string"/>
    <slot name="ca21" val="false" type="string"/>
    <slot name="ca22" val="false" type="string"/>
    <slot name="ca23" val="false" type="string"/>
    <slot name="ca24" val="false" type="string"/>
    <slot name="ca25" val="false" type="string"/>
    <slot name="ca26" val="false" type="string"/>
-->
  </atom>

  <atom>
    <slot name="type" val="allowed_hri_interfaces"/>
    <slot name="subtype" val="user"/>
    <!-- 
        A list of HRI Interfaces separated by '|' that the CA can use.
        Current supported interface names:  
          'arms|head|mouth|eyes|voice|audio|image|ar
    -->
    <slot name="interfaces" val="_NO_VALUE_" type="string" />
    <slot name="arms"   val='0' type='number'/>
    <slot name="head"   val='0' type='number'/>
    <slot name="mouth"  val='0' type='number'/>
    <slot name="eyes"   val='0' type='number'/>
    <slot name="voice"  val='0' type='number'/>
    <slot name="audio"  val='0' type='number'/>
    <slot name="image"  val='0' type='number'/>
    <slot name="ar"     val='0' type='number'/>
    <slot name="timestamp" value="_NO_VALUE_" type="number"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>
  

  <!-- An Atom to block a recipe. Use it carefully -->
  <atom>
    <slot name="type" val="idle"/>
    <slot name="subtype" val="user"/>
    <slot name="do_nothing" val="_"/>
  </atom>

  <!-- ############################################################## -->
  <!-- ####### GLOBALS                                          ##### -->
  <!-- ############################################################## -->

  <!-- Globals atom with Slots unified by Iwaki, and user atom inherit this slots-->
  <atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="time" val="_NO_VALUE_" type="number" var="_im_time"/>
    <slot name="consumed" val="false" type="string"/>
  </atom>

  <atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="user"/>
  </atom>

</default_atoms>
