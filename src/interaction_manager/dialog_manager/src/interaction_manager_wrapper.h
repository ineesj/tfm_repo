#ifndef IM_H
#define IM_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <deque>

#include <string>
#include <csignal>
#include <algorithm>

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ros/time.h"
#include "ros/duration.h"

#include "dialog_manager_msgs/sema.h"
#include "dialog_manager_msgs/AtomMsg.h"
#include "dialog_manager_msgs/ActionMsg.h"
#include "dialog_manager_msgs/ActionStatusMsg.h"

#include "iwaki.h"
#include "log.h"

#define RESET_COLOR "\e[m"
#define MAKE_GREEN "\e[1;32m"
#define MAKE_BLACK "\e[0;30m"
#define MAKE_RED "\e[1;31m"
#define MAKE_BLUE "\e[1;34m"
#define MAKE_CYAN "\e[1;36m"
#define MAKE_BROWN "\e[0;33m"
#define MAKE_PURPLE "\e[1;35m"
#define MAKE_YELLOW "\e[1;33m"
#define MAKE_BLUE_UNDERLINE "\e[4;34m"
#define MAKE_YELLOW_UNDERLINE "\e[4;33m"

using namespace std;
using std::string;
using std::iostream;

class InteractionManagerWrapper {

  public:

    InteractionManagerWrapper(){}
    ~InteractionManagerWrapper(){}
    void init(int argc, char *argv[]);
    int load_arguments( int argc, char * argv[] );
    void main_loop();

    /* input functions */
    void atomReceivedCallback( const dialog_manager_msgs::AtomMsg::ConstPtr & msg);
    void actionStatusReceivedCallback( const dialog_manager_msgs::ActionStatusMsg::ConstPtr & msg);

    /* output functions */
    void publish_action( Action &anAction );


    void dispatchActionsFromOutputQueue();
    void callbackAction( Action &an_action, ActionStatus &astat);

    /* action executor's function */
    bool executeDefaultAction( Action &an_action );
    virtual bool executeMoreActions( Action &an_action );


    /* action completion status handler */
    void hndActionCompletionStatus(ActionStatus &astat);

    /* fusion multimodal - perception */
    ros::Subscriber im_atom_sub;
    ros::Subscriber im_action_state_sub;

    /* fision multimodal - expression */
    ros::Publisher im_action_pub;

    /* Everythig is here */
    InteractionManager interaction_manager;

    /* global members */
    string logfile_name;
    ros::Duration period_time;

};

#endif // IM_H
