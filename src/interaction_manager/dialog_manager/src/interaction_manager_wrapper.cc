#include "interaction_manager_wrapper.h"

/** Enroll the interaction_manager node and load arguments */
void InteractionManagerWrapper::init(int argc, char * argv[]) {
    ros::init( argc, argv, "interaction_manager_wrapper" );
    ros::NodeHandle node_handle;

    // Subscribe to receive atoms for multimodal fusion
    im_atom_sub = node_handle.subscribe( "im_atom", 0, &InteractionManagerWrapper::atomReceivedCallback, this );
    // Subscribe to receive actions to invoke for iwaki parser of actions
    im_action_state_sub = node_handle.subscribe( "im_action_state", 0, &InteractionManagerWrapper::actionStatusReceivedCallback, this );

    // Publisher fo emit actions when receive im_action_state
    im_action_pub = node_handle.advertise<dialog_manager_msgs::ActionMsg>( "im_action", 0 );

    period_time = ros::Duration(1);
    this->load_arguments(argc, argv);
}

/** Load the input arguments received for this node by the launcher */
int InteractionManagerWrapper::load_arguments( int argc, char * argv[] ) {
    char* cvalue;
    string debug_level;
    int c;
    while(1) {
        static struct option long_options[] =
        {
            /* These options set a flag. */
            //{"verbose", no_argument,       &verbose_flag, 1},
            //{"brief",   no_argument,       &verbose_flag, 0},
            /* These options don't set a flag.
                       We distinguish them by their indices. */
            {"help",    no_argument,       0, 'h'},
            {"text_ui", no_argument,       0, 'x'},
            {"log",     required_argument, 0, 'l'},
            {"debug",   required_argument, 0, 'd'},
            {"timer",   required_argument, 0, 't'},
            {"init",    required_argument, 0, 'i'},
            {"atoms",   required_argument, 0, 'a'},
            {"path",    required_argument, 0, 'p'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;
        c = getopt_long (argc, argv, "hxl:d:t:i:a:p:s:", long_options, &option_index);

        if (c == -1)  // Detect the end of the options.
            break;

        switch (c) {
        case 0:

            /* If this option set a flag, do nothing else now. */
            if (long_options[option_index].flag != 0)
                break;
            ROS_INFO ("option %s", long_options[option_index].name);
            if (optarg)
                ROS_INFO (" with arg %s", optarg);
            ROS_INFO ("\n");
            break;

        case 'h':
            cout << "Usage: imcore [OPTION]... \n";
            cout << "Mandatory arguments to long options are mandatory for short options too.\n";
            cout << "   -d --debug DEBUG_LEVEL\n"
                    "\tDEBUG_LEVEL is one of the following:\n"
                    "\tERROR, WARNING, INFO, DEBUG, DEBUG1, DEBUG2, DEBUG3, DEBUG4\n";
            cout << "   -t --timer TIMER_PERIOD\n"
                    "\tIf present, this option causes IM to run in timer mode,\n"
                    "\twith every TIMER_PERIOD  seconds IM state update is forced.\n"
                    "\tIf the option is not present IM runs in callback mode\n";
            cout << "   -l --log LOG_FILE\n"
                    "\tIf present, this option causes "
                    "all the debug output be directed to the LOG_FILE\n";
            cout << "   -i --init INIT_FILE\n"
                    "\tname of the IM init file that overrides default initialize_im.xml\n";
            cout << "   -a --atoms DEFAULTS_FILE\n"
                    "\tName of the atom defaults file that overrides "
                    "default default_atoms.xml\n ";
            cout << "   -p --path PATH\n"
                    "\tpath to recipes, actions and functions directories\n ";
            cout << "   -h --help\n"
                    "\tThis info.\n";
            cout << "   For example:\n"
                    "\t./bin/my_first_example -t 0.1 -d DEBUG3 -l log1 -p ./data -i initialize_im.xml\n";
            cout << endl;
            return -1;
        case 'd':
        {
            cvalue = optarg;
            debug_level = string((const char*) cvalue);
            ROS_INFO(MAKE_BLUE "The debug level is: %s" RESET_COLOR,debug_level.c_str());
            break;
        }
        case 'p':
        {
            cvalue = optarg;
            interaction_manager.script_path = (string) ((const char*) cvalue);
            ROS_INFO(MAKE_BLUE "The PATH to recipes/atoms/actions/init_files is: %s" RESET_COLOR,interaction_manager.script_path.c_str());
            break;
        }
        case 'l':
        {
            cvalue = optarg;
            logfile_name = (string)((const char*) cvalue);
            if (!setLogFile(logfile_name)) {
                return -1;
            }else{
                ROS_INFO (MAKE_BLUE "The log file is %s: " RESET_COLOR,logfile_name.c_str());
            }
            break;
        }
        case 't':
        {
            cvalue = optarg;
            std::string cvalue_str = string((const char*) cvalue);
            double timer_period_sec = string_to_double(cvalue_str);
            if ((timer_period_sec > 1.0)||(timer_period_sec <= 0)) {
                ROS_ERROR("Timer period should be positive and not more than 1.0 seconds.");
                return -1;
            }
            period_time = ros::Duration(timer_period_sec);
            ROS_INFO(MAKE_BLUE "Loop rate set at %lf seconds" RESET_COLOR,period_time.toSec());
            break;
        }
        case 'i':
         {
            cvalue = optarg;
            interaction_manager.init_file_name = (string) ((const char*) cvalue);
            ROS_INFO(MAKE_BLUE "The init_file  is %s: "RESET_COLOR,interaction_manager.init_file_name.c_str());
            break;
        }
        case 'a':
        {
            cvalue = optarg;
            interaction_manager.default_atoms_file_name = (string) ((const char*) cvalue);
            ROS_INFO(MAKE_BLUE "The file with de default atoms loaded is: %s" RESET_COLOR,interaction_manager.default_atoms_file_name.c_str());
            break;
        }
        default:
            ROS_ERROR("Unknown option. Try 'my_first_example --help' for more information");
            return -1;
        }

    }

    /* Print any remaining command line arguments (not options). */
    if (optind < argc) {
        ROS_INFO ("non-option ARGV-elements: ");
        while (optind < argc)
            ROS_INFO("%s ", argv[optind++]);
        putchar ('\n');
    }

    FILELog::ReportingLevel() =  FILELog::FromString(debug_level.empty() ? "DEBUG1" : debug_level);


    if (!interaction_manager.initialize()) {
        ROS_ERROR("Initialization of Interaction Manager has failed");
        return -1;
    }

    ROS_DEBUG (MAKE_BLUE "Recipes loaded: " RESET_COLOR);
    interaction_manager.printRecipes();
    interaction_manager.printTriggerables();

    ROS_INFO(MAKE_GREEN "Initialization of Interaction Manager succeeded." RESET_COLOR);

    return 0;

}


/** Executed for the main */
void InteractionManagerWrapper::main_loop() {

    ros::Time start_tick,end_tick;
    ros::Duration elapsed_time,time_to_sleep;

    while (ros::ok()) {

        start_tick = ros::Time::now();
        interaction_manager.doTick();

        /* Dispatch actions collected at the output queue */
        dispatchActionsFromOutputQueue();


        /* For log, check if need to swap log file */
        FILE* pStream = Output2FILE::Stream();
        // fseek( pStream, 0, SEEK_END );
        int logfile_size = ftell( pStream );
        FILE_LOG(logDEBUG3) << "Logfile size: " << logfile_size;
        if (logfile_size > MAX_LOGFILE_SIZE ) {
            /* move on to a new logfile */
            if (!setLogFile(logfile_name)) {
                ROS_ERROR("Problem to set logFile. Aborted execution");
                exit(-1);
            }
        }


        end_tick = ros::Time::now();
        elapsed_time = end_tick - start_tick;
        time_to_sleep = period_time-elapsed_time;
        ROS_DEBUG("Performed tick at %lf sec", elapsed_time.toSec());
        ROS_DEBUG(MAKE_BLUE "Time to sleep at %lf sec" RESET_COLOR, time_to_sleep.toSec());


        //PRINT IWAKI INFORMATION ABOUT NODES WHEN COUNTER BECOME 0
        if (interaction_manager.timing_dump_counter == 0) {
            //ROS_INFO(MAKE_BLUE "///////////////////iWaki internal state info/////////////////////" RESET_COLOR);
            //interaction_manager.ptree.print(logINFO);
            //interaction_manager.getGlobalBindings()->print(logINFO);
            //ROS_INFO(MAKE_YELLOW "/////////////////////////////////////////////////////////////////" RESET_COLOR);
        }


        ros::spinOnce();
        //it is neccessary to sleep to perform the callback the recieved atoms emitted from multimodal_fusion_dialog
        if (time_to_sleep.toNSec()>0) time_to_sleep.sleep();



    }

}


/**
  * Received atoms for multimodal fusion dialog and dispatch it to Iwaki input queue
  */
void InteractionManagerWrapper::atomReceivedCallback(const dialog_manager_msgs::AtomMsg::ConstPtr &anAtomMsg) {

    Atom new_atom;

    for (std::vector<dialog_manager_msgs::VarSlot>::const_iterator varslot_it = anAtomMsg->varslots.begin();
         varslot_it != anAtomMsg->varslots.end(); varslot_it++) {
        VarSlot newVarSlot;
        newVarSlot.name = varslot_it->name;
        ROS_DEBUG("Slot name %s",newVarSlot.name.c_str());
        newVarSlot.val = varslot_it->val;
        ROS_DEBUG("Slot value %s",newVarSlot.val.c_str());
        newVarSlot.relation = varslot_it->relation;
        ROS_DEBUG("Slot relation %s",newVarSlot.relation.c_str());
        newVarSlot.type = varslot_it->type;
        ROS_DEBUG("Slot type %s",newVarSlot.type.c_str());
        newVarSlot.unique_mask = varslot_it->unique_mask;
        ROS_DEBUG("Slot unique_mask %d",newVarSlot.unique_mask);
        new_atom.varslots.push_back(newVarSlot);
        if (newVarSlot.name=="type"){
            ROS_DEBUG (MAKE_BLUE "==> Atom '%s'' received for Interaction Manager and dispatech to IWaki" RESET_COLOR,newVarSlot.val.c_str());
        }
    }
    interaction_manager.input_queue.push_back(new_atom);
}



/**
  * Ths method publish the action msgs of dialog_manager_msgs to be received for multimodal_fission_dialog
  */
void InteractionManagerWrapper::publish_action(Action &anAction) {

    ROS_DEBUG (MAKE_BLUE "<-- Action message is sent, with name: " MAKE_BLUE "%s"  RESET_COLOR, anAction.name.c_str());

    dialog_manager_msgs::ActionMsg anActionMsg;

    for (std::list<ArgSlot>::iterator arg_it = anAction.args.begin();
         arg_it != anAction.args.end(); arg_it++) {
        dialog_manager_msgs::ArgSlot anArgSlot;
        anArgSlot.name = arg_it->name;
        anArgSlot.value = arg_it->value;
        anArgSlot.default_value = arg_it->default_value;
        anArgSlot.type = arg_it->type;
        anArgSlot.unit = arg_it->unit;
        anArgSlot.var = arg_it->var;  // var is not really used for action slots
        anActionMsg.args.push_back(anArgSlot);
    }

    /* convert the return args*/
    for (std::list<ArgSlot>::iterator arg_it = anAction.return_args.begin();
         arg_it != anAction.return_args.end(); arg_it++) {
        dialog_manager_msgs::ArgSlot anArgSlot;
        anArgSlot.name = arg_it->name;
        anArgSlot.value = arg_it->value;
        anArgSlot.default_value = arg_it->default_value;
        anArgSlot.type = arg_it->type;
        anArgSlot.unit = arg_it->unit;
        anArgSlot.var = arg_it->var; // var is not really used for action return slots

        anActionMsg.return_args.push_back(anArgSlot);
    }

    anActionMsg.name = anAction.name;
    anActionMsg.id = anAction.id;
    anActionMsg.actor = anAction.actor;
    anActionMsg.thetimeout = anAction.timeout; // "timeout" name could give compilation errors, therefore uses thetimeout.
    anActionMsg.if_timeout = anAction.if_timeout;
    anActionMsg.priority = anAction.priority;


    this->im_action_pub.publish(anActionMsg);
}





/**
  * This callback is performed when iwaki parse an action on the current recipe. It is neccesary to publish
  * an action topic so the multimodal fision received it and tranlate to the concret output modes
  */
void InteractionManagerWrapper::actionStatusReceivedCallback(const dialog_manager_msgs::ActionStatusMsg::ConstPtr &anActionStatusMsg) {

    ROS_DEBUG (MAKE_BLUE "Action Status received for Interaction Manager and dispatech to IWaki" RESET_COLOR);
    ActionStatus newActionStatus;

    for (std::vector<dialog_manager_msgs::ArgSlot>::const_iterator argslot_it =
         anActionStatusMsg->return_args.begin();
         argslot_it != anActionStatusMsg->return_args.end(); argslot_it++) {
        ArgSlot anArgSlot;
        anArgSlot.name = argslot_it->name;
        anArgSlot.value = argslot_it->value;
        anArgSlot.default_value = argslot_it->default_value;
        anArgSlot.type = argslot_it->type;
        anArgSlot.unit = argslot_it->unit;
        anArgSlot.var = argslot_it->var;

        newActionStatus.return_args.push_back(anArgSlot);
    }

    newActionStatus.action_id = anActionStatusMsg->action_id;
    newActionStatus.executor = anActionStatusMsg->executor;
    newActionStatus.status = anActionStatusMsg->status;

    this->hndActionCompletionStatus( newActionStatus );

}



/**
  * Publish secuancially all actions on queue, for thit fisrtly call to callbackAction, that internally execute publish_action
  */
void InteractionManagerWrapper::dispatchActionsFromOutputQueue() {
    std::list<Action>::iterator action_it = interaction_manager.output_queue.begin();

    ROS_DEBUG (MAKE_BLUE "The action queue has legth: %d" RESET_COLOR,  interaction_manager.output_queue.size());


    while( action_it != interaction_manager.output_queue.end() ) {
        /* hndAction:
             * here we dispatch the action from the queue.
             * Generally, since an action can take time longer than allowed
             * for the tick of the main loop,
             * hndAction and hndActionCompletionStatus
             * should be done asynchronously from this process. */
        ActionStatus astat;
        callbackAction(*action_it, astat);
        /* hndActionCompletionStatus:
             * here we pass the action completion status to the IM. We
             * do this synchroneously because in our example hndAction takes very
             * short time (it creates a new gstreamer process) and the ActionStatus
             * is returned right away, before the gstreamer finishes
             * playing the sound. Generally,
             * hndActionCompletionStatus should be called whenever an
             * asychnroneous ActionStatus signal arrives . */
        hndActionCompletionStatus(astat);

        action_it++;
        interaction_manager.output_queue.pop_front();
    }
}


/**
  * It is invoked by the dispatchActionsFromOutputQueue when new actions are invoked by the current recipe. Then
  * try to publish de action msgs of dialog_manager_msgs to be translated by the multimodal_fission_dialog
  */
void InteractionManagerWrapper::callbackAction( Action &an_action, ActionStatus &astat) {

    FILE_LOG(logDEBUG1) << "Generator received action:";
    FILE_LOG(logDEBUG1)  << "  action name: " << an_action.name;
    FILE_LOG(logDEBUG1)  << "  action id: " << an_action.id;
    FILE_LOG(logDEBUG1)  << "  action actor: " << an_action.actor;

    /* Publish the action on topic of dialog_manager_msgs*/
    ROS_DEBUG (MAKE_BLUE "<== Action message '%s'' sended to multimodal fission dialog" RESET_COLOR, an_action.name.c_str());
    this->publish_action( an_action );



    if (!executeDefaultAction( an_action )) {
        FILE_LOG(logWARNING)  << "Action named: "
                              << an_action.name << ", action id: "
                              << an_action.id << " not found by default. Searching in user action implementation";
        if( !executeMoreActions( an_action ) ) {
            FILE_LOG( logERROR ) << "There was an error executing action named: " << an_action.name << ", action id: ";
        }
    }

    /* get the args*/
    for (std::list<ArgSlot>::iterator arg_it = an_action.args.begin();
         arg_it != an_action.args.end(); arg_it++) {
        FILE_LOG(logDEBUG1)  << "  arg name: " << arg_it->name;
        FILE_LOG(logDEBUG1)  << "  arg value: " << arg_it->value;
        FILE_LOG(logDEBUG1)  << "  arg type: " << arg_it->type;
    }



    /* send an action completed status when done */
    astat.status = "completed";
    astat.executor = an_action.actor;
    astat.action_id = an_action.id;

    ROS_DEBUG (MAKE_BLUE "Generator sent status completed for action id: %d" RESET_COLOR, an_action.id);
    //FILE_LOG(logDEBUG1) << "Generator sent status completed for action id: " << an_action.id;
    return;
}

bool InteractionManagerWrapper::executeMoreActions( Action &an_action ) {}

/* action executor's function
TODO: handle return_args property of action.
*/
bool InteractionManagerWrapper::executeDefaultAction( Action &an_action ) {    
    ROS_DEBUG (MAKE_BLUE "Trying to execute the default action: %s" RESET_COLOR, an_action.name.c_str());
    ROS_DEBUG (MAKE_BLUE "Nothing to do!!!!!!!!!" RESET_COLOR);
    return true;

}

/* action completion status handler */
void InteractionManagerWrapper::hndActionCompletionStatus(ActionStatus &astat) {
    ROS_DEBUG (MAKE_BLUE "Received action completion status: %s" RESET_COLOR, astat.status.c_str());
    //FILE_LOG(logDEBUG3) << "IM received action completion status: ";
    //astat.print(logDEBUG3);
    interaction_manager.processActionCompletionStatus(astat);
    interaction_manager.ptree.print(logDEBUG3);
}

