#!/usr/bin/env python


# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.


PKG = 'monarch_multimodal_fission'
NNAME = 'action_to_gesture_expresion_node'

import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_multimodal_fission import aggregators
from dialog_manager_msgs.msg import (ActionMsg, ArgSlot)
from std_msgs.msg import String
from monarch_msgs.msg import GestureExpression


def generate_gesture_expression_action(names, values):
    slots = (ArgSlot(name=n, value=v) for n, v in zip(names, values))
    return ActionMsg(name='execute_gesture', actor='gesture_player',
                     args=list(slots))


class TestActionToGestureExpressionTranslatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestActionToGestureExpressionTranslatorNode, self).__init__(*args)
        # Publishers and Subscribers
        rospy.init_node(NNAME)
        rospy.Subscriber('express_gesture', GestureExpression, self.command_cb)
        self.publisher = rospy.Publisher('im_action', ActionMsg)
        self.iface_pub = rospy.Publisher('allowed_interfaces', String)

    def setUp(self):
        gesture_msg = self._generate_new_gesture_msg()
        self.iface_aggregator = aggregators.AllowedInterfacesAggregator()
        self.gesture_expression = self.iface_aggregator(gesture_msg)
        self.a_msg = generate_gesture_expression_action(self.names, self.values)

    def tearDown(self):
        pass

    def _generate_new_gesture_msg(self):
        self.names = ('gesture', 'emotion', 'user_name')
        self.values = ('give_greetings', 'happy', 'Joao')
        d = dict(zip(self.names, self.values))
        return GestureExpression(gesture=d.pop('gesture'),
                                 params=kvpa.from_dict(d))

    def command_cb(self, gesture_expr_msg):
        self.assertEqual(self.gesture_expression, gesture_expr_msg)

    def test_action_is_translated(self):
        self.publisher.publish(self.a_msg)
        # rospy.sleep(0.2)

    def test_node_changes_interfaces_when_receives_new_ifaces(self):
        ifaces = "arms|head|voice"
        self.iface_pub.publish(ifaces)
        rospy.sleep(0.2)
        gesture_msg = self._generate_new_gesture_msg()
        self.gesture_expression = self.iface_aggregator(gesture_msg)
        self.publisher.publish(self.a_msg)


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, NNAME, TestActionToGestureExpressionTranslatorNode)
