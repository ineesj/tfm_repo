#!/usr/bin/python
"""
@author: Victor Gonzalez Pacheco
@date: 2015-03
"""

from __future__ import division
PKG = 'monarch_multimodal_fission'
import roslib
roslib.load_manifest(PKG)

import unittest
from monarch_multimodal_fission import aggregators

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import GestureExpression


class TestAllowedInterfacesAggregator(unittest.TestCase):
    def __init__(self, *args):
        super(TestAllowedInterfacesAggregator, self).__init__(*args)

    def setUp(self):
        self.names = ('gesture', 'emotion', 'user_name')
        self.values = ('give_greetings', 'happy', 'Joao')
        d = dict(zip(self.names, self.values))
        self.gesture_expression = GestureExpression(gesture=d.pop('gesture'),
                                                    params=kvpa.from_dict(d))

    def tearDown(self):
        pass

    def test_allowed_interfaces_aggregator_aggregates_interfaces_to_msg(self):
        iface_aggregator = aggregators.AllowedInterfacesAggregator()
        gesture_msg = iface_aggregator(self.gesture_expression)
        dict_ = kvpa.to_dict(gesture_msg.params)
        self.assertTrue('allowed_interfaces' in dict_)
        self.assertEqual(aggregators._ALL_INTERFACES,
                         dict_['allowed_interfaces'])


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(PKG, 'test_action_to_kvpa', TestAllowedInterfacesAggregator)
