#!/usr/bin/env python

# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

""" Test for action to KVPA translator node """

PKG = 'monarch_multimodal_fission'
NNAME = 'test_action_to_kvpa_translator_node'

import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import KeyValuePairArray as KVPA
from dialog_manager_msgs.msg import (ActionMsg, ArgSlot)

_ITEMS = ('one', 'two', 'three')


def generate_action():
    slots = (ArgSlot(name=n, value=str(i))
             for i, n in enumerate(_ITEMS))
    return ActionMsg(name='an_action', actor='an_actor', args=list(slots))


class TestActionToKVPATranslatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestActionToKVPATranslatorNode, self).__init__(*args)
        rospy.init_node(NNAME)
        # Publishers and Subscribers
        # rospy.Subscriber('kvpa_topic', KVPA, self.kvpa_cb)
        self.publisher = rospy.Publisher('im_action', ActionMsg, latch=True)

    def setUp(self):
        d = {k: str(v) for v, k in enumerate(_ITEMS)}
        self.kvpa = kvpa.from_dict(d)

    def tearDown(self):
        pass

    # def kvpa_cb(self, gesture_expr_msg):
    #     self.assertEqual(self.gesture_expression, gesture_expr_msg)

    def _publish_and_then_wait_for_msg(self, msg, timeout=0.2):
        self.publisher.publish(msg)
        kvpa_msg = rospy.wait_for_message('kvpa_topic', KVPA, timeout)
        return kvpa_msg

    def _publish_and_expect_for_timeout(self, msg, timeout=0.2):
        self.publisher.publish(msg)
        with self.assertRaises(rospy.ROSException):
            _ = rospy.wait_for_message('kvpa_topic', KVPA, timeout)

    def test_action_to_kvpa_does_not_publish_anything_if_empty_action(self):
        self._publish_and_expect_for_timeout(ActionMsg())

    def test_action_to_kvpa_does_not_publish_anything_if_actor_not_valid(self):
        action = generate_action()
        action.name = "this is not the expected action"
        self._publish_and_expect_for_timeout(action)

    def test_action_to_kvpa_does_not_publish_anything_if_action_not_valid(self):
        action = generate_action()
        action.actor = "this is not the expected actor"
        self._publish_and_expect_for_timeout(action)

    def test_action_to_kvpa(self):
        action = generate_action()
        # rospy.Subscriber('kvpa_topic', KVPA, self.kvpa_cb)
        # self.publisher.publish(action)
        kvpa_msg = self._publish_and_then_wait_for_msg(action)
        self.assertEqual(kvpa.to_dict(self.kvpa),  kvpa.to_dict(kvpa_msg))


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, NNAME, TestActionToKVPATranslatorNode)
