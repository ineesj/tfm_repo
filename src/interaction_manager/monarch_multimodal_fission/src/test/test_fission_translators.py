#!/usr/bin/python
"""
@author: Victor Gonzalez Pacheco
@date: 2015-02
"""

from __future__ import division
PKG = 'monarch_multimodal_fission'
import roslib
roslib.load_manifest(PKG)

import unittest
from monarch_multimodal_fission import translators

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import KeyValuePairArray as KVPA
from dialog_manager_msgs.msg import (ActionMsg, ArgSlot)
from monarch_msgs.msg import GestureExpression

_ITEMS = ('one', 'two', 'three')


def generate_action():
    slots = (ArgSlot(name=n, value=i)
             for i, n in enumerate(_ITEMS))
    return ActionMsg(name='an_action', actor='an_actor', args=list(slots))


def generate_gesture_expression_ation(names, values):
    slots = (ArgSlot(name=n, value=v) for n, v in zip(names, values))
    return ActionMsg(name='execute_gesture', actor='gesture_player',
                     args=list(slots))


class TestActionToKVPATranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestActionToKVPATranslator, self).__init__(*args)

    def setUp(self):
        d = {k: v for v, k in enumerate(_ITEMS)}
        self.kvpa = kvpa.from_dict(d)

    def tearDown(self):
        pass

    def test_action_to_kvpa_returns_empty_kvpa_if_empty_action(self):
        self.assertEqual(KVPA(), translators.action_to_kvpa(ActionMsg()))

    def test_action_to_kvpa(self):
        action = generate_action()
        self.assertEqual(self.kvpa, translators.action_to_kvpa(action))


class TestActionToGestureExpressionTranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestActionToGestureExpressionTranslator, self).__init__(*args)

    def setUp(self):
        self.names = ('gesture', 'emotion', 'user_name')
        self.values = ('give_greetings', 'happy', 'Joao')
        d = dict(zip(self.names, self.values))
        self.gesture_expression = GestureExpression(gesture=d.pop('gesture'),
                                                    params=kvpa.from_dict(d))

    def tearDown(self):
        pass

    def test_action_to_gesture_expression_returns_empty_for_empty_actions(self):
        self.assertEqual(GestureExpression(),
                         translators.action_to_gesture_expression(ActionMsg()))

    def test_action_to_kvpa(self):
        action = generate_gesture_expression_ation(self.names, self.values)
        self.assertEqual(self.gesture_expression,
                         translators.action_to_gesture_expression(action))

if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(PKG, 'test_action_to_kvpa', TestActionToKVPATranslator)
    rosunit.unitrun(PKG, 'test_action_to_gesture_expression_translator',
                    TestActionToGestureExpressionTranslator)
