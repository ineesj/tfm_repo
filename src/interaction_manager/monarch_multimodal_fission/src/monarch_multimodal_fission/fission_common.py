#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fission')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)
from rospy_utils import as_iter


def str_to_bool(s):
    ''' Converts the strings 'true' and 'false' to its corresponding bool values
    '''
    return s.lower() == 'true'


# def as_iter(obj):
#     '''
#         Checks if an object is iterable. If not it encapuslates it in a tuple
#         @param obj: the object to check if is iterable
#         @return: obj if it is iterable, else (obj, )

#         Eg.:

#             >>> as_iter([1,2,3])
#             ... [1,2,3]
#             >>> as_iter(123)
#             ... (123,)
#             >>> as_iter('hello')
#             ... ('hello',)
#     '''
#     return obj if hasattr(obj, '__iter__') else (obj,)


# def arg_to_msg_attrib(msg, arg):
def set_attrib(msg, arg):
    ''' Creates arg.name as a new attribute of msg if arg.value is not empty.
        The value of the new attribute of msg is retrieved from arg.value.
        The type of the new attribute of msg is retrieved from arg.type.
    '''
    if arg.value:
        attrib_type = type(getattr(msg, arg.name))
        setattr(msg, arg.name, attrib_type(arg.value))


def translate_arguments_to_msg(arguments, msg_ctor):
    ''' Translates a list of L{ArgSlots} to a L{Utterance} message
        :return:  L{Utterance} message corresponding to the passed ArgSlots
    '''
    msg = msg_ctor()
    for arg in as_iter(arguments):
        # loginfo("ETTS_TRANSTLATOR: Processing ArgSlot: {}".format(str(arg)))
        set_attrib(msg, arg)
    return msg
