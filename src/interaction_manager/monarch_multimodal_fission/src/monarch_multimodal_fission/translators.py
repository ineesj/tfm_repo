#!/usr/bin/env python

""" Translators for MOnarCH Multimodal Fission. """

# import roslib
# roslib.load_manifest('monarch_multimodal_fission')
# import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import GestureExpression


def action_to_kvpa(action):
    """ Translate an ActionMsg to a KeyValuePairArray message. """
    d = {arg.name: arg.value for arg in action.args}
    logdebug("Translated Action {}".format(action.name))
    logdebug("ArgSlots of action {} are:\n{}".format(action.name, d))
    return kvpa.from_dict(d)


def action_to_gesture_expression(action):
    """ Translate an ActionMsg to a GestureExpression message. """
    if not action.args:
        return GestureExpression()
    d = {arg.name: arg.value for arg in action.args}
    return GestureExpression(gesture=d.pop('gesture'), params=kvpa.from_dict(d))
