#!/usr/bin/env python

"""Translators for MOnarCH Multimodal Fission."""

import roslib
roslib.load_manifest('monarch_multimodal_fission')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_msgs.msg import KeyValuePair as KVP
from copy import deepcopy

from std_msgs.msg import String
_ALL_INTERFACES = \
    "arms|audio|cheeks|eyes|head|image|leds_base|mouth|projector|voice"


def aggregate_interfaces(interfaces, gesture_msg):
    """
    Append a list of interfaces to the params of a gesture_msg.

    :param str interfaces: the interfaces to append
    :param monarch_msgs.msg.GestureExpression gesture_msg: the message
    :return: the message with the appended list of interfaces
    :rtype: monarch_msgs.msg.GestureExpression
    """
    msg = deepcopy(gesture_msg)
    msg.params.array.append(KVP(key="allowed_interfaces", value=interfaces))
    return msg


class AllowedInterfacesAggregator():

    """
    Functor node that returns the allowed interfaces when called.

    The node subscribes to allowed_interfaces and, it returns them when called.
    """

    def __init__(self, **kwargs):
        """Init."""
        self.allowed_interfaces = _ALL_INTERFACES
        rospy.Subscriber("allowed_hri_interfaces", String, self._callback)

    def __call__(self, gesture_msg):
        """Return a copy of gesture_msg with allowed_interfaces appended. """
        return aggregate_interfaces(self.allowed_interfaces, gesture_msg)

    def get_allowed_interfaces(self):
        """Return the allowed interfaces."""
        return self.allowed_interfaces

    def _callback(self, interfaces_msg):
        """Store internally the data received in the interfaces message."""
        self.allowed_interfaces = interfaces_msg.data
