#!/usr/bin/env python
import rospy
#from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_msgs.msg import KeyValuePairArray as KVPA
from sam_helpers.writer import SAMWriter

if __name__ == '__main__':
    try:
        rospy.init_node('command_from_user_sam_publisher')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        robot_id = rospy.get_namespace()
        robot_id = robot_id.strip('/')
        slot_name = "command_from_user"
        sam_writer = SAMWriter(slot_name, agent_name = robot_id)
        rospy.Subscriber('command_from_user', KVPA, sam_writer.publish)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
