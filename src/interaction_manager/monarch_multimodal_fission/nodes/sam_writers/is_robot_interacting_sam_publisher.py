#!/usr/bin/env python
import rospy

from std_msgs.msg import Bool
from sam_helpers.writer import SAMWriter

if __name__ == '__main__':
    try:
        rospy.init_node('command_from_user_sam_publisher')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        robot_id = rospy.get_namespace()
        robot_id = robot_id.strip('/')
        slot_name = "interacting"
        sam_writer = SAMWriter(slot_name, agent_name = robot_id)
        rospy.Subscriber('interacting', Bool, sam_writer.publish)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
