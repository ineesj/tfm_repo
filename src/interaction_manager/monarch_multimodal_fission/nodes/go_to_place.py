#!/usr/bin/env python
# :copyright:    Copyright (C) 2015 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

""" Node that gets a Location by name and sends tells the nav to move there. """

import roslib
roslib.load_manifest('monarch_multimodal_fission')
import argparse
import sys
from functools import partial
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_msgs_utils import key_value_pairs as kvpa
from rospy_utils import coroutines as co
from monarch_multimodal_fission import translators
from geometry_msgs.msg import Point
from monarch_msgs.msg import KeyValuePairArray as KVPA
from dialog_manager_msgs.msg import ActionMsg

_DEFAULT_NAME = 'go_to_place_node'
_NODE_PARAMS = ()
_ACTOR_NAME = ''
_DEFAULT_ACTION_NAME = ''

PLACES = {"classroom": Point(x=6.75, y=-4.14, z=0.64),
          "playroom": Point(x=2.25, y=-2.24, z=0.96),
          "corridor": Point(x=-1.8, y=0.26, z=0.7),
          "dormitory": Point(x=4.25, y=11.41, z=0.69),
          "cantina": Point(x=1.0, y=5.96, z=0.80)}


def get_place_name(kvpa_msg):
    """ Return a place name from a KVPA Messave. """
    logdebug("GOT KVPA MSG: {}".format(kvpa.to_dict(kvpa_msg)))
    if kvpa_msg.array:
        d = kvpa.to_dict(kvpa_msg)
        return d['place']


def get_location_from_place_name(destination):
    """ Return a location coordinates from its name. """
    return PLACES.get(destination, None)


if __name__ == '__main__':
    try:
        rospy.init_node(_DEFAULT_NAME)
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))
        pipe = co.pipe([co.mapper(get_place_name),
                        co.filterer(lambda place: bool(place)),
                        co.mapper(get_location_from_place_name),
                        co.publisher('screen_send_nav', Point)])
        co.PipedSubscriber('move_robot', KVPA, pipe)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
