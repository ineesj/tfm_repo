#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fission')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

#from std_msgs.msg import String
from dialog_manager_msgs.msg import ActionMsg
# from dialog_manager_msgs.msg import ArgSlot

from monarch_msgs.msg import NonVerbalSound


_DEFAULT_NODE_NAME = 'action_to_nvsound_translator_node'
_NODE_PARAMS = ()
_ACTOR_NAME = 'sound_player'
_DEFAULT_ACTION_NAME = 'play_sound'


def as_iter(obj):
    '''
        Checks if an object is an iterable. If not it encapuslates it in a tuple
        @param obj: the object to check if is iterable
        @return: obj if it is iterable, else (obj, )

        Eg.:

            >>> as_iter([1,2,3])
            ... [1,2,3]
            >>> as_iter(123)
            ... (123,)
            >>> as_iter('hello')
            ... ('hello',)
    '''
    return obj if hasattr(obj, '__iter__') else (obj,)


def set_attrib(msg, arg):
    ''' Creates arg.name as a new attribute of msg if arg.value is not empty.
        The value of the new attribute of msg is retrieved from arg.value.
        The type of the new attribute of msg is retrieved from arg.type.
    '''
    if arg.value:
        attrib_type = type(getattr(msg, arg.name))
        setattr(msg, arg.name, attrib_type(arg.value))


def translate_arguments(arguments):
    ''' Translates a list of L{ArgSlots} to a L{Utterance} message
        :return:  L{Utterance} message corresponding to the passed ArgSlots
    '''
    nvs = NonVerbalSound()
    for arg in as_iter(arguments):
        # loginfo("ETTS_TRANSTLATOR: Processing ArgSlot: {}".format(str(arg)))
        set_attrib(nvs, arg)
    return nvs


def process_action(action):
    ''' @todo
        Should process the name of the action and execute accordingly.
        Features:
            should be able to process:
              - say_text
              - say_random_text
    '''
    if action.name != _DEFAULT_ACTION_NAME:
        logwarn("Action {} should be {}".format(action.name,
                                                _DEFAULT_ACTION_NAME))
        return
    return translate_arguments(action.args)


class ActionToNonVerbalSoundTranslator():

    ''' Node that receives L{ActionMsg} messages, transforms them
        to L{Utterance} messages and publishes these utterances to the etts
    '''

    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NODE_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        # Subscribers
        rospy.Subscriber("im_action", ActionMsg, self.action_cb)
        # Publishers
        self.publisher = rospy.Publisher('play_sound', NonVerbalSound)

    def action_cb(self, msg):
        logdebug("Recived ActionMsg: {}".format(str(msg)))
        if msg.actor != _ACTOR_NAME:
            logdebug("Actor {} is not {}".format(msg.actor, _ACTOR_NAME))
            return
        nvsound = process_action(msg)
        logdebug("Publishing NonVerbalSound: \n{}\n".format(str(nvsound)))
        self.publisher.publish(nvsound)

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        loginfo('Shutting down ' + rospy.get_name() + ' node.')


if __name__ == '__main__':
    try:
        node = ActionToNonVerbalSoundTranslator()
        node.run()
    except rospy.ROSInterruptException:
        pass
