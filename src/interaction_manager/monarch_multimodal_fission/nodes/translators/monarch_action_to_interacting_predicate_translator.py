#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fission')
import rospy
from rospy import (loginfo, logdebug, logwarn, logerr, logfatal)


from std_msgs.msg import Bool
from dialog_manager_msgs.msg import ActionMsg

# from monarch_multimodal_fission import fission_common as fis
from monarch_multimodal_fission import fission_common as fis

_DEFAULT_NODE_NAME = 'action_to_interacting_predicate_node'
_NODE_PARAMS = ()
_ACTOR_NAME = 'interacting_publisher'
_DEFAULT_ACTION_NAME = 'interacting'


def process_action(action):
    '''
        Converts the action to a message
    '''
    if action.name != _DEFAULT_ACTION_NAME:
        logwarn("Action {} should be {}".format(action.name,
                                                _DEFAULT_ACTION_NAME))
        return
    return fis.translate_arguments_to_msg(action.args, Bool)


class ActionToInteractingPredicateTranslator():

    ''' Node that receives L{ActionMsg} messages, transforms them
        to L{Utterance} messages and publishes these utterances to the etts
    '''

    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NODE_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        # Subscribers
        rospy.Subscriber("im_action", ActionMsg, self.action_cb)
        # Publishers
        self.publisher = rospy.Publisher('interacting', Bool)

    def action_cb(self, msg):
        logdebug("Recived ActionMsg: {}".format(str(msg)))
        if msg.actor != _ACTOR_NAME:
            logdebug("Actor {} is not {}".format(msg.actor, _ACTOR_NAME))
            return

        if msg.name != _DEFAULT_ACTION_NAME:
            return

        bool_msg = Bool()
        # logdebug("Interacting message received: {}".format(str(msg)))
        bool_msg.data = fis.str_to_bool(msg.args[0].value)
        loginfo("Interacting with user: {}\n".format(str(bool_msg)))
        self.publisher.publish(bool_msg)

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        loginfo('Shutting down ' + rospy.get_name() + ' node.')


if __name__ == '__main__':
    try:
        node = ActionToInteractingPredicateTranslator()
        node.run()
    except rospy.ROSInterruptException:
        pass
