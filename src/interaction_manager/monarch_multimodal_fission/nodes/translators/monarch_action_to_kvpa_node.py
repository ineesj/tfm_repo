#!/usr/bin/env python
# :copyright:    Copyright (C) 2015 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

""" Node that translates dialog_manager_msgs/ActionMsg messages to
    monarch_msgs/KeyValuePairArray messages

The arguments of the node are:

Arguments
---------
out_topic
    Topic to which send the KeyValuePairArray msg
expected_action
    Name of the action to be executed
expected_actor
    Name of the actor that should execute the action
"""

import roslib
roslib.load_manifest('monarch_multimodal_fission')
import argparse
import sys
from functools import partial
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from rospy_utils import coroutines as co
from monarch_multimodal_fission import translators
from monarch_msgs.msg import KeyValuePairArray as KVPA
from dialog_manager_msgs.msg import ActionMsg

_DEFAULT_NAME = 'action_to_kvpa_translator'
_NODE_PARAMS = ()
_ACTOR_NAME = ''
_DEFAULT_ACTION_NAME = ''


def parse_arguments(args):
    """Parses all the arguments"""

    parser = argparse.ArgumentParser(
        description="Creates a node that translates from " +
                    "ActionMsg to KeyValuePairArray messages ")
    parser.add_argument('out_topic',
                        help='Topic to which send the KeyValuePairArray msg')
    parser.add_argument("action_name",
                        help="Name of the action to be executed")
    parser.add_argument("actor_name",
                        help="Name of the actor that should execute the action")
    args = parser.parse_args(args)
    return args.out_topic, args.action_name, args.actor_name


def is_action_valid(action, expected_action_name):
    """ Check if action name is the expected_action_name. """
    if expected_action_name == action.name:
        return True
    logdebug("Action {} != {} Discarding"
             .format(action.name, expected_action_name))
    return False
    # return expected_action_name == action.name


def is_actor_valid(action, expected_actor):
    """ Check if action_actor is the expected_actor """
    if expected_actor == action.actor:
        return True
    logdebug("Action actor {} != {} Discarding"
             .format(action.actor, expected_actor))
    return False
    # return expected_actor == action.actor


def validate_action(action, actor, action_name):
    """ Evaluate action against check_action and check_actor predicats. """
    if (is_actor_valid(action, actor) and is_action_valid(action, action_name)):
        return True
    logdebug("Actor {} discarded action {}".format(actor, action.name))
    return False


if __name__ == '__main__':

    my_args = rospy.myargv(sys.argv)[1:]
    out_topic, expected_action, expected_actor = parse_arguments(my_args)

    check_action_predicates = partial(validate_action,
                                      actor=expected_actor,
                                      action_name=expected_action)
    try:
        rospy.init_node(_DEFAULT_NAME)
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        pipe = co.pipe([co.filterer(check_action_predicates),
                        co.mapper(translators.action_to_kvpa),
                        co.publisher(out_topic, KVPA)])
        co.PipedSubscriber('im_action', ActionMsg, pipe)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
