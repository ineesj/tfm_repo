#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fission')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from rospy_utils import coroutines as co
from monarch_multimodal_fission import (translators, aggregators)
from dialog_manager_msgs.msg import ActionMsg
from monarch_msgs.msg import GestureExpression

_DEFAULT_NAME = 'action_to_gesture_expression_translator'
_NODE_PARAMS = ()
_ACTOR_NAME = 'gestural_actor'
_DEFAULT_ACTION_NAME = 'execute_gesture'

tranlsate_gesture_msg = translators.action_to_gesture_expression  # Alias


def is_action_execute_gesture(action):
    return 'execute_gesture' == action.name


def is_actor_gestural_actor(action):
    return 'gestural_actor' == action.actor


def should_process_action(action):
    return is_action_execute_gesture(action) and is_actor_gestural_actor(action)


if __name__ == '__main__':
    try:
        rospy.init_node('action_to_gesture_expression_translator')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        aggregate_interfaces = aggregators.AllowedInterfacesAggregator()

        pipe = co.pipe([co.filterer(should_process_action),
                        co.mapper(tranlsate_gesture_msg),
                        co.mapper(aggregate_interfaces),
                        co.publisher('express_gesture', GestureExpression)])
        co.PipedSubscriber('im_action', ActionMsg, pipe)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
