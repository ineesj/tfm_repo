#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from dialog_manager_msgs.msg import AtomMsg
from monarch_msgs.msg import InteractionExecutorStatus as StatusMsg
from monarch_multimodal_fusion import translators

# from itertools import chain


_DEFAULT_NAME = 'gesture_status_to_atom_translator'
_NODE_PARAMS = ()


class GestureStatusToAtomTranslator():

    ''' Node that receives an L{InteractionExecutorStatus} msg, transforms it
        to L{AtomMsg} and publishes it
    '''

    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        # Publishers and Subscribers
        self.publisher = rospy.Publisher('im_atom', AtomMsg)
        rospy.Subscriber("gesture_status", StatusMsg, self.status_cb)

    def status_cb(self, status_msg):
        gesture_status_atom = translators.gesture_status_to_atom(status_msg)
        logdebug("Publishing status_msg: {}".format(str(status_msg)))
        self.publisher.publish(gesture_status_atom)

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        loginfo('Shutting down ' + rospy.get_name() + ' node.')
