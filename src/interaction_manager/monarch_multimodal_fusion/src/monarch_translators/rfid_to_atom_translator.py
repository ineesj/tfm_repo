#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from dialog_manager_msgs.msg import AtomMsg
from monarch_msgs.msg import RfidReading as RfidMsg
from monarch_multimodal_fusion import translators

_DEFAULT_NAME = 'rfid_to_atom_translator'
_NODE_PARAMS = ()


class RfidToAtomTranslator():

    ''' Node that receives L{ActionMsg} message, transforms it
        to L{RfidMsg} and publishes it
    '''

    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        # Subscribers
        rospy.Subscriber("rfid_tag", RfidMsg, self.rfid_cb)
        # Publishers
        self.publisher = rospy.Publisher('im_atom', AtomMsg)

    def rfid_cb(self, rfid_msg):
        rfid_atom_msg = translators.rfid_to_atom(rfid_msg)
        logdebug("Publishing rfid_msg: {}".format(str(rfid_msg)))
        self.publisher.publish(rfid_atom_msg)

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        loginfo('Shutting down ' + rospy.get_name() + ' node.')
