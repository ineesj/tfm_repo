#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from collections import deque
import numpy as np

from monarch_multimodal_fusion import (translators, utils)
from dialog_manager_msgs.msg import AtomMsg
from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg

_DEFAULT_NAME = 'touch_to_atom_translator'
_NODE_PARAMS = ()


class TouchToAtomTranslator():

    """Translates CapacitiveSensorsReadings to AtomMsg."""

    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        self.accumulators = (deque([], 5), deque([], 5), deque([], 5),
                             deque([], 5), deque([], 5))

        self.dqs = dict(zip(utils.TOUCH_SENSORS, self.accumulators))

        self.touched = dict(zip(utils.TOUCH_SENSORS,
                                [False, False, False, False, False]))
        self.last_touch_msg = TouchMsg(head=False,
                                       left_shoulder=False, left_arm=False,
                                       right_shoulder=False, right_arm=False)
        # Subscribers
        rospy.Subscriber("cap_sensors", TouchMsg, self.touch_cb)
        # Publishers
        self.publisher = rospy.Publisher('im_atom', AtomMsg)
        self.filtered_pub = rospy.Publisher("body_touched", TouchMsg)

    def lb_filter(self, touch_msg):
        """
        Filter a L{TouchMsg} with a low band filter which averages the
        current touch measure with the previous 5
        """
        tmsg = TouchMsg(header=touch_msg.header)
        for s in touch_msg.__slots__[1:]:
            logdebug("bodypart: {}. Touched: {}"
                     .format(s, getattr(touch_msg, s)))
            self.dqs[s].append(getattr(touch_msg, s))
            logdebug("DQ[{}]: {}".format(s, self.dqs[s]))
            logdebug("Touched Dict (PRE_FILTER): {}".format(self.touched))
            self.touched[s] = np.median(self.dqs[s]) > utils._TOUCH_THRESHOLD
            logdebug("Touched Dict (POST_FILTER): {}".format(self.touched))
            setattr(tmsg, s, self.touched[s])
        logdebug("\nTouch Msg: {}\n".format(tmsg))
        return tmsg

    def touch_cb(self, touch_msg):
        """ Tranlsate incomming TouchMsg if the user has touched the robot. """
        filtered_msg = self.lb_filter(touch_msg)
        touch_atom_msg = translators.touch_to_atom(filtered_msg)
        #if utils.is_touched(filtered_msg):
        if utils.touch_change(filtered_msg, self.last_touch_msg):
            self.filtered_pub.publish(filtered_msg)
            self.publisher.publish(touch_atom_msg)
            self.last_touch_msg = filtered_msg

    def run(self):
        rospy.spin()



    def shutdown(self):
        """ Closes the node """
        loginfo('Shutting down ' + rospy.get_name() + ' node.')
