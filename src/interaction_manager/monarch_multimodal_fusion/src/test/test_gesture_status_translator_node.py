#!/usr/bin/env python


# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.


PKG = 'monarch_multimodal_fusion'
NNAME = 'test_gesture_status_translator_node'
from itertools import chain

import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_msgs.msg import InteractionExecutorStatus as StatusMsg
from dialog_manager_msgs.msg import (AtomMsg, VarSlot)


class TestGestureStatusTranslatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestGestureStatusTranslatorNode, self).__init__(*args)
        # Publishers and Subscribers
        rospy.init_node(NNAME)
        rospy.Subscriber('im_atom', AtomMsg, self.atom_cb)
        self.publisher = rospy.Publisher('gesture_status', StatusMsg)

    def setUp(self):
        modename = 'head'
        status = 'OK'
        self.status_msg = StatusMsg(modename, status)
        self.def_slots = [VarSlot(name="type", val='gesture_status'),
                          VarSlot(name="subtype", val='user'),
                          VarSlot(name="timestamp", val="_NO_VALUE_",
                                  type="number"),
                          VarSlot(name="consumed", val="false", type="string")]
        self.gs_slots = \
            [VarSlot(name="mode_name", val=modename, type="string"),
             VarSlot(name="status", val=status, type="string")]

        self.status_atom = AtomMsg(list(chain(self.def_slots, self.gs_slots)))

    def tearDown(self):
        pass

    def atom_cb(self, atom_msg):
        self.assertEqual(self.status_atom, atom_msg)

    def test_touch_node(self):
        self.publisher.publish(self.status_msg)


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, 'test_touch_translator_node',
                   TestGestureStatusTranslatorNode, sysargs=None)
                   # sysargs=['--cov'])
