#!/usr/bin/env python


# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.


PKG = 'monarch_multimodal_fusion'
NNAME = 'test_user_command_aggregator_node'

import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_aggregators import utils as aggutils

from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg
from monarch_msgs.msg import KeyValuePairArray as KVPA


def generate_touch_msgs():
    yield TouchMsg(head=True)
    yield TouchMsg(left_arm=True)
    yield TouchMsg(right_arm=True)


class TestTouchToCommandAggregatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestTouchToCommandAggregatorNode, self).__init__(*args)
        # Publishers and Subscribers
        rospy.init_node(NNAME)
        rospy.Subscriber('user_command', KVPA, self.command_cb)
        self.publisher = rospy.Publisher('cap_sensors', TouchMsg)

    def setUp(self):
        self.touch_msgs = generate_touch_msgs()
        self.commands = iter([aggutils._ABORT_CMD,
                             aggutils._TAKE_ME_TO_ROOM_CMD,
                             aggutils._FOLLOW_ME_CMD])

    def tearDown(self):
        pass

    def command_cb(self, cmd_msg):
        self.assertEqual(next(self.commands), cmd_msg)

    def test_touch_node(self):
        for touch in self.touch_msgs:
            self.publisher.publish(touch)
            rospy.sleep(0.2)


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, 'test_touch_to_cmd_aggregator_node',
                   TestTouchToCommandAggregatorNode, sysargs=None)
                   # sysargs=['--cov'])
