#!/usr/bin/python

PKG = 'monarch_multimodal_fusion'
import unittest
from monarch_multimodal_fusion import utils
from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg


class TestTouchUtils(unittest.TestCase):
    def __init__(self, *args):
        super(TestTouchUtils, self).__init__(*args)

    def setUp(self):
        self.str_touch = 'head|left_shoulder|right_shoulder|left_arm|right_arm'
        self.str_some = 'head'

        self.not_touched = TouchMsg(None,
                                    *[False for _ in TouchMsg.__slots__[1:]])
        self.touched = TouchMsg(None, *[True for _ in TouchMsg.__slots__[1:]])
        self.some_touched = TouchMsg(None, True,
                                     *[False for _ in TouchMsg.__slots__[2:]])

    def tearDown(self):
        pass

    def test_is_touched_returns_true_when_any_part_is_touched(self):
        self.assertEqual(False, utils.is_touched(TouchMsg()))
        self.assertEqual(False, utils.is_touched(self.not_touched))
        self.assertEqual(True, utils.is_touched(self.some_touched))
        self.assertEqual(True, utils.is_touched(self.touched))

    def test_get_body_parts_returns_all_but_header(self):
        self.assertEqual(self.touched.__slots__[1:],
                         utils.get_body_parts(self.touched))

    def test_get_touched_returns_touched_parts(self):
        self.assertEqual([], list(utils.get_touched(self.not_touched)))
        self.assertEqual(['head'], list(utils.get_touched(self.some_touched)))
        self.assertEqual(TouchMsg.__slots__[1:],
                         list(utils.get_touched(self.touched)))

    def test_get_touched_as_string_converts_touched_to_string(self):
        # Unless this is a trivial function I test it anyway
        self.assertEqual("", utils.get_touched_as_string(self.not_touched))
        self.assertEqual(self.str_touch,
                         utils.get_touched_as_string(self.touched))
        self.assertEqual(self.str_some,
                         utils.get_touched_as_string(self.some_touched))


class TestStringUtils(unittest.TestCase):
    def __init__(self, *args):
        super(TestStringUtils, self).__init__(*args)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_str_from_list(self):
        l = ['one', 'two', 'three']
        self.assertEqual(utils.str_from_list(l), 'one|two|three')
        self.assertEqual(utils.str_from_list(l, separator='.'), 'one.two.three')

    def test_string_to_list(self):
        s = 'head|eyes|mouth'
        self.assertEqual(utils.string_to_list(s), ['head', 'eyes', 'mouth'])
        s = 'head;eyes;mouth'
        self.assertEqual(utils.string_to_list(s, separator=';'),
                         ['head', 'eyes', 'mouth'])

    def test_str_to_bool(self):
        self.assertEqual(utils.str_to_bool('true'),  True)
        self.assertEqual(utils.str_to_bool('True'),  True)
        self.assertEqual(utils.str_to_bool('false'),  False)
        self.assertEqual(utils.str_to_bool('False'),  False)


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(PKG, 'test_touch_utils', TestTouchUtils)
    rosunit.unitrun(PKG, 'test_name', TestStringUtils)
