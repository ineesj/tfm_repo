#!/usr/bin/python
"""
@author: Victor Gonzalez Pacheco
@date: 2015-02
"""

from __future__ import division
PKG = 'monarch_multimodal_fusion'
import roslib
roslib.load_manifest(PKG)

import unittest
from itertools import chain

from monarch_multimodal_fusion import translators
from std_msgs.msg import Bool
from monarch_msgs.msg import RfidReading as RfidMsg
from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg
from monarch_msgs.msg import InteractionExecutorStatus as StatusMsg
# from monarch_msgs.msg import KeyValuePairArray as KVPAMsg
# from monarch_msgs.msg import KeyValuePair as PMsg
from monarch_msgs_utils import key_value_pairs as kvpa
from dialog_manager_msgs.msg import AtomMsg, VarSlot


class TestGenerateDefaultSlots(unittest.TestCase):
    def __init__(self, *args):
        super(TestGenerateDefaultSlots, self).__init__(*args)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_generate_default_slots(self):
        _atype = 'my_type'
        _asubtype = 'my_subype'
        slots = translators.generate_default_slots(_atype, _asubtype)
        self.assertEqual(next(slots), VarSlot(name='type', val=_atype))
        self.assertEqual(next(slots), VarSlot(name='subtype', val=_asubtype))
        self.assertEqual(next(slots), VarSlot(name="timestamp",
                                              val="_NO_VALUE_", type="number"))
        self.assertEqual(next(slots), VarSlot(name="consumed",
                                              val="false", type="string"))

    def test_generate_default_slots_with_empty_data(self):
        slots = translators.generate_default_slots()
        self.assertEqual(next(slots), VarSlot(name='type', val='_NO_VALUE_'))
        self.assertEqual(next(slots), VarSlot(name='subtype', val='user'))
        self.assertEqual(next(slots), VarSlot(name="timestamp",
                                              val="_NO_VALUE_", type="number"))
        self.assertEqual(next(slots), VarSlot(name="consumed",
                                              val="false", type="string"))


class TestRFIDTranslator(unittest.TestCase):
    """Tests"""

    def __init__(self, *args):
        super(TestRFIDTranslator, self).__init__(*args)
        pass

    def setUp(self):
        self.VARSLOTS = [VarSlot(name="type", val='rfid'),
                         VarSlot(name="subtype", val='user'),
                         VarSlot(name="timestamp",
                                 val="_NO_VALUE_", type="number"),
                         VarSlot(name="consumed", val="false", type="string"),
                         VarSlot(name='tag_id', val='1', type='number')]
        self.ATOM = AtomMsg(varslots=self.VARSLOTS)

        self.RFID = RfidMsg(tag_id=1)

    def tearDown(self):
        pass

    def test_translator_empty_msg(self):
        self.assertEqual(AtomMsg(), translators.rfid_to_atom(None))

    def test_translator(self):
        self.assertEqual(self.ATOM, translators.rfid_to_atom(self.RFID))


def _make_touch_atom(touched):
    ''' Helper function to create touch Atoms in the tests '''
    def_slots = [VarSlot(name="type", val='touch'),
                 VarSlot(name="subtype", val='user'),
                 VarSlot(name="timestamp", val="_NO_VALUE_", type="number"),
                 VarSlot(name="consumed", val="false", type="string")]

    is_touched = int(bool(touched))
    slots = [VarSlot(name="is_touched", val=str(is_touched), type="number"),
             VarSlot(name="body_part_touched", val=touched, type="string")]
    return AtomMsg(varslots=list(chain(def_slots, slots)))


class TestTouchTranslator(unittest.TestCase):
    """Tests for the touch translator"""

    def __init__(self, *args):
        super(TestTouchTranslator, self).__init__(*args)
        pass

    def setUp(self):
        self.str_touch = 'head|left_shoulder|right_shoulder|left_arm|right_arm'
        self.str_some = 'head'

        self.not_touched_atom = _make_touch_atom('')
        self.some_touched_atom = _make_touch_atom(self.str_some)
        self.touched_atom = _make_touch_atom(self.str_touch)

        self.not_touched = TouchMsg(None,
                                    *[False for _ in TouchMsg.__slots__[1:]])
        self.touched = TouchMsg(None, *[True for _ in TouchMsg.__slots__[1:]])
        self.some_touched = TouchMsg(None, True,
                                     *[False for _ in TouchMsg.__slots__[2:]])

    def tearDown(self):
        pass

    def test_touch_translator_empty_msg(self):
        self.assertEqual(self.not_touched_atom,
                         translators.touch_to_atom(TouchMsg()))

    def test_touch_translator(self):
        self.assertEqual(self.not_touched_atom,
                         translators.touch_to_atom(self.not_touched))
        self.assertEqual(self.some_touched_atom,
                         translators.touch_to_atom(self.some_touched))
        self.assertEqual(self.touched_atom,
                         translators.touch_to_atom(self.touched))


class TestPersonDetectedTranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestPersonDetectedTranslator, self).__init__(*args)

    def setUp(self):
        self.detected = Bool(True)
        self.not_detected = Bool(False)

        def_slots = [VarSlot(name="type", val='person_detected'),
                     VarSlot(name="subtype", val='user'),
                     VarSlot(name="timestamp", val="_NO_VALUE_", type="number"),
                     VarSlot(name="consumed", val="false", type="string")]

        self.detected_slot = VarSlot(name='person_detected', val='true',
                                     type='string')
        self.not_detected_slot = VarSlot(name='person_detected', val='false',
                                         type='string')

        self.detected_atom = \
            AtomMsg(list(chain(def_slots, [self.detected_slot])))
        self.not_detected_atom = \
            AtomMsg(varslots=(list(chain(def_slots, [self.not_detected_slot]))))

    def tearDown(self):
        pass

    def test_generate_person_detected_slots(self):
        slots = list(translators.generate_person_detected_slots(self.detected))
        self.assertEqual(slots, [self.detected_slot])
        slots = \
            list(translators.generate_person_detected_slots(self.not_detected))
        self.assertEqual(slots, [self.not_detected_slot])

    def test_person_detected_to_atom_emtpy_msg(self):
        self.assertEqual(AtomMsg(), translators.person_detected_to_atom(None))

    def test_person_detected_to_atom(self):
        self.assertEqual(self.detected_atom,
                         translators.person_detected_to_atom(self.detected))
        self.assertEqual(self.not_detected_atom,
                         translators.person_detected_to_atom(self.not_detected))


class TestKVPATranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestKVPATranslator, self).__init__(*args)

    def setUp(self):
        data = {'1': 'one', '2': 'two', '3': 'three', '4': 'four'}
        self.kvpa_msg = kvpa.from_dict(data)
        self.def_slots = [VarSlot(name="type", val='kvpa'),
                          VarSlot(name="subtype", val='user'),
                          VarSlot(name="timestamp", val="_NO_VALUE_",
                                  type="number"),
                          VarSlot(name="consumed", val="false", type="string")]
        self.kvpa_slots = [VarSlot(name="1", val="one", type="string"),
                           VarSlot(name="2", val="two", type="string"),
                           VarSlot(name="3", val="three", type="string"),
                           VarSlot(name="4", val="four", type="string")]
        self.kvpa_atom = AtomMsg(list(chain(self.def_slots, self.kvpa_slots)))

    def tearDown(self):
        pass

    def test_generate_kvpa_slots(self):
        slots = list(translators.generate_kvpa_slots(self.kvpa_msg))
        self.assertEqual(slots, list(self.kvpa_slots))

    def test_kvpa_to_atom_empty_msg(self):
        self.assertEqual(translators.kvpa_to_atom(None), AtomMsg())

    def test_kvpa_to_atom(self):
        self.assertEqual(translators.kvpa_to_atom(self.kvpa_msg),
                         self.kvpa_atom)


class TestUserCommandTranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestUserCommandTranslator, self).__init__(*args)

    def setUp(self):
        command1 = {'command': 'follow_me'}
        command2 = {'command': 'take_me_to',
                    'destination': 'room_15'}
        self.c1_msg = kvpa.from_dict(command1)
        self.c2_msg = kvpa.from_dict(command2)
        self.def_slots = [VarSlot(name="type", val='command_received'),
                          VarSlot(name="subtype", val='user'),
                          VarSlot(name="timestamp", val="_NO_VALUE_",
                                  type="number"),
                          VarSlot(name="consumed", val="false", type="string")]
        self.c1_slots = \
            [VarSlot(name="command", val="follow_me", type="string")]
        self.c2_slots = \
            [VarSlot(name="command", val="take_me_to", type="string"),
             VarSlot(name="destination", val="room_15", type="string")]
        self.c1_atom = AtomMsg(list(chain(self.def_slots, self.c1_slots)))
        self.c2_atom = AtomMsg(list(chain(self.def_slots, self.c2_slots)))

    def tearDown(self):
        pass

    def test_user_command_to_atom_empty_msg(self):
        self.assertEqual(AtomMsg(), translators.user_command_to_atom(None))

    def test_user_command_to_atom(self):
        self.assertEqual(self.c1_atom,
                         translators.user_command_to_atom(self.c1_msg))
        self.assertEqual(self.c2_atom,
                         translators.user_command_to_atom(self.c2_msg))


class TestGestureStatusTranslator(unittest.TestCase):
    def __init__(self, *args):
        super(TestGestureStatusTranslator, self).__init__(*args)

    def setUp(self):
        modename = 'head'
        status = 'OK'
        self.status_msg = StatusMsg(modename, status)
        self.def_slots = [VarSlot(name="type", val='gesture_status'),
                          VarSlot(name="subtype", val='user'),
                          VarSlot(name="timestamp", val="_NO_VALUE_",
                                  type="number"),
                          VarSlot(name="consumed", val="false", type="string")]
        self.gs_slots = \
            [VarSlot(name="expression_name", val=modename, type="string"),
             VarSlot(name="status", val=status, type="string")]

        self.status_atom = AtomMsg(list(chain(self.def_slots, self.gs_slots)))

    def tearDown(self):
        pass

    def test_generate_gesture_status_slots(self):
        slots = list(translators.generate_gesture_status_slots(self.status_msg))
        self.assertEqual(slots, list(self.gs_slots))

    def test_gesture_status_to_atom_empty_msg(self):
        self.assertEqual(AtomMsg(), translators.gesture_status_to_atom(None))

    def test_gesture_status_to_atom(self):
        self.assertEqual(self.status_atom,
                         translators.gesture_status_to_atom(self.status_msg))


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(PKG, 'test_generate_def_slots', TestGenerateDefaultSlots)
    rosunit.unitrun(PKG, 'test_rfid_translator', TestRFIDTranslator)
    rosunit.unitrun(PKG, 'test_touch_translator', TestTouchTranslator)
    rosunit.unitrun(PKG, 'test_person_detected_translator',
                    TestPersonDetectedTranslator)
    rosunit.unitrun(PKG, 'test_kvpa_translator', TestKVPATranslator)
    rosunit.unitrun(PKG, 'test_user_command_translator',
                    TestUserCommandTranslator)
    rosunit.unitrun(PKG, 'test_gesture_status_translator',
                    TestGestureStatusTranslator)
