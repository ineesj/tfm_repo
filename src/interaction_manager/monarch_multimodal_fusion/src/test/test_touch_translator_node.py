#!/usr/bin/env python


# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.


PKG = 'monarch_multimodal_fusion'
NNAME = 'test_touch_translator_node'
from itertools import chain

import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg
from dialog_manager_msgs.msg import (AtomMsg, VarSlot)


def _make_touch_atom(touched):
    ''' Helper function to create touch Atoms in the tests '''
    def_slots = [VarSlot(name="type", val='touch'),
                 VarSlot(name="subtype", val='user'),
                 VarSlot(name="timestamp", val="_NO_VALUE_", type="number"),
                 VarSlot(name="consumed", val="false", type="string")]

    is_touched = int(bool(touched))
    slots = [VarSlot(name="is_touched", val=str(is_touched), type="number"),
             VarSlot(name="body_part_touched", val=touched, type="string")]
    return AtomMsg(varslots=list(chain(def_slots, slots)))


class TestTouchTranslatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestTouchTranslatorNode, self).__init__(*args)
        # Publishers and Subscribers
        rospy.init_node(NNAME)
        rospy.Subscriber('im_atom', AtomMsg, self.atom_cb)
        self.publisher = rospy.Publisher('cap_sensors', TouchMsg)

    def setUp(self):
        self.str_touch = 'head|left_shoulder|right_shoulder|left_arm|right_arm'
        self.str_some = 'head'

        self.not_touched_atom = _make_touch_atom('')
        self.some_touched_atom = _make_touch_atom(self.str_some)
        self.touched_atom = _make_touch_atom(self.str_touch)

        self.not_touched = TouchMsg(None,
                                    *[False for _ in TouchMsg.__slots__[1:]])
        self.touched = TouchMsg(None, *[True for _ in TouchMsg.__slots__[1:]])
        self.some_touched = TouchMsg(None, True,
                                     *[False for _ in TouchMsg.__slots__[2:]])

        self.msgs = iter([self.not_touched, self.touched, self.some_touched])
        self.atoms = iter([self.not_touched_atom, self.touched_atom,
                           self.some_touched_atom])

    def tearDown(self):
        pass

    def atom_cb(self, atom_msg):
        self.assertEqual(next(self.atoms), atom_msg)

    def test_touch_node(self):
        self.publisher.publish(self.not_touched)
        self.publisher.publish(self.touched)
        self.publisher.publish(self.some_touched)


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, 'test_touch_translator_node',
                   TestTouchTranslatorNode, sysargs=None)
                   # sysargs=['--cov'])
