#!/usr/bin/env python


# :version:      0.1.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.


PKG = 'monarch_multimodal_fusion'
NNAME = 'test_rfid_translator_node'
import roslib
roslib.load_manifest(PKG)
import rospy
import unittest

from monarch_msgs.msg import RfidReading as RfidMsg
from dialog_manager_msgs.msg import (AtomMsg, VarSlot)


VARSLOTS = [VarSlot(name="type", val='rfid'),
            VarSlot(name="subtype", val='user'),
            VarSlot(name="timestamp", val="_NO_VALUE_", type="number"),
            VarSlot(name="consumed", val="false", type="string"),
            VarSlot(name='tag_id', val='1', type='number')]
RFID_ATOM = AtomMsg(varslots=VARSLOTS)

RFID_MSG = RfidMsg(tag_id=1)


class TestRFIDTranslatorNode(unittest.TestCase):
    def __init__(self, *args):
        super(TestRFIDTranslatorNode, self).__init__(*args)
        # Publishers and Subscribers
        rospy.init_node(NNAME)
        rospy.Subscriber('im_atom', AtomMsg, self.atom_cb)
        self.publisher = rospy.Publisher('rifd_tag', RfidMsg)

    def atom_cb(self, atom_msg):
        self.assertEqual(RFID_ATOM, atom_msg)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_rfid_node(self):
        self.publisher.publish(RFID_MSG)


if __name__ == '__main__':
    import rostest
    rostest.rosrun(PKG, 'test_RFID_translator_node',
                   TestRFIDTranslatorNode, sysargs=['--cov'])
                   # sysargs=['--cov'])
