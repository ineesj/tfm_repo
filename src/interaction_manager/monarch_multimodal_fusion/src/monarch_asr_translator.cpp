#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <pthread.h>
#include "ros/ros.h"

#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include <ros/package.h>
#include "boost/date_time/posix_time/posix_time.hpp" //for avanced time manipulation

#include "dialog_manager_msgs/AtomMsg.h"    //dialog_manager_msgs
#include "dialog_manager_msgs/VarSlot.h"    //dialog_manager_msgs

#include "monarch_msgs/RecognizedSpeech.h"  // MOnarCH's ASR messages

//#include "asr_msgs/recog_results.h"         //asr_msgs
//#include "asr_msgs/attribute_value.h"       //asr_msgs

//#include "src/time/timer.h"                 //for timestamp

using namespace std;


ros::Publisher atom_pub;


/* Just gets current system time in milliseconds 
   @TODO: move this function to an external lib or use queio.h's one
*/
double getSystemTimeMSec(){

  struct timeval cur_time;
  double cur_time_msec, cur_time_msec_usec;

  /* get current time */
  gettimeofday(&cur_time, NULL);
  cur_time_msec = (double)cur_time.tv_sec;
  cur_time_msec_usec = (double)cur_time.tv_usec;
  cur_time_msec = (cur_time_msec*1000.0 + cur_time_msec_usec/1000.0) + 0.5;
  return cur_time_msec;
}

/**
  * This funtion is called when  ASR message is emitted with results. 
  * Converts from ASR ROS message format to AtomMsg format, 
  * ready to be interpreted for iWaki dialog manager.
  */
void AsrOKMessageCallback(const monarch_msgs::RecognizedSpeech::ConstPtr & msg ) {
    ROS_DEBUG ("<== Received ASR results on multimodal_fusion." );
    monarch_msgs::RecognizedSpeech AsrResults = *msg;

    dialog_manager_msgs::AtomMsg atom;
    dialog_manager_msgs::VarSlot varSlot;

    ROS_INFO("__________ ASR OK__________");

    // Internal slots for all recipes
    varSlot.name = "type";
    varSlot.val = "asr_OK";
    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    //varSlot.unique_mask = true;
    atom.varslots.push_back(varSlot);

    varSlot.name = "subtype";
    varSlot.val = "user";
    ROS_DEBUG("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    //varSlot.unique_mask = true;
    atom.varslots.push_back(varSlot);


    varSlot.name = "words";
    varSlot.val = msg->recognized;
    varSlot.type= "string";
    varSlot.unique_mask = true;
    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    atom.varslots.push_back(varSlot);

//    varSlot.name = "numWords";
//    varSlot.val = ""+  boost::lexical_cast<std::string>(msg->numWords);
//    varSlot.type= "number";
//    varSlot.unique_mask = true;
//    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    atom.varslots.push_back(varSlot);

    varSlot.name = "confidence";
    string confidence = boost::lexical_cast<std::string>(msg->confidence);
    varSlot.val = confidence;
    varSlot.type= "number";
    varSlot.unique_mask = true;
    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    atom.varslots.push_back(varSlot);

    // If multiple semantics
//    for (unsigned numSem=0; numSem < AsrResults.semantics.size(); ++numSem){
//        varSlot.name = AsrResults.semantics[numSem].atribute;
//        varSlot.val = AsrResults.semantics[numSem].value;
//        varSlot.type= "string";
//        varSlot.unique_mask = true;
//        ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//        atom.varslots.push_back(varSlot);
//    }

    varSlot.name = "semantic";
    varSlot.val = AsrResults.semantic;
    varSlot.type= "string";
    varSlot.unique_mask = true;
    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    atom.varslots.push_back(varSlot);


//    varSlot.name = "nameUserSpeaking";
//    string nameUser = boost::lexical_cast<std::string>(msg->nameUserSpeaking);
//    varSlot.val = nameUser;
//    varSlot.type= "string";
//    varSlot.unique_mask = true;
//    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    atom.varslots.push_back(varSlot);


    varSlot.name = "timestamp";
    //boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
    //long milliseconds = current_date_microseconds.time_of_day().total_seconds();
    varSlot.val =  boost::lexical_cast<std::string>(getSystemTimeMSec()/1000.0); /* store in seconds */
    varSlot.type= "number";
    varSlot.unique_mask = true;
    ROS_DEBUG("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    atom.varslots.push_back(varSlot);

    varSlot.name = "consumed";
    varSlot.val = "false";
    varSlot.type= "string";
    varSlot.unique_mask = true;
    ROS_DEBUG("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
    atom.varslots.push_back(varSlot);

    ROS_INFO( "_____________________________________");

    //publish in format AtomMsg the ros message received for this input mode
    atom_pub.publish(atom);
    ROS_DEBUG ("==> Published Atom with   ASR results on multimodal_fusion\n" );
}


///**
//  * This funtion is called when  ASR message is emitted with NO results.
//  * Converts from ASR ROS message format
//  * to AtomMsg format, ready to be interpreted for iWaki dialog manager.
//  */
//void AsrNoResultsMessageCallback(const  std_msgs::Int16::ConstPtr& msg) {
//    ROS_DEBUG ("<== Received NO results for  ASR on multimodal_fusion" );

//    dialog_manager_msgs::AtomMsg atom;
//    dialog_manager_msgs::VarSlot varSlot;


//    ROS_INFO( "__________ ASR FAIL__________");

//    // Internal slots for all recipes
//    varSlot.name = "type";
//    varSlot.val = "asr_Fail";
//    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    //varSlot.unique_mask = true;
//    atom.varslots.push_back(varSlot);

//    varSlot.name = "subtype";
//    varSlot.val = "user";
//    ROS_INFO("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    //varSlot.unique_mask = true;
//    atom.varslots.push_back(varSlot);

//    varSlot.name = "timestamp";
//    //boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
//    //long milliseconds = current_date_microseconds.time_of_day().total_seconds();
//    varSlot.val =  boost::lexical_cast<std::string>(getSystemTimeMSec()/1000.0); /* store in seconds */
//    varSlot.type= "number";
//    varSlot.unique_mask = true;
//    ROS_DEBUG("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    atom.varslots.push_back(varSlot);

//    varSlot.name = "consumed";
//    varSlot.val = "false";
//    varSlot.type= "string";
//    varSlot.unique_mask = true;
//    ROS_DEBUG("%s = %s",varSlot.name.c_str(),varSlot.val.c_str() );
//    atom.varslots.push_back(varSlot);

//    ROS_INFO( "_____________________________________");

//    //publish in format AtomMsg the ros message received for this input mode
//    atom_pub.publish(atom);
//    ROS_DEBUG ("==> Published Atom with   ASR NO results on multimodal_fusion\n" );
//}



/** Main funtion */
int main(int argc, char **argv){

    ros::init(argc, argv, "_asr_translator");  //enroll the name of the node
    ros::NodeHandle node;   //node to perform communication

    ROS_INFO("Started node _asr_translator.");

    //Publisher for publish AtomMsgs (dialog_manager_msgs)
    atom_pub = node.advertise<dialog_manager_msgs::AtomMsg>("im_atom", 0);

    //perform subscripton to messages and events
    ros::Subscriber s1 = node.subscribe( "audio/speech_rec", 1000, AsrOKMessageCallback );
//    ros::Subscriber s2 = node.subscribe( "REC_FAIL", 1000, AsrNoResultsMessageCallback );

    ros::AsyncSpinner spinner(2);
    spinner.start();
    ros::waitForShutdown();

    return 0;
}


