#!/usr/bin/env python

""" Monarch Multimodal Fusion Translator Functions. """

# import roslib
# roslib.load_manifest('monarch_multimodal_fusion')
# import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)
from itertools import chain
from monarch_multimodal_fusion import utils
# from dialog_manager_msgs.msg import AtomMsg, VarSlot
from dialog_manager_msgs.msg import (VarSlot, AtomMsg)
# from monarch_msgs_utils import key_value_pairs as kvpa


###############################################################################
# Slot Generators
###############################################################################
def generate_default_slots(atom_type='_NO_VALUE_', atom_subtype='user'):
    """
    Generate the slots that are common to every Atom.

    :param str atom_type: Type of the atom
    :param str atom_subtype: Subtype of te atom. Defaults to 'user'
    :return: Yields the default VarSlots
    :rtype: VarSlot
    """
    yield VarSlot(name="type", val=atom_type)
    yield VarSlot(name="subtype", val=atom_subtype)
    yield VarSlot(name="timestamp", val="_NO_VALUE_", type="number")
    yield VarSlot(name="consumed", val="false", type="string")


def slot_from_string(string, slotname, slottype='string'):
    """
    Produce a VarSlot from a string.
    Example:
        >>> next(slot_from_string('the_slot_value', 'the_slot_name'))
        name: the_slot_name
        relation: ''
        val: the_slot_value
        type: string
        unique_mask: False
    """
    yield VarSlot(name=slotname, val=string, type=slottype)


def slot_from_num(num, slotname, slottype='number'):
    """
    Produce a VarSlot from an num.
    It accepts int and float.
    Example:
        >>> next(slot_from_num(123, 'an_int_slot'))
        name: an_int_slot
        relation: ''
        val: 123
        type: number
        unique_mask: False
        >>> next(slot_from_num(0.12345, 'a_float_slot'))
        name: a_float_slot
        relation: ''
        val: 0.12345
        type: number
        unique_mask: False
    """
    yield VarSlot(name=slotname, val=str(num), type=slottype)


def slot_from_iter(arr, slotname, slottype='string'):
    """Produce a Varslot from an iterable.
    Example:
    >>> next(slot_from_iter([1, 2, 3, 4], 'an_array_slot'))
    name: an_array_slot
    relation: ''
    val: 1|2|3|4
    type: string
    unique_mask: False
    >>> strings = ['an_str', 'another_str', 'third_str']
    >>> next(slot_from_iter(strings, slotname='an_array_slot_with_strings'))
    name: an_array_slot_with_strings
    relation: ''
    val: an_str|another_str|third_str
    type: string
    unique_mask: False
    """
    slot_value = '|'.join(str(item) for item in arr)
    yield VarSlot(name=slotname, val=slot_value, type=slottype)


def slot_from_bool(b, slotname, slottype='string'):
    """
    Produce a VarSlot from a boolean.
    Example:
        >>> next(slot_from_bool(False, 'a_boolean_slot'))
        name: a_boolean_slot
        relation: ''
        val: false
        type: string
        unique_mask: False
    """
    yield VarSlot(name=slotname, val=str(b).lower(), type=slottype)


basic_type_parsers = {'bool': slot_from_bool,
                      'string': slot_from_string,
                      'int16': slot_from_num,
                      'int32': slot_from_num,
                      'int64': slot_from_num,
                      'float16': slot_from_num,
                      'float32': slot_from_num,
                      'float64': slot_from_num}


def get_slot_parser(slot_type):
    """Return a slot parser function associated to slot_type."""
    try:
        return basic_type_parsers[slot_type]
    except KeyError:
        return slot_from_iter


def _get_msg_fields(msg):
    """Return all message field names and types except its header."""
    try:
        getattr(msg, 'header')
    except AttributeError:
        return zip(msg.__slots__, msg._slot_types)
    return zip(msg.__slots__[1:], msg._slot_types[1:])


def msg_to_slots(msg):
    """Convert msg fields to VarSlots.
    At this point only bool, string, int*, and float* types are supported.
    Examples:
    --------
    >>> from std_msgs.msg import Bool
    >>> list(msg_to_slots(Bool()))
    [name: data
    relation: ''
    val: false
    type: string
    unique_mask: False]
    >>> from std_msgs.msg import ColorRGBA
    >>> list(msg_to_slots(ColorRGBA()))
    [name: r
    relation: ''
    val: 0.0
    type: number
    unique_mask: False, name: g
    relation: ''
    val: 0.0
    type: number
    unique_mask: False, name: b
    relation: ''
    val: 0.0
    type: number
    unique_mask: False, name: a
    relation: ''
    val: 0.0
    type: number
    unique_mask: False]

    It is also possible to parse messages that contain array fields:

    >>> from sensor_msgs.msg import Joy
    >>> joy_msg = Joy(axes=[1.0,2.1,3.33,4.0], buttons=[1, 2, 3])
    >>> axes_varslot, buttons_varslot = list(msg_to_slots(joy_msg))
    >>> print axes_varslot
    name: axes
    relation: ''
    val: 1.0|2.1|3.33|4.0
    type: string
    unique_mask: False
    >>> print buttons_varslot
    name: buttons
    relation: ''
    val: 1|2|3
    type: string
    unique_mask: False
    """
    for sname, stype in _get_msg_fields(msg):
        parser = get_slot_parser(stype)
        yield next(parser(msg.__getattribute__(sname), slotname=sname))


## Specific slot translators ###################################################

def generate_rfid_slots(rfid_msg):
    """ Yield the slots of the atom."""
    yield VarSlot(name="tag_id", val=str(rfid_msg.tag_id), type="number")


def generate_touch_slots(touch_msg):
    """ Yield the slots of the atom. """
    istouched = utils.is_touched(touch_msg)
    touched_str = utils.get_touched_as_string(touch_msg)
    yield VarSlot(name="is_touched", val=str(int(istouched)), type="number")
    yield VarSlot(name="body_part_touched", val=touched_str, type="string")


def generate_person_detected_slots(detected_msg):
    yield VarSlot(name='person_id', val=str(detected_msg.person_id),
                  type='string')
    yield VarSlot(name='person_detected', val=str(detected_msg.data).lower(),
                  type='string')
    # yield VarSlot(name='user_id', val)


def generate_kvpa_slots(kvpa_msg):
    for pair in kvpa_msg.array:
        yield VarSlot(name=str(pair.key), val=str(pair.value), type='string')


def generate_gesture_status_slots(status_msg):
    yield VarSlot(name="expression_name", val=status_msg.expression_name,
                  type="string")
    yield VarSlot(name="status", val=status_msg.status, type="string")


# def generate_user_info_slots(uinfo):
#     """ Yield the slots of the atom from a user_info dict. """
#     yield VarSlot(name="uid", val=uinfo['uid'], type="number")
#     yield VarSlot(name="name", val=uinfo['name'], type="string")
#     yield VarSlot(name="surname", val=uinfo['surname'], type="string")
#     yield VarSlot(name="welcome_msg", val=uinfo['welcome_msg'], type="string")
#     yield VarSlot(name="non_verbal_sound",
#                   val=uinfo['non_verbal_sound'],
#                   type="string")
#     yield VarSlot(name="greetings_expression",
#                   val=uinfo["greetings_expression"],
#                   type="string")
#     yield VarSlot(name="role", val=uinfo['role'], type="string")
#     yield VarSlot(name="gender", val=uinfo['gender'], type="string")

###############################################################################
# Atom Generators
###############################################################################


def to_atom_msg(msg, generator_func, atom_name, atom_subtype='user'):
    """
    Return an AtomMsg from a msg instance and a generator function.

    :param msg: The message to translate
    :param function generator_func: The function to translate the message
    :param str atom_name: Name of the atom
    :param str atom_subtype: (default: 'user') Name of the atom subtype
    :return: The msg converted to an AtomMsg
    :rtype: AtomMsg
    """
    if not msg:
        return AtomMsg()
    def_slots = generate_default_slots(atom_name, atom_subtype)
    varslots = chain(def_slots, generator_func(msg))
    atom = AtomMsg(varslots=list(varslots))
    utils.log_atom(atom, logger=logdebug)
    return atom
    # return AtomMsg(varslots=list(varslots))


def generic_atom_translator(msg, atom_name,
                            atom_subtype='user', *args, **kwargs):
    """A generic atom translator that returns an atom from a ros msg instance.
    It supports translation for msgs with simple types such as:
        -bool, string, int*, and float*
    and their array versions:
        -bool[], string[], int*[], and float*[]
    Example:
    >>> from sensor_msgs.msg import Joy
    >>> joy_msg = Joy(axes=[1.0,2.1,3.33,4.0], buttons=['a', 'b', 'd'])
    >>> generic_atom_translator(joy_msg, atom_name='joy_atom')
    varslots: 
      - 
        name: type
        relation: ''
        val: joy_atom
        type: ''
        unique_mask: False
      - 
        name: subtype
        relation: ''
        val: user
        type: ''
        unique_mask: False
      - 
        name: timestamp
        relation: ''
        val: _NO_VALUE_
        type: number
        unique_mask: False
      - 
        name: consumed
        relation: ''
        val: false
        type: string
        unique_mask: False
      - 
        name: axes
        relation: ''
        val: 1.0|2.1|3.33|4.0
        type: string
        unique_mask: False
      - 
        name: buttons
        relation: ''
        val: a|b|d
        type: string
        unique_mask: False
    """
    return to_atom_msg(msg, msg_to_slots, atom_name=atom_name,
                       atom_subtype='user', *args, **kwargs)


def rfid_to_atom(rfid_msg):
    """ Returns an :py:class:`AtomMsg` from a py:class:`RFidReading` msg

        :param .:py:class:`monarch_msgs.msg.RFidReading` rfid_msg:
            The RFId message to translate
        :return: The input message translated to an AtomMsg
        :rtype: :py:class:`dialog_manager_msgs.msg.AtomMsg`
    """
    return to_atom_msg(rfid_msg, generate_rfid_slots, 'rfid')


def touch_to_atom(touch_msg):
    """ Produces an L{AtomMsg} from a TouchMsg """
    return to_atom_msg(touch_msg, generate_touch_slots, 'touch')


# def person_detected_to_atom(detected_msg):
#     return to_atom_msg(detected_msg, generate_person_detected_slots,
#                        'person_detected')

def person_detected_to_atom(detected_msg):
    """ Translate a messages from person_detected topic. """
    return to_atom_msg(detected_msg, generate_kvpa_slots, 'person_detected')


def kvpa_to_atom(kvpa_msg, atom_name='kvpa'):
    """ Returns an :py:class:`AtomMsg` from a :py:class:`KeyValuePairArray` msg

        :return: The input message translated to an AtomMsg
        :rtype: :py:class:`dialog_manager_msgs.msg.AtomMsg`
    """
    return to_atom_msg(kvpa_msg, generate_kvpa_slots, atom_name=atom_name)


def user_command_to_atom(command_msg):
    """ Returns an :py:class`AtomMsg` from a command message. """
    return to_atom_msg(command_msg, generate_kvpa_slots, 'command_received')


def gesture_status_to_atom(status_msg):
    """ Returns an :py:class:`AtomMsg` from a GestureStatus message
    """
    return to_atom_msg(status_msg, generate_gesture_status_slots,
                       'gesture_status')
