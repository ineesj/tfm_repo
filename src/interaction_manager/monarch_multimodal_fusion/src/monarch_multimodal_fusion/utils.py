#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
# import rospy
# from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from itertools import ifilter
from functools import partial
import colorama

from rospy import loginfo

from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg

# _DEFAULT_NAME = 'touch_to_atom_translator'
# _NODE_PARAMS = ()

# __BODY_PARTS = ('head', 'left_shoulder', 'right_shoulder',
#                 'left_arm', 'right_arm')

# _FILTER_LENGTH = 5
#_TOUCH_THRESHOLD = 0.5
_TOUCH_THRESHOLD = 0.9

TOUCH_SENSORS = TouchMsg.__slots__[1:]

## UTILS FOR TOUCH ############################################################


def get_body_parts(msg):
    """Return all fields of the message except the header.

    In the case of a TouchMsg, this means returning all body parts.
    """
    # return [bp for bp in msg.__slots__ if bp != 'header']
    ## not_header = partial(ne, 'header')
    ## return ifilter(not_header, msg.__slots__)
    return msg.__slots__[1:]


def get_touched(touch_msg):
    """Return an iterator containing the body limbs that have been touched.

    Example:

    >>> from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg
    >>> touch_msg = TouchMsg(head=True, left_arm=True)
    >>> list(get_touched(touch_msg))
    ['head', 'left_arm']
    """
    gattr = partial(getattr, touch_msg)
    return ifilter(gattr, get_body_parts(touch_msg))


def get_touched_as_string(touch_msg):
    """Return a string containing the touched body parts separated by '|'

    Args:
        touch_msg (monarch_msgs/CapacitiveSensorsReadings): The touch Msg.

    Example:

        >>> # Assume that head and left_arm are touched
        >>> touch_msg = TouchMsg(head=True, left_arm=True)
        >>> get_touched_as_string(touch_msg)
        'head|left_arm'
    """
    return str_from_list(list(get_touched(touch_msg)))


def is_touched(touch_msg):
    """ Predicate that returns if body has been touched"""
    return any(get_touched(touch_msg))


## STRING UTILS ###############################################################


def str_from_list(lst, separator='|'):
    """Return lst in a string where lst elements are separated by `separator`.

    Example:

        >>> l = ['one', 'two', 'three']
        >>> str_from_list(l)
        'one|two|three'
        >>> str_from_list(l, separator='.')
        'one.two.three'
    """
    return separator.join(lst)


def string_to_list(string, separator='|'):
    """Split a string using a specific separator (default: '|')

    Example:

        >>> s = 'head|eyes|mouth'
        >>> string_to_list(s)
        ['head', 'eyes', 'mouth']
        >>> s = 'head;eyes;mouth'
        >>> string_to_list(s, separator=';')
        ['head', 'eyes', 'mouth']
    """
    return string.split(separator)


def str_to_bool(s):
    """Convert the strings 'true' and 'false' to its corresponding bool values.

    Example:

        >>> str_to_bool('true')
        True
        >>> str_to_bool('True')
        True
        >>> str_to_bool('false')
        False
        >>> str_to_bool('False')
        False
    """
    return s.lower() == 'true'


def is_number(s):
    """
    Return True if s is a number and False otherwise.

    Examples:

    >>> is_number('')
    False
    >>> is_number('hi!')
    False
    >>> is_number('--023, hi')
    False
    >>> is_number('132')
    True
    >>> is_number('-1')
    True
    >>> is_number('0')
    True
    >>> is_number('-0.3')
    True
    >>> is_number('0.0')
    True
    >>> is_number('1.2')
    True
    """
    if s.isdigit():
        return True
    try:
        float(s)
        return True
    except ValueError:
        pass
    return False


## LOGGING UTILS ###############################################################
def_slots = {'timestamp', 'consumed', 'subtype', 'type'}


def colorize(logmsg, color=colorama.Fore.MAGENTA):
    """Colorize a message with green colors."""
    return ''.join([color, logmsg, colorama.Fore.RESET])

red = partial(colorize, color=colorama.Fore.RED)
green = partial(colorize, color=colorama.Fore.GREEN)
blue = partial(colorize, color=colorama.Fore.BLUE)
magenta = partial(colorize, color=colorama.Fore.MAGENTA)
yellow = partial(colorize, color=colorama.Fore.YELLOW)


def log_atom(msg, logger=loginfo, color=blue):
    """
    Log an AtomMsg.

    :param msg AtomMsg: The message to send to logger
    :param logger: The logger function. Default :py:func:rospy.loginfo
    :param color: function to colororize a string
    :type color: function
    """
    dmsg = {varslot.name: varslot for varslot in msg.varslots}
    atom_name = dmsg['type'].val
    logger(color("----------"))
    logger(color("Translated Atom with Type: ") + "{}".format(atom_name))
    logger(color("The Slots of the Atom are:"))
    for k in dmsg.keys():
        if k not in def_slots:
            log_varslot(dmsg[k], logger=logger)
    logger(color("----------"))


def log_varslot(varslot, logger=loginfo, color=blue):
    """
    Log a VarSlot msg.

    :param varslot: The message to send to logger
    :type varslot: VarSlot
    :param logger: The logger function. Default :py:func:rospy.loginfo
    :param color: function to colororize a string
    :type color: function
    """
    logger("{} {} -- {} {} -- {} {}"
           .format(color("Name:"), varslot.name,
                   color("Val:"), varslot.val,
                   color("Type:"), varslot.type))


def touch_change(touch_msg, last_touch_msg):
    """
    Check whether there is a change in the touch state.

    :parm last_touch_msg: Touch message for the last change
    :type last_touch_msg: TouchMsg
    :param touch_msg: Last received touch message
    :type touch_msg: TouchMsg
    """
    if any([touch_msg.head != last_touch_msg.head,
            touch_msg.left_arm != last_touch_msg.left_arm,
            touch_msg.right_arm != last_touch_msg.right_arm,
            touch_msg.left_shoulder != last_touch_msg.left_shoulder,
            touch_msg.right_shoulder != last_touch_msg.right_shoulder]):
        return True
    return False
