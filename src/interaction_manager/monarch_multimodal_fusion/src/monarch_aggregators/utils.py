#!/usr/bin/env python

""" Utils for MOnarCH Aggregators. """

# import roslib
# roslib.load_manifest('monarch_multimodal_fusion')

from copy import deepcopy

# from monarch_msgs_utils import key_value_pairs as kvpa
from geometry_msgs.msg import Point
from monarch_msgs.msg import KeyValuePairArray as KVPA
from monarch_msgs.msg import KeyValuePair as KVP

# from monarch_translators.touch_to_atom_translator import get_touched
from monarch_multimodal_fusion import utils as futils

_ABORT_CMD = KVPA(array=[KVP(key='command', value='abort')])
_FOLLOW_ME_CMD = KVPA(array=[KVP(key='command', value='follow_me')])
_TAKE_ME_TO_ROOM_CMD = KVPA(array=[KVP(key='command', value='take_me_to_room'),
                                   KVP(key='destination', value='corridor')])

_NOT_ALLOWED = KVP(key='allowed', value='False')
TOUCHED_MAPPING = {'head': _ABORT_CMD,
                   'left_arm': _TAKE_ME_TO_ROOM_CMD,
                   'right_arm': _FOLLOW_ME_CMD}


def touch_to_command(touch_msg):
    """ Gets a TouchMessage and returns a KVPA with a command associated to the
        body part that has been touched
    """
    touched = next(futils.get_touched(touch_msg))
    return TOUCHED_MAPPING.get(touched, None)


def set_command_as_rejected(command_msg):
    """ Sets a command as rejected.

        :TODO: At some point I need to decide who decides this
    """
    cmd = deepcopy(command_msg)
    cmd.array.append(_NOT_ALLOWED)
    return cmd


locations = {"LOCATION-CLASSROOM": "classroom",
             "LOCATION-PLAYROOM": "playroom",
             "LOCATION-CORRIDOR": "corridor",
             "LOCATION-ROOM": "dormitory",
             "LOCATION-CANTINA": "cantina"}
places = {"classroom": Point(x=6.75, y=-4.14, z=0.64),
          "playroom": Point(x=2.25, y=-2.24, z=0.96),
          "corridor": Point(x=-1.8, y=0.26, z=0.7),
          "dormitory": Point(x=4.25, y=11.41, z=0.69),
          "cantina": Point(x=1.0, y=5.96, z=0.80)}


def get_place_from_asr(asr_msg):
    """ Return a destination from an ASR Message or None if does not exist."""
    return locations.get(asr_msg.semantic, None)


def get_location_from_name(destination):
    """ Return a location coordinates from its name. """
    return places.get(destination, None)
