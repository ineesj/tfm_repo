#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy

from monarch_translators import gesture_status_to_atom_translator as gstr


_DEFAULT_NAME = 'gesture_status_to_atom_translator'
_NODE_PARAMS = ()


if __name__ == '__main__':
    try:
        node = gstr.GestureStatusToAtomTranslator()
        node.run()
    except rospy.ROSInterruptException:
        pass
