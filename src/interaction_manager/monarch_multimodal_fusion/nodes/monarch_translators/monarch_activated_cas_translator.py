#!/usr/bin/env python


# :version:      0.0.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

"""
:author: Victor Gonzalez Pacheco
:maintainer: Victor Gonzalez Pacheco

Node that translates Activated CAs messages to :py:class:`AtomMsg`

"""

# from functools import partial
import rospy
from rospy_utils import coroutines as co
from monarch_multimodal_fusion import translators as tr
# from monarch_msgs_utils import key_value_pairs as kvpa

from monarch_msgs.msg import KeyValuePairArray as KVPAmsg
from dialog_manager_msgs.msg import AtomMsg
# from std_msgs.msg import String

# from functools import partial


# CA_NAMES = ['ca{0:02d}'.format(num) for num in range(1, 26)]


# def get_activated_cas(cas_msg):
#     """
#     Return a dict indicating which CAs are active.

#     It gets a cas_msg and returns a dict indicating
#     which cas are active and inactive.
#     """
#     cas = cas_msg.data.split('|')
#     return {ca_name: str(True).lower() for ca_name in cas}

# translate_activated_cas = partial(tr.to_atom_msg,
#                                   generator_func=tr.generate_kvpa_slots,
#                                   atom_name='ca_activations')

if __name__ == '__main__':
    try:
        rospy.init_node('monarch_user_command_translator')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))
        # pipe = co.pipe([co.transformer(get_activated_cas),
        #                 co.transformer(kvpa.from_dict),
        #                 co.transformer(translate_activated_cas),
        #                 co.publisher('im_atom', AtomMsg)])
        # co.PipedSubscriber('ca_activations', String, pipe)

        pipe = co.pipe([co.starmapper(tr.kvpa_to_atom,
                                      atom_name='ca_activations'),
                        co.publisher('im_atom', AtomMsg)])
        co.PipedSubscriber('ca_activations', KVPAmsg, pipe)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
