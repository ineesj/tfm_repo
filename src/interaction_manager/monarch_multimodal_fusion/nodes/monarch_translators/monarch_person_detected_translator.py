#!/usr/bin/env python


# :version:      0.0.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

'''
:author: Victor Gonzalez Pacheco
:maintainer: Victor Gonzalez Pacheco

Node that translates from person detected messages to AtomMsg and sends them
to the Interaction Manager

'''
import rospy
from rospy_utils import coroutines as co
from monarch_multimodal_fusion import translators as tr
from dialog_manager_msgs.msg import AtomMsg
# from monarch_msgs.msg import KeyValuePairArray as KVPAmsg
# from std_msgs.msg import Bool
from monarch_msgs.msg import KeyValuePairArray as KVPA


pipe = co.pipe([co.transformer(tr.person_detected_to_atom),
                co.publisher('im_atom', AtomMsg)])

if __name__ == '__main__':
    try:
        rospy.init_node('monarch_person_detected_translator')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))
        # co.PipedSubscriber('person_detected', KVPAmsg, pipe)
        co.PipedSubscriber('person_detected', KVPA, pipe)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
