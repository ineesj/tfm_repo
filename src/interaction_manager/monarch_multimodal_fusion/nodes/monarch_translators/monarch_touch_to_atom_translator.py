#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy

from monarch_translators import touch_to_atom_translator as tat


if __name__ == '__main__':
    try:
        node = tat.TouchToAtomTranslator()
        node.run()
    except rospy.ROSInterruptException:
        pass
