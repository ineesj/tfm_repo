#!/usr/bin/env python


# :version:      0.0.0
# :copyright:    Copyright (C) 2014 Universidad Carlos III de Madrid.
#                Todos los derechos reservados.
# :license       LASR_UC3M v1.0, ver LICENCIA.txt

# Este programa es software libre: puede redistribuirlo y/o modificarlo
# bajo los terminos de la Licencia Academica Social Robotics Lab - UC3M
# publicada por la Universidad Carlos III de Madrid, tanto en su version 1.0
# como en una version posterior.

# Este programa se distribuye con la intencion de que sea util,
# pero SIN NINGUNA GARANTIA. Para mas detalles, consulte la
# Licencia Academica Social Robotics Lab - UC3M version 1.0 o posterior.

# Usted ha recibido una copia de la Licencia Academica Social
# Robotics Lab - UC3M en el fichero LICENCIA.txt, que tambien se encuentra
# disponible en <URL a la LASR_UC3Mv1.0>.

'''
:author: Victor Gonzalez Pacheco
:maintainer: Victor Gonzalez Pacheco

Node that translates User Command messages to :py:class:`AtomMsg`

'''

# from functools import partial
import rospy
from rospy_utils import coroutines as co
from monarch_multimodal_fusion import translators as tr
# from std_msgs.msg import String
from dialog_manager_msgs.msg import AtomMsg
from monarch_msgs.msg import KeyValuePairArray as KVPAmsg


# recv_command_translator = partial(tr.to_atom_msg,
#                                  generator_func=tr.generate_cmd_receivd_slots)

pipe = co.pipe([co.transformer(tr.user_command_to_atom),
                co.publisher('im_atom', AtomMsg)])

if __name__ == '__main__':
    try:
        rospy.init_node('monarch_user_command_translator')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))
        co.PipedSubscriber('user_command', KVPAmsg, pipe)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
