#!/usr/bin/env python
""" Node that fetchs data from input sources and maps it as user commands """

# import roslib
# roslib.load_manifest('monarch_multimodal_fusion')
import rospy
# from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from rospy_utils import coroutines as co

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import RecognizedSpeech as AsrMsg
from monarch_msgs.msg import KeyValuePairArray as KVPA
# from monarch_msgs.msg import KeyValuePair as KVP
# from monarch_multimodal_fusion import utils as futils
# from monarch_aggregators import utils as aggutils


SEMANTICS = {'LOCATION-CLASSROOM': {'command': 'goto_place',
                                    'location': 'classroom'},
             'LOCATION-PLAYROOM': {'command': 'goto_place',
                                   'location': 'playroom'},
             'LOCATION-CORRIDOR': {'command': 'goto_place',
                                   'location': 'corridor'},
             'LOCATION-ROOM': {'command': 'goto_place',
                               'location': 'dormitory'},
             'LOCATION-CANTINA': {'command': 'goto_place',
                                  'location': 'cantina'},
             'COMMAND-SHOW_MEDIA-TREE': {'command': 'select_media_content',
                                         'media_content': 'tree'},
             'COMMAND-SHOW_MEDIA-TRAIN': {'command': 'select_media_content',
                                          'media_content': 'train'},
             'COMMAND-SHOW_MEDIA-DOG': {'command': 'select_media_content',
                                        'media_content': 'dog'}}


# LOCATIONS = {"LOCATION-CLASSROOM": "classroom",
#              "LOCATION-PLAYROOM": "playroom",
#              "LOCATION-CORRIDOR": "corridor",
#              "LOCATION-ROOM": "dormitory",
#              "LOCATION-CANTINA": "cantina"}

# COMMANDS = {'COMMAND-SHOW_MEDIA-TREE': 'tree',
#             'COMMAND-SHOW_MEDIA-TRAIN': 'train',
#             'COMMAND-SHOW_MEDIA-DOG': 'dog'}

def parse_asr(asr_msg):
    return SEMANTICS.get(asr_msg.semantic, None)


# def get_place_from_asr(asr_msg):
#     """ Return a destination from an ASR Message or None if does not exist."""
#     return LOCATIONS.get(asr_msg.semantic, None)


# def location_to_user_command(location):
#     d = {'command': 'goto_place',
#          'location': location}
#     return kvpa.from_dict(d)

if __name__ == '__main__':
    try:
        rospy.init_node('monarch_user_command_aggregator')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        user_command_publisher = co.publisher('user_command', KVPA)
        asr_pipe = co.pipe([co.mapper(parse_asr),
                            co.filterer(lambda msg_: bool(msg_)),
                            co.mapper(kvpa.from_dict),
                            user_command_publisher])
        # asr_pipe = co.pipe([co.mapper(get_place_from_asr),
        #                     co.filterer(lambda place: bool(place)),
        #                     co.mapper(location_to_user_command),
        #                     user_command_publisher])
        co.PipedSubscriber('audio/speech_rec', AsrMsg, asr_pipe)
        # Screen already produces user commands so we send them directly.
        co.PipedSubscriber('screen_touched', KVPA, user_command_publisher)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
