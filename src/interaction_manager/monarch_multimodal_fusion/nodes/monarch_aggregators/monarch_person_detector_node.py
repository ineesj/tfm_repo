#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy
from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import KeyValuePairArray as KVPA
from monarch_msgs.msg import RfidReading as RfidMsg
from monarch_msgs.msg import CapacitiveSensorsReadings as TouchMsg
from std_msgs.msg import Bool


_DEFAULT_NAME = 'person_detector_node'
_NODE_PARAMS = ()


def _fill_person_detected_msg(person_detected=True,
                              person_id='UNKNOWN'):
    """ Return a dict with the data of the person detected. """
    person_dict = {'person_detected': str(person_detected).lower(),
                   'person_id': str(person_id)}
    return person_dict


class PersonDetectorNode():

    ''' Detects people and sends a predicate to "person_detected" topic
        Now it detects people with the following sensors:
            - Rfid sensor
            - Touch sensor
            - Camera

        :TODO: - publish the user ID
               - At some point publish the confidence of the detection
               - The interfaces that have detected it

    '''
    def __init__(self, **kwargs):
        name = kwargs.get('node_name', _DEFAULT_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        loginfo("Initializing " + self.node_name + " node...")

        # Subscribers
        # rospy.Subscriber("rfid_tag", RfidMsg, self.rfid_cb)
        # rospy.Subscriber("cap_sensors", TouchMsg, self.touch_cb)
        rospy.Subscriber("people_near", Bool, self.camera_cb)
        rospy.Subscriber("user_info", KVPA, self.user_info_cb)
        # Publishers
        # self.publisher = rospy.Publisher('person_detected', Bool)
        self.publisher = rospy.Publisher('person_detected', KVPA)

    def user_info_cb(self, uinfo_msg):
        uinfo = kvpa.to_dict(uinfo_msg)
        logdebug("Person detected by User INFO.")
        person_dict = _fill_person_detected_msg(person_id=uinfo['uid'])
        self.publisher.publish(kvpa.from_dict(person_dict))
        # self.publisher.publish(Bool(data=True))

    # def touch_cb(self, touch_msg):
    #     logdebug("Person detected by Touch")
    #     # At some point we should set False
    #     person_dict = _fill_person_detected_msg()
    #     self.publisher.publish(kvpa.from_dict(person_dict))

    def camera_cb(self, cam_msg):
        logdebug("Person detected by the cameras")
        ## TODO: Process distances (and orientation) to check if person is near
        logwarn("camera_cb: Distance computation NOT IMPLEMENTED")
        person_dict = _fill_person_detected_msg()
        self.publisher.publish(kvpa.from_dict(person_dict))

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        loginfo('Shutting down ' + rospy.get_name() + ' node.')


if __name__ == '__main__':
    try:
        node = PersonDetectorNode()
        node.run()
    except rospy.ROSInterruptException:
        pass
