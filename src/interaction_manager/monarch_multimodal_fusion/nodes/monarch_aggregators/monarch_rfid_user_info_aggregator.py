#!/usr/bin/env python
import roslib
roslib.load_manifest('monarch_multimodal_fusion')
import rospy
# from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from monarch_multimodal_fusion import translators
from monarch_msgs_utils import key_value_pairs as kvpa
from monarch_msgs.msg import KeyValuePairArray as KVPA
from dialog_manager_msgs.msg import AtomMsg, VarSlot
from monarch_msgs.msg import RfidReading as RfidMsg


_DEFAULT_NAME = 'rfid_to_atom_translator'


RFID_STOP_INTERACTING_TAGS = {22816}


def _user_not_found(id_):
    return {'uid': id_,
            'name': 'not_found',
            'surname': 'not_found',
            'welcome_msg': "Sorry. Your ID is not in the user database",
            'non_verbal_sound': 'not_found',
            'greetings_expression': 'not_found',
            'role': 'not_found',
            'gender': 'not_found'}


# def generate_def_slots(atom_type, atom_subtype='user'):
#     yield VarSlot(name="type", val=atom_type)
#     yield VarSlot(name="subtype", val=atom_subtype)
#     yield VarSlot(name="timestamp", val="_NO_VALUE_", type="number")
#     yield VarSlot(name="consumed", val="false", type="string")


def generate_slots(user_info):
    ''' Yields the slots of the atom from a user_info dict.'''
    # for slot in common.generate_def_slots('user_info'):
    #     yield slot
    yield VarSlot(name="uid", val=str(user_info['uid']), type="number")
    yield VarSlot(name="name", val=str(user_info['name']), type="string")
    yield VarSlot(name="surname", val=str(user_info['surname']), type="string")
    yield VarSlot(name="welcome_msg",
                  val=str(user_info['welcome_msg']),
                  type="string")
    yield VarSlot(name="non_verbal_sound",
                  val=str(user_info['non_verbal_sound']),
                  type="string")
    yield VarSlot(name="greetings_expression",
                  val=str(user_info["greetings_expression"]),
                  type="string")
    yield VarSlot(name="role", val=str(user_info['role']), type="string")
    yield VarSlot(name="gender", val=str(user_info['gender']), type="string")


# def produce_atom_msg(user_info):
#     ''' Produces an L{AtomMsg} from a user_info dict
#         :see: entries of params/user_info.yaml) '''
#     return AtomMsg(varslots=list(generate_slots(user_info)))


def get_user_info(param_name='user_info'):
    ''' returns an user_info dictionary from the parameter server.
        :see: entries of params/user_info.yaml)
        :raise: Exception if paramter name is not in the parameter server.
    '''
    param_full_name = rospy.search_param(param_name)
    return rospy.get_param(param_full_name)


def _stringize_dict_values(dict_):
    return {k: str(v) for k, v in dict_.items()}


class UserInfoAggregator(object):
    """docstring for UserInfoAggregator"""
    def __init__(self, **kwargs):
        super(UserInfoAggregator, self).__init__()
        name = kwargs.get('node_name', _DEFAULT_NAME)
        rospy.init_node(name, anonymous=True)
        self.node_name = rospy.get_name()
        rospy.on_shutdown(self.shutdown)
        rospy.loginfo("Initializing " + self.node_name + " node...")

        self.user_info = get_user_info()
        # rospy.loginfo("All Users Info: \n{}\n\n".format(self.user_info))
        # Subscribers
        rospy.Subscriber("rfid_tag", RfidMsg, self.rfid_cb)
        # Publishers
        self.publisher = rospy.Publisher('im_atom', AtomMsg)
        self.uinfo_pub = rospy.Publisher('user_info', KVPA, latch=True)
        self.stop_interacting_pub = rospy.Publisher('ca_activations',
                                                    KVPA, latch=True)

    def rfid_cb(self, rfid_msg):
        if rfid_msg.tag_id in RFID_STOP_INTERACTING_TAGS:
            self.publish_rfid_stop_interacting()
            return
        user_id = str(rfid_msg.tag_id)
        user_data = self.user_info.get(user_id, _user_not_found(user_id))
        user_data = _stringize_dict_values(user_data)
        user_info_atom = \
            translators.to_atom_msg(user_data, generate_slots, 'user_info')
        rospy.logdebug("Publishing user_info_msg: {}".format(user_data))
        self.publisher.publish(user_info_atom)
        self.uinfo_pub.publish(kvpa.from_dict(user_data))

    def publish_rfid_stop_interacting(self):
        """Publish stop interacting command to ca_activations topic."""
        rospy.loginfo("STOP INTERACTING TAG readed. Sending stop interacting.")
        stop_interacting_cmd = {'activated_cas': 'stop_interacting'}
        self.stop_interacting_pub.publish(kvpa.from_dict(stop_interacting_cmd))

    def run(self):
        rospy.spin()

    def shutdown(self):
        ''' Closes the node '''
        rospy.loginfo('Shutting down ' + rospy.get_name() + ' node.')


if __name__ == '__main__':
    try:
        node = UserInfoAggregator()
        node.run()
    except rospy.ROSInterruptException:
        pass
