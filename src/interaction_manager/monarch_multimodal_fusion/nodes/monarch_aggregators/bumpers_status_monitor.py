#!/usr/bin/env python
"""Node that notifies if the robot has been [dis]connected from charger.

:author: Victor Gonzalez
:date: 2016-02-11
"""

# import roslib
# roslib.load_manifest('monarch_multimodal_fusion')
import rospy
# from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from rospy_utils import coroutines as co
from monarch_msgs.msg import BumpersReadings as Bump


def msg_values(msg):
    """Return all values of a ROS msg without the header."""
    return (getattr(msg, bname) for bname in msg.__slots__[1:])


def is_bump_pressed(bump_msg):
    """Return True if any field of a BumpersReadings msg is True)."""
    return any(msg_values(bump_msg))


def bumps_released(prev, curr):
    """Predicate that returns True if all bumps have been released.

       That is, previous bumps were presed and current bumps are not pressed."""
    return has_bump_changed(prev, curr) and not is_bump_pressed(curr)


def has_bump_changed(prev, cur):
    """Predicate that returns if two BumpersReadings msgs are different."""
    p_vals = msg_values(prev)
    c_vals = msg_values(cur)
    return (set(p_vals) != set(c_vals))


def is_bumper_triggered(prev, cur):
    """Predicate that returns True if any field of a bumper is pressed."""
    p_vals = msg_values(prev)
    c_vals = msg_values(cur)
    return (False, True) in zip(p_vals, c_vals)

if __name__ == '__main__':
    try:
        rospy.init_node('bumper_monitor')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        pipe = co.pipe([co.sliding_window(2),
                        co.filterer(lambda x: is_bumper_triggered(x[0], x[-1])),
                        co.mapper(lambda x: x[-1]),  # current bumper reading
                        co.publisher('bumper_triggered', Bump)])
        co.PipedSubscriber('bumpers', Bump, pipe)

        released_pipe =\
            co.pipe([co.sliding_window(2),
                     co.filterer(lambda x: bumps_released(x[0], x[-1])),
                     co.mapper(lambda x: x[-1]),  # current bumper reading
                     co.publisher('bumpers_released', Bump)])
        co.PipedSubscriber('bumpers', Bump, released_pipe)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
