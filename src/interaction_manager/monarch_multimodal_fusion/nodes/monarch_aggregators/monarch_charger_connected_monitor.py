#!/usr/bin/env python
"""Node that notifies if hte robot has been [dis]connected from charger.

:author: Victor Gonzalez
:date: 2016-01-26
"""

# import roslib
# roslib.load_manifest('monarch_multimodal_fusion')
import rospy
# from rospy import (logdebug, loginfo, logwarn, logerr, logfatal)

from rospy_utils import coroutines as co
from std_msgs.msg import Bool
from monarch_msgs.msg import BatteriesVoltage as BattMsg

if __name__ == '__main__':
    try:
        rospy.init_node('charger_connected_monitor')
        rospy.loginfo("Initializing {} Node".format(rospy.get_name()))

        pipe = co.pipe([co.mapper(lambda x: x.charger > 0.0),
                        co.sliding_window(2),
                        co.filterer(lambda x: x[0] != x[-1]),
                        co.mapper(lambda x: x[-1]),
                        co.publisher('is_charger_connected', Bool)])

        co.PipedSubscriber('batteries_voltage', BattMsg, pipe)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
