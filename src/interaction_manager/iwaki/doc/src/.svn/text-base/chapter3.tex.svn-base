\chapter{Content representation in Iwaki}
\label{Chapter3}

\section{Initialization file}

Iwaki initialization file is located in the root of scripts directory, and by default is named \texttt{initialize\_im.xml}. The default init file name can be changed by \texttt{-i init\_file} command line option.

The initializer file has three sections: list of triggerable recipes, list of recipe files and list of action files---see the example below.

\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="US-ASCII"?>
 
<iminit>
  <!-- these recepies are always ready to be triggered by im itself-->
  <triggerables>
    <recipe name="how_are_you" max_instances="1"/>
    <recipe name="how_are_you_and_you" max_instances="1"/>
    <recipe name="heartbeat" max_instances="1"/>
  </triggerables>

 
  <recipefiles>
    <file>georgi/how_are_you_and_you.xml</file>
    <file>georgi/how_are_you.xml</file>
    <file>georgi/heartbeat.xml</file>
  </recipefiles>


  <actionfiles>
    <file>defaults.xml</file>
  </actionfiles>
</iminit>
\end{lstlisting}

A recipe instance is an active recipe that is in a node of the execution tree. ``max\_instances'' establishes the maximum number of recipe instances, that are, nodes where a same recipe can be in. A value of ``any'' sets that the recipe can be instantiated in any number of nodes.

More than one recipe can be implemented in one recipe file. These files are searched in a directory called \texttt{recipes} within the root of scripts directory established by the \texttt{-p} option, e.g. \texttt{interaction\_commons/data/recipes}. Notice that inside of such directory several subdirectories can be created, each one for each IM project, e.g. \texttt{interaction\_commons/data/recipes/i\_spy\_game, or interaction\_commons/data/recipes/alz\_presentation}

\section{Default atoms file}

In the initialization step, the Interaction Manager (IM) also loads a set of default atoms from the default\_atoms file. By default, this file is in the root scripts directory and is called \texttt{default\_atoms.xml}. The default atoms file name can be changed by \texttt{-a my\_default\_atoms.xml} command line option.

Default atoms file specifies default values of atom slots, their types, and type constraints. 

\lstset{language=XML}
\begin{lstlisting}
<atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="q2u_how_are_you_handled" val="true" enum="true, false"/>
</atom>
\end{lstlisting}

The listing above, for example, specifies an atom of type \texttt{im}, subtype \texttt{globals}, and a slot named \texttt{q2u\_how\_are\_you\_handled} of a default type \texttt{string}, that can assume values \texttt{true} and \texttt{false}. Enumerable values are also possible. The off-line type checker that runs when the recipes are loaded will verify that the values associated with slot \texttt{q2u\_how\_are\_you\_handled} are always within the list specified by \texttt{enum} attribute, and will throw an error otherwise.

Default atoms initialize a set of slots that can be used by any recipe in any formula both in preconditions and assignments. 

Each atom has two mandatory slots: \texttt{type} and \texttt{subtype}. These slots are used in the unification process between input atoms, default atoms and atoms in the precondition formula.

The globals atom, an atom with \texttt{type = ``im''}, and \texttt{subtype = ``globals''} is mandatory and should be present in the default atoms file. The globals atom has the following internal slots by default (it is not necessary to implement them):

\begin{itemize}
 \item \texttt{time} in seconds, bound to a global variable called ``\_im\_time''.
 \item \texttt{year} number since ``0''.
 \item \texttt{month} number since January (that is 1). 
 \item \texttt{month\_day}, days since 1st.
 \item \texttt{week\_day}, days since Monday.
 \item \texttt{hour}, hours since midnight.
 \item \texttt{minute} 0-59.
 \item \texttt{second} 0-59.
\end{itemize}


\section{Atoms and Slots}

Atoms represent the input data to the IM. Notice that this input can come from interactive perception capabilities or from any part of the control architecture (counters, timers, outputs of another applicationes, etc).

An atom is a list of slots. Atoms are built in three ways: in the default atoms file, in the precondition section of a  recipe or as input data to the IM. 

Atom just has one attribute: \texttt{quantifier} that in this iwaki's version it is not handled.

Slots have the following attributes:

\begin{itemize}
 \item \texttt{name} defines the name of the slot. It is mandatory. 
 \item \texttt{val}, default value of the slot.
 \item \texttt{rel}, it defines a relation for the unification process (see below): ``$<$'', ``$>$'', ``='', ... if numbers; ``='', ``substr''... if strings. It is also possible to use a Regular Expression (RE) between strings.
 \item \texttt{type} of the data the slot contains. Type can be \texttt{number} or \texttt{string}. Slot type establishes the type of the relation operator (for numbers or for strings).
 \item \texttt{var}. It declares a global variable inside the atom and binds it the value of the corresponding slot.
\end{itemize}

For example:
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="US-ASCII"?>

<!-- default values for atom slots -->
<default_atoms>
  <!-- globals atom -->
  <atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="time" val = "_NO_VALUE_" type="number" var="_im_time"/>
    <slot name="prev_beat_time" val = "0" type="number"/>
    <slot name="r_val" val = "1" type="number"/>
    <slot name="beat_handled" val="false"/>
  </atom>

</default_atoms>
\end{lstlisting}

In the above atom, it is defined the var \texttt{\_im\_time} with default value \texttt{\_NO\_VALUE\_} that is bound to an internal global slot \texttt{time} (this slot has been described above).

Each atom has an internal slot called \texttt{this}. The slot works as a type of ``key'' for accessing and refering to different atom's slots in different parts of a recipe or between recipes; \texttt{this} value is automaticaly initialized by the IM when an atom is created.

At the begining, the Interaction Manager loads the init file, then sets the globals atom, sets up the plan tree and sets the history stack (a register of the interaction process with timming values).

\section{Unification}
\label{sec:Ch3Unification}

Unification is the process of merging two or more atoms in a unique one and check the formulas implemented in the precondition sections for all triggerable recipes. Unification is made between atoms in the input\_queue of the Interaction Manager, with the default atoms, and with the corresponding atoms defined in the recipes.

The unification between two atoms is made taking into account the matching of the values of two mandatory slots: \texttt{type} and \texttt{subtype}.

So, when a new atom is received into the IM, it is added to the input queue and unified with the corresponding default atom if there is a matching between type and subtype slots. In the unification, the received atom rewrites the slots with the same name, in the corresponding default atom. Bindings, the creation of variables bound to slot values, are also made.

For each triggerable or backchainable recipe, the unified atom is compared with the atoms defined in the formula of their preconditions. Comparation is made slot by slot for each atom within each formula within each triggerable recipe. Recipes whose formula matches the comparation are triggered, that is, their body section is executed.

Unification is made between all the input atoms received in a \texttt{tick} of execution and the corresponding default atoms.

\section{Formula}
\label{sec:Ch3Formula}

A Formula is a disjunction of a few conjunction. A conjunction is just an AND relation; a disjunction
an OR relation. In iwaki, a formula is expressed by one or more atoms with a conditional relation inside each slot. 

Each atom slot has a relation attribute that specifies a logical condition (e.g. ``$<$ '', ``=='', ``$>$'', etc) between the slot value and a specified value. For example, this slot
\begin{lstlisting}
      <!-- arguments -->
      <slot name="prev_beat_time" rel="<" val="\$_im_time-randi(1,5)" type="number"/>
\end{lstlisting}

could be translated to the following formula:
\begin{lstlisting}
 if( prev_beat_time < (_im_time - randi(1,5) ) 
\end{lstlisting}

Notice that there is an arithmetic expression inside \texttt{val} value. Arithmetic and logical operations are allowed inside \texttt{val} values. 

The access to a variable value inside a formula it is marked by the begining character $\$$. In the example \texttt{\$\_im\_time} it would give the value of the variable \_im\_time bound to a slot inside an atom in the precondition section. Therefore, variables are a cue key for accessing to slot values between atoms. 

It is also possible to call to an implemented function inside the \texttt{val} value. In the example, the value calls $randi(1,5)$ function that is implemented inside iwaki and gives a random integer between one and five. Other implemented functions includes $rand(n,m)$, $@PartialMatch()$ and $@FullMatch()$.  


\section{Recipes}
\label{sec:Ch3Recipes}

IM is a production system, with rules specified as recipes. Recipe instance is a node inside a tree the main interaction structure. A recipe consists of the following elements:

\begin{itemize}
\item XML header: 
\begin{lstlisting}
  <?xml version="1.0" encoding="US-ASCII"?>
\end{lstlisting}

\item A unique recipe name with optional priority value. 
\lstset{language=XML}
\begin{lstlisting}
  <recipe name="heartbeat" priority="1">
\end{lstlisting}
Priority works as follows: if two recipes are triggered by the same input atom, the recipe with the greatest priority is executed first. Priority is not a mandatory parameter and its default value is ``1''.

\item A precondition, which is either a Formula (a disjunction of a few conjunctions), or a set of Atoms corresponding to a single conjunction:
\lstset{language=XML}
\begin{lstlisting}
<!-- preconditions: unification and bindings -->
<precondition>
    <atom quantifier="exist">
      <!-- object type and subtype -->
      <slot name="type" val="im"/>
      <slot name="subtype" val="globals"/>
      <!-- arguments -->
      <slot name="prev_beat_time" rel="<" val="\$_im_time-randi(1,5)" type="number"/>
      <!--bindings -->
      <slot name="time" rel="bind" var="_im_time"/>
      <slot name="this" rel="bind" var="the_globals_atom"/>
    </atom>
  </precondition>
\end{lstlisting}
Default atoms are unified with the preconds of the triggerable recipes at the begining in the unification process. Bindings to variables (variable creation or update) are only allowed in the precondition section. 

\item A whilecondition, which is either a Formula (a disjunction of a few conjunctions), or a set of Atoms corresponding to a single conjunction:

\lstset{language=XML}
\begin{lstlisting}
<!-- purge the recipe when this condition fails -->
<whilecondition>
  <atom>
    <slot name="present" val="true"/>
    <slot name="this" var="present_user_atom"/>
  </atom>
</whilecondition>
\end{lstlisting}

Notice that whilecondition can be empty, as in the \texttt{heartbeat} recipe. When the formula of the whilecondition does not match after a new unification the recipe stops the execution. Recipe's attribute \texttt{if\_failed} specifies what to do if the whilecondition fails: to go to the assignpost section (\texttt{``skip\_to\_assignpost''}) or directly ends the recipe (\texttt{``skip\_to\_end''}). 

\item A body of the recipe, which is a sequence of one of the following elements: an assignment, a list of actions or a goal. By default the steps are performed sequentially and conditionally on the successful execution of the preceding steps; the attribute \texttt{order = ``sequence''} is not implemented yet. 

\lstset{language=XML}
\begin{lstlisting}
 <body order="sequence">
    <assignment>
      <atom>
        <slot name="prev_beat_time" val="$_im_time" type="number"/>
        <slot name="this" var="the_globals_atom"/>
      </atom>
    </assignment>

    <action name="generate_utterance" actor="generator" action_space="speech">
      <roboml:args>
        <arg name="utterance_file" value="georgi/heartbeat.ogg" type="string"/>
      </roboml:args>
    </action>
    
  </body>
\end{lstlisting}

\item An assignpost, which is an assignment that is to be performed upon the completion of the recipe. This assignment are equivalent to the assignments in the end of the recipe body, except for they are also used in matching the recipe against the currently active goal. 

\lstset{language=XML}
\begin{lstlisting}
<!-- set right after execution ends -->
  <assignpost>
    <atom>
    <!-- set object which name is equal to the one stored in var -->
      <slot name="this" var="present_user_atom"/>
      <slot name="q2u_how_are_you_handled" val="true"/>
    </atom>
  </assignpost>
\end{lstlisting}

\end{itemize}

\section{Action definition files}

Actions are the outputs of the Interaction Manager. They consist of a list of arguments, sequence of datablocks, and elements defined by the Behavior Markup Language~\citep{BMLweb}.


Here is an action with 4 arguments as declared in the \texttt{actions.xml} file.

\lstset{language=XML}
\begin{lstlisting}
<bml name="generate_utterance">
  <roboml:args>
    <arg name="utterance_string" default="_NO_VALUE_" type="string"/>
    <arg name="utterance_token" default="_NO_VALUE_" type="string"/>
    <arg name="utterance_file" default="_NO_VALUE_" type="string"/>
    <arg name="test_arg" default="0" type="number"/>
  </roboml:args>
</bml>
\end{lstlisting}

First of all, every action has to be declared and/or implemented in the default actions file as described above. However, the action is called in the body of the recipe, where it is finally implemented:

\lstset{language=XML}
\begin{lstlisting}
    <action name="generate_utterance" actor="generator" action_space="speech">
      <roboml:args>
        <arg name="utterance_file" value="georgi/thats_good_thats_good" type="string"/>
      </roboml:args>
    </action>
\end{lstlisting}
