\chapter{Recipes}
\label{Chapter4}

In Section~\ref{sec:Ch3Recipes}, we outlined the structure of a recipe. In this chapter we will describe each of the recipe components in detail. You will know how to write a recipe to respond to a particular input, and how to implement control flow, including conditionals via backchaining.



\section{Preconditions}
Precondition is the section where it is defined the formula that indicates the conditions by which the recipe should trigger.

Once unification between an input atom and defaults atoms is made, the unified atom is compared within the formula implemented in the preconditions. If the comparation matches, the recipe is triggered.

For example, let's imagine that the next input atom is received:

\lstset{language=XML}
\begin{lstlisting}
<atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="input_sema" val="greet"/>
    <slot name="confidence" val="0,4" type="number"/>
</atom>
\end{lstlisting}

Then, it will fit type and subtype slots with the globals default atom, that is:

\lstset{language=XML}
\begin{lstlisting}
<precondition>
  <atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="time" val = "_NO_VALUE_" type="number" var="_im_time"/>
    <slot name="prev_beat_time" val = "0" type="number"/>
    <slot name="input_sema" val = "_NO_VALUE_" type="string"/>
    <slot name="q2u_how_are_you_handled" val="true"/>
  </atom>
</precondition>
\end{lstlisting}
\newpage
Unification would give the following atom:

\lstset{language=XML}
\begin{lstlisting}
<atom>
    <slot name="type" val="im"/>
    <slot name="subtype" val="globals"/>
    <slot name="time" val = "5983212314" type="number" var="_im_time"/>
    <slot name="prev_beat_time" val = "0" type="number"/>
    <slot name="input_sema" val="greet"/>
    <slot name="q2u_how_are_you_handled" val="true"/>
    <slot name="confidence" val="0,4" type="number"/>
</atom>
\end{lstlisting}

Notice that slots with the same name are rewriten in the default globals atom. New slots are just added to the unified atom and bindings between slots and variables are kept (variable values are updated).


\section{Body}

A body of the recipe consists of a sequence of \textsl{body elements}. A body element is an \textsl{assignment}, an \textsl{action}, or a \textsl{goal}. At present (version 0.1), body elements are executed sequentially. Action and goal body elements are different from the assignment in that they may take undetermined amount of time. To allow for the execution to continue even if an action or a goal takes too long time to complete, action tag has an optional timeout attribute. Actions are different from both assignments and goals in that their execution may return a status different from the successful \textsl{completed} status. Returned action status can be used to specify the control flow for the rest of the recipe, such as moving to the next body element, moving to the assignpost, or moving to the end (purging the recipe).

\subsection{Assignments}
Assignments allow to change slots' values to any visible atom. Visible atoms are the default atoms that have been unified with the precondition atoms. 

To make an assignment the special slot value \texttt{name = "this"} is used. For example:

\lstset{language=XML}
\begin{lstlisting}
  <assignment>
    <atom>
      <slot name="this" var="present_user_atom"/>
      <slot name="uu_unhandled" val="false"/>
    </atom>
  </assignment>
\end{lstlisting}

This assignment changes the slot value \texttt{uu\_unhandled} to the value \texttt{"false"}. The atom is refered by the slot \texttt{this} that points to the atom whose this value is the value in the variable \texttt{"present\_user\_atom"}. Notice that to access to this variable value it is not necessary to use the $\$$ character before the var name.

Therefore, previously to an assignment, a bound to this slot is necessary. This is done in the bindings section of the precondition part in the recipe:

\lstset{language=XML}
\begin{lstlisting}
<precondition> 
  <atom quantifier="exist">
    ...
      <!-- bindings -->
      <slot name="id" rel="bind" var="present_user_id"/>
      <slot name="this" rel="bind" var="present_user_atom"/>
    </atom>
  </precondition>

\end{lstlisting}

In the above example, it is created the variable \texttt{present\_user\_atom} whose value is bound to the value of the \texttt{this} slot. Later, in the assignment inside the body section of the recipe, the access to the atom is made by using such variable.

When a slot value changes, the variable bound to such slot also changes to the new assignment.

\subsection{Actions}
Actions are defined in the default action file loaded at the begining and finally defined in the body part of the recipe. Actions 
are implemented and handled in the subscriptor of the action message, that is, outside of the IM. The implementation should also call a function to give back the so called action status object. Action status could be "completed", "aborted", "executing" and "pending". It is information that the action executor gives back to the IM so the interaction flow can be controlled.

Actions have the following attributes:

\begin{itemize}
 \item \texttt{name}. It is mandatory.
 \item \texttt{actor}. The executor name for the action implementator.
 \item \texttt{action\_space}. A namespace for the action implementator
 \item \texttt{timeout} in msecs, defines the maximum time an action status can be "executing" or "pending".
 \item \texttt{priority}. Not implemented yet.
 \item \texttt{if\_timeout}. Behaviour when the time of execution of the action takes more than the timeout defined. This is checked when action status gives a value of "executing" or "pending". “skip\_to\_next”, goes to the next body element (action, assignment, or goal); “skip\_to\_end”, goes to directly to the end of the recipe.
 \item \texttt{if\_completed}. Behaviour when the action status received in the IM is "completed". Can also be "skip\_to\_end" or "skip\_to\_next".
 \item \texttt{if\_aborted}.Behaviour when the action status received in the IM is "aborted". Can also be "skip\_to\_end" or "skip\_to\_next".
 \item \texttt{if\_failed}. Not implemented yet.
 \item \texttt{if\_node\_purged}. Behaviour when the received action status is "executing". Depending on \texttt{if\_node\_purged} variable of the active BodyElement it either abort, or wait, or abort and wait for confirmation. If active element is a goal then these three possibilities are applied recursively to children. If "wait", just do a regular wait (do nothing special). If "abort" do as prescribed in \texttt{ifWhileconditionFailed}, rather than in bodyElements if\_aborted, for whilecondition failure is a special situation, but first sends abort commands to the executors.
\end{itemize}

Actions should include input and output arguments, and also can include random selection section. 

\lstset{language=XML}
\begin{lstlisting}
   <action name="generate_utterance" actor="generator" action_space="speech">
      <roboml:random>
        <!-- outcome 1 -->
        <roboml:random_outcome p="0.5">
          <roboml:args>
            <arg name="utterance_file" value="georgi/getting_it_done" type="string"/>
          </roboml:args>
        </roboml:random_outcome>
        <!-- outcome 2 -->
        <roboml:random_outcome p="0.5">
          <roboml:args>
            <arg name="utterance_file" value="georgi/living_a_dream_clean" type="string"/>
          </roboml:args>
        </roboml:random_outcome>
      </roboml:random>
    </action>
\end{lstlisting}   

Tags as \texttt{roboml:random}, \texttt{roboml:random\_outcome}, \texttt{roboml:args} and \texttt{roboml:return\_args} are implemented by iwaki. The last one nests the arguments of the action. The \texttt{arg} tag include name (mandatory), value, type, default (for default values), unit and var. The last attribute is conceived for binding the return\_args of the recipe to a new var.

\subsection{Goals}

A goal specifies the set of recipes that can be used to backchain on it. There are two way to define a goal. First, via a formula that must be satisfied by the child recipe. Second, by explicitly listing the names of potential child recipes. In the latter case, the formula is optional. We refer to a goal without a formula as an \textsl{empty goal}.

At the begining, the IM binds type and subtype slots of every goal in every triggerable recipe, and the assignpost of the backchainable recipes according to preconds of the child recipes. That is, when a goal with a formula is defined inside the body of a recipe, the IM automatically binds the atoms of the formula. This binding establishes the backchain of child recipes. Child recipes are the ones whose postconditions (or post assignation) satisfy the formula defined by the goal.

\subsubsection{A goal with a formula}

An example of a goal with a formula is show below:

\lstset{language=XML}
\begin{lstlisting}
    <goal recipe_name="any">
      <atom>
        <slot name="q2u_how_are_you_handled" val="true"/>
        <slot name="this" var="present_user_atom"/>
      </atom>
    </goal>
\end{lstlisting}


During the preprocessing stage, for each of the goals, Iwaki identifies recipes whose postconditions, once executed, will satisfy the goal's formula.  These recipes are selected from those listed in the \textsl{recipe\_name} attribute. If the \textsl{recipe\_name}  attribute is empty, or is equal to 'any', then all recipes loaded via init file are potential candidates. 
 
\subsubsection{An empty goal}

\lstset{language=XML}
\begin{lstlisting}
    <goal recipe_name="q2u_how_are_you_answer_yes, q2u_how_are_you_answer_no"/>
\end{lstlisting}

An empty goal does not include a formula. Potential child recipes are specified by \textsl{recipe\_name} attribute. If \textsl{recipe\_name} is left empty or set to ``any'' a warning will be issued at the preprocessing stage, since that means that any recipe can be a potential child.

\subsubsection{Control flow with the goals}

When execution reaches the goal element of a recipe's body, the further control flow depends on whether the goal contains a formula or not, and whether the \textsl{forced} attribute of goal tag is set to ``true'' or not. 
\begin{itemize}
\item If the goal's \textbf{formula is not empty} \textit{and} the goal's \textsl{forced} attribute \textbf{is not set to ``true''}, the formula will be checked for satisfiability against current global state of the system. If the formula is already satisfied, the execution flow moves to the next body element (or the postcondition if the goal was the last element of the body). If the formula is not satisfied, backchainable recipes will be attempted, in order of their priority, to be backchained upon by matching them against the goal and checking if their preconditions are satisfied.
\item If the goal's \textbf{formula is not empty} \textit{and} the goal's \textsl{forced} attribute \textbf{is set to ``true''}, then the goal's formula is \textit{not} checked on satisfiability by current state, and the attempt to backchain is forced independently on whether the goal's formula is already satisfied.
\item If the goal's \textbf{formula is empty}, then the attempt to backchain is made on the backchainable recipes specified in the goal's \textsl{recipe\_name} attribute. This can be thought as a forced backchaining on a formula that is already satisfied, since an empty formula is true by the definition.
\end{itemize}

Why would we want the forced backchaining on a non-empty goal? Consider the following example. We would like to backchain on a set of recipes that are conditioned an uninstantiated user atom and execute some action for that particular user, once it gets instatiated. The parent recipe (the one that contains the goal) may want to constrain the children recipes to be instantiated (and their preconditions to be checked against) a particular user atom. In that case, the goal may look as follows: 

\lstset{language=XML}
\begin{lstlisting}
    <goal recipe_name="q2u_how_are_you_answer_yes, q2u_how_are_you_answer_no" forced="true">
      <atom>
        <slot name="this" var="present_user_atom"/>
      </atom>
    </goal>
\end{lstlisting}
\newpage
The postconditions of \textsl{q2u\_how\_are\_you\_answer\_yes}, \textsl{q2u\_how\_are\_you\_answer\_no} recipes may be something like this:

\lstset{language=XML}
\begin{lstlisting}
  <assignpost>
    <atom>
      <slot name="this" var="present_user_atom_of_child_recipe"/>
    </atom>
  </assignpost>
\end{lstlisting}

Since the atom in the goal's formula in this example will match any atom of postcondition (because \textsl{this} slot of postcondition is always uninstantiated before the actual backchained recipe is loaded), more slots may be necessary if there is an ambiguity between the goal's and postcondition's atoms.

\section{Postconditions}
Are assignments that are executed when all body elements have been executed with successs. IM checks what recipe's postcontitions satisfy what goals, and make a interaction tree.
%Iwaki limitations force programmer to put \texttt{this} slot at the ending of each assignment, as follows:

\lstset{language=XML}
\begin{lstlisting}
 <!-- set right after execution ends -->
<assignpost>
  <atom>
    <!-- set object which name is equal to the one stored in var -->
    <slot name="this" var="present_user_atom"/>
    <slot name="uu_unhandled" val="false"/>
  </atom>
</assignpost>
\end{lstlisting}

In the above example, the slot \texttt{uu_unhandled} of the atom whose \texttt{"this"} slot value is equal to the variable value \texttt{"present_user_atom"} is assigned to the value \texttt{"false"}. (Read it again carefully)


