TESTS 2015-01
=============

The `Iteraction Manager` (IM) Big Box is automatically started when you launch the `mbot.launch`

## Using the interaction manager
You can chekc the different CAs in the following way.
At this point, the IM is able to give greetings to a person and receive tow different commans.

1. Give greetings:
  - Simply approach an RFID tag to the robot

2. Accept command:
  - `FollowMe`: Touch the robot's *right arm*
  - `TakeMeToRoomX`: Touch the robot's *left arm*
  - `AbortCommand`: Touch the robot's *head*

**Known Issues**

Right now, these commands aren't sent to the planner. 
They simply generate some NonVerbalSounds and express some gestures.



## External Dependencies:

This new version has a new external dependency: `rospy_utils`. 
To install it, you need to follow these steps:

    $ cd <your_catkin_ws_dir>/src
    $ git clone https://github.com/UC3MSocialRobots/rospy_utils.git
    $ rospack profile
    $ roscd rospy_utils
    $ # Install dependencies of the package
    $ sudo pip install -r requirements.txt

You can also test it by runing `nosetests` or `tox` inside the `rospy_utils` dir.



---

IPOL TESTS 2014-10
==================

To launch each experiment you need to change the init_file in the argument of the interaction manager section of the global mbot launcher.

For instance, to launch experiment 1, replace: 
      <arg name="init_file" value="init_monarch_interaction.xml" />
with:
      <arg name="init_file" value="init_ipol_experiment_1" />


The name of the experiments are:
    
    init_ipol_experiment_1
    init_ipol_experiment_2
    init_ipol_experiment_3
    init_ipol_experiment_6
    init_ipol_experiment_7
    init_ipol_experiment_8


To modify what the robot tells to each person you need to change this yaml file:
monarch_repo/code/trunk/catkin_ws/src/interaction_manager/monarch_multimodal_fusion/params/user_info.yaml

In the file there is an entry for each user to be supported. An entry example in Yaml would be:

    user_info: {
    '1': {
        uid: 1,
        name: Victor,
        surname: Gonzalez,
        welcome_msg: Hello victor. I'm glad to see you,
        non_verbal_sound: sound1,
        greetings_expression: greetings_1,
        role: staff,
        gender: male},
    '2':{
        uid: 2,
        name: Joao,
        surname: Sequeira,
        welcome_msg: Hi Joao. It's nice to have you here,
        non_verbal_sound: sound2,
        greetings_expression: greetings_2,
        role: staff,
        gender: male},
    '3':{
        uid: 3,
        name: Pedro,
        surname: Silva,
        welcome_msg: Hi Pedro. How do you feel today,
        non_verbal_sound: sound3,
        greetings_expression: greetings_3,
        role: child,
        gender: male},
    }

To know the meaning of each field of the yaml file is described in the own yaml file

To known which messages are sent to the Interaction Executor, you can see the 
following message definitions

MESSAGE DATA:
-------------

### Experiment 1
    
    NonVerbalSound (personalized user utterance)
        Type: NonVerbalSound.msg
        Topic: /mbotxy/play_sound
        Data: Depends on the user. You define it in the user_info.yaml
    EmotionalExpression
        Type: EmotionalExpression
        Topic: /mobotxy/express_emotion
        Data:
            emotion: "shake_head"
            Interfaces: in this case only the head :)
    
### Experiment 2

    EmotionalExpression
        Data : "shake_head"
    EmotionalExpression (for personalized eye blinks)
        Data : 
            * interface list : only eyes 
            * emotion : Depending on the user. You define it in the user_info.yaml
    
    Experiment 3
    ------------
    EmotionalExpression (shake_head, same as in experiment 1)
    NonVerbalSound (Personalized nvs, same as in experiment 1)
    EmotionalExpression (personalized eye blinking: same as in exp. 2)
    
    Experiment 4
    ------------
    There is no interaction in this experiment
    
    Experiment 5
    ------------
    There is no interaction in this experiment
    
    Experiment 6
    ------------
    Utterance (verbal message spoken by the robot)
        Type: Utterance.msg
        Topic: mbotxy/say_sentence
        Data: the phrase to say. It is defined in the greetings_msg field of the user_info.yaml
    
    Experiment 7
    ------------
    EmotionalExpression (for personalized simle depeding if staff or child)
        Data: 
            * interface list: head and mouth
            * emotion 'greet_child' or 'greet_staff_member'
    Utterance to give greetings verbally to the person (same as experiment 6)
    
    Experiment 8
    ------------
    EmotionalExpression : Same as experiment 7, but adding more interfaces
        Data 
        * interface list : depending wether the user is staff or child: arms, head, eyes, mouth 
        * emotion : same as experiment 7
    NonVerbalSound
        Data: 'greet_child' or 'greet_staff_member'
    ALL Experiments
        all previously described topics, except Utterance (6.1):
            'not_found' ---> if the detected RFID tag is not in the user_info.yaml file
        Utterance:
            Data: a sentence saying that the detected RFID is not in the robot's database (the yaml file :).

---

1st INTEGRATION WEEK VERSION
============================
At this version, the interaction is just a demo of the interaction capabilities of the robot. 
It demonstrates that the pipeline between the Planner and the HRI interfaces is connected. 
When you approach the robot with an RFID tag, it will say hi and it will ask your name. 
There are a set of names written in this

To use interaction, you simply need to follow these steps:

1. Launch the main mbot launcher
2. In the windows machine, launch the shortcut that runs the proxy and the interfaces
3. Send the greetings behavior from the planner to your MBot
4. Approach an RFID tag to the robot's head. The robot will salute you and ask your name.
5. Answer the robot in Portuguese. Note that the grammar incorporates a set of names
6. The robot will tell you that it likes your name :)
7. That's it

Notes:
· If you want to launch the interaction again, you will need to reactivate the greetings plan in the planner
· Note that the grammar does not incorporate all the possible names, just a few of them: Pedro, Joao, Victor, etc. See YDreams' grammar section for more info.
