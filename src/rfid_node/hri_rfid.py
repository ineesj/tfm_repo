#!/usr/bin/env python
# license removed for brevity
import rospy
import subprocess
import time
from std_msgs.msg import String
from monarch_msgs.msg import *
from random import randint

ros_namespace=None
move_head = 1
sound=''


def set_leds(device,r,g,b,vel):
    leds = LedControl()
    leds.device=device
    leds.r=r
    leds.g=g
    leds.b=b
    leds.change_time=vel
    #print device, r, g, b
    #print leds
    pub.publish(leds)
    time.sleep(0.3)

def start():
    #Starts normal
    global ros_namespace, pub, pub_mouth,pub_head

    list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()
    for string in list_output.split("\n"):
        if string != "":
            ros_namespace = string
            break

    rospy.init_node('rfid_leds', anonymous=True)

    topic_2_pub = '/'+ ros_namespace +'/cmd_leds'
    pub = rospy.Publisher(topic_2_pub, LedControl)

    topic_2_pub_mouth = '/'+ ros_namespace +'/cmd_mouth'
    pub_mouth = rospy.Publisher(topic_2_pub_mouth, MouthLedControl)

    topic_2_pub_head = '/'+ ros_namespace +'/cmd_head'
    pub_head = rospy.Publisher(topic_2_pub_head, HeadControl)

    
    # Initial State
    set_leds(0,0,50,50,100) #Left Eye
    set_leds(0,0,50,50,100) # -------
    set_leds(1,0,50,50,0)   #Right Eye
    set_leds(2,50,0,0,0)    #Cheeks
    set_leds(3,0,0,0,0)  #Right Base
    set_leds(4,0,0,0,0)  #Center Base
    set_leds(5,0,0,0,0)  #Left Base
    mouth_normal()
    
    #Test for moving head - -90 to 90 degrees and back to 0
    """head=HeadControl()
    head.movement_speed=70
    head.head_pos=-1.5
    pub_head.publish(head)
    rospy.sleep(5)
    head.head_pos=1.5
    pub_head.publish(head)
    rospy.sleep(5)
    head.head_pos=0
    pub_head.publish(head)"""


def stop():
    set_leds(0,0,0,0,0)
    set_leds(1,0,0,0,0)
    set_leds(2,0,0,0,0)
    set_leds(3,0,0,0,0)
    set_leds(4,0,0,0,0)
    set_leds(5,0,0,0,0)


def mouth_normal():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*8+[True]*16+[False]*8
    mouth.data=mouth.data+[False]*7+[True]+[False]*16+[True]+[False]*7
    mouth.data=mouth.data+[False]*6+[True]+[False]*18+[True]+[False]*6
    mouth.data=mouth.data+[False]*5+[True]+[False]*20+[True]+[False]*5
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*32

    mouth.data=mouth.data[::-1]

    #print mouth.data
    time.sleep(1)
    pub_mouth.publish(mouth)


def mouth_sad():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*8+[True]*16+[False]*8
    mouth.data=mouth.data+[False]*7+[True]+[False]*16+[True]+[False]*7
    mouth.data=mouth.data+[False]*6+[True]+[False]*18+[True]+[False]*6
    mouth.data=mouth.data+[False]*5+[True]+[False]*20+[True]+[False]*5
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*32

    time.sleep(1)
    pub_mouth.publish(mouth)

def mouth_happy():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*1+[True]*30+[False]*1
    mouth.data=mouth.data+[False]*3+[True]*2+[False]*22+[True]*2+[False]*3
    mouth.data=mouth.data+[False]*5+[True]*2+[False]*18+[True]*2+[False]*5
    mouth.data=mouth.data+[False]*7+[True]*2+[False]*14+[True]*2+[False]*7
    mouth.data=mouth.data+[False]*9+[True]*3+[False]*8+[True]*3+[False]*9
    mouth.data=mouth.data+[False]*12+[True]*8+[False]*12
    mouth.data=mouth.data+[False]*32

    time.sleep(1)
    pub_mouth.publish(mouth)


def eyes_play():

    set_leds(0,50,0,50,100)
    set_leds(1,50,0,50,100)
    set_leds(0,0,50,0,100)
    set_leds(1,0,50,0,100)
    set_leds(0,0,0,50,100)
    set_leds(1,0,0,50,100)

    set_leds(0,0,50,50,100) #back to default
    set_leds(1,0,50,50,0)   #---------------


def callback(data):

    ######## HEAD CONTROL and base LEDs###############
    head = HeadControl()
    rand = randint(-5,5)
    rand_ang = rand*3.14/180

    if data.tag_angle < 0:
        led = 3
        if move_head == 1:
            head.head_pos = -0.78+rand_ang #move -45 degrees + rand(-5,5)
            #print head.head_pos
            head.movement_speed = 70
            pub_head.publish(head)
    elif data.tag_angle >= 0:
        led = 5
        if move_head == 1:
            head.head_pos = 0.78+rand_ang #move 45 degrees + rand(-5,5)
            #print head.head_pos
            head.movement_speed = 70
            pub_head.publish(head)    
    
    set_leds(led,0,0,100,50)

    reset=0
    if data.tag_angle > -90 and data.tag_angle < 90:
        set_leds(4,0,0,100,50)
        reset=1
    #############################################

    mouth_happy()
    
    #Play one of the sounds
    if sound == 'voice':
        num = randint(1,9)
        subprocess.call("mpg321 mp3/%s_%d.mp3 -g 60" % (sound,num),shell=True)
    else:
        num = randint(0,6)
        subprocess.call("mpg321 mp3/%s_%d.mp3 -g 30" % (sound,num),shell=True)

    eyes_play()

    time.sleep(4)
    
    head.head_pos=0
    pub_head.publish(head)

    set_leds(led,0,0,0,50)
    if reset==1:
        set_leds(4,0,0,0,50)
    mouth_normal()

def listener():

    topic_2_sub = '/'+ ros_namespace +'/rfid_position'
    rospy.Subscriber(topic_2_sub, RfidReading, callback, queue_size=1)
    rospy.spin()


if __name__ == '__main__':

    if len(sys.argv)== 2 and sys.argv[1]=='s':
        sound = 'r2d2'
    elif len(sys.argv)== 2 and sys.argv[1]=='v':
        sound = 'voice'
    else:
        sound = 'r2d2'

    start()

    try:
        listener()
    except rospy.ROSInterruptException:
        pass
