#!/usr/bin/env python

# Code to get samples for robot localization algorithm

# --- duarte.gameiro, 2015 ---

import rospy, subprocess
from sklearn import svm
from scipy import io as spio
import numpy as np
import cPickle
import database_tags as DB
from monarch_msgs.msg import RfidReading

def tag_callback(data):
    global tags
    t=data.tag_id
    if tags.has_key(t):
        tags[t]+=1
    else:
        tags[t]=1

if __name__ == '__main__':

    n = 10 #total number of tags
    tags = {}       #dictionary of tags
    ttt = spio.loadmat('svm/svm_trainA.mat')
    train = ttt['train']
    #print "train: ",train.shape
    label = np.ravel(ttt['label'])
    #print "label: ", label.shape
    clf = svm.SVC()
    clf.fit(train, label)
    #with open('svm/svm_zoneA.pkl', 'rb') as fid:
    #    clf = cPickle.load(fid)

    try:
        rospy.init_node('rfid_localization_predict_zoneA')
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        rospy.Subscriber(ros_namespace+'/rfid_tag',RfidReading,tag_callback)

        time = rospy.get_time()
        rate = 1 # seconds per sample

        test = np.zeros((n*2,), dtype=np.int)
        while not rospy.is_shutdown():
            if rospy.get_time() > time+rate:
                for ID in tags:
                    print "ID:",ID,"-->",tags[ID]
                    tag_index = DB.get_index(str(ID))
                    test[tag_index] = 1
                    test[tag_index + n] = tags[ID]
                #PREDICT
                print "test: ", test
                prediction = clf.predict([test])
                print prediction
                
                #RESET
                tags = {}
                test = np.zeros((n*2,), dtype=np.int)
                time = rospy.get_time()

    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        # quit
        sys.exit()