#!/usr/bin/env python

# single tag probability computation - 

# --- duarte.gameiro, 2015 ---

import rospy
from monarch_msgs.msg import RfidReading
from std_msgs.msg import UInt64
from sam_helpers.writer import SAMWriter
import subprocess

def callback(data):
    global detect,tagID
    
    if tagID == 0:
        reading.tag_id = data.tag_id
        tagID = data.tag_id
        pub_first.publish(tagID)
        if samString:
            samWriter.publish(tagID)
    
    if data.tag_id == tagID:
        detect=detect+1

if __name__ == '__main__':
    detect = 0
    tagID = 0
    freq = 100

    rospy.init_node('rfid_prob')
    reading = RfidReading()
    pub = rospy.Publisher('rfid_prob', RfidReading)
    pub_first = rospy.Publisher('rfid_first_tag', UInt64) #publishes a flag the first time a new tag is detected

    list_cmd = subprocess.Popen("rosnode list | grep sam_node", shell = True, stdout = subprocess.PIPE)
    samString = list_cmd.stdout.read()
    if samString:
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        samWriter = SAMWriter("RFIDTagID",ros_namespace)
    rospy.Subscriber('rfid_tag', RfidReading, callback)

    while not rospy.is_shutdown():
        if detect > 0:
            for i in range(freq):
                rospy.sleep(0.05)
            prob = float(detect)/freq
            prob = float("{0:.2f}".format(prob))
            if prob != 0.0:
                if prob > 1.0:
                    prob = 1.0
                reading.header.stamp = rospy.Time.now()
                reading.tag_prob = prob
                pub.publish(reading)
            detect = 0
            tagID = 0
