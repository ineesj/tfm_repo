#!/usr/bin/env python

# Multiple tag probability computation

# --- duarte.gameiro, 2015 ---

import rospy
from monarch_msgs.msg import RfidReading
from std_msgs.msg import UInt64
from sam_helpers.writer import SAMWriter
import subprocess

def callback(data):
    global tagCount,tagTime,duration,pub_first

    tagID = str(data.tag_id)
    #print "tagID: ",tagID

    if tagCount.has_key(tagID):
        #print "Tag ",tagID, " already in the dict"
        tagCount[tagID]+=1
    else:
        #print "Tag ",tagID, " not in the dict"
        tagCount[tagID]=1
        tagTime[tagID]=rospy.get_time()+duration
        pub_first.publish(tagID)
        #if samString:                      ### COMMENTED, was giving an error. Unresolved.
        #    samWriter.publish(tagID)

if __name__ == '__main__':
    try:
        tagCount = {}
        tagTime = {}
        duration = 5 #secs that detection lasts
        freq = duration*20

        rospy.init_node('tag_prob_mult')

        pub = rospy.Publisher('rfid_prob_mult',RfidReading)
        pub_first = rospy.Publisher('rfid_first_tag', UInt64) #publishes a flag the first time a new tag is detected

        list_cmd = subprocess.Popen("rosnode list | grep sam_node", shell = True, stdout = subprocess.PIPE)
        samString = list_cmd.stdout.read()
        if samString:
            list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
            list_output = list_cmd.stdout.read()
            for string in list_output.split("\n"):
                if string != "":
                    ros_namespace = string
                    break
            samWriter = SAMWriter("RFIDTagID",ros_namespace)

        rospy.Subscriber("rfid_tag", RfidReading, callback)

        while not rospy.is_shutdown():
            if tagTime:
                if rospy.get_time() >= tagTime.values()[0]:
                    key = tagTime.keys()[0]
                    #print key
                    #print rospy.get_time()
                    #print tagTime.values()[0]
                    #print tagCount[key]
                    prob = float(tagCount[key])/freq
                    prob = float("{0:.2f}".format(prob))
                    #print "prob: ", prob
                    if prob > 1.0:
                        prob = 1.0
                    reading=RfidReading()
                    reading.header.stamp = rospy.Time.now()
                    reading.tag_id = int(key)
                    reading.tag_prob = prob
                    pub.publish(reading)
                    tagCount.pop(key)
                    tagTime.pop(key)

    except rospy.ROSInterruptException:
        pass