#!/usr/bin/env python
import subprocess,time

list_cmd = subprocess.Popen("ls", shell = True, stdout = subprocess.PIPE)
list_output = list_cmd.stdout.read()
#print list_output
for string in list_output.split("\n"):
    if string[0:16] == 'person_detection':
        print string
        list_cmd1 = subprocess.Popen("rosbag info -y -k duration %s" % string, shell = True, stdout = subprocess.PIPE)
        t_str = (list_cmd1.stdout.read())
        t = float(t_str[0:-1])
        print t
        list_cmd2 = subprocess.Popen("rosbag play %s" % string, shell = True, stdout = subprocess.PIPE)
        time.sleep(t)