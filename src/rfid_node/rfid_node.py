#!/usr/bin/env python

# basic tag reader
# tag ids are identified with 4 digits
# --- joao.sequeira,  2014 ---
# --- duarte.gameiro, 2014 ---

import rospy
import serial,sys
from monarch_msgs.msg import RfidReading
import database_tags as tagDB

#########################
# open the serial port
# default is 8 bits, no parity, 1 stop bit
def serial_port_open():
    ser = serial.Serial('/dev/mbot-rfidreader', 57600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)

    if ser.isOpen()==False:
        ser.open()
    return ser

#########################
# read full msgs
def read_full_msg(ser):
    k = 0
    while ser.inWaiting()>0:
        #print ser.inWaiting()
        rxd_list = ser.read(ser.inWaiting())
        return rxd_list
        k = k + 1
        # in case there are chars left in the buffer they are simply re-read in the next iteration


##################################
# CRC for the RFID reader msgs
def CRC16_get( pucY, ucX):

    PRESET_VALUE = int('0xffff', 16)
    POLYNOMIAL = int('0x8408', 16)

    uiCrcValue = PRESET_VALUE
    range_ucX = range(ucX)

    for ucI in range_ucX:
        uiCrcValue = uiCrcValue ^ ord(pucY[ucI])
        range_ucJ = range(8)
        for ucJ in range_ucJ:
            if uiCrcValue & int('0x0001', 16):
                uiCrcValue = (uiCrcValue >> 1) ^ POLYNOMIAL
            else:
                uiCrcValue = (uiCrcValue >> 1)
    return uiCrcValue

#########################
# get Work Mode
def Get_WorkMode(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 4), 'Latin-1')
    # default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command -  (section 8.4.10)
    dataBlock += bytearray(unichr(int('0x36',16)), 'Latin-1')
        
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    #print 'total txd msg len ', len(msg), ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock

#########################
# set Work Mode
def Set_WorkMode(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 10), 'Latin-1')
    # default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command -  (section 8.4.9)
    dataBlock += bytearray(unichr(int('0x35',16)), 'Latin-1')

    #data
    #byte1 - Read_mode - 0x00 for Answer Mode - 0x01 for Scan Mode
    dataBlock += bytearray(unichr(int('0x00',16)), 'Latin-1')
    #byte2 - Mode_state (0x02 Beep ON, 0x06 Beep OFF)
    dataBlock += bytearray(unichr(int('0x06',16)), 'Latin-1')
    #byte3 - Mem_Iven
    dataBlock += bytearray(unichr(int('0x01',16)), 'Latin-1')
    #byte4 - First_Adr
    dataBlock += bytearray(unichr(int('0x00',16)), 'Latin-1')
    #byte5 - Word_Num
    dataBlock += bytearray(unichr(int('0x01',16)), 'Latin-1')
    #byte6 - Tag_Time
    dataBlock += bytearray(unichr(int('0x00',16)), 'Latin-1')
        
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    #print ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock

#########################
# set output power of the reader
def Set_Power(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 5), 'Latin-1')
    # default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command -  (section 8.4.6)
    dataBlock += bytearray(unichr(int('0x2f',16)), 'Latin-1')
    # data (Power: 0~1e / 0~30 )
    dataBlock += bytearray(unichr(int('0x1e',16)), 'Latin-1')
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    #print 'total txd msg len ', len(msg), ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock



##################################
# compose a GetReader Information message
def Get_Reader_Information(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 4), 'Latin-1')
    # default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command - Get Reader Information (section 8.4)
    dataBlock += bytearray(unichr(int('0x21',16)), 'Latin-1')
    # data
    #if msg:
    #    dataBlock += bytearray(msg, 'Latin-1')
        
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    #print 'total txd msg len ', len(msg), ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock


##################################
# compose a message to write to the serial line
# the CRC is applied to the whole message
def Inventory(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 4), 'Latin-1')
    # the default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command - Inventory (section 8.2.1)
    dataBlock += bytearray(unichr(int('0x01',16)), 'Latin-1')
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    # print 'total txd msg len ', len(msg), ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock


##################################
# parses a received msg - simply a dumb if-then-else parser
# returns any undecoded part of the received message (to be completed with further chars to be received)
# this function assumes that synch occurs at the very begining of the run of the program
# no further synch is done - to be upgraded later
def msg_parse( buffered_msg, pub ):

    tag_numID = 0
    msg_idx = 0

    while msg_idx < len(buffered_msg):

        msg = buffered_msg[msg_idx:]
        
        len_msg = ord( msg[0] ) + 1
        if len_msg > len(msg):
            #print 'incomplete message received - bayling out msg_decode\n'
            return msg
    
        adr_msg = ord( msg[1] )
        reCmd_msg = ord( msg[2] )
        status_msg = ord( msg[3] )
        data_msg = msg[4:len_msg-2]
        crc_LSB = ord( msg[len_msg-2] )
        crc_MSB = ord( msg[len_msg-1] )


        #print 'len ', len_msg, ' adr ', adr_msg, ' reCmd ', reCmd_msg, ' status ', status_msg, '\n'

        check_crc = CRC16_get( msg[0:len_msg-2], len_msg-2 )
        check_crc_LSB = check_crc % 256
        check_crc_MSB = check_crc // 256

        if (crc_LSB!=check_crc_LSB) and (crc_MSB!=check_crc_MSB):
            pass
            #print 'crc error in message\n'
            #print 'crc LSB ', crc_LSB, check_crc_LSB, ' crc MSB ', crc_MSB, check_crc_MSB, '\n'

        if reCmd_msg==0:
            #print 'unrecognized command\n'
            pass

        ### ANSWER MODE - Inventory
        if reCmd_msg==1:
            if data_msg:
                
                num_tags = int(data_msg[0].encode('hex'),16)
                #print num_tags
                #print 'number of tags detected:', num_tags
                tagEPC = data_msg[1:].encode('hex')           
                len_tagEPC = len(tagEPC)
                if num_tags>0:
                    dim_tag = len_tagEPC/num_tags
        
                    reading = RfidReading()
                    for i in range(num_tags):
                        tag_numID = int(tagEPC[(i+1)*dim_tag-4:(i+1)*dim_tag],16)
                        #print tag_numID
                        reading.header.stamp = rospy.Time.now()
                        reading.tag_id = tag_numID
                        reading.tag_type,reading.tag_info = tagDB.tag_search(str(tag_numID))
                        pub.publish(reading)
        

    ### Read-Defined Commands
        if status_msg==0:
            #print 'len_msg:', len_msg, '--- adr_msg:', adr_msg
            #print 'sucessful command:', hex(reCmd_msg), '--- status:', status_msg, '--- length_data:', len(data_msg),'\n'

            if reCmd_msg==int('0x21',16):
                # get reader info
                Version = int(data_msg[0].encode('hex'),16)
                subVersion = int(data_msg[1].encode('hex'),16)
                readerType = int(data_msg[2].encode('hex'),16)
                Tr_type_bit1 = (int(data_msg[3].encode('hex'),16) & int('0x02', 16))
                Tr_type_bit0 = (int(data_msg[3].encode('hex'),16) & int('0x01', 16))
                DMaxFre_band = (int(data_msg[4].encode('hex'),16) & int('0xc0', 16))
                DmaxFre_maxFre = (int(data_msg[4].encode('hex'),16) & int('0x3f', 16))
                DMinFre_band = (int(data_msg[5].encode('hex'),16) & int('0xc0', 16))
                DminFre_minFre = (int(data_msg[5].encode('hex'),16) & int('0x3f', 16))
                Power = int(data_msg[6].encode('hex'),16)
                Scntm = int(data_msg[7].encode('hex'),16)

                """print 'Version ', Version, ' subVersion ', int(subVersion), '\n'
                print 'reader type ', readerType, '\n'
                print 'protocol info ', Tr_type_bit1, '   ', Tr_type_bit0, '\n'
                print 'freq band ', DMaxFre_band, ' max freq ', DmaxFre_maxFre, '\n'
                print 'freq band ', DMinFre_band, ' min freq ', DminFre_minFre, '\n'
                print 'power ', Power, '\n'
                print 'inventory scan time ', Scntm, '\n'"""
                
            elif reCmd_msg==int('0x36',16):
                #get work mode
                Wg_mode = int(data_msg[0].encode('hex'),16)
                Wg_Data_Interval = int(data_msg[1].encode('hex'),16)
                Wg_Pulse_Width = int(data_msg[2].encode('hex'),16)
                Wg_Pulse_Interval = int(data_msg[3].encode('hex'),16)
                Read_mode = int(data_msg[4].encode('hex'),16)
                Mode_state = int(data_msg[5].encode('hex'),16)
                Mem_Iven = int(data_msg[6].encode('hex'),16)
                First_Adr = int(data_msg[7].encode('hex'),16)
                Word_Num = int(data_msg[8].encode('hex'),16)
                Tag_Time = int(data_msg[9].encode('hex'),16)
                accuracy = int(data_msg[10].encode('hex'),16)
                OffsetTime = int(data_msg[11].encode('hex'),16)

                """print 'Wg_mode:', Wg_mode
                print 'Wg_Data_Interval:',Wg_Data_Interval
                print 'Wg_Pulse_Width:', Wg_Pulse_Width
                print 'Wg_Pulse_Interval:', Wg_Pulse_Interval
                print 'Read_mode:', Read_mode
                print 'Mode_state:', Mode_state
                print 'Mem_Iven:', Mem_Iven
                print 'First_Adr:', First_Adr
                print 'Word_Num:', Word_Num
                print 'Tag_Time:', Tag_Time
                print 'accuracy:', accuracy
                print 'OffsetTime:', OffsetTime,'\n'"""

                if Read_mode==1:
                    sys.exit('Error: Reader in Scan Mode. Run setup.py to change to Answer Mode')

        msg_idx += len_msg

    # if it gets here it's because there's no leftover string
    # print 'full string processed'
    return ''

# processes incoming response from the reader after sending commands.
def proccess_msg(ser,rxd_msg,pub):
    aux = read_full_msg(ser)
    if aux:
        rxd_msg += aux
    else:
        sys.exit('Error: RFID reader not working.\nCheck connections.\nCheck if other processes \
are trying to access the RFID reader.\nIf it persists check README file.')
    if rxd_msg:
        leftover_msg = msg_parse(rxd_msg,pub)
    else:
        leftover_msg = ''
    rxd_msg = leftover_msg
    return rxd_msg


##########################################################################
# main code
if __name__ == '__main__':
    try:
        ser = serial_port_open()
        
        rospy.init_node('rfid_get_tag')
        pub = rospy.Publisher('rfid_tag',RfidReading)
        rxd_msg = ''

        txd_msg = Get_WorkMode(ser)
        rospy.sleep(0.1)
        rxd_msg = proccess_msg(ser,rxd_msg,pub)

        while not rospy.is_shutdown():
            txd_msg = Inventory(ser) # testing the inventory of tags
            rospy.sleep(0.05)
            rxd_msg = proccess_msg(ser,rxd_msg,pub)

    except rospy.ROSInterruptException:
        pass

    ser.close()
