#!/usr/bin/env python

### angle_person - Publishes the tag angle relative to the robot
# Now also publishes position of tag relative to the robot

### duarte.gameiro,2015

import rospy
import numpy as np
from scipy import io as spio
from monarch_msgs.msg import RfidReading
from skimage.morphology import medial_axis
from sam_helpers.writer import SAMWriter
import subprocess

def callback(data):
    prob = data.tag_prob

    idx_bool = np.logical_and(prob_map>prob-thresh, prob_map<prob+thresh)
    prob_idx = np.column_stack(np.where(idx_bool))
    prob_xy = np.array([(40-prob_idx[:,1])*-res,(40-prob_idx[:,0])*-res]).T #conversion from 81x81 image to xy coordinates

    binary_map = np.zeros(prob_map.shape,dtype=np.int)
    binary_map[len(prob_map)-1-prob_idx[::,0],prob_idx[:,1]] = 1  #len(prob_map)-1-prob_idx[::,0] - the y axis in images is inverted.
    skel, distance = medial_axis(binary_map, return_distance=True) # Compute the medial axis (skeleton) and the distance transform
    dist_on_skel = distance * skel # Distance to the background for pixels of the skeleton

    skel_idx = np.column_stack(np.where(skel))
    skel_xy = np.array([(40-skel_idx[:,1])*-.05,(40-skel_idx[:,0])*.05]).T #inverted collumns

    row,col = np.unravel_index(dist_on_skel.argmax(), dist_on_skel.shape) #row collumn for maximum value of distance to border
    tag_est = np.array([(40-col)*-.05,(40-row)*.05])
    
    angle = np.arctan2(tag_est[1],tag_est[0]) * 180 / np.pi - 90 # -90: the robot is facing up - origin of orientation is the vertical axis
    angle = float("{0:.2f}".format(angle))
    
    reading = RfidReading()
    reading.header.stamp = rospy.Time.now()
    reading.tag_id = data.tag_id
    reading.tag_prob = prob
    reading.tag_position.x = tag_est[0]
    reading.tag_position.y = tag_est[1]
    reading.tag_position.z = 0
    reading.tag_angle = angle
    
    pub.publish(reading)
    if samString:
        samWriter.publish(reading)
    
if __name__ == '__main__':
    
    #Probability Model
    model = spio.loadmat('mbot01_model.mat')
    prob_map = model['fit_cubic']
    res = model['resolution'][0][0]

    #Parameters
    thresh = 0.1

    try:
        if rospy.search_param('bf'):
            if rospy.get_param('bf')==0:
                pass
            elif rospy.get_param('bf') == 1:
                prob_map[:41][:] = float('NaN') #Front
                print 'Mask applied: Person in front of the robot'
            elif rospy.get_param('bf') == 2:
                prob_map[42:][:] = float('NaN') #Back
                print 'Mask applied: Person behind the robot'
            else:
                print 'No mask applied. Front: 1, Back: 2'

        rospy.init_node('rfid_tag_position')
        pub = rospy.Publisher('rfid_position', RfidReading)
        rospy.Subscriber('rfid_prob', RfidReading, callback)

        list_cmd = subprocess.Popen("rosnode list | grep sam_node", shell = True, stdout = subprocess.PIPE)
        samString = list_cmd.stdout.read()
        if samString:
            list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
            list_output = list_cmd.stdout.read()
            for string in list_output.split("\n"):
                if string != "":
                    ros_namespace = string
                    break
            samWriter = SAMWriter("RFIDAngle",ros_namespace)
        
        rospy.spin()
    except rospy.ROSInterruptException:
        pass