import numpy as np
from scipy import io as spio
from sklearn import svm
from sklearn.externals import joblib

zones = ['A','B','C','D','E']
for s in zones:
        mat_SVM = spio.loadmat('svm/svm_train%s.mat' % s)
        train = mat_SVM['train']
        label = np.ravel(mat_SVM['label'])
        clf = svm.SVC()
        clf.fit(train, label)
        joblib.dump(clf, 'svm/svm_zone%s.pkl' % s)

classifiers = []
for s in zones:
    clf = joblib.load('svm/svm_zone%s.pkl' % s)
    classifiers.append(clf)