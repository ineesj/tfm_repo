#!/usr/bin/env python

# Robot localization with RFID tags, using SVMs
# Author: Duarte Gameiro
# Description: 

import rospy, subprocess, sys
import numpy as np
import database_tags as DB
from sklearn.externals import joblib
from geometry_msgs.msg import PoseWithCovarianceStamped
from monarch_msgs.msg import RfidReading

n_tags = 10     # number of tags in environment
rate = 1 # seconds per sample
zones = ['A','B','C','D','E']

class RfidTags:
    """RfidTags collects the detected tags"""
    def __init__(self,n_tags):
        self.n_tags = n_tags
        self.detected_tags = {}
        self.total_detections = 0

### READING TAG CALLBACK ###
def reading_tag(data,rf):
    #global detected_tags,total_detections
    t=data.tag_id
    if rf.detected_tags.has_key(t):
        rf.detected_tags[t]+=1
        rf.total_detections+=1
    else:
        rf.detected_tags[t]=1
        rf.total_detections += 1


### LOAD SVM CLASSIFIERS ###
def load_classifiers():
    classifiers = []
    for s in zones:
        clf = joblib.load('svm/svm_zone%s.pkl' % s)
        classifiers.append(clf)
    return classifiers


### TEST SVMs ###
def test_SVM(rf,clf_test):
    vector = np.zeros((rf.n_tags*2,), dtype=np.int)
    for ID in rf.detected_tags:
        tag_index = DB.get_index(str(ID))
        if tag_index:
            vector[tag_index] = 1
            vector[tag_index + rf.n_tags] = rf.detected_tags[ID]
    result = clf_test.predict([vector])
    return result,vector


### SVM CLASSIFICATION ###
def classification(clf,rf):
    #detected_tags = {}      #dictionary of tags per sample
    #total_detections = 0    #total number of tags detections per sample
    empty_count = 0
    time = rospy.get_time()
    while not rospy.is_shutdown():     
        if rospy.get_time()>time+rate:
            for index,clf_test in enumerate(clf):
                result,vector = test_SVM(rf,clf_test)
                if int(result) == 1:
                    predicted_zone = zones[index]
                    break
            print 'Robot in zone ',predicted_zone
            rf.detected_tags = {}
            rf.total_detections = 0
            time = rospy.get_time()


### GET MBOT NAME ###
def get_mbot_name():
    list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()
    for string in list_output.split("\n"):
        if string != "":
            mbot_name = string
            break
    return mbot_name

### MAIN ###
if __name__ == '__main__':
    try:
        rf = RfidTags(n_tags)
        rospy.init_node('rfid_robot_localization')
        ros_namespace = get_mbot_name()
        rospy.Subscriber(ros_namespace+'/rfid_tag',RfidReading,reading_tag,callback_args=rf)
        clf = load_classifiers()
        classification(clf,rf)
    except rospy.ROSInterruptException:
        pass