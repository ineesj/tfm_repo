#!/usr/bin/env python
# license removed for brevity
import rospy
import subprocess
import time
from std_msgs.msg import String
from monarch_msgs.msg import *

ros_namespace=None

def set_leds(device,r,g,b,vel):
    leds = LedControl()
    leds.device=device
    leds.r=r
    leds.g=g
    leds.b=b
    leds.change_time=vel

    print device, r, g, b

    pub.publish(leds)
    time.sleep(0.3)

def stop():
    #Turn off leds
    global ros_namespace, pub, topic_2_pub, rate

    list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()

    for string in list_output.split("\n"):
        if string != "":
            ros_namespace = string
            break
    #device,r,g,b,vel

    rospy.init_node('stop_leds', anonymous=True)

    topic_2_pub = '/'+ ros_namespace +'/cmd_leds'

    pub = rospy.Publisher(topic_2_pub, LedControl)

    set_leds(0,0,0,0,0)
    set_leds(0,0,0,0,0)
    set_leds(1,0,0,0,0)
    set_leds(2,0,0,0,0)
    set_leds(3,0,0,0,0)
    set_leds(4,0,0,0,0)
    set_leds(5,0,0,0,0) 

    mouth_stop()

def mouth_stop():

    topic_2_pub_head = '/'+ ros_namespace +'/cmd_mouth'

    pub_head = rospy.Publisher(topic_2_pub_head, MouthLedControl)

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*8+[False]*16+[False]*8
    mouth.data=mouth.data+[False]*7+[False]+[False]*16+[False]+[False]*7
    mouth.data=mouth.data+[False]*6+[False]+[False]*18+[False]+[False]*6
    mouth.data=mouth.data+[False]*5+[False]+[False]*20+[False]+[False]*5
    mouth.data=mouth.data+[False]*4+[False]+[False]*22+[False]+[False]*4
    mouth.data=mouth.data+[False]*4+[False]+[False]*22+[False]+[False]*4
    mouth.data=mouth.data+[False]*32

    print "turning off mouth leds..."

    #print mouth.data
    time.sleep(1)
    pub_head.publish(mouth)


if __name__ == '__main__':
    stop()
    #time.sleep(1)
    