#!/usr/bin/env python

# Multiple tag probability computation

# --- duarte.gameiro, 2015 ---

import rospy
from monarch_msgs.msg import RfidReading
from std_msgs.msg import UInt64
from sam_helpers.writer import SAMWriter
import subprocess

ros_namespace=None
duration = 2 #secs that detection lasts

class TagArray():
    def __init__(self,pub_first):
        self.tagCount = {}
        self.tagTime = {}
        self.pub_first=pub_first

def callback(data,tag_array,samWriter=0):
    global duration

    tagID = str(data.tag_id)
    #print "tagID: ",tagID

    if tag_array.tagCount.has_key(tagID):
        #print "Tag ",tagID, " already in the dict"
        tag_array.tagCount[tagID]+=1
    else:
        #print "Tag ",tagID, " not in the dict"
        tag_array.tagCount[tagID]=1
        tag_array.tagTime[tagID]=rospy.get_time()+duration
        tag_array.pub_first.publish(int(tagID))
        if samWriter!=0:
            samWriter.publish(int(tagID))

def main():
    global ros_namespace,duration
    freq = duration*20

    rospy.init_node('rfid_tag_probability')
    
    pub = rospy.Publisher('rfid_prob',RfidReading)
    pub_first = rospy.Publisher('rfid_first_tag', UInt64) #publishes a flag the first time a new tag is detected

    tag_array = TagArray(pub_first)

    list_cmd = subprocess.Popen("rosnode list | grep sam_node", shell = True, stdout = subprocess.PIPE)
    samString = list_cmd.stdout.read()
    if samString:
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        samWriter = SAMWriter("RFIDTagID",ros_namespace)
        rospy.Subscriber("rfid_tag", RfidReading, callback,callback_args=[tag_array,samWriter])
    else:
        rospy.Subscriber("rfid_tag", RfidReading, callback,callback_args=tag_array)

    try:
        while not rospy.is_shutdown():
            if tag_array.tagTime:
                if rospy.get_time() >= tag_array.tagTime.values()[0]:
                    key = tag_array.tagTime.keys()[0]
                    prob = float(tag_array.tagCount[key])/freq
                    prob = float("{0:.2f}".format(prob))
                    if prob > 1.0:
                        prob = 1.0
                    reading=RfidReading()
                    reading.header.stamp = rospy.Time.now()
                    reading.tag_id = int(key)
                    reading.tag_prob = prob
                    pub.publish(reading)
                    tag_array.tagCount.pop(key)
                    tag_array.tagTime.pop(key)

    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
    