#!/usr/bin/env python

#Reader Setup

import rospy
import serial,sys

#########################
# open the serial port
# default is 8 bits, no parity, 1 stop bit
def serial_port_open():
    ser = serial.Serial('/dev/mbot-rfidreader', 57600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)

    if ser.isOpen()==False:
        ser.open()
    return ser

#########################
# read full msgs
def read_full_msg(ser):
    k = 0
    while ser.inWaiting()>0:
        #print ser.inWaiting()
        rxd_list = ser.read(ser.inWaiting())
        return rxd_list
        k = k + 1
        # in case there are chars left in the buffer they are simply re-read in the next iteration

##################################
# CRC for the RFID reader msgs
def CRC16_get( pucY, ucX):

    PRESET_VALUE = int('0xffff', 16)
    POLYNOMIAL = int('0x8408', 16)

    uiCrcValue = PRESET_VALUE
    range_ucX = range(ucX)

    for ucI in range_ucX:
        uiCrcValue = uiCrcValue ^ ord(pucY[ucI])
        range_ucJ = range(8)
        for ucJ in range_ucJ:
            if uiCrcValue & int('0x0001', 16):
                uiCrcValue = (uiCrcValue >> 1) ^ POLYNOMIAL
            else:
                uiCrcValue = (uiCrcValue >> 1)
    return uiCrcValue

#########################
# get Work Mode
def Get_WorkMode(ser):

    len_msg = 0
    # len
    dataBlock = bytearray(unichr(len_msg + 4), 'Latin-1')
    # default address for the reader is 0x00
    dataBlock += bytearray(unichr(int('0x00', 16)), 'Latin-1')
    # command -  (section 8.4.10)
    dataBlock += bytearray(unichr(int('0x36',16)), 'Latin-1')
        
    # the CRC starts from the len byte
    CrcValue = CRC16_get( str(dataBlock), len(dataBlock) )
    # low byte
    dataBlock += bytearray(unichr(CrcValue % 256), 'Latin-1')
    # high byte
    dataBlock += bytearray(unichr(CrcValue // 256), 'Latin-1')

    #print 'total txd msg len ', len(msg), ' total frame len ', len(dataBlock)
    ser.write( dataBlock )
    return dataBlock


##################################
# parses a received msg - simply a dumb if-then-else parser
# returns any undecoded part of the received message (to be completed with further chars to be received)
# this function assumes that synch occurs at the very begining of the run of the program
# no further synch is done - to be upgraded later
def msg_parse(buffered_msg):

    #array_tags = []
    tag_numID = 0
    msg_idx = 0


    while msg_idx < len(buffered_msg):

        msg = buffered_msg[msg_idx:]
        
        len_msg = ord( msg[0] ) + 1
        if len_msg > len(msg):
            print 'incomplete message received - bayling out msg_decode\n'
            return msg
    
        adr_msg = ord( msg[1] )
        reCmd_msg = ord( msg[2] )
        status_msg = ord( msg[3] )
        data_msg = msg[4:len_msg-2]
        crc_LSB = ord( msg[len_msg-2] )
        crc_MSB = ord( msg[len_msg-1] )


        #print 'len ', len_msg, ' adr ', adr_msg, ' reCmd ', reCmd_msg, ' status ', status_msg, '\n'

        check_crc = CRC16_get( msg[0:len_msg-2], len_msg-2 )
        check_crc_LSB = check_crc % 256
        check_crc_MSB = check_crc // 256

        if (crc_LSB!=check_crc_LSB) and (crc_MSB!=check_crc_MSB):
            pass
            #print 'crc error in message\n'
            #print 'crc LSB ', crc_LSB, check_crc_LSB, ' crc MSB ', crc_MSB, check_crc_MSB, '\n'

        if reCmd_msg==0:
            #print 'unrecognized command\n'
            pass

    ### Read-Defined Commands
        if status_msg==0:
            #print 'len_msg:', len_msg, '--- adr_msg:', adr_msg
            #print 'sucessful command:', hex(reCmd_msg), '--- status:', status_msg, '--- length_data:', len(data_msg),'\n'
    
            if reCmd_msg==int('0x36',16):
                #get work mode
                Wg_mode = int(data_msg[0].encode('hex'),16)
                Wg_Data_Interval = int(data_msg[1].encode('hex'),16)
                Wg_Pulse_Width = int(data_msg[2].encode('hex'),16)
                Wg_Pulse_Interval = int(data_msg[3].encode('hex'),16)
                Read_mode = int(data_msg[4].encode('hex'),16)
                Mode_state = int(data_msg[5].encode('hex'),16)
                Mem_Iven = int(data_msg[6].encode('hex'),16)
                First_Adr = int(data_msg[7].encode('hex'),16)
                Word_Num = int(data_msg[8].encode('hex'),16)
                Tag_Time = int(data_msg[9].encode('hex'),16)
                accuracy = int(data_msg[10].encode('hex'),16)
                OffsetTime = int(data_msg[11].encode('hex'),16)

                print 'Read_mode: ',
                if Read_mode==0:
                    print 'Answer Mode'
                elif Read_mode==1:
                    print 'Scan Mode'

                print 'Beep: ',
                if Mode_state==2:
                    print 'ON'
                elif Mode_state==6:
                    print 'OFF'

        msg_idx += len_msg

    # if it gets here it's because there's no leftover string
    # print 'full string processed'
    return ''

##########################################################################
# main code
if __name__ == '__main__':

    ser = serial_port_open()
    rxd_msg = ''
    reader_not_working = 0

    txd_msg = Get_WorkMode(ser)
    rospy.sleep(0.1)

    aux = read_full_msg(ser)
    if aux:
        rxd_msg += aux
    else:
        sys.exit('Error: RFID reader not working.\nCheck connections.\nCheck if other processes \
are trying to access the RFID reader.\nIf it persists check README file.')
    if rxd_msg:
        leftover_msg = msg_parse(rxd_msg)
    else:
        leftover_msg = ''
    rxd_msg = leftover_msg
        
    ser.close()
