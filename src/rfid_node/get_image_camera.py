#!/usr/bin/env python

import rospy,time,thread,sys
from scipy import io as spio
from sensor_msgs.msg import Image
import matplotlib.pyplot as plt
import StringIO
import numpy as np

class GetImage():
    def __init__(self):
        self.camera = ''
        self.data_image = 0
        self.is_processing = 0
        self.height = 480/3
        self.width = 640
        self.image_matrix = np.zeros([self.height,self.width])


    def plot_image(self,image):
        plt.imshow(image,cmap='hot')
        plt.show()


    def get_value(self,image_byte):
        bias = 127
        string = ''
        for i in range(3):
            bin_str = np.binary_repr(int(image_byte[i]))
            len_bin = 8 - len(bin_str)
            #print len_bin
            zeros = ''
            for k in range(len_bin):
                 zeros += '0'
            string = zeros + bin_str + string
            #print string
        #print '3bytes',string
        exp = int(image_byte[3])-bias
        #print 'exp',exp
        value = 1 * 2**exp
        for k in range(len(string)):
            value+= int(string[k])*2**(exp-k+1)
        return value


    def process_image(self):
        print "image is being processed"
        data = self.data_image
        #with open("test.txt","w") as out:
        #    out.write(str(self.image))
        buf = StringIO.StringIO(data)
        for i in range(11):
            line = buf.readline()
                #print line
        line = buf.readline()
        image = line[7:-1].split(",")
        image_length = len(image)
        #print image_length
        one_third = image_length / 3
        
        for k in range(one_third,one_third*2,2):
            r = int(np.floor((k-one_third)/(2*self.width)))
            c = k/2 % self.width
            #print r,c
            self.image_matrix[r,c] = int(image[k+1])*256+int(image[k])
            #self.image_matrix[r,c] = self.get_value(image[k:k+4])

        max_val = self.image_matrix.max()
        self.image_matrix = np.floor(self.image_matrix*255/max_val).astype(int)
        #thread.start_new_thread(self.plot_image,())

        plt.axis([0,self.width,self.height,0])
        plt.ion()
        plt.show()
        plt.imshow(self.image_matrix,cmap='hot')
        plt.draw()
        plt.pause(0.001)
        now = rospy.get_rostime().secs
        to_save = {'image_data': self.image_matrix}
        spio.savemat('%s_imgs/image_data_%s.mat' % (self.camera,now), to_save)
        plt.savefig('%s_imgs/image_%s.png' % (self.camera,now))

        self.is_processing = 0


def callback(img,cool):
    #print "callback_image"
    #print data
    if cool.is_processing == 0:
        cool.data_image = img
        cool.is_processing = 1
    #print type(img.data)


def main():
    cool = GetImage()
    if len(sys.argv)== 2 and sys.argv[1]=='asus':
        cool.camera = 'asus'
    elif len(sys.argv)== 2 and sys.argv[1]=='kinect':
        cool.camera = 'kinect'
    else:
        sys.exit('please specify camera: asus or kinect')
    rospy.init_node("get_image_%s"%cool.camera)
    rospy.Subscriber('/%s/depth/image_raw' % cool.camera, Image, callback,callback_args=cool)
    while not rospy.is_shutdown():
        if cool.is_processing == 1:
            cool.process_image()

if __name__ == '__main__':
    main()