#!/usr/bin/env python

import subprocess
import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros
from random import randint
from geometry_msgs.msg import PoseStamped,Pose
import time

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
from mosmach.actions.get_cap_sensors_action import GetCapSensorsAction
from mosmach.actions.run_ce_action import RunCeAction
#from mosmach.actions.topic_reader_action import TopicReaderAction
#from mosmach.actions.run_ca_action import RunCaAction
#from mosmach.actions.run_ce_action import RunCeAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading
#from move_base_msgs.msg import MoveBaseGoal
#from mosmach.util import pose2pose_stamped
#from gesture_player_utils.msg import GestureStatus



"""########### Get Capacitive Sensor ################
class GetCapSensorsActionState(MonarchState):

  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("GetCapSensorsActionState")
    
    getCapSensorsAction = GetCapSensorsAction(self, self.cap_callback)
    self.add_action(getCapSensorsAction)

  def cap_callback(self, data, userdata):
    if data.head == True:
        userdata.sensor_activated = 'head_touched'
    elif data.left_shoulder == True:
        userdata.sensor_activated = 'left_shoulder_touched'
    elif data.right_shoulder == True:
        userdata.sensor_activated = 'right_shoulder_touched'
    elif data.left_arm == True:
        userdata.sensor_activated = 'left_arm_touched'
    elif data.right_arm == True:
        #subprocess.call("mpg321 mp3/r2d2_1.mp3 -g 60",shell=True)
        time.sleep(5)
        userdata.sensor_activated = 'right_arm_touched'


####### Touch Interaction ################
class TouchInteractionState(MonarchState):
  def __init__(self):
    MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['sensor_activated'], output_keys=['sensor_activated'])
    rospy.loginfo("TouchInteractionState")

    runCeState = RunCeAction(self, self.greeting_name_callback, is_dynamic = True)
    self.add_action(runCeState)

  def greeting_name_callback(self, userdata):
    if userdata.sensor_activated == 'head_touched':
      return 'mbot_warm_expression'
    elif userdata.sensor_activated == 'left_arm_touched':
      return 'give_greetings_child'
    elif userdata.sensor_activated == 'right_arm_touched':
      return 'mbot_follow_robot'
    elif userdata.sensor_activated == 'left_shoulder_touched':
      return 'mbot_behavior_succeeded'
    elif userdata.sensor_activated == 'right_shoulder_touched':
      return 'mbot_behavior_succeeded'
"""


######### Patrolling State Machine #############
class MoveToWP1State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['pose_data'], output_keys=['pose_data'])
        rospy.loginfo("Init MoveToWP1State")

        navMoveToOneAction = MoveToAction(self, data_cb=self.move_to_cb, is_dynamic=True)
        self.add_action(navMoveToOneAction)

        self.ind = 0 #Index to make sure there is not consecutive goal point which are the same. Robot may block if so.

        #IPOL points
        #1.90,0.05,1.57   #down in IPOL
        #0.15,18.70,-1.51 #up IPOL
        #-0.20,24.20,-1.51 entrance
        #-0.10,-0.30,0.32 near docking
        #0.90,10.15,1.65 middle corridor

        #ISR LRM points
        #(-4.90,8.85,0.45) left in the lab
        #(-1.00,10.90,-2.69) right in the lab
        #(5.20,0.23,-1.09)  Elevador
        #(1.40,11.73,-2.68)  Meio LRM
        #(-1.40,-1.27,2.05)  Fim casa
        #(-8.90,6.72,0.43)  bonair

    #IPOL#
    """def move_to_cb(self,userdata):
        while True: 
            rand = randint(1,6)
            if rand != self.ind:
                self.ind = rand
                break
        print self.ind

        if rand == 1:
            userdata.pose_data.pose.position.x = 1.90 #down in IPOL
            userdata.pose_data.pose.position.y = 0.05
            userdata.pose_data.pose.orientation.z = 1.57
        elif rand == 2:
            userdata.pose_data.pose.position.x = 0.15 #up IPOL
            userdata.pose_data.pose.position.y = 18.70
            userdata.pose_data.pose.orientation.z = -1.51
        elif rand == 3:
            userdata.pose_data.pose.position.x = -0.2 #entrance
            userdata.pose_data.pose.position.y = 24.2
            userdata.pose_data.pose.orientation.z = -1.51
        elif rand == 4:
            userdata.pose_data.pose.position.x = -0.1 #near docking
            userdata.pose_data.pose.position.y = -0.3
            userdata.pose_data.pose.orientation.z = 0.32
        elif rand == 5:
            userdata.pose_data.pose.position.x = 0.90 #middle corridor
            userdata.pose_data.pose.position.y = 10.15
            userdata.pose_data.pose.orientation.z = 1.65
        return userdata.pose_data"""


    #ISR#
    def move_to_cb(self,userdata):
        while True: 
            rand = randint(1,6)
            if rand != self.ind:
                self.ind = rand
                break
            print "repeat"
        print self.ind
        
        if rand == 1:
            userdata.pose_data.pose.position.x = -4.90 #Left in LRM
            userdata.pose_data.pose.position.y = 8.85
            userdata.pose_data.pose.orientation.z = 0.45
        elif rand == 2:
            userdata.pose_data.pose.position.x = -1 #Right in LRM
            userdata.pose_data.pose.position.y = 10.90
            userdata.pose_data.pose.orientation.z = -2.69
        elif rand == 3:
            userdata.pose_data.pose.position.x = -8.9 #Boinar LRM
            userdata.pose_data.pose.position.y = 6.72
            userdata.pose_data.pose.orientation.z = 0.43
        elif rand == 4:
            userdata.pose_data.pose.position.x = -1.4 # Fim casa LRM
            userdata.pose_data.pose.position.y = -1.27
            userdata.pose_data.pose.orientation.z = 2.05
        elif rand == 5:
            userdata.pose_data.pose.position.x = 1.4 # meio LRM
            userdata.pose_data.pose.position.y = 11.73
            userdata.pose_data.pose.orientation.z = -2.68
        return userdata.pose_data



######### Main function
def main():

    rospy.init_node("IPOL_one_click")

    # State Machine Patrolling
    sm_patrolling = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
    sm_patrolling.userdata.sm_data = PoseStamped()
    with sm_patrolling:
        sm_patrolling.add('WAYPOINT1', MoveToWP1State(), transitions = {'succeeded':'WAYPOINT1'},remapping={'pose_data':'sm_data'})
    
    """sm_cap = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
    sm_cap.userdata.sm_sensor_activated = 0
    with sm_cap:
      sm_cap.add('GetCapSensors', GetCapSensorsActionState(), transitions={'succeeded':'GetCapSensors'}, remapping={'sensor_activated':'sm_sensor_activated'})
      #sm_cap.add('TouchInteraction', TouchInteractionState(), transitions={'succeeded':'succeeded'}, remapping={'sensor_activated':'sm_sensor_activated'})"""

    outcome1 = sm_patrolling.execute()
    #outcome2 = sm_cap.execute()

    rospy.spin()

if __name__ == '__main__':
    main()
