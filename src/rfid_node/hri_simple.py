#!/usr/bin/env python

import rospy
import subprocess
import time
from std_msgs.msg import String
from monarch_msgs.msg import *
from random import randint

ros_namespace=None
sound=''


def move_head(angle,reset=0):
    head = HeadControl()
    rand = randint(-5,5)
    rand_ang = rand*3.14/180
    if reset == 0:
        head.head_pos = angle+rand_ang #move -45 degrees + rand(-5,5)
    elif reset == 1:
        head.head_pos = angle
    head.movement_speed = 70
    pub_head.publish(head)


def set_leds(device,r,g,b,vel):
    leds = LedControl()
    leds.device=device
    leds.r=r
    leds.g=g
    leds.b=b
    leds.change_time=vel
    #print device, r, g, b
    #print leds
    pub.publish(leds)
    time.sleep(0.3)


def play_sound(bumpers=0):
    global sound

    if bumpers == 1:
        num = randint(1,3)
        subprocess.call("mpg321 mp3/bumper%d.mp3 -g 60" % num,shell=True)
    elif sound == 'voice':
        num = randint(1,9)
        subprocess.call("mpg321 mp3/%s_%d.mp3" % (sound,num),shell=True)
    else:
        num = randint(0,6)
        subprocess.call("mpg321 mp3/%s_%d.mp3 -g 60" % (sound,num),shell=True)


def stop_leds():
    set_leds(0,0,0,0,0)
    set_leds(1,0,0,0,0)
    set_leds(2,0,0,0,0)
    set_leds(3,0,0,0,0)
    set_leds(4,0,0,0,0)
    set_leds(5,0,0,0,0)


def mouth_normal():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*8+[True]*16+[False]*8
    mouth.data=mouth.data+[False]*7+[True]+[False]*16+[True]+[False]*7
    mouth.data=mouth.data+[False]*6+[True]+[False]*18+[True]+[False]*6
    mouth.data=mouth.data+[False]*5+[True]+[False]*20+[True]+[False]*5
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*32

    mouth.data=mouth.data[::-1]

    #print mouth.data
    time.sleep(1)
    pub_mouth.publish(mouth)


def mouth_sad():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*8+[True]*16+[False]*8
    mouth.data=mouth.data+[False]*7+[True]+[False]*16+[True]+[False]*7
    mouth.data=mouth.data+[False]*6+[True]+[False]*18+[True]+[False]*6
    mouth.data=mouth.data+[False]*5+[True]+[False]*20+[True]+[False]*5
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*4+[True]+[False]*22+[True]+[False]*4
    mouth.data=mouth.data+[False]*32

    time.sleep(1)
    pub_mouth.publish(mouth)


def mouth_happy():

    mouth = MouthLedControl()   

    mouth.data=[False]*32
    mouth.data=mouth.data+[False]*1+[True]*30+[False]*1
    mouth.data=mouth.data+[False]*3+[True]*2+[False]*22+[True]*2+[False]*3
    mouth.data=mouth.data+[False]*5+[True]*2+[False]*18+[True]*2+[False]*5
    mouth.data=mouth.data+[False]*7+[True]*2+[False]*14+[True]*2+[False]*7
    mouth.data=mouth.data+[False]*9+[True]*3+[False]*8+[True]*3+[False]*9
    mouth.data=mouth.data+[False]*12+[True]*8+[False]*12
    mouth.data=mouth.data+[False]*32

    time.sleep(1)
    pub_mouth.publish(mouth)


def eyes_play():

    set_leds(0,50,0,50,100)
    set_leds(1,50,0,50,100)
    set_leds(0,0,50,0,100)
    set_leds(1,0,50,0,100)
    set_leds(0,0,0,50,100)
    set_leds(1,0,0,50,100)

    set_leds(0,0,50,50,100) #back to default
    set_leds(1,0,50,50,0)   #---------------


def callback_rfid(data):

    #MOVE HEAD
    if data.tag_angle < 0:
        led = 3
        move_head(-0.78)
    elif data.tag_angle >= 0:
        led = 5
        move_head(0.78)    
    set_leds(led,0,0,100,50) #BASE LEDS

    front_base=0
    if data.tag_angle > -90 and data.tag_angle < 90:
        set_leds(4,0,0,100,50)
        front_base=1
    
    #MOUTH
    mouth_happy()
    
    #PLAY SOUND
    play_sound()

    #EYES PLAY
    eyes_play()

    time.sleep(4)

    #Back to default
    move_head(0,1)
    set_leds(led,0,0,0,50)
    if front_base==1:
        set_leds(4,0,0,0,50)
    mouth_normal()


def callback_cap(data):
    active = 0
    if data.left_arm == True:
        move_head(0.78)
        active = 1
    elif data.right_arm == True:
        move_head(-0.78)
        active = 1
    elif data.head == True:
        active = 1
    elif data.left_shoulder == True:
        active = 1
    elif data.right_shoulder == True:
        active = 1

    if active == 1:
        time.sleep(0.5)
        play_sound()
        time.sleep(0.5)
    
    


def callback_bumpers(data):
    if data.leftRearBump == True:
        print "Ai!"
        play_sound(1)
        time.sleep(0.5)
    

def start():
    #Starts normal
    global ros_namespace, pub, pub_mouth, pub_head, pub_sound

    list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()
    for string in list_output.split("\n"):
        if string != "":
            ros_namespace = string
            break

    rospy.init_node('hri_simple', anonymous=True)

    #LEDS
    #topic_2_pub = '/'+ ros_namespace +'/cmd_leds'
    pub = rospy.Publisher('cmd_leds', LedControl)

    #MOUTH
    #topic_2_pub_mouth = '/'+ ros_namespace +'/cmd_mouth'
    pub_mouth = rospy.Publisher('cmd_mouth', MouthLedControl)

    #HEAD
    #topic_2_pub_head = '/'+ ros_namespace +'/cmd_head'
    pub_head = rospy.Publisher('cmd_head', HeadControl)

    #PLAY SOUNDS
    pub_sound = rospy.Publisher('sound',NonVerbalSound)


    # Initial State
    
    set_leds(0,0,50,50,100) #Left Eye
    set_leds(0,0,50,50,100) # -------
    set_leds(1,0,50,50,0)   #Right Eye
    set_leds(2,50,0,0,0)    #Cheeks
    set_leds(3,0,0,0,0)  #Right Base
    set_leds(4,0,0,0,0)  #Center Base
    set_leds(5,0,0,0,0)  #Left Base
    mouth_normal()
    
    #Test for moving head - -90 to 90 degrees and back to 0
    """head=HeadControl()
    head.movement_speed=70
    head.head_pos=-1.5
    pub_head.publish(head)
    rospy.sleep(5)
    head.head_pos=1.5
    pub_head.publish(head)
    rospy.sleep(5)
    head.head_pos=0
    pub_head.publish(head)"""


def listeners():

    topic_2_sub = '/'+ ros_namespace +'/rfid_position'
    rospy.Subscriber(topic_2_sub, RfidReading, callback_rfid, queue_size=1)
    topic_2_sub_cap_sensors = '/'+ ros_namespace +'/cap_sensors'
    rospy.Subscriber(topic_2_sub_cap_sensors,CapacitiveSensorsReadings, callback_cap, queue_size=1)
    topic_2_sub_bumpers = '/'+ ros_namespace +'/bumpers'
    rospy.Subscriber(topic_2_sub_bumpers,BumpersReadings, callback_bumpers, queue_size=1)
    rospy.spin()


if __name__ == '__main__':


    if rospy.search_param('voice'):
        if rospy.get_param('voice')=='v':
            sound = 'voice'
        elif rospy.get_param('bf') == 's':
            sound = 'r2d2'
        else:
            sound = 'r2d2'


    if len(sys.argv)== 2 and sys.argv[1]=='s':
        sound = 'r2d2'
    elif len(sys.argv)== 2 and sys.argv[1]=='v':
        sound = 'voice'
    else:
        sound = 'r2d2'

    start()

    try:
        listeners()
    except rospy.ROSInterruptException:
        pass
