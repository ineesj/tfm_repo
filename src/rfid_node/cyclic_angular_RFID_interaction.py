#!/usr/bin/env python

import roslib; roslib.load_manifest('rfid_node')
import rospy
import smach
import smach_ros

from monarch_msgs.msg import RfidReading
from mosmach.monarch_state import MonarchState
from mosmach.change_condition import ChangeCondition
from mosmach.change_conditions.topic_condition import TopicCondition
from mosmach.actions.move_head_action import MoveHeadAction
from mosmach.actions.run_ce_action import RunCeAction
from mosmach.actions.runtime_action import RunTimeAction

from monarch_msgs.msg import HeadControl
import random

class GetTagAngleState(MonarchState):
	def __init__(self):
		MonarchState.__init__(self, state_outcomes = ['succeeded'], input_keys=['tag_angle'], output_keys=['tag_angle'])
		rospy.loginfo("GetTagAngleState")

		self.add_change_condition(TopicCondition(self, 'rfid_position', RfidReading, self.rfidCondition_cb), ['succeeded'])

	def rfidCondition_cb(self, data, userdata):
		userdata.tag_angle = data.tag_angle
		return 'succeeded'


class MoveHeadAndInteractState(MonarchState):
	
	def __init__(self):
		MonarchState.__init__(self, state_outcomes=['succeeded'], input_keys=['tag_angle'], output_keys=['tag_angle'])
		rospy.loginfo("MoveHeadAndInteractState")
		
#		self.add_action(RunCeAction(self, 'give_greetings_child'))
		self.add_action(MoveHeadAction(self,data_cb=self.tagAngleCallback, is_dynamic=True))
		self.add_action(RunTimeAction(self,2))

	def tagAngleCallback(self, userdata):
		headControl = HeadControl()
		headControl.movement_speed = 50
		headControl.head_pos = (userdata.tag_angle/2 * 3.14/180)

		return headControl


######### Main function
def main():
	rospy.init_node("cyclic_angular_RFID_interaction")

	# State Machine     
	sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
	sm.userdata.tag_angle = 0	
	with sm:
		sm.add('GetTagAngle',GetTagAngleState(),transitions = {'succeeded':'MoveHeadAndInteract'})
		sm.add('MoveHeadAndInteract', MoveHeadAndInteractState(),transitions = {'succeeded':'GetTagAngle'})

	
	outcome = sm.execute()


if __name__ == '__main__':	
	main()
