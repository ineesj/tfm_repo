*Authors*
João Sequeira
Duarte Gameiro
Any problems/doubts please contact duarteflgameiro@tecnico.ulisboa.pt


*Purpose*
rfid_node.py - publishes RFID tag IDs when detected by the reader.
rfid_person_detection.launch - publishes the angle estimate of the tag relative to the robot (in the head's reference frame)


*Install and setup*
Following packages need to be installed:
sudo easy_install -U distribute
sudo pip install -U scikit-image
Run: sudo chmod +x *.py (to make python files executable in the case they are not)
After, setup.py must be run for the reader to function properly (in Answer Mode, not in Scan Mode). It needs 1 arg: "on" or "off", for the beep option. This can be used every time one needs to turn on or off the beep.
To check if it is ready, run get_workmode.py. The output should have "Answer Mode" and the chosen beep option.
(Make sure there's no other processes running that access the RFID reader. It creates confltics if more than one process is trying it)


*Running*
Just to  detect tags: rosrun rfid_node rfid_node.py. Publishes to topic rfid_tag
To get tag angle estimate for people detection: roslaunch rfid_person_detection.launch. Publishes to topic rfid_angle (and also to SAM on the slot named RFIDAngle).
If there is a priori information of where the person is relative to the robot, one optional argument to the launch file can be added.
bf:=1 (person in front of the robot); bf:=2 (person behind the robot).
If rfid_person_detection.launch is launched, then rfid_node.py is also running, so don't run both! (check the launch file)


*Errors*
When running get_workmode.py, setup.py or rfid_node.py, if the message 'Error: RFID reader not working' appears, then check if all
cables are connected, usb, serial port, power supply, etc (the serial port is converted into usb). If it still doesn't work then the problem can be worse.
What has been noticied in IST is that the RFIDs from time to time start to get worse detections or simply stop working (how amazing that 
is!). The temporary/quick/"still don't have any better" solution is to get a pc with Windows and run the software that comes with the
reader and set the default definitions (rfid_default.jpg shows how to do it). Magically it starts working again. One possible reason could be the bad quality of the
electronics and with time the software starts to deteriorate, maybe due to heat. Remember to launch setup.py again to change the reader
to Answer Mode (not default) and set the beep option. If this happens please send an email to duarteflgameiro@tecnico.ulisboa.pt.
The software is available in monarch/sw folder.


*Probabilistic Model*
For person detection, the algorithm needs a probabilistic model of the RFID reader. For now, mbot01_model.mat is used in all robots.


*Additional code*
tag_prob.py computes the probability of detection of a tag. This probability is then used by angle_person.py to get an angle estimate.
tag_prob_mult.py (UNDER DEVELOPMENT) is an improvement of tag_prob.py as it enables to detect multiple tags probabilites at once and not sequentially. Soon it will be part the the main code.
bag_IPOL.py (also UNDER DEVELOPMENT) is the code used to record bags with image from cameras and angle estimates.
