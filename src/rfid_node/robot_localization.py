#!/usr/bin/env python

# Robot localization algorithm with SVM training and zone prediction.

# --- duarte.gameiro, 2015 ---

import rospy, subprocess, sys
import numpy as np
from scipy import io as spio
from sklearn import svm
import database_tags as DB
import cPickle
from geometry_msgs.msg import PoseWithCovarianceStamped
from monarch_msgs.msg import RfidReading

def tag_callback(data):
    global detected_tags,total_detections
    t=data.tag_id
    if detected_tags.has_key(t):
        detected_tags[t]+=1
        total_detections+=1
    else:
        detected_tags[t]=1
        total_detections += 1

def pose_callback(data):
    global x,y
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y

def p2w(px, py):
    x0=-21.2; y0=-10
    scale=0.05
    W=832; H=608
    x = x0 + scale*px
    y = y0 + scale*(H-py-1)
    return (x, y)

def get_label(x,y,zone): # to complete when all ISR8 is taken into account.

    if zone == 'A':
        x1,y1 = p2w(584,139)
        x2,y2 = p2w(618,121)
        mA = (y1-y2)/(x1-x2)
        bA = y1-mA*x1
    elif zone == 'B':
        x1,y1 = p2w(584,139)
        x2,y2 = p2w(618,121)
        mA = (y1-y2)/(x1-x2)
        bA = y1-mA*x1
        x1,y1 = p2w(621,198)
        x2,y2 = p2w(652,179)
        mB = (y1-y2)/(x1-x2)
        bB = y1-mB*x1
    elif zone == 'C':
        x1,y1 = p2w(621,198)
        x2,y2 = p2w(652,179)
        mB = (y1-y2)/(x1-x2)
        bB = y1-mB*x1        
        x1,y1 = p2w(656,261)
        x2,y2 = p2w(686,244)
        mC = (y1-y2)/(x1-x2)
        bC = y1-mC*x1
    elif zone == 'D':
        x1,y1 = p2w(656,261)
        x2,y2 = p2w(686,244)
        mC = (y1-y2)/(x1-x2)
        bC = y1-mC*x1
        x1,y1 = p2w(703,344)
        x2,y2 = p2w(734,328)
        mD = (y1-y2)/(x1-x2)
        bD = y1-mD*x1
    elif zone == 'E':
        x1,y1 = p2w(703,344)
        x2,y2 = p2w(734,328)
        mD = (y1-y2)/(x1-x2)
        bD = y1-mD*x1
        x1,y1 = p2w(708,353)
        x2,y2 = p2w(726,384)
        mE = (y1-y2)/(x1-x2)
        bE = y1-mE*x1

    if zone == 'A':
        if y >= mA*x+bA:
            temp_label = 1
        else:
            temp_label = 0
    if zone == 'B':
        if y < mA*x+bA and y >= mB*x+bB:
            temp_label = 1
        else:
            temp_label = 0
    elif zone == 'C':
        if y < mB*x+bB and y >= mC*x+bC:
            temp_label = 1
        else:
            temp_label = 0
    elif zone == 'D':
        if y < mC*x+bC and y >= mD*x+bD:
            temp_label = 1
        else:
            temp_label = 0
    elif zone == 'E':
        if y < mD*x+bD and y >= mE*x+bE:
            temp_label = 1
        else:
            temp_label = 0

    return temp_label

def test_SVM(detected_tags,n_tags,clf_test):
    test = np.zeros((n_tags*2,), dtype=np.int)
    for ID in detected_tags:
        #print "ID:",ID,"-->",detected_tags[ID]
        tag_index = DB.get_index(str(ID))
        test[tag_index] = 1
        test[tag_index + n_tags] = detected_tags[ID]
    #print "test: ", test
    result = clf_test.predict([test])
    print result
    return result,test

def get_zone_mode():
    if len(sys.argv) == 1:
        zone = 'A'
        mode = 'test'
    #elif len(sys.argv) == 2 and sys.argv[1] == 'all'
    #    zone = sys.argv[1]
    #    mode = 'test'
    elif len(sys.argv) == 2:
        zone = sys.argv[1]
        mode = 'train'
    elif len(sys.argv) == 3:
        zone = sys.argv[1]
        mode = sys.argv[2]
    else:
        sys.exit("Incorrect argument(s). Check python file")
    return zone,mode


if __name__ == '__main__':

    zone,mode = get_zone_mode() 

    n_tags = 10     # number of tags in environment
    detected_tags = {}      #dictionary of tags per sample
    total_detections = 0    #total number of tags detections per sample
    prev = 0        #total number of tags detections in previous sample
    
    fo = open('svm/SVM_samples_%s.txt' % zone, 'w')
    ft = open('svm/trajectory_%s_%s.txt' % (mode,zone), 'w')

    sample = 0
    total_samples = 600 #defined value
    rate = 1 # seconds per sample

    x = 999 #starting default 'non-sense' values for x and y
    y = 999

    try:
        rospy.init_node('rfid_localization_learning')
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        rospy.Subscriber(ros_namespace+'/amcl_pose',PoseWithCovarianceStamped,pose_callback)        
        rospy.Subscriber(ros_namespace+'/rfid_tag',RfidReading,tag_callback)

        if mode == 'test':
            mat_SVM = spio.loadmat('svm/svm_train%s.mat' % zone)
            train = mat_SVM['train']
            label = np.ravel(mat_SVM['label'])
            clf_test = svm.SVC()
            clf_test.fit(train, label)
            #with open('svm/svm_zoneA.pkl', 'rb') as fid:
            #    clf_test = cPickle.load(fid)
            time = rospy.get_time()
            while not rospy.is_shutdown():
                if rospy.get_time() > time+rate and x!=999: #MISSING DISABLE CALLBACKS
                    ft.write('%.2f'%x+','+'%.2f'%y+',') #write on trajectory file
                    result,test = test_SVM(detected_tags,n_tags,clf_test)
                    ft.write(str(int(result))+',')
                    ft.write(str(test)+'\n')
                    if int(result) == 1:
                        print 'Tag in zone %s' % zone
                    else:
                        print 'Tag not in zone %s' % zone 
                    #RESET
                    detected_tags = {}
                    total_detections = 0
                    time = rospy.get_time()

        if mode == 'train':
            print "entering train MODE for zone %s" % zone
            train = np.zeros((total_samples,n_tags*2), dtype=np.int)
            label = np.zeros((total_samples,), dtype=np.int)
            if zone != 'A':
                prev_zone = chr(ord('%s'% zone)-1) 
                mat_SVM = spio.loadmat('svm/svm_train%s.mat' % prev_zone)
                prev_train = mat_SVM['train']
                prev_label = np.ravel(mat_SVM['label'])
                clf_test = svm.SVC()
                clf_test.fit(prev_train,prev_label)
                #with open('svm/svm_zone%s.pkl' % prev_zone, 'rb') as fid:
                #    clf_test = cPickle.load(fid)
            time = rospy.get_time()
            while sample != total_samples:
                if rospy.get_time() > time+rate and x!=999:
                    print "sample number: ", sample," total_detections: ",total_detections
                    ft.write('%.2f'%x+','+'%.2f'%y+'\n') #write on trajectory file
                    if (prev==0 and total_detections!=0) or prev!=0:
                        fo.write('%.2f'%x+','+'%.2f'%y+','+str(total_detections)) #write on training file
                        for ID in detected_tags:
                            fo.write(','+str(ID)+','+str(detected_tags[ID]))
                            tag_index = DB.get_index(str(ID))
                            train[sample,tag_index] = 1
                            train[sample,tag_index + n_tags] = detected_tags[ID]
                        fo.write('\n')

                        temp_label = get_label(x,y,zone) # 1 means it's in this zone or previous, 0 next zones

                        if zone != 'A':
                            prev_label,training = test_SVM(detected_tags,n_tags,clf_test)
                            if int(prev_label)== 0 and temp_label == 1: # if it's not in previous area
                                label[sample] = 1
                            else:
                                label[sample] = 0
                        else: #zone A
                            label[sample] = temp_label
                        print "label for training: ",label[sample]
                        #RESET everything
                        prev = total_detections
                        total_detections = 0
                        detected_tags = {}
                        sample+=1
                    time = rospy.get_time()

            clf = svm.SVC()
            clf.fit(train, label)
            fit_dict = {'train': train, 'label':label}
            spio.savemat('svm/svm_train%s.mat' % zone, fit_dict)
            with open('svm/svm_zone%s.pkl' % zone, 'wb') as fid:
                cPickle.dump(clf, fid)

    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        # quit
        fo.close()
        ft.close()
        sys.exit()