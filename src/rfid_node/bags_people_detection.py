#!/usr/bin/env python

# Code to get bags for people localization using RFID

# --- duarte.gameiro, 2016 ---

import rospy, subprocess, os, sys
from sam_helpers.reader import SAMReader

ros_namespace=None

def start_record():
    rospy.loginfo('now the topic recording should start')

    args = '/'+ros_namespace+'/amcl_pose'+' '+'/'+ros_namespace+'/rfid_position'
    now = rospy.get_rostime()
    command = 'rosbag record ' + args + ' -O bags/bagsIPOLjan26/bag_%i.bag' % now.secs
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True)

def stop_record():
    rospy.loginfo('now the topic recording should stop')

    list_cmd = subprocess.Popen("rosnode list", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()
    retcode = list_cmd.wait()
    assert retcode == 0, rospy.logwarn("List command returned %d" % retcode)
    for s in list_output.split("\n"):
        if (s.startswith("/record")):
            os.system("rosnode kill " + s)


if __name__ == '__main__':

    """if len(sys.argv)== 3:
        camera = sys.argv[1]
        ros_namespace = sys.argv[2]
    elif len(sys.argv)== 2:
        camera = sys.argv[1]
        ros_namespace= 'mbot09'
    elif len(sys.argv)== 1:
        camera = 98
        ros_namespace= 'mbot09'"""

    rospy.init_node('rfid_people_detection_bags')

    try:
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        start_record()
        
        #rc = SAMReader("RFIDTagID", start_camera_record,agent_name=ros_namespace)
        #rc2 = SAMReader("RFIDAngle",stop_camera_record,agent_name=ros_namespace)
        
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        stop_record()
        sys.exit()