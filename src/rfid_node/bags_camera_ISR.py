#!/usr/bin/env python

import rospy
import subprocess,os,sys
from sam_helpers.reader import SAMReader
from monarch_msgs.msg import RfidReading

def start_record(data):
    rospy.loginfo('now the topic recording should start')

    args = '/camera_driver_%s_3/stream/compressed /rfid_angle_2_bag' % camera
    now = rospy.get_rostime()
    command = 'rosbag record ' + args + ' -O person_detection_ISR_%i.bag' % now.secs
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True)
    
def stop_record(data):

    reading = data
    pub.publish(reading)

    rospy.loginfo('now the topic recording should stop')

    list_cmd = subprocess.Popen("rosnode list", shell = True, stdout = subprocess.PIPE)
    list_output = list_cmd.stdout.read()
    retcode = list_cmd.wait()
    assert retcode == 0, rospy.logwarn("List command returned %d" % retcode)
    for s in list_output.split("\n"):
        #print s
        if (s.startswith("/record")):
            os.system("rosnode kill " + s) 

if __name__ == '__main__':

    if len(sys.argv)== 3:
        camera = sys.argv[1]
        ros_namespace = sys.argv[2]
    elif len(sys.argv)== 2:
        camera = sys.argv[1]
        ros_namespace= 'mbot05'
    elif len(sys.argv)== 1:
        camera = 98
        ros_namespace= 'mbot05'

    rospy.init_node('rfid_bag')
    pub = rospy.Publisher('rfid_angle_2_bag', RfidReading)
    
    rc = SAMReader("RFIDTagID", start_record,agent_name=ros_namespace)
    rc2 = SAMReader("RFIDAngle",stop_record,agent_name=ros_namespace)

    rospy.spin()