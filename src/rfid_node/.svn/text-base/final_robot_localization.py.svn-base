#!/usr/bin/env python

# Robot localization algorithm with SVM training and zone prediction.

# --- duarte.gameiro, 2015 ---

import rospy, subprocess, sys
import numpy as np
from scipy import io as spio
from sklearn import svm
import database_tags as DB
import cPickle
from geometry_msgs.msg import PoseWithCovarianceStamped
from monarch_msgs.msg import RfidReading

def tag_callback(data):
    global detected_tags,total_detections
    t=data.tag_id
    if detected_tags.has_key(t):
        detected_tags[t]+=1
        total_detections+=1
    else:
        detected_tags[t]=1
        total_detections += 1

def pose_callback(data):
    global x,y
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y

def p2w(px, py):
    x0=-21.2; y0=-10
    scale=0.05
    W=832; H=608
    x = x0 + scale*px
    y = y0 + scale*(H-py-1)
    return (x, y)

def get_real_zone(x,y): # to complete when all ISR8 is taken into account.
    
    x1,y1 = p2w(567,112)
    x2,y2 = p2w(599,93)
    x3,y3 = p2w(584,139)
    x4,y4 = p2w(618,121)        
    x5,y5 = p2w(621,198)
    x6,y6 = p2w(652,179)
    x7,y7 = p2w(656,261)
    x8,y8 = p2w(686,244)
    x9,y9 = p2w(704,347)
    x10,y10 = p2w(734,329)
    x11,y11 = p2w(726,384)
    x12,y12 = p2w(759,364)

    m1 = (y1-y2)/(x1-x2)
    b1 = y1-m1*x1
    m2 = (y1-y11)/(x1-x11)
    b2 = y1-m2*x1
    m3 = (y2-y12)/(x2-x12)
    b3 = y2-m3*x2
    m4 = (y3-y4)/(x3-x4)
    b4 = y3-m4*x3
    m5 = (y5-y6)/(x5-x6)
    b5 = y5-m5*x5
    m6 = (y7-y8)/(x7-x8)
    b6 = y7-m6*x7
    m7 = (y9-y10)/(x9-x10)
    b7 = y9-m7*x9
    m8 = (y11-y12)/(x11-x12)
    b8 = y11-m8*x11
    m9 = (y9-y11)/(x9-x11)
    b9 = y9-m9*x9    

    if y>=m2*x+b2 and y<=m3*x+b3:
        if y<=m1*x+b1 and y>=m4*x+b4:
            return 'A'
        elif y<m4*x+b4 and y>=m5*x+b5:
            return 'B'
        elif y<m5*x+b5 and y>=m6*x+b6:
            return 'C'
        elif y<m6*x+b6 and y>=m7*x+b7:
            return 'D'
        elif y<m7*x+b7 and y>=m8*x+b8:
            return 'E'
    else:
        return 'out'

def test_SVM(detected_tags,n_tags,clf_test):
    
    vector = np.zeros((n_tags*2,), dtype=np.int)
    
    for ID in detected_tags:
        tag_index = DB.get_index(str(ID))
        vector[tag_index] = 1
        vector[tag_index + n_tags] = detected_tags[ID]
    result = clf_test.predict([vector])
    
    return result,vector


if __name__ == '__main__':

    n_tags = 10     # number of tags in environment
    detected_tags = {}      #dictionary of tags per sample
    total_detections = 0    #total number of tags detections per sample
    empty_count = 0

    fo = open('final_svm.txt', 'w')
    ft = open('final_svm_trajectory.txt', 'w')

    rate = 1 # seconds per sample

    x = 999 #starting default 'non-sense' values for x and y
    y = 999

    predicted_zone = 'out'

    try:
        rospy.init_node('rfid_localization_learning')
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        rospy.Subscriber(ros_namespace+'/amcl_pose',PoseWithCovarianceStamped,pose_callback)        
        rospy.Subscriber(ros_namespace+'/rfid_tag',RfidReading,tag_callback)

        #load classifiers
        zones = ['A','B','C','D','E']
        classifiers = []
        for s in zones:
            mat_SVM = spio.loadmat('svm/svm_train%s.mat' % s)
            train = mat_SVM['train']
            label = np.ravel(mat_SVM['label'])
            clf = svm.SVC()
            clf.fit(train, label)
            classifiers.append(clf)
            #with open('svm/svm_zone%s.pkl' % s, 'rb') as fid:
            #    clf = cPickle.load(fid)
            #    classifiers.append(clf)
    
        #prediction loop
        time = rospy.get_time()
        while not rospy.is_shutdown():     
            if rospy.get_time()>time+rate and x!=999:
                ft.write('%.2f'%x+','+'%.2f'%y+'\n') #write on trajectory file
                if detected_tags == {} and (predicted_zone =='A' or predicted_zone == 'E'):
                    empty_count += 1
                else:
                    empty_count = 0
                    for index,clf_test in enumerate(classifiers):
                        result,vector = test_SVM(detected_tags,n_tags,clf_test)
                        if int(result) == 1:
                            predicted_zone = zones[index]
                            break
                real_zone = get_real_zone(x,y)
                print "Detections:",total_detections
                print "empty:",empty_count
                if empty_count >= 10:
                    predicted_zone = 'out'
                print 'Robot real in zone ',real_zone
                print 'Robot prediction in zone ',predicted_zone
                fo.write(predicted_zone+','+real_zone+','+'%.2f'%x+','+'%.2f'%y+'\n')
                #RESET
                detected_tags = {}
                total_detections = 0
                time = rospy.get_time()

    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        # quit
        fo.close()
        ft.close()
        sys.exit()