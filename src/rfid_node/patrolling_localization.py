#!/usr/bin/env python

import subprocess
import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
#from mosmach.actions.topic_reader_action import TopicReaderAction
#from mosmach.actions.run_ca_action import RunCaAction
#from mosmach.actions.run_ce_action import RunCeAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading
#from move_base_msgs.msg import MoveBaseGoal
#from mosmach.util import pose2pose_stamped
#from gesture_player_utils.msg import GestureStatus


######### Patrolling State Machine
class MoveToWP1State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP1State")

        navMoveToOneAction = MoveToAction(self,7.95,14.60,-0.90) #TOP LEFT
        self.add_action(navMoveToOneAction)

        #runCeState = RunCeAction(self, 'mbot_start_walking')
        #self.add_action(runCeState)


class MoveToWP2State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP2State")

        navMoveToTwoAction = MoveToAction(self, 8.25,14.80,-0.98) #TOP CENTER
        self.add_action(navMoveToTwoAction)


class MoveToWP3State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP3State")

        navMoveToTwoAction = MoveToAction(self, 8.60,14.90,-1.57) #TOP RIGHT
        self.add_action(navMoveToTwoAction)


class MoveToWP4State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP4State")

        navMoveToTwoAction = MoveToAction(self, 14.90,2.00,1.57) #BOTTOM LEFT
        self.add_action(navMoveToTwoAction)


class MoveToWP5State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP5State")

        navMoveToTwoAction = MoveToAction(self, 15.50,2.25,2.08) #BOTTOM CENTER
        self.add_action(navMoveToTwoAction)


class MoveToWP6State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP6State")

        navMoveToTwoAction = MoveToAction(self, 15.75,2.40,2.23) #BOTTOM RIGHT
        self.add_action(navMoveToTwoAction)


class MoveToWP7State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP7State")

        navMoveToTwoAction = MoveToAction(self, 8.35,14.00,-0.98) #ZONE A LEFT
        self.add_action(navMoveToTwoAction)


class MoveToWP8State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP8State")

        navMoveToTwoAction = MoveToAction(self, 8.65,14.15,-1.02) #ZONE A CENTER
        self.add_action(navMoveToTwoAction)


class MoveToWP9State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP9State")

        navMoveToTwoAction = MoveToAction(self, 9.00,14.40,-1.50) #ZONE A RIGHT
        self.add_action(navMoveToTwoAction)    

        #8.60,14.90,-1.57 top right
        #7.95,14.60,-0.90 top left
        #8.25,14.80,-0.98 top center
        #15.75,2.40,2.23 bottom right
        #14.90,2.00,1.57 bottom left
        #15.50,2.25,2.08 bottom center
        #8.35,14.00,-0.98 A left
        #8.65,14.15,-1.02 A center
        #9.00,14.40,-1.50 A right

######### RunTime State Machine
class RunTimeState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init RunTimeAction")

        timeCondition = RunTimeAction(self, 30)
        self.add_action(timeCondition)


######### Dock State Machine
class SendDockState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
        rospy.loginfo("Init DockState")

        navSendToDockAction = MoveToAction(self, 5.15,42.60,0.22)
        self.add_action(navSendToDockAction)



"""######### Restart State
class RestartState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init RestartState")"""



######### Main function
def main():

    rospy.init_node("patrolling_RFID_training")

    # State Machine Patrolling + HRI
    sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
    with sm:
        #sm.add('WAYPOINT1', MoveToWP1State(), transitions = {'succeeded':'WAYPOINT2'})
        sm.add('WAYPOINT2', MoveToWP4State(), transitions = {'succeeded':'WAYPOINT3'})
        sm.add('WAYPOINT3', MoveToWP5State(), transitions = {'succeeded':'WAYPOINT4'})
        sm.add('WAYPOINT4', MoveToWP2State(), transitions = {'succeeded':'WAYPOINT5'})
        sm.add('WAYPOINT5', MoveToWP3State(), transitions = {'succeeded':'WAYPOINT6'})
        sm.add('WAYPOINT6', MoveToWP6State(), transitions = {'succeeded':'WAYPOINT7'})
        sm.add('WAYPOINT7', MoveToWP1State(), transitions = {'succeeded':'WAYPOINT8'})
        sm.add('WAYPOINT8', MoveToWP9State(), transitions = {'succeeded':'WAYPOINT9'})
        sm.add('WAYPOINT9', MoveToWP7State(), transitions = {'succeeded':'WAYPOINT10'})
        sm.add('WAYPOINT10', MoveToWP3State(), transitions = {'succeeded':'WAYPOINT11'})
        sm.add('WAYPOINT11', MoveToWP8State(), transitions = {'succeeded':'WAYPOINT12'})
        sm.add('WAYPOINT12', MoveToWP2State(), transitions = {'succeeded':'WAYPOINT13'})
        sm.add('WAYPOINT13', MoveToWP1State(), transitions = {'succeeded':'WAYPOINT2'})
        #sm.add('WAYPOINT5', MoveToWP5State(), transitions = {'succeeded':'WAYPOINT2'})
        
        #sm.add('DOCK', SendDockState(), transitions = {'succeeded':'WAYPOINT1'})
        #sm.add('RESTART', RestartState(), transitions = {'tag_detected':'WAYPOINT1'})

    #sis = smach_ros.IntrospectionServer('patrolling_and_dock', sm, '/SM_ROOT')
    #sis.start()
    
    outcome = sm.execute()
    rospy.spin()


if __name__ == '__main__':
    main()
