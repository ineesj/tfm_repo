#!/usr/bin/env python

# Database of all tags IDs used in MOnarCH project (under development)

# ID: ['type','description','index',x,y]
# type: 'robot' for tags on walls for localization, 'person' for tags carried by people for detection

#--- duarte.gameiro,2015

tagDB_ISR = {'30917':['robot','LRM lab door',        '0',7.50,14.45],
             '19702':['robot','Room 8.17 door',      '1',8.30,15.35],
             '22821':['robot','ITER poster',         '2',9.95,13.60],
             '19667':['robot','Ladies WC',           '3',9.40,11.25],
             '31049':['robot','Common room door',    '4',12.10,10.0], 
             '30849':['robot','Stairway door',       '5',11.55,7.55],
             '31017':['robot','Prof.Sequeira office','6',13.80,6.95],
             '22830':['robot','MOnarCH poster',      '7',14.40,5.70],
             '22822':['robot','Wall near workshop',  '8',15.60,3.60],
             '19642':['robot','Room 8.11 door',      '9',16.00,1.75],
             '22831':['other','test tag 1'],
             '28223':['other','test tag 2']}

def tag_search(ID):
    if tagDB_ISR.has_key(ID):
        return tagDB_ISR[ID][0],tagDB_ISR[ID][1]
    else:
        return 'unknown','tag not in database'

def get_description(ID):
    return tagDB_ISR[ID][1]

def get_index(ID):
    if tagDB_ISR[ID][0]== 'robot':
        return int(tagDB_ISR[ID][2])

def get_position(ID):
    if tagDB_ISR[ID][0]== 'robot':
        return tagDB_ISR[ID][3],tagDB_ISR[ID][4]
    #to_complete
def get_IDs():
    return list(tagDB_ISR)
    
def add_tag():
    pass

def remove_tag():
    pass