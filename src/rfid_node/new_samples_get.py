#!/usr/bin/env python

# Code to get samples for robot localization algorithm

# --- duarte.gameiro, 2015 ---

import rospy, subprocess
import numpy as np
from geometry_msgs.msg import PoseWithCovarianceStamped
from monarch_msgs.msg import RfidReading

def tag_callback(data):
    global tags,detected
    t=data.tag_id
    if tags.has_key(t):
        tags[t]+=1
        detected+=1
    else:
        tags[t]=1
        detected += 1

def pose_callback(data):
    global x,y
    #global sample_on,x,y
    #print sample_on
    #if start_sampling == 0:
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y
    #start_sampling = 1

if __name__ == '__main__':

    tags = {}       #dictionary of tags
    detected = 0    #total number of tags detections
    prev = 0        #total number of tags detections in previous sample
    start_sampling = 0
    fo = open('SVM_localization_samples.txt', 'w')
    ft = open('sampling_trajectory.txt', 'w')

    x = 999 #starting default 'non-sense' values for x and y
    y = 999

    try:
        rospy.init_node('rfid_localization_samples')
        list_cmd = subprocess.Popen("echo $MBOT_NAME", shell = True, stdout = subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        for string in list_output.split("\n"):
            if string != "":
                ros_namespace = string
                break
        rospy.Subscriber(ros_namespace+'/amcl_pose',PoseWithCovarianceStamped,pose_callback)        
        rospy.Subscriber(ros_namespace+'/rfid_tag',RfidReading,tag_callback)

        time = rospy.get_time()
        rate = 1 # seconds per sample

        while not rospy.is_shutdown():
            if rospy.get_time() > time+rate and x!=999:
                print "hi there!",rospy.get_time()
                ft.write(str(float("{0:.2f}".format(x))))
                ft.write(',')
                ft.write(str(float("{0:.2f}".format(y))))
                ft.write('\n')
                if (prev==0 and detected!=0) or prev!=0:
                    fo.write(str(float("{0:.2f}".format(x))))
                    fo.write(',')
                    fo.write(str(float("{0:.2f}".format(y))))
                    fo.write(',')
                    fo.write(str(detected))
                    print "detections: ", detected
                    if detected > 0:
                        #print "tags ID: "
                        for ID in tags:
                            fo.write(',')
                            fo.write(str(ID))
                            print ID
                            fo.write(',')
                            fo.write(str(tags[ID]))
                    fo.write('\n')

                    #RESET everything
                    prev = detected
                    detected = 0
                    tags = {}
                time = rospy.get_time()

    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        # quit
        fo.close()
        ft.close()
        sys.exit()