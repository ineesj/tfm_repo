#!/usr/bin/env python

import subprocess
import roslib; roslib.load_manifest('mosmach')
import rospy
import smach
import smach_ros

from mosmach.monarch_state import MonarchState
from mosmach.actions.move_to_action import MoveToAction
#from mosmach.actions.topic_reader_action import TopicReaderAction
#from mosmach.actions.run_ca_action import RunCaAction
#from mosmach.actions.run_ce_action import RunCeAction
from mosmach.change_conditions.topic_condition import TopicCondition

from monarch_msgs.msg import RfidReading
#from move_base_msgs.msg import MoveBaseGoal
#from mosmach.util import pose2pose_stamped
#from gesture_player_utils.msg import GestureStatus


######### Patrolling State Machine
class MoveToWP1State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP1State")

        navMoveToOneAction = MoveToAction(self,-4.90,8.85,0.45) #LEFT in the lab
        self.add_action(navMoveToOneAction)

        #runCeState = RunCeAction(self, 'mbot_start_walking')
        #self.add_action(runCeState)


class MoveToWP2State(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init MoveToWP2State")

        navMoveToTwoAction = MoveToAction(self, -1.00,10.90,-2.69) #RIGHT in the lab
        self.add_action(navMoveToTwoAction)


######### RunTime State Machine
class RunTimeState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init RunTimeAction")

        timeCondition = RunTimeAction(self, 30)
        self.add_action(timeCondition)


######### Dock State Machine
class SendDockState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded','preempted'])
        rospy.loginfo("Init DockState")

        navSendToDockAction = MoveToAction(self, 5.15,42.60,0.22)
        self.add_action(navSendToDockAction)



"""######### Restart State
class RestartState(MonarchState):
    def __init__(self):
        MonarchState.__init__(self, state_outcomes=['succeeded'])
        rospy.loginfo("Init RestartState")"""



######### Main function
def main():

    rospy.init_node("patrolling_demonstration")

    # State Machine Patrolling + HRI
    sm = smach.StateMachine(outcomes = ['succeeded','preempted','aborted'])
    with sm:
        sm.add('WAYPOINT1', MoveToWP1State(), transitions = {'succeeded':'WAYPOINT2'})
        sm.add('WAYPOINT2', MoveToWP2State(), transitions = {'succeeded':'WAYPOINT1'})
        
        #sm.add('DOCK', SendDockState(), transitions = {'succeeded':'WAYPOINT1'})
        #sm.add('RESTART', RestartState(), transitions = {'tag_detected':'WAYPOINT1'})

    #sis = smach_ros.IntrospectionServer('patrolling_and_dock', sm, '/SM_ROOT')
    #sis.start()
    
    outcome = sm.execute()
    rospy.spin()


if __name__ == '__main__':
    main()
