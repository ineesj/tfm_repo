#!/bin/bash


if [ $# -eq 0 ]; then
    echo "Usage ./setup.sh controller [make, map_name]"
    exit 1
fi

COMPILE=${2:-0}						#Default compile flag if nothing is specified
MAP_NAME=${3:-"ipolMapNoObstacles"}			#Default map if nothing is specified

cmd="rospack find webots_nodes"
CURRENT_PATH=$(eval $cmd)
SPLIT=trunk/*
ROOT_DIR=${CURRENT_PATH/$SPLIT/trunk}
CATKIN_WS=$ROOT_DIR/catkin_ws
ROSBUILD_WS=$ROOT_DIR/rosbuild_ws

echo "Catkin workspace = $CATKIN_WS"
echo "Rosbuild workspace = $ROSBUILD_WS"

if [ "$COMPILE" = 1 ]; then
	
	$ROOT_DIR/compile_mbot.bash
else
	source $CATKIN_WS/devel/setup.bash
	export ROS_PACKAGE_PATH=$ROSBUILD_WS:$ROS_PACKAGE_PATH
fi

export ROS_PACKAGE_PATH=$CATKIN_WS/src/webots_nodes/webconsoleMulti:$ROS_PACKAGE_PATH

cd $CURRENT_PATH
forkedPids=()

###### Setting ros params and exporting variables ###### 
rosparam set use_sim_time true
export MAP=$ROOT_DIR/rosbuild_ws/maps/$MAP_NAME\.yaml
export NAVMAP=$ROOT_DIR/rosbuild_ws/maps/$MAP_NAME\_nav.yaml

###### This is to avoid Nan transforms from AMCL ###### 
rosparam delete /$1/amcl			

echo "Initializing nodes for $1"
###### Spawn a new process ######
# { roslaunch webots_nodes webotMbot.launch mbotName:=$1 &	2>&3 & } 3>&2 2>/dev/null 	
# { roslaunch webots_nodes webotMbot.launch mbotName:=$1	2>>stderr.log &}
roslaunch webots_nodes webotMbot.launch mbotName:=$1 &	
forkedPids+=($!)

wait
echo "BYE!!"
