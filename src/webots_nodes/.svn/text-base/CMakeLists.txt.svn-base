cmake_minimum_required(VERSION 2.8.3)
project(webots_nodes)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp rospy std_msgs message_generation tf sensor_msgs)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

#######################################
## Declare ROS messages and services ##
#######################################

## Generate messages in the 'msg' folder
add_message_files(
    FILES
    wheelSpeed.msg
)

## Generate services in the 'srv' folder
  add_service_files(
    FILES
    camera_get_info.srv
    camera_save_image.srv
    connector_get_presence.srv
    connector_lock.srv
    connector_set_presence.srv
    differential_wheels_enable_control.srv
    differential_wheels_get_speed.srv
    display_draw_line.srv
    display_draw_oval.srv
    display_draw_pixel.srv
    display_draw_polygon.srv
    display_draw_rectangle.srv
    display_draw_text.srv
    display_fill_oval.srv
    display_fill_polygon.srv
    display_fill_rectangle.srv
    display_get_info.srv
    display_image_copy.srv
    display_image_delete.srv
    display_image_load.srv
    display_image_new.srv
    display_image_paste.srv
    display_image_save.srv
    display_set_alpha.srv
    display_set_color.srv
    display_set_opacity.srv
    emitter_get_buffer_size.srv
    emitter_get_channel.srv
    emitter_get_range.srv
    emitter_send.srv
    emitter_set_channel.srv
    emitter_set_range.srv
    field_get_bool.srv
    field_get_color.srv
    field_get_count.srv
    field_get_float.srv
    field_get_int32.srv
    field_get_node.srv
    field_get_rotation.srv
    field_get_string.srv
    field_get_type.srv
    field_get_type_name.srv
    field_get_vec2f.srv
    field_get_vec3f.srv
    field_import_node.srv
    field_set_bool.srv
    field_set_color.srv
    field_set_float.srv
    field_set_int32.srv
    field_set_rotation.srv
    field_set_string.srv
    field_set_vec2f.srv
    field_set_vec3f.srv
    led_get.srv
    led_set.srv
    motor_get_max_position.srv
    motor_get_min_position.srv
    motor_get_target_position.srv
    motor_set_acceleration.srv
    motor_set_available_force.srv
    motor_set_available_torque.srv
    motor_set_control_p.srv
    motor_set_force.srv
    motor_set_position.srv
    motor_get_position.srv
    motor_get_target_position.srv
    motor_set_torque.srv
    motor_set_velocity.srv
    node_get_center_of_mass.srv
    node_get_number_of_contact_points.srv
    node_get_contact_point.srv
    node_get_field.srv
    node_get_orientation.srv
    node_get_position.srv
    node_get_static_balance.srv
    node_get_type.srv
    node_get_type_name.srv
    pen_set_ink_color.srv
    pen_write.srv
    receiver_get_channel.srv
    receiver_get_data_size.srv
    receiver_get_emitter_direction.srv
    receiver_get_queue_length.srv
    receiver_get_signal_strength.srv
    receiver_next_packet.srv
    receiver_set_channel.srv
    robot_device_list.srv
    robot_get_basic_time_step.srv
    robot_get_controller_arguments.srv
    robot_get_controller_name.srv
    robot_get_key.srv
    robot_get_mode.srv
    robot_get_model.srv
    robot_get_number_of_devices.srv
    robot_get_project_path.srv
    robot_get_synchronization.srv
    robot_get_time.srv
    robot_get_type.srv
    robot_set_keyboard.srv
    robot_set_mode.srv
    robot_set_time_step.srv
    sensor_get_type.srv
    sensor_sampling_period.srv
    sensor_set.srv
    supervisor_export_image.srv
    supervisor_get_from_def.srv
    supervisor_get_root.srv
    supervisor_set_label.srv
    supervisor_simulation_physics_reset.srv
    supervisor_simulation_quit.srv
    supervisor_simulation_revert.srv
    supervisor_start_movie.srv
    supervisor_stop_movie.srv
  )

## Generate added messages and services with any dependencies listed here
  generate_messages(
    DEPENDENCIES
    std_msgs
  )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES webots_nodes
   CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Instructions for Laser node

add_executable(Laser src/Laser.cpp)

add_dependencies(Laser webots_nodes_generate_messages_cpp)

target_link_libraries(Laser
	${catkin_LIBRARIES}
)

## Instructions for navigationExecutor node

add_executable(navigationExecutor src/navigationExecutor.cpp)

add_dependencies(navigationExecutor webots_nodes_generate_messages_cpp)

target_link_libraries(navigationExecutor
	${catkin_LIBRARIES}
)


## Instructions for Odometry node

add_executable(Odometry src/Odometry.cpp)

add_dependencies(Odometry webots_nodes_generate_messages_cpp)

target_link_libraries(Odometry
	${catkin_LIBRARIES}
)
## Instructions for simulationClock node

add_executable(simulationClock src/simulationClock.cpp)

add_dependencies(simulationClock webots_nodes_generate_messages_cpp)

target_link_libraries(simulationClock
	${catkin_LIBRARIES}
)
## Instructions for battery node

add_executable(Battery src/Battery.cpp)

add_dependencies(Battery webots_nodes_generate_messages_cpp)

target_link_libraries(Battery
	${catkin_LIBRARIES}
)
