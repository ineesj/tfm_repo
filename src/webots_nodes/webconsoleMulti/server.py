
import re
import math
import shutil
import os
import sys
import os.path as osp
import urllib
import mimetypes
import numpy as np
import Image as PILImage
from BaseHTTPServer import *
import socket
import subprocess

NODE_NAME = 'scout_webconsole'
import roslib; roslib.load_manifest(NODE_NAME)
import rospy
import yaml
import tf
import tf.transformations

from std_msgs.msg import *
from geometry_msgs.msg import *
from sensor_msgs.msg import *
from tf.msg import *
from scout_msgs.msg import *
from scout_msgs.srv import *

try:
	from monarch_msgs.msg import *
	USE_MBOT = True
except ImportError:
	USE_MBOT = False

DEFAULT_PORT = 8888
#SERVER_ADDRESS = ('', DEFAULT_PORT)
ROOT_PATH = osp.dirname(osp.realpath(__file__))
STATIC_PATH = osp.join(ROOT_PATH, "static")
DEFAULT_HOME = "home_scout.html"
DEFAULT_CAMERA = "/stream?topic=/camera/image_raw"

# DEFAULT_MAP = "sala_corredor_crop"
# DEFAULT_MAPFILE = os.path.join(roslib.packages.get_pkg_dir('maps'), DEFAULT_MAP+".yaml")
#DEFAULT_MAPFILE = []
DEFAULT_MAP = None

RRAD  = 0.41/2

STDEV_XY = 0.2
STDEV_TH = np.deg2rad(30)

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
COV0 = [STDEV_XY**2, 0, 0, 0, 0, 0,
		0, STDEV_XY**2, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, STDEV_TH**2]


class Server(BaseHTTPRequestHandler):

	def log_request(self, *args):
		pass
	
	def do_GET(self):
		self.serve('GET')
		
	def do_HEAD(self):
		self.serve('HEAD')

	def serve(self, method):
		for (pat, func) in self.URLS:
			m = re.match(pat, self.path)
			if m is not None:
				func(self, method, m)
				return
		self.send_error(404, "Not found: %s"%(self.path))

	def handle_home(self, method, match):
		self.send_response(301)
		self.send_header("Location", "/static/"+DEFAULT_HOME)
		self.end_headers()

	def handle_static(self, method, match):
		fn = osp.normpath(osp.join(STATIC_PATH, match.group(1)))
		if fn.startswith(STATIC_PATH):
			try:
				ctype = mimetypes.guess_type(fn)
				stat = os.stat(fn)
				with open(fn) as fh:
					self.send_response(200)
					self.send_header("Content-type", ctype)
					self.send_header("Content-Length", str(stat[6]))
					self.end_headers()
					if method=='GET':
						shutil.copyfileobj(fh, self.wfile)
			except (IOError, OSError):
				self.send_error(404, "Unknown URL: %s"%(match.group()))
		else:
			self.send_error(404, "Unauthorized")

	def handle_do(self, method, match):
		reqs = match.group(1).split('?', 1)
		cmd  = reqs[0]
		args = reqs[1].split('&') if len(reqs)>1 else []
		for (c, f) in self.CMDS:
			if cmd==c:
				f(self, method, args)
				return
		self.send_error(404, "Unrecognized request")

	def header(self, type):
		self.send_response(200)
		self.send_header("Content-type", type)
		self.end_headers()

	def handle_map(self, method, args):
		self.header("text/xml")
		if method=='GET':
			self.wfile.write(proxy.get_map())

	def handle_map_img(self, method, args):
		self.header("image/png")
		if method=='GET':
			fn = proxy.get_map_img_path(args[0])
			im = PILImage.open(fn)
			im.save(self.wfile, 'png')

	def handle_set_location(self, method, args):
		if len(args)==3:
			(x, y, t) = map(float, args)
			self.header("text/plain")
			(suc, res) = proxy.set_pose(x, y, t)
			if method=='GET':
				print >>self.wfile, res

	def handle_set_dct_goal(self, method, args):
		if len(args)==3:
			(x, y, t) = map(float, args)
			self.header("text/plain")
			(suc, res) = proxy.set_dct_goal(x, y, t)
			if method=='GET':
				print >>self.wfile, res

	def handle_set_route_goal(self, method, args):
		if len(args)==3:
			(x, y, t) = map(float, args)
			self.header("text/plain")
			(suc, res) = proxy.set_route_goal(x, y, t)
			if method=='GET':
				print >>self.wfile, res

	def handle_teleop(self, method, args):
		self.header("text/plain")
		(suc, res) = proxy.send_teleop(args)
		if method=='GET':
			print >>self.wfile, res

	def handle_get_mbot_name(self, method, args):
		self.header("text/plain")
		(suc, res) = proxy.get_mbot_name()
		if method=='GET':
			print >>self.wfile, res



	# Keep this to the end of class definition
	URLS = (('^/$', handle_home),
			('^/static/(.*)$', handle_static),
			('^/do/(.*)$', handle_do))

	CMDS = (('map', handle_map),
			('map_img', handle_map_img),
			('set_location', handle_set_location),
			('set_dct_goal', handle_set_dct_goal),
			('set_route_goal', handle_set_route_goal),
			('teleop', handle_teleop),
			('get_mbot_name', handle_get_mbot_name))




class Proxy:
	pose  = None
	lidar = {}
	particlecloud = None
	battery = voltage = None
	battery_db = {}
	undock_pubs = None
	temperature = None
	
	def __init__(self):
		rospy.init_node(NODE_NAME, argv=sys.argv)
		global DEFAULT_MAP
		global DEFAULT_HOME
		global DEFAULT_CAMERA

		DEFAULT_HOME = rospy.get_param("~home", DEFAULT_HOME)
		rospy.loginfo("home: %s"%(DEFAULT_HOME))
		DEFAULT_CAMERA = rospy.get_param("~camera", DEFAULT_CAMERA)
		rospy.loginfo("camera: %s"%(DEFAULT_CAMERA))
		mapfile = rospy.get_param("navmap", rospy.get_param("map"))
		rospy.loginfo("map: %s"%(mapfile))
		DEFAULT_MAP = os.path.splitext(os.path.basename(mapfile))[0]
		self.load_map(mapfile)
		self.tf = tf.TransformListener()
		self.pose_msg = PoseWithCovarianceStamped(header=Header(frame_id="/map"),
												  pose=PoseWithCovariance(pose=Pose(position=Point(0, 0, 0),
																					orientation=Quaternion(0, 0, 0, 1)),
																		  covariance=COV0))
		self.init_pub   = rospy.Publisher("initialpose", PoseWithCovarianceStamped)
		self.guid_srv   = rospy.ServiceProxy("scout/guidance", GuidanceSrv)
		self.teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
		self.speech_srv = rospy.ServiceProxy("speech", SpeechSrv)
		#rospy.Subscriber("/tf", tfMessage, self.tf_handler, queue_size=1)
		rospy.Subscriber("scan", LaserScan, self.lidar_handler, queue_size=1)
		rospy.Subscriber("particlecloud", PoseArray, self.particlecloud_handler, queue_size=1)
		if USE_MBOT:
			rospy.Subscriber("batteries_voltage", BatteriesVoltage, self.battery_handler_mbot, ['main'])
			rospy.Subscriber("auxiliary_batteries_voltage", AuxiliaryBatteriesVoltage, self.battery_handler_mbot, ['aux'])
			rospy.Subscriber("motor_board_voltages", MotorBoardVoltages, self.voltage_handler_mbot)
			rospy.Subscriber("charger_status", ChargerStatus, self.battery_handler_mbot, ['charger'])
			self.undock_pubs = [rospy.Publisher("set_state_aux_batt1_power", SetStateAuxiliaryPowerBattery),
								rospy.Publisher("set_state_aux_batt2_power", SetStateAuxiliaryPowerBattery),
								rospy.Publisher("set_state_electronics_power", SetStateElectronicPower),
								rospy.Publisher("set_state_motors_power", SetStateMotorsPower)]
		else:
			self.battery_timer = rospy.Timer(rospy.Duration(5), self.battery_handler_omni)
			rospy.Subscriber("/omni/temperature", OmniTemperatures, self.temperature_handler_omni)
			# OLD code for scout
			# rospy.Subscriber("/scout/battery", ScoutBatteryMsg, self.battery_handler_scout, queue_size=1)
		rospy.loginfo("Proxy ready")

	def p2w(self, (px, py)):
		x = self.x0 + self.scale*px
		y = self.y0 + self.scale*(self.H-py-1)
		return (x, y)

	def w2p(self, (x, y)):
		px = int(round((x - self.x0) / self.scale))
		py = int(self.H - round((y - self.y0) / self.scale) - 1)
		return (px, py)

	def np_w2p(self, (x, y)):
		px = np.rint((x - self.x0) / self.scale).astype('int')
		py = (self.H - np.rint((y - self.y0) / self.scale) - 1).astype('int')
		return (px, py)

	def load_map(self, mapfile):
		# Load raw data
		with open(mapfile) as fh:
			self.meta = yaml.load(fh)
		#
		pgm_file = os.path.join(os.path.dirname(mapfile), self.meta['image'])
		img = PILImage.open(pgm_file)
		(self.x0, self.y0, self.z0) = self.meta['origin']
		(self.W, self.H) = img.size
		self.scale = self.meta['resolution']

	def set_pose(self, xp, yp, tp):
		(x, y) = self.p2w((xp, yp))
		t = -tp
		self.pose_msg.header.stamp = rospy.get_rostime()
		self.pose_msg.pose.pose.position.x = x
		self.pose_msg.pose.pose.position.y = y
		self.pose_msg.pose.pose.orientation.z = math.sin(t/2.0)
		self.pose_msg.pose.pose.orientation.w = math.cos(t/2.0)
		self.init_pub.publish(self.pose_msg)
		return (True, "Location set to (%.2f,%.2f,%.2f)"%(x,y,t))

	def set_dct_goal(self, xp, yp, tp):
		(x, y) = self.p2w((xp, yp))
		t = -tp
		try:
			ret = self.guid_srv(cmd=1, x=x, y=y, t=t)
			return (ret.success, ret.result)
		except rospy.ServiceException:
			return (False, "Navigation service unavailable")

	def set_route_goal(self, xp, yp, tp):
		(x, y) = self.p2w((xp, yp))
		t = -tp
		try:
			ret = self.guid_srv(cmd=2, x=x, y=y, t=t)
			return (ret.success, ret.result)
		except rospy.ServiceException:
			return (False, "Navigation service unavailable")

	def get_mbot_name(self):
		if not rospy.get_namespace():
			return(False, "Namespace not defined (this is sympthom that thre is something wrong)")
		else:
			return (True, rospy.get_namespace().strip('/'))

	def send_teleop(self, args):
		# format: keyword -> (cmd, fac)
		TELEOP_MAP = { 'up':     (1,  1),
					   'down':   (1, -1),
					   'left':   (2,  2),
					   'right':  (2, -2),
					   'sleft':  (3,  1),
					   'sright': (3, -1),
					   'stop':   (0,  0)}
		cmd = args[0]
		if cmd=='say':
			content = urllib.unquote(args[1])
			try:
				ret = self.speech_srv(content)
				if ret.success:
					return (True, "Said \"%s\""%(content))
				else:
					return (False, "Unable to speak")
			except rospy.ServiceException:
				return (False, "Speech service unavailable")
		elif cmd=='undock':
			if self.request_undock():
				return (True, "Disconnection from charger requested")
			else:
				return (False, "Undocking unavailable")
		else:
			(cmd, fac) = TELEOP_MAP[cmd]
			speed = 0.25
			vel   = fac*speed
			tout  = 0
			try:
				ret = self.teleop_srv(cmd, vel, tout)
				return (ret.success, ret.result)
			except rospy.ServiceException:
				return (False, "Teleoperation service unavailable")

				
	# tf_failed = False
	# def tf_handler(self, msg):
	#     for trf in msg.transforms:
	#         try:
	#             #t = self.tf.getLatestCommonTime("/base_link", "/map")
	#             position, quaternion = self.tf.lookupTransform("/map", "/base_link", rospy.Time())
	#             self.pose = (position[0], position[1], 2*math.atan2(quaternion[2], quaternion[3]))
	#             if self.tf_failed:
	#                 rospy.loginfo("[webconsole] Regained robot pose")
	#             self.tf_failed = False
	#         except tf.Exception:
	#             if not self.tf_failed:
	#                 rospy.logwarn("[webconsole] Failed to get robot pose -- IGNORING")
	#                 self.tf_failed = True

	def lidar_handler(self, scan):
		frame = scan.header.frame_id
		try:
			position, quaternion = self.tf.lookupTransform("/map", frame, rospy.Time())
		except tf.Exception:
			return
		z = np.array(scan.ranges)
		a = np.arange(scan.angle_min, scan.angle_min+scan.angle_increment*len(z), scan.angle_increment)
		assert len(z)==len(a), "Range and angle arrays do not match"
		valid = (z>scan.range_min)&(z<scan.range_max)
		if valid.any():
			# obtain laser pointcloud in device coordinates
			z, a = z[valid], a[valid]
			xl = z*np.cos(a)
			yl = z*np.sin(a)
			# transform pointcloud according to tf
			T = tf.transformations.quaternion_matrix(quaternion)
			T[0:3,3] = position
			pl = np.vstack([xl, yl, np.zeros_like(z), np.ones_like(z)])
			pw = np.dot(T, pl)
			xw = pw[0,:]
			yw = pw[1,:]
			self.lidar[frame] = self.np_w2p((xw, yw))

	def OLD_lidar_handler(self, scan):
		frame = scan.header.frame_id
		try:
			position, quaternion = self.tf.lookupTransform("/map", frame, rospy.Time())
			pose = (position[0], position[1], 2*math.atan2(quaternion[2], quaternion[3]))
		except tf.Exception:
			return
		(xr, yr, tr) = pose
		z = np.array(scan.ranges)
		a = tr + np.arange(scan.angle_min, scan.angle_min+scan.angle_increment*len(z), scan.angle_increment)
		#a = tr + np.arange(scan.angle_min, scan.angle_max+scan.angle_increment, scan.angle_increment)
		assert len(z)==len(a), "Range and angle arrays do not match"
		valid = (z>scan.range_min)&(z<scan.range_max)
		if valid.any():
			# project LIDAR pointcloud into map
			z, a = z[valid], a[valid]
			xw = xr + z*np.cos(a)
			yw = yr + z*np.sin(a)
			self.lidar[frame] = self.np_w2p((xw, yw))

	def particlecloud_handler(self, msg):
		n = len(msg.poses)
		poses = np.empty((n, 3))
		for i in xrange(n):
			p = msg.poses[i]
			poses[i] = (p.position.x, p.position.y, 2*math.atan2(p.orientation.z, p.orientation.w))
		(xw, yw) = self.np_w2p((poses[:,0], poses[:,1]))
		tw = -poses[:,2]
		self.particlecloud = (xw, yw, tw)

	def battery_handler_scout(self, msg):
		self.battery = "%.3fV / %.3fV"%(msg.battery1, msg.battery2)

	def battery_handler_omni(self, event):
		try:
			ln = subprocess.check_output(["acpi", "-b"])
			fs = ln.strip().split(':', 1)
			if len(fs)>1:
				fs2 = fs[1].split(',', 1)
				self.battery = (fs2[1] if len(fs2)>1 else fs[1])
		except OSError:
			self.battery = "(?)"
		if self.temperature is not None:
			self.battery += " [motors: %dC %dC %dC]"%self.temperature

	def temperature_handler_omni(self, msg):
		self.temperature = (msg.motor1, msg.motor2, msg.motor3)
		
	def battery_handler_mbot(self, msg, args):
		FMT = "%.1fV"
		self.battery_db[args[0]] = msg
		charger = electronics = motors = pc1 = pc2 = "NA"
		pc1_bc = pc2_bc = motors_bc = electronics_bc = ""
		if 'main' in self.battery_db:
			m  = self.battery_db['main']
			charger = FMT%(m.charger)
			electronics = FMT%(m.electronics)
			motors = FMT%(m.motors)
		if 'aux' in self.battery_db:
			m  = self.battery_db['aux']
			pc1 = FMT%(m.pc1)
			pc2 = FMT%(m.pc2)
		if 'charger' in self.battery_db:
			m  = self.battery_db['charger']
			pc1_bc = "(pc1 charging)" if m.pc1BatteryCharging else ""
			pc2_bc = "(pc2 charging)" if m.pc2BatteryCharging else ""
			motors_bc = "(motors charging)" if m.motorsBatteryCharging else ""
			electronics_bc = "(electronics charging)" if m.electronicsBatteryCharging else ""
		self.battery = "pc1=%s pc2=%s motors=%s electronics=%s / charger=%s %s %s %s %s"%(pc1,pc2,motors,electronics,charger,pc1_bc,pc2_bc,motors_bc,electronics_bc)

	def voltage_handler_mbot(self, msg):
		FMT = "%.1fV"
		mvolt = FMT%(msg.motorVoltage)
		dvolt = FMT%(msg.driverVoltage)
		evolt = FMT%(msg.electronicsVoltage)
		mden  = "ENABLED" if msg.motorDriversEnabled else "DISABLED"
		mpok  = "ON" if msg.motorPowerOk else "OFF"
		dpok  = "ON" if msg.driverPowerOk else "OFF"
		epok  = "ON" if msg.electronicsPowerOk else "OFF"
		self.voltage = "motor=%s driver=%s electronics=%s / motor drivers %s, motor power %s,  driver power %s, electronics power %s"%(mvolt,dvolt,evolt,mden,mpok,dpok,epok)

	def available_maps(self):
		res = []
		path = roslib.packages.get_pkg_dir('maps')
		for fn in os.listdir(path):
			if fn.endswith(".yaml"):
				res.append(fn[:-5])
		return res
		
	def get_map(self):
		map_url = '/do/map_img?' + DEFAULT_MAP
		xml = '<?xml version="1.0"?>\n<map>\n<img>%s</img>\n'%(map_url)
		try:
			position, quaternion = self.tf.lookupTransform("/map", "/base_link", rospy.Time())
			pose = (position[0], position[1], 2*math.atan2(quaternion[2], quaternion[3]))
			(x, y, t) = pose
			(px, py)  = self.w2p((x, y))
			pr        = RRAD / self.scale
			xml += '<robot x="%d" y="%d" t="%.3f" r="%d"/>\n'%(px,py,t,pr)
		except tf.Exception:
			rospy.logwarn("[webconsole] Failed to get robot pose -- IGNORING")
		if len(self.lidar)>0:
			xml += '<lidar>\n'
			for (frame,(xl,yl)) in self.lidar.iteritems():
				xml += ''.join(['<p x="%d" y="%d"/>\n'%(xl[i], yl[i]) for i in xrange(len(xl))])
			xml += '</lidar>\n'
		if self.particlecloud is not None:
			(xl, yl, tl) = self.particlecloud
			xml += '<particlecloud>\n'
			xml += ''.join(['<p x="%d" y="%d" t="%.3f"/>\n'%(xl[i], yl[i], tl[i]) for i in xrange(len(xl))])
			xml += '</particlecloud>\n'
		if self.battery is not None:
			xml += '<battery>%s</battery>\n'%(self.battery)
		if self.voltage is not None:
			xml += '<voltage>%s</voltage>\n'%(self.voltage)
		xml += '<camera>%s</camera>\n'%(DEFAULT_CAMERA)
		maps = self.available_maps()
		xml += '<maps current="%s">%s</maps>\n'%(DEFAULT_MAP, ''.join(['<m id="%s">%s</m>'%(m,m) for m in maps]))
		xml += '</map>\n'
		return xml

	def get_map_img_path(self, name):
		yfn = osp.join(roslib.packages.get_pkg_dir('maps'), name+".yaml")
		with open(yfn) as yfh:
			meta = yaml.load(yfh)
		return osp.join(osp.dirname(yfn), meta['image'])

	def request_undock(self):
		RELAY_DELAY = 0.25
		if self.undock_pubs is None:
			return False
		else:
			(aux1, aux2, elec, moto) = self.undock_pubs
			aux1.publish(SetStateAuxiliaryPowerBattery(status=2))
			rospy.sleep(RELAY_DELAY)
			aux2.publish(SetStateAuxiliaryPowerBattery(status=2))
			rospy.sleep(RELAY_DELAY)
			elec.publish(SetStateElectronicPower(status=2))
			rospy.sleep(RELAY_DELAY)
			moto.publish(SetStateMotorsPower(status=2))
			rospy.sleep(RELAY_DELAY)
			return True


def main():
	global proxy
	ServerPort = DEFAULT_PORT
	proxy = Proxy()
	while True:
		try:
			httpd = HTTPServer(('', ServerPort), Server)
			rospy.loginfo("web console for mbot %s is running on port %d", rospy.get_namespace(), ServerPort)
			break
		except socket.error:
			rospy.loginfo("web socket on port %d is busy, trying next port!!..", ServerPort)
			ServerPort=ServerPort+1

	httpd.serve_forever()

if __name__=="__main__":
	main()

# EOF
