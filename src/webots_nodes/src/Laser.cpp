/* Enable the topic range_finder for both front and back LRF, 
 creates a node LaserScan subscribing to the topic, deserializing the data and creating  
 a message of type LaserScan which is advertised under the name 
 of /scan and publish in /MBOT_NAME/tf topic the transformation for 
 the two LRF wrt to the base_link frame.
*/

#include "ros/ros.h"

#include <webots_nodes/robot_device_list.h>
#include <webots_nodes/sensor_set.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32MultiArray.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_broadcaster.h>

#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <signal.h>
#include <stdio.h>
#include <limits>
#include <algorithm>

#define TIME_STEP 100   //refresh rate of LRF (10 Hz).  This parameter heavily affects the running time of the simulation in webots

static int controllerCount;
static std::vector<std::string> controllerList;
static std::string controllerName;

ros::ServiceClient LaserFrontClient;
ros::ServiceClient LaserBackClient;
webots_nodes::sensor_set LaserSrv;
ros::Publisher LaserScanPublisher[2]; 

//---------------------------------
//
//    ROS CALLBACKS
//
//---------------------------------

void quit(int sig)
{
  ROS_INFO("user stopped the node LaserRangeFinder.");
  ros::shutdown();
  exit(0);
}

// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr& name)
{
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("controller #%d : %s", controllerCount, controllerList.back().c_str());
}
//laser callback
void laserCallback(const std_msgs::Float32MultiArray::ConstPtr& values, ros::Publisher& LaserScanPublisher, std::string deviceName) {

  static tf::TransformBroadcaster br;
  sensor_msgs::LaserScan scan;
  tf::Transform tf;
  std::string laser_frame;
  std::string base_frame;

  if(!deviceName.compare("URG_04LX_front")){
    tf = tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.2795, 0.0, 0.1352));
    laser_frame = "lrf01";
    base_frame = "base_link";
  }
  else if(!deviceName.compare("URG_04LX_back")){
    tf= tf::Transform(tf::Quaternion(0, 1, 0, 0), tf::Vector3(-0.254, 0.0, 0.1352));
    laser_frame = "lrf02";
    base_frame = "base_link";
  }

  br.sendTransform(tf::StampedTransform( tf, ros::Time::now(), base_frame, laser_frame));

  scan.header.stamp = ros::Time::now();
  scan.header.frame_id = laser_frame;
  scan.angle_min = -2.08621382713;
  scan.angle_max =  2.08621382713;
  scan.angle_increment = 0.00613592332229;
  scan.time_increment = 9.76562514552e-05;
  scan.scan_time = 0.10000000149;
  scan.range_min = 0.019999999553;
  scan.range_max = 5.59999990463;

  for (std::vector<float>::const_iterator it = values->data.begin(); it != values->data.end(); ++it){
    //Need to crop value at 90% of the range cause webots LRF above maximum range returns max_range and we need inf instead
    if (*it <= 0.9*scan.range_max)
      scan.ranges.push_back(*it); 
    else
      scan.ranges.push_back(std::numeric_limits<float>::infinity());
  }
  //reversing the vector in the right position wrt the frame
  if(!deviceName.compare("URG_04LX_front"))
    std::reverse(scan.ranges.begin(),scan.ranges.end());

  LaserScanPublisher.publish(scan);
}
//Enable laser topic on webots
ros::Subscriber enableLaser(ros::NodeHandle n, std::string controllerName, std::string deviceName, ros::Publisher& LaserScanPublisher ) {
std::stringstream ssTimeStep;
ssTimeStep << TIME_STEP;

ros::ServiceClient LaserClient = n.serviceClient<webots_nodes::sensor_set>("/"+controllerName+"/"+ deviceName +"/set_sensor");
webots_nodes::sensor_set LaserSrv;
  
LaserSrv.request.period = TIME_STEP;
ros::Subscriber laserData;
  
if (LaserClient.call(LaserSrv) && LaserSrv.response.success == 1) {
  ROS_INFO("Laser scan %s enabled with sampling period of %d ms", deviceName.c_str(), LaserSrv.request.period);
    
  laserData = n.subscribe<std_msgs::Float32MultiArray>("/"+controllerName+"/"+ deviceName +"/"+ssTimeStep.str()+'_'+"range_finder", 1 , boost::bind(laserCallback, _1,  LaserScanPublisher, deviceName) );
        
  // wait for the topics to be initialized
  while (laserData.getNumPublishers() == 0);
}
else
ROS_ERROR("Failed to call service set_sensor ");
return laserData;
}

//---------------------------------
//
//    MAIN
//
//---------------------------------

int main(int argc, char **argv)
{
  // create a node named 'LaserRangeFinder' on ROS network
  ros::init(argc, argv, "LaserRangeFinder");  
  ros::NodeHandle n;
 
  signal(SIGINT,quit);

  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n.subscribe("/model_name", 100, controllerNameCallback);
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers())
  {
   ros::spinOnce();
   ros::spinOnce();
   ros::spinOnce();
  }
  ros::spinOnce();

  //choose the appropriate controller relative to its relative namespace
  int i=0;
  bool controllerFound= false;
  while (i< controllerCount && controllerFound==false){
    std::string NameSapce = n.getNamespace();
    if(controllerList[i].compare(NameSapce.erase(0,2))== 0){
      controllerFound= true;
      controllerName= controllerList[i];
    }
    i++;
  }
  if (!controllerFound){
    ROS_ERROR("No controller suitable for the namespace where i m running...ABORTING");
    return(-1);
  }
    ROS_DEBUG("name space where i m running is %s and my controller is %s", n.getNamespace().c_str(), controllerName.c_str());

  nameSub.shutdown();

  ros::Subscriber FrontLaserSensor;
  ros::Subscriber BackLaserSensor;

  LaserScanPublisher[0] = n.advertise<sensor_msgs::LaserScan>("scan", 1);   //front LRF
  LaserScanPublisher[1] = n.advertise<sensor_msgs::LaserScan>("scan", 1);   //back LRF

  ROS_INFO("Enabling laser scan topics.");

  FrontLaserSensor = enableLaser(n, controllerName, "URG_04LX_front", LaserScanPublisher[0]);
  BackLaserSensor = enableLaser(n, controllerName, "URG_04LX_back", LaserScanPublisher[1]);
  
  // Main Loop
  ros::Rate loop_rate(10);
  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
}
