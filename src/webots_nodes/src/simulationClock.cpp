

#include "ros/ros.h"

#include <webots_nodes/robot_device_list.h>
#include <webots_nodes/robot_set_time_step.h>
#include <webots_nodes/robot_get_time.h>
#include <rosgraph_msgs/Clock.h>
#include <std_msgs/String.h>


#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <signal.h>
#include <stdio.h>
#include <limits>
#include <algorithm>

#define TIME_STEP 50 

static int controllerCount;
static std::vector<std::string> controllerList;
static std::string controllerName;

ros::ServiceClient timeStepClient;
ros::ServiceClient timeClient;

webots_nodes::robot_set_time_step timeStepSrv;
webots_nodes::robot_get_time timeSrv;

ros::Publisher timePub;

//---------------------------------
//
//    ROS CALLBACKS
//
//---------------------------------

void quit(int sig)
{
  ROS_INFO("user stopped the node simulationClock.");
  ros::shutdown();
  exit(0);
}

// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr& name)
{
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("controller #%d : %s", controllerCount, controllerList.back().c_str());
}

void timePublisher(){

  rosgraph_msgs::Clock simulationClock;
  timeClient.call(timeSrv);

  simulationClock.clock.sec = (int)timeSrv.response.time;
  simulationClock.clock.nsec = (timeSrv.response.time - simulationClock.clock.sec) *  1.0e+9;
  timePub.publish(simulationClock);
}
//---------------------------------
//
//    MAIN
//
//---------------------------------

void empty_cb(const rosgraph_msgs::Clock::ConstPtr& time){}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "simulationClock", ros::init_options::AnonymousName );  
  ros::NodeHandle n;
 

  signal(SIGINT,quit);

  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n.subscribe("/model_name", 100, controllerNameCallback);
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers())
  {
   ros::spinOnce();
   ros::spinOnce();
   ros::spinOnce();
  }
  ros::spinOnce();

  //Check if /clock is already being published
  ros::Subscriber clock_sub = n.subscribe<rosgraph_msgs::Clock>("/clock", 1, empty_cb);
  //sleep a random time DON'T TAKE THIS OUT OR THERE MIGHT BE MULTIPLE CLOCK INSTANCES RUNNING

  std::string mbotName = argv[1];
  char ch = *mbotName.rbegin();
  srand ((int)ch);
  float sleepTime = (2.5 - 1.0) * ((((float) rand()) / (float) RAND_MAX)) + 0.5 ;
  sleep(sleepTime);
  //
  ROS_WARN("%d", clock_sub.getNumPublishers());
  if(clock_sub.getNumPublishers()== 0 ){ 

    clock_sub.shutdown();
    //for each controller activate the time step
    for (int i= 0; i< controllerCount; i++){
      ROS_INFO("activating time step for controller %s", controllerList[i].c_str());
      timeStepClient = n.serviceClient<webots_nodes::robot_set_time_step>("/"+controllerList[i]+"/Robot/time_step");
      timeStepSrv.request.step = TIME_STEP;
      timeStepClient.call(timeStepSrv);
    }
    nameSub.shutdown();

    //Use the first controller to advertise the simulation clock on /clock
    timeClient = n.serviceClient<webots_nodes::robot_get_time>("/"+controllerList[0]+"/Robot/get_time");
    timeSrv.request.ask = 1;

    timePub = n.advertise<rosgraph_msgs::Clock>("clock", 100);
    ROS_INFO("Simulation clock Enabled");
    
    // Main Loop
    while (ros::ok())
      timePublisher();
  }
  else{
    quit(-1);
    ROS_WARN("/clock is already being advertised...shutting down current node...");
  }

}
