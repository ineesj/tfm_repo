#!/usr/bin/env python

import roslib
roslib.load_manifest('webots_nodes')

import rospy
from monarch_behaviors.srv import BehaviorRequest
from monarch_behaviors.srv import BehaviorRequestRequest
from monarch_msgs.msg import BehaviorSelection
from monarch_msgs.msg import KeyValuePair
from std_srvs.srv import Empty
from std_msgs.msg import Header
from sam_helpers.reader import SAMReader
from functools import partial

import sys
import signal
import re
import subprocess

import time
import os
import rospkg
from subprocess import Popen



class SimulationObject(object):

	def __init__(self, msg,  robot_list, iterations_nb, topic_to_record, timeout):
		self.robot_list = robot_list
		self.behavior_req = BehaviorRequestRequest()
		self.behavior_req.behavior = msg

		self.iterations_nb = iterations_nb

		self.restart_request = rospy.ServiceProxy('/restartSimulation', Empty)
		self.behavior_request = rospy.ServiceProxy('/mcentral/request_behavior', BehaviorRequest)

		self.rospack = rospkg.RosPack()
		self.setup_script = self.rospack.get_path("webots_nodes") + "/setup_mbot.sh"
		self.mcentral_script = [ 'roslaunch', 'webots_nodes', 'mcentral_simulation.launch']
		
		if not os.path.exists(self.rospack.get_path("webots_nodes")+'/data'):
			os.makedirs(self.rospack.get_path("webots_nodes")+'/data')


		self.rosbag_record = ['rosbag', 'record']
		self.rosbag_record.extend(topic_to_record)

		self.robots_current_executing_behaviors = {robot_list[idx] : None for idx in range(0,len(robot_list))}
		self.restart = False
		self.behaviors_readers = []
		print 'Registering timeout callback'
		self.timeout = rospy.Timer(rospy.Duration(timeout), self.timeoutExpired_cb)


	def timeoutExpired_cb(self, event):
		print 'Timeout expired!! Going to reset the simulation!!'
		sys.stdout.flush()
		self.restart = True


	def current_executing_behaviors_cb(self, msg, mbot):

		if self.robots_current_executing_behaviors[mbot] != None:
			if len(msg.array) == 0 or (len(msg.array) != 0 and self.robots_current_executing_behaviors[mbot] != msg.array[0].name):
				self.restart = True
				# print 'robot ', mbot , ' just finished the current behavior!!!!'
				print 'Behavior terminated!!'
		elif len(msg.array) != 0:
			self.robots_current_executing_behaviors[mbot] = msg.array[0].name

	def startSimulation(self):

		iteration = 0
		while not rospy.is_shutdown() and iteration < iterations_nb:

			print "*************************************************************"
			print "*********************** ITERATION ", iteration, "************************" 
			print "*************************************************************"

			try:
				#############  New iteration: ##############
				self.restart = False
				pidList = []
				devnull = open(os.devnull, 'wb')
				# Restart mcentral nodes and SAM
				print '-----------> Executing ', ' '.join(self.mcentral_script)
				pidList.append(Popen(self.mcentral_script, stdout=devnull, stderr=devnull))
				time.sleep(15.)
				#Start recording the bag for the current iteration 
				current_rosbag =  self.rosbag_record
				current_rosbag.append('-O')
				current_rosbag.append(self.rospack.get_path("webots_nodes")+'/data/'+ self.behavior_req.behavior.name +'_'+ str(iteration)+'.bag')
				bag_pid = Popen(current_rosbag, stdout=devnull, stderr=devnull)

				################### Restart all the nodes running on robots #####################

				for mbot in robot_list:
					if mbot != '00':
						print '-----------> Executing ', self.setup_script + ' ' + mbot
						pidList.append(Popen([self.setup_script, 'mbot' + mbot], stdout=devnull, stderr=devnull))
						#Create readers to check action status
						self.behaviors_readers.append( SAMReader('[mbot'+mbot+'] CurrentExecutingBehaviors', 
															partial(self.current_executing_behaviors_cb,  mbot = mbot)) )
					else:
						#Create readers to check action status
						self.behaviors_readers.append( SAMReader('[mpc01] CurrentExecutingBehaviors', 
															partial(self.current_executing_behaviors_cb,  mbot = mbot)) )

					time.sleep(5.)
				time.sleep(10.)
				################# Activating behavior #######################
				try:
					rospy.wait_for_service('/mcentral/request_behavior')
					self.behavior_request(self.behavior_req)

				except rospy.ServiceException, e:
					print "Service call failed: %s"%e
					for singlePid in pidList:
						self.terminate_process_and_children(singlePid)
						# Popen(['pkill','-TERM', '-P', str(singlePid.pid)], stdout=devnull, stderr=devnull)
					Popen(['pkill','-TERM', '-P',  str(os.getpid())], stdout=devnull, stderr=devnull)

				############### Waiting for the behavior to terminate #########################
				r = rospy.Rate(0.5)
				sys.stdout.write('RUNNING')			

				while not self.restart: 
					sys.stdout.write('*')
					sys.stdout.flush()
					r.sleep()
				print ''

				########## Once i am out i need to unregisters the sam readers and to clean up all the processes ######################
				print 'Doing clenup after the simulation'
				self.timeout.shutdown()
				self.terminate_process_and_children(bag_pid)

				for idx in range(0,len(robot_list)):
					self.behaviors_readers[idx].remove()
					self.robots_current_executing_behaviors[robot_list[idx]] = None

				for singlePid in pidList:
					self.terminate_process_and_children(singlePid)
					# Popen(['pkill','-TERM', '-P', str(singlePid.pid)], stdout=devnull, stderr=devnull)
				Popen(['pkill','-TERM', '-P',  str(os.getpid())], stdout=devnull, stderr=devnull)

				iteration+=1
				self.restart_request()
				time.sleep(10.)

			################ Catch all that could possibly go wrong cause i dont want to leave any process hanging ################
			except Exception, e:
				print 'Something went wrong and i had to kill all the nodes!!'
				for singlePid in pidList:
					self.terminate_process_and_children(singlePid)
				Popen(['pkill','-TERM', '-P',  str(os.getpid())], stdout=devnull, stderr=devnull)
				print 'CLEAN!!!'

		print 'DONE!!'

	def terminate_process_and_children(self, pid):
	    ps_command = Popen("ps -o pid --ppid %d --noheaders" % pid.pid, shell=True, stdout=subprocess.PIPE)
	    ps_output = ps_command.stdout.read()
	    retcode = ps_command.wait()
	    assert retcode == 0, "ps command returned %d" % retcode
	    for pid_str in ps_output.split("\n")[:-1]:
	            os.kill(int(pid_str), signal.SIGINT)
	    pid.terminate()

if __name__ == '__main__':

	rospy.init_node('multiple_simulation', anonymous=True)

	#############   CUSTOMIZE THIS PART ACCORDING TO YOUR NEEDS   #####################
	iterations_nb = 2						#Number of iterations

	robot_list = ['00', '01']				#List of robots in the simulation you wish to activate   oss: 00 referes to mcentral
	#Create behavior selection request
	msg = BehaviorSelection()
	msg.name = "interactivegame"
	msg.instance_id = 0
	msg.robots = [0, 1]												# number 0 refers to mcentral
	msg.parameters.append(KeyValuePair(Header(),'players','mbot01'))
	msg.parameters.append(KeyValuePair(Header(),'rows','3'))
	msg.parameters.append(KeyValuePair(Header(),'cols','3'))
	msg.parameters.append(KeyValuePair(Header(),'level','0'))
	msg.parameters.append(KeyValuePair(Header(),'tracking','0'))
	msg.active = True
	#List of topics you want to record for each run. The rosbag will record data for each run and save in a folder
	# inside webots_nodes/data with filename template run_behavior_#iteration. Be careful to save these files if you
	# are running the same behavior multiple times becasue it will overwrite the file. Please specify the full name
	#of the topic included the namespace.

	topic_to_record = ['/mbot01/scan','/shapes']
	timeout = 300				# seconds

	######################################### END OF CUSTOMIZABLE PART #########################################

	obj = SimulationObject(msg,  robot_list, iterations_nb, topic_to_record, timeout)
	obj.startSimulation()