/*Creates a node Battery publishing battery values*/
#include "ros/ros.h"

#include <webots_nodes/robot_set_time_step.h>
#include <webots_nodes/sensor_set.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>
#include <monarch_msgs/BatteriesVoltage.h>
#include <monarch_msgs/AuxiliaryBatteriesVoltage.h>
#include <monarch_msgs/MotorBoardVoltages.h>
#include <monarch_msgs/ChargerStatus.h>

#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <cmath>
#include <signal.h>
#include <stdio.h>

#define TIME_STEP 1000
#define MAX_READING 57600
#define MAX_VOLTAGE 13.6

static int controllerCount;
static float lastJouleReading = -1;
static float currentVoltage = -1;
static bool recharging = false;
static std::vector<std::string> controllerList;
static std::string controllerName;


ros::Subscriber batterySubscriber;
ros::ServiceClient timeStepClient;
webots_nodes::robot_set_time_step timeStepSrv;
ros::ServiceClient batteryClient; 
webots_nodes::sensor_set batterySrv;
ros::Publisher MotorVoltagePublisher; 
ros::Publisher BatteryVoltagePublisher;
ros::Publisher AuxiliaryBatteryVoltagePublisher;
ros::Publisher ChargerStatusPublisher;

//---------------------------------
//
//    ROS CALLBACKS
//
//---------------------------------

void quit(int sig){
	ros::shutdown();
	exit(0);
}

// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr& name){
	controllerCount++;
	controllerList.push_back(name->data);
	ROS_INFO("controller #%d : %s", controllerCount, controllerList.back().c_str());
}

void computeVoltageCallback(const std_msgs::Float64::ConstPtr& jouleReading){
	monarch_msgs::MotorBoardVoltages motor_voltages;
	monarch_msgs::BatteriesVoltage batteries_voltage;
	monarch_msgs::AuxiliaryBatteriesVoltage auxiliary_batteries_voltage;
	monarch_msgs::ChargerStatus charger_status;


	if(jouleReading-> data != lastJouleReading){
		if(jouleReading-> data < lastJouleReading){
			recharging = false;
			ROS_DEBUG("DISCHARGING");
		}
		else if(jouleReading-> data > lastJouleReading) {
			ROS_DEBUG("RECHARGING");
			recharging = true;
		}
		currentVoltage = (log(jouleReading-> data -1)*MAX_VOLTAGE)/log(MAX_READING-1);
		ROS_INFO("Voltage is %f volts", currentVoltage);
	}
	//status: fully charged
	if( recharging == true && jouleReading-> data == .99*MAX_READING){
			ROS_DEBUG("Fully charged");
	}
	lastJouleReading = jouleReading-> data;

	//filling up messages
	motor_voltages.header.stamp = ros::Time::now();
	batteries_voltage.header.stamp = ros::Time::now();
	charger_status.header.stamp = ros::Time::now();  
	auxiliary_batteries_voltage.header.stamp = ros::Time::now();

	motor_voltages.motorDriversEnabled = true;
	motor_voltages.motorPowerOk = true;
	motor_voltages.driverPowerOk = true;
	motor_voltages.electronicsPowerOk = true;

	if(recharging== false){
		batteries_voltage.charger = 0.0;
		charger_status.pc2BatteryCharging = false;
		charger_status.pc1BatteryCharging = false;
		charger_status.motorsBatteryCharging = false;
		charger_status.electronicsBatteryCharging = false;
	}
	else{
		batteries_voltage.charger = MAX_VOLTAGE;
		charger_status.pc2BatteryCharging = true;
		charger_status.pc1BatteryCharging = true;
		charger_status.motorsBatteryCharging = true;
		charger_status.electronicsBatteryCharging = true;
	}
	//

	if(currentVoltage!= -1){
		motor_voltages.motorVoltage = currentVoltage;
		motor_voltages.driverVoltage = currentVoltage;
		motor_voltages.electronicsVoltage = currentVoltage;
		batteries_voltage.electronics = currentVoltage;
		batteries_voltage.motors = currentVoltage;
		auxiliary_batteries_voltage.pc1 = currentVoltage;
		auxiliary_batteries_voltage.pc2 = currentVoltage;

		//publishing
		MotorVoltagePublisher.publish(motor_voltages);
		BatteryVoltagePublisher.publish(batteries_voltage);
		AuxiliaryBatteryVoltagePublisher.publish(auxiliary_batteries_voltage);
		ChargerStatusPublisher.publish(charger_status);
	}
}



//---------------------------------
//
//    MAIN
//
//---------------------------------

int main(int argc, char **argv)
{
	ros::init(argc, argv, "Battery"); 
	ros::NodeHandle n;

	signal(SIGINT,quit);

	ros::Subscriber nameSub = n.subscribe("/model_name", 100, controllerNameCallback);
	while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers()){
		ros::spinOnce();
		ros::spinOnce();
		ros::spinOnce();
	}
	ros::spinOnce();

	//choose the appropriate controller relative to its relative namespace
	int i=0;
	bool controllerFound = false;
	while (i< controllerCount && controllerFound==false){
		std::string NameSapce = n.getNamespace();
		if(controllerList[i].compare(NameSapce.erase(0,2))== 0){
			controllerFound= true;
			controllerName= controllerList[i];
		}
		i++;
	}
	if (!controllerFound){
		ROS_ERROR("No controller suitable for the namespace where i m running...ABORTING");
		return(-1);
	}
	nameSub.shutdown();


	MotorVoltagePublisher = n.advertise<monarch_msgs::MotorBoardVoltages>("motor_board_voltages", 100); 
	BatteryVoltagePublisher = n.advertise<monarch_msgs::BatteriesVoltage>("batteries_voltage", 100);
	AuxiliaryBatteryVoltagePublisher = n.advertise<monarch_msgs::AuxiliaryBatteriesVoltage>("auxiliary_batteries_voltage", 100);
	ChargerStatusPublisher = n.advertise<monarch_msgs::ChargerStatus>("charger_status", 100);

	batteryClient= n.serviceClient<webots_nodes::sensor_set>("/"+controllerName+"/battery_sensor/set_sensor");    
	batterySrv.request.period = TIME_STEP;    //100
		
	if (batteryClient.call(batterySrv) && batterySrv.response.success == 1) {
		std::stringstream ssTimeStep;
		ssTimeStep << TIME_STEP;
		
		ROS_INFO("Battery sensor topic is activated succesfully with sampling period of %d", TIME_STEP);
		batterySubscriber = n.subscribe<std_msgs::Float64>("/"+controllerName+"/battery_sensor/"+ ssTimeStep.str(), 1, boost::bind(computeVoltageCallback, _1));
	}
	else
		ROS_ERROR("Failed to call service set_sensor ");
	
	 // Main Loop
	ros::Rate loop_rate(1);
	while (ros::ok())
	{ 
		ros::spinOnce();
		loop_rate.sleep();
	}
}
