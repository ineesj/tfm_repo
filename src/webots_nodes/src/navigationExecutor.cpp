#include "ros/ros.h"

#include <webots_nodes/robot_device_list.h>
#include <webots_nodes/robot_set_time_step.h>
#include <webots_nodes/motor_set_position.h>
#include <webots_nodes/motor_set_velocity.h>
#include <webots_nodes/wheelSpeed.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>

#include <signal.h>
#include <stdio.h>

#include <boost/bind.hpp>
#include <boost/ref.hpp>

#define MAX_POSITION 1E20	// "Infinite" value for motor position - bug using INFINITY
#define L1 0.175	       	// Distance from robot center to wheel center (x axis) (in m)
#define L2 0.146	       	// Distance from robot center to wheel center (y axis) (in m)
#define L 0.321		      	// Sum of L1 + L2
#define R 0.05			     // Wheel radius (in m)

using namespace std;

static int controllerCount;
static std::vector<std::string> controllerList;
static std::string controllerName;
static bool cmdVelLock = false;

ros::ServiceClient timeStepClient;
ros::Publisher VelocityPublisher;
ros::Subscriber joyStickInput;
ros::Subscriber moveBaseInput;
webots_nodes::robot_set_time_step timeStepSrv;

bool obstacleAvoidance;
float moveBaseVelocities[3];
float joystickVelocities[3]= {0};


// Services for Wheels - set_position and set_velocity (wheel position in robot: N=North,E=East,...)
ros::ServiceClient NEWheelPositionClient;
webots_nodes::motor_set_position NEWheelPositionSrv;

ros::ServiceClient NEWheelVelocityClient;
webots_nodes::motor_set_velocity NEWheelVelocitySrv;

ros::ServiceClient NWWheelPositionClient;
webots_nodes::motor_set_position NWWheelPositionSrv;

ros::ServiceClient NWWheelVelocityClient;
webots_nodes::motor_set_velocity NWWheelVelocitySrv;

ros::ServiceClient SWWheelPositionClient;
webots_nodes::motor_set_position SWWheelPositionSrv;

ros::ServiceClient SWWheelVelocityClient;
webots_nodes::motor_set_velocity SWWheelVelocitySrv;

ros::ServiceClient SEWheelPositionClient;
webots_nodes::motor_set_position SEWheelPositionSrv;

ros::ServiceClient SEWheelVelocityClient;
webots_nodes::motor_set_velocity SEWheelVelocitySrv;

//---------------------------------
//
//    ROS CALLBACKS
//
//---------------------------------

void quit(int sig)
{
  ROS_INFO("user stopped the node navigationExecutor.");
  ros::shutdown();
  exit(0);
}

// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr& name)
{
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("controller #%d : %s", controllerCount, controllerList.back().c_str());
}

// read velocity commands from joy topic
void joyVelocityCallback(const sensor_msgs::Joy::ConstPtr& joyInput)
{  
  // vector with linear velocity in x,y and angular velocity
  joystickVelocities[0]= joyInput-> axes[1];
  joystickVelocities[1]= joyInput-> axes[0]; 
  joystickVelocities[2]= joyInput-> axes[2];

}
// read inputs from cmd_vel topic published by move_base node
void moveBaseVelocityCallback(const geometry_msgs::Twist::ConstPtr& moveBasenput)
{
  // vector with linear velocity in x,y and angular velocitY
  moveBaseVelocities[0]= moveBasenput-> linear.x;
  moveBaseVelocities[1]= moveBasenput-> linear.y; 
  moveBaseVelocities[2]= moveBasenput-> angular.z;
  cmdVelLock = true;
}
//************************
void computePrioritizedVelocityCallback(const ros::TimerEvent& Timer, float joyVelocities[], float movebaseVelocities[]){

  float motorPosition[4]= {MAX_POSITION,MAX_POSITION,MAX_POSITION,MAX_POSITION}; // motor position values: inf or -inf
  float motorVelocity[4];                                                        // motor velocity values
  float commandVelocity[3];

  //Giving higher priority to joystick inputs.

  if((joyVelocities[0]!= 0 || joyVelocities[1]!= 0 || joyVelocities[2]!= 0 ) ){
    commandVelocity[0] = joyVelocities[0];
    commandVelocity[1] = joyVelocities[1];
    commandVelocity[2] = joyVelocities[2];
  }
  else if(moveBaseInput.getNumPublishers() > 0 && cmdVelLock ){
    cmdVelLock =false;
    commandVelocity[0] = movebaseVelocities[0];
    commandVelocity[1] = movebaseVelocities[1];
    commandVelocity[2] = movebaseVelocities[2];

  }
  else{
    commandVelocity[0] = 0.0;
    commandVelocity[1] = 0.0;
    commandVelocity[2] = 0.0; 
  }

  // computing angular velocities for wheels according to kinematic equations and max motor velocity
  motorVelocity[0] = (commandVelocity[0] - commandVelocity[1] - (L1 + L2)*commandVelocity[2])/R;
  motorVelocity[1] = (commandVelocity[0] + commandVelocity[1] + (L1 + L2)*commandVelocity[2])/R;
  motorVelocity[2] = (commandVelocity[0] + commandVelocity[1] - (L1 + L2)*commandVelocity[2])/R;
  motorVelocity[3] = (commandVelocity[0] - commandVelocity[1] + (L1 + L2)*commandVelocity[2])/R;

  ROS_DEBUG("Transformed command into angular velocity for wheels:  NW %f - NE %f - SW %f - SE %f",motorVelocity[0],motorVelocity[1],motorVelocity[2],motorVelocity[3]);

  for (int i= 0; i< 4; i++)
    if (motorVelocity[i] < 0.0)
    {
      motorPosition[i] = -MAX_POSITION;
      motorVelocity[i] = -motorVelocity[i];
    }

  //Remap wheel s configuration to fit webots configuration
  NWWheelPositionSrv.request.position = motorPosition[0];
  SWWheelPositionSrv.request.position = motorPosition[2];
  SEWheelPositionSrv.request.position = motorPosition[3];
  NEWheelPositionSrv.request.position = motorPosition[1];

  if (!NEWheelPositionClient.call(NEWheelPositionSrv) || NEWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new NE wheel position command to the robot");

  if (!NWWheelPositionClient.call(NWWheelPositionSrv) || NWWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new NW wheel position command to the robot");

  if (!SWWheelPositionClient.call(SWWheelPositionSrv) || SWWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new SW wheel position command to the robot");

  if (!SEWheelPositionClient.call(SEWheelPositionSrv) || SEWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new SE wheel position command to the robot");

  // set requested velocity to motors
  NWWheelVelocitySrv.request.velocity = motorVelocity[0];
  SWWheelVelocitySrv.request.velocity = motorVelocity[2];
  SEWheelVelocitySrv.request.velocity = motorVelocity[3];
  NEWheelVelocitySrv.request.velocity = motorVelocity[1];

  //send the velocity commands
  if (!NEWheelVelocityClient.call(NEWheelVelocitySrv) || NEWheelVelocitySrv.response.success !=1)
    ROS_ERROR("failed to send new velocity command for NE wheel to the robot");

  if (!NWWheelVelocityClient.call(NWWheelVelocitySrv) || NWWheelVelocitySrv.response.success !=1)
    ROS_ERROR("failed to send new velocity command for NW wheel to the robot");

  if (!SWWheelVelocityClient.call(SWWheelVelocitySrv) || SWWheelVelocitySrv.response.success !=1)
    ROS_ERROR("failed to send new velocity command for SW wheel to the robot");

  if (!SEWheelVelocityClient.call(SEWheelVelocitySrv) || SEWheelVelocitySrv.response.success !=1)
    ROS_ERROR("failed to send new velocity command for SE wheel to the robot");

}

//Enable wheels position and velocity control, set position to 0.0
void initializeWheels(ros::NodeHandle n){
  // NE Wheel
  NEWheelPositionClient = n.serviceClient<webots_nodes::motor_set_position>("/"+controllerName+"/wheel1/set_position");
  NEWheelVelocityClient = n.serviceClient<webots_nodes::motor_set_velocity>("/"+controllerName+"/wheel1/set_velocity");

  NEWheelPositionSrv.request.position = 0.0;
  if (!NEWheelPositionClient.call(NEWheelPositionSrv) || NEWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new NE wheel position command to the robot");

  // NW Wheel
  NWWheelPositionClient = n.serviceClient<webots_nodes::motor_set_position>("/"+controllerName+"/wheel2/set_position");
  NWWheelVelocityClient = n.serviceClient<webots_nodes::motor_set_velocity>("/"+controllerName+"/wheel2/set_velocity");

  NWWheelPositionSrv.request.position = 0.0;
  if (!NWWheelPositionClient.call(NWWheelPositionSrv) || NWWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new NW wheel position command to the robot");

  // SE Wheel
  SEWheelPositionClient = n.serviceClient<webots_nodes::motor_set_position>("/"+controllerName+"/wheel3/set_position");
  SEWheelVelocityClient = n.serviceClient<webots_nodes::motor_set_velocity>("/"+controllerName+"/wheel3/set_velocity");

  SEWheelPositionSrv.request.position = 0.0;
  if (!SEWheelPositionClient.call(SEWheelPositionSrv) || SEWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new SE wheel position command to the robot");

  // SW Wheel
  SWWheelPositionClient = n.serviceClient<webots_nodes::motor_set_position>("/"+controllerName+"/wheel4/set_position");
  SWWheelVelocityClient = n.serviceClient<webots_nodes::motor_set_velocity>("/"+controllerName+"/wheel4/set_velocity");

  SWWheelPositionSrv.request.position = 0.0;
  if (!SWWheelPositionClient.call(SWWheelPositionSrv) || SWWheelPositionSrv.response.success != 1)
    ROS_ERROR("failed to send new SW wheel position command to the robot");

}

//---------------------------------
//
//    MAIN
//
//---------------------------------

int main(int argc, char **argv)
{
  // create a node named 'navigationExecutor' on ROS network
  ros::init(argc, argv, "navigationExecutor");
  ros::NodeHandle n;

  signal(SIGINT,quit);

  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n.subscribe("/model_name", 100, controllerNameCallback);
   while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers())
   {
     ros::spinOnce();
     ros::spinOnce();
     ros::spinOnce();
   }
  ros::spinOnce();

  //choose the appropriate controller relative to its relative namespace
  int i=0;
  bool controllerFound= false;
 while (i< controllerCount && controllerFound==false){
    std::string NameSapce = n.getNamespace();
    if(controllerList[i].compare(NameSapce.erase(0,2))== 0){
      controllerFound= true;
      controllerName= controllerList[i];
    }
    i++;
  }
  if (!controllerFound){
    ROS_ERROR("No controller suitable for the namespace where i m running...ABORTING");
    return(-1);
  }
  nameSub.shutdown();

  //Intialize robot
  initializeWheels(n);
  timeStepClient = n.serviceClient<webots_nodes::robot_set_time_step>("/"+controllerName+"/Robot/time_step");

  ROS_INFO("Robot set up complete, robot is ready.");
  
  //subscribe to topic /cmd_vel and to topic /joy. /joy topic inputs have higher priority
  joyStickInput = n.subscribe<sensor_msgs::Joy>("joy", 1 , boost::bind(joyVelocityCallback, _1));                                    //JOYSTICK DISABLED IN FOR ROBOTS. UNCOMMENT THIS TO REACTIVATE
  moveBaseInput = n.subscribe<geometry_msgs::Twist>(n.getNamespace()+"/cmd_vel", 1 , boost::bind(moveBaseVelocityCallback, _1));

  //Timed callback 20Hz
  ros::Timer timer = n.createTimer(ros::Duration(0.05), boost::bind(computePrioritizedVelocityCallback, _1, joystickVelocities, moveBaseVelocities));

  // Main Loop
  ros::Rate loop_rate(10);
  while (ros::ok()){
    ros::spinOnce();
    loop_rate.sleep();
  }
}
