/*Creates a node Odometry publishing on /tf topic the transformation 
from odom frame to base_link frame

*/
#include "ros/ros.h"

#include <webots_nodes/robot_set_time_step.h>
#include <webots_nodes/wheelSpeed.h>
#include <webots_nodes/motor_set_position.h>
#include <webots_nodes/sensor_set.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>

#include <tf/transform_broadcaster.h>

#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <signal.h>
#include <stdio.h>
#include <math.h> 

#define L1 0.175          // Distance from robot center to wheel center (x axis) (in m)
#define L2 0.146          // Distance from robot center to wheel center (y axis) (in m)
#define L 0.321           // Sm of L1 + L2
#define R 0.05           // Wheel radius (in m)
#define TIME_STEP 50

static int controllerCount;
static std::vector<std::string> controllerList;
static std::string controllerName;
static bool synchLock[4]= {true, true, true, true};


ros::Subscriber wheelPositionsSubscriber[4];  //NE NW SE SW
geometry_msgs::TransformStamped odom_tf;
ros::ServiceClient timeStepClient;
nav_msgs::Odometry odometry;
ros::Publisher odomPublisher;
webots_nodes::robot_set_time_step timeStepSrv;

double previousCallTime = -1.0;
float previousWheelPositionNE, previousWheelPositionSE, previousWheelPositionNW, previousWheelPositionSW;
float WheelPositions[4]= {0.0, 0.0, 0.0, 0.0};
float odometryX = 0.0;
float odometryY = 0.0;
float odometryTheta = 0.0;

ros::ServiceClient wheelGetPositionClient[4];   //NE NW SE SW


//---------------------------------
//
//    ROS CALLBACKS
//
//---------------------------------

void quit(int sig)
{

  ROS_INFO("user stopped the node Odometry.");
  ros::shutdown();
  exit(0);
}

// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr& name)
{
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("controller #%d : %s", controllerCount, controllerList.back().c_str());
}

void computeOdometry(float NEWheel, float NWWheel, float SEWheel, float SWWheel)
{ 
  double startTime = ros::Time::now().toSec();
  double RobotFrameSpeed[3];
  double elapsedTime;

  float distanceWheelNE;
  float distanceWheelNW;
  float distanceWheelSE;
  float distanceWheelSW;  

  //handles the first time the function is called at node startup and the case where not all the readings from all the four wheels where received (mutex)
  elapsedTime= ros::Time::now().toSec() - previousCallTime;

  distanceWheelNE = NEWheel - previousWheelPositionNE;
  distanceWheelNW = NWWheel - previousWheelPositionNW;
  distanceWheelSE = SEWheel - previousWheelPositionSE;
  distanceWheelSW = SWWheel - previousWheelPositionSW;
    
  if(previousCallTime!= -1 && std::find(synchLock, synchLock+4, true) == synchLock+4 && elapsedTime!= 0){     

    //Transform wheel speed into robot velocity: dx, dy, dTheta
    RobotFrameSpeed[0] = R/4 * (distanceWheelNW/elapsedTime + distanceWheelNE/elapsedTime + distanceWheelSW/elapsedTime + distanceWheelSE/elapsedTime);
    RobotFrameSpeed[1] = R/4 * (-distanceWheelNW/elapsedTime + distanceWheelNE/elapsedTime + distanceWheelSW/elapsedTime - distanceWheelSE/elapsedTime);
    RobotFrameSpeed[2] = R/4 * ( -((distanceWheelNW/elapsedTime)/L) + ((distanceWheelNE/elapsedTime)/L) - ((distanceWheelSW/elapsedTime)/L) + ((distanceWheelSE/elapsedTime)/L));
    
    //Updating odometry
    odometryX += cos(odometryTheta) * (RobotFrameSpeed[0]* elapsedTime) - sin(odometryTheta) * (RobotFrameSpeed[1] * elapsedTime);
    odometryY += sin(odometryTheta) * (RobotFrameSpeed[0]* elapsedTime) + cos(odometryTheta) * (RobotFrameSpeed[1] * elapsedTime);
    odometryTheta += RobotFrameSpeed[2]* elapsedTime;
  }
  else{
        ROS_DEBUG("Some locsk are still locked");
  }

   ROS_DEBUG("Odometry is: X %f - Y %f - Theta %f", odometryX, odometryY, odometryTheta);

  //Advertise current tf
  geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(odometryTheta);

  //filling in tf message
  odom_tf.header.stamp = ros::Time::now();
  odom_tf.header.frame_id = "odom";
  odom_tf.child_frame_id = "base_link";
  odom_tf.transform.translation.x = odometryX;
  odom_tf.transform.translation.y = odometryY;
  odom_tf.transform.translation.z = 0.0;       //ground plane
  odom_tf.transform.rotation = odom_quat;

  //publishinhg on topic odom
  odometry.header.stamp = ros::Time::now();
    geometry_msgs::PoseWithCovariance PoseCovariance;
      geometry_msgs::Pose Pose;
        geometry_msgs::Point position;
          position.x = odometryX;
          position.y = odometryY;
          position.z = 0.0;
      Pose.position = position;
      Pose.orientation = odom_quat;
    PoseCovariance.pose = Pose;
  odometry.pose = PoseCovariance;    

  //update previous wheel Positions with current values
  previousWheelPositionNE = NEWheel;
  previousWheelPositionNW = NWWheel;
  previousWheelPositionSE = SEWheel;
  previousWheelPositionSW = SWWheel;

  previousCallTime = startTime;
  //re enable the locks
  for(int i=0; i< 4; i++)
    synchLock[i]= true;

  //ROS_INFO("Callback Hz %f", (1/(ros::Time::now().toSec()- startTime)));
}

void storeWheelPositionCB(const std_msgs::Float64::ConstPtr& wheelPosition, int wheelNumber){
  WheelPositions[wheelNumber]= wheelPosition-> data;
  synchLock[wheelNumber]= false;
}

void enableWheelPosition(ros::NodeHandle n, int wheelID, ros::Subscriber& wheelSubscriber) {
  std::stringstream ssTimeStep;
  ssTimeStep << TIME_STEP;
  std::stringstream wheelIDString;
  wheelIDString << wheelID+1;

  webots_nodes::sensor_set positionSrv;
    
  positionSrv.request.period = TIME_STEP;    //50ms
    
  if (wheelGetPositionClient[wheelID].call(positionSrv) && positionSrv.response.success == 1) {
    ROS_INFO("wheel %d position topic enabled with sampling period %d ms", (wheelID+1), positionSrv.request.period);
    wheelSubscriber = n.subscribe<std_msgs::Float64>("/"+controllerName+"/wheel"+ wheelIDString.str() +"PositionSensor/"+ssTimeStep.str(), 1 , boost::bind(storeWheelPositionCB, _1, wheelID));

    // wait for the topics to be initialized
    while (wheelSubscriber.getNumPublishers() == 0);
  }

}

//---------------------------------
//
//    MAIN
//
//---------------------------------

int main(int argc, char **argv)
{
  ros::init(argc, argv, "Odometry"); 
  tf::TransformBroadcaster br; 
  ros::NodeHandle n;

  signal(SIGINT,quit);

  // Get the list of availables controllers
  ros::Subscriber nameSub = n.subscribe("/model_name", 100, controllerNameCallback);
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers())
  {
    ros::spinOnce();
    ros::spinOnce();
    ros::spinOnce();
  }
  ros::spinOnce();

  //choose the appropriate controller relative to its relative namespace
  int i=0;
  bool controllerFound= false;
while (i< controllerCount && controllerFound==false){
    std::string NameSapce = n.getNamespace();
    if(controllerList[i].compare(NameSapce.erase(0,2))== 0){
      controllerFound= true;
      controllerName= controllerList[i];
    }
    i++;
  }
  if (!controllerFound){
    ROS_ERROR("No controller suitable for the namespace where i'm running...ABORTING");
    return(-1);
  }
  ROS_INFO("name space where i'm running is %s and my controller is %s", n.getNamespace().c_str(), controllerName.c_str());
  // leave topic once it's not necessary anymore
  nameSub.shutdown();

  //Initialize odom tf
  odom_tf.header.stamp = ros::Time::now();
  odom_tf.header.frame_id = "odom";
  odom_tf.child_frame_id = "base_link";
  odom_tf.transform.translation.x = 0.0;
  odom_tf.transform.translation.y = 0.0;
  odom_tf.transform.translation.z = 0.0;
  odom_tf.transform.rotation = tf::createQuaternionMsgFromYaw(0);
  br.sendTransform(odom_tf);

  //Initialize wheelPosition
  previousWheelPositionNE = 0.0;
  previousWheelPositionSE = 0.0;
  previousWheelPositionNW = 0.0;
  previousWheelPositionSW = 0.0;

  for(int i=0; i< 4; i++){

    std::ostringstream robotNumber;
    robotNumber << (i+1);
    wheelGetPositionClient[i]= n.serviceClient<webots_nodes::sensor_set>("/"+controllerName+"/wheel"+ robotNumber.str() +"PositionSensor/set_sensor");
    enableWheelPosition(n, i, wheelPositionsSubscriber[i]);
  }
  
  odomPublisher = n.advertise<nav_msgs::Odometry>("odom", 100); 
  
   // Main Loop
  ros::Rate loop_rate(20);
  while (ros::ok())
  { 
    computeOdometry(WheelPositions[0], WheelPositions[1], WheelPositions[2], WheelPositions[3]);
    br.sendTransform(odom_tf);
    odomPublisher.publish(odometry);
    ros::spinOnce();
    loop_rate.sleep();
  }
}
