import re
import os
import sys
from pylab import *
import argparse

data = {'ForwBack':[], 'Rotate':[], 'Strafe':[]}

def dataPlot(env, save):
	print '****STATISTICS FOR ', env.upper(), '****'

	for key,value in data.iteritems():
		if key=='ForwBack':
			title = 'Forward - backward'
			yLabel= 'meters/sec'
		elif key=='Rotate':
			title= 'Rotation'
			yLabel= 'degrees/sec'
		elif key=='Strafe':
			title= 'Strafing'
			yLabel= 'meters/sec'

		startIdx = 0
		if key=='Rotate':
			startIdx  = 1
		amclData = [x[startIdx] for x in value]
		odomData = [x[startIdx + 2] for x in value]
		measuredData = [x[startIdx + 4] for x in value]

		#Print useful statistics
		print '-----', key, '-----'
		print '  AMCL'.ljust(15) + 'mean:', str(mean(amclData)).ljust(15) + ' variance: ', var(amclData)
		print '  Odometry'.ljust(15) + 'mean:', str(mean(odomData)).ljust(15) + ' variance: ', var(odomData)
		print '  Measurements'.ljust(15) + 'mean:', str(mean(measuredData)).ljust(15) + ' variance: ', var(measuredData)
		#
		if save:
			fig, ax = plt.subplots()	
			ax.grid(True)
			ax.plot(range(0,len(amclData)), amclData, 'g+', label='amcl')
			ax.plot(range(0,len(odomData)), odomData, 'r+', label='odometry')
			ax.plot(range(0,len(measuredData)), measuredData, 'b+', label='real values')
			ax.plot(range(0,len(amclData)), [mean(amclData)]*len(amclData), 'g-', label='amcl avg')
			ax.plot(range(0,len(odomData)), [mean(odomData)]*len(odomData), 'r-', label='odom avg')
			ax.plot(range(0,len(measuredData)), [mean(measuredData)]*len(measuredData), 'b-', label='real avg')
			ax.legend(loc=2) 							# upper left corner
			ax.set_xlabel('# samples', fontsize=18)
			ax.set_ylabel(yLabel, fontsize=18)
			ax.set_title(title)
			plt.savefig('results' + key + '_' + env+'.svg')

def normalizeValues():
	for key, value in data.iteritems():
		for idx, singleSample in enumerate(value):
			value[idx] = [x / singleSample[1] for x in singleSample[2::]]

def parseLines(line):
	currentLine= re.sub("[^-.e0-9 ]", "",line).lstrip().split(" ")
	currentLine = [float(item) for item in currentLine]
	if 'FORWARD' in line or 'BACKWARD' in line:
		data['ForwBack'].append(currentLine)
	elif 'ROTATE' in line:
		data['Rotate'].append(currentLine)
	elif 'STRAFE' in line:
		data['Strafe'].append(currentLine)



def parseFile(lines):
	for line in lines[:]:
		line = re.sub('[()'',]', '', line)
		if not line.isspace() :
			parseLines(line)

##Program start here ##
parser = argparse.ArgumentParser(description='plot values')

parser.add_argument('environment', action="store", type=str, help="Specify whether you are working in simulation or in real environment")
parser.add_argument('--save', action="store_true", default=0, help="If set save plots of the results")

arguments = parser.parse_args()

dataFile = open(os.getcwd()+'/calibrationData.'+ arguments.environment, 'r')
lines = dataFile.readlines()
dataFile.close()
parseFile(lines)

normalizeValues()
dataPlot(arguments.environment, arguments.save)
