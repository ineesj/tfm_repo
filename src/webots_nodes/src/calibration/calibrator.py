#!/usr/bin/env python
import rospy
import os
import sys
import math
import time

from numpy import *
from numpy.linalg import *

from std_msgs.msg import String
import tf
import rospkg
rospack = rospkg.RosPack()
sys.path.append(os.path.join(rospack.get_path("scout_msgs"), "src"))
from scout_msgs.srv import *
from webots_nodes.srv import *
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry 



class Calibrator(object):
    locked= True
    dump= list()
    commands={'FB': 1,'R': 2, 'S': 3}
    keyMappings= {'i': 'FORWARD', 'm':'BACKWARD', 'o':'ROTATE_RIGHT','u':'ROTATE_LEFT','l':'STRAFE_RIGHT','j':'STRAFE_LEFT'}
    estimatedPose= list()
    odom= list()
    simMeasured= list()
    #Create initial rotation matrix according to initial webots rotation
    initialRotation = array([1, 0, 0, 0, 0, 1, 0, -1, 0])
    initialRotation = initialRotation.reshape(3,3)
    initialRotation = asmatrix(initialRotation)

    CALIBRATION_RADIUS= 0.4215          #Radius of calibration circle in EPFL lab room in meters

    def __init__(self, env, rNode='Null'):
        rospy.loginfo('Initializing object')
        self.f = open(os.getcwd()+'/calibrationData.'+ env, 'a')
        self.environment  = env
        if rNode!='Null':
            self.robotNode = rNode

    def readInput(self):
        command= 0
        velocity= 0.2
        
        print "Command [i m l j o u] -- press q to quit:  "
        direction = raw_input()

        if direction=='i':                  #FORWARD
            command= self.commands['FB']
        elif direction=='m':                #BACKWARD
            command= self.commands['FB']
            velocity= -velocity
        elif direction=='o':                #ROTATE RIGHT
            command= self.commands['R']
            velocity= -velocity
        elif direction=='u':                #ROTATE LEFT
            command= self.commands['R']
        elif direction=='l':                #STRAFE RIGHT
            command= self.commands['S']
            velocity= -velocity
        elif direction=='j':                #STRAFE LEFT
            command= self.commands['S']
        elif direction=='q':
            self.f.close()
            rospy.signal_shutdown("User requested shutdown")
            exit()
        else:
            print "Illegal key"
            rospy.logerror("Illegal key")
            rospy.signal_shutdown("shutting down")
            exit()

        return (direction, command, velocity)   

    def poseEstimateCB(self, data):
        if not self.locked:
            self.estimatedPose.append((data.pose.pose.position.x, data.pose.pose.position.y, data.pose.pose.position.z))
            self.estimatedPose.append(math.degrees(tf.transformations.euler_from_quaternion((data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w))[2]))
            self.locked= True

    def odomCB(self, data):
        if not self.locked:
            self.odom.append((data.pose.pose.position.x, data.pose.pose.position.y, data.pose.pose.position.z))
            self.odom.append(math.degrees(tf.transformations.euler_from_quaternion((data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w))[2]))
            self.locked= True
     

    def computeDistance(self, data):
        """ Computing the distance travelled by the robot as Euclidean distance and orientation difference """

        initial = array(data[0])
        final = array(data[2])
        travelled_distance = linalg.norm(initial-final)
        theta= abs(data[3] - data[1])
        return (travelled_distance, theta)

    def retriveSimulationData(self):
        """ Calling supervisor service to retrive position and orientation """
        try:
            position = getPos_srv(self.robotNode.node)
            rotation = getRot_srv(self.robotNode.node)

        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

        totalRotation = array(rotation.orientation).reshape(3,3)
        zRotation = self.initialRotation.I * asmatrix(totalRotation)
        self.simMeasured.append((position.position)) 
        self.simMeasured.append(math.degrees(arctan2(zRotation.item(3), zRotation.item(0))))


    def calibrate(self):
        """Calibration routine differentiate the behavior according to the value
        of environment variable which can assume values 'real' or 'simulation'.
        For real measurements the functions expects some user measurements, while for
        simulation it retrives the values by itself by calling the supervisor function, i.e 
        no user interaction is needed apart from the desired direction """

        direction, command, velocity = self.readInput()
        if command:
            if direction not in ['u','o'] and self.environment=='real':
                print 'Input initial reading: '
                initial_reading = float(raw_input())
            elif self.environment=='simulation':
                self.retriveSimulationData()

            self.dump.append(self.keyMappings[direction])
            self.dump.append(velocity)
            self.locked = False
            time.sleep(2)      #make sure the latched message is relative to the last position
            #AMCL
            poseSubscriber = rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, self.poseEstimateCB, queue_size= 1)
            while not self.locked:
                pass
            poseSubscriber.unregister()
            #odometry
            self.locked = False
            poseSubscriber = rospy.Subscriber("odom", Odometry, self.odomCB, queue_size= 1)
            while not self.locked:
                pass
            poseSubscriber.unregister()

            #START MOTION
            startTime= rospy.get_time()
            teleop_srv(command, velocity, 0)
            time.sleep(3)
            teleop_srv(0, 0, 0)
            finishTime= rospy.get_time()
            #END MOTION
            self.locked= False
            time.sleep(4)      #make sure the latched message is relative to the last position
            #AMCL
            poseSubscriber = rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, self.poseEstimateCB, queue_size= 1)
            while not self.locked:
                pass
            poseSubscriber.unregister()
            #odometry
            self.locked = False
            poseSubscriber = rospy.Subscriber("odom", Odometry, self.odomCB, queue_size= 1)
            while not self.locked:
                pass
            poseSubscriber.unregister()

            self.dump.append(finishTime-startTime)
            self.dump.append((self.computeDistance(self.estimatedPose)))
            self.dump.append((self.computeDistance(self.odom)))

            if self.environment=='real':
                ##### Input of final reading  ####
                print 'Input final reading: [if measuring rotation please input the length of the chord, the starting point is discarded]'
                final_reading = float(raw_input())
            elif self.environment=='simulation':
                self.retriveSimulationData()

            if direction in ['i','m','l','j'] and self.environment=='real':
                self.dump.append((abs(final_reading- initial_reading), 0.0))
            elif direction in ['u','o'] and self.environment== 'real':
                self.dump.append((0.0, math.degrees(2*math.asin((final_reading/2)/self.CALIBRATION_RADIUS))))

            elif self.environment=='simulation':
                self.dump.append((self.computeDistance(self.simMeasured)))


            self.f.write("".join(str(self.dump).strip('[]')))
            print 'writing to file...'
            self.f.write('\n')
            self.f.flush()
            self.dump= []
            self.estimatedPose= []
            self.simMeasured= []
            self.odom= []

if __name__ == '__main__':
    if len(sys.argv)!=2 or sys.argv[1] not in ['simulation', 'real']:
        print 'calibrator arg1: {simulation, real}'
        exit(-1)

    rospy.init_node('calibrator')
    rospy.loginfo("initializing calibration")
    teleop_srv = rospy.ServiceProxy("scout/teleop", TeleOpSrv)
    if sys.argv[1]=='simulation':
        getFromDef_srv= rospy.ServiceProxy("/supervisor/get_from_def", supervisor_get_from_def)
        robotNode = getFromDef_srv('MBOT01')
        getPos_srv = rospy.ServiceProxy("/supervisor/node/get_position", node_get_position)
        getRot_srv = rospy.ServiceProxy("/supervisor/node/get_orientation", node_get_orientation)

        calibrator= Calibrator(sys.argv[1], robotNode)
    else:
        calibrator= Calibrator(sys.argv[1])


    while not rospy.is_shutdown():
        calibrator.calibrate()
