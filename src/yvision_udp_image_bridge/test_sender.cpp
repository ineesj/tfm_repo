
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream> 

#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace std; 
using namespace cv; 

#define DGRAM_SIZE 1202
#define IMG_BLOCK_SIZE 1200
#define NUM_BLOCKS 256

#define UDP_SEND_PORT 32000
#define UDP_RECV_PORT 32001


int main(int argc,char **argv) 
{ 
  in_addr_t targetAddress;

  if(argc != 2)
  {
    cout << "Usage: " << argv[0] << " <target ip address>" << endl;
    return -1;
  }

  {
    targetAddress = inet_addr(argv[1]);

    if(targetAddress == INADDR_NONE)
    {
      cout << "failed to parse IP address" << endl;
    }
  }

  Mat image;
  image = imread("test.png", CV_LOAD_IMAGE_GRAYSCALE);   // Read the file

  if(! image.data )                              // Check for invalid input
  {
    cout <<  "Could not open or find the image" << std::endl ;
    return -1;
  }

//    namedWindow("Display Image", CV_WINDOW_AUTOSIZE );
//    imshow("Display Image", image);
//    waitKey(0);

  int sockfd;
  struct sockaddr_in sendaddr;
  struct sockaddr_in recvaddr;
  char msg[DGRAM_SIZE];

  sockfd=socket(AF_INET,SOCK_DGRAM,0);

  bzero(&sendaddr,sizeof(sendaddr));
  bzero(&recvaddr,sizeof(recvaddr));

  sendaddr.sin_family = AF_INET;
  sendaddr.sin_addr.s_addr=htonl(INADDR_ANY); // TODO use address passed as argument
  sendaddr.sin_port=htons(UDP_SEND_PORT);
  bind(sockfd,(struct sockaddr *)&sendaddr,sizeof(sendaddr));

  recvaddr.sin_family = AF_INET;
  recvaddr.sin_addr.s_addr=targetAddress;
  recvaddr.sin_port=htons(UDP_RECV_PORT);

  unsigned char frameNumber = 0;

  while(true)
  {
    unsigned int blockNumber = 0;

    for(;blockNumber<NUM_BLOCKS; ++blockNumber)
    {
      msg[0] = frameNumber;
      msg[1] = (unsigned char)blockNumber;
      memcpy(msg+2, image.data+IMG_BLOCK_SIZE*blockNumber, IMG_BLOCK_SIZE);

      ssize_t res = sendto(sockfd, msg, DGRAM_SIZE, 0, (const sockaddr*)&recvaddr, sizeof(recvaddr));

      if(res!=DGRAM_SIZE)
      {
        cout << "failed to send dgram" << endl;
      }

      usleep(10);
    }


    usleep(11000);

    ++frameNumber;

  }

  return 0; 
}
