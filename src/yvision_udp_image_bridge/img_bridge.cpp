#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/image_encodings.h"

#include <iostream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <unistd.h>

#include <boost/foreach.hpp>

#define DGRAM_SIZE 1202
#define IMG_BLOCK_SIZE 1200
#define NUM_BLOCKS 256
#define NUM_FRAMES 256
#define IMG_WIDTH 640
#define IMG_HEIGHT 480
#define PIXEL_SIZE 1
#define IMG_FORMAT sensor_msgs::image_encodings::MONO8
#define IMG_SIZE (IMG_WIDTH*IMG_HEIGHT*PIXEL_SIZE)


#define UDP_RECV_PORT 32001


using namespace std;
using namespace ros;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "img_bridge");

  ros::NodeHandle n;

  ros::Publisher pub = n.advertise<sensor_msgs::Image>("image", 1000);

  unsigned char * frames = (unsigned char*)calloc(NUM_FRAMES*IMG_SIZE, sizeof(unsigned char));

  if(frames == 0)
  {
    cout << "failed to allocate frames" << endl;
    return -1;
  }



  unsigned char * block_ok = (unsigned char*)calloc(NUM_BLOCKS*NUM_FRAMES,sizeof(unsigned char*));
  unsigned char * block_ok_zero = (unsigned char*)calloc(NUM_BLOCKS, sizeof(unsigned char*));
  unsigned char * block_ok_ones = (unsigned char*)calloc(NUM_BLOCKS, sizeof(unsigned char*));

  memset(block_ok_ones, 1, NUM_BLOCKS);


  Time * frame_time = (Time*)calloc(NUM_FRAMES, sizeof(Time));

  if(frame_time == 0)
  {
    cout << "failed to allocate frame_time" << endl;
    return -1;
  }

  for(unsigned int i=0; i<NUM_FRAMES; ++i)
  {
    Time * tPtr = frame_time+i;
    *tPtr = Time();
  }

  int sockfd;
  struct sockaddr_in recvaddr;
  char msg[DGRAM_SIZE];

  sockfd=socket(AF_INET,SOCK_DGRAM,0);

  bzero(&recvaddr,sizeof(recvaddr));
  recvaddr.sin_family = AF_INET;
  recvaddr.sin_addr.s_addr=htonl(INADDR_ANY);
  recvaddr.sin_port=htons(UDP_RECV_PORT);
  bind(sockfd,(struct sockaddr *)&recvaddr,sizeof(recvaddr));

  while(ros::ok())
  {
    ssize_t s = recv(sockfd, msg, DGRAM_SIZE, 0);

    if(s != DGRAM_SIZE)
    {
      cout << "strange datagram size: " << s << endl;
    }


    Time toa = ros::Time::now();
    unsigned char frameNumber = msg[0];
    unsigned char blockNumber = msg[1];
    Time * tPtr = frame_time + frameNumber;
    unsigned char * blockOkStart = block_ok+NUM_BLOCKS*frameNumber; 
    unsigned char * blockOkItem = block_ok + frameNumber*NUM_BLOCKS + blockNumber;
    unsigned char* localBlockStart = frames+frameNumber*IMG_SIZE+blockNumber*IMG_BLOCK_SIZE;

    //check if previous TOA is too old
    Duration toaOldness = toa - *tPtr;
    if( (tPtr->isZero() == false) && (toaOldness.sec >= 1) )
    {
      cout << "detected incomplete frame: " << (int)frameNumber << " : " << tPtr->toSec() << endl;



      //too old, invalidate block ok
      memcpy(blockOkStart, block_ok_zero, NUM_BLOCKS);

      //reset toa
      *tPtr = Time();
    }


    if(tPtr->isZero())
    {
      *tPtr = toa;
    }   


    // set time of arrival
    

//    cout << "got: " << (int)frameNumber << ":" << (int)blockNumber << endl;


    *blockOkItem = 1;


    memcpy(localBlockStart, msg+2, IMG_BLOCK_SIZE);

    //check if frame complete

    if(memcmp(blockOkStart, block_ok_ones, NUM_BLOCKS) == 0)
    {
      cout << "got complete frame: " << (int)frameNumber << std::endl;


      sensor_msgs::Image img;
      img.header.stamp = *tPtr;
      img.height = IMG_HEIGHT;
      img.width = IMG_WIDTH;
      img.encoding = IMG_FORMAT;
      img.is_bigendian = false;
      img.step = IMG_WIDTH*PIXEL_SIZE;

      img.data.resize(IMG_SIZE);

      unsigned char * data = img.data.data();

      memcpy(data, frames+frameNumber*IMG_SIZE, IMG_SIZE);

 
      pub.publish(img);

      //invalidate block ok
      memcpy(blockOkStart, block_ok_zero, NUM_BLOCKS);

      //reset toa
      *tPtr = Time();
    }


    ros::spinOnce();

  }


  return 0;
}

