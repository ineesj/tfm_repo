#! /bin/bash
rm -rf src/monarch_java_messages
rm -rf src/distributed_coverage
rm -rf src/socially_aware_planner_pkg
rm -rf src/sap_visual
rm -rf src/monarch_perception
rm -rf src/graph_based_formations
rm -rf src/flowfree_game
rm -rf src/kio_uwb*
rm -rf src/uwb_tag_follower
rm -rf src/projector
rm -rf src/ubisense_uwb
rm -rf src/hri_interfaces/mouth
