set(_CATKIN_CURRENT_PACKAGE "arms_maggie")
set(arms_maggie_MAINTAINER "ines <ines@todo.todo>")
set(arms_maggie_DEPRECATED "")
set(arms_maggie_VERSION "0.0.0")
set(arms_maggie_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(arms_maggie_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(arms_maggie_BUILDTOOL_DEPENDS "catkin")