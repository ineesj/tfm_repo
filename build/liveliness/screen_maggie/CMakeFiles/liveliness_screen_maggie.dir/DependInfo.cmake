# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ines/catkin_ws/src/liveliness/screen_maggie/src/liveliness_screen_maggie.cpp" "/home/ines/catkin_ws/build/liveliness/screen_maggie/CMakeFiles/liveliness_screen_maggie.dir/src/liveliness_screen_maggie.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  "MAGICKCORE_QUANTUM_DEPTH=16"
  "MAGICKCORE_HDRI_ENABLE=0"
  "ROS_PACKAGE_NAME=\"screen_maggie\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
