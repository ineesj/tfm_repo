# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/src/liveliness_signal.cpp" "/home/ines/catkin_ws/build/liveliness/liveliness_init/CMakeFiles/liveliness_signal.dir/src/liveliness_signal.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"liveliness_init\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
