# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "liveliness_init: 3 messages, 0 services")

set(MSG_I_FLAGS "-Iliveliness_init:/home/ines/catkin_ws/src/liveliness/liveliness_init/msg;-Istd_msgs:/opt/ros/hydro/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(liveliness_init_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/signalParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init
)
_generate_msg_cpp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/onOffInterfaces.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init
)
_generate_msg_cpp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/colorsParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init
)

### Generating Services

### Generating Module File
_generate_module_cpp(liveliness_init
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(liveliness_init_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(liveliness_init_generate_messages liveliness_init_generate_messages_cpp)

# target for backward compatibility
add_custom_target(liveliness_init_gencpp)
add_dependencies(liveliness_init_gencpp liveliness_init_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS liveliness_init_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/signalParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init
)
_generate_msg_lisp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/onOffInterfaces.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init
)
_generate_msg_lisp(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/colorsParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init
)

### Generating Services

### Generating Module File
_generate_module_lisp(liveliness_init
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(liveliness_init_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(liveliness_init_generate_messages liveliness_init_generate_messages_lisp)

# target for backward compatibility
add_custom_target(liveliness_init_genlisp)
add_dependencies(liveliness_init_genlisp liveliness_init_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS liveliness_init_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/signalParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init
)
_generate_msg_py(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/onOffInterfaces.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init
)
_generate_msg_py(liveliness_init
  "/home/ines/catkin_ws/src/liveliness/liveliness_init/msg/colorsParameters.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init
)

### Generating Services

### Generating Module File
_generate_module_py(liveliness_init
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(liveliness_init_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(liveliness_init_generate_messages liveliness_init_generate_messages_py)

# target for backward compatibility
add_custom_target(liveliness_init_genpy)
add_dependencies(liveliness_init_genpy liveliness_init_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS liveliness_init_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/liveliness_init
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(liveliness_init_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/liveliness_init
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(liveliness_init_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/liveliness_init
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(liveliness_init_generate_messages_py std_msgs_generate_messages_py)
