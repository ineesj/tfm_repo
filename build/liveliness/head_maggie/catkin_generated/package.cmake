set(_CATKIN_CURRENT_PACKAGE "head_maggie")
set(head_maggie_MAINTAINER "ines <ines@todo.todo>")
set(head_maggie_DEPRECATED "")
set(head_maggie_VERSION "0.0.0")
set(head_maggie_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(head_maggie_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(head_maggie_BUILDTOOL_DEPENDS "catkin")