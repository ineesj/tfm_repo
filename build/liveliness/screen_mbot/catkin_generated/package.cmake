set(_CATKIN_CURRENT_PACKAGE "screen_mbot")
set(screen_mbot_MAINTAINER "ines <ines@todo.todo>")
set(screen_mbot_DEPRECATED "")
set(screen_mbot_VERSION "0.0.0")
set(screen_mbot_BUILD_DEPENDS "roscpp" "rospy" "std_msgs" "liveliness_init")
set(screen_mbot_RUN_DEPENDS "roscpp" "rospy" "std_msgs" "liveliness_init")
set(screen_mbot_BUILDTOOL_DEPENDS "catkin")