# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/ines/catkin_ws/src/liveliness/screen_mbot/src/liveliness_screen_mbot.cpp" "/home/ines/catkin_ws/build/liveliness/screen_mbot/CMakeFiles/liveliness_screen_mbot.dir/src/liveliness_screen_mbot.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  "MAGICKCORE_QUANTUM_DEPTH=16"
  "MAGICKCORE_HDRI_ENABLE=0"
  "ROS_PACKAGE_NAME=\"screen_mbot\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
