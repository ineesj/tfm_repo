# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "interaction_actions: 21 messages, 0 services")

set(MSG_I_FLAGS "-Iinteraction_actions:/home/ines/catkin_ws/devel/share/interaction_actions/msg;-Iactionlib_msgs:/opt/ros/hydro/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/hydro/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(interaction_actions_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)
_generate_msg_cpp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
)

### Generating Services

### Generating Module File
_generate_module_cpp(interaction_actions
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(interaction_actions_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(interaction_actions_generate_messages interaction_actions_generate_messages_cpp)

# target for backward compatibility
add_custom_target(interaction_actions_gencpp)
add_dependencies(interaction_actions_gencpp interaction_actions_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS interaction_actions_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)
_generate_msg_lisp(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
)

### Generating Services

### Generating Module File
_generate_module_lisp(interaction_actions
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(interaction_actions_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(interaction_actions_generate_messages interaction_actions_generate_messages_lisp)

# target for backward compatibility
add_custom_target(interaction_actions_genlisp)
add_dependencies(interaction_actions_genlisp interaction_actions_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS interaction_actions_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseResult.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionResult.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/IdleInteractionGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeechGrammarGoal.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/hydro/share/actionlib_msgs/cmake/../msg/GoalID.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)
_generate_msg_py(interaction_actions
  "/home/ines/catkin_ws/devel/share/interaction_actions/msg/SpeakPhraseFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
)

### Generating Services

### Generating Module File
_generate_module_py(interaction_actions
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(interaction_actions_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(interaction_actions_generate_messages interaction_actions_generate_messages_py)

# target for backward compatibility
add_custom_target(interaction_actions_genpy)
add_dependencies(interaction_actions_genpy interaction_actions_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS interaction_actions_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/interaction_actions
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(interaction_actions_generate_messages_cpp actionlib_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/interaction_actions
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(interaction_actions_generate_messages_lisp actionlib_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/interaction_actions
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(interaction_actions_generate_messages_py actionlib_msgs_generate_messages_py)
