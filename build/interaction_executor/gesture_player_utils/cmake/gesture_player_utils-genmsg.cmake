# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "gesture_player_utils: 3 messages, 0 services")

set(MSG_I_FLAGS "-Igesture_player_utils:/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg;-Istd_msgs:/opt/ros/hydro/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(gesture_player_utils_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/GestureStatus.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_cpp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesturePriority.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_cpp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils
)

### Generating Services

### Generating Module File
_generate_module_cpp(gesture_player_utils
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(gesture_player_utils_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(gesture_player_utils_generate_messages gesture_player_utils_generate_messages_cpp)

# target for backward compatibility
add_custom_target(gesture_player_utils_gencpp)
add_dependencies(gesture_player_utils_gencpp gesture_player_utils_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gesture_player_utils_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/GestureStatus.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_lisp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesturePriority.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_lisp(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils
)

### Generating Services

### Generating Module File
_generate_module_lisp(gesture_player_utils
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(gesture_player_utils_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(gesture_player_utils_generate_messages gesture_player_utils_generate_messages_lisp)

# target for backward compatibility
add_custom_target(gesture_player_utils_genlisp)
add_dependencies(gesture_player_utils_genlisp gesture_player_utils_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gesture_player_utils_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/GestureStatus.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_py(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesturePriority.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg;/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils
)
_generate_msg_py(gesture_player_utils
  "/home/ines/catkin_ws/src/interaction_executor/gesture_player_utils/msg/KeyframeGesture.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/hydro/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils
)

### Generating Services

### Generating Module File
_generate_module_py(gesture_player_utils
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(gesture_player_utils_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(gesture_player_utils_generate_messages gesture_player_utils_generate_messages_py)

# target for backward compatibility
add_custom_target(gesture_player_utils_genpy)
add_dependencies(gesture_player_utils_genpy gesture_player_utils_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gesture_player_utils_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gesture_player_utils
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(gesture_player_utils_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gesture_player_utils
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(gesture_player_utils_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gesture_player_utils
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(gesture_player_utils_generate_messages_py std_msgs_generate_messages_py)
