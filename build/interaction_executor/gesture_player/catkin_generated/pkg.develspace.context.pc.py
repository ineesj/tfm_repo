# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ines/catkin_ws/src/interaction_executor/gesture_player/src".split(';') if "/home/ines/catkin_ws/src/interaction_executor/gesture_player/src" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;roslib;std_msgs;utils;gesture_player_utils;etts_msgs;monarch_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "gesture_player"
PROJECT_SPACE_DIR = "/home/ines/catkin_ws/devel"
PROJECT_VERSION = "0.0.0"
