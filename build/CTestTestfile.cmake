# CMake generated Testfile for 
# Source directory: /home/ines/catkin_ws/src
# Build directory: /home/ines/catkin_ws/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(dynamixel_motor/dynamixel_driver)
SUBDIRS(dynamixel_motor/dynamixel_controllers)
SUBDIRS(dynamixel_motor/dynamixel_msgs)
SUBDIRS(liveliness/head_maggie)
SUBDIRS(liveliness/leds_mini)
SUBDIRS(liveliness/liveliness_init)
SUBDIRS(liveliness/arms_mbot)
SUBDIRS(liveliness/leds_mbot)
SUBDIRS(liveliness/neck_mbot)
SUBDIRS(liveliness/screen_maggie)
SUBDIRS(liveliness/screen_mbot)
SUBDIRS(liveliness/screen_mini)
SUBDIRS(dynamixel_speed)
SUBDIRS(liveliness/head_mini)
SUBDIRS(liveliness/neck_mini)
