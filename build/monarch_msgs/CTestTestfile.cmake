# CMake generated Testfile for 
# Source directory: /home/ines/catkin_ws/src/monarch_msgs
# Build directory: /home/ines/catkin_ws/build/monarch_msgs
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_monarch_msgs_nosetests_src.test.test_kvpa_utils.py "/home/ines/catkin_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/hydro/share/catkin/cmake/test/run_tests.py" "/home/ines/catkin_ws/build/test_results/monarch_msgs/nosetests-src.test.test_kvpa_utils.py.xml" "--return-code" "/usr/bin/cmake -E make_directory /home/ines/catkin_ws/build/test_results/monarch_msgs" "/usr/bin/nosetests -P --process-timeout=60 /home/ines/catkin_ws/src/monarch_msgs/src/test/test_kvpa_utils.py --with-xunit --xunit-file=/home/ines/catkin_ws/build/test_results/monarch_msgs/nosetests-src.test.test_kvpa_utils.py.xml")
