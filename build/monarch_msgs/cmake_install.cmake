# Install script for directory: /home/ines/catkin_ws/src/monarch_msgs

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/opt/monarch_msgs")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  INCLUDE("/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/safe_execute_install.cmake")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/monarch_msgs/msg" TYPE FILE FILES
    "/home/ines/catkin_ws/src/monarch_msgs/msg/ArmsControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/AuxiliaryBatteriesVoltage.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/BatteriesVoltage.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/BehaviorRequestLog.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/BehaviorSelection.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/BehaviorSelectionArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/BumpersReadings.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/CapacitiveSensorsReadings.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/ChargerStatus.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/EyesControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/FourWheelOmniDelta.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GameGoal.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GameHriGoal.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GamePlayerStatus.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GameTurnSummary.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GestureExpression.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GroundSensorsReadings.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/HeadControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/HardstopStatus.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/InteractionExecutorStatus.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/KeyValuePair.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/KeyValuePairArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/LedControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MotorBoardCommunicationStatusReadings.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MotorBoardTemperatures.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MotorBoardVoltages.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MotorsCoolingFans.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MouthLedControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MouthPictureLedControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MouthStringLedControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/MouthVumeterControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/NonVerbalSound.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonFrontofRobot.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonFrontRobotArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonLocalizationTrackingData.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonLocalizationTrackingDataArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/mcmc.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/mcmcArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/loc.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/locs.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PlayAudioControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PlayVideoControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/RecognizedSpeech.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/RfidReading.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/ScreenButton.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/ScreenInterfaceInputConfig.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SensorBoardCommunicationStatusReadings.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetMotorBoardCoolingFans.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetStateAuxiliaryPowerBattery.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetStateElectronicPower.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetStateImu.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetStateMotorsPower.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/SetStateSonars.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/Utterance.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/VideoProjectorControl.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonLocalizationTrackingParticle.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/PersonLocalizationTrackingParticleArray.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GaussianCostmap.msg"
    "/home/ines/catkin_ws/src/monarch_msgs/msg/GaussianCostmapArray.msg"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/monarch_msgs/cmake" TYPE FILE FILES "/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/installspace/monarch_msgs-msg-paths.cmake")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/ines/catkin_ws/devel/include/monarch_msgs")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/ines/catkin_ws/devel/share/common-lisp/ros/monarch_msgs")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/ines/catkin_ws/devel/lib/python2.7/dist-packages/monarch_msgs")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/ines/catkin_ws/devel/lib/python2.7/dist-packages/monarch_msgs")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/installspace/monarch_msgs.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/monarch_msgs/cmake" TYPE FILE FILES "/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/installspace/monarch_msgs-msg-extras.cmake")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/monarch_msgs/cmake" TYPE FILE FILES
    "/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/installspace/monarch_msgsConfig.cmake"
    "/home/ines/catkin_ws/build/monarch_msgs/catkin_generated/installspace/monarch_msgsConfig-version.cmake"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/monarch_msgs" TYPE FILE FILES "/home/ines/catkin_ws/src/monarch_msgs/package.xml")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

