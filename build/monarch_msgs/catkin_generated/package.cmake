set(_CATKIN_CURRENT_PACKAGE "monarch_msgs")
set(monarch_msgs_MAINTAINER "Marco Barbosa <marco.barbosa@selftech.pt>")
set(monarch_msgs_DEPRECATED "")
set(monarch_msgs_VERSION "0.1.4")
set(monarch_msgs_BUILD_DEPENDS "message_generation" "geometry_msgs" "sensor_msgs" "std_msgs" "rospy" "move_base_msgs")
set(monarch_msgs_RUN_DEPENDS "message_runtime" "geometry_msgs" "sensor_msgs" "std_msgs" "rospy" "roslaunch" "move_base_msgs")
set(monarch_msgs_BUILDTOOL_DEPENDS "catkin")