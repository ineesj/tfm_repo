# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/opt/monarch_msgs/include".split(';') if "/opt/monarch_msgs/include" != "" else []
PROJECT_CATKIN_DEPENDS = "message_runtime;geometry_msgs;sensor_msgs;std_msgs;move_base_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "monarch_msgs"
PROJECT_SPACE_DIR = "/opt/monarch_msgs"
PROJECT_VERSION = "0.1.4"
