# generated from genmsg/cmake/pkg-msg-paths.cmake.em

# message include dirs in installspace
_prepend_path("${monarch_msgs_DIR}/.." "msg" monarch_msgs_MSG_INCLUDE_DIRS UNIQUE)
set(monarch_msgs_MSG_DEPENDENCIES geometry_msgs;sensor_msgs;std_msgs;actionlib_msgs;move_base_msgs)
