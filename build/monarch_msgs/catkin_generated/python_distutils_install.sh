#!/bin/sh -x

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

cd "/home/ines/catkin_ws/src/monarch_msgs"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
/usr/bin/env \
    PYTHONPATH="/opt/monarch_msgs/lib/python2.7/dist-packages:/home/ines/catkin_ws/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/ines/catkin_ws/build" \
    "/usr/bin/python" \
    "/home/ines/catkin_ws/src/monarch_msgs/setup.py" \
    build --build-base "/home/ines/catkin_ws/build/monarch_msgs" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/opt/monarch_msgs" --install-scripts="/opt/monarch_msgs/bin"
