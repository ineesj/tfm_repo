# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "asr_msgs: 1 messages, 0 services")

set(MSG_I_FLAGS "-Iasr_msgs:/home/ines/catkin_ws/src/hri_interfaces/asr_msgs/msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(asr_msgs_generate_messages ALL)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(asr_msgs
  "/home/ines/catkin_ws/src/hri_interfaces/asr_msgs/msg/open_grammar_recog_results.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/asr_msgs
)

### Generating Services

### Generating Module File
_generate_module_cpp(asr_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/asr_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(asr_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(asr_msgs_generate_messages asr_msgs_generate_messages_cpp)

# target for backward compatibility
add_custom_target(asr_msgs_gencpp)
add_dependencies(asr_msgs_gencpp asr_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS asr_msgs_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(asr_msgs
  "/home/ines/catkin_ws/src/hri_interfaces/asr_msgs/msg/open_grammar_recog_results.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/asr_msgs
)

### Generating Services

### Generating Module File
_generate_module_lisp(asr_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/asr_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(asr_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(asr_msgs_generate_messages asr_msgs_generate_messages_lisp)

# target for backward compatibility
add_custom_target(asr_msgs_genlisp)
add_dependencies(asr_msgs_genlisp asr_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS asr_msgs_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(asr_msgs
  "/home/ines/catkin_ws/src/hri_interfaces/asr_msgs/msg/open_grammar_recog_results.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/asr_msgs
)

### Generating Services

### Generating Module File
_generate_module_py(asr_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/asr_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(asr_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(asr_msgs_generate_messages asr_msgs_generate_messages_py)

# target for backward compatibility
add_custom_target(asr_msgs_genpy)
add_dependencies(asr_msgs_genpy asr_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS asr_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/asr_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/asr_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/asr_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/asr_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/asr_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/asr_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/asr_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
